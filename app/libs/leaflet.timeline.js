(function (factory) {
    // Packaging/modules magic dance
    var L;
    var moment;
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['leaflet', 'moment'], factory);
    } else if (typeof module !== 'undefined') {
        // Node/CommonJS
        L = require('leaflet');
        moment = require('moment');
        module.exports = factory(L, moment);
    } else {
        // Browser globals
        if (typeof window.L === 'undefined') {
            throw new Error('Leaflet must be loaded first');
        }
        if (typeof window.moment === 'undefined') {
            throw new Error('Moment.js must be loaded first');
        }
        factory(window.L, window.moment);
    }
}(function (L, moment) {
    'use strict';

    L.Control.Timeline = (function () {

        var Timeline = L.Draggable.extend({
            initialize: function (element, stepHeight, timelineHeight, stepPoints) {
                L.Draggable.prototype.initialize.call(this, element, element);
                this._element = element;

                this._stepHeight = stepHeight;
                this._timelineHeight = timelineHeight;
                this._stepPoints  = stepPoints;

                this.on('predrag', function () {
                    this._newPos.x = 0;
                    this._newPos.y = this.setPosition(this._newPos.y);
                }, this);
            },

            _currentSelected: null,

            _closestStep: function (num, stepPoints) {
                var curr = stepPoints[0].position;
                var step = null;
                var diff = Math.abs (num - curr);
                var i = 0;
                for (i; i < stepPoints.length;) {
                    var newdiff = Math.abs (num - stepPoints[i].position);
                    if (newdiff <= diff) {
                        diff = newdiff;
                        curr = stepPoints[i].position;
                        step = stepPoints[i];
                    }
                    i = i + 1;
                }
                this._currentSelected = step;
                return curr;
            },

            _adjust: function (y, relative) {
                // pega ponto mais próximo da posição relativa y
                // priorizar critério por ponto com valor
                var valueY;

                if (relative) {
                    valueY = y;
                } else {
                    valueY = (100 * Math.round(y)) / this._timelineHeight; // relativo
                }

                var stepPoints = this._stepPoints;

                if (stepPoints && stepPoints.length > 0) {
                    return (this._timelineHeight * this._closestStep(valueY, stepPoints) / 100); // converte posição relativa para absoluta na timeline
                }
            },

            setPositionRelative: function (y) {
                L.DomUtil.setPosition(this._element,
                    L.point(0, this._adjust(y, true)));
            },


            setPosition: function (y) {
                y = this._adjust(y);
                L.DomUtil.setPosition(this._element,
                    L.point(0, y));
                return y;
            },

            getValue: function () {
                return this._currentSelected;
            }
        });

        var TimelineControl = L.Control.extend({
            options: {
                position: 'topright',
                // Height of zoom-slider.png in px
                stepHeight: 8,
                // Height of the timeline div in px (including border)
                timelineHeight: 120,
                styleNS: 'leaflet-control-timeline',
                stepPoints: []
            },

            _stepPoints: [],

            initialize: function (o) {
                //L.Control.prototype.initialize.call(this.options);

                if ( o.stepPoints.length > 0) {
                    this.options.stepPoints = o.stepPoints;
                    this.setData(o.stepPoints);
                }

            },

            setData: function (stepPoints) {
                // [{ dataRef: 10/10/2010 10:10:00, value: n, properties: { n:n }}, ...]
                var length = this.options.stepPoints.length;
                var firstDate = stepPoints[0];
                var lastDate = stepPoints[length - 1];
                var i = 0;
                var position = 0;

                // first date
                var totalSteps = moment(lastDate.dataRef, 'DD/MM/YYYY HH:mm:ss').diff(moment(firstDate.dataRef, 'DD/MM/YYYY HH:mm:ss'), 'days');
                //this._timeline.setSteps(steps);
                // ex: 122 de 365 ->

                for (i; i < length;) {
                    // posição relativa
                    position = moment(lastDate.dataRef, 'DD/MM/YYYY HH:mm:ss').diff(moment(stepPoints[i].dataRef, 'DD/MM/YYYY HH:mm:ss'), 'days');
                    stepPoints[i].index = i;
                    stepPoints[i].position = (position * 100) / totalSteps;
                    i = i + 1;
                }

                this._stepPoints = stepPoints;
            },
            _hasSelectedValue: false,
            onAdd: function (map) {
                var i = 0;
                var length = this._stepPoints.length;
                var marker = null;

                this._map = map;
                this._ui = this._createUI();
                this._ui.body.style.height = this.options.timelineHeight + 'px';

                this._timeline = new Timeline(this._ui.timeline,
                    this.options.stepHeight,
                    this.options.timelineHeight,
                    this._stepPoints
                );

                for (i; i < length;) {
                    var position = this._stepPoints[i].position;
                    marker = L.DomUtil.create('div', 'timeline-marker', this._ui.body);
                    marker.style.top = Math.round(position) + '%';
                    if (this._stepPoints[i].hasOwnProperty('selected') && this._stepPoints[i].selected === true) {
                        this._timeline.setPosition(position);
                        var x = this._timeline.getValue();
                        this._hasSelectedValue = true;
                        this._map.fire('timeline.change', [x]);
                    }
                    i = i + 1;
                }

                map.whenReady(this._initTimeline,        this)
                    .whenReady(this._initEvents,      this)
                    //.whenReady(this._updateSize,      this)
                    .whenReady(this._updateTimelineValue, this)
                    .whenReady(this._updateDisabled,  this);

                if (this._hasSelectedValue) {
                    this.enableClear(false);
                }

                return this._ui.bar;
            },

            onRemove: function (map) {
                //.off('zoomlevelschange',         this._updateSize,      this)
                //map.off('zoomend zoomlevelschange', this._updateTimelineValue, this)
                    //.off('zoomend zoomlevelschange', this._updateDisabled,  this);
            },

            _createUI: function () {
                var ui = {},
                    ns = this.options.styleNS;

                ui.bar     = L.DomUtil.create('div', ns + ' leaflet-bar clear');
                ui.next  = this._createDirectionBtn('up', 'top', ui.bar);
                ui.wrap    = L.DomUtil.create('div', ns + '-wrap leaflet-bar-part', ui.bar);
                ui.previous = this._createDirectionBtn('down', 'bottom', ui.bar);
                ui.body    = L.DomUtil.create('div', ns + '-body', ui.wrap);
                ui.timeline    = L.DomUtil.create('div', ns + '-handler');
                ui.tooltip    = L.DomUtil.create('div', 'leaflet-control-handler-tooltip', ui.timeline);
                ui.clear  = this._createClearBtn(ui.bar);

                L.DomEvent.disableClickPropagation(ui.bar);
                L.DomEvent.disableClickPropagation(ui.timeline);

                return ui;
            },
            _createClearBtn: function (container) {
                var classDef = this.options.styleNS + '-' + 'clear' +
                        ' leaflet-bar-part' +
                        ' leaflet-bar-part-clear',
                    link = L.DomUtil.create('a', classDef, container);

                var icon = L.DomUtil.create('i', 'material-icons', link);
                icon.innerHTML = 'add';

                link.href = '#';
                link.title = 'Escolher período';

                L.DomEvent.on(icon, 'click', L.DomEvent.preventDefault);
                L.DomEvent.on(link, 'click', L.DomEvent.preventDefault);

                return link;
            },
            _createDirectionBtn: function (dir, end, container) {
                var classDef = this.options.styleNS + '-' + dir +
                        ' leaflet-bar-part' +
                        ' leaflet-bar-part-' + end,
                    link = L.DomUtil.create('a', classDef, container);

                var icon = L.DomUtil.create('i', 'material-icons', link);
                icon.innerHTML = dir === 'up' ? 'arrow_upward' : 'arrow_downward';

                link.href = '#';
                link.title =  dir === 'up' ? 'Próximo' : 'Anterior';

                L.DomEvent.on(icon, 'click', L.DomEvent.preventDefault);
                L.DomEvent.on(link, 'click', L.DomEvent.preventDefault);

                return link;
            },

            _initTimeline: function () {
                this._timeline.enable();
                this._ui.body.appendChild(this._ui.timeline);
            },
            _initEvents: function () {
                this._map
                    //.on('zoomlevelschange',         this._updateSize,      this)
                    //.on('zoomend zoomlevelschange', this._updateTimelineValue, this)
                    //.on('zoomend zoomlevelschange', this._updateDisabled,  this);

                L.DomEvent.on(this._ui.body,    'click', this._onSliderClick, this);
                L.DomEvent.on(this._ui.next,  'click', this._next,        this);
                L.DomEvent.on(this._ui.previous, 'click', this._previous,       this);
                L.DomEvent.on(this._ui.clear, 'click', this._clearClick,       this);

                this._timeline.on('drag', this._onDrag, this);
                this._timeline.on('dragend', this._onDragEnd, this);

            },
            _timeout: 3000,
            showTooltipTimer: function () {
                var self = this;
                self._ui.tooltip.style.display = 'block';
                self._ui.tooltip.innerHTML = self._timeline.getValue().dataRef;
                window.clearTimeout(this._timeout);
                this._timeout = window.setTimeout(function () {
                    self._ui.tooltip.style.display = 'none';
                }, 3000);
            },
            enableClear: function (setPosition) {
                var icon = this._ui.clear.getElementsByClassName('material-icons')[0];
                icon.innerHTML  = 'clear';
                this._ui.clear.title = 'Limpar';
                this._ui.bar.classList.remove('clear');
                if (setPosition) {
                    this._timeline.setPosition(0);
                    var x = this._timeline.getValue();
                    this._map.fire('timeline.change', [x]);
                }
                this.showTooltipTimer();
            },
            disableClear: function () {
                var icon = this._ui.clear.getElementsByClassName('material-icons')[0];
                icon.innerHTML = 'add';
                this._ui.clear.title = 'Escolher período';
                this._ui.bar.classList.add('clear');
                this._map.fire('timeline.clear');
                this._ui.tooltip.style.display = 'none';
            },
            _clearClick: function (e) {
                if (this._ui.bar.classList.contains('clear')) {
                    this.enableClear(true);
                } else {
                    this.disableClear();
                }
            },
            _onSliderClick: function (e) {
                var self = this;
                var first = (e.touches && e.touches.length === 1 ? e.touches[0] : e),
                    y = L.DomEvent.getMousePosition(first, this._ui.body).y;

                this._timeline.setPosition(y);
                var x = this._timeline.getValue();

                this._map.fire('timeline.change', [x]);

                self._ui.bar.classList.remove('clear');
                this.showTooltipTimer();
            },

            _onDragEnd: function (e) {
                var x = this._timeline.getValue();
                this._map.fire('timeline.change', [x]);
            },

            _onDrag: function (e) {
                this.showTooltipTimer();
                console.log('drag');
            },

            _next: function () {
                var currentIndex = this._timeline.getValue().index;
                if (this._stepPoints[currentIndex + 1]) {
                    this._timeline.setPositionRelative(this._stepPoints[currentIndex + 1].position);
                    var x = this._timeline.getValue();
                    this.showTooltipTimer();
                    this._map.fire('timeline.change', [x]);
                }
                //this._map.zoomIn(e.shiftKey ? 3 : 1);
            },
            _previous: function (e) {
                var currentIndex = this._timeline.getValue().index;
                if (this._stepPoints[currentIndex - 1]) {
                    this._timeline.setPositionRelative(this._stepPoints[currentIndex - 1].position);
                    var x = this._timeline.getValue();
                    this.showTooltipTimer();
                    this._map.fire('timeline.change', [x]);
                }
            },

            _updateTimelineValue: function () {
                //this._timeline.setValue(this._toValue(1));
            },
            _updateDisabled: function () {
                var className = this.options.styleNS + '-disabled';

                L.DomUtil.removeClass(this._ui.next,  className);
                L.DomUtil.removeClass(this._ui.previous, className);

            }
        });

        return TimelineControl;
    })();

    L.Map.addInitHook(function () {
        if (this.options.timelineControl) {
            this.timelineControl = new L.Control.Timeline();
            this.addControl(this.timelineControl);
        }
    });

    L.control.timeline = function (options) {
        return new L.Control.Timeline(options);
    };
}));
