define(['leaflet'], function (L) {
    // L.Tooltip.include({
    //     _setPosition: function (pos) {
    //         var layer = this._source,
    //             map = this._map,
    //             container = this._container,
    //             element = layer.getElement(),
    //             centerPoint = map.latLngToContainerPoint(map.getCenter()),
    //             tooltipPoint = map.layerPointToContainerPoint(pos),
    //             direction = this.options.direction,
    //             tooltipWidth = container.offsetWidth,
    //             tooltipHeight = container.offsetHeight,
    //             offset = L.point(this.options.offset),
    //             anchor = this._getAnchor();
    //
    //         // if (layer.getType() === mapUtils.type.POLYGON ||
    //         //     layer.getType() === mapUtils.type.MULTIPOLYGON) {
    //         //     //
    //         //     // L.DomUtil.addClass(container, 'leaflet-tooltip-area');
    //         //     //
    //         //     // $(container).css({ width: element.getBoundingClientRect().width, height: element.getBoundingClientRect().height });
    //         //     L.DomUtil.setPosition(container, pos.subtract(L.point( element.getBoundingClientRect().width / 2, element.getBoundingClientRect().height / 2, true )));
    //         //     window.setTimeout(function () {
    //         //         fitTeuxt(container, 1.2, { maxFontSize: '15px', minFontSize: '7px' });
    //         //     }, 50);
    //         //
    //         //
    //         // } else {
    //
    //         if (direction === 'top') {
    //             pos = pos.add(L.point(-tooltipWidth / 2 + offset.x, -tooltipHeight + offset.y + anchor.y, true));
    //         } else if (direction === 'bottom') {
    //             pos = pos.subtract(L.point(tooltipWidth / 2 - offset.x, -offset.y, true));
    //         } else if (direction === 'center') {
    //             pos = pos.subtract(L.point(tooltipWidth / 2 + offset.x, tooltipHeight / 2 - anchor.y + offset.y, true));
    //         } else if (direction === 'right' || direction === 'auto' && tooltipPoint.x < centerPoint.x) {
    //             direction = 'right';
    //             pos = pos.add(L.point(offset.x + anchor.x, anchor.y - tooltipHeight / 2 + offset.y, true));
    //         } else {
    //             direction = 'left';
    //             pos = pos.subtract(L.point(tooltipWidth + anchor.x - offset.x, tooltipHeight / 2 - anchor.y - offset.y, true));
    //         }
    //
    //         L.DomUtil.removeClass(container, 'leaflet-tooltip-right');
    //         L.DomUtil.removeClass(container, 'leaflet-tooltip-left');
    //         L.DomUtil.removeClass(container, 'leaflet-tooltip-top');
    //         L.DomUtil.removeClass(container, 'leaflet-tooltip-bottom');
    //         L.DomUtil.addClass(container, 'leaflet-tooltip-' + direction);
    //         L.DomUtil.setPosition(container, pos);
    //         //}
    //     }
    // });
    L.Layer.include({
        bindTooltipReplace: function () {
            var self = this;
            var args = [].slice.call(arguments);
            var text = args.shift(); // text: "Titulo: {0} <br /> Valor 2: {1}", args, ['Descricao', 'Valor']
            var layer = this;
            if (args.length > 0) {
                var feature = layer.feature;
                var textResult = self._replaceToText(feature, text, args);
                if (feature && textResult) {
                    layer.bindTooltip('<span>' + textResult + '</span>', { position: 'center', permanent: true, sticky: true });
                }
            }
        },
        _replaceToText: function (feature, text, args) {
            var exp = /{(\d{0,99})}/gi;
            var matches = text.match(exp);
            var length = matches.length;
            var i = 0;
            var index;
            for (i; i < length;) {
                index = matches[i].replace(/\D/gi, ''); // {0} -> 0, {1} -> 1
                if (args[index] && feature.properties.hasOwnProperty(args[index])) {
                    text = text.replace(new RegExp('\\{' + i + '\\}', 'g'), feature.properties[args[index]]);
                } else {
                    //console.error('O índice ' + index + ' não encontrou resultados');
                    return false;
                }
                i++;
            }
            return text;
        }
    });
    L.LayerGroup.include({

        bindTooltipReplace: function () {
            var self = this;
            var args = [].slice.call(arguments);
            var text = args.shift(); // text: "Titulo: {0} <br /> Valor 2: {1}", args, ['Descricao', 'Valor']
            if (args.length > 0) {
                this.eachLayer(function (layer) {
                    var feature = layer.feature;
                    var textResult = self._replaceToText(feature, text, args);
                    if (feature && textResult) {
                        layer.bindTooltip('<span>' + textResult + '</span>', { position: 'center', permanent: true, sticky: true });
                    }
                });
            }
        },
        openTooltip: function () {
            this.eachLayer(function (layer) {
                if (layer.feature) {
                    layer.openTooltip();
                }
            });
        },
        closeTooltip: function () {
            this.eachLayer(function (layer) {
                if (layer.feature) {
                    layer.closeTooltip();
                }
            });
        }
    });
});
