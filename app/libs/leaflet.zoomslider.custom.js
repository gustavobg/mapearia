define(['leaflet', 'leaflet.zoomslider'], function (L) {
    return L.Control.ZoomSliderCustom = L.Control.Zoomslider.extend({
        _createUI: function () {
            var ui = L.Control.Zoomslider.prototype._createUI.call(this);
            ui.tooltip = L.DomUtil.create('div', 'leaflet-control-handler-tooltip', ui.knob);
            return ui;
        },
        _onSliderClick: function (e) {
            L.Control.Zoomslider.prototype._onSliderClick.call(this, e);
            this._ui.tooltip.style.display = 'none';
        },
        _onDrag: function () {
            this._ui.tooltip.style.display = 'block';
            this._ui.tooltip.innerHTML = this._knob.getValue();
        },
        _onDragEnd: function () {
            this._ui.tooltip.style.display = 'none';
        },
        _updateKnobValue: function () {
            L.Control.Zoomslider.prototype._updateKnobValue.call(this);
            this._ui.tooltip.innerHTML = this._map.getZoom();
        },
        _initEvents: function () {
            L.Control.Zoomslider.prototype._initEvents.call(this);
            this._knob.on('drag', this._onDrag, this);
            this._knob.on('dragend', this._onDragEnd, this);
        }
    })
});