define(['knockout', 'text!templates/area-position.html', 'map.utils'], function (ko, templateAreaPosition, mapUtils) {

    'use strict';



    var areaPositionInfo = function (params) {
        var layer = params.layer;
        var geometryInfo = ko.observable();

        ko.computed(function () {
            var layerValue = ko.unwrap(layer);
            //var layerUpdated = mapService.getLayerById(layerValue.getFeatureId(), layerValue.getLayerGroupId());
            //var geoInfo = getLayerAreaPosition(layerUpdated);
            var geoInfo = mapUtils.getLayerAreaPosition(layerValue);
            geometryInfo(geoInfo);
        });

        return {
            layer: layer,
            geometryInfo: geometryInfo
        }
    };

    var instance = null;

    ko.components.register('area-position-info', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                instance = new areaPositionInfo(params, componentInfo);
                return instance;
            }
        },
        template: templateAreaPosition
    });
});