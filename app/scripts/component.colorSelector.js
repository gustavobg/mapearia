define(['knockout', 'text!templates/color-selector.html'], function (ko, templateColorSelector) {

    'use strict';

    ko.components.register('seletor-cores', {
        viewModel: function (params) {
            var selectedColor = params.selectedColor;
            var setColorToLayer = params.setColorToLayer !== undefined ? params.setColorToLayer : true;
            //var colors = ko.observableArray(['#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5', '#2196F3', '#03A9F4', '#00BCD4', '#009688', '#4CAF50', '#8BC34A', '#CDDC39', '#FFEB3B', '#FFC107', '#FF9800', '#FF5722', '#795548', '#9E9E9E', '#607D8B']);
            var colors = ko.observableArray(['#880E4F', '#A52714', '#E65100', '#F9A825', '#FFD600', '#817717', '#558B2F', '#097138', '#006064', '#01579B', '#1A237E', '#673AB7', '#4E342E', '#C2185B', '#FF5252', '#F57C00', '#FBC02D', '#FFEA00', '#AFB42B', '#7CB342', '#0F9D58', '#0097A7', '#0288D1', '#3949AB', '#9C27B0', '#795548', '#F48FB1', '#EDA29B', '#FFCC80', '#FADA80', '#FFFF8D', '#E6EE9C', '#C5E1A5', '#87CEAC', '#B2EBF2', '#A1C2FA', '#9FA8DA', '#CE93D8', '#BCAAA4', '#BDBDBD', '#757575', '#424242', '#000000']);

            var changeColor = function (data) {
                if (setColorToLayer) {
                    //var layerId = ko.unwrap(params.layerId);
                    //var layerGroupId = ko.unwrap(params.layerGroupId);
                    //var layerSet = mapService.getLayerById(layerId, layerGroupId);
                    var layerSet = ko.unwrap(params.layer);
                    if (layerSet.feature.geometry.type === 'Point') {
                        layerSet.setIconStyle({color: data});
                    } else {
                        layerSet.setStyle({fillColor: data, color: data});
                        layerSet.feature.properties.styles.fillColor = data;
                    }
                    layerSet.feature.properties.styles.color = data;
                }
                selectedColor(data);
            };
            return {
                colors: colors,
                selectedColor: selectedColor,
                changeColor: changeColor
            }
        },
        template: templateColorSelector
    });
});