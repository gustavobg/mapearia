define(['knockout', 'text!templates/fill-opacity-selector.html'], function (ko, templateFillOpacitySelector) {

    'use strict';

    ko.components.register('fill-opacity-selector', {
        viewModel: function (params) {
            var selectedFillOpacity = params.selectedFillOpacity;

            var changeFillOpacity = function (data, event) {
                var value = event.target.value;
                //var layerId = ko.unwrap(params.layerId);
                //var layerGroupId = ko.unwrap(params.layerGroupId);
                //var layerSet = mapService.getLayerById(layerId, layerGroupId);
                var layerSet = ko.unwrap(params.layer);
                layerSet.setStyle({ fillOpacity: value });
                layerSet.feature.properties.styles.fillOpacity = value;
                selectedFillOpacity(value);
            };
            return {
                selectedFillOpacity: selectedFillOpacity,
                changeFillOpacity: changeFillOpacity
            }
        },
        template: templateFillOpacitySelector
    });
});


