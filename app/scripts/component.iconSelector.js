define(['leaflet', 'knockout', 'text!templates/icon-selector.html'], function (L, ko, templateIconSelector) {
    'use strict';

    // TODO: Marcardor com letra ou número
    ko.components.register('seletor-icones', {
        viewModel: function (params) {

            var selectedIconName = params.selectedIconName;
            var selectedColor = params.selectedColor;
            var icons = ko.observableArray(['flight', 'directions_car', 'done', 'done_all', 'face', 'local_dining', 'local_drink', 'local_cafe', 'local_florist', 'store_mall_directory', 'local_see', 'directions_run', 'assignment', 'bookmark', 'favorite', 'home', 'lock', 'pets', 'star_rate']);
            var changeIcon = function (data) {
                var layerSet = ko.unwrap(params.layer);
                layerSet.options.icon.options.name = data;
                layerSet.getElement().getElementsByClassName('icon')[0].innerHTML = data;
                layerSet.feature.properties.styles.icon = { options: { name: data } };
                layerSet.feature.properties.styles.color = ko.unwrap(selectedColor);
                selectedIconName(data);
            };
            return {
                icons: icons,
                selectedIconName: selectedIconName,
                changeIcon: changeIcon
            }
        },
        template: templateIconSelector
    });


});

