define(['knockout', 'text!templates/stroke-weight-selector.html'], function (ko, templateStrokeWeightSelector) {
    'use strict';

    ko.components.register('stroke-weight-selector', {
        viewModel: function (params) {
            var selectedStrokeWeight = params.selectedStrokeWeight;
            var changeStrokeWeight = function (data, event) {
                var value = event.target.value;
                //var layerId = ko.unwrap(params.layerId);
                //var layerGroupId = ko.unwrap(params.layerGroupId);
                //var layerSet = mapService.getLayerById(layerId, layerGroupId);
                var layerSet = ko.unwrap(params.layer);
                layerSet.setStyle({ weight: value });
                layerSet.feature.properties.styles.weight = value;
                selectedStrokeWeight(value);
            };
            return {
                selectedStrokeWeight: selectedStrokeWeight,
                changeStrokeWeight: changeStrokeWeight
            }
        },
        template: templateStrokeWeightSelector
    });
});

