define(['leaflet'], function (L) {
    L.Layer.include({
        setInteractive: function (value) {
            if (value === true) {
                this.options.interactive = true;
                this.getElement().classList.add('leaflet-interactive');
            } else {
                this.options.interactive = false;
                this.getElement().classList.remove('leaflet-interactive');
            }
        },
        getId: function () {
            return this._leaflet_id;
        },
        getFeatureId: function () {
            return this.feature.id;
        },
        getLayerGroupId: function () {
            return this.feature.layerGroupId;
        },
        show: function () {
            this.getElement().classList.remove('leaflet-hide');
        },
        hide: function () {
            this.getElement().classList.add('leaflet-hide');
        },
        setZIndex: function (index) {
            var element = this.getElement();
            var parentNode = element.parentNode;
            if (parentNode) {
                var length = parentNode.childNodes.length;
                if (length > 0) {
                    if (index >= length) {
                        this.bringToFront();
                    } else if (index <= 0) {
                        this.bringToBack();
                    } else {
                        L.DomUtil.toBack(parentNode.childNodes[index]);
                    }
                }
            }
        },
        getZIndex: function () {
            var element = this.getElement();
            var parentNode = element.parentNode;
            if (parentNode) {
                return parentNode.childNodes.length;
            } else {
                return null;
            }
        }
    });

    L.Path.include({
        getType: function () {
            return this.toGeoJSON().geometry.type;
        }
    });

    L.Marker.include({
        setIconStyle: function (value) {
            this.options.color = value.color;
            this.getElement().childNodes[0].style.backgroundColor = value.color;
        },
        getType: function () {
            return this.toGeoJSON().geometry.type;
        }
    });

    L.GeoJSON.include({
        //getFeatureId: function (layerId) {
        //    // get by feature id
        //    var layerSelected = null;
        //    this.eachLayer(function (layerId) {
        //        if (layerId === layer.getFeatureId()) {
        //            layerSelected = layer;
        //            return false;
        //        } else {
        //            return true;
        //        }
        //    });
        //    return layerSelected;
        //},
        restoreLayer: function (layer) {
            var feature = layer.toGeoJSON();
            var originalFeature = feature.properties.originalFeature;

            if (feature.hasOwnProperty('properties') && feature.properties.hasOwnProperty('_status') && feature.properties._status === 'new') {
                this.removeLayer(layer);
            } else {
                this.updateData(JSON.parse(originalFeature), layer);
            }
        },
        addDataLayer: function (geojson) {
            // same as addData, but returns layer recently added
            var layers = this.addData(geojson);
            layers = layers.getLayers();
            var layerLength = layers.length;
            return layers[layerLength - 1];
        },
        updateData: function (geojson, layerOrId, newId) {
            var options = this.options;

            if (options.filter && !options.filter(geojson)) { return this; }

            var layer = L.GeoJSON.geometryToLayer(geojson, options);
            if (!layer) {
                return this;
            }
            layer.feature = L.GeoJSON.asFeature(geojson);

            var previousLayer = layerOrId;

            if (previousLayer) {
                // get previous layer properties
                layer.feature.id = previousLayer.feature.id;
                layer.feature.layerGroupId = previousLayer.feature.layerGroupId;
                layer.options.originalFeature = previousLayer.options.originalFeature;
                layer.options.originalStyles = previousLayer.options.originalStyles;
                layer.defaultOptions = layer.options;

                this.removeLayer(previousLayer);
            }

            if (newId) {
                layer.feature.id = newId;
            }

            this.resetStyle(layer);

            if (options.onEachFeature) {
                options.onEachFeature(geojson, layer);
            }

            this.addLayer(layer);

            return layer;
        }
    });

    L.LayerGroup.include({
        setInteractive: function (value) {
            this.eachLayer(function (layer) {
                if (value === true) {
                    layer.options.interactive = true;
                    layer.getElement().classList.add('leaflet-interactive');
                } else {
                    layer.options.interactive = false;
                    layer.getElement().classList.remove('leaflet-interactive');
                }
            });
        },
        show: function () {
            this.eachLayer(function (layer) {
                if (layer.feature) {
                    layer.getElement().classList.remove('leaflet-hide');
                }
            });
        },
        hide: function () {
            this.eachLayer(function (layer) {
                if (layer.feature) {
                    layer.getElement().classList.add('leaflet-hide');
                }
            });
        }
    });

    var layerExtensionPolyGetPerimeter = function () {
        var latlngs = this.getLatLngs();
        var length = 0;
        for (var i = 0; i < latlngs.length; i++) {
            if (Array.isArray(latlngs[i])) {
                for (var x = 1; x < latlngs[i].length; x++) {
                    if (Array.isArray(latlngs[i][x])) {
                        for (var z = 1; z < latlngs[i].length; z++) {
                            length += latlngs[i][x][z].distanceTo(latlngs[i][x][z - 1]);
                        }
                    } else {
                        length += latlngs[i][x].distanceTo(latlngs[i][x - 1]);
                    }
                }
            } else {
                if (i > 0) {
                    length += latlngs[i].distanceTo(latlngs[i - 1]);
                }
            }
        }
        return length;
    };

    L.Polyline.include({
        getPerimeter: layerExtensionPolyGetPerimeter
    });
    L.Polygon.include({
        getPerimeter: layerExtensionPolyGetPerimeter,
        getArea: function () {
            var feature = this.toGeoJSON();
            return turf.area(feature); // m2
        }
    });
});