define(['jquery', 'map.service', 'map.layerEdit', 'map.toolbar', 'chance', 'bootbox', 'toastr', 'wicket.leaflet', 'chance', 'leaflet.timeline', 'leaflet.tooltip.custom', 'bootstrap/modal', 'leaflet.layer.collision', 'leaflet.measure'], function ($, mapService, layerEditService, toolbar, chance, bootbox, toastr, Wkt) {

    'use strict';

    /*
     Projeto Mapearia:

     Foco: Importar GeoJson, editar elementos e visualizar.
     Recursos de Edição:
     - Navegação de camadas e elementos (árvore);
     - Desenhos de polígono, pontos e linhas;
     - Detecção de colisão entre elementos de uma mesma camada, com tomada de decisão.
     - Multiseleção de camadas, com opção de criar multipoligonos, duplicar, remover.
     - Editar estilos de features, aplicar estilos por camada.

     */

    var mapInstance = mapService.init();

    layerEditService.init(mapService, 'layer-edit-container');
    toolbar.init(mapService, { featureGroup: mapService.getFeatureGroup(), templateId: 'tooltip' });
    toolbar.setMode(mapUtils.toolbarMode.DRAW);

    var testValues = [
        { dataRef: '01/01/2017 10:12', value: 5 },
        { dataRef: '15/01/2017 08:02', value: 13 },
        { dataRef: '17/01/2017 08:18', value: 19 },
        { dataRef: '19/02/2017 22:37', value: 170, selected: true },
        { dataRef: '01/11/2017 04:05', value: 231 },
        { dataRef: '30/12/2017 13:55', value: 231 }
    ];

    var timeline = new L.Control.Timeline({position: 'topright', stepPoints: testValues });
    //timeline.setData(testValues);
    mapInstance.addControl(timeline);

    mapInstance.on('timeline.change', function (e, x) {
       console.log(e, x);
    });

    window.mapService = mapService;

    // bind data from localStorage
    if (window.localStorage) {
        var geoJson = localStorage.getItem('mapGeoJson');
        if (geoJson !== null) {
            mapService.featureAdd(geoJson);
        }
    } else {
        throw 'Your browser don\'t support localStorage';
    }


   var collisionLayer = L.layerGroup.collision({margin:5});
   collisionLayer.addTo(mapInstance);
   mapInstance.eachLayer(function (l) {
        if (l.hasOwnProperty('feature')) {
            var type = l.getType();
            if (type !== mapUtils.type.POINT) {
                var center = l.getBounds().getCenter();
            } else {
                var center = l.getLatLng();
            }
            var backgroundColor = l.getElement().getAttribute('stroke');
            var label = L.marker(center, {
                icon: L.divIcon({
                    iconSize: null,
                    className: 'leaflet-label',
                    html: '<div>Teste<br />Talhão XYZ<br />Informação desnecessária YPT<div class="bg" style="background-color: ' + backgroundColor + '"></div></div>'
                })
            }).addTo(collisionLayer);
        }
   });
    //var x = new L.marker()
    //collisionLayer.addLayer( L.marker( {} ) );
    //mapService.getFeatureGroup().bindTooltipReplace('Meu nome é: {0}, também conhecido como: {0}', 'name');

    var addWkt = function (wktString) {
        // Todas as informações são inseridas através de features e suas respectivas propriedades
        var wktInstance = new Wkt.Wkt();
        wktInstance.read(wktString);
        var geoJson = {
            geometry: wktInstance.toJson(),
            properties: {},
            type: 'Feature'
        };
        mapService.featureAdd(geoJson);
    };

    mapService.addFeatureGroupModal = function (e, data) {
        bootbox.prompt({
            title: 'Qual o nome do grupo de camadas?',
            message: '',
            className: 'modal-sm',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar'
                }
            },
            callback: function (title) {
                if (title && title.length > 0) {
                    var featureGroupSelect = $('#featureGroupSelect');
                    mapService.addFeatureGroup(null, title, {});
                } else {
                    toastr.error('Informe o nome da camada');
                }
            }
        });
    };

    window.addWkt = addWkt;

    mapInstance.on('mapearia.featureAdd', function () {
        localStorage.setItem('mapGeoJson', JSON.stringify(mapService.getGeoJson()));
    });

    mapInstance.on('mapearia.featureGroupAdd', function () {
        localStorage.setItem('mapFeatureGroups', JSON.stringify(mapService.getAllFeatureGroups()));
    });

    mapInstance.on('mapearia.featureGroupRemove', function () {
        localStorage.setItem('mapFeatureGroups', JSON.stringify(mapService.getAllFeatureGroups()));
    });

    mapInstance.on('mapearia.featureGroupUpdate', function () {
        localStorage.setItem('mapFeatureGroups', JSON.stringify(mapService.getAllFeatureGroups()));
    });

    mapInstance.on('mapearia.featureUpdate', function () {
        localStorage.setItem('mapGeoJson', JSON.stringify(mapService.getGeoJson()));
    });

    mapInstance.on('mapearia.featureRemove', function () {
        localStorage.setItem('mapGeoJson', JSON.stringify(mapService.getGeoJson()));
    });

    mapInstance.on('mapearia.layerEdit.open', function (layer) {
        toolbar.setMode(mapUtils.toolbarMode.EDIT);
        //var chance = new chance.Chance();
        layer.feature.properties.name = 'Zé bolota';//chance.name();
        layerEditService.setLayer(layer);
        $('#layer-edit-container').show();
    });

    var layerEditCancelEvent = function (event, layer) {
        toolbar.setMode(mapUtils.toolbarMode.DRAW);
        $('#layer-edit-container').hide();
    };

    var layerEditSaveEvent = function (event, layer) {
        toolbar.setMode(mapUtils.toolbarMode.DRAW);
        $('#layer-edit-container').hide();
    };

    mapInstance.on('mapearia.layerEdit.cancel', layerEditCancelEvent);
    mapInstance.on('mapearia.layerEdit.save', layerEditSaveEvent);

});