define(['leaflet', 'tiles.google'], function (L) {

    var gridlayer = {};
    var gridLayerInstance = null;

    gridlayer.init = function (mapInstance) {
        var roadMutant = L.gridLayer.googleMutant({
            maxZoom: 24,
            type: 'roadmap',
            className: 'google-roadmap'
        });

        var satMutant = L.gridLayer.googleMutant({
            maxZoom: 24,
            type: 'satellite',
            className: 'google-satellite'
        });

        var terrainMutant = L.gridLayer.googleMutant({
            maxZoom: 24,
            type: 'terrain',
            className: 'google-terrain'
        });

        var hybridMutant = L.gridLayer.googleMutant({
            maxZoom: 24,
            type: 'hybrid',
            className: 'google-hybrid'
        }).addTo(mapInstance);

        var styleMutant = L.gridLayer.googleMutant({
            styles: [
                {elementType: 'labels', stylers: [{visibility: 'off'}]},
                {featureType: 'water', stylers: [{color: '#444444'}]},
                {featureType: 'landscape', stylers: [{color: '#eeeeee'}]},
                {featureType: 'road', stylers: [{visibility: 'off'}]},
                {featureType: 'poi', stylers: [{visibility: 'off'}]},
                {featureType: 'transit', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative', stylers: [{visibility: 'off'}]},
                {featureType: 'administrative.locality', stylers: [{visibility: 'off'}]}
            ],
            maxZoom: 24,
            type: 'roadmap',
            className: 'google-custom'
        });

        gridLayerInstance = L.control.layers({
            'Híbrido': hybridMutant,
            'Mapa': roadMutant,
            'Aéreo': satMutant,
            'Terreno': terrainMutant,
            'Estilizado': styleMutant
        }, {}, {
            collapsed: true
        }).addTo(mapInstance);

        //var grid = L.gridLayer({  });

        //grid.createTile = function (coords) {
        //    var tile = L.DomUtil.create('div', 'tile-coords');
        //    tile.innerHTML = [coords.x, coords.y, coords.z].join(', ');
        //    return tile;
        //};

        //mapInstance.addLayer(grid);


        //var gridLayers = L.control.layers(baseMaps, null, {}, {
        //    collapsed: false
        //});

        //mapInstance.addControl(gridLayers);

    };

    var getInstance = function () {
        return gridLayerInstance;
    };

    gridlayer.getInstance = getInstance;

    return gridlayer;

});


