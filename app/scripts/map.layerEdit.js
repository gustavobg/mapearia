define(['jquery', 'knockout', 'text!../templates/panel-feature-edit.html', 'toastr', 'component.areaPositionInfo', 'component.strokeWeightSelector',  'component.fillOpacitySelector', 'component.colorSelector', 'component.iconSelector'], function ($, ko, layerEditTemplate, toastr) {

    var layerEdit = {};
    var $layerEditElement = null;
    var isEditing = false;
    var originalLayer = null;
    var _mapService = null;


    // criação de polígono, passos:
    // 1 - Desenho do polígono (polígono fica em suspensão em variável de polígono em edição)
    // 2 - Edição de polígono (polígono continua em suspensão, podendo ser acrescido ou diminuído por ferramentas de desenho)
    //     Após a união dos elementos, o polígono é substituído pela nova forma
    // 3 - Após informar as características, a forma dependendo de parâmetros, sofre um corte entre todos os demais polígonos já desenhados

    var cancelEvent = function (data, e) {
        e.preventDefault();
        layerEdit.cancel();
    };
    var saveEvent = function (data, e) {
        e.preventDefault();
        layerEdit.save();
    };
    layerEdit.viewModel = {
        layer: ko.observable(null),
        cancelEvent: cancelEvent,
        saveEvent: saveEvent,
        // observable properties
        fillColor: ko.observable(),
        strokeWeight: ko.observable(),
        fillOpacity: ko.observable(),
        iconName: ko.observable(),
        layerGroupId: ko.observable(),
        layerFeatureId: ko.observable(),
        layerGeometry: ko.observable()
    };
    // TODO: trocar init por instância?
    layerEdit.init = function (mapService, idElement, layerEditCustomTemplate) {
        _mapService = mapService;
        if (idElement) {
            $layerEditElement = $('#' + idElement);
            $layerEditElement[0].innerHTML = layerEditCustomTemplate || layerEditTemplate;

            ko.applyBindings(layerEdit.viewModel, $layerEditElement[0]);

            return $layerEditElement;
        }
    };

    layerEdit.isEditing = function () {
        return isEditing;
    };

    layerEdit.getLayer = function () {
        return layerEdit.viewModel.layer;
    };

    layerEdit.setBackup = function (layer) {
        // backup feature
        layer.feature = layer.toGeoJSON();
        layer.feature.properties.originalFeature = JSON.stringify(layer.feature);
        // backup styles
        layer.feature.properties.originalStyles = JSON.stringify(_mapService.getLayerStyles(layer));
        return layer;
    };

    layerEdit.add = function (newLayer, layerGroupId) {

        var newFeature = newLayer.toGeoJSON();

        if (newFeature.geometry.type === mapUtils.type.POLYGON) {
            if (layerEdit.isEditing()) {
                // editando polígono existente (editável)
                // se usuário já está editando a layer e adicionar um novo elemento,
                // utilizará o comportamento de unir polígono à edição corrente
                var layerEditing = ko.unwrap(layerEdit.getLayer());
                var featureUnion = turf.union(layerEditing.toGeoJSON(), newFeature);
                var layerUpdated = _mapService.getFeatureGroup(layerGroupId).updateData(featureUnion, layerEditing);
                if (layerUpdated.getType() !== mapUtils.type.MULTIPOLYGON) { // todo, permitir edição de multipoligos no futuro
                    layerUpdated.enableEdit()
                } else {
                    layerUpdated.setInteractive(false);
                }
                layerEdit.setLayer(layerUpdated);

            } else {
                // new feature
                newLayer.feature = newFeature;
                newLayer.feature.properties = {};
                newLayer.feature.layerGroupId = layerGroupId;
                newLayer.feature.properties._status = 'new'; // create temp flag to restore layer removal
                newLayer = _mapService.getFeatureGroup(layerGroupId).addDataLayer(newFeature);
                layerEdit.edit(newLayer);
            }
        } else {
            // not polygon
            newFeature.properties._status = 'new';
            newFeature.layerGroupId = layerGroupId;
            newLayer = _mapService.getFeatureGroup(layerGroupId).addDataLayer(newFeature);
            layerEdit.edit(newLayer);
        }

        return newLayer;
    };

    layerEdit.edit = function (layer) {
        layerEdit.setBackup(layer);
        setEditableProperties(layer);
        _mapService.getInstance().fire('mapearia.layerEdit.open', layer, true);
    };

    var setEditableProperties = function (layer) {
        // inicia edição do elemento

        if (!layer.editEnabled()) {
            isEditing = true;

            var allLayers = _mapService.getAllFeatureLayers();
            allLayers.setInteractive(false); // torna todas as outras layers não editáveis

            layer.show();
            if (layer.getType() === mapUtils.type.POINT) {
                layer.enableEdit()
            } else {
                layer.enableEdit();
                layer.getElement().classList.remove('leaflet-path-hover');
            }
        }
    };

    layerEdit.setLayer = function (layer) {
        originalLayer = layer;
        layerEdit.viewModel.layer(layer);

        layerEdit.viewModel.fillColor(layer.options.fillColor || layer.options.color);
        layerEdit.viewModel.strokeWeight(layer.options.weight);
        layerEdit.viewModel.fillOpacity(layer.options.fillOpacity);
        layerEdit.viewModel.layerFeatureId(layer.feature.id);
        layerEdit.viewModel.layerGroupId(layer.feature.layerGroupId);
        layerEdit.viewModel.layerGeometry(layer.feature.geometry);
        if (layer.options.hasOwnProperty('icon')) {
            layerEdit.viewModel.iconName(layer.options.icon.options.name);
            layerEdit.viewModel.fillColor(layer.options.icon.options.color);
        }

        layer.on('edit', function (e) {
            layerEdit.viewModel.layer.valueHasMutated();
        });

        layer.on('dragend', function (e) {
            var layerDrag = e.target;
            layerEdit.viewModel.layer(layerDrag);
        });
    };

    var resetLayerPanelState = function (currentLayer) {
        isEditing = false;
        currentLayer.disableEdit();
        _mapService.getAllFeatureLayers().setInteractive(true);
    };

    layerEdit.triggerUpdate = function () {
        // atualiza a layer de acordo com o id da feature
        // quando uma layer tem a feature atualizada (Layer.featureUpdate), perde-se o objeto com as informações do elemento SVG, pois o método de atualizar
        // deve obrigatoriamente remover e adicionar a layer utilizando o addData.
        // Enquanto isso não for solucionado, deve-se recuperar a layer novamente quanto este método for executado
        var layer = ko.unwrap(layerEdit.viewModel.layer);
        var idFeature = layer.feature.id;
        var idFeatureGroup = layer.feature.layerGroupId;
        var updatedLayer = _mapService.getLayerById(idFeature, idFeatureGroup);

        layerEdit.viewModel.layer(updatedLayer);
    };

    layerEdit.save = function (featureOverride) {
        var currentLayer = ko.unwrap(layerEdit.viewModel.layer);
        currentLayer.feature = currentLayer.toGeoJSON();

        if (typeof (featureOverride) === 'object') {
            currentLayer.feature = $.extend(true, currentLayer.feature, featureOverride);
        }
        if (currentLayer.feature.hasOwnProperty('properties') && currentLayer.feature.properties.hasOwnProperty('_status')) {
            // status é necessário para remover layer se cancelado edição de elemento recém desenhado
            currentLayer.feature.properties._status = undefined;
        }

        if (_mapService.getOptions().checkPolygonIntersectionsAfterSave && _mapService.getOptions().checkPolygonIntersections) {
            currentLayer.feature = _mapService.featureClipIntersections(currentLayer.feature, true);
            currentLayer = _mapService.getFeatureGroup(currentLayer.feature.layerGroupId).updateData(currentLayer.feature, currentLayer);
        }

        resetLayerPanelState(currentLayer);

        // remove propriedades de backup ao salvar
        delete currentLayer.feature.properties.originalFeature;
        delete currentLayer.feature.properties.originalStyles;


        _mapService.getInstance().fire('mapearia.layerEdit.save', [currentLayer]);
        _mapService.getInstance().fire('mapearia.featureUpdate');

        //console.log('saving layer: ', currentLayer.toGeoJSON()); // atualizado

        return currentLayer;
    };

    layerEdit.cancel = function () {
        var currentLayer = ko.unwrap(layerEdit.viewModel.layer);

        resetLayerPanelState(currentLayer);

        // restore any feature changes
        _mapService.getFeatureGroup(currentLayer.feature.layerGroupId).restoreLayer(currentLayer);
        _mapService.getInstance().fire('mapearia.layerEdit.cancel', [currentLayer]);
    };

    return layerEdit;

});
