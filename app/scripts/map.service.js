define(['leaflet', 'jquery', 'map.layerEdit', 'knockout', 'map.gridlayer', 'text!../templates/popup-feature-edit.html', 'turf.overlaps', 'component.areaPositionInfo', 'leaflet.extensions', 'map.utils'], function (L, $, layerEditorService, ko, gridlayer, templateFeatureEdit, overlaps) {


    //window.overlaps = overlaps;
    // lista de tarefas
    // https://docs.google.com/spreadsheets/d/1Qr4T1xikj3-R3f4tkcIp3JxaGkBEMWLvWRA_-s2baek/edit?usp=sharing
    // TODO: Problema reconhecido. Ao atualizar uma layer, sempre perde-se a referência daquela que foi atualizada
    // TODO: Atenção, nunca acessar a feature, utilizar o getGeoJSON
    // TODO: Implementar funcionalidade de painel aside
    var mapService = {
        instance: null,
        getInstance: function () {
            if (typeof(mapService.instance) === 'object') {
                return mapService.instance;
            } else {
                throw 'Map is not initialized, call mapService.init prior this method';
            }
        }
    };

    var selectedLayers = [];

    var defaultOptions = {
        checkPolygonIntersections: true,
        checkPolygonIntersectionsAfterSave: true,
        createFeatureAfterDraw: false,
        center: new L.LatLng(-22.163565, -48.362347),
        zoomControl: false,
        zoom: 7,
        minZoom: 4,
        maxZoom: 17,
        bindEditPopup: true,
        idElement: 'map',
        idElementLayerEditContainer: 'layer-edit-container',
        layerEditCustomTemplate: null,
        useSingleLayerGroup: false,
        draggablePolygons: true,
        useCustomPopup: false,
        setAutoFeatureIds: true,
        popupOptions: { autoPanPaddingTopLeft: new L.Point(80, 180) },
        showMeasurements: true,
        measurementOptions: {},
        eachLayerFeature: function (l, f) { return l; }
    };

    var options = {};

    mapService.getOptions = function () {
        return options;
    };

    var getGeoJsonOptions = function () {
        return {
            //filter: geoJsonFilter,
            onEachFeature: geoJsonEachFeature,
            style: geoJsonStyle,
            pointToLayer: geoJsonPointToLayer,
            //coordsToLatLng: coordsToLatLng,
            smoothFactor: 0.5
        }
    };

    mapService.init = function (o) {
        // bug de eventos de toque do Chrome
        // https://github.com/Leaflet/Leaflet/issues/5180
        // https://github.com/Leaflet/Leaflet/pull/5187
        // https://github.com/Leaflet/Leaflet.draw/issues/655
        (function () {
            if(! L.Browser.chrome)
                return;

            console.log('Testing chrome version: ');
            var x = navigator.appVersion;
            var y = x.split('Chrome/')[1];

            if( typeof y != 'undefined' ){
                y = y.split(' ')[0];
                if(y >= '55.0.2883.75') {
                    L.Browser.pointer = false;
                    L.Browser.touch = false;
                }
            }
        }());

        options = $.extend({}, defaultOptions, o);
        options.editable = true;

        mapService.baseFeatureGroup = new L.geoJson(null, { filter: geoJsonFilter, onEachFeature: geoJsonEachFeature, style: geoJsonStyle, pointToLayer: geoJsonPointToLayer, /*coordsToLatLng: coordsToLatLng,*/ smoothFactor: 0.5 });

        var mapInstance = new L.Map(options.idElement, options);
        mapService.instance = mapInstance;

        if (options.useSingleLayerGroup) {
            mapService.baseFeatureGroup.addTo(mapInstance);
        }
        mapService.tmpFeatureGroup = new L.geoJson(null, getGeoJsonOptions()).addTo(mapInstance);

        // setup map events
        mapInstance.on('keypress', function (e) {
            console.log(e.originalEvent);
        });

        mapInstance.on('preclick', function (e) {
        });

        gridlayer.init(mapInstance);


        return mapInstance;
    };

    var _featureGroups = [];
    //var _baseFeatureGroup = null;
    var _indexFg = [];

    mapService.getAllFeatureGroups = function () {
        if (_featureGroups.length > 0) {
            return _featureGroups;
        } else {
            return [];
        }
    };

    //var getBaseFeatureGroup = function () {
    //    if (_baseFeatureGroup === null) {
    //        _baseFeatureGroup = new L.geoJson(null, getGeoJsonOptions()).addTo(mapService.getInstance());
    //        return _baseFeatureGroup;
    //    } else {
    //        return _baseFeatureGroup;
    //    }
    //};

    var getFeatureGroupById = function (id) {
        // id -> _leaflet_id
        var fg = null;
        mapService.getInstance().eachLayer(function (layer) {
            if (_indexFg[id] === layer.getId()) {
                fg = layer;
                return true;
            }
        });
        return fg;
    };

    var addFeatureGroup = function (featureGroupId, title, options) {
        featureGroupId = featureGroupId ? featureGroupId : new Date().getTime();
        var mapInstance = mapService.getInstance();
        var fg = new L.geoJson(null, { onEachFeature: geoJsonEachFeature, style: geoJsonStyle, pointToLayer: geoJsonPointToLayer, /*coordsToLatLng: coordsToLatLng,*/ smoothFactor: 0.5 });
        fg.addTo(mapInstance);
        _indexFg[featureGroupId] = fg.getId();
        _featureGroups.push({ id: featureGroupId, title: title, options: options });
        gridlayer.getInstance().addOverlay(fg, title);
        mapInstance.fire('mapearia.featureGroupAdd', _featureGroups[featureGroupId]);
        return fg;
    };


    var coordsToLatLng = function (coords) {
        coords[0] = Number(coords[0].toFixed(5));
        coords[1] = Number(coords[1].toFixed(5));
        if (coords[2]) {
            coords[2] = Number(coords[2].toFixed(5));
            return new L.LatLng(coords[1], coords[0], coords[2]);
        }
        return new L.LatLng(coords[1], coords[0]);
    };

    var setupLayerEvents = function (layer) {

        if (options.bindEditPopup) {
            layer.addEventListener('dblclick', layerDblClick);
            layer.addEventListener('click', layerClick);
        }

        if (!options.useCustomPopup) {
            bindLayerPopup(layer);
        }
    };

    var bindLayerPopup = function (layer) {
        layer.bindPopup(templateFeatureEdit, options.popupOptions)
            .addEventListener('popupopen', editPopupOpen)
            .addEventListener('popupclose', editPopupClose);
    };

    var unbindLayerPopup = function (layer) {
        layer.unbindPopup();
    };

    var geoJsonStyle = function (feature) {
        // get feature style properties and apply to leaflet layer styles
        var featurePropertiesStyle = feature.properties.styles;
        var defaultStyle = Object.assign({}, mapUtils.defaultStyles.POLYGON);
        return $.extend(defaultStyle, featurePropertiesStyle);
    };

    var geoJsonFilter = function (feature) {
        if (options.useSingleLayerGroup) {
            return true;
        }
        if (feature.hasOwnProperty('layerGroupId')) {
            if (_indexFg[feature.layerGroupId] !== undefined) {
                mapService.getFeatureGroupById(feature.layerGroupId).addData(feature);
            } else {
                var fg = addFeatureGroup(feature.layerGroupId, feature.layerGroupId, {});
                fg.addData(feature);
            }
            return false;
        } else {
            // todo: get selected feature group
            return true;
        }
    };

    var geoJsonPointToLayer = function (geojson, latlng) {
        //var color = geojson.hasOwnProperty('properties') && geojson.properties.hasOwnProperty('styles') &&  geojson.properties.styles.hasOwnProperty('color') ? geojson.properties.styles.color : mapUtils.defaultStyles.POINT.color;
        var iconOptions = {};
        var text = geojson.properties.label ? geojson.properties.label : null;
        // get geojson properties
        if (geojson.hasOwnProperty('properties') && geojson.properties.hasOwnProperty('styles') && geojson.properties.styles.hasOwnProperty('icon')) {
            return L.marker(latlng, {
                icon: L.divIcon(mapUtils.getIconOptions({ name: geojson.properties.styles.icon.options.name, text: text, color: geojson.properties.styles.color }))
            });
        } else {
            return L.marker(latlng, {
                icon: L.divIcon(mapUtils.getIconOptions({ name: mapUtils.defaultStyles.POINT.name, text: text, color: geojson.hasOwnProperty('properties') && geojson.properties.hasOwnProperty('styles') ? geojson.properties.styles.color : mapUtils.defaultStyles.POINT.color }))
            });
        }

    };

    mapService.getLayerStyles = function (layer) {
        var type = layer.getType();
        var obj = {};
        if (type === mapUtils.type.POINT) {
            obj = {
                icon: {
                    options: {
                        name: layer.options.icon.options.name
                    }
                },
                color: layer.options.color
            };

        } else {
            obj = {
                stroke: layer.options.stroke,
                color: layer.options.color,
                weight: layer.options.weight,
                fill: layer.options.fill,
                fillColor: layer.options.fillColor,
                fillOpacity: layer.options.fillOpacity,
                fillRule: layer.options.fillRule,
                dashArray: layer.options.dashArray
            }
        }
        return obj;
    };

    var formatDistance = function (a) {
        // m
        var m = a;
        if (m > 1000) {
            return (m / 1000).toFixed(2) + ' km'
        } else {
            return m.toFixed(2) + ' m'
        }
    };

    var formatArea = function (a) {
        var m2 = a;
        if (m2 > 10000) {
            return (m2 / 10000).toFixed(2) + ' ha'
        } else {
            return (m2).toFixed(2) + ' km';
        }
    };

    var _index = [];
    var i = 0;
    var seed = 1;
    var geoJsonEachFeature = function (feature, layer) {

        if (!feature.hasOwnProperty('id') || feature.id == null) {
            feature.id = new Date().getTime() + seed;
            seed++;
        }
        _index[feature.id] = mapService.getFeatureGroup(feature.layerGroupId).getLayerId(layer);

        if (!feature.properties.hasOwnProperty('styles')) {
            feature.properties.styles = {};
        }

        layer.options.selected = false;
        layer.options.interactive = true;

        //if (!feature.properties.hasOwnProperty('originalFeature')) {
        //    feature.properties.originalFeature = JSON.stringify(feature); // clone
        //}
        //if (!feature.properties.hasOwnProperty('originalStyles')) {
        //    feature.properties.originalStyles =  mapService.getLayerStyles(layer); // clone
        //}
        if (feature.properties.hasOwnProperty('text')) {
            //L.Util.setOptions(layer, { text: feature.properties.text });
        }

        setupLayerEvents(layer);

        layer = options.eachLayerFeature(layer, feature);
        mapService.getInstance().fire('mapearia.eachFeature', layer, feature);
    };

    //var featureAdd = function (feature, featureGroupId) {
    var featureAdd = function (feature) {
        //featureGroupId = featureGroupId ? featureGroupId : null;
        var feature = typeof(feature) === 'string' ? JSON.parse(feature) : feature;
        mapService.baseFeatureGroup.addData(feature);
    };

    var getFeatureGroup = function (idFeatureGroup) {
        if (options.useSingleLayerGroup) {
            return mapService.baseFeatureGroup;
        }
        if (idFeatureGroup) {
            if (_indexFg[idFeatureGroup] !== undefined) {
                return getFeatureGroupById(idFeatureGroup);
            }
        }
        if (_featureGroups.length === 0) {
            return mapService.baseFeatureGroup;
        } else if (_featureGroups.length === 1) {
            return getFeatureGroupById(_featureGroups[0].id);
        } else {
            console.error('featureGroup not found');
            return;
        }
    };

    var isBound = function(id) {
        return !!ko.dataFor(document.getElementById(id));
    };

    var editPopupClose = function (e) {
        document.getElementById('popup-feature-edit').id = 'popup-feature-closing';
        var layer = e.target;
        layer.options.selected = false;
        layer.fire('mouseout');
    };

    var editPopupOpen = function (e) {
        var layer = e.target;
        ko.applyBindings({
            layer: layer,
            layerEditPopupEvent: layerEditPopupEvent,
            layerRemovePopupEvent: layerRemovePopupEvent
        }, document.getElementById('popup-feature-edit'));
    };

    var layerEditPopupEvent = function (data) {
        var layer = data.layer;
        layerEdit(layer.getFeatureId(), layer.getLayerGroupId());
    };

    var layerEdit = function (layerId, layerGroupId) {
        var layer = mapService.getLayerById(layerId, layerGroupId);
        layerEditorService.edit(layer, layerGroupId);
        layer.closePopup();
    };

    var layerEditSaveEvent = function (e, layer) {
        mapService.getInstance().fire('mapearia.featureUpdate');
    };

    var layerEditCancelEvent = function (e, layer) {
    };

    var layerRemovePopupEvent = function (data) {
        var layer = data.layer;
        layer.closePopup();
        layerRemove(layer.getFeatureId(), layer.getLayerGroupId());
    };

    var layerRemove = function (layerId, layerGroupId) {
        var layerToRemove = mapService.getLayerById(layerId, layerGroupId);
        mapService.getFeatureGroup(layerGroupId).removeLayer(layerToRemove);
        mapService.getInstance().fire('mapearia.featureRemove', { layerId: layerId, layerGroupId: layerToRemove.getLayerGroupId() });
        return { layerId: layerId, layerGroupId: layerToRemove.getLayerGroupId() };
    };

    var layerClick = function (e) {
        var layer = e.target;
        if (!layer.editEnabled()) {
            bindLayerPopup(layer);
            //e.originalEvent.preventDefault();
            //e.originalEvent.stopImmediatePropagation();
            layerSelect(layer);
        } else {
            unbindLayerPopup(layer);
        }
    };

    var layerUpdate = function (geojson, layer, layerGroupId, newId) {
        mapService.getFeatureGroup(layerGroupId).updateData(geojson, layer, newId);
    };

    var layerDblClick = function (e) {
        L.DomEvent.stopPropagation(e);

        var layer = e.target;

        if (!layer.editEnabled()) {
            layerSelect(layer);
            layerEdit(layer.getFeatureId());
        }
    };

    var layerSelect = function (layer) {
        if (layer.getType() !== mapUtils.type.POINT) {
            layer.options.selected = true;
            clearSelected();
            selectedLayers.push(layer);

            mapService.getFeatureGroup(layer.getLayerGroupId()).eachLayer(function (l) {
                if (l.getType() !== mapUtils.type.POINT) {
                    l.getElement().classList.remove('leaflet-path-hover');
                    //l.bringToBack();
                }
            });
            //layer.getElement().classList.add('leaflet-path-hover');
            //layer.bringToFront();
        }
    };

    var layerShow = function (layerId) {
        var layer = mapService.getLayerById(layerId);
        if (layer) {
            layer.show();
        } else {
            console.error('Layer not found');
        }
    };

    var layerHide = function (layerId) {
        var layer = mapService.getLayerById(layerId);
        if (layer) {
            layer.hide();
        } else {
            console.error('Layer not found');
        }
    };

    var layerSelectZoom = function (layerId) {
        var mapInstance = mapService.getInstance();
        var layer = mapService.getLayerById(layerId);
        if (layer) {
            layerSelect(layer);
            if (layer.getType() === mapUtils.type.POINT) {
                mapService.instance.flyTo(layer.getLatLng(), 18, { animate: false })
            } else {
                mapInstance.flyToBounds(layer.getBounds(), { animate: false });
            }
        } else {
            console.error('Layer not found');
        }
    };
    var layersSelectZoom = function (arrLayerIds) {
        // if param is undefined, get all feature layers
        var mapInstance = mapService.getInstance();
        var feature = null;
        var featureGroup = null;
        var bounds = new L.latLngBounds();
        var count = 0;

        if (arrLayerIds && arrLayerIds.length > 0) {
            var features = getLayersFeaturesByFilter({ inId: arrLayerIds });
            featureGroup = L.featureGroup(features);
        } else {
            // get all features
            featureGroup = getAllFeatureLayers()
        }

        featureGroup.eachLayer(function (layer) {
            feature = layer.toGeoJSON();
            var layerVisible = !layer.getElement().classList.contains('leaflet-hide');
            if (feature && layerVisible) {
                bounds.extend(feature.geometry.type === 'Point' ? layer.getLatLng() : layer.getBounds());
                count++;
            }
        });

        if (count > 0) {
            mapInstance.flyToBounds(bounds, {animate: false});
        }
    };
    var clearSelected = function () {
        // TODO:
        selectedLayers = [];
    };

    //var layerMouseOver = function (e) {
    //    var layer = e.target;
    //    var type = layer.feature.geometry.type;
    //    //if (layer.getType() === mapUtils.type.POINT) {
    //    //    layer.getElement().classList.add('leaflet-marker-hover');
    //    //} else {
    //    //    layer.getElement().classList.add('leaflet-path-hover');
    //    //}
    //    console.log('id: ' + layer.feature.id + ', layer: ' + layer.feature.layerGroupId, layer.feature);
    //};
    //
    //var layerMouseOut = function (e) {
    //    var layer = e.target;
    //    if (layer.options.selected === false) {
    //        if (layer.getType() === mapUtils.type.POINT) {
    //            layer.getElement().classList.remove('leaflet-marker-hover');
    //        } else {
    //            layer.getElement().classList.remove('leaflet-path-hover');
    //        }
    //    }
    //};

    var drawEdited = function (e) {
    };

    mapService.drawEdited = drawEdited;

    var _selectedLayerGroupId = null;
    var getSelectedLayerGroup = function () {
        //return _selectedLayerGroupId;
        if (_featureGroups.length === 0) {
            addFeatureGroup(null, 'Camada 1', {});
            _featureGroups[0].id;
        } else {
            return _featureGroups[0].id; // todo, fazer mecanismo de seleção
        }
    };

    var drawFeature = function (e) {
        // draw new layer feature
        var mapInstance = mapService.getInstance();
        var newLayer = e.layer;
        // todo, pre-selecionar grupo para adicionar camada

        var layerAdded = layerEditorService.add(newLayer, getSelectedLayerGroup());
        // save to db
        mapInstance.fire('mapearia.drawFeature', layerAdded.toGeoJSON());
    };


    var getLayerById = function (id, fg) {
        // id -> _leaflet_id
        // todo get others featureGroups
        return mapService.getFeatureGroup(fg).getLayer(_index[id]);
    };

    var drawFeatureStop = function (e) {
        console.log('drawFeatureStop');
    };

    var getFeatureById = function (idFeature) {
        var result = getFeaturesByFilter({ id: idFeature });
        if (result && result.hasOwnProperty('features') && result.features.length > 0) {
            return result.features[0];
        } else {
            return null;
        }
    };

    var getFeaturesByFilter = function (filterOptions) {
        var layers = getLayersFeaturesByFilter(filterOptions);
        var featureGroup = L.featureGroup(layers);
        return featureGroup.toGeoJSON();
    };

    var getLayersFeaturesByFilter = function (filterOptions) {
        var layers = [];
        var feature = null;
        var filter = {
            id: null,
            geometryType: null,
            layerGroupId: null,
            notInId: null, // string or array todo: array
            inId: null
        };

        $.extend(true, filter, filterOptions);

        var featureCollection = getAllFeatureLayers();

        featureCollection.eachLayer(function (layer) {
           feature = layer.toGeoJSON();
            if (filter.id !== null) {
                if (feature.id !== filter.id) {
                    return;
                }
            }
            if (filter.geometryType !== null) {
                var geometryType = feature.geometry.type;
                // array or string
                if (Array.isArray(filter.geometryType)) {
                    if (filter.geometryType.indexOf(geometryType) === -1) {
                        return;
                    }
                } else {
                    if (geometryType != filter.geometryType) {
                        return;
                    }
                }
            }
            if (filter.layerGroupId !== null) {
                if (filter.layerGroupId != feature.layerGroupId + '') {
                    return;
                }
            }
            if (filter.notInId !== null) {
                if (Array.isArray(filter.notInId)) {
                    if (filter.notInId.indexOf(feature.id + '') === -1) {
                        return;
                    }
                } else {
                    if (filter.notInId == feature.id + '') {
                        return;
                    }
                }
            }
            if (filter.inId !== null && Array.isArray(filter.inId)) {
                if (filter.inId.indexOf(feature.id + '') === -1) {
                    return true;
                }
            }
            layers.push(layer);
        });

        // return layers array
        return layers;
    };

    var featureToTurf = function (feature) {
        // Polygons and MultiPolygons
        // "EPSG:3857" > "EPSG:4326"
        feature = Object.assign({}, feature);
        var multiPolygonLength = feature.geometry.coordinates.length;
        if (multiPolygonLength > 1) {
            // multi
            for (var x = 0; x < multiPolygonLength; x++) {
                for (var i = 0; i < feature.geometry.coordinates[x][0].length; i++) {
                    //console.log(feature.geometry.coordinates[x][0][i]);
                    //feature.geometry.coordinates[x][0][i] = proj4("EPSG:4326", "EPSG:3857", feature.geometry.coordinates[x][0][i]);
                    feature.geometry.coordinates[x][0][i] = proj4('EPSG:4326', 'EPSG:3857', feature.geometry.coordinates[x][0][i]);
                }
            }
        } else {
            // poly
            for (var i = 0; i < feature.geometry.coordinates[0].length; i++) {
                //console.log(feature.geometry.coordinates[0][i]);
                feature.geometry.coordinates[0][i] = proj4('EPSG:4326', 'EPSG:3857', feature.geometry.coordinates[0][i]);
            }
        }
        //console.log('to turf', feature);
        return feature;
    };

    var featureFromTurf = function (feature) {
        // Polygons and MultiPolygons
        // "EPSG:4326" > "EPSG:3857"
        feature = Object.assign({}, feature);
        var multiPolygonLength = feature.geometry.coordinates.length;
        if (multiPolygonLength > 1) {
            // multi
            for (var x = 0; x < multiPolygonLength; x++) {
                for (var i = 0; i < feature.geometry.coordinates[x][0].length; i++) {
                    feature.geometry.coordinates[x][0][i] = proj4('EPSG:3857', 'EPSG:4326', feature.geometry.coordinates[x][0][i]);
                }
            }
        } else {
            // poly
            for (var i = 0; i < feature.geometry.coordinates[0].length; i++) {
                feature.geometry.coordinates[0][i] = proj4('EPSG:3857', 'EPSG:4326', feature.geometry.coordinates[0][i]);
            }
        }
        //console.log('from turf', feature);
        return feature;
    };

    var jstsOperations = {
        union: function (feature, featureToAdd) {
            var reader = new jsts.io.GeoJSONReader();
            var a = reader.read(feature.geometry);
            var b = reader.read(featureToAdd.geometry);

            var r = a.union(b);
            //r = r.buffer(1);

            var parser = new jsts.io.GeoJSONWriter();
            var resultGeometry = parser.write(r);

            feature.geometry = resultGeometry;

            return feature;
        },
        buffer: function (feature) {
            var reader = new jsts.io.GeoJSONReader();
            var a = reader.read(feature.geometry);

            var r = a.buffer(2);
            //r = r.buffer(1);

            var parser = new jsts.io.GeoJSONWriter();
            var resultGeometry = parser.write(r);

            feature.geometry = resultGeometry;

            return feature;
        },
        difference: function (featureToCrop, feature) {

            // feature = gp.parse(feature, 3);
            // featureToCrop = gp.parse(featureToCrop, 3);



            //var geoJsonGeometryFactory = new jsts.geom.GeometryFactory();
            //debugger;
            var reader = new jsts.io.GeoJSONReader();
            var a = reader.read(featureToCrop.geometry);
            var b = reader.read(feature.geometry);

            var r = a.difference(b);

            var parser = new jsts.io.GeoJSONWriter();
            var resultGeometry = parser.write(r);

            // var precisionModel = new jsts.geom.PrecisionModel(3);
            // var geoJsonGeometryFactory = new jsts.geom.GeometryFactory(precisionModel),
            //     geoJsonReader = new jsts.io.GeoJSONReader(geoJsonGeometryFactory),
            //     precisionReducer = new jsts.precision.GeometryPrecisionReducer(precisionModel);
            //
            // precisionReducer.setRemoveCollapsedComponents(true);
            //
            // var _a = geoJsonReader.read(JSON.stringify(resultGeometry));
            //
            // _a = precisionReducer.reduce(_a);
            //
            // resultGeometry = parser.write(_a);
            //
            // debugger;


            // var precisionModel = new jsts.geom..PrecisionModel(6),
            //     geoJsonGeometryFactory = new jsts.geom.GeometryFactory(precisionModel),
            //     geoJsonReader = new jsts.io.GeoJSONReader(geoJsonGeometryFactory),
            //     precisionReducer = new jsts.precision.GeometryPrecisionReducer(precisionModel);
            //

            // debugger;
            // var geoJsonGeometryFactory = new jsts.geom.GeometryFactory();
            // var geoJsonReader = new jsts.io.GeoJSONReader(geoJsonGeometryFactory);
            // var precisionReducer = new jsts.precision.GeometryPrecisionReducer();
            // var _a = geoJsonReader.read(JSON.stringify(resultGeometry));
            // _a = precisionReducer.reduce(_a);




            featureToCrop.geometry = resultGeometry;

            return featureToCrop;
        }
    };

    var featureClipIntersections = function (currentFeature, checkLayerId) {
        // percorremos todos as features de uma mesma camada para efetuar a interseção
        // sempre o último elemento adicionado será o clipado
        if (currentFeature.geometry.type === mapUtils.type.POINT || currentFeature.geometry.type === mapUtils.type.LINESTRING)
            return currentFeature;

        //var originalFeature = jQuery.extend(true, {}, feature);
        var originalFeature = Object.assign({}, currentFeature);

        // pega todas as features polygons, menos o incluso no notIn
        var options = {
            geometryType: ['Polygon', 'MultiPolygon'],
            notInId: currentFeature.id
        };

        if (checkLayerId) {
            options = $.extend(true, options, { layerGroupId: currentFeature.layerGroupId || 0 });
        }

        var featureArray = getFeaturesByFilter(options);
        var algorithm = 'join-to-clip-jsts'; // clip-individual, join-to-clip-turf, join-to-clip-jsts, clip-intersections, clip-intersections-add

        var extendProp = function (p, color) {
            var color = color || '#000000';
            var p = Object.assign({}, p);
            p.properties = {
                styles: {
                    color: color,
                    fillColor: color,
                    fillOpacity: '1'
                }
            };
            return p;
        };

        if (algorithm === 'clip-individual') {
            featureArray.features.every(function (f) {
                if (overlaps(currentFeature, f)) {
                currentFeature = featureToTurf(currentFeature);
                var currentLayerToCrop = mapService.getLayerById(f.id);

                f = featureToTurf(f);

                // TODO
                currentFeature = jstsOperations.difference(currentFeature, f);
                //L.geoJSON(geometryResult).addTo(mapService.getInstance())




                    //var lineIntersect = turf.intersect(currentFeature, f);
                    //var difference = turf.difference(currentFeature, f);
                    //mapService.tmpFeatureGroup.addData(extendProp(featureFromTurf(currentFeature), '#cccccc'));
                    //mapService.tmpFeatureGroup.addData(extendProp(featureFromTurf(f), '#ffffff'));
                    //var differenceIntersect = turf.intersect(currentFeature, difference); // remove line intersection turf bug
                    //if (differenceIntersect) {
                    //    difference = turf.difference(currentFeature, differenceIntersect);
                    //}
                    //if (difference) {
                        //currentFeature.geometry = geometryResult;
                        currentFeature = featureFromTurf(currentFeature);
                        //mapService.tmpFeatureGroup.addData(extendProp(currentFeature, '#FF0000'));
                        //mapService.tmpFeatureGroup.clearLayers();
                        //return true;
                    //}
                }
                return true;

            });

        } else if (algorithm === 'join-to-clip-turf') {
            //'join-to-clip'
            var unionFeature = null;
            var counter = 0;

            featureArray.features.every(function (f) {
                f = featureToTurf(f);
                if (counter === 0) {
                    unionFeature = f;
                } else {
                    unionFeature = turf.union(unionFeature, f);
                    //mapService.tmpFeatureGroup.addData(extendProp(featureFromTurf(unionFeature)));
                }
                counter++;
                return true;
            });

            if (unionFeature !== null) {
                //mapService.tmpFeatureGroup.addData(extendProp(currentFeature, '#FF0000'));
                //mapService.tmpFeatureGroup.addData(extendProp(unionFeature));

                // converte para projeção do turf, EPSG:4326
                currentFeature = featureToTurf(currentFeature);
                unionFeature = unionFeature; // is already converted
                //mapService.tmpFeatureGroup.addData(extendProp(featureFromTurf(unionFeature)));

                var difference = turf.difference(currentFeature, unionFeature);
                //console.log('still overlaps?', overlaps(difference, unionFeature));
                if (difference) {
                    // converte para projeção do mapa, EPSG:3857
                    currentFeature = featureFromTurf(difference);
                    console.log('difference', JSON.stringify(currentFeature));
                }
            }
        } else if (algorithm === 'join-to-clip-jsts') {
            //'join-to-clip'
            var unionFeature = null;
            var counter = 0;


            featureArray.features.every(function (f) {
                f = featureToTurf(f);
                if (counter === 0) {
                    unionFeature = f;
                } else {
                    unionFeature = jstsOperations.union(unionFeature, f);
                }
                counter++;
                return true;
            });

            if (unionFeature !== null) {
                // converte para projeção do turf, EPSG:4326
                currentFeature = featureToTurf(currentFeature);
                unionFeature = jstsOperations.buffer(unionFeature); // is already converted

                var difference = jstsOperations.difference(currentFeature, unionFeature);
                //console.log('still overlaps?', overlaps(difference, unionFeature));

                if (difference) {
                    // converte para projeção do mapa, EPSG:3857
                    currentFeature = featureFromTurf(difference);
                    //currentFeature = difference;
                    console.log('difference', JSON.stringify(currentFeature));
                }
            }
        } else {
            // clip-intersections - fail
            currentFeature = featureToTurf(currentFeature);
            featureArray.features.every(function (f) {
                f = featureToTurf(f);
                var intersect = turf.intersect(currentFeature, f);
                if (intersect) {
                    //mapService.tmpFeatureGroup.addData(extendProp(intersect));
                    var difference = turf.difference(currentFeature, intersect);
                    currentFeature = featureFromTurf(difference);
                }
                return true;
            });
        }
        if (currentFeature === null) {
            console.log('A interseção não retornou nenhuma forma');
            // retorna feature sem nenhuma interseção
            return originalFeature;
        }
        // retorna feature com interseções realizadas
        return currentFeature;
    };


    var getAllLayers = function () {
        var layers = [];
        var mapInstance = mapService.getInstance();
        mapInstance.eachLayer(function (layer) {
            layers.push(layer);
        });
        return layers;
    };
    var getFeaturesCount = function () {
        var layers = getAllFeatureLayers();
        var counter = 0;
        layers.eachLayer(function () {
            counter++;
        });
        return counter;
    };
    var getGeoJson = function () {
        var f = getAllFeatureLayers();
        return f.toGeoJSON();
    };

    var getAllFeatureLayers = function () {
        if (options.useSingleLayerGroup) {
            return mapService.baseFeatureGroup;
        } else {
            var allFgs = mapService.getAllFeatureGroups();
            var fgArr = [];
            allFgs.forEach(function (fg) {
                mapService.getFeatureGroup(fg.id).eachLayer(function (l) {
                    fgArr.push(l);
                });
            });
            return L.featureGroup(fgArr);
        }
    };

    mapService.addFeatureGroup = addFeatureGroup;
    mapService.featureAdd = featureAdd;
    mapService.featureClipIntersections = featureClipIntersections;
    mapService.getLayersFeaturesByFilter = getLayersFeaturesByFilter;
    mapService.getFeatureGroupById = getFeatureGroupById;
    mapService.layerEdit = layerEdit;
    mapService.layerShow = layerShow;
    mapService.layerHide = layerHide;
    mapService.layerSelectZoom = layerSelectZoom;
    mapService.layerRemove = layerRemove;
    mapService.layerUpdate = layerUpdate;
    mapService.layersSelectZoom = layersSelectZoom;
    mapService.getFeaturesCount = getFeaturesCount;
    mapService.getFeatureById = getFeatureById;
    mapService.getAllLayers = getAllLayers;
    mapService.getAllFeatureLayers = getAllFeatureLayers;
    mapService.getLayerById = getLayerById;
    mapService.getFeatureGroup = getFeatureGroup;
    mapService.getGeoJson = getGeoJson;
    mapService.drawFeature = drawFeature;
    mapService.drawFeatureStop = drawFeatureStop;
    mapService.featureToTurf = featureToTurf;
    mapService.featureFromTurf = featureFromTurf;

    return mapService;

});