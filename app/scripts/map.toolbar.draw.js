define([], function () {

   // extensions
    L.Draw.PolygonMeasure = L.Draw.Polygon.extend({
        _fireCreatedEvent: function () {
            var poly = new this.Poly(this._poly.getLatLngs(), this.options.shapeOptions);
            poly.action = 'measure';
            L.Draw.Feature.prototype._fireCreatedEvent.call(this, poly);
        }
    });

    L.Draw.PolylineMeasure = L.Draw.Polyline.extend({
        _fireCreatedEvent: function () {
            var poly = new this.Poly(this._poly.getLatLngs(), this.options.shapeOptions);
            poly.action = 'measure';
            L.Draw.Feature.prototype._fireCreatedEvent.call(this, poly);
        }
    });

    L.DrawToolbar.include({
        startPolygon: function (map, options) {
            new L.Draw.Polygon(map, options)
        },
        _createButton: function (options) {
            var contextOptions = options.context.options;
            var extendClassName = contextOptions.hasOwnProperty('extendClassName') ? ' ' + contextOptions.extendClassName : '';
            var link = L.DomUtil.create('a', options.className + extendClassName || '', options.container);
            link.href = '#';

            if (options.text) {
                link.innerHTML = options.text;
            }

            if (options.title) {
                link.title = options.title;
            }

            L.DomEvent
                .on(link, 'click', L.DomEvent.stopPropagation)
                .on(link, 'mousedown', L.DomEvent.stopPropagation)
                .on(link, 'dblclick', L.DomEvent.stopPropagation)
                .on(link, 'click', L.DomEvent.preventDefault)
                .on(link, 'click', options.callback, options.context);

            return link;
        },
        getModeHandlers: function (map) {
            return [
                {
                    enabled: this.options.marker,
                    handler: new L.Draw.Marker(map, this.options.marker),
                    title: L.drawLocal.draw.toolbar.buttons.marker
                },
                {
                    enabled: this.options.polyline,
                    handler: new L.Draw.Polyline(map, this.options.polyline),
                    title: L.drawLocal.draw.toolbar.buttons.polyline
                },
                {
                    enabled: this.options.polygon,
                    handler: new L.Draw.Polygon(map, this.options.polygon),
                    title: L.drawLocal.draw.toolbar.buttons.polygon
                },
                {
                    enabled: this.options.polyline,
                    handler: new L.Draw.PolylineMeasure(map, this.options.polylineMeasure),
                    title: L.drawLocal.draw.toolbar.buttons.polylineMeasure
                },
                {
                    enabled: this.options.polygon,
                    handler: new L.Draw.PolygonMeasure(map, this.options.polygonMeasure),
                    title: L.drawLocal.draw.toolbar.buttons.polygonMeasure
                },
                {
                    enabled: this.options.rectangle,
                    handler: new L.Draw.Rectangle(map, this.options.rectangle),
                    title: L.drawLocal.draw.toolbar.buttons.rectangle
                },
                {
                    enabled: false,//this.options.circle,
                    handler: new L.Draw.Circle(map, this.options.circle),
                    title: L.drawLocal.draw.toolbar.buttons.circle
                }
            ];
        }
    });

    L.drawLocal = {
        draw: {
            toolbar: {
                // ex: actions.undo  or actions.cancel
                actions: {
                    title: 'Cancelar desenho',
                    text: 'Cancelar'
                },
                finish: {
                    title: 'Concluir desenho',
                    text: 'Concluir'
                },
                undo: {
                    title: 'Remover último ponto desenhado',
                    text: 'Remover último ponto'
                },
                buttons: {
                    polyline: 'Inserir uma linha',
                    polylineMeasure: 'Medir uma linha',
                    polygon: 'Inserir uma área',
                    polygonMeasure: 'Medir uma área',
                    rectangle: 'Inserir um retângulo',
                    circle: 'Inserir um círculo',
                    marker: 'Inserir um marcador'
                }
            },
            handlers: {
                circle: {
                    tooltip: {
                        start: 'Clique a arraste para desenhar o círculo.'
                    },
                    radius: 'Raio'
                },
                marker: {
                    tooltip: {
                        start: 'Clique em algum lugar do mapa para inserir o marcador.'
                    }
                },
                polygon: {
                    tooltip: {
                        start: '', //'Clique para começar a desenhar a forma.',
                        cont: '', //'Clique para continuar a desenhar.',
                        end: '' //'Clique no primeiro ponto para fechar esta forma.'
                    }
                },
                polygonMeasure: {
                    tooltip: {
                        start: '', //'Clique para começar a desenhar a forma.',
                        cont: '', //'Clique para continuar a desenhar.',
                        end: '' //'Clique no primeiro ponto para fechar esta forma.'
                    }
                },
                polyline: {
                    error: '<strong>Erro:</strong> as linhas não podem se sobrepor!',
                    tooltip: {
                        start: '', //'Clique para começar a desenhar a linha.',
                        cont: '',// 'Clique para continuar a desenhar a linha.',
                        end: ''//'Clique novamente no último ponto para concluir a linha.'
                    }
                },
                polylineMeasure: {
                    error: '<strong>Erro:</strong> as linhas não podem se sobrepor!',
                    tooltip: {
                        start: '', //'Clique para começar a desenhar a linha.',
                        cont: '',// 'Clique para continuar a desenhar a linha.',
                        end: ''//'Clique novamente no último ponto para concluir a linha.'
                    }
                },
                rectangle: {
                    tooltip: {
                        start: 'Clique e arraste para desenhar um retângulo.'
                    }
                },
                simpleshape: {
                    tooltip: {
                        end: 'Solte o clique para finalizar o desenho.'
                    }
                }
            }
        },
        edit: {
            toolbar: {
                actions: {
                    save: {
                        title: 'Salvar alterações.',
                        text: 'Salvar'
                    },
                    cancel: {
                        title: 'Cancelar edição, descartar todas as alterações.',
                        text: 'Cancelar'
                    }
                },
                buttons: {
                    edit: 'Editar elements.',
                    editDisabled: 'Sem elementos para edição.',
                    remove: 'Deletar elementos.',
                    removeDisabled: 'Sem elementos para remover.'
                }
            },
            handlers: {
                edit: {
                    tooltip: {
                        text: 'Drag handles, or marker to edit feature.',
                        subtext: 'Clique cancelar para desfazer as alterações.'
                    }
                },
                remove: {
                    tooltip: {
                        text: 'Clique em um elemento para removê-lo.'
                    }
                }
            }
        }
    };

    var drawControlDefaults = {
        position: 'topright',
        rectangle: false,
        polygon: {
            allowIntersection: false, // Restricts shapes to simple polygons
            guidelineDistance: 8,
            drawError: {
                color: '#e1e100', // Color the shape will turn when intersects
                message: 'Termine de desenhar o polígono ou pressione ESC para cancelar' // Message that will show when intersect
            },
            repeatMode: false
        },
        polygonMeasure: {
            allowIntersection: false, // Restricts shapes to simple polygons
            guidelineDistance: 8,
            drawError: {
                color: '#e1e100', // Color the shape will turn when intersects
                message: 'Termine de desenhar o polígono ou pressione ESC para cancelar' // Message that will show when intersect
            },
            repeatMode: false
        },
        polyline: {
            guidelineDistance: 8,
            metric: true
        },
        polylineMeasure: {
            guidelineDistance: 8,
            metric: true
        }
    };

    var drawOptions = {};
    var drawControl = null;
    var _mapService = null;
    var _isDrawing = false;
    var _drawActions = null;
    var _toolbarElement = null;

   // initialize leaflet.draw
    var init = function (mapService, featureGroup) {
        _mapService = mapService;
        var mapInstance = _mapService.getInstance();

        drawControl = new L.Control.Draw();
        drawOptions = $.extend(true, drawControlDefaults, drawOptions);
        drawControl.setDrawingOptions($.extend(true, drawOptions,
            {
                polygon: {
                    shapeOptions: mapUtils.defaultStyles.POLYGON
                },
                polyline: {
                    shapeOptions: mapUtils.defaultStyles.POLYGON,
                    feet: false
                },
                edit: {
                    featureGroup: featureGroup
                },
                marker: {
                    icon: L.divIcon(mapUtils.defaultStyles.POINT)
                },
                polygonMeasure: {
                    extendClassName: 'polygon-measure',
                    shapeOptions: $.extend({}, mapUtils.defaultStyles.POLYGON, {
                        fillColor: '#0288D1',
                        color: '#0288D1',
                        className: 'path-measure'
                    })
                },
                polylineMeasure: {
                    extendClassName: 'polyline-measure',
                    shapeOptions: $.extend({}, mapUtils.defaultStyles.POLYLINE, {
                        fillColor: '#0288D1',
                        color: '#0288D1',
                        className: 'path-measure'
                    }),
                    feet: false
                }
            }
        ));

        _drawActions = new L.EditToolbar.Edit(mapInstance, {
            featureGroup: featureGroup,
            selectedPathOptions: drawControl.options.edit.selectedPathOptions
        });

        _toolbarElement = $(drawControl.getContainer());

        setupEvents(mapInstance);

        return drawControl;
    };

    var formatDistance = function (a) {
        // m
        var m = a;
        if (m > 1000) {
            return (m / 1000).toFixed(2) + ' km'
        } else {
            return m.toFixed(2) + ' m'
        }
    };
    var formatArea = function (a) {
        var m2 = a;
        if (m2 > 10000) {
            return (m2 / 10000).toFixed(2) + ' ha'
        } else {
            return (m2).toFixed(2) + ' km';
        }
    };

    var drawStop = function (e) {
        _isDrawing = false;
    };

    var drawVertex = function (e) {
        _isDrawing = true;
    };


    var setupEvents = function (mapInstance) {
        var measureFeatureGroup = new L.featureGroup();
        measureFeatureGroup.addTo(mapInstance);
        //mapInstance.on('editable:drawing:commit', _mapService.drawFeature);

        mapInstance.on('draw:editmove', function () {});
        mapInstance.on('draw:drawvertex ', drawVertex);
        mapInstance.on('draw:drawstart', function () {});
        mapInstance.on('draw:drawstop', drawStop);
        mapInstance.on('draw:edited', _mapService.drawEdited);
        mapInstance.on('draw:created', function (e) {
            if (e.layer && e.layer.hasOwnProperty('action') && e.layer.action === 'measure') {
                // measurement
                debugger;
                var newLayer = e.layer;
                var options = _mapService.getOptions();

                measureFeatureGroup.clearLayers();

                if (options.showMeasurements === true) {
                    var measurementOptions = $.extend({}, {
                        showDistances: true,
                        formatDistance: formatDistance,
                        formatArea: formatArea
                    }, options.measurementOptions);
                    L.Util.setOptions(newLayer, {showMeasurements: true, measurementOptions: measurementOptions});
                }

                newLayer.addTo(measureFeatureGroup);
                newLayer.showMeasurements();

                newLayer.on('click', function (e) {
                    var layer = e.target;
                    layer.remove();
                });
            } else {
                _mapService.drawFeature(e);
                _isDrawing = false;
            }
        });

        mapInstance.on('draw:editstop', function (e) {
            _mapService.drawFeatureStop(e);
            _isDrawing = false;
        });


        $(window).on('keydown', function (e) {
            e.stopPropagation();
        });

        L.DomEvent.on(window, 'keydown', function (e) {
            if (e.srcElement.nodeName !== 'INPUT' && e.srcElement.nodeName !== 'TEXTAREA') {
                L.DomEvent.stopPropagation(e);
                _mapService.getInstance().keyboard._onKeyDown(e);
            }
        });

        L.DomEvent.on(mapInstance.getContainer(), 'mousemove', function (e) {
            if (_isDrawing) {
                L.DomEvent.stopPropagation(e);
                var el = $(this);
                var offset = el.offset();
                // Then refer to
                var x = e.pageX - offset.left;
                var y = e.pageY - offset.top;
                var width = el.width();
                var height = el.height();

                // todo, fazer diagonais
                //if (y < 35 && x > 35 && x < (width - 35)) {
                if (y < 35) {
                    mapInstance.panBy([0, -80]); // top
                } else if (x < 35) {
                    mapInstance.panBy([-80, 0]); // left
                } else if (y > (height - 35)) {
                    mapInstance.panBy([0, 80]); // bottom
                } else if (x > (width - 35)) {
                    mapInstance.panBy([80, 0]); // right
                }
            }
        });
    };

    var getOptions = function () {
        return drawOptions;
    };

    var setOptions = function (options) {
        drawControl.setDrawingOptions($.extend(true, drawControlDefaults, options));
    };
    var isDrawing = function () {
        return _isDrawing;
    };

    var show = function () {
        _toolbarElement.show();
    };

    var hide = function () {
        _toolbarElement.hide();
    };
    var getElement = function () {
        return _toolbarElement;
    };
    var deselectAll = function () {
        _toolbarElement.find('button').removeClass('selected');
    };
    var getDrawActions = function () {
        return _drawActions;
    };
    var getDrawControl = function () {
        return drawControl;
    };
    var toggleSelected = function (element) {
       deselectAll();
        $(element).addClass('selected');
    };
    var cancelSelection = function () {
        var x = $('.leaflet-draw-actions').find('a').last()[0];
        if (x) {
            x.click();
        }
    };
    var setMode = function (toolbarMode) {
        _toolbarElement.find('a').hide();
        if (toolbarMode === mapUtils.toolbarMode.DRAW) {
            _toolbarElement.find('.leaflet-draw-draw-polyline, .leaflet-draw-draw-polygon, .leaflet-draw-draw-marker').show();
        } else if (toolbarMode === mapUtils.toolbarMode.VIEW) {
            _toolbarElement.find('.polyline-measure, .polygon-measure').show();
        } else if (toolbarMode === mapUtils.toolbarMode.EDIT) {
            _toolbarElement.find('.leaflet-draw-draw-polygon, .polyline-measure, .polygon-measure').show();
        }
    };

    return {
        init: init,
        draw: {
            getElement: getElement,
            getOptions: getOptions,
            setOptions: setOptions,
            isDrawing: isDrawing,
            show: show,
            hide: hide,
            getDrawActions: getDrawActions,
            getDrawControl: getDrawControl,
            toggleSelected: toggleSelected,
            cancelSelection: cancelSelection,
            setMode: setMode
        }
    }
});