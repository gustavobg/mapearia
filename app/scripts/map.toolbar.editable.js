define(['knockout', 'text!../templates/editable-info.html', 'leaflet.editable', 'leaflet.measure', 'leaflet.toolbar'], function (ko, templateEditableInfo) {


    var drawControlDefaults = {
    };

    var drawOptions = {};
    var drawControl = null;
    var _mapService = null;
    var _isDrawing = false;
    var _drawActions = null;
    var _toolbarElement = null;

   // initialize leaflet.draw
    var init = function (mapService, options) {
        _mapService = mapService;
        var mapInstance = _mapService.getInstance();
        var info = options.templateId ? L.DomUtil.get(options.templateId) : $('<div class="info-editable"></div>').appendTo('body').get(0);

        var drawMarker = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-marker',
                    tooltip: 'Inserir marcador'
                }
            },
            addHooks: function () {
                mapInstance.editTools.startMarker(null, mapUtils.defaultStyles.POINT)
            }
        });

        var drawLine = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-polyline',
                    tooltip: 'Desenhar linha'
                }
            },
            addHooks: function () {
                mapInstance.editTools.startPolyline(null, mapUtils.defaultStyles.POLYLINE);
            }
        });

        var drawRectangle = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-rectangle',
                    tooltip: 'Desenhar retângulo'
                }
            },
            addHooks: function () {
                mapInstance.editTools.startRectangle(null, mapUtils.defaultStyles.POLYGON);
            }
        });

        var drawPolygon = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-polygon',
                    tooltip: 'Desenhar área'
                }
            },
            addHooks: function () {
                mapInstance.editTools.startPolygon(null, mapUtils.defaultStyles.POLYGON);
            }
        });

        var drawCircle = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-circle',
                    tooltip: 'Desenhar círculo'
                }
            },
            addHooks: function () {
                mapInstance.editTools.startCircle();
            }
        });

        var measurementOptions = {
            showDistances: true,
            formatDistance: mapUtils.formatDistance,
            formatArea: mapUtils.formatArea
        };

        var measureArea = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-bar-segment leaflet-editable-measure polygon',
                    tooltip: 'Medir área'
                }
            },
            addHooks: function () {
                var options = Object.assign({}, mapUtils.defaultStyles.POLYGON);
                options.measure = true;
                options.color = '#0288D1';
                options.showMeasurements = true;
                options.measurementOptions = measurementOptions;
                mapInstance.editTools.startPolygon(null, options);
                this._link.classList.add('active');
            }
        });

        var measurePolyline = L.ToolbarAction.extend({
            options: {
                toolbarIcon: {
                    html: '',
                    className: 'leaflet-editable-measure polyline',
                    tooltip: 'Medir linha'
                }
            },
            addHooks: function () {
                var options = Object.assign({}, mapUtils.defaultStyles.POLYLINE);
                options.measure = true;
                options.color = '#0288D1';
                options.showMeasurements = true;
                options.measurementOptions = measurementOptions;
                mapInstance.editTools.startPolyline(null, options);
                this._link.classList.add('active');
            }
        });

        var drawControl = new L.Toolbar.Control({
            actions: [ drawMarker, drawLine, drawPolygon, drawCircle, drawRectangle, measureArea, measurePolyline ],
            className: 'leaflet-editable-toolbar leaflet-bar',
            position: 'topleft'
        }).addTo(mapInstance);

        var vmInfo = {
            type: ko.observable(),
            area_m2: ko.observable(),
            area_ha: ko.observable(),
            area_alq: ko.observable(),
            perimeter_m: ko.observable(),
            perimeter_km: ko.observable(),
            latitude: ko.observable(),
            longitude: ko.observable()
        };

        var isBound = function(id) {
            var element = typeof (id)  === 'string' ? document.getElementById(id) : id;
            return !!ko.dataFor(element);
        };

        function addInfo (e) {
            //L.DomEvent.on(document, 'mousemove', moveInfo);
            if (!isBound(info)) {
                info.innerHTML = templateEditableInfo;
                ko.applyBindings(vmInfo, info);
            }
        }

        function removeInfo (e) {
            info.style.display = 'none';
            L.DomEvent.off(document, 'mousemove', moveInfo);
        }

        function moveInfo (e) {
            info.style.left = e.clientX + 20 + 'px';
            info.style.top = e.clientY - 10 + 'px';
        }

        function updateInfo (e) {
            var layer = e.layer;

            var v = mapUtils.getLayerAreaPosition(layer);

            vmInfo.type(v.type);
            vmInfo.area_m2(Number(v.area_m2));
            vmInfo.area_ha(Number(v.area_ha));
            vmInfo.area_alq(Number(v.area_alq));
            vmInfo.perimeter_m(Number(v.perimeter_m));
            vmInfo.perimeter_km(Number(v.perimeter_km));
            vmInfo.latitude(v.latitude);
            vmInfo.longitude(v.longitude);

            if (layer.getType() !== mapUtils.type.POINT) {
                var segments = e.layer.editor.feature._parts;
                if (segments && segments.length > 0 && segments[0].length >= 2) {
                    info.style.display = 'block';
                }
            } else {
                info.style.display = 'block';
            }
        }

        _toolbarElement = $(drawControl.getContainer());

        _toolbarElement.find('a').on('click', function (e) {
            e.preventDefault();
        });

        //mapInstance.on('editable:drawing:start', addInfo);
        mapInstance.on('editable:drawing:end', removeInfo);
        mapInstance.on('editable:vertex:new', updateInfo);
        mapInstance.on('editable:vertex:dragend', updateInfo);
        mapInstance.on('editable:enable', addInfo);
        mapInstance.on('editable:disable', removeInfo);
        mapInstance.on('editable:editing', function (e) {
            var layer = e.layer;
            // measure
            if (layer.options && layer.options.hasOwnProperty('measure') && layer.options.measure) {
                layer.updateMeasurements();
            }

        });
        mapInstance.on('editable:shape:new', function (e) {
            var layer = e.layer;
            // measure
            if (layer.options && layer.options.hasOwnProperty('measure') && layer.options.measure) {
                measureFeatureGroup.clearLayers();
                layer.addTo(measureFeatureGroup);
                layer.showMeasurements();
            }
        });

        setupEvents(mapInstance);

        return drawControl;
    };
    
    var measureFeatureGroup;
    var setupEvents = function (mapInstance) {
        measureFeatureGroup = new L.featureGroup({ editOptions: { skipMiddleMarkers: true } });
        measureFeatureGroup.addTo(mapInstance);

        mapInstance.on('editable:drawing:commit', function (e) {
            if (e.layer.options && e.layer.options.hasOwnProperty('measure') && e.layer.options.measure) {
                // measurement
                var newLayer = e.layer;
                var options = _mapService.getOptions();

                measureFeatureGroup.clearLayers();

                $('.leaflet-editable-toolbar .leaflet-toolbar-icon').removeClass('active');

                if (options.showMeasurements === true) {
                    var measurementOptions = $.extend({}, {
                        showDistances: true,
                        formatDistance: mapUtils.formatDistance,
                        formatArea: mapUtils.formatArea
                    }, options.measurementOptions);
                    L.Util.setOptions(newLayer, {showMeasurements: true, measurementOptions: measurementOptions});
                }

                newLayer.addTo(measureFeatureGroup);
                newLayer.showMeasurements();
                newLayer.disableEdit();

                newLayer.on('click', function (e) {
                    var layer = e.target;
                    layer.remove();
                });
            } else {
                measureFeatureGroup.clearLayers();
                _mapService.drawFeature(e);
                e.layer.remove();
                _isDrawing = false;
            }
        });

        mapInstance.on('draw:editstop', function (e) {
            _mapService.drawFeatureStop(e);
            _isDrawing = false;
        });


        var Z = 90, latlng, redoBuffer = [];

        L.DomEvent.on(window, 'keyup', function (e) {
            if (e.keyCode === 27) {
                cancelEdit();
            }
        });

        var cancelEdit = function () {
            var editTools = _mapService.getInstance().editTools;
            if (!editTools._drawingEditor) return;
            var feature = editTools._drawingEditor.feature;
            feature.disableEdit();
            feature.remove();
            $('.leaflet-editable-toolbar .leaflet-toolbar-icon').removeClass('active');
        };

        L.DomEvent.on(window, 'keydown', function (e) {

            // eventos de teclado do editor
            // remove segmentos CTRL-Z, ou DELETE
            console.log(e.keyCode);
            if (e.keyCode === 90 || e.keyCode === 46) { // Z ou Delete
                e.stopPropagation();
                var editTools = _mapService.getInstance().editTools;
                if (!editTools._drawingEditor) return;
                if (e.shiftKey) { // todo: delete
                    if (redoBuffer.length) editTools._drawingEditor.push(redoBuffer.pop());
                } else {
                    latlng = editTools._drawingEditor.pop();
                    if (latlng) redoBuffer.push(latlng);
                }
            } else if (e.keyCode === 27) {
                e.stopPropagation();
                measureFeatureGroup.clearLayers();
                cancelEdit();
            } else {
                if (e.srcElement.nodeName !== 'INPUT' && e.srcElement.nodeName !== 'TEXTAREA') {
                    L.DomEvent.stopPropagation(e);
                    _mapService.getInstance().keyboard._onKeyDown(e);
                }
            }
        });

        L.DomEvent.on(mapInstance.getContainer(), 'mousemove', function (e) {
            if (mapInstance.editTools.drawing()) {
                L.DomEvent.stopPropagation(e);
                var el = $(this);
                var offset = el.offset();
                // Then refer to
                var x = e.pageX - offset.left;
                var y = e.pageY - offset.top;
                var width = el.width();
                var height = el.height();

                // todo, fazer diagonais
                //if (y < 35 && x > 35 && x < (width - 35)) {
                if (y < 35) {
                    mapInstance.panBy([0, -80]); // top
                } else if (x < 35) {
                    mapInstance.panBy([-80, 0]); // left
                } else if (y > (height - 35)) {
                    mapInstance.panBy([0, 80]); // bottom
                } else if (x > (width - 35)) {
                    mapInstance.panBy([80, 0]); // right
                }
            }
        });
    };

    var getOptions = function () {
        return drawOptions;
    };

    var setOptions = function (options) {
        drawControl.setDrawingOptions($.extend(true, drawControlDefaults, options));
    };
    var isDrawing = function () {
        return _isDrawing;
    };

    var show = function () {
        _toolbarElement.show();
    };

    var hide = function () {
        _toolbarElement.hide();
    };
    var getElement = function () {
        return _toolbarElement;
    };
    var deselectAll = function () {
        _toolbarElement.find('button').removeClass('selected');
    };
    var getDrawActions = function () {
        return _drawActions;
    };
    var getDrawControl = function () {
        return drawControl;
    };
    var toggleSelected = function (element) {
       deselectAll();
        $(element).addClass('selected');
    };
    var cancelSelection = function () {
        var x = $('.leaflet-draw-actions').find('a').last()[0];
        if (x) {
            x.click();
        }
    };
    var setMode = function (toolbarMode) {
        _toolbarElement.find('a').hide();
        if (toolbarMode === mapUtils.toolbarMode.DRAW) {
            _toolbarElement.find('.leaflet-editable-polyline, .leaflet-editable-polygon, .leaflet-editable-marker, .leaflet-editable-measure').show();
        } else if (toolbarMode === mapUtils.toolbarMode.VIEW) {
            _toolbarElement.find('.leaflet-editable-polyline-measure, .leaflet-editable-measure').show();
        } else if (toolbarMode === mapUtils.toolbarMode.EDIT) {
            _toolbarElement.find('.leaflet-editable-polyline-measure, .leaflet-editable-polygon-measure, .leaflet-editable-polygon, .leaflet-editable-measure').show();
        }
    };

    return {
        init: init,
        editable: {
            getElement: getElement,
            getOptions: getOptions,
            setOptions: setOptions,
            isDrawing: isDrawing,
            show: show,
            hide: hide,
            getDrawActions: getDrawActions,
            getDrawControl: getDrawControl,
            toggleSelected: toggleSelected,
            cancelSelection: cancelSelection,
            setMode: setMode
        }
    }
});