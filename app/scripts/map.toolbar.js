define(['jquery', 'map.toolbar/editable', 'geosearch', 'leaflet.zoomslider.custom', 'map.utils'], function ($, toolbarEditable, GeoSearch) {

    var _mapService = null;

    var toolbar = {
        toolbarInstance: null,
        getInstance: function () {
            if (typeof(toolbarInstance) === 'object') {
                return instance;
            } else {
                throw 'Toolbar is not initialized, call toolbarService.init prior this method';
            }
        },
        drawingMode: {
            POLYGON: 'Polygon',
            LINESTRING: 'LineString',
            POINT: 'Point'
        }
    };

    var defaultOptions = {
        useDraw: false,
        useEditable: true
    };

    // define barra de ferramentas do mapa e suas ações
    toolbar.init = function (mapService, featureGroup, options) {
        $.extend(true, options, defaultOptions);

        _mapService = mapService;

        var mapInstance = mapService.getInstance();

        toolbarEditable.init(mapService, featureGroup);
        toolbar = $.extend(true, toolbar, toolbarEditable.editable );

        //var zoomControl = L.control.zoom({position: 'bottomright'});
        var printControl = L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (map) {
                var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
                container.title = 'Imprimir';
                L.DomEvent
                    .addListener(container, 'click', L.DomEvent.stopPropagation)
                    .addListener(container, 'click', L.DomEvent.preventDefault)
                    .addListener(container, 'click', function () {
                        window.print();
                    });

                var button = L.DomUtil.create('a', 'leaflet-print', container);
                button.innerHTML = '<span class="mapearia-printer"></span>';

                return container;
            }
        });

        var focusControl = L.Control.extend({
            options: {
                position: 'bottomright'
            },
            onAdd: function (map) {
                var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
                container.title = 'Centralizar';
                L.DomEvent
                    .addListener(container, 'click', L.DomEvent.stopPropagation)
                    .addListener(container, 'click', L.DomEvent.preventDefault)
                    .addListener(container, 'click', function () {
                        _mapService.layersSelectZoom();
                    });

                var button = L.DomUtil.create('a', 'leaflet-focus', container);
                button.innerHTML = '<i class="material-icons" style="font-size: 1.4em">my_location</i>';

                return container;
            }
        });

        var geoSearchControl = new GeoSearch.GeoSearchControl({
            provider: new GeoSearch.GoogleProvider({ params: { key: 'AIzaSyCnynEiBhiItwzvp2Ly-p7mL7up2mHl8nA' } }),
            style: 'button',
            showMarker: false,
            searchLabel: 'Digite o endereço, cep ou coordenada',
            notFoundMessage: 'Desculpe, não encontramos o endereço',
            position: 'topleft',
            autoClose: true
        });

        var scale = L.control.scale();
        scale.addTo(mapInstance);

        mapInstance.addControl(new printControl());
        mapInstance.addControl(geoSearchControl);
        mapInstance.addControl(new L.Control.ZoomSliderCustom({position: 'bottomright'}));
        mapInstance.addControl(new focusControl());
    };

    return toolbar;
});
