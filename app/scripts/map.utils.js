define(['jquery-inputmask'], function () {
    var mapUtils = {};

    mapUtils.formatArea = function (m2) {
        var result = 0;
        m2 = Number(m2);
        if (m2 >= 1000000000) {
            result = (m2 / 1000000).toFixed(2); // + ' km²'
            return Inputmask.format(result,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' km²' });
        } else if (m2 >= 10000) {
            result = (m2 / 10000).toFixed(2); // + ' ha'
            return Inputmask.format(result,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' ha' });
        } else {
            result = (m2).toFixed(2); // + ' km';
            return Inputmask.format(result,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' m²' });
        }
    };

    mapUtils.formatDistance = function (m) {
        var result = 0;
        m = Number(m);
        if (m >= 1000) {
            result = (m / 1000).toFixed(2); // + ' km'
            return Inputmask.format(result,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' km' });
        } else {
            result = m.toFixed(2); // + ' m'
            return Inputmask.format(result,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' m' });
        }
    };

    mapUtils.getLayerAreaPosition = function (layer) {
        var area_m2 = 0;
        var area_ha = 0;
        var area_alq = 0;
        var perimeter_m = 0;
        var perimeter_km = 0;
        var latitude = 0;
        var longitude = 0;
        var feature = layer.toGeoJSON();
        var geometryType = feature.geometry.type;

        if (geometryType === 'Point') {
            var latLng = feature.geometry.coordinates;
            longitude = latLng[0];
            latitude = latLng[1];

            // formata para ter no máximo 8 casas decimais
            var _latitude = latitude.toString().match(/[-,+]?\d{0,4}\.\d{0,8}/gi);
            var _longitude = longitude.toString().match(/[-,+]?\d{0,4}\.\d{0,8}/gi);
            latitude = _latitude.length > 0 ? _latitude[0] : 0;
            longitude = _longitude.length > 0 ? _longitude[0] : 0;

        } else if (geometryType === 'Polyline') {
            perimeter_m = layer.getPerimeter().toFixed(2);
            perimeter_km = (perimeter_m / 1000).toFixed(2);
        } else {
            // Polygon, Circle or Rectangle
            var m2 = turf.area(feature);
            //var m2 = layer.getArea();
            area_m2 = m2.toFixed(3);
            area_ha = (m2 / 10000).toFixed(2);
            area_alq = (m2 / 24200).toFixed(2); // paulista
            //area_alqueirao = (m2 / 48400).toFixed(2); // mineiro
            perimeter_m = layer.getPerimeter().toFixed(2);
            perimeter_km = (perimeter_m / 1000).toFixed(2);
        }

        return {
            type: geometryType,
            area_m2: area_m2,
            area_ha: area_ha,
            area_alq: area_alq,
            perimeter_m: perimeter_m,
            perimeter_km: perimeter_km,
            latitude: latitude,
            longitude: longitude,
            getAreaMasked: function () {
                return mapUtils.formatArea(this.area_m2);
            },
            getAreaAlqMasked: function () {
                return Inputmask.format(this.area_alq,  { alias: 'decimal', autoGroup: true, groupSeparator: '.', groupSize: 3, radixPoint: ',', suffix: ' alq (sp)' });
            },
            getPerimeterMasked: function () {
                return mapUtils.formatDistance(this.perimeter_m);
            }
        }
    };

    mapUtils.type = {
        POLYGON: 'Polygon',
        MULTIPOLYGON: 'MultiPolygon',
        POLYLINE: 'LineString',
        RECTANGLE: 'Rectangle',
        CIRCLE: 'Circle',
        POINT: 'Point'
    };

    mapUtils.defaultStyles = {};
    mapUtils.defaultStyles = {
        POLYGON: {
            color: '#F9A825',
            weight: 2.2,
            fillOpacity: 0.3,
            opacity: 1
        },
        MULTIPOLYGON: mapUtils.defaultStyles.POLYGON,
        POLYLINE: {
            color: '#F9A825',
            weight: 2.2,
            fillOpacity: 0.3,
            opacity: 1
        },
        RECTANGLE: mapUtils.defaultStyles.POLYGON,
        CIRCLE: mapUtils.defaultStyles.POLYGON,
        POINT: {
            color: '#F9A825',
            shadowUrl :'marker-shadow.png',
            name: '',
            iconSize: [25,41],
            iconAnchor: [12,41],
            popupAnchor: [1,-34],
            tooltipAnchor: [16,-28],
            shadowSize: [41,41],
            className: 'leaflet-custom-icon'
        }
    };

    mapUtils.toolbarMode = {
        VIEW: 'view',
        DRAW: 'draw',
        EDIT: 'edit'
    };

    mapUtils.getIconOptions = function (iconOptions) {
        var iconOptions = $.extend({}, mapUtils.defaultStyles.POINT, iconOptions);
        var htmltext = '';

        if (iconOptions.text) {
            htmltext += '<span class="icon-text">{text}</span>';
        }

        htmltext += '<i class="material-icons icon" style="background-color: {color}">{name}</i>';

        var html =  htmltext +
            '<div class="base-icon-circle" style="background-color: {color}"></div>' +
            '<i class="material-icons base-icon">place</i></div><i class="material-icons base-icon base-icon-shadow">place</i>';

        html = L.Util.template(html, iconOptions);

        return $.extend(true, iconOptions, { html: html });
    };

    window.mapUtils = mapUtils;

    return mapUtils;
});


