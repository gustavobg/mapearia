var b = '../../bower_components/';
var n = '../../node_modules/';
var libs = '../libs/';
require.config({
    baseUrl: 'scripts',
    shim: {
        'bootstrap/modal': { deps: ['jquery', 'bootstrap/transition'] },
        'bootstrap/transition': { deps: ['jquery'] },
        //'geosearch/google': { deps: ['leaflet', 'geosearch'] },
        //'geosearch/osm': { deps: ['leaflet', 'geosearch'] },
        'leaflet.drag': { deps: ['leaflet'] },
        'leaflet.draw': { deps: ['leaflet', 'leaflet.drag'] },
        'leaflet.editable': { deps: ['leaflet', 'leaflet.drag'] },
        //'leaflet.measure': { deps: ['leaflet.measure/measures'] },
        //'leaflet.measure/measures': { deps: ['leaflet', 'leaflet.editable'] },
        'leaflet.measure': { deps: ['leaflet'] },
        'geosearch': { deps: ['leaflet'] },
        'leaflet.toolbar': { deps: ['leaflet'] },
        'tiles.google': { deps: ['leaflet'] },
        'leaflet.labeltext': { deps: ['leaflet'] },
        'leaflet.layer.collision': { deps: ['leaflet'] }
    },
    paths: {
        // libs
        //'rbush': libs + 'rbush',
        'bootstrap/alert': b + 'bootstrap/js/alert',
        'bootstrap/button': b + 'bootstrap/js/button',
        'bootstrap/dropdown': b + 'bootstrap/js/dropdown',
        'bootstrap/transition': b + 'bootstrap/js/transition',
        'bootstrap/modal': b + 'bootstrap/js/modal',
        'knockout.mapping': libs + 'knockout.mapping',
        'knockout': b + 'knockout/dist/knockout',
        'moment': b + 'moment/moment',
        'jquery': b + 'jquery/dist/jquery.min',
        'toastr': b + 'toastr/toastr.min',
        'colorpicker': b + 'mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker',
        'colorpicker.plus': b + 'bootstrap-colorpicker-plus/dist/js/bootstrap-colorpicker-plus',
        'text': b + 'text/text',
        'bootbox': b + 'bootbox.js/bootbox',
        'chance': b + 'chance/chance',        
        'jquery-inputmask': b + 'jquery.inputmask/dist/min/jquery.inputmask.bundle.min',
        'geosearch': libs + 'leaflet.geosearch/dist/bundle.min',
        'leaflet.layer.collision': libs + 'leaflet.layer.collision',
        'leaflet': b + 'leaflet/dist/leaflet-src',
        'leaflet.timeline': libs + 'leaflet.timeline',
        //'leaflet.measure/measures': libs + 'leaflet.measure.labelled/leaflet_measure',
        //'leaflet.measure': libs + 'leaflet.measure.labelled/measures',
        'leaflet.measure': b + 'leaflet-measure-path/leaflet-measure-path',
        'leaflet.drag': libs + 'leaflet.drag/Path.Drag',
        'leaflet.editable': b + 'leaflet.editable/src/Leaflet.Editable',
        'leaflet.zoomslider': b + 'Leaflet.zoomslider/src/L.Control.Zoomslider',
        'leaflet.zoomslider.custom': libs + 'leaflet.zoomslider.custom',
        'leaflet.labeltext': b + 'Leaflet.LabelText/dist/L.LabelTextCollision',
        //'leaflet.draw': b + 'leaflet-draw/dist/leaflet.draw-src',
        'leaflet.toolbar': b + 'Leaflet.toolbar/dist/leaflet.toolbar-src',
        'leaflet.tooltip.custom': libs + 'leaflet.tooltip.custom',
        // tiles
        'tiles.google': libs + 'leaflet.google',
        'wicket': libs + 'wicket/wicket',
        'wicket.leaflet': libs + 'wicket/wicket-leaflet',
        // components
        'component.areaPositionInfo': 'component.areaPositionInfo',
        // paths
        'templates': '/templates',
        // scripts
        'turf.overlaps': 'turf.overlaps',
        'leaflet.extensions': 'leaflet.extensions',
        'map.toolbar': 'map.toolbar',
        'map.toolbar/draw': 'map.toolbar.draw',
        'map.toolbar/editable': 'map.toolbar.editable',
        'map.utils': 'map.utils',
        'map.service': 'map.service',
        'map.gridlayer': 'map.gridlayer',
        'map.layerEdit': 'map.layerEdit',
        'main': 'main'
    },
    map: {
        '*': {
            'bootstrap-colorpicker': 'colorpicker'
        }
    }
});
