define(['map.service', 'map.toolbar', 'toastr'], function (mapService, toolbar, toastr) {
    /*
     Projeto Mapearia:

     Foco: Importar GeoJson, editar elementos e visualizar.
     Recursos de Edição:
     - Navegação de camadas e elementos (árvore);
     - Desenhos de polígono, pontos e linhas;
     - Detecção de colisão entre elementos de uma mesma camada, com tomada de decisão.
     - Multiseleção de camadas, com opção de criar multipoligonos, duplicar, remover.
     - Editar estilos de features, aplicar estilos por camada.

     */

    var mapInstance = mapService.init({ checkPolygonIntersections: false });
    var counter = 0;
    mapInstance.on('mapearia.drawFeature', function (feature, a) {
        debugger;
        delete feature.properties;
        delete feature.target;
        feature.type = 'Feature';
        if (counter === 0) {
            vm.a(JSON.stringify(feature));
        } else if (counter === 1) {
            vm.b(JSON.stringify(feature));
        } else {
            counter = 0;
            mapService.featureGroup.clearLayers();
            vm.a(JSON.stringify(feature));
            vm.b('{}');
        }
        counter++;
    });

    var vm = function () {
        this.a = ko.observable('{}');
        this.b = ko.observable('{}');
        this.result = ko.pureComputed({
            read: function () {
                try {
                    var a = JSON.parse(ko.unwrap(this.a));
                    var b = JSON.parse(ko.unwrap(this.b));
                    debugger;
                    if (a.hasOwnProperty('geometry') && b.hasOwnProperty('geometry')) {
                        var r = turf.difference(a, b);
                        r.properties.fillColor = '#000000';
                        r.properties.fillOpacity = 1;
                        mapService.featureAdd(r);
                        mapService.layersSelectZoom();
                        return JSON.stringify(r);
                    } else {
                        return '{}';
                    }
                } catch (err) {
                    this.result('geoJson inválido');
                }
            },
            write: function () {

            }
        }, this);

        window.A = this.a;
        window.B = this.b;
        window.RESULT = this.result;
    };

    vm = new vm();


    toolbar.init(mapInstance, mapService.getFeatureGroup());
    toolbar.setMode(mapUtils.toolbarMode.DRAW);
    debugger;

    ko.applyBindings(vm, document.getElementById('layer-edit-container'));


});