'use strict';

// generated on 2016-09-06 using generator-webapp 2.1.0
var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var browserSync = require('browser-sync');
var del = require('del');
var wiredep = require('wiredep').stream;
var mainBowerFiles = require('main-bower-files');
var base64 = require('gulp-base64');

var $ = gulpLoadPlugins();
var reload = browserSync.reload;

gulp.task('styles', function () {
  return gulp.src('app/styles/*.scss')
      .pipe($.plumber())
      .pipe($.sourcemaps.init())
      .pipe($.sass.sync({
        outputStyle: 'expanded',
        precision: 10,
        includePaths: ['.']
      }).on('error', $.sass.logError))
          .pipe($.autoprefixer({ browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'] }))
          .pipe($.sourcemaps.write())
          .pipe(gulp.dest('dist/styles'))
          .pipe(gulp.dest('.tmp/styles'))
          .pipe(reload({ stream: true }));
});

gulp.task('scripts', function () {
  return gulp.src('app/scripts/**/*.js')
      .pipe($.plumber())
      .pipe($.sourcemaps.init())
      .pipe($.babel())
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest('.tmp/scripts'))
      .pipe(reload({ stream: true }));
});

function lint(files, options) {
  return gulp.src(files).pipe(reload({ stream: true, once: true }))
      .pipe($.eslint(options))
      .pipe($.eslint.format())
      .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', function () {
  return lint('app/scripts/**/*.js', {
    fix: true
  }).pipe(gulp.dest('app/scripts'));
});
gulp.task('lint:test', function () {
  return lint('test/spec/**/*.js', {
    fix: true,
    env: {
      mocha: true
    }
  }).pipe(gulp.dest('test/spec/**/*.js'));
});

gulp.task('html', ['styles', 'scripts'], function () {
  return gulp.src('app/*.html')
      .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
      .pipe($.if('*.js', $.uglify()))
      .pipe($.if('*.css',  $.cssnano({ safe: true, autoprefixer: false })))
      .pipe($.if('*.css',  base64({ debug: true, baseDir: '.tmp', extensions: ['svg', 'png', 'gif', 'jpg'] })))
      .pipe($.if('*.html', $.htmlmin({ collapseWhitespace: true })))
      .pipe(gulp.dest('dist'));
});

gulp.task('images', function () {
  return gulp.src('app/libs/images/**/*').pipe($.cache($.imagemin({
    progressive: true,
    interlaced: true,
    // don't remove IDs from SVGs, they are often used
    // as hooks for embedding and styling
    svgoPlugins: [{ cleanupIDs: false }]
  }))).pipe(gulp.dest('dist/images')).pipe(gulp.dest('.tmp/images'));
});

gulp.task('images:bower', ['wiredep'], function () {
   return gulp.src(mainBowerFiles('**/*.{jpg,png,svg,gif}', function (err) {})).pipe($.cache($.imagemin({
    progressive: true,
    interlaced: true,
    // don't remove IDs from SVGs, they are often used
    // as hooks for embedding and styling
    svgoPlugins: [{ cleanupIDs: false }]
  }))).pipe(gulp.dest('.tmp/images'))
      .pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function () {
  return gulp.src(mainBowerFiles('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
      .concat('app/fonts/**/*'))
      .pipe(gulp.dest('.tmp/fonts'))
      .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', function () {
  return gulp.src(['app/*.*', '!app/*.html'], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('serve', ['styles', 'scripts', 'fonts'], function () {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch(['app/*.html', 'app/images/**/*', '.tmp/fonts/**/*']).on('change', reload);

  gulp.watch('app/styles/**/*.scss', ['styles']);
  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('app/fonts/**/*', ['fonts']);
  gulp.watch('bower.json', ['wiredep', 'fonts']);
  gulp.watch('app/index.html', ['images:bower']);
});

gulp.task('serve:dist', function () {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

gulp.task('serve:test', ['scripts'], function () {
  browserSync({
    notify: false,
    port: 9000,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/bower_components': 'bower_components'
      }
    }
  });

  gulp.watch('app/scripts/**/*.js', ['scripts']);
  gulp.watch('test/spec/**/*.js').on('change', reload);
  gulp.watch('test/spec/**/*.js', ['lint:test']);
  gulp.watch('app/index.html', ['images:bower']);
});

// inject bower components
gulp.task('wiredep', function () {
  // gulp.src('app/styles/*.scss').pipe(wiredep({
  //   ignorePath: /^(\.\.\/)+/
  // })).pipe(gulp.dest('app/styles'));

  gulp.src('app/index.html').pipe(wiredep({
    ignorePath: /^(\.\.\/)*\.\./
  })).pipe(gulp.dest('app'));
});

gulp.task('build', ['lint', 'html', 'images', 'images:bower', 'fonts', 'extras'], function () {
  return gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true }));
});

gulp.task('default', ['clean'], function () {
  gulp.start('build');
});