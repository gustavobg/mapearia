﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.AmbienteProducao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Model.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class AmbienteProducaoController : ControllerExtended
    {
        public AmbienteProducaoController()
        {
            IndexUrl = "/Agricola/AmbienteProducao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoAmbienteProducaoInfo info = ProducaoAmbienteProducaoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoAmbienteProducao, "ProducaoAmbienteProducao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
       
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new ProducaoAmbienteProducaoInfo();

            if (vm.IdProducaoAmbienteProducao.HasValue && vm.IdProducaoAmbienteProducao > 0)
                info = ProducaoAmbienteProducaoBll.Instance.ListarPorCodigo(vm.IdProducaoAmbienteProducao.Value);

            ViewModelToModelMapper.Map<ProducaoAmbienteProducaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            var response = ProducaoAmbienteProducaoBll.Instance.Salvar(info);

            SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoAmbienteProducao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoAmbienteProducao, "ProducaoAmbienteProducao", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoAmbienteProducaoEmpresaInfo info = new ProducaoAmbienteProducaoEmpresaInfo();
            ProducaoAmbienteProducaoEmpresaInfo response = new ProducaoAmbienteProducaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoAmbienteProducaoEmpresaBll.Instance.ListarPorParametros(new ProducaoAmbienteProducaoEmpresaInfo { IdProducaoAmbienteProducao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoAmbienteProducaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoAmbienteProducao, "ProducaoAmbienteProducao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoAmbienteProducaoEmpresaInfo info = new ProducaoAmbienteProducaoEmpresaInfo();
            ProducaoAmbienteProducaoEmpresaInfo response = new ProducaoAmbienteProducaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoAmbienteProducaoEmpresaBll.Instance.ListarPorParametros(new ProducaoAmbienteProducaoEmpresaInfo { IdProducaoAmbienteProducao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoAmbienteProducaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoAmbienteProducao, "ProducaoAmbienteProducao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoAmbienteProducaoInfo info = new ProducaoAmbienteProducaoInfo(IdEmpresa);
            info.IdProducaoAmbienteProducao = vm.IdProducaoAmbienteProducao;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.Sigla = vm.Sigla;
            info.RegistroProprio = vm.RegistroProprio;

            var retorno = new ListPaged<ProducaoAmbienteProducaoInfo>(ProducaoAmbienteProducaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoAmbienteProducao", true);
        }
    }
}