﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Caracterizacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Bll.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class CaracterizacaoController : ControllerExtended
    {
        public CaracterizacaoController()
        {
            IndexUrl = "/Agricola/Caracterizacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoCaracterizacaoInfo info = ProducaoCaracterizacaoBll.Instance.ListarPorParametros(new ProducaoCaracterizacaoInfo { IdProducaoCaracterizacao = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoCaracterizacao, "ProducaoCaracterizacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoCaracterizacaoInfo info = new ProducaoCaracterizacaoInfo();

            ViewModelToModelMapper.Map<ProducaoCaracterizacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoCaracterizacaoBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoCaracterizacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoCaracterizacao, "ProducaoCaracterizacao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoCaracterizacaoEmpresaInfo info = new ProducaoCaracterizacaoEmpresaInfo();
            ProducaoCaracterizacaoEmpresaInfo response = new ProducaoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new ProducaoCaracterizacaoEmpresaInfo { IdProducaoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCaracterizacao, "ProducaoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoCaracterizacaoEmpresaInfo info = new ProducaoCaracterizacaoEmpresaInfo();
            ProducaoCaracterizacaoEmpresaInfo response = new ProducaoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new ProducaoCaracterizacaoEmpresaInfo { IdProducaoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCaracterizacao, "ProducaoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoCaracterizacaoInfo info = new ProducaoCaracterizacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdProducaoCaracterizacaoIn = vm.IdProducaoCaracterizacaoIn;
            info.IdProducaoCaracterizacaoNotIn = vm.IdProducaoCaracterizacaoNotIn;
            info.CaracterizaLocal = vm.CaracterizaLocal;
            info.CaracterizaUnidade = vm.CaracterizaUnidade;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoCaracterizacaoInfo> retorno = new ListPaged<ProducaoCaracterizacaoInfo>(ProducaoCaracterizacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Caracterização de Locais e Unidades ", true);

        }
    }
}
