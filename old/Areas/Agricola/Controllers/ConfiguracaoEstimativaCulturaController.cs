﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaCultura;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class ConfiguracaoEstimativaCulturaController : ControllerExtended
    {
        public ConfiguracaoEstimativaCulturaController()
        {
            IndexUrl = "Agricola/ConfiguracaoEstimativaCultura/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(int parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            var info = ProducaoCulturaBll.Instance.ListarPorCodigo(parentId);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            vm.DescricaoExibicao = info.Descricao;
            vm.ParentId = parentId;
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoConfiguracaoEstimativaCulturaInfo info = new ProducaoConfiguracaoEstimativaCulturaInfo();

            ViewModelToModelMapper.Map<ProducaoConfiguracaoEstimativaCulturaInfo>(vm, info);

            if (!string.IsNullOrEmpty(vm.IdsEspacamento))
            {
                info.Espacamentos = new List<ProducaoConfiguracaoEstimativaEspacamentoInfo>();

                CommaSeparatedToList(vm.IdsEspacamento).ForEach(idProducaoPlantioEspacamento =>
                {
                    ProducaoConfiguracaoEstimativaEspacamentoInfo item = new ProducaoConfiguracaoEstimativaEspacamentoInfo();
                    item.IdProducaoPlantioEspacamento = idProducaoPlantioEspacamento;

                    info.Espacamentos.Add(item);
                });
            }

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = ProducaoConfiguracaoEstimativaCulturaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoConfiguracaoEstimativaCultura.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProducaoConfiguracaoEstimativaCultura, "ProducaoConfiguracaoEstimativaCultura", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int parentId)
        {
            NewEditVM vm = new NewEditVM();
            var info = ProducaoCulturaBll.Instance.ListarPorCodigo(parentId);
            vm.DescricaoExibicao = info.Descricao;
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProducaoConfiguracaoEstimativaCulturaInfo info = new ProducaoConfiguracaoEstimativaCulturaInfo();

                info = ProducaoConfiguracaoEstimativaCulturaBll.Instance.ListarPorIdCompleto(new ProducaoConfiguracaoEstimativaCulturaInfo { IdProducaoConfiguracaoEstimativaCultura = id.Value });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoConfiguracaoEstimativaCultura, "");
            }
            else
            {
                #region Regra Exibição de Estimativa de Produção
                var cultura = ProducaoCulturaBll.Instance.ListarPorCodigo(parentId.Value);
                if (cultura.PermiteVariasColheitas.Value && ((cultura.AvaliacaoProdutividadeTipo == 1) || (cultura.AvaliacaoProdutividadeTipo == 3 && cultura.EstimativaTipo == 1)))
                {
                    vm.ExibeEstimativaProducao = true;
                }
                else if (!cultura.PermiteVariasColheitas.Value && ((cultura.AvaliacaoProdutividadeTipo == 2) || (cultura.AvaliacaoProdutividadeTipo == 3 && cultura.EstimativaTipo == 3)))
                {
                    vm.ExibeEstimativaProducao = false;
                }
                else
                {
                    vm.ExibeEstimativaProducao = false;
                }
                cultura = null;
                #endregion
                vm.Ativo = true;
            }

            //Itens Relacionados com a Cultura.
            var culturaInfo = ProducaoCulturaBll.Instance.ListarPorIdCompleto(parentId.Value, IdEmpresa);
            vm.IdsEspacamentosCultura = culturaInfo.IdsEspacamentos;
            vm.IdsProdutoServico = culturaInfo.IdsProdutos;
            vm.IdsEpocaPlantio = culturaInfo.IdsEpocaPlantio;
            vm.ColheitasPossiveisProducaoCultura = culturaInfo.ColheitasPossiveis;

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoConfiguracaoEstimativaCulturaInfo info = new ProducaoConfiguracaoEstimativaCulturaInfo();
            info.IdProducaoConfiguracaoEstimativaCultura = vm.IdProducaoConfiguracaoEstimativaCultura;
            info.IdProducaoCultura = vm.IdProducaoCultura;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdProducaoEpocaPlantio = vm.IdProducaoEpocaPlantio;
            info.Ativo = vm.Ativo;

            ListPaged<ProducaoConfiguracaoEstimativaCulturaInfo> retorno = new ListPaged<ProducaoConfiguracaoEstimativaCulturaInfo>(ProducaoConfiguracaoEstimativaCulturaBll.Instance.ListarPorParametrosComEspacamentos(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Configuração Estimativa de Produção (Cultura)", true);
        }

    }
}
