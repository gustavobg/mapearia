﻿
using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaVariedade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class ConfiguracaoEstimativaVariedadeController : ControllerExtended
    {
        public ConfiguracaoEstimativaVariedadeController()
        {
            IndexUrl = "Agricola/ConfiguracaoEstimativaCultura/Index";
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoConfiguracaoEstimativaVariedadeInfo info = new ProducaoConfiguracaoEstimativaVariedadeInfo();

            ViewModelToModelMapper.Map<ProducaoConfiguracaoEstimativaVariedadeInfo>(vm, info);

            if (!string.IsNullOrEmpty(vm.IdsProducaoPlantioEspacamento))
            {
                info.Espacamentos = new List<ProducaoConfiguracaoEstimativaVariedadeEspacamentoInfo>();

                CommaSeparatedToList(vm.IdsProducaoPlantioEspacamento).ForEach(idProducaoPlantioEspacamento =>
                {
                    ProducaoConfiguracaoEstimativaVariedadeEspacamentoInfo item = new ProducaoConfiguracaoEstimativaVariedadeEspacamentoInfo();
                    item.IdProducaoPlantioEspacamento = idProducaoPlantioEspacamento;

                    info.Espacamentos.Add(item);
                });
            }

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = ProducaoConfiguracaoEstimativaVariedadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoConfiguracaoEstimativaVariedade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProducaoConfiguracaoEstimativaVariedade, "ProducaoConfiguracaoEstimativaVariedade", response.Response.IdHistorico);

            return Json(response);
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(int parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            var info = ProducaoVariedadeBll.Instance.ListarPorCodigo(parentId);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            vm.DescricaoExibicao = info.Descricao;
            vm.ParentId = parentId;
            return View(vm);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int parentId)
        {
            NewEditVM vm = new NewEditVM();
            var info = ProducaoVariedadeBll.Instance.ListarPorCodigo(parentId); //Apenas para retornar a Descrição da Variedade
            vm.DescricaoExibicao = info.Descricao;
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProducaoConfiguracaoEstimativaVariedadeInfo info = new ProducaoConfiguracaoEstimativaVariedadeInfo();

                info = ProducaoConfiguracaoEstimativaVariedadeBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoConfiguracaoEstimativaVariedade, "ProducaoConfiguracaoEstimativaVariedade");
            }
            else
            {
                #region Regra Exibição de Estimativa de Produção

                var variedadeItem = ProducaoVariedadeBll.Instance.ListarPorCodigo(parentId.Value);

                if (variedadeItem != null)
                {
                    var cultura = ProducaoCulturaBll.Instance.ListarPorCodigo(variedadeItem.IdProducaoCultura.Value);
                    if (cultura.PermiteVariasColheitas.Value && ((cultura.AvaliacaoProdutividadeTipo == 1) || (cultura.AvaliacaoProdutividadeTipo == 3 && cultura.EstimativaTipo == 1)))
                    {
                        vm.ExibeEstimativaProducao = true;
                    }
                    else if (!cultura.PermiteVariasColheitas.Value && ((cultura.AvaliacaoProdutividadeTipo == 2) || (cultura.AvaliacaoProdutividadeTipo == 3 && cultura.EstimativaTipo == 3)))
                    {
                        vm.ExibeEstimativaProducao = false;
                    }
                    else
                    {
                        vm.ExibeEstimativaProducao = false;
                    }
                    cultura = null;
                }

                #endregion
                vm.Ativo = true;
            }

            //Itens Relacionados com a Cultura.
            var variedadeInfo = ProducaoVariedadeBll.Instance.ListarPorIdCompleto(parentId.Value, IdEmpresa);
            vm.IdsProdutoServico = variedadeInfo.IdsProdutoServico;
            vm.IdsProducaoEspacamento = variedadeInfo.IdsProducaoEspacamento;
            vm.IdsProducaoEpocaPlantio = variedadeInfo.IdsProducaoEpocaPlantio;
            vm.VidaEmColheitas = variedadeInfo.VidaEmColheitas;

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoConfiguracaoEstimativaVariedadeInfo info = new ProducaoConfiguracaoEstimativaVariedadeInfo();
            info.IdProducaoConfiguracaoEstimativaVariedade = vm.IdProducaoConfiguracaoEstimativaVariedade;
            info.IdProducaoVariedade = vm.IdProducaoVariedade;
            info.Ativo = vm.Ativo;

            ListPaged<ProducaoConfiguracaoEstimativaVariedadeInfo> retorno = new ListPaged<ProducaoConfiguracaoEstimativaVariedadeInfo>(ProducaoConfiguracaoEstimativaVariedadeBll.Instance.ListarEstimativasComEspacamentos(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Configuração Estimativa de Producao (Variedade)", true);
        }

    }
}
