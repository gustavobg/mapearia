﻿
using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Cultura;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class CulturaController : ControllerExtended
    {
        public CulturaController()
        {
            IndexUrl = "/Agricola/Cultura/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoCulturaInfo info = ProducaoCulturaBll.Instance.ListarPorIdCompleto( id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoCultura, "ProducaoCultura");
            }
            else
            {
                vm.Ativo = true;
            }

            //Busca parametro
            //var caracteristica = new CaracteristicaBLL().AdquirirCaracteristicaComValor(new DTO.Caracteristica.DTOAdquirirCaracteristica { Codigo = "Centro_Custo_Obrigatorio" }, IdEmpresa: IdEmpresa).FirstOrDefault();
            //if (caracteristica != null)
            //    if (!string.IsNullOrEmpty(caracteristica.CaracteristicaValor.Valor))
            //        vm.CentroCustoObrigatorio = Convert.ToBoolean(caracteristica.CaracteristicaValor.Valor);

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoCulturaInfo info = new ProducaoCulturaInfo();

            ViewModelToModelMapper.Map<ProducaoCulturaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            CommaSeparatedToList(vm.IdsObjetivos).ForEach(IdProducaoObjetivo =>
            {
                info.Objetivos.Add(new ProducaoCulturaObjetivoInfo()
                {
                    IdProducaoObjetivo = IdProducaoObjetivo
                });
            });

            CommaSeparatedToList(vm.IdsEspacamentos).ForEach(IdProducaoPlantioEspacamento =>
            {
                info.CulturasEspacamentos.Add(new ProducaoCulturaPlantioEspacamentoInfo()
                {
                    IdProducaoPlantioEspacamento = IdProducaoPlantioEspacamento
                });
            });

            CommaSeparatedToList(vm.IdsProdutos).ForEach(idProdutoServico =>
            {
                info.Produtos.Add(new ProducaoCulturaProdutoServicoInfo()
                {
                    IdProdutoServico = idProdutoServico
                });
            });

            var response = ProducaoCulturaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoCultura.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoCultura, "ProducaoCultura", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoCulturaEmpresaInfo info = new ProducaoCulturaEmpresaInfo();
            ProducaoCulturaEmpresaInfo response = new ProducaoCulturaEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCulturaEmpresaBll.Instance.ListarPorParametros(new ProducaoCulturaEmpresaInfo { IdProducaoCultura = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoCulturaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCultura, "ProducaoCultura", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoCulturaEmpresaInfo info = new ProducaoCulturaEmpresaInfo();
            ProducaoCulturaEmpresaInfo response = new ProducaoCulturaEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCulturaEmpresaBll.Instance.ListarPorParametros(new ProducaoCulturaEmpresaInfo { IdProducaoCultura = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoCulturaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCultura, "ProducaoCultura", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoCulturaInfo info = new ProducaoCulturaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoCultura = vm.IdProducaoCultura;
            info.IdAtividadeEconomica = vm.IdAtividadeEconomica;
            info.IdAtividadeEconomicaIn = vm.IdAtividadeEconomicaIn;
            info.IdProducaoCulturaIn = vm.IdProducaoCulturaIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;
            info.ComCiclos = vm.ComCiclos.HasValue ? vm.ComCiclos.Value : false;

            ListPaged<ProducaoCulturaInfo> retorno = new ListPaged<ProducaoCulturaInfo>(
                    info.ComCiclos == true ? ProducaoCulturaBll.Instance.ListarPorParametrosComCiclos(info) : ProducaoCulturaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Cultura", true);

        }

    }
}
