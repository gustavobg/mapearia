﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.CulturaTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class CulturaTipoController : ControllerExtended
    {
        public CulturaTipoController()
        {
            IndexUrl = "/Agricola/CulturaTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoCulturaTipoInfo info = ProducaoCulturaTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoCulturaTipo, "ProducaoCulturaTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoCulturaTipoInfo info = new ProducaoCulturaTipoInfo();

            ViewModelToModelMapper.Map<ProducaoCulturaTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoCulturaTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoCulturaTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoCulturaTipo, "ProducaoCulturaTipo", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoCulturaTipoEmpresaInfo info = new ProducaoCulturaTipoEmpresaInfo();
            ProducaoCulturaTipoEmpresaInfo response = new ProducaoCulturaTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCulturaTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoCulturaTipoEmpresaInfo { IdProducaoCulturaTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoCulturaTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCulturaTipo, "ProducaoCulturaTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoCulturaTipoEmpresaInfo info = new ProducaoCulturaTipoEmpresaInfo();
            ProducaoCulturaTipoEmpresaInfo response = new ProducaoCulturaTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoCulturaTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoCulturaTipoEmpresaInfo { IdProducaoCulturaTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoCulturaTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoCulturaTipo, "ProducaoCulturaTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoCulturaTipoInfo info = new ProducaoCulturaTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoCulturaTipo = vm.IdProducaoCulturaTipo;
            info.IdProducaoCulturaTipoIn = vm.IdProducaoCulturaTipoIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoCulturaTipoInfo> retorno = new ListPaged<ProducaoCulturaTipoInfo>(ProducaoCulturaTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Tipo de Cultura", true);

        }
    }
}
