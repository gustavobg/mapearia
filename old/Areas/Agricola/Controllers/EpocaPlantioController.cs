﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.EpocaPlantio;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class EpocaPlantioController : ControllerExtended
    {
        public EpocaPlantioController()
        {
            IndexUrl = "/Agricola/EpocaPlantio/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoEpocaPlantioInfo info = ProducaoEpocaPlantioBll.Instance.ListarPorParametros(new ProducaoEpocaPlantioInfo { IdEmpresa = IdEmpresa, IdProducaoEpocaPlantio = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoEpocaPlantio, "ProducaoEpocaPlantio");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoEpocaPlantioInfo info = new ProducaoEpocaPlantioInfo();

            ViewModelToModelMapper.Map<ProducaoEpocaPlantioInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoEpocaPlantioBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoEpocaPlantio.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoEpocaPlantio, "ProducaoEpocaPlantio", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoEpocaPlantioEmpresaInfo info = new ProducaoEpocaPlantioEmpresaInfo();
            ProducaoEpocaPlantioEmpresaInfo response = new ProducaoEpocaPlantioEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoEpocaPlantioEmpresaBll.Instance.ListarPorParametros(new ProducaoEpocaPlantioEmpresaInfo { IdProducaoEpocaPlantio = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoEpocaPlantioEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoEpocaPlantio, "ProducaoEpocaPlantio", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoEpocaPlantioEmpresaInfo info = new ProducaoEpocaPlantioEmpresaInfo();
            ProducaoEpocaPlantioEmpresaInfo response = new ProducaoEpocaPlantioEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoEpocaPlantioEmpresaBll.Instance.ListarPorParametros(new ProducaoEpocaPlantioEmpresaInfo { IdProducaoEpocaPlantio = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoEpocaPlantioEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoEpocaPlantio, "ProducaoEpocaPlantio", info.Response.IdHistorico);
            }

            return Json(new { response });
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoEpocaPlantioInfo info = new ProducaoEpocaPlantioInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoEpocaPlantio = vm.IdProducaoEpocaPlantio;
            info.IdProducaoEpocaPlantioNotIn = vm.IdProducaoEpocaPlantioNotIn;
            info.IdProducaoEpocaPlantioIn = vm.IdProducaoEpocaPlantioIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoEpocaPlantioInfo> retorno = new ListPaged<ProducaoEpocaPlantioInfo>(ProducaoEpocaPlantioBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Época de Plantio", true);

        }
    }
}
