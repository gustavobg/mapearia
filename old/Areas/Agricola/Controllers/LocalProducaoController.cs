﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.LocalProducao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class LocalProducaoController : ControllerExtended
    {
        public LocalProducaoController()
        {
            IndexUrl = "/Agricola/LocalProducao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New/Edit

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoLocalInfo info = new ProducaoLocalInfo();

            ViewModelToModelMapper.Map<ProducaoLocalInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.IdsCentroCusto))
            {
                info.CentrosCustos = new List<ProducaoLocalCentroCustoInfo>();
                CommaSeparatedToList(vm.IdsCentroCusto).ForEach(idcentrocusto =>
                {
                    ProducaoLocalCentroCustoInfo item = new ProducaoLocalCentroCustoInfo();
                    item.IdCentroCusto = idcentrocusto;

                    info.CentrosCustos.Add(item);

                });
            }

            if(!string.IsNullOrEmpty(vm.IdsCaracterizacao))
            {
                info.ItensCaracterizados = new List<ProducaoCaracterizacaoItemInfo>();
                CommaSeparatedToList(vm.IdsCentroCusto).ForEach(idProducaoLocal =>
                {
                    ProducaoCaracterizacaoItemInfo item = new ProducaoCaracterizacaoItemInfo();
                    item.IdProducaoLocal = idProducaoLocal;

                    info.ItensCaracterizados.Add(item);
                });
            }

            #endregion

            var response = ProducaoLocalBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoLocal.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoLocal, "ProducaoLocal", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoLocalInfo info = ProducaoLocalBll.Instance.ListarPorIdCompleto(new ProducaoLocalInfo { IdProducaoLocal = id.Value });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoLocal, "ProducaoLocal");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoLocalInfo info = new ProducaoLocalInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.IdPessoaProprietarioPrincipal = vm.IdPessoaProprietarioPrincipal;
            info.IdProducaoLocalIn = vm.IdProducaoLocalIn;
            info.IdProducaoLocalTipoIn = vm.IdProducaoLocalTipoIn;
            info.IdProducaoRegiaoIn = vm.IdProducaoRegiaoIn;
            info.IdProducaoCaracterizacaoIn = vm.IdProducaoCaracterizacaoIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.ProducaoLocalTipoPermiteRepresentacaoGeo = vm.ProducaoLocalTipoPermiteRepresentacaoGeo;

            #region Parâmetros para o Modulo de Mapeamento

            if (vm.SemItensRelacionadosComMapeamento.HasValue && vm.SemItensRelacionadosComMapeamento.Value)
            {
                List<string> lst = new List<string>();
                var lstProducaoLocal = MapeamentoElementoBll.Instance.ListarPorParametros(new Model.Mapeamento.MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 2 });
                if (!string.IsNullOrEmpty(info.IdProducaoLocalNotIn))
                {

                    lst.AddRange(info.IdProducaoLocalNotIn.Split(','));
                    if (lstProducaoLocal.Count() > 0)
                    {
                        var ids = string.Join(",", lstProducaoLocal.Select(p => p.IdReferencia).ToList());

                        lst.AddRange(ids.Split(','));
                    }
                    info.IdProducaoLocalNotIn = string.Join(",", lst);
                }
                else
                {
                    info.IdProducaoLocalNotIn = string.Join(",", lstProducaoLocal.Select(p => p.IdReferencia));
                }

            }

            #endregion

            ListPaged<ProducaoLocalInfo> retorno = new ListPaged<ProducaoLocalInfo>(ProducaoLocalBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Local de Produção", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListSetor(ListVM vm)
        {
            var info = new ProducaoLocalSetorInfo();
            info.IdProducaoLocalSetor = vm.IdProducaoLocalSetor;
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoLocal = vm.IdProducaoLocal;            
            info.Descricao = vm.Descricao;            
            
            var retorno = new ListPaged<ProducaoLocalSetorInfo>(ProducaoLocalSetorBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoLocalSetor", true);

        }

        [HttpPost, CustomAuthorize]
        public ActionResult VerificaDescricaoLocalPorEmpresa(string descricao, int? idProducaoLocal)
        {
            bool isEquals = false;

            isEquals = ProducaoLocalBll.Instance.ValidaDescricaoPorEmpresa(descricao, IdEmpresa, idProducaoLocal);
            return Json(new { data = isEquals });
        }

        #endregion
    }
}