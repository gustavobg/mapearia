﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.NaturezaArea;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class NaturezaAreaController : ControllerExtended
    {

        public NaturezaAreaController()
        {
            IndexUrl = "/Agricola/NaturezaArea/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoAreaNaturezaInfo info = new ProducaoAreaNaturezaInfo();

            ViewModelToModelMapper.Map<ProducaoAreaNaturezaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = ProducaoAreaNaturezaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoAreaNatureza.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoAreaNatureza, "ProducaoAreaNatureza", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoAreaNaturezaInfo info = ProducaoAreaNaturezaBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoAreaNatureza, "ProducaoAreaNatureza");
            }
            else
            {
                vm.Ativo = true;
            }

            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoAreaNaturezaInfo info = new ProducaoAreaNaturezaInfo();
            info.IdProducaoAreaNatureza = vm.IdProducaoAreaNatureza;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<ProducaoAreaNaturezaInfo> retorno = new ListPaged<ProducaoAreaNaturezaInfo>(ProducaoAreaNaturezaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoAreaNatureza", true);

        }
    }
}
