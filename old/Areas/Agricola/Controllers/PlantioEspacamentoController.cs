﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.PlantioEspacamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Web.Mvc;
using System.Linq;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Bll.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class PlantioEspacamentoController : ControllerExtended
    {
        public PlantioEspacamentoController()
        {
            IndexUrl = "/Agricola/PlantioEspacamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            var vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            var vm = new NewEditVM();
            return View(vm);
        }
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            var vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                var info = ProducaoPlantioEspacamentoBll.Instance.ListarPorParametros(new ProducaoPlantioEspacamentoInfo { IdEmpresa = IdEmpresa, IdProducaoPlantioEspacamento = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoPlantioEspacamento, "ProducaoPlantioEspacamento");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new ProducaoPlantioEspacamentoInfo();

            ViewModelToModelMapper.Map<ProducaoPlantioEspacamentoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoPlantioEspacamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoPlantioEspacamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoPlantioEspacamento, "ProducaoPlantioEspacamento", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoPlantioEspacamentoEmpresaInfo info = new ProducaoPlantioEspacamentoEmpresaInfo();
            ProducaoPlantioEspacamentoEmpresaInfo response = new ProducaoPlantioEspacamentoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoPlantioEspacamentoEmpresaBll.Instance.ListarPorParametros(new ProducaoPlantioEspacamentoEmpresaInfo { IdProducaoPlantioEspacamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoPlantioEspacamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoPlantioEspacamento, "ProducaoPlantioEspacamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoPlantioEspacamentoEmpresaInfo info = new ProducaoPlantioEspacamentoEmpresaInfo();
            ProducaoPlantioEspacamentoEmpresaInfo response = new ProducaoPlantioEspacamentoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoPlantioEspacamentoEmpresaBll.Instance.ListarPorParametros(new ProducaoPlantioEspacamentoEmpresaInfo { IdProducaoPlantioEspacamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoPlantioEspacamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoPlantioEspacamento, "ProducaoPlantioEspacamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ProducaoPlantioEspacamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoCultura = vm.IdProducaoCultura;
            info.IdProducaoPlantioEspacamento = vm.IdProducaoPlantioEspacamento;
            info.IdProducaoPlantioEspacamentoIn = vm.IdProducaoPlantioEspacamentoIn;
            info.IdProducaoPlantioEspacamentoNotIn = vm.IdProducaoPlantioEspacamentoNotIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            var retorno = new ListPaged<ProducaoPlantioEspacamentoInfo>(ProducaoPlantioEspacamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Espaçamento", true);
        }
    }
}
