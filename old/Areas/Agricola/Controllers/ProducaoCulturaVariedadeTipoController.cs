﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ProducaoCulturaVariedadeTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class ProducaoCulturaVariedadeTipoController : ControllerExtended
    {

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoCulturaVariedadeTipoInfo info = new ProducaoCulturaVariedadeTipoInfo();
            info.IdProducaoCultura = vm.IdProducaoCultura;
            info.IdProducaoCulturaVariedadeTipo = vm.IdProducaoCulturaVariedadeTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<ProducaoCulturaVariedadeTipoInfo> retorno = new ListPaged<ProducaoCulturaVariedadeTipoInfo>(ProducaoCulturaVariedadeTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Tipos de Variedade por Cultura", true);

        }

    }
}

