﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ProducaoUnidadeTipoLocalTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class ProducaoUnidadeTipoLocalTipoController : ControllerExtended 
    {
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoUnidadeTipoLocalTipoInfo info = new ProducaoUnidadeTipoLocalTipoInfo();
            info.IdProducaoLocalTipoIn = vm.IdProducaoLocalTipoIn;
            info.IdProducaoUnidadeTipoIn = vm.IdProducaoUnidadeTipoIn;

            //Obrigatório enviar ao menos 1 parâmetro 
            if(string.IsNullOrEmpty(vm.IdProducaoLocalTipoIn) && string.IsNullOrEmpty(vm.IdProducaoUnidadeTipoIn)) 
                return null;
            var retorno = new ListPaged<ProducaoUnidadeTipoLocalTipoInfo>(ProducaoUnidadeTipoLocalTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Relacional Unidade Tipo X Local Tipo", true);
        }

    }
}
