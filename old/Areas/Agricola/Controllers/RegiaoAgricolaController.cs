﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.RegiaoAgricola;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class RegiaoAgricolaController : ControllerExtended
    {
        public RegiaoAgricolaController()
        {
            IndexUrl = "/Agricola/RegiaoAgricola/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoRegiaoInfo info = ProducaoRegiaoBll.Instance.ListarPorParametros(new ProducaoRegiaoInfo { IdEmpresa = IdEmpresa, IdProducaoRegiao = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoRegiao, "ProducaoRegiao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoRegiaoInfo info = new ProducaoRegiaoInfo();

            ViewModelToModelMapper.Map<ProducaoRegiaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoRegiaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoRegiao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoRegiao, "ProducaoRegiao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoRegiaoEmpresaInfo info = new ProducaoRegiaoEmpresaInfo();
            ProducaoRegiaoEmpresaInfo response = new ProducaoRegiaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoRegiaoEmpresaBll.Instance.ListarPorParametros(new ProducaoRegiaoEmpresaInfo { IdProducaoRegiao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoRegiaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoRegiao, "ProducaoRegiao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoRegiaoEmpresaInfo info = new ProducaoRegiaoEmpresaInfo();
            ProducaoRegiaoEmpresaInfo response = new ProducaoRegiaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoRegiaoEmpresaBll.Instance.ListarPorParametros(new ProducaoRegiaoEmpresaInfo { IdProducaoRegiao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoRegiaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoRegiao, "ProducaoRegiao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoRegiaoInfo info = new ProducaoRegiaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoRegiaoInfo> retorno = new ListPaged<ProducaoRegiaoInfo>(ProducaoRegiaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoRegiao", true);

        }
    }
}
