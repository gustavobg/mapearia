﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.TipoLocal;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class TipoLocalController : ControllerExtended
    {
        public TipoLocalController()
        {
            IndexUrl = "/Agricola/TipoLocal/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoLocalTipoInfo info = ProducaoLocalTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoLocalTipo, "ProducaoLocalTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoLocalTipoInfo info = new ProducaoLocalTipoInfo();

            ViewModelToModelMapper.Map<ProducaoLocalTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoLocalTipoBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoLocalTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoLocalTipo, "ProducaoLocalTipo", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoLocalTipoEmpresaInfo info = new ProducaoLocalTipoEmpresaInfo();
            ProducaoLocalTipoEmpresaInfo response = new ProducaoLocalTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoLocalTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoLocalTipoEmpresaInfo { IdProducaoLocalTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoLocalTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoLocalTipo, "ProducaoLocalTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoLocalTipoEmpresaInfo info = new ProducaoLocalTipoEmpresaInfo();
            ProducaoLocalTipoEmpresaInfo response = new ProducaoLocalTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoLocalTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoLocalTipoEmpresaInfo { IdProducaoLocalTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoLocalTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoLocalTipo, "ProducaoLocalTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoLocalTipoInfo info = new ProducaoLocalTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoLocalTipo = vm.IdProducaoLocalTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.PermiteSubLocais = vm.PermiteSubLocais;
            info.IdProducaoLocalTipoIn = vm.IdProducaoLocalTipoIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoLocalTipoInfo> retorno = new ListPaged<ProducaoLocalTipoInfo>(ProducaoLocalTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoLocalTipo", true);

        }

    }
}
