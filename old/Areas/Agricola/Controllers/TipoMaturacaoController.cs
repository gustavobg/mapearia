﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.TipoMaturacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class TipoMaturacaoController : ControllerExtended
    {

        public TipoMaturacaoController()
        {
            IndexUrl = "/Agricola/TipoMaturacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoTipoMaturacaoInfo info = ProducaoTipoMaturacaoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoTipoMaturacao, "ProducaoTipoMaturacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoTipoMaturacaoInfo info = new ProducaoTipoMaturacaoInfo();

            ViewModelToModelMapper.Map<ProducaoTipoMaturacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoTipoMaturacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoTipoMaturacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoTipoMaturacao, "ProducaoTipoMaturacao", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoTipoMaturacaoEmpresaInfo info = new ProducaoTipoMaturacaoEmpresaInfo();
            ProducaoTipoMaturacaoEmpresaInfo response = new ProducaoTipoMaturacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoTipoMaturacaoEmpresaBll.Instance.ListarPorParametros(new ProducaoTipoMaturacaoEmpresaInfo { IdProducaoTipoMaturacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoTipoMaturacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoTipoMaturacao, "ProducaoTipoMaturacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoTipoMaturacaoEmpresaInfo info = new ProducaoTipoMaturacaoEmpresaInfo();
            ProducaoTipoMaturacaoEmpresaInfo response = new ProducaoTipoMaturacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoTipoMaturacaoEmpresaBll.Instance.ListarPorParametros(new ProducaoTipoMaturacaoEmpresaInfo { IdProducaoTipoMaturacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoTipoMaturacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoTipoMaturacao, "ProducaoTipoMaturacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoTipoMaturacaoInfo info = new ProducaoTipoMaturacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoTipoMaturacao = vm.IdProducaoTipoMaturacao;
            info.Descricao = vm.Descricao;
            info.Sigla = vm.Sigla;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoTipoMaturacaoInfo> retorno = new ListPaged<ProducaoTipoMaturacaoInfo>(ProducaoTipoMaturacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Tipo de Maturação", true);

        }
    }
}
