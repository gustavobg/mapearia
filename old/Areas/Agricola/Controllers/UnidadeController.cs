using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Unidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Bll.Mapeamento;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class UnidadeController : ControllerExtended
    {
        public UnidadeController()
        {
            IndexUrl = "/Agricola/Unidade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new ProducaoUnidadeInfo();

            ViewModelToModelMapper.Map<ProducaoUnidadeInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.IdsProducaoCaracterizacao))
            {
                info.lstUnidadeCaracterizacao = new List<ProducaoUnidadeCaracterizacaoInfo>();

                CommaSeparatedToList(vm.IdsProducaoCaracterizacao).ForEach(idProducaoCaracterizacao =>
                {
                    ProducaoUnidadeCaracterizacaoInfo item = new ProducaoUnidadeCaracterizacaoInfo();
                    item.IdProducaoCaracterizacao = idProducaoCaracterizacao;

                    info.lstUnidadeCaracterizacao.Add(item);
                });
            }

            #endregion

            var response = ProducaoUnidadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoUnidade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoUnidade, "ProducaoUnidade", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoUnidadeInfo info = ProducaoUnidadeBll.Instance.ListarCompletoPorId(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoUnidade, "ProducaoUnidade");
            }
            else
            {
                vm.IdUnidadeAltitude = 1;
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ProducaoUnidadeInfo();
            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.IdProducaoUnidade = vm.IdProducaoUnidade;
            info.IdProducaoUnidadeIn = vm.IdProducaoUnidadeIn;
            info.Descricao = vm.Descricao;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.IdProducaoLocalIn = vm.IdProducaoLocalIn;
            info.IdProducaoLocalTipoIn = vm.IdProducaoLocalTipoIn;
            info.IdProducaoUnidadeTipoIn = vm.IdProducaoUnidadeTipoIn;
            info.IdProducaoCaracterizacaoIn = vm.IdProducaoCaracterizacaoIn;
            info.CodigoDescricao = vm.CodigoDescricao;
            info.ProducaoUnidadeTipoPermiteRepresentacaoGeo = vm.ProducaoUnidadeTipoPermiteRepresentacaoGeo;

            #region Parâmetros para o Modulo de Mapeamento

            if (vm.SemItensRelacionadosComMapeamento.HasValue && vm.SemItensRelacionadosComMapeamento.Value)
            {
                List<string> lst = new List<string>();
                var lstProducaoUnidade = MapeamentoElementoBll.Instance.ListarPorParametros(new Model.Mapeamento.MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 3 });
                if (!string.IsNullOrEmpty(info.IdProducaoUnidadeNotIn))
                {

                    lst.AddRange(info.IdProducaoUnidadeNotIn.Split(','));
                    if (lstProducaoUnidade.Count() > 0)
                    {
                        var ids = string.Join(",", lstProducaoUnidade.Select(p => p.IdReferencia).ToList());

                        lst.AddRange(ids.Split(','));
                    }
                    info.IdProducaoUnidadeNotIn = string.Join(",", lst);
                }
                else
                {
                    info.IdProducaoUnidadeNotIn = string.Join(",", lstProducaoUnidade.Select(p => p.IdReferencia));
                }

            }

            #endregion

            var retorno = new ListPaged<ProducaoUnidadeInfo>(ProducaoUnidadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Unidade de Produção", true);
        }
    }
}
