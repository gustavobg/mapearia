using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeImplantacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Bll.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class UnidadeImplantacaoController : ControllerExtended
    {
        public UnidadeImplantacaoController()
        {
            IndexUrl = "/Agricola/UnidadeImplantacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            var vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new ProducaoUnidadeImplantacaoInfo();

            ViewModelToModelMapper.Map<ProducaoUnidadeImplantacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.DataImplantacaoExibicao))
                info.DataImplantacao = Convert.ToDateTime(vm.DataImplantacaoExibicao);

            var response = ProducaoUnidadeImplantacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoUnidadeImplantacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoUnidadeImplantacao, "ProducaoUnidadeImplantacao", info.Response.IdHistorico);
            
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            var vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                var info = ProducaoUnidadeImplantacaoBll.Instance.ListarCompletoPorId(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                if (info.DataImplantacao.HasValue)
                    vm.DataImplantacaoExibicao = info.DataImplantacao.Value.ToShortDateString();

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoUnidadeImplantacao, "ProducaoUnidadeImplantacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            var vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ProducaoUnidadeImplantacaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoUnidadeImplantacao = vm.IdProducaoUnidadeImplantacao;
            info.Codigo = vm.Codigo;
            //info.DataImplantacao = vm.DataImplantacao;
            info.IdProducaoUnidadeIn = vm.IdProducaoUnidadeIn;
            info.IdProducaoEpocaPlantioIn = vm.IdProducaoEpocaPlantioIn;
            info.Ativo = vm.Ativo;

            var retorno = new ListPaged<ProducaoUnidadeImplantacaoInfo>(ProducaoUnidadeImplantacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                    {
                        CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                        PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                        GroupBy = vm.Page.GroupBy,
                        OrderBy = vm.Page.OrderBy
                    }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoUnidadeImplantacao", true);
        }
    }
}
