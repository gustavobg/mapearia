using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeTipo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Bll.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class UnidadeTipoController : ControllerExtended
    {
        public UnidadeTipoController()
        {
            IndexUrl = "/Agricola/UnidadeTipo/Index";
        }


        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoUnidadeTipoInfo info = ProducaoUnidadeTipoBll.Instance.ListarCompletoPorId(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoUnidadeTipo, "ProducaoUnidadeTipoLocalTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoUnidadeTipoInfo info = new ProducaoUnidadeTipoInfo();


            ViewModelToModelMapper.Map<ProducaoUnidadeTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            // monta lista de IdPessoaPerfilPessoa
            if (!string.IsNullOrEmpty(vm.IdsProducaoLocalTipo))
            {
                info.UnidadesLocais = new List<ProducaoUnidadeTipoLocalTipoInfo>();

                CommaSeparatedToList(vm.IdsProducaoLocalTipo).ForEach(IdProducaoLocalTipo =>
                {
                    ProducaoUnidadeTipoLocalTipoInfo x = new ProducaoUnidadeTipoLocalTipoInfo();
                    x.IdProducaoLocalTipo = IdProducaoLocalTipo;

                    info.UnidadesLocais.Add(x);
                });
            }


            var response = ProducaoUnidadeTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoUnidadeTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoUnidadeTipo, "ProducaoUnidadeTipoLocalTipo", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoUnidadeTipoEmpresaInfo info = new ProducaoUnidadeTipoEmpresaInfo();
            ProducaoUnidadeTipoEmpresaInfo response = new ProducaoUnidadeTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoUnidadeTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoUnidadeTipoEmpresaInfo { IdProducaoUnidadeTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoUnidadeTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoUnidadeTipo, "ProducaoUnidadeTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoUnidadeTipoEmpresaInfo info = new ProducaoUnidadeTipoEmpresaInfo();
            ProducaoUnidadeTipoEmpresaInfo response = new ProducaoUnidadeTipoEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoUnidadeTipoEmpresaBll.Instance.ListarPorParametros(new ProducaoUnidadeTipoEmpresaInfo { IdProducaoUnidadeTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoUnidadeTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoUnidadeTipo, "ProducaoUnidadeTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoUnidadeTipoInfo info = new ProducaoUnidadeTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.Descricao = vm.Descricao;
            info.IdProducaoUnidadeTipo = vm.IdProducaoUnidadeTipo;
            info.RegistroProprio = vm.RegistroProprio;

            ProducaoUnidadeTipoBll.Instance.TranslateCaracteristicaUnidadeTipo(vm.CaracteristicaUnidadeTipo, info);

            var retorno = new ListPaged<ProducaoUnidadeTipoInfo>(ProducaoUnidadeTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProducaoUnidadeTipo", true);
        }

        [HttpPost]
        public JsonResult GetTipoUnidadeCompleto(int? id)
        {
            var info = new ProducaoUnidadeTipoInfo();
            if (id.HasValue && id > 0)
            {
                //IdEmpresa == EmpresaLogada
                info = ProducaoUnidadeTipoBll.Instance.ListarCompletoPorId(id.Value, IdEmpresa);
            }

            return Json(info);
        }
    }
}
