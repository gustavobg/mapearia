﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Variedade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.Controllers
{
    public class VariedadeController : ControllerExtended
    {
        public VariedadeController()
        {
            IndexUrl = "/Agricola/Variedade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProducaoVariedadeInfo info = ProducaoVariedadeBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProducaoVariedade, "ProducaoVariedade");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProducaoVariedadeInfo info = new ProducaoVariedadeInfo();

            ViewModelToModelMapper.Map<ProducaoVariedadeInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProducaoVariedadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProducaoVariedade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProducaoVariedade, "ProducaoVariedade", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProducaoVariedadeEmpresaInfo info = new ProducaoVariedadeEmpresaInfo();
            ProducaoVariedadeEmpresaInfo response = new ProducaoVariedadeEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoVariedadeEmpresaBll.Instance.ListarPorParametros(new ProducaoVariedadeEmpresaInfo { IdProducaoVariedade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProducaoVariedadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoVariedade, "ProducaoVariedade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProducaoVariedadeEmpresaInfo info = new ProducaoVariedadeEmpresaInfo();
            ProducaoVariedadeEmpresaInfo response = new ProducaoVariedadeEmpresaInfo();

            if (id > 0)
            {
                info = ProducaoVariedadeEmpresaBll.Instance.ListarPorParametros(new ProducaoVariedadeEmpresaInfo { IdProducaoVariedade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProducaoVariedadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProducaoVariedade, "ProducaoVariedade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProducaoVariedadeInfo info = new ProducaoVariedadeInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProducaoCultura = vm.IdProducaoCultura;
            info.IdProducaoVariedade = vm.IdProducaoVariedade;
            info.Descricao = vm.Descricao;
            info.Sigla = vm.Sigla;
            info.Ativo = vm.Ativo;
            info.IdProducaoVariedadeIn = vm.IdProducaoVariedadeIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProducaoVariedadeInfo> retorno = new ListPaged<ProducaoVariedadeInfo>(ProducaoVariedadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Varidades", true);

        }

    }
}
