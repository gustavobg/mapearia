﻿using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.AmbienteProducao
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url)
            : base(url)
        {
        }
    }
}