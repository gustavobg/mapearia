﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.AmbienteProducao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProducaoAmbienteProducao { get; set; }

        public int? IdEmpresa { get; set; }

        public string Sigla { get; set; }

        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}