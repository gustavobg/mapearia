﻿using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.AmbienteProducao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoAmbienteProducao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }
    }
}