﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Caracterizacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoCaracterizacao { get; set; }

        public string IdProducaoCaracterizacaoIn { get; set; }
        public string IdProducaoCaracterizacaoNotIn { get; set; }
        public string Descricao { get; set; }

        public bool? CaracterizaLocal { get; set; }
        public bool? CaracterizaUnidade { get;set;}
        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}