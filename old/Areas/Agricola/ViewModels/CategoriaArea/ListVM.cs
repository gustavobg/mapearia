﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.CategoriaArea
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoAreaCategoria { get; set; }
        public int? IdProducaoAreaNatureza { get; set; }

        public string Descricao { get; set; }
        public string IdProducaoAreaNaturezaIn { get; set; }
        public string IdProducaoAreaCategoriaNotIn { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
        
    }
}