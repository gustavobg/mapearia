﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaCultura
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdProducaoConfiguracaoEstimativaCultura { get; set; }
        public int? IdProducaoCultura { get; set; }
        public int? IdProdutoServico { get; set; }
        public int? IdProducaoEpocaPlantio { get; set; }

        public bool? Ativo { get; set; }
    }
}