﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaCultura
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Itens = new List<ItemVM>();
        }

        #region Propriedades para Exibição
        public string DescricaoExibicao { get; set; }
        public int ParentId { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEspacamentosCultura { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoNotIn { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEpocaPlantioNotIn { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEspacamentosNotIn { get; set; }
        #endregion


        [ViewModelToModelAttribute]
        public int? IdProducaoConfiguracaoEstimativaCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public bool Principal { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeEstimativaProducao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEspacamento { get; set; }

        [ViewModelToModelAttribute]
        public int? ColheitasPossiveisProducaoCultura { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<ItemVM> Itens { get; set; }

        public class ItemVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoConfiguracaoEstimativaItem { get; set; }

            [ViewModelToModelAttribute]
            public int? Colheita { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PerdaValor { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ProducaoValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeRendimento { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidadeRendimento { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoEstimativaComposta { get; set; }

        }
    }
}