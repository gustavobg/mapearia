﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaVariedade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoConfiguracaoEstimativaVariedade { get; set; }
        public int? IdProducaoVariedade { get; set; }

        public bool? Ativo { get; set; }

    }
}