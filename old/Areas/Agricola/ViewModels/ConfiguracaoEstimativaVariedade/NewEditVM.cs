﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ConfiguracaoEstimativaVariedade
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Itens = new List<ItemVM>();
        }

        #region Propriedades especiais

        [ViewModelToModelAttribute]
        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoEspacamento { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public int? VidaEmColheitas { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeEstimativaProducao { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdProducaoConfiguracaoEstimativaVariedade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoVariedade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public bool? Principal { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoPlantioEspacamento { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemVM> Itens { get; set; }

        public class ItemVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoConfiguracaoEstimativaItem { get; set; }

            [ViewModelToModelAttribute]
            public int? Colheita { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PerdaValor { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ProducaoValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeRendimento { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidadeRendimento { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoEstimativaComposta { get; set; }
        }
    }
}