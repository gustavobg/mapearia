﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Cultura
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoCultura { get; set; }
        public int? IdAtividadeEconomica { get; set; }

        public string IdProducaoCulturaIn { get; set; }
        public string Descricao { get; set; }
        public string IdAtividadeEconomicaIn { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

        /// <summary>
        /// Propriedade utilizada para buscar ou não os Ciclos 
        /// </summary>
        public bool? ComCiclos { get; set; }
    }
}