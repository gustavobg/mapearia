﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Cultura
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            CulturasEpocas = new List<ProdutoCulturaEpocaPlantioVM>();
            VariedadesTipo = new List<ProducaoCulturaVariedadeTipo>();
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCulturaTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdObjetoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdObjeto { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool PlantioObrigatorio { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteVariasColheitas { get; set; }

        [ViewModelToModelAttribute]
        public int? ColheitasPossiveis { get; set; }

        [ViewModelToModelAttribute]
        public int? AvaliacaoProdutividadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? EstimativaTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsObjetivos { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEspacamentos { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutos { get; set; }

        [ViewModelToModelAttribute]
        public bool CentroCustoObrigatorio { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<ProdutoCulturaEpocaPlantioVM> CulturasEpocas { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProducaoCulturaVariedadeTipo> VariedadesTipo { get; set; }

        public class ProdutoCulturaEpocaPlantioVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoCulturaEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoCultura { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string TempoPrimeiraColheita { get; set; }

            [ViewModelToModelAttribute]
            public string TempoDemaisColheitas {get;set;}

            [ViewModelToModelAttribute]
            public string DescricaoProducaoEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidade { get; set; }
        }

        public class ProducaoCulturaVariedadeTipo
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoCulturaVariedadeTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoCultura { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }
        }
    }
}