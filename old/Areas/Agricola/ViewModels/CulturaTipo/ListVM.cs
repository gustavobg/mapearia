﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.CulturaTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoCulturaTipo { get; set; }
        public string IdProducaoCulturaTipoIn { get; set; }
        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}