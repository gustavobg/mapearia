﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.CulturaTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() {
            PermiteVariasColheitas = false;
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoCulturaTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool SemDefinicao { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteVariasColheitas { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}