﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.EpocaPlantio
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdProducaoEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicio { get; set; }

        [ViewModelToModelAttribute]
        public string DataTermino { get; set; }

        [ViewModelToModelAttribute]
        public int? Ordem { get; set; }

        [ViewModelToModelAttribute]
        public bool Padrao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}