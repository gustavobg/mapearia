﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.LocalProducao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProducaoLocal { get; set; }
        public int? IdPessoaProprietarioPrincipal { get; set; }
        public int? IdProducaoLocalSetor { get; set; }

        public string IdProducaoLocalTipoIn { get; set; }
        public string IdProducaoLocalIn { get; set; }
        public string IdProducaoRegiaoIn { get; set; }
        public string IdProducaoCaracterizacaoIn { get; set; }
        public string Descricao { get; set; }
        public string BuscaAvancada { get; set; }

        
        public bool? ProducaoLocalTipoPermiteRepresentacaoGeo { get; set; }
        public bool? SemItensRelacionadosComMapeamento { get; set; }
        public bool? Ativo { get; set; }
    }
}
