﻿using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.LocalProducao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            Areas = new List<ProducaoLocalAreaVM>();
            Setores = new List<ProducaoLocalSetorVM>();
            Proprietarios = new List<ProducaoLocalProprietarioVM>();
            Pessoa = new NewVM();
        }

        public NewVM Pessoa {get;set;}

        [ViewModelToModelAttribute]
        public int? IdProducaoLocal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoLocalTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeProducaoLocalTipo { get; set; } //IdUnidade informada no local no Tipo de Local de Produção [ProducaoLocalTipo]

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoRegiao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public string Latitude { get; set; }

        [ViewModelToModelAttribute]
        public string Longitude { get; set; }

        [ViewModelToModelAttribute]
        public string Altitude { get; set; }

        [ViewModelToModelAttribute]
        public string CCIR { get; set; }

        [ViewModelToModelAttribute]
        public string NIRF { get; set; }

        [ViewModelToModelAttribute]
        public string IE { get; set; }

        [ViewModelToModelAttribute]
        public string CNPJ { get; set; }

        [ViewModelToModelAttribute]
        public decimal? CotaTotal { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public int? Matricula { get; set; }

        [ViewModelToModelAttribute]
        public bool LocalProprio { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEstado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdLocalidade { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCaracterizacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAreasNotIn { get; set; } //Utilizado Apenas na View

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProducaoLocalAreaVM> Areas { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProducaoLocalSetorVM> Setores { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProducaoLocalProprietarioVM> Proprietarios { get; set; }

        public class ProducaoLocalAreaVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoLocalArea { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoAreaCategoria { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProducaoAreaCategoria { get; set; }

            [ViewModelToModelAttribute]
            public string ApresentacaoValor 
            {
                get
                {
                    var apresentacao = DescricaoProducaoAreaCategoria;
                    if (!string.IsNullOrEmpty(SiglaUnidade))
                    {
                        apresentacao = string.Format("{0} ({1})", Valor, SiglaUnidade);
                    }

                    return apresentacao;
                }
            }
        }
        public class ProducaoLocalSetorVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoLocalSetor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public string Sigla { get; set; }

            [ViewModelToModelAttribute]
            public bool Ativo { get; set; }
        }
        public class ProducaoLocalProprietarioVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoLocalProprietario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Cota { get; set; }

            [ViewModelToModelAttribute]
            public bool? Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomePessoa { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }
        }

    }
}