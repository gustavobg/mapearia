﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Objetivo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProducaoObjetivo { get; set; }
        public string IdProducaoObjetivoIn { get; set; }        
        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}