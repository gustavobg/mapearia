using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.PlantioEspacamento
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url)
            : base(url)
        {
        }
    }
}
