using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.PlantioEspacamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProducaoCultura { get; set; }
        public int? IdProducaoPlantioEspacamento { get; set; }
        public int? IdEmpresa { get; set; }
        
        public string IdProducaoPlantioEspacamentoIn { get; set; }
        public string IdProducaoPlantioEspacamentoNotIn { get; set; }

        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}
