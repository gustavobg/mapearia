using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.PlantioEspacamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoPlantioEspacamento { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdUnidade { get; set; }
        
        [ViewModelToModelAttribute]
        public string Descricao { get; set; }
        
        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }
        
        [ViewModelToModelAttribute]
        public int EspacamentoOpcao { get; set; }
        
        [ViewModelToModelAttribute]
        public decimal? Linha { get; set; }
        
        [ViewModelToModelAttribute]
        public decimal? EntreLinha { get; set; }
        
        [ViewModelToModelAttribute]
        public decimal? EntreLinhaDupla { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }


    }
}
