﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ProducaoCulturaVariedadeTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdProducaoCulturaVariedadeTipo { get; set; }
        public int? IdProducaoCultura { get; set; }

        public string Descricao { get; set; }

        public bool? Ativo { get; set; }
    }
}