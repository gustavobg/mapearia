﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.ProducaoUnidadeTipoLocalTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public string IdProducaoLocalTipoIn { get; set; }
        public string IdProducaoUnidadeTipoIn { get; set; }
    }
}