﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.TipoLocal
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoLocalTipo { get; set; }

        public string IdProducaoLocalTipoIn { get; set; }
        public string Descricao { get; set; }

        public bool? PermiteSubLocais { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}