﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.TipoLocal
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            PermiteSubLocais = false;
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoLocalTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Prefixo { get; set; }

        [ViewModelToModelAttribute]
        public bool? PossuiUnidadeProducao { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteSubLocais { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteRepresentacaoGeo { get; set; }

        [ViewModelToModelAttribute]
        public bool? GeracaoAutomaticaMapa { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamadaCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamada { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}