﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.TipoMaturacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoTipoMaturacao { get; set; }
        public int? IdEmpresa { get; set; }
        
        public string Descricao { get; set; }
        public string Sigla { get; set; }
        
        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; } 

    }
}