using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Unidade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProducaoUnidade { get; set; }
        public int? IdProducaoCaracterizacao { get; set; }
        public int? IdProducaoLocal { get; set; }

        public string Descricao { get; set; }
        public string CodigoDescricao { get; set; }
        public string IdProducaoCaracterizacaoIn { get; set; }

        public string IdProducaoUnidadeTipoIn { get; set; }
        public string IdProducaoLocalIn { get; set; }
        public string IdProducaoLocalTipoIn { get; set; }
        public string IdProducaoUnidadeIn { get; set; }

        public bool? ProducaoUnidadeTipoPermiteRepresentacaoGeo { get; set; }
        public bool? SemItensRelacionadosComMapeamento { get; set; }
        public bool? Ativo { get; set; }
    }
}
