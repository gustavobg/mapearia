using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Unidade
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            //BasesValores = new List<BaseValorVM>();
            Nomenclatura = 1;
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidade { get; set; }

        [ViewModelToModelAttribute]
        public string IdProducaoUnidadeIn { get; set; }

        [ViewModelToModelAttribute]
        public string IdProducaoUnidadeNotIn { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoLocal { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoProducaoLocal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoAmbienteProducao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoProducaoUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoLocalSetor { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? Nomenclatura { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string Longitude { get; set; }

        [ViewModelToModelAttribute]
        public string Latitude { get; set; }

        [ViewModelToModelAttribute]
        public string Altitude { get; set; }

        [ViewModelToModelAttribute]
        public string UnidadeAltitude { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeAltitude { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoUnidadeAltitude { get; set; }

        [ViewModelToModelAttribute]
        public string Declividade { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool PossuiUnidadeBase { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoSubUnidade { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorUnidadePrincipal { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorUnidadeSecundaria { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoCaracterizacao { get; set; }

        //[ViewModelToModelAttribute(ComplexType = true)]
        //public List<BaseValorVM> BasesValores { get; set; }

        //public class BaseValorVM
        //{
        //    [ViewModelToModelAttribute]
        //    public int? IdProducaoUnidadeBase { get; set; }

        //    [ViewModelToModelAttribute]
        //    public int? IdProducaoUnidade { get; set; }

        //    [ViewModelToModelAttribute]
        //    public int? IdProducaoTipoUnidadeBase { get; set; }

        //    [ViewModelToModelAttribute]
        //    public decimal? Valor { get; set; }

        //    [ViewModelToModelAttribute]
        //    public int? IdUnidade { get; set; }

        //    [ViewModelToModelAttribute]
        //    public string DescricaoUnidade { get; set; }
        //}
    }
}
