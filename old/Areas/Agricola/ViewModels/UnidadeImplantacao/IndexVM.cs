using HTM.MasterGestor.Web.UI.Infrastructure;
namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeImplantacao
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url)
            : base(url)
        {
        }
    }
}