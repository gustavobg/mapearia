using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeImplantacao
{
    public class ListVM : ViewModelListRequestBase
    {
        
        public ListVM()
        {
        }

        public int? IdProducaoUnidadeImplantacao { get; set; }

        public string Codigo;
        public string IdProducaoUnidadeIn { get; set; }
        public string IdProducaoEpocaPlantioIn { get; set; }

        public bool? Ativo { get; set; }
    }
}