using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeImplantacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            PlantioVariedades = new List<PlantioVariedadeVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidadeImplantacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoPlantioEspacamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoEpocaPlantio { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataImplantacao { get; set; }

        [ViewModelToModelAttribute]
        public string DataImplantacaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string AreaTotal { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<PlantioVariedadeVM> PlantioVariedades { get; set; }
                
        public class PlantioVariedadeVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoUnidadePlantioVariedade { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoVariedade { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProducaoVariedade { get; set; }

            [ViewModelToModelAttribute]
            public decimal Area { get; set; }

            [ViewModelToModelAttribute]
            public int IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }
        }
    }
}
