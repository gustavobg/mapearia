using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Model.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {        
        }

        public int? IdProducaoUnidadeTipo { get; set; }

        public CaracteristicaTipoUnidade? CaracteristicaUnidadeTipo { get; set; }

        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

    }
}
