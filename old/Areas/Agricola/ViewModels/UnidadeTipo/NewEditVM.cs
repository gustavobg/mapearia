using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Agricola;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.UnidadeTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            TipoControleSubUnidade = 1;
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTipoItem { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Prefixo { get; set; }

        [ViewModelToModelAttribute]
        public bool? PodeSerPlantado { get; set; }

        [ViewModelToModelAttribute]
        public bool? SeraColhido { get; set; }

        [ViewModelToModelAttribute]
        public bool? CaracterizaArea { get; set; }

        [ViewModelToModelAttribute]
        public bool PossuiUnidadeBase { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteRepresentacaoGeo { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoLocalTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoControleSubUnidade  { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoSubUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeTipoPrincipal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePrincipal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeTipoSecundaria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeSecundaria { get; set; }

        [ViewModelToModelAttribute]
        public CaracteristicaTipoUnidade CaracteristicaUnidadeTipo { get; set; }
        
    }
}
