﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Variedade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProducaoVariedade { get; set; }
        public int? IdProducaoCultura { get; set; }

        public string IdProducaoVariedadeIn { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get;set;}

    }
}