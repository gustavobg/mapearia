﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Agricola.ViewModels.Variedade
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            VariedadesEpocas = new List<ProdutoVariedadeEpocaPlantioVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdProducaoVariedade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCulturaVariedadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoTipoMaturacao { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public int? VidaEmColheitas { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProdutoVariedadeEpocaPlantioVM> VariedadesEpocas { get; set; }

        public class ProdutoVariedadeEpocaPlantioVM
        {
            [ViewModelToModelAttribute]
            public int? IdProducaoVariedadeEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoVariedade { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string TempoPrimeiraColheita { get; set; }

            [ViewModelToModelAttribute]
            public string TempoDemaisColheitas { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProducaoEpocaPlantio { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidade { get; set; }

        }
    }
}