﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao
{
    public class AplicacaoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Aplicacao";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Aplicacao_default",
                "Aplicacao/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
