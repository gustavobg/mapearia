﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoReferencia;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class AplicacaoReferenciaController : ControllerExtended
    {
        public AplicacaoReferenciaController()
        {
            IndexUrl = "Aplicacao/AplicacaoReferencia/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(string id)
        {
            NewEditVM vm = new NewEditVM();

            if (!string.IsNullOrEmpty(id))
            {
                AplicacaoReferenciaInfo info = new AplicacaoReferenciaInfo();
                info = AplicacaoReferenciaBll.Instance.ListarAplicacaoPorChaveComSubAplicacao(id,IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAplicacaoReferencia, "AplicacaoReferencia");
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListInformacaoCompletaPorAplicacaoTipo(ListVM vm)
        {
            AplicacaoReferenciaInfo info = new AplicacaoReferenciaInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacaoReferencia = vm.IdAplicacaoReferencia;

            ListPaged<AplicacaoReferenciaInfo> retorno = new ListPaged<AplicacaoReferenciaInfo>(AplicacaoReferenciaBll.Instance.ListarInformacaoCompletaPorAplicacaoTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Aplicações", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AplicacaoReferenciaInfo info = new AplicacaoReferenciaInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacaoReferencia = vm.IdAplicacaoReferencia;

            ListPaged<AplicacaoReferenciaInfo> retorno = new ListPaged<AplicacaoReferenciaInfo>(AplicacaoReferenciaBll.Instance.ListarAplicacaoPorAplicacaoTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Aplicacao Referência", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListPorAplicacaoTipo(ListVM vm)
        {
            AplicacaoReferenciaInfo info = new AplicacaoReferenciaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAplicacaoReferencia = vm.IdAplicacaoReferencia;
            info.IdAplicacaoReferenciaIn = vm.IdAplicacaoReferenciaIn;
            info.DescricaoBusca = vm.DescricaoBusca;

            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacaoTipoIn = vm.IdAplicacaoTipoIn;

            //Agrícola
            info.IdProducaoLocalIn = vm.IdProducaoLocalIn;
            info.IdProducaoLocalTipoIn = vm.IdProducaoLocalTipoIn;

            //Benfeitoria
            info.IdBenfeitoriaTipoIn = vm.IdBenfeitoriaTipoIn;
            info.IdBenfeitoriaIn = vm.IdBenfeitoriaIn;

            //Automotivo
            info.IdAutomotivoTipoEquipamentoIn = vm.IdAutomotivoTipoEquipamentoIn;
            info.IdAutomotivoEquipamentoIn = vm.IdAutomotivoEquipamentoIn;

            //Criação
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;

            if (!string.IsNullOrEmpty(vm.IdCriacaoLoteIn))
                info.IdCriacaoLoteIn = vm.IdCriacaoLoteIn;
            if (!string.IsNullOrEmpty(vm.IdCriacaoAnimalIn))
                info.IdCriacaoAnimalIn = vm.IdCriacaoAnimalIn;

            info.Ativo = vm.Ativo;

            ListPaged<AplicacaoReferenciaInfo> retorno = new ListPaged<AplicacaoReferenciaInfo>(AplicacaoReferenciaBll.Instance.ListAplicacaoPorTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Aplicacao Referência", true);
        }

        [HttpGet]
        public ActionResult ListarPorId(int id)
        {
            var info = AplicacaoReferenciaBll.Instance.ListarPorCodigo(id);

            var response = AplicacaoReferenciaBll.Instance.ListAplicacaoPorTipo(new AplicacaoReferenciaInfo 
                           {    IdEmpresa = info.IdEmpresa, 
                                IdAplicacaoTipoIn = info.IdAplicacaoTipo.Value.ToString(), 
                                IdAplicacaoReferencia = info.IdAplicacaoReferencia })
                            .FirstOrDefault();

            return Json(new { Data = response }, JsonRequestBehavior.AllowGet);
        }
    }
}
