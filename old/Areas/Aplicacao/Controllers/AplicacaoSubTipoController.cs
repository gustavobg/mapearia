﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoSubTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class AplicacaoSubTipoController : ControllerExtended
    {
        
        public AplicacaoSubTipoController()
        {

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AplicacaoSubTipoInfo info = new AplicacaoSubTipoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.DescricaoProducaoLocalTipo = vm.DescricaoProducaoLocalTipo;
            info.IdAplicacaoSubTipoIn = vm.IdAplicacaoSubTipoIn;
            //info.Descricao = vm.Descricao;
            //info.Ativo = vm.Ativo;

            ListPaged<AplicacaoSubTipoInfo> retorno = new ListPaged<AplicacaoSubTipoInfo>(AplicacaoSubTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Sub Tipo de Aplicação", true);
        }

    }
}
