﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class AplicacaoTipoController : ControllerExtended
    {
        public AplicacaoTipoController()
        {
            IndexUrl = "/Aplicacao/AplicacaoTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AplicacaoTipoInfo info = new AplicacaoTipoInfo();

            ViewModelToModelMapper.Map<AplicacaoTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            if (!vm.EditarRegistro.Value)
                info.IdEmpresa = null;
            else
                info.IdEmpresa = IdEmpresa;

            var response = AplicacaoTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAplicacaoTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdAplicacaoTabelaReferencia, "AplicacaoTipo", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                AplicacaoTipoInfo info = AplicacaoTipoBll.Instance.ListarPorCodigoCompleto(new AplicacaoTipoInfo { IdAplicacaoTipo = id.Value }, ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value);

                if (info.IdEmpresa == null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value == 1)
                    info.EditarRegistro = true;
                else if (info.IdEmpresa == ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa)
                    info.EditarRegistro = true;

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAplicacaoTabelaReferencia, "AplicacaoTipo");
            }
            else
            {
                vm.Ativo = true;
                vm.EditarRegistro = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AplicacaoTipoInfo info = new AplicacaoTipoInfo();
            //info.IdEmpresa = IdEmpresa;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacaoTipoIn = vm.IdAplicacaoTipoIn;
            info.Descricao = vm.Descricao;
            info.DescricaoCompostaBusca = vm.DescricaoCompostaBusca;
            info.Ativo = vm.Ativo;

            ListPaged<AplicacaoTipoInfo> retorno = new ListPaged<AplicacaoTipoInfo>(AplicacaoTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            foreach (var item in retorno)
            {
                if (item.IdEmpresa != null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value != 1)
                {
                    item.EditarRegistro = true;
                }
                if (item.IdEmpresa == null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa == 1)
                {
                    item.EditarRegistro = true;
                }
            }

            return base.CreateListResult(vm, retorno, "Exportação:Tipo Aplicação", true);

        }

    }
}
