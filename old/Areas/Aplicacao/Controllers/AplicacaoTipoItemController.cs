﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoTipoItem;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class AplicacaoTipoItemController : ControllerExtended
    {

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AplicacaoTipoItemInfo info = new AplicacaoTipoItemInfo();

            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.UtilizaSubAplicacaoTipo = vm.UtilizaSubAplicacaoTipo;
            info.DescricaoSubAplicacao = vm.DescricaoSubAplicacao;
            info.DescricaoComposta = vm.DescricaoComposta;

            ListPaged<AplicacaoTipoItemInfo> retorno = new ListPaged<AplicacaoTipoItemInfo>(AplicacaoTipoItemBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Itens de Tipos de Aplicações ", true);
        }


    }
}
