﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.SubAplicacaoReferencia;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class SubAplicacaoReferenciaController : ControllerExtended
    {

        public SubAplicacaoReferenciaController()
        {

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            SubAplicacaoReferenciaInfo info = new SubAplicacaoReferenciaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdSubAplicacaoReferencia = vm.IdSubAplicacaoReferencia;
            info.DescricaoSubAplicacaoBusca = vm.DescricaoSubAplicacaoBusca;

            info.IdSubAplicacaoReferenciaIn = vm.IdSubAplicacaoReferenciaIn;
            info.IdAplicacaoTipoIn = vm.IdAplicacaoTipoIn;
            info.IdAplicacaoReferenciaIn = vm.IdAplicacaoReferenciaIn;
            info.IdSubAplicacaoReferenciaIn = vm.IdSubAplicacaoReferenciaIn;

            //Agricola
            info.IdProducaoUnidadeTipoIn = vm.IdProducaoUnidadeTipoIn;

            //Automotivo
            //TODO

            //Criação
            //TODO

            info.DescricaoBusca = vm.DescricaoBusca; //Busca GERAL

            //info.Descricao = vm.Descricao;
            //info.Ativo = vm.Ativo;

            ListPaged<SubAplicacaoReferenciaInfo> retorno = new ListPaged<SubAplicacaoReferenciaInfo>(SubAplicacaoReferenciaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Sub Aplicação", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListPorTipoAplicacao(ListVM vm)
        {
            SubAplicacaoReferenciaInfo info = new SubAplicacaoReferenciaInfo();
            info.IdEmpresa = IdEmpresa;
            info.DescricaoBusca = vm.DescricaoBusca; //Busca GERAL

            info.IdSubAplicacaoReferencia = vm.IdSubAplicacaoReferencia;
            info.DescricaoSubAplicacaoBusca = vm.DescricaoSubAplicacaoBusca;

            info.IdAplicacaoTipoIn = vm.IdAplicacaoTipoIn;
            info.IdAplicacaoReferenciaIn = vm.IdAplicacaoReferenciaIn;
            info.IdSubAplicacaoReferenciaIn = vm.IdSubAplicacaoReferenciaIn;

            //Itens Auxiliares
            //Agrícola
            info.IdProducaoUnidadeTipoIn = vm.IdProducaoUnidadeTipoIn;

            //Criação
            info.IdCriacaoCategoriaIn = vm.IdCriacaoCategoriaIn;

            //Automotivo
            info.IdAutomotivoParteEquipamentoIn = vm.IdAutomotivoParteEquipamentoIn;

            info.Ativo = vm.Ativo;
            ListPaged<SubAplicacaoReferenciaInfo> retorno = new ListPaged<SubAplicacaoReferenciaInfo>(SubAplicacaoReferenciaBll.Instance.ListPorTipoAplicacao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Sub Aplicação", true);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ListarPorId(int id)
        {
            var info = SubAplicacaoReferenciaBll.Instance.ListarPorCodigo(id);

            var response = SubAplicacaoReferenciaBll.Instance.ListarSubAplicacaoesPorTipo(new SubAplicacaoReferenciaInfo
            {
                IdEmpresa = info.IdEmpresa,
                IdAplicacaoTipoIn = info.IdAplicacaoTipo.Value.ToString(),
                IdAplicacaoTipo = info.IdAplicacaoTipo,
                IdAplicacaoReferencia = info.IdAplicacaoReferencia
            }).FirstOrDefault();

            return Json(new { Data = response }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ListarPorIds(string id)
        {
            var lst = SubAplicacaoReferenciaBll.Instance.ListarPorParametros(new SubAplicacaoReferenciaInfo { IdSubAplicacaoReferenciaIn = id });
            List<SubAplicacaoReferenciaInfo> response = new List<SubAplicacaoReferenciaInfo>();

            foreach (var item in lst)
            {
                response.Add(SubAplicacaoReferenciaBll.Instance.ListPorTipoAplicacao(new SubAplicacaoReferenciaInfo
                        {
                            IdEmpresa = item.IdEmpresa,
                            IdAplicacaoTipoIn = item.IdAplicacaoTipo.Value.ToString(),
                            IdSubAplicacaoReferencia = item.IdSubAplicacaoReferencia
                        }).FirstOrDefault());
            }


            return Json(new { Data = response }, JsonRequestBehavior.AllowGet);
        }

    }
}
