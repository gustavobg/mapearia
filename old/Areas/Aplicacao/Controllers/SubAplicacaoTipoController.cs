﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.SubAplicacaoTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class SubAplicacaoTipoController : ControllerExtended
    {
        public SubAplicacaoTipoController()
        {
            IndexUrl = "/Aplicacao/SubAplicacaoTipo/Index";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            SubAplicacaoTipoInfo info = new SubAplicacaoTipoInfo();
            info.IdSubAplicacaoTipo = vm.IdSubAplicacaoTipo;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            info.IdSubAplicacaoTipoIn = vm.IdSubAplicacaoTipoIn;
            info.IdSubAplicacaoTipoNotIn = vm.IdSubAplicacaoTipoNotIn;
            info.IdAplicacaoTipoIn = vm.IdAplicacaoTipoIn;

            ListPaged<SubAplicacaoTipoInfo> retorno = new ListPaged<SubAplicacaoTipoInfo>(SubAplicacaoTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Sub Aplicação", true);
        }

    }
}
