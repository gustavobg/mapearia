﻿using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.TabelaReferencia;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.Controllers
{
    public class TabelaReferenciaController : ControllerExtended
    {

        public TabelaReferenciaController()
        {
            IndexUrl = "Aplicacao/TabelaReferencia/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AplicacaoTabelaReferenciaInfo info = new AplicacaoTabelaReferenciaInfo();

            ViewModelToModelMapper.Map<AplicacaoTabelaReferenciaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            var response = AplicacaoTabelaReferenciaBll.Instance.Salvar(info);

            SalvarHistoricoAcesso(IndexUrl, vm.IdAplicacaoTabelaReferencia.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdAplicacaoTabelaReferencia, "AplicacaoTabelaReferencia", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                AplicacaoTabelaReferenciaInfo info = AplicacaoTabelaReferenciaBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAplicacaoTabelaReferencia, "AplicacaoTabelaReferencia");
            }
            else
            {
                vm.Ativo = true;
            }

            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AplicacaoTabelaReferenciaInfo info = new AplicacaoTabelaReferenciaInfo();
            info.IdAplicacaoTabelaReferencia = vm.IdAplicacaoTabelaReferencia;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<AplicacaoTabelaReferenciaInfo> retorno = new ListPaged<AplicacaoTabelaReferenciaInfo>(AplicacaoTabelaReferenciaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Tabela Referência", true);

        }
    }
}
