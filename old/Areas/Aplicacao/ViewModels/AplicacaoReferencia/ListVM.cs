﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoReferencia
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdAplicacaoReferencia { get; set; }
        public int? IdAplicacaoTipo { get; set; }
        public int? IdProducaoLocal { get; set; }

        public string DescricaoBusca { get; set; }

        //Aplicação
        public string IdAplicacaoTipoIn { get; set; }
        public string IdAplicacaoReferenciaIn { get; set; }

        //Agrícola
        public string IdProducaoLocalIn { get; set; }
        public string IdProducaoLocalTipoIn { get; set; }

        //Benfeitoria
        public string IdBenfeitoriaTipoIn { get;set; }
        public string IdBenfeitoriaIn { get; set; }

        //Automotivo
        public string IdAutomotivoTipoEquipamentoIn { get; set; }
        public string IdAutomotivoEquipamentoIn { get; set; }

        //Criação
        public string IdCriacaoTipoIn { get; set; }
        public string IdCriacaoLoteIn { get; set; }
        public string IdCriacaoAnimalIn { get; set; }


        public bool? Ativo { get;set; }

    }
}