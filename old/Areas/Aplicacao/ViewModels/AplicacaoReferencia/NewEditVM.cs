﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoReferencia
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            SubAplicacoes = new List<SubAplicacaoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoAplicacaoReferenciaExibicao { get; set; }

        #region Exibição

        [ViewModelToModelAttribute]
        public string DescricaoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoSubCategoria { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoBenfeitoria { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoProducaoLocal { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoCriacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoAutomotivoEquipamento { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<SubAplicacaoVM> SubAplicacoes { get; set; }

        public class SubAplicacaoVM
        {
            [ViewModelToModelAttribute]
            public int? IdSubAplicacaoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoSubAplicacaoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCategoria { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoSubCategoria { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }
        }
    }
}