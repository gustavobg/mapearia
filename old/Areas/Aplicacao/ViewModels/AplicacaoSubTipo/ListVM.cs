﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoSubTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdEmpresa { get; set; }
        public int? IdAplicacaoSubTipo { get; set; }
        public int? IdAplicacaoTipo { get; set; }

        public string DescricaoProducaoLocalTipo { get; set; }
        public string IdAplicacaoSubTipoIn { get; set; }

    }
}