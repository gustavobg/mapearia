﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoTipo
{
    public class ListVM:ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdAplicacaoTipo { get; set; }

        public string Descricao { get; set; }
        public string IdAplicacaoTipoIn { get; set; }
        public string DescricaoCompostaBusca { get; set; }

        public bool? Ativo { get; set; }

    }
}