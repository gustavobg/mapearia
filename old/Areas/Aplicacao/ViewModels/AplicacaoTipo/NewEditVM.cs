﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Itens = new List<VMAplicacaoTipoItem>();
        }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoTabelaReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTabelaReferenciaTipoAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? CentroCustoTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool? UtilizaCategoriaAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? EditarRegistro { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<VMAplicacaoTipoItem> Itens { get; set; }

        public class VMAplicacaoTipoItem
        {
            [ViewModelToModelAttribute]
            public int? IdAplicacaoTipoItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public bool? UtilizaSubAplicacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCategoria { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoSubAplicacao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoSubAplicacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoSubCategoria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoTabelaReferenciaCategoria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoTabelaReferenciaSubAplicacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoTabelaReferenciaSubAplicacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicacaoTabelaReferenciaSubCategoria { get; set; }

            [ViewModelToModelAttribute]
            public int? NomenclaturaSubAplicacao { get; set; }

            [ViewModelToModelAttribute]
            public int? CentroCustoTipo { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }
        }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTipo { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTabelaReferencia { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTabelaReferenciaSubAplicacao { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTabelaReferenciaCategoria { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTabelaReferenciaSubCategoria { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdAplicacaoTabelaReferenciaSubAplicacaoTipo { get; set; }

        //[ViewModelToModelAttribute]
        //public string Codigo { get; set; }

        //[ViewModelToModelAttribute]
        //public string Descricao { get; set; }

        //[ViewModelToModelAttribute]
        //public string DescricaoSubAplicacao { get; set; }

        //[ViewModelToModelAttribute]
        //public string DescricaoCategoria { get; set; }

        //[ViewModelToModelAttribute]
        //public string DescricaoSubCategoria { get; set; }

        //[ViewModelToModelAttribute]
        //public string DescricaoSubAplicacaoTipo { get; set; }

        //[ViewModelToModelAttribute]
        //public bool UtilizaSubAplicacaoTipo { get; set; }

        //[ViewModelToModelAttribute]
        //public bool? Ativo { get; set; }

        //[ViewModelToModelAttribute]
        //public Guid? ChaveVersao { get; set; }

    }
}
