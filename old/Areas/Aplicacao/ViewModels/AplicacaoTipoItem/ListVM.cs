﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.AplicacaoTipoItem
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM(){}

        public int? IdAplicacaoTipoItem { get; set; }
        public int? IdEmpresa { get; set; }

        public bool? Ativo { get; set; }
        public bool? UtilizaSubAplicacaoTipo { get; set; }

        public string DescricaoSubAplicacao { get; set; }
        public string DescricaoComposta { get; set; }
        
    }
}