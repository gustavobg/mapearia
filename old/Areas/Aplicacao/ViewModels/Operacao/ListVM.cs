﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.Operacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdAplicacaoOperacao { get; set; }

        public string Descricao { get; set; }
        public string IdModuloIn { get; set; }
    }
}