﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.SubAplicacaoReferencia
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }
        
        public int? IdEmpresa { get; set; }
        public int? IdSubAplicacaoReferencia { get; set; }

        public string DescricaoBusca { get; set; }

        public string IdSubAplicacaoReferenciaIn { get; set; }
        public string IdAplicacaoReferenciaIn { get; set; }
        public string IdAplicacaoTipoIn { get; set; }

        //Agrícola
        public string IdProducaoUnidadeTipoIn { get; set; }

        //Criação
        public string IdCriacaoCategoriaIn { get; set; }

        //Automotivo
        public string IdAutomotivoParteEquipamentoIn { get; set; }

        public string DescricaoSubAplicacaoBusca { get; set; }

        public bool? Ativo { get; set; }
    }
}