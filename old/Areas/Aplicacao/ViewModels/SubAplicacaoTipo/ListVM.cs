﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.SubAplicacaoTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdSubAplicacaoTipo { get; set; }
        public int? IdAplicacaoTipo { get;set;}

        public string IdSubAplicacaoTipoIn { get; set; }
        public string IdSubAplicacaoTipoNotIn { get; set; }
        public string IdAplicacaoTipoIn { get; set; }
        public string Descricao { get; set; }

        public bool? Ativo { get; set; }

    }
}