﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.TabelaReferencia
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdAplicacaoTabelaReferencia { get; set; }
        public string Descricao { get; set; }
        
        public string TABLE_NAME { get; set; }
        public string COLUMN_NAME { get; set; }

        public bool? Ativo { get; set; }
        
    }
}
