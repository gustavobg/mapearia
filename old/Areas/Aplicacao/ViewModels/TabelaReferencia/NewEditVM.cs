﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Aplicacao.ViewModels.TabelaReferencia
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoTabelaReferencia { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Tabela { get; set; }

        [ViewModelToModelAttribute]
        public string CampoChave { get; set; }

        [ViewModelToModelAttribute]
        public string CampoDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string TipoCampoChave { get; set; }

        [ViewModelToModelAttribute]
        public string TipoCampoDescricao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}
