﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo
{
    public class AutomotivoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Automotivo";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Automotivo_default",
                "Automotivo/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
