using HTM.MasterGestor.Bll.Automotivo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Automotivo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.CorEquipamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class CorEquipamentoController : ControllerExtended
    {
        public CorEquipamentoController()
        {
            IndexUrl = "/Automotivo/CorEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            return View(vm);
        }

        #region New Edit
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AutomotivoCorEquipamentoInfo info = AutomotivoCorEquipamentoBll.Instance.ListarPorParametros( new AutomotivoCorEquipamentoInfo { IdAutomotivoCorEquipamento = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAutomotivoCorEquipamento, "AutomotivoCorEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AutomotivoCorEquipamentoInfo info = new AutomotivoCorEquipamentoInfo();

            ViewModelToModelMapper.Map<AutomotivoCorEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = AutomotivoCorEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoCorEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoCorEquipamento, "AutomotivoCorEquipamento", response.Response.IdHistorico);

            return Json(response);
        }


        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AutomotivoCorEquipamentoEmpresaInfo info = new AutomotivoCorEquipamentoEmpresaInfo();
            AutomotivoCorEquipamentoEmpresaInfo response = new AutomotivoCorEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoCorEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoCorEquipamentoEmpresaInfo { IdAutomotivoCorEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AutomotivoCorEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoCorEquipamento, "AutomotivoCorEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AutomotivoCorEquipamentoEmpresaInfo info = new AutomotivoCorEquipamentoEmpresaInfo();
            AutomotivoCorEquipamentoEmpresaInfo response = new AutomotivoCorEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoCorEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoCorEquipamentoEmpresaInfo { IdAutomotivoCorEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AutomotivoCorEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoCorEquipamento, "AutomotivoCorEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [CustomAuthorize, JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult List(ListVM vm)
        {
            var info = new AutomotivoCorEquipamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.Descricao = vm.Descricao;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AutomotivoCorEquipamentoInfo> retorno = new ListPaged<AutomotivoCorEquipamentoInfo>(AutomotivoCorEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: AutomotivoCorEquipamento",true);
        }
    }
}