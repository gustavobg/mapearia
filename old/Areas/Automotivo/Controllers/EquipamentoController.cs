﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.Equipamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.ExtensionMethod;
using System.Configuration;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Automotivo;
using HTM.MasterGestor.Model.Automotivo;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class EquipamentoController : ControllerExtended
    {
        public EquipamentoController()
        {
            IndexUrl = "/Automotivo/Equipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult SalvarEquipamento(NewEditVM vm)
        {
            var info = new AutomotivoEquipamentoInfo();

            ViewModelToModelMapper.Map<AutomotivoEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            CommaSeparatedToList(vm.IdsPartes).ForEach(idParteEquipamento =>
            {
                info.PartesEquipamento.Add(new AutomotivoEquipamentoParteEquipamentoInfo()
                {
                    IdAutomotivoParteEquipamento = idParteEquipamento
                });
            });

            var response = AutomotivoEquipamentoBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoEquipamento, "AutomotivoEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AutomotivoEquipamentoInfo info = AutomotivoEquipamentoBll.Instance.ListarCompletoPorId(id.Value);
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                vm.IdsPartes = string.Join(",", info.PartesEquipamento.Select(x => x.IdAutomotivoParteEquipamento));

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAutomotivoEquipamento, "AutomotivoEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            //Busca parâmetro
            //var caracteristica = new CaracteristicaBLL().AdquirirCaracteristicaComValor(new DTO.Caracteristica.DTOAdquirirCaracteristica { Codigo = "Centro_Custo_Obrigatorio" }, IdEmpresa: IdEmpresa).FirstOrDefault();
            //if (caracteristica != null)
            //    if (!string.IsNullOrEmpty(caracteristica.CaracteristicaValor.Valor))
            //        vm.CentroCustoObrigatorio = Convert.ToBoolean(caracteristica.CaracteristicaValor.Valor);

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new AutomotivoEquipamentoInfo();
            info.Ativo = vm.Ativo;
            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoTipoEquipamentoIn = vm.IdTipoIn;
            info.IdProdutoServicoMarcaIn = vm.IdMarcaIn;
            info.IdAutomotivoModeloEquipamentoIn = vm.IdModeloIn;
            info.IdAutomotivoSituacaoEquipamentoIn = vm.IdSituacaoIn;
            info.IdAutomotivoEquipamentoProprietarioIn = vm.IdProprietarioIn;
            info.BuscaDetalhada = vm.BuscaDetalhada;

            ListPaged<AutomotivoEquipamentoInfo> retorno = new ListPaged<AutomotivoEquipamentoInfo>(AutomotivoEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: AutomotivoEquipamento", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListaPersonalizada(ListVM vm)
        {
            var info = new AutomotivoEquipamentoInfo();
            info.Ativo = vm.Ativo;
            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoTipoEquipamentoIn = vm.IdTipoIn;
            info.IdProdutoServicoMarcaIn = vm.IdMarcaIn;
            info.IdAutomotivoModeloEquipamentoIn = vm.IdModeloIn;
            info.IdAutomotivoSituacaoEquipamentoIn = vm.IdSituacaoIn;
            info.IdPessoaIn = vm.IdProprietarioIn;
            info.BuscaDetalhada = vm.BuscaDetalhada;

            #region Tipo de Equipamento
            //Busca Personalizada
            //1 - Buscar os tipos de Equipamentos
            //1.1 - Descobrir a Arvore que cada tipo pertence
            string idsBuscaTipoEquipamento = string.Empty;
            if (!string.IsNullOrEmpty(vm.IdTipoInBuscaSeparada))
            {
                string[] idsTipo = vm.IdTipoInBuscaSeparada.Split(',');

                List<AutomotivoTipoEquipamentoInfo> lstTiposEquipamento = new List<AutomotivoTipoEquipamentoInfo>();
                foreach (var item in idsTipo)
                {
                    //lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamento = int.Parse(item), IdEmpresa = IdEmpresa }));
                    //lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { ArvoreRight = "%." + item + ".%", IdEmpresa = IdEmpresa }));
                    var tipoEquipamentoInfo = AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamento = int.Parse(item), IdEmpresa = IdEmpresa }).FirstOrDefault();
                    if (tipoEquipamentoInfo != null && tipoEquipamentoInfo.IdAutomotivoTipoEquipamento.HasValue)
                    {
                        lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { ArvoreRight = tipoEquipamentoInfo.Arvore + ".%", IdEmpresa = IdEmpresa }));
                        lstTiposEquipamento.Add(tipoEquipamentoInfo);
                    }

                }

                List<AutomotivoTipoEquipamentoInfo> lstItemAItem = new List<AutomotivoTipoEquipamentoInfo>();
                if (lstTiposEquipamento.Count() > 0)
                {
                    foreach (AutomotivoTipoEquipamentoInfo item in lstTiposEquipamento)
                    {
                        string[] arvore = item.Arvore.Split('.');
                        string idsBusca = string.Empty;
                        bool entraif = false;
                        foreach (string arvoreItem in arvore)
                        {
                            if (int.Parse(arvoreItem) == item.IdAutomotivoTipoEquipamento || entraif == true)
                            {
                                idsBusca += arvoreItem + ",";
                                entraif = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(idsBusca) && idsBusca.Substring(idsBusca.Length - 1) == ",")
                            idsBusca = idsBusca.Remove(idsBusca.Length - 1);
                        lstItemAItem.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarPorParametros(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamentoIn = idsBusca, IdEmpresa = IdEmpresa }));
                    }
                    if (lstItemAItem.Count > 0)
                    {
                        var result = lstItemAItem.Where(p => p.IdAutomotivoTipoEquipamento != null).GroupBy(p => p.IdAutomotivoTipoEquipamento).Select(grp => grp.First()).ToList();
                        idsBuscaTipoEquipamento = string.Join(",", result.Select(p => p.IdAutomotivoTipoEquipamento));
                    }
                }

            }
            if (!string.IsNullOrEmpty(idsBuscaTipoEquipamento))
                info.IdAutomotivoTipoEquipamentoIn = idsBuscaTipoEquipamento;

            #endregion

            ListPaged<AutomotivoEquipamentoInfo> retorno = new ListPaged<AutomotivoEquipamentoInfo>(AutomotivoEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            if (retorno.Count > 0)
            {
                string idsTipoEquipamentos = string.Join(",", retorno.Select(p => p.IdAutomotivoTipoEquipamento));

                var lstTipo = AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamentoIn = idsTipoEquipamentos,IdEmpresa = IdEmpresa });

                foreach (var tipo in lstTipo)
                {
                    foreach (var equipamento in retorno)
                        if (tipo.IdAutomotivoTipoEquipamento == equipamento.IdAutomotivoTipoEquipamento)
                            equipamento.DescricaoCompletaTipoEquipamento = tipo.DescricaoCompleta;
                }

            }

            return base.CreateListResult(vm, retorno, "Exportação: Equipamentos", true);
        }

        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult Pessoas(ListVM vm)
        //{
        //    DTOAdquirirPessoa dto = new DTOAdquirirPessoa();
        //    dto.IdEmpresa = IdEmpresa;
        //    dto.Page = vm.Page;
        //    dto.Nome = vm.Nome;
        //    dto.NomeSecundario = vm.NomeSecundario;
        //    dto.Ativo = vm.PessoaAtivo;
        //    dto.Corporacao = false; //Apenas para retornar Pessoas

        //    ListPaged<Pessoa> retorno = new PessoaBLL().AdquirirPessoa(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: Pessoa");
        //}

    }
}
