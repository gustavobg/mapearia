﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.EquipamentoProprietario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Automotivo;
using HTM.MasterGestor.Bll.Automotivo;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class EquipamentoProprietarioController : ControllerExtended
    {
        public EquipamentoProprietarioController()
        {
            IndexUrl = "/Automotivo/EquipamentoProprietario/Index";
        }

        //private AutomotivoEquipamentoProprietarioBLL bll = new AutomotivoEquipamentoProprietarioBLL();

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarAutomotivoEquipamentoProprietario dto = new DTOSalvarAutomotivoEquipamentoProprietario();
        //    ViewModelToModelMapper.Map<AutomotivoEquipamentoProprietario>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = dto.Entidade.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarAutomotivoEquipamentoProprietarioResponse response = bll.SalvarAutomotivoEquipamentoProprietario(dto);
        //    if (response.Sucesso)
        //        SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoEquipamentoProprietario.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoEquipamentoProprietario, "AutomotivoEquipamentoProprietario", response.IdHistorico);

        //    return Json(response);
        //}

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            //if (id.HasValue && id > 0)
            //{
            //    AutomotivoEquipamentoProprietario entidade = bll.AdquirirAutomotivoEquipamentoProprietario(new DTOAdquirirAutomotivoEquipamentoProprietario() { IdAutomotivoEquipamentoProprietario = id }).FirstOrDefault();
            //    ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

            //  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdAutomotivoEquipamentoProprietario, "AutomotivoEquipamentoProprietario");
            //}
            //else
            //{
            //    vm.Ativo = true;
            //}

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AutomotivoEquipamentoProprietarioInfo info = new AutomotivoEquipamentoProprietarioInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaIn = vm.IdPessoaIn;


            ListPaged<AutomotivoEquipamentoProprietarioInfo> retorno = new ListPaged<AutomotivoEquipamentoProprietarioInfo>(AutomotivoEquipamentoProprietarioBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Proprietários Equipamento", true);
        }
    }
}
