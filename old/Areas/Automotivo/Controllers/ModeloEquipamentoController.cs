using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.ModeloEquipamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Automotivo;
using HTM.MasterGestor.Model.Automotivo;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class ModeloEquipamentoController : ControllerExtended
    {
        public ModeloEquipamentoController()
        {
            IndexUrl = "/Automotivo/ModeloEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [CustomAuthorize, HttpPost]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AutomotivoModeloEquipamentoInfo info = AutomotivoModeloEquipamentoBll.Instance.ListarCompletoPorId(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                vm.IdsAutomotivoParteEquipamento = string.Join(",", info.PartesEquipamento.Select(p => p.IdAutomotivoParteEquipamento));

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAutomotivoModeloEquipamento, "AutomotivoModeloEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [CustomAuthorize, HttpGet]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [CustomAuthorize, HttpPost, ValidateJsonAntiForgeryToken]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new AutomotivoModeloEquipamentoInfo();

            if (vm.IdAutomotivoModeloEquipamento.HasValue && vm.IdAutomotivoModeloEquipamento.Value > 0)
            {
                info = AutomotivoModeloEquipamentoBll.Instance.ListarPorCodigo(vm.IdAutomotivoModeloEquipamento.Value);
            }

            ViewModelToModelMapper.Map<AutomotivoModeloEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            CommaSeparatedToList(vm.IdsAutomotivoParteEquipamento).ForEach(x =>
            {
                info.PartesEquipamento.Add(new AutomotivoModeloEquipamentoAutomotivoParteEquipamentoInfo()
                {
                    IdAutomotivoParteEquipamento = x
                });
            });

            var response = AutomotivoModeloEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoModeloEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoModeloEquipamento, "AutomotivoModeloEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AutomotivoModeloEquipamentoEmpresaInfo info = new AutomotivoModeloEquipamentoEmpresaInfo();
            AutomotivoModeloEquipamentoEmpresaInfo response = new AutomotivoModeloEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoModeloEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoModeloEquipamentoEmpresaInfo { IdAutomotivoModeloEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AutomotivoModeloEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoModeloEquipamento, "AutomotivoModeloEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AutomotivoModeloEquipamentoEmpresaInfo info = new AutomotivoModeloEquipamentoEmpresaInfo();
            AutomotivoModeloEquipamentoEmpresaInfo response = new AutomotivoModeloEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoModeloEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoModeloEquipamentoEmpresaInfo { IdAutomotivoModeloEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AutomotivoModeloEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoModeloEquipamento, "AutomotivoModeloEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new AutomotivoModeloEquipamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.Descricao = vm.Descricao;
            info.IdAutomotivoModeloEquipamentoIn = vm.IdAutomotivoModeloEquipamentoIn;
            info.IdProdutoServicoMarcaIn = vm.IdProdutoServicoMarcaIn;
            info.IdAutomotivoModeloEquipamento = vm.IdAutomotivoModeloEquipamento;
            info.DescricaoCompostaBusca = vm.DescricaoComposta;
            info.IdAutomotivoTipoEquipamentoIn = vm.IdAutomotivoTipoEquipamentoIn;
            info.RegistroProprio = vm.RegistroProprio;

            #region Tipo de Equipamento
            //Busca Personalizada
            //1 - Buscar os tipos de Equipamentos
            //1.1 - Descobrir a Arvore que cada tipo pertence
            string idsBuscaTipoEquipamento = string.Empty;
            if (!string.IsNullOrEmpty(vm.IdTipoInBuscaSeparada))
            {
                string[] idsTipo = vm.IdTipoInBuscaSeparada.Split(',');

                List<AutomotivoTipoEquipamentoInfo> lstTiposEquipamento = new List<AutomotivoTipoEquipamentoInfo>();
                foreach (var item in idsTipo)
                {
                    //lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamento = int.Parse(item), IdEmpresa = IdEmpresa }));
                    //lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { ArvoreRight = "%." + item + ".%", IdEmpresa = IdEmpresa }));
                    var tipoEquipamentoInfo = AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamento = int.Parse(item), IdEmpresa = IdEmpresa }).FirstOrDefault();
                    if (tipoEquipamentoInfo != null && tipoEquipamentoInfo.IdAutomotivoTipoEquipamento.HasValue)
                    {
                        lstTiposEquipamento.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { ArvoreRight = tipoEquipamentoInfo.Arvore + ".%", IdEmpresa = IdEmpresa }));
                        lstTiposEquipamento.Add(tipoEquipamentoInfo);
                    }

                }

                List<AutomotivoTipoEquipamentoInfo> lstItemAItem = new List<AutomotivoTipoEquipamentoInfo>();
                if (lstTiposEquipamento.Count() > 0)
                {
                    foreach (AutomotivoTipoEquipamentoInfo item in lstTiposEquipamento)
                    {
                        string[] arvore = item.Arvore.Split('.');
                        string idsBusca = string.Empty;
                        bool entraif = false;
                        foreach (string arvoreItem in arvore)
                        {
                            if (int.Parse(arvoreItem) == item.IdAutomotivoTipoEquipamento || entraif == true)
                            {
                                idsBusca += arvoreItem + ",";
                                entraif = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(idsBusca) && idsBusca.Substring(idsBusca.Length - 1) == ",")
                            idsBusca = idsBusca.Remove(idsBusca.Length - 1);
                        lstItemAItem.AddRange(AutomotivoTipoEquipamentoBll.Instance.ListarPorParametros(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamentoIn = idsBusca, IdEmpresa = IdEmpresa }));
                    }
                    if (lstItemAItem.Count > 0)
                    {
                        var result = lstItemAItem.Where(p => p.IdAutomotivoTipoEquipamento != null).GroupBy(p => p.IdAutomotivoTipoEquipamento).Select(grp => grp.First()).ToList();
                        idsBuscaTipoEquipamento = string.Join(",", result.Select(p => p.IdAutomotivoTipoEquipamento));
                    }
                }

            }
            if (!string.IsNullOrEmpty(idsBuscaTipoEquipamento))
                info.IdAutomotivoTipoEquipamentoIn = idsBuscaTipoEquipamento;

            #endregion

            var retorno = new ListPaged<AutomotivoModeloEquipamentoInfo>(AutomotivoModeloEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            if (retorno.Count() > 0)
            {
                string idsTipo = string.Join(",", retorno.Select(p => p.IdAutomotivoTipoEquipamento));

                var lstTipoEquipamento = AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(new AutomotivoTipoEquipamentoInfo { IdAutomotivoTipoEquipamentoIn = idsTipo, IdEmpresa = IdEmpresa });

                foreach (var item in retorno)
                {
                    foreach (var tipo in lstTipoEquipamento)
                    {
                        if (item.IdAutomotivoTipoEquipamento == tipo.IdAutomotivoTipoEquipamento)
                        {
                            item.DescricaoCompletaAutomotivoTipoEquipamento = tipo.DescricaoCompleta;
                        }
                    }
                }
            }

            return base.CreateListResult(vm, retorno, "Exportação: Automotivo - Modelo de Equipamento", true);
        }
    }
}
