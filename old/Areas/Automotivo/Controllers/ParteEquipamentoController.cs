using HTM.MasterGestor.Bll.Automotivo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Automotivo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.ParteEquipamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class ParteEquipamentoController : ControllerExtended
    {
        public ParteEquipamentoController()
        {
            IndexUrl = "/Automotivo/ParteEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [CustomAuthorize]
        [HttpPost]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AutomotivoParteEquipamentoInfo entidade = AutomotivoParteEquipamentoBll.Instance.ListarPorParametros(new AutomotivoParteEquipamentoInfo { IdAutomotivoParteEquipamento = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdAutomotivoParteEquipamento, "AutomotivoParteEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [CustomAuthorize]
        [HttpGet]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [CustomAuthorize, HttpPost, ValidateJsonAntiForgeryToken]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new AutomotivoParteEquipamentoInfo();

            if (vm.IdAutomotivoParteEquipamento.HasValue && vm.IdAutomotivoParteEquipamento.Value > 0)
            {
                info = AutomotivoParteEquipamentoBll.Instance.ListarPorCodigo(vm.IdAutomotivoParteEquipamento.Value);
            }

            ViewModelToModelMapper.Map<AutomotivoParteEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = AutomotivoParteEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoParteEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoParteEquipamento, "AutomotivoParteEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AutomotivoParteEquipamentoEmpresaInfo info = new AutomotivoParteEquipamentoEmpresaInfo();
            AutomotivoParteEquipamentoEmpresaInfo response = new AutomotivoParteEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoParteEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoParteEquipamentoEmpresaInfo { IdAutomotivoParteEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AutomotivoParteEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoParteEquipamento, "AutomotivoParteEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AutomotivoParteEquipamentoEmpresaInfo info = new AutomotivoParteEquipamentoEmpresaInfo();
            AutomotivoParteEquipamentoEmpresaInfo response = new AutomotivoParteEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoParteEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoParteEquipamentoEmpresaInfo { IdAutomotivoParteEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AutomotivoParteEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoParteEquipamento, "AutomotivoParteEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [CustomAuthorize]
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult List(ListVM vm)
        {
            var info = new AutomotivoParteEquipamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoParteEquipamento = vm.IdAutomotivoParteEquipamento;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdAutomotivoParteEquipamentoIn = vm.IdAutomotivoParteEquipamentoIN;

            var retorno = new ListPaged<AutomotivoParteEquipamentoInfo>(AutomotivoParteEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Automotivo - Parte de Equipamento",true);
        }

        [HttpPost]
        public string RetornaParteEquipamentoPorModelo(int idModelo)
        {
            //string idsParte = string.Empty;
            //DTOAdquirirAutomotivoModeloEquipamentoAutomotivoParteEquipamento dto = new DTOAdquirirAutomotivoModeloEquipamentoAutomotivoParteEquipamento();
            //dto.IdAutomotivoModeloEquipamento = idModelo;

            //ListPaged<AutomotivoModeloEquipamentoAutomotivoParteEquipamento> retorno = new AutomotivoModeloEquipamentoAutomotivoParteEquipamentoBLL().AdquirirAutomotivoModeloEquipamentoAutomotivoParteEquipamento(dto);

            //if (retorno.Count == 0)
            //{
            //    return string.Empty;
            //}
            //else
            //{
            //    return string.Join(",", retorno.Select(x => x.IdAutomotivoParteEquipamento));
            //}

            return string.Empty;
        }
    }
}
