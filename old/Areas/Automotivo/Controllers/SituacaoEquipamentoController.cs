using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.SituacaoEquipamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Automotivo;
using HTM.MasterGestor.Bll.Automotivo;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class SituacaoEquipamentoController : ControllerExtended
    {
        public SituacaoEquipamentoController()
        {
            IndexUrl = "/Automotivo/SituacaoEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        [HttpPost, ValidateJsonAntiForgeryToken]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new AutomotivoSituacaoEquipamentoInfo();
            ViewModelToModelMapper.Map<AutomotivoSituacaoEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = AutomotivoSituacaoEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoSituacaoEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoSituacaoEquipamento, "AutomotivoSituacaoEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AutomotivoSituacaoEquipamentoInfo entidade = AutomotivoSituacaoEquipamentoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdAutomotivoSituacaoEquipamento, "AutomotivoSituacaoEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AutomotivoSituacaoEquipamentoEmpresaInfo info = new AutomotivoSituacaoEquipamentoEmpresaInfo();
            AutomotivoSituacaoEquipamentoEmpresaInfo response = new AutomotivoSituacaoEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoSituacaoEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoSituacaoEquipamentoEmpresaInfo { IdAutomotivoSituacaoEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AutomotivoSituacaoEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoSituacaoEquipamento , "AutomotivoSituacaoEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AutomotivoSituacaoEquipamentoEmpresaInfo info = new AutomotivoSituacaoEquipamentoEmpresaInfo();
            AutomotivoSituacaoEquipamentoEmpresaInfo response = new AutomotivoSituacaoEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoSituacaoEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoSituacaoEquipamentoEmpresaInfo { IdAutomotivoSituacaoEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AutomotivoSituacaoEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoSituacaoEquipamento, "AutomotivoSituacaoEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new AutomotivoSituacaoEquipamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoSituacaoEquipamento = vm.IdAutomotivoSituacaoEquipamento;
            info.Ativo = vm.Ativo;
            info.Descricao = vm.Descricao;
            info.RegistroProprio = vm.RegistroProprio;

            var retorno = new ListPaged<AutomotivoSituacaoEquipamentoInfo>(AutomotivoSituacaoEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Situação de Equipamentos",true);
        }

    }
}
