using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.SubTipoEquipamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class SubTipoEquipamentoController : ControllerExtended
    {
        //private AutomotivoSubTipoEquipamentoBLL bll = new AutomotivoSubTipoEquipamentoBLL();

        public SubTipoEquipamentoController()
        {
            IndexUrl = "/Automotivo/SubTipoEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        //[CustomAuthorize, HttpPost, ValidateJsonAntiForgeryToken]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarAutomotivoSubTipoEquipamento dto = new DTOSalvarAutomotivoSubTipoEquipamento();
        //    ViewModelToModelMapper.Map<AutomotivoSubTipoEquipamento>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa;

        //    DTOSalvarAutomotivoSubTipoEquipamentoResponse response = bll.SalvarAutomotivoSubTipoEquipamento(dto);

        //    if (response.Sucesso)
        //        SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoSubTipoEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoSubTipoEquipamento, dto.Entidade.ToString(), response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpPost, CustomAuthorize]
        //public ActionResult NewEditJson(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        AutomotivoSubTipoEquipamento entidade = bll.AdquirirAutomotivoSubTipoEquipamento(new DTOAdquirirAutomotivoSubTipoEquipamento() { IdAutomotivoSubTipoEquipamento = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdAutomotivoSubTipoEquipamento, entidade.ToString());
        //    }
        //    else
        //    {
        //        vm.Ativo = true;
        //    }

        //    return Json(vm);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();
        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirAutomotivoSubTipoEquipamento dto = new DTOAdquirirAutomotivoSubTipoEquipamento();
        //    dto.Page = vm.Page;
        //    dto.Ativo = vm.Ativo;
        //    dto.Descricao = vm.Descricao;

        //    if (!string.IsNullOrEmpty(vm.IdAutomotivoTipoEquipamentoIN))
        //    {
        //        dto.IdAutomotivoTipoEquipamentoIN = vm.IdAutomotivoTipoEquipamentoIN.Replace(" ", "").Split(',').Select(x => int.Parse(x)).ToList();
        //    }

        //    ListPaged<AutomotivoSubTipoEquipamento> retorno = bll.AdquirirAutomotivoSubTipoEquipamento(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: Automotivo - Sub-Tipo de Equipamento");
        //}

    }
}
