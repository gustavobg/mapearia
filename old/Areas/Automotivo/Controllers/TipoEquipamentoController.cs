using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.TipoEquipamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Automotivo;
using HTM.MasterGestor.Bll.Automotivo;
using HTM.MasterGestor.Bll.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.Controllers
{
    public class TipoEquipamentoController : ControllerExtended
    {
        public TipoEquipamentoController()
        {
            IndexUrl = "/Automotivo/TipoEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AutomotivoTipoEquipamentoInfo info = new AutomotivoTipoEquipamentoInfo();
            ViewModelToModelMapper.Map<AutomotivoTipoEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = info.IdEmpresa = IdEmpresa;

            var response = AutomotivoTipoEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAutomotivoTipoEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAutomotivoTipoEquipamento, "AutomotivoTipoEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            AutomotivoTipoEquipamentoInfo info = new AutomotivoTipoEquipamentoInfo();

            if (id.HasValue && id > 0)
            {
                info = AutomotivoTipoEquipamentoBll.Instance.ListarPorParametros(new AutomotivoTipoEquipamentoInfo { IdEmpresa = IdEmpresa, IdAutomotivoTipoEquipamento = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAutomotivoTipoEquipamento, "AutomotivoTipoEquipamento");
            }
            else
            {
                vm.Ativo = true;
            }

            //Busca parâmetro
            //var caracteristica = CaracteristicaBll.Instance. AdquirirCaracteristicaComValor(new DTO.Caracteristica.DTOAdquirirCaracteristica { Codigo = "Centro_Custo_Obrigatorio" }, IdEmpresa: IdEmpresa).FirstOrDefault();
            //if (caracteristica != null)
            //    if (!string.IsNullOrEmpty(caracteristica.CaracteristicaValor.Valor))
            //        vm.CentroCustoObrigatorio = Convert.ToBoolean(caracteristica.CaracteristicaValor.Valor);

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AutomotivoTipoEquipamentoEmpresaInfo info = new AutomotivoTipoEquipamentoEmpresaInfo();
            AutomotivoTipoEquipamentoEmpresaInfo response = new AutomotivoTipoEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoTipoEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoTipoEquipamentoEmpresaInfo { IdAutomotivoTipoEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AutomotivoTipoEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoTipoEquipamento, "AutomotivoTipoEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AutomotivoTipoEquipamentoEmpresaInfo info = new AutomotivoTipoEquipamentoEmpresaInfo();
            AutomotivoTipoEquipamentoEmpresaInfo response = new AutomotivoTipoEquipamentoEmpresaInfo();

            if (id > 0)
            {
                info = AutomotivoTipoEquipamentoEmpresaBll.Instance.ListarPorParametros(new AutomotivoTipoEquipamentoEmpresaInfo { IdAutomotivoTipoEquipamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AutomotivoTipoEquipamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAutomotivoTipoEquipamentoEmpresa, "AutomotivoTipoEquipamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }
        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AutomotivoTipoEquipamentoInfo info = new AutomotivoTipoEquipamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoTipoEquipamento = vm.IdAutomotivoTipoEquipamento;
            info.Descricao = vm.Descricao;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.IdAutomotivoTipoEquipamentoIn = vm.IdAutomotivoTipoEquipamentoIn;
            info.IdAutomotivoTipoEquipamentoPaiIn = vm.IdAutomotivoTipoEquipamentoPaiIn;
            info.IdAutomotivoTipoEquipamentoNotIn = vm.IdAutomotivoTipoEquipamentoNotIn;
            info.UltimoNivel = vm.UltimoNivel;
            info.Nivel = vm.Nivel;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AutomotivoTipoEquipamentoInfo> retorno = new ListPaged<AutomotivoTipoEquipamentoInfo>(AutomotivoTipoEquipamentoBll.Instance.ListarHierarquicamente(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Automotivo - Tipo de Equipamento", true);
        }

    }
}
