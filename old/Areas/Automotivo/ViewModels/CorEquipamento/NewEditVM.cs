using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.CorEquipamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

		[ViewModelToModelAttribute]
		public int? IdAutomotivoCorEquipamento { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
		public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }


    }
}