﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.Equipamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public string Descricao { get; set; }
        public string IdTipoIn { get; set;}
        public string IdMarcaIn { get; set; }
        public string IdModeloIn { get; set; }
        public string IdSituacaoIn { get; set; }
        public string IdProprietarioIn { get; set; }
        public string BuscaDetalhada { get; set; }

        public bool? Ativo { get; set; }

        //Pessoa
        public string Nome { get; set; }
        public string NomeSecundario { get; set; }
        public string IdTipoInBuscaSeparada { get; set; }

        public bool? PessoaAtivo { get; set; }

    }
}