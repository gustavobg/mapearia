﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.Equipamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Proprietarios = new List<EquipamentoProprietarioVM>();
            ArquivosDigitais = new List<ArquivoDigital>();
            Pessoa = new NewVM();
        }

        public NewVM Pessoa
        {
            get;
            set;
        }

        [ViewModelToModelAttribute]
        public Int32? IdAutomotivoEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdAutomotivoTipoEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdAutomotivoModeloEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdAutomotivoSituacaoEquipamento { get; set; }

        //[ViewModelToModelAttribute]
        //public Int32 IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public String Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string AnoModelo { get; set; }

        [ViewModelToModelAttribute]
        public int? AnoFabricacao { get; set; }

        [ViewModelToModelAttribute]
        public String Placa { get; set; }

        [ViewModelToModelAttribute]
        public String Chassis { get; set; }

        [ViewModelToModelAttribute]
        public String Renavam { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataCompra { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int Nomenclatura { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool CentroCustoObrigatorio { get; set; }

        public string IdsPartes { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EquipamentoProprietarioVM> Proprietarios { get; set; }

        public class EquipamentoProprietarioVM
        {
            [ViewModelToModelAttribute]
            public int? IdAutomotivoEquipamentoProprietario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Cota { get; set; }

            [ViewModelToModelAttribute]
            public bool Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomePessoa { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }
        }
    }
}