﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.EquipamentoProprietario
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdAutomotivoEquipamentoProprietario { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Decimal Cota { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }


    }
}