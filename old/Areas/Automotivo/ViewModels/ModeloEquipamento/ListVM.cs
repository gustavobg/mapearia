using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.ModeloEquipamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdAutomotivoModeloEquipamento { get; set; }
        public int? IdAutomotivoTipoEquipamento { get; set; }

        public string Sigla { get; set; }
        public string Descricao { get; set; }
        
        public string IdAutomotivoModeloEquipamentoIn { get; set; }
        public string IdProdutoServicoMarcaIn { get; set; }
        public string DescricaoComposta { get; set; }
        public string IdAutomotivoTipoEquipamentoIn { get; set; }
        public string IdTipoInBuscaSeparada { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}