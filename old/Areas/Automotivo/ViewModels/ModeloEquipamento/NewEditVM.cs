using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.ModeloEquipamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
		public int? IdAutomotivoModeloEquipamento { get; set; }

        [ViewModelToModelAttribute]
		public int IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAutomotivoTipoEquipamento { get; set; }

        [ViewModelToModelAttribute]
		public Boolean Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? Motorizado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeMedidor { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoReduzida { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
		public String Descricao { get; set; }

        [ViewModelToModelAttribute]
		public int? IdProdutoServicoMarca { get; set; }

        public string IdsAutomotivoParteEquipamento { get; set; }
    }
}