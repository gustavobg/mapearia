using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.SituacaoEquipamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

		[ViewModelToModelAttribute]
		public int? IdAutomotivoSituacaoEquipamento { get; set; }

        [ViewModelToModelAttribute]
		public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean SituacaoEquipamento { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
		public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }


    }
}