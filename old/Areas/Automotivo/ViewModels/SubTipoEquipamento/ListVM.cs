using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.SubTipoEquipamento
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public string Descricao { get; set; }
        public bool? Ativo { get; set; }
        public string IdAutomotivoTipoEquipamentoIN { get; set; }
    }
}