using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.TipoEquipamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdAutomotivoTipoEquipamento { get; set; }
        public int? IdEmpresa { get; set; }

        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string IdAutomotivoTipoEquipamentoIn { get; set; }
        public string IdAutomotivoTipoEquipamentoNotIn { get; set; }
        public string IdAutomotivoTipoEquipamentoPaiIn { get; set; }

        public int? IdGrupoExcecao { get; set; }
        public int? Nivel { get; set; }

        public bool? ComPai { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComHierarquia { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? SemRelacionamentoComProduto { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
