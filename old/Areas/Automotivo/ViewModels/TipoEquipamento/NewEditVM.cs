using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Automotivo.ViewModels.TipoEquipamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
        }

		[ViewModelToModelAttribute]
		public int? IdAutomotivoTipoEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAutomotivoTipoEquipamentoPai { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool CentroCustoObrigatorio { get; set; }
    }
}