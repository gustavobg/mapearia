﻿using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Caracteristica;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class CaracteristicaController : ControllerExtended
    {
        public CaracteristicaController()
        {
            IndexUrl = "/Configuracao/Caracteristica/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CaracteristicaInfo info = new CaracteristicaInfo();

            ViewModelToModelMapper.Map<CaracteristicaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TipoCaracteristica = (int)TipoCaracteristicaEnum.CampoPersonalizado;

            var response = CaracteristicaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            SalvarHistoricoAcesso(IndexUrl, vm.IdCaracteristica.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCaracteristica, "Caracteristica", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                CaracteristicaInfo info = new CaracteristicaInfo();
                info = CaracteristicaBll.Instance.ListarCaracteristicaComOpcoes(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCaracteristica, "Caracteristica");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CaracteristicaInfo info = new CaracteristicaInfo();
            info.IdCaracteristicaTabela = vm.IdCaracteristicaTabela;
            info.IdCaracteristicaTipo = vm.IdCaracteristicaTipo;
            info.IdModuloIn = vm.IdModuloIn;
            info.TipoCaracteristica = (int)TipoCaracteristicaEnum.CampoPersonalizado;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<CaracteristicaInfo> retorno = new ListPaged<CaracteristicaInfo>(CaracteristicaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Campo Personalizado", true);
        }

    }
}
