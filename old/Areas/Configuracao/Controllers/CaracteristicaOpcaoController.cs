﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaOpcao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class CaracteristicaOpcaoController : ControllerExtended
    {
        public CaracteristicaOpcaoController()
        {
            IndexUrl = "/Configuracao/CaracteristicaOpcao/Index";
        }

        //private CaracteristicaOpcaoBLL bll = new CaracteristicaOpcaoBLL();

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarCaracteristicaOpcao dto = new DTOSalvarCaracteristicaOpcao();
        //    ViewModelToModelMapper.Map<CaracteristicaOpcao>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarCaracteristicaOpcaoResponse response = bll.SalvarCaracteristicaOpcao(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdCaracteristicaOpcao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCaracteristicaOpcao, dto.Entidade.ToString(), response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        CaracteristicaOpcao entidade = bll.AdquirirCaracteristicaOpcao(new DTOAdquirirCaracteristicaOpcao() { IdCaracteristicaOpcao = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdCaracteristicaOpcao, entidade.ToString());
        //    }

        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirCaracteristicaOpcao dto = new DTOAdquirirCaracteristicaOpcao();
        //    dto.Page = vm.Page;
        //    dto.IdCaracteristicaOpcao = vm.IdCaracteristicaOpcao;
        //    dto.IdCaracteristica = vm.IdCaracteristica;
        //    dto.Valor = vm.Valor;

        //    ListPaged<CaracteristicaOpcao> retorno = bll.AdquirirCaracteristicaOpcao(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: CaracteristicaOpcao");
        //}

    }
}
