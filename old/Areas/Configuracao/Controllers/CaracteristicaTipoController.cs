﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaTipo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class CaracteristicaTipoController : ControllerExtended
    {
        public CaracteristicaTipoController()
        {
            IndexUrl = "/Configuracao/CaracteristicaTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CaracteristicaTipoInfo info = new CaracteristicaTipoInfo();
            ViewModelToModelMapper.Map<CaracteristicaTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = CaracteristicaTipoBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCaracteristicaTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCaracteristicaTipo, "CaracteristicaTipo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                CaracteristicaTipoInfo info = new CaracteristicaTipoInfo();
                info = CaracteristicaTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCaracteristicaTipo, "CaracteristicaTipo");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CaracteristicaTipoInfo info = new CaracteristicaTipoInfo();
            info.IdCaracteristicaTipo = vm.IdCaracteristicaTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<CaracteristicaTipoInfo> retorno = new ListPaged<CaracteristicaTipoInfo>(CaracteristicaTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Característica", true);
        }
    }
}
