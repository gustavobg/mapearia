﻿//using HTM.MasterGestor.Business.Favorito;

using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Favorito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Bll.Configuracao;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class FavoritoController : ControllerExtended
    {
        public FavoritoController()
        {
            IndexUrl = "/Configuracao/Favorito/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FavoritoInfo info = new FavoritoInfo();

            ViewModelToModelMapper.Map<FavoritoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FavoritoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFavorito.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFavorito, "Favorito", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FavoritoInfo info = FavoritoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFavorito, "Favorito");
            }

            return View(vm);
        }


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FavoritoInfo info = new FavoritoInfo();
            info.Url = vm.Url;
            info.IdUsuario = ControladorSessaoUsuario.SessaoCorrente.Pessoa.Usuario.IdUsuario;

            ListPaged<FavoritoInfo> retorno = new ListPaged<FavoritoInfo>(FavoritoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Favorito");
        }

        public ActionResult Remover(string url)
        {
            var telaInfo = TelaBll.Instance.ListarPorParametros(new TelaInfo { Url = url }).FirstOrDefault();

            bool sucesso = FavoritoBll.Instance.ExcluirPorParametro(new FavoritoInfo { IdUsuario = ControladorSessaoUsuario.SessaoCorrente.Pessoa.Usuario.IdUsuario, IdTela = telaInfo.IdTela });

            return Json(new { Sucesso = sucesso });
        }

        public ActionResult Adicionar(string url)
        {
            FavoritoInfo info = new FavoritoInfo();

            var telaInfo = TelaBll.Instance.ListarPorParametros(new TelaInfo { Url = url }).FirstOrDefault();

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdUsuario = ControladorSessaoUsuario.SessaoCorrente.Pessoa.Usuario.IdUsuario.Value;
            info.IdTela = telaInfo.IdTela.Value;

            var response = FavoritoBll.Instance.Salvar(info);
            return Json(new { Sucesso = response.Response.Sucesso });
        }

    }
}
