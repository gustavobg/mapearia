using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.GrupoMenu;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class GrupoMenuController : ControllerExtended
    {
        public GrupoMenuController()
        {
            IndexUrl = "/Configuracao/GrupoMenu/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            GrupoMenuInfo info = new GrupoMenuInfo();
            ViewModelToModelMapper.Map<GrupoMenuInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            // arvore de menu
            info.Itens = DesfazerArvore(vm.Tree);

            // monta lista de IdModulo
            info.Modulos = new List<GrupoMenuModuloInfo>();
            CommaSeparatedToList(vm.IdsModulos).ForEach(idModulo =>
            {
                info.Modulos.Add(new GrupoMenuModuloInfo()
                {
                    IdModulo = idModulo
                });
            });

            CommaSeparatedToList(vm.IdsAplicacoesTipos).ForEach(IdAplicacaoTipo =>
            {
                info.AplicacoesTipo.Add(new GrupoMenuAplicacaoTipoInfo()
                {
                    IdAplicacaoTipo = IdAplicacaoTipo
                });
            });

            CommaSeparatedToList(vm.IdsNaturezaOperacional).ForEach(idNaturezaOperacional =>
            {
                info.NaturezasOperacionais.Add(new GrupoMenuNaturezaOperacionalInfo()
                {
                    IdNaturezaOperacional = idNaturezaOperacional
                });
            });

            var response = GrupoMenuBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdGrupoMenu.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdGrupoMenu, "GrupoMenu", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                GrupoMenuInfo info = GrupoMenuBll.Instance.ListarPorIdCompleto(new GrupoMenuInfo() { IdGrupoMenu = id });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                vm.IdsModulos = string.Join(",", info.Modulos.Select(x => x.IdModulo));

                if (info.Itens.Count > 0)
                    vm.Tree = GerarArvore(info.Itens);
                else
                {
                    JsTree treeRoot = new JsTree();
                    treeRoot.text = "[Clique com o botão direito do mouse]";
                    treeRoot.type = "folder";
                    treeRoot.icon = "fa fa-folder";
                    vm.Tree.Add(treeRoot);
                }

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdGrupoMenu, "GrupoMenu");
            }
            else
            {
                JsTree treeRoot = new JsTree();
                treeRoot.text = "[Clique com o botão direito do mouse]";
                treeRoot.type = "folder";
                treeRoot.icon = "fa fa-folder";
                vm.Tree.Add(treeRoot);

                vm.Ativo = true;
            }

            vm.TreeHelper = GerarTreeHelper((ModuloBll.Instance.ListarModuloETelas(new ModuloInfo { Ativo = true })));
            return Json(vm);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            GrupoMenuInfo info = new GrupoMenuInfo();
            info.IdGrupoMenu = vm.IdGrupoMenu;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<GrupoMenuInfo> retorno = new ListPaged<GrupoMenuInfo>(GrupoMenuBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Grupo de Menu", true);
        }

        private List<JsTree> GerarArvore(List<GrupoMenuItemInfo> itens, GrupoMenuItemInfo atual = null)
        {
            //TODO: CRIAR UMA PROCEDURE PARA RETORNAR OS ITENS HIERARQUICAMENTE. 
            List<JsTree> retorno = new List<JsTree>();
            List<GrupoMenuItemInfo> nivelCorrente;

            if (atual == null)
            {
                nivelCorrente = itens.Where(x => !x.IdGrupoMenuItemPai.HasValue).OrderBy(x => x.Ordem).ToList();
            }
            else
            {
                nivelCorrente = itens.Where(x => x.IdGrupoMenuItemPai == atual.IdGrupoMenuItem).OrderBy(x => x.Ordem).ToList();
            }

            foreach (GrupoMenuItemInfo grpItem in nivelCorrente)
            {
                JsTree treeItem = new JsTree();
                treeItem.data.id = grpItem.IdTela.HasValue ? grpItem.IdTela.Value.ToString() : grpItem.IdGrupoMenuItem.Value.ToString();
                //treeItem.data.reference = grpItem.IdTela.HasValue ? "tela" : "modulo";
                treeItem.text = grpItem.IdTela.HasValue ? grpItem.Tela.TituloMenu : grpItem.Descricao;
                treeItem.type = grpItem.IdTela.HasValue ? "default" : "folder";
                treeItem.icon = grpItem.IdTela.HasValue ? "fa fa-file-o" : "fa fa-folder";
                treeItem.children = new List<JsTree>();
                treeItem.children.AddRange(GerarArvore(itens, grpItem));

                retorno.Add(treeItem);
            }

            return retorno;
        }
        private List<GrupoMenuItemInfo> DesfazerArvore(List<JsTree> itens, int ordem = 0)
        {
            List<GrupoMenuItemInfo> retorno = new List<GrupoMenuItemInfo>();

            foreach (JsTree treeItem in itens)
            {
                ordem++;
                GrupoMenuItemInfo grpItem = new GrupoMenuItemInfo();

                if (treeItem.type == "default")
                {
                    grpItem.IdTela = int.Parse(treeItem.data.id);
                }
                else
                {
                    grpItem.Descricao = treeItem.text;
                }
                grpItem.Ordem = ordem;

                grpItem.Filhos.AddRange(DesfazerArvore(treeItem.children, ordem));
                retorno.Add(grpItem);
            }

            return retorno;
        }
        private List<JsTree> GerarTreeHelper(List<ModuloInfo> mod)
        {
            List<JsTree> retorno = new List<JsTree>();
            List<ModuloInfo> modulosPai = mod.OrderBy(x => x.Descricao).
                Where(x => x.IdModuloPai == null && (x.Telas.Count() == 0 || x.Telas == null)).
                Select(x => new ModuloInfo()
                {
                    IdModulo = x.IdModulo,
                    Descricao = x.Descricao
                }).ToList();

            modulosPai.ForEach(pai =>
            {
                JsTree nodePai = new JsTree();
                nodePai.text = pai.DescricaoCompleta;
                //nodePai.data.reference = "modulo";
                nodePai.type = "folder";
                nodePai.icon = "fa fa-folder";

                mod.Where(x => x.IdModuloPai == pai.IdModulo).ToList().ForEach(modulo =>
                {
                    JsTree node = new JsTree();
                    node.text = modulo.Descricao;
                    node.type = "folder";
                    node.icon = "fa fa-folder";
                    //node.data.reference = "modulo";

                    List<TelaInfo> telasFilhas = modulo.Telas;
                    telasFilhas.ForEach(tela =>
                    {
                        JsTree nodeTela = new JsTree();
                        nodeTela.text = tela.TituloMenu;
                        nodeTela.icon = "fa fa-file-o";
                        nodeTela.type = "default";
                        nodeTela.data.id = tela.IdTela.ToString();
                        //nodeTela.data.reference = "tela";
                        node.children.Add(nodeTela);
                    });

                    nodePai.children.Add(node);
                });

                retorno.Add(nodePai);
            });

            return retorno;
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListPersonalizado(ListVM vm)
        {
            GrupoUsuarioMenuInfo info = new GrupoUsuarioMenuInfo();
            info.IdTela = vm.IdTela;
            info.IdTelaIn = vm.IdTelaIn;
            info.TituloTela = vm.TituloTela;
            info.IdGrupoUsuario = vm.IdGrupoUsuario;

            ListPaged<GrupoUsuarioMenuInfo> retorno = new ListPaged<GrupoUsuarioMenuInfo>(GrupoUsuarioMenuBll.Instance.ListarTelasPorGrupoUsuario(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação:GrupoUsuarioMenu", true);
        }
    }
}
