using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.GrupoMenuItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class GrupoMenuItemController : ControllerExtended
    {
        //private GrupoMenuItemBLL bll = new GrupoMenuItemBLL();

        public GrupoMenuItemController()
        {
            IndexUrl = "/Configuracao/GrupoMenuItem/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        //public JsonResult Arvore(int idGrupoMenu) {
        //    DTOAdquirirGrupoMenu dto = new DTOAdquirirGrupoMenu();
        //    dto.IdGrupoMenu = idGrupoMenu;
        //    dto.Ativo = true;

        //    GrupoMenu grp = new GrupoMenuBLL().AdquirirGrupoMenu(dto).FirstOrDefault();

        //    return new JsonResult() { Data = GerarArvore(grp.Itens), JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        //}

        //private List<JsTree> GerarArvore(List<GrupoMenuItem> itens, GrupoMenuItem atual = null)
        //{
        //    List<JsTree> retorno = new List<JsTree>();
        //    List<GrupoMenuItem> nivelCorrente;

        //    if (atual == null)
        //    {
        //        nivelCorrente = itens.Where(x => !x.IdGrupoMenuItemPai.HasValue).OrderBy(x => x.Ordem).ToList();
        //    }
        //    else
        //    {
        //        nivelCorrente = itens.Where(x => x.IdGrupoMenuItemPai == atual.IdGrupoMenuItem).OrderBy(x => x.Ordem).ToList();
        //    } 

        //    foreach (GrupoMenuItem grpItem in nivelCorrente)
        //    {
        //        JsTree treeItem = new JsTree();

        //        treeItem.data.id = grpItem.IdTela.HasValue ? grpItem.IdTela.Value.ToString() : grpItem.IdGrupoMenuItem.Value.ToString();                
        //        treeItem.text = grpItem.IdTela.HasValue ? grpItem.Tela.TituloMenu : grpItem.Descricao;
        //        treeItem.type = grpItem.IdTela.HasValue ? "default": "folder";
        //        treeItem.icon = grpItem.IdTela.HasValue ? "fa fa-file-o" : "fa fa-folder";
        //        treeItem.children = new List<JsTree>();
        //        treeItem.children.AddRange(GerarArvore(itens, grpItem));
        //        retorno.Add(treeItem);
        //    }

        //    return retorno;
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirGrupoMenuItem dto = new DTOAdquirirGrupoMenuItem();
        //    dto.Page = vm.Page;

        //    ListPaged<GrupoMenuItem> retorno = bll.AdquirirGrupoMenuItem(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: GrupoMenuItem");
        //}

    }
}
