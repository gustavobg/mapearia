using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Modulo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    
    public class ModuloController : ControllerExtended
    {
        public ModuloController()
        {
            IndexUrl = "/Configuracao/Modulo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ModuloInfo info = new ModuloInfo();
            ViewModelToModelMapper.Map<ModuloInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;


            var response = ModuloBll.Instance.Salvar(info);
            if(info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdModulo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdModulo, "Modulo", response.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ModuloInfo info = ModuloBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdModulo, "Modulo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ModuloInfo info = new ModuloInfo();
            info.IdModulo = vm.IdModulo;
            info.IdModuloIn = vm.IdModuloIn;
            info.Descricao = vm.Descricao;
            info.IdModuloPaiIn = vm.IdModuloPaiIn;
            info.IdModuloNotIn = vm.IdModuloNotIn;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.Ativo = vm.Ativo;
            info.ComPai = vm.ComPai;

            ListPaged<ModuloInfo> retorno = new ListPaged<ModuloInfo>(ModuloBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Modulo", true);
        }
    }
}
