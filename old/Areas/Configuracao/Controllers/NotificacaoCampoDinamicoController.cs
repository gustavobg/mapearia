﻿


using HTM.MasterGestor.Bll.Notificacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoCampoDinamico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoCampoDinamicoController : ControllerExtended
    {
        public NotificacaoCampoDinamicoController()
        {
            IndexUrl = "/Configuracao/NotificacaoCampoDinamico/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            NotificacaoCampoDinamicoInfo info = new NotificacaoCampoDinamicoInfo();
            ViewModelToModelMapper.Map<NotificacaoCampoDinamicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = NotificacaoCampoDinamicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdNotificacaoCampoDinamico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdNotificacaoCampoDinamico, "NotificacaoCampoDinamico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            vm.IdNotificacaoCampoDinamico = id;
            return View(vm);
        }

        // Teste Ajax
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                NotificacaoCampoDinamicoInfo info = NotificacaoCampoDinamicoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdNotificacaoCampoDinamico, "NotificacaoCampoDinamico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            NotificacaoCampoDinamicoInfo info = new NotificacaoCampoDinamicoInfo();
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<NotificacaoCampoDinamicoInfo> retorno = new ListPaged<NotificacaoCampoDinamicoInfo>(NotificacaoCampoDinamicoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: NotificacaoCampoDinamico", true);
        }

    }
}
