﻿using HTM.MasterGestor.Bll.Notificacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Notificacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoController : ControllerExtended
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Todas()
        {           
            IndexVM vm = new IndexVM();
            return View(vm);
        }

        public ActionResult List()
        {
            NotificacaoEnvioInfo info = new NotificacaoEnvioInfo();
            IndexVM vm = new IndexVM();


            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa;

            var lst = NotificacaoEnvioBll.Instance.ListarTodasNotificacoesPorEmpresaEPessoa(info);

            info.Notificacoes = new List<NotificacaoEnvioInfo>();
            info.Notificacoes.AddRange(lst);

            foreach (var item in info.Notificacoes)
            {
                HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Notificacao.IndexVM.NotificacoesVM x = new IndexVM.NotificacoesVM();
                x.DataEnviada = item.DataEnviada;
                x.DataVisualizacao = item.DataVisualizacao;
                x.AssuntoFormatado = item.AssuntoFormatado.Length > 50 ? item.AssuntoFormatado.Substring(0, 50) : item.AssuntoFormatado;
                x.CorpoFormatado = item.CorpoFormatado.Length > 50 ? item.CorpoFormatado.Substring(0, 50) : item.CorpoFormatado;
                x.DataHoraEnviada = item.DataHoraEnviada;

                vm.Notificacoes.Add(x);
            }            

            //ViewModelToModelMapper.MapBack<IndexVM>(vm, info);

            return Json(vm.Notificacoes);
        }

    }
}
