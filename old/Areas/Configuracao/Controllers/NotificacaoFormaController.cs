﻿


using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoForma;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Bll.Notificacao;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoFormaController : ControllerExtended
    {
        public NotificacaoFormaController()
        {
            IndexUrl = "/Configuracao/NotificacaoForma/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new NotificacaoFormaInfo();
            ViewModelToModelMapper.Map<NotificacaoFormaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = NotificacaoFormaBll.Instance.Salvar(info);

            SalvarHistoricoAcesso(IndexUrl, vm.IdNotificacaoForma.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdNotificacaoForma, "NotificacaoForma", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                var entidade = NotificacaoFormaBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdNotificacaoForma, "NotificacaoForma");
            }
            else
                vm.Ativo = true;
            return Json(vm);
        }


        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new NotificacaoFormaInfo();            
            info.IdNotificacaoForma = vm.IdNotificacaoForma;
            info.Descricao = vm.Descricao;
            info.IdNotificacaoFormaIn = vm.IdNotificacaoFormaIn;
            info.IdNotificacaoFormaNotIn = vm.IdNotificacaoFormaNotIn;
            info.IdNotificacaoFormaPai = vm.IdNotificacaoFormaPai;
            info.IdNotificacaoFormaPaiIn = vm.IdNotificacaoFormaPaiIn;
            info.Ativo = vm.Ativo;

            var retorno = new ListPaged<NotificacaoFormaInfo>(NotificacaoFormaBll.Instance.ListarHierarquicamente(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: NotificacaoForma", true);
        }

    }
}
