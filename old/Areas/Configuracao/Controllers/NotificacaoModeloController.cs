﻿



using HTM.MasterGestor.Bll.Notificacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoModelo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoModeloController : ControllerExtended
    {
        public NotificacaoModeloController()
        {
            IndexUrl = "/Configuracao/NotificacaoModelo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new NotificacaoModeloInfo();
            ViewModelToModelMapper.Map<NotificacaoModeloInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = NotificacaoModeloBll.Instance.Salvar(info);
            SalvarHistoricoAcesso(IndexUrl, vm.IdNotificacaoModelo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdNotificacaoModelo, "NotificacaoCampoDinamico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                var entidade = NotificacaoModeloBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdNotificacaoModelo, entidade.ToString());
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new NotificacaoModeloInfo();
            info.IdNotificacaoModelo = vm.idNotificacaoModelo;
            info.IdModuloIn = vm.IdModuloIn;
            info.IdNotificacaoFormaIn = vm.IdNotificacaoFormaIn;
            info.IdNotificacaoTipoIn = vm.IdNotificacaoTipoIn;
            info.Assunto = vm.Assunto;
            info.Ativo = vm.Ativo;
            info.IdEmpresa = vm.IdEmpresa;

            ListPaged<NotificacaoModeloInfo> retorno = new ListPaged<NotificacaoModeloInfo>(NotificacaoModeloBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: NotificacaoModelo", true);
        }
    }
}
