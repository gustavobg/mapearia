﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTela;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Models.Notificacao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Bll.Notificacao;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoTelaController : ControllerExtended
    {
        public NotificacaoTelaController()
        {
            IndexUrl = "/Configuracao/NotificacaoTela/Index";
        }


        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var notificacaoTelaInfo = new NotificacaoTelaInfo();
            ViewModelToModelMapper.Map<NotificacaoTelaInfo>(vm, notificacaoTelaInfo);

            notificacaoTelaInfo.IdPessoaOperacao = IdPessoa;
            notificacaoTelaInfo.IdEmpresaLogada = IdEmpresa;

            var response = NotificacaoTelaBll.Instance.Salvar(notificacaoTelaInfo);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdNotificacaoTela.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdNotificacaoTela, "NotificacaoTela", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                NotificacaoTelaInfo entidade = NotificacaoTelaBll.Instance.AdquirirNotificacaoTelaCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

                if (!vm.IdTela.HasValue)
                    vm.TodasTelas = true;
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdNotificacaoTela, entidade.ToString());
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new NotificacaoTelaInfo();
            info.IdNotificacaoFormaIn = vm.IdNotificacaoFormaIn;
            info.IdNotificacaoTipoIn = vm.IdNotificacaoTipoIn;
            info.IdTelaIn = vm.IdTelaIn;
            info.AssuntoFormatado = vm.AssuntoFormatado;
            info.Ativo = vm.Ativo;

            var retorno = new ListPaged<NotificacaoTelaInfo>(NotificacaoTelaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: NotificacaoTela", true);
        }
    }
}
