﻿using HTM.MasterGestor.Bll.Notificacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoTipoController : ControllerExtended
    {
        public NotificacaoTipoController()
        {
            IndexUrl = "/Configuracao/NotificacaoTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new NotificacaoTipoInfo();

            ViewModelToModelMapper.Map<NotificacaoTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = NotificacaoTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdNotificacaoTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdNotificacaoTipo, "NotificacaoTipo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                NotificacaoTipoInfo info = NotificacaoTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdNotificacaoTipo, "NotificacaoTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new NotificacaoTipoInfo();

            info.IdNotificacaoTipo = vm.IdNotificacaoTipo;
            info.IdNotificacaoTipoIn = vm.IdNotificacaoTipoIn;
            info.Descricao = vm.Descricao;
            info.IdNotificacaoTipoPaiIn = vm.IdNotificacaoTipoPaiIn;
            info.IdNotificacaoTipoNotIn = vm.IdNotificacaoTipoNotIn;
            info.IdNotificacaoTipoPai = vm.idNotificacaoTipoPai;
            info.Ativo = vm.Ativo;

            var retorno = new ListPaged<NotificacaoTipoInfo>(NotificacaoTipoBll.Instance.ListarHierarquicamente(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: NotificacaoTipo", true);
        }
    }
}
