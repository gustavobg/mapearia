﻿using HTM.MasterGestor.Bll.Notificacao;
using HTM.MasterGestor.Model.Notificacao;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Models.Notificacao;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class NotificacaoWorkerController : AsyncController
    {
        [HttpPost]
        public ActionResult PostNotificacaoExibida(int idNotificacaoEnvio)
        {
            var salvarNotificaoUsuario = new NotificacaoUsuarioInfo();
            salvarNotificaoUsuario.IdNotificacaoEnvio = idNotificacaoEnvio;
            salvarNotificaoUsuario.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;
            salvarNotificaoUsuario.DataLeitura = DateTime.Now;

            var response = NotificacaoUsuarioBll.Instance.Salvar(salvarNotificaoUsuario);

            return Json(response.Response.Sucesso);

        }

        [HttpPost]
        public ActionResult AtualizaDataVisualizacao(string idsNotificacoesEnvio)
        {

            var notificacaoEnvioInfo = new NotificacaoEnvioInfo();
            notificacaoEnvioInfo.IdNotificacaoEnvioIn = idsNotificacoesEnvio;

            var lst = NotificacaoEnvioBll.Instance.ListarPorParametros(notificacaoEnvioInfo);

            foreach (var info in lst)
                info.DataVisualizacao = DateTime.Now;

            var sucesso = NotificacaoEnvioBll.Instance.Salvar(lst);

            return Json(new { Sucesso = sucesso });

        }

        [HttpPost]
        public JsonResult GetNotificacaoTelaExibicao(int idTela)
        {
            var notificacaoEnvioInfo = new NotificacaoEnvioInfo();

            if (ControladorSessaoUsuario.SessaoCorrente != null)
            {
                notificacaoEnvioInfo.IdEmpresa = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value;
                notificacaoEnvioInfo.Situacao = (int)NotificacaoSituacaoEnum.AguardandoEnvio;
                notificacaoEnvioInfo.IdTela = idTela;

                List<NotificacaoTelaExibicao> notificacaoExibicao = new List<NotificacaoTelaExibicao>();

                List<NotificacaoEnvioInfo> notificacoes = NotificacaoEnvioBll.Instance.ListarComTelaPorParametros(notificacaoEnvioInfo);

                if (notificacoes != null && notificacoes.Any())
                {
                    FiltrarNotificacoesLidas(notificacoes);

                    var notificacoesParaExibir = FiltrarDatasNotificacoes(notificacoes);

                    notificacaoExibicao = notificacoesParaExibir.Select(x => new NotificacaoTelaExibicao
                    {
                        IdNotificacaoEnvio = x.IdNotificacaoEnvio.Value,
                        Assunto = x.AssuntoFormatado,
                        Corpo = x.CorpoFormatado,
                        IdNotificacaoTipo = x.NotificacaoTela.IdNotificacaoTipo.Value,
                        IdNotificacaoForma = x.NotificacaoTela.IdNotificacaoForma.Value,
                        TelaCampo = x.NotificacaoTela.TelaCampo
                    }).ToList();
                }
                return Json(notificacaoExibicao);
            }
            else
                return Json(null);
        }

        /// <summary>
        /// Utilizado para notificações Globais, como por exemplo Atualização dos Sistema
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetNotificacaoSemTelaExibicao()
        {
            var notificacaoEnvioInfo = new NotificacaoEnvioInfo();

            if (ControladorSessaoUsuario.SessaoCorrente != null)
            {
                notificacaoEnvioInfo.IdEmpresa = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value;
                notificacaoEnvioInfo.Situacao = (int)NotificacaoSituacaoEnum.AguardandoEnvio;

                List<NotificacaoTelaExibicao> notificacaoExibicao = new List<NotificacaoTelaExibicao>();

                List<NotificacaoEnvioInfo> notificacoes = NotificacaoEnvioBll.Instance.ListarSemTelaPorParametros(notificacaoEnvioInfo);

                if (notificacoes != null && notificacoes.Any())
                {
                    FiltrarNotificacoesLidas(notificacoes);

                    var notificacoesParaExibir = FiltrarDatasNotificacoes(notificacoes);

                    notificacaoExibicao = notificacoesParaExibir.Select(x => new NotificacaoTelaExibicao
                    {
                        IdNotificacaoEnvio = x.IdNotificacaoEnvio.Value,
                        Assunto = x.AssuntoFormatado,
                        Corpo = x.CorpoFormatado,
                        IdNotificacaoTipo = x.NotificacaoTela.IdNotificacaoTipo.Value,
                        IdNotificacaoForma = x.NotificacaoTela.IdNotificacaoForma.Value,
                    }).ToList();
                }
                return Json(notificacaoExibicao);
            }
            else
                return Json(null);
        }

        private void FiltrarNotificacoesLidas(List<NotificacaoEnvioInfo> notificacoes)
        {
            var notificacaoUsuarioInfo = new NotificacaoUsuarioInfo();
            notificacaoUsuarioInfo.IdNotificacaoEnvioIn = string.Join(",", notificacoes.Select(n => n.IdNotificacaoEnvio.Value));
            notificacaoUsuarioInfo.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa;

            var notificacoesLidas = NotificacaoUsuarioBll.Instance.ListarPorParametros(notificacaoUsuarioInfo);

            if (notificacoesLidas != null && notificacoesLidas.Any())
            {
                notificacoes.RemoveAll(n => notificacoesLidas.Any(nl => nl.IdNotificacaoEnvio == n.IdNotificacaoEnvio));
            }
        }

        private List<NotificacaoEnvioInfo> FiltrarDatasNotificacoes(List<NotificacaoEnvioInfo> notificacoes)
        {
            notificacoes.RemoveAll(n => n.NotificacaoTela == null);
            notificacoes.RemoveAll(n => !n.NotificacaoTela.Ativo.Value);

            var notificacoesParaExibir = notificacoes.Where(n => !n.NotificacaoTela.DataInicio.HasValue && n.NotificacaoTela.DataTermino.HasValue && n.NotificacaoTela.DataTermino.Value >= DateTime.Now).ToList();

            notificacoesParaExibir.AddRange(notificacoes.Where(n => !n.NotificacaoTela.DataTermino.HasValue &&
                                                                    n.NotificacaoTela.DataInicio.HasValue &&
                                                                    n.NotificacaoTela.DataInicio.Value <= DateTime.Now));

            notificacoesParaExibir.AddRange(notificacoes.Where(n => n.NotificacaoTela.DataInicio.HasValue &&
                                                                    n.NotificacaoTela.DataInicio.Value <= DateTime.Now &&
                                                                    n.NotificacaoTela.DataTermino.HasValue &&
                                                                    n.NotificacaoTela.DataTermino.Value >= DateTime.Now).ToList());

            notificacoesParaExibir.AddRange(notificacoes.Where(n => !n.NotificacaoTela.DataInicio.HasValue && !n.NotificacaoTela.DataTermino.HasValue).ToList());

            return notificacoesParaExibir;
        }

        /// <summary>
        /// Retorna a Quantidade de Notificações não Lidas (exclusivas do usuário ou as notificações da Empresa)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult RetornaTotalNotificacoesNovas(bool ApenasTotalizador, string guid)
        {

            NotificacaoEnvioInfo notificacaoEnvioInfo = new NotificacaoEnvioInfo();
            List<NotificacaoTelaExibicao> notificacaoExibicao = new List<NotificacaoTelaExibicao>();
            int total = 0;


            if (ControladorSessaoUsuario.SessaoCorrente != null)
            {
                notificacaoEnvioInfo.IdEmpresa = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value;
                notificacaoEnvioInfo.Situacao = (int)NotificacaoSituacaoEnum.AguardandoEnvio;
                notificacaoEnvioInfo.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa;
                List<NotificacaoEnvioInfo> notificacoes = NotificacaoEnvioBll.Instance.ListarTodasNotificacaoesParaVisualizacaoSistema(notificacaoEnvioInfo);

                //if (notificacoes != null && notificacoes.Any())
                //{
                //    FiltrarNotificacoesLidas(notificacoes);

                //    var notificacoesParaExibir = FiltrarDatasNotificacoes(notificacoes);

                //    notificacaoExibicao = notificacoesParaExibir.Select(x => new NotificacaoTelaExibicao
                //    {
                //        IdNotificacaoEnvio = x.IdNotificacaoEnvio.Value,
                //        Assunto = x.AssuntoFormatado,
                //        Corpo = x.CorpoFormatado,
                //        IdNotificacaoTipo = x.NotificacaoTela.IdNotificacaoTipo.Value,
                //        IdNotificacaoForma = x.NotificacaoTela.IdNotificacaoForma.Value,
                //        TelaCampo = x.NotificacaoTela.TelaCampo
                //    }).ToList();
                //}

                total = notificacoes.Count();

                if (ApenasTotalizador)
                {
                    return Json(new
                    {
                        Data = total
                    });
                }
                else
                {
                    return Json(new
                    {
                        Data = notificacoes
                    });
                }

            }
            else
            {
            return Json(new
            {
                Data = 0
            });
            }
        }
    }
}


