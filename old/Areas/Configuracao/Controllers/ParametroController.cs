﻿using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Caracteristica;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Parametro;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class ParametroController : ControllerExtended
    {
        public ParametroController()
        {
            IndexUrl = "/Configuracao/Parametro/Index";
        }


        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CaracteristicaInfo info = new CaracteristicaInfo();
            ViewModelToModelMapper.Map<CaracteristicaInfo>(vm, info);

            if (vm.NivelAcesso == 1)
            {
                info.IdEmpresa = null;
            }
            else if (vm.NivelAcesso == 3)
            {
                info.IdEmpresa = IdEmpresa;
            }


            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TipoCaracteristica = (int)TipoCaracteristicaEnum.Parametro;

            var response = CaracteristicaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCaracteristica.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCaracteristica, "Caracteristica", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                CaracteristicaInfo info = new CaracteristicaInfo();
                info = CaracteristicaBll.Instance.ListarCaracteristicaComOpcoes(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCaracteristica, "Caracteristica");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        #endregion

        //[HttpGet, CustomAuthorize]
        //public ActionResult Configuracao(int? id)
        //{
        //    ConfiguracaoVM vm = new ConfiguracaoVM("/Configuracao/Parametro/Configuracao");

        //    var modulos = new ModuloBLL().AdquirirModulo(new DTOAdquirirModulo { ComPai = true, Page = new PagingRequest { OrderBy = "Modulo.DescricaoCompleta" } });
        //    List<SelectListItem> lst = new List<SelectListItem>();
        //    if (id.HasValue)
        //        lst.Add(new SelectListItem() { Text = "Todos", Value = "" });
        //    else
        //        lst.Add(new SelectListItem() { Text = "Todos", Value = "", Selected = true });

        //    foreach (var itemModulo in modulos)
        //    {
        //        if (id.HasValue)
        //        {
        //            if (itemModulo.IdModulo == id.Value)
        //                lst.Add(new SelectListItem { Text = itemModulo.DescricaoCompleta, Value = itemModulo.IdModulo.ToString(), Selected = true });
        //            else
        //                lst.Add(new SelectListItem { Text = itemModulo.DescricaoCompleta, Value = itemModulo.IdModulo.ToString() });
        //        }
        //        else
        //            lst.Add(new SelectListItem { Text = itemModulo.DescricaoCompleta, Value = itemModulo.IdModulo.ToString() });

        //    }
        //    ViewBag.Modulos = lst;
        //    return View(vm);
        //}

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CaracteristicaInfo info = new CaracteristicaInfo();
            info.IdCaracteristicaTabela = vm.IdCaracteristicaTabela;
            info.IdCaracteristicaTipo = vm.IdCaracteristicaTipo;
            info.IdModuloIn = vm.IdModuloIn;
            info.TipoCaracteristica = (int)TipoCaracteristicaEnum.Parametro;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<CaracteristicaInfo> retorno = new ListPaged<CaracteristicaInfo>(CaracteristicaBll.Instance.ListarPorParametrosComModulos(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Parâmetros", true);
        }

        //[HttpPost, CustomAuthorize]
        //public ActionResult ConfiguracaoJson(int? id)
        //{
        //    ConfiguracaoVM vm = new ConfiguracaoVM("/Configuracao/Parametro/Configuracao");

        //    Entity.Caracteristica.Caracteristica entidade = new Entity.Caracteristica.Caracteristica();
        //    entidade.Caracteristicas = bll.AdquirirCaracteristicaComValor(new DTOAdquirirCaracteristica
        //    {
        //        TipoCaracteristica = (int)TipoCaracteristica.Parametro,
        //        IdEmpresaCaracteristicaValor = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa,
        //        IdModulo = id,
        //        IdPessoaEmpresaCaracteristicaValor = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa

        //    },
        //    IdEmpresa: IdEmpresa).OrderBy(p =>p.Ordem).ToList();

        //    ConfiguracaoEntity modulosD = new ConfiguracaoEntity();
        //    modulosD.Configuracao = new List<Entity.Configuracao>();

        //    foreach (var moduloCaracteristicas in entidade.Caracteristicas.OrderBy(x => x.DescricaoModulo).GroupBy(x => x.DescricaoModulo))
        //    {
        //        modulosD.Configuracao.Add(new Entity.Configuracao
        //        {
        //            DescricaoModulo = moduloCaracteristicas.Key,
        //            Caracteristicas = moduloCaracteristicas.Select(c => c).ToList()
        //        });
        //    }

        //    foreach (var item in vm.Configuracao)
        //    {
        //        foreach (var item2 in item.Caracteristicas)
        //        {
        //            if (item2.TipoCaracteristica.Value == 10)
        //                item2.CaracteristicaValor.Valor.ToLower();
        //        }
        //    }

        //    ViewModelToModelMapper.MapBack<ConfiguracaoVM>(vm, modulosD);


        //    return Json(vm);

        //}

        //[HttpPost]
        //public ActionResult SalvarConfiguracao(string json)
        //{
        //    CaracteristicaValorBLL caracteristicaValorBLL = new CaracteristicaValorBLL();
        //    DTOSalvarCaracteristicaValor dto = new DTOSalvarCaracteristicaValor();
        //    List<Caracteristica> lstCaracteristicas = new List<Caracteristica>();
        //    var vm = JsonConvert.DeserializeObject<ConfiguracaoVM>(json);

        //    //Salvar Apenas os Valores das Caracteristicas (CaracteristicaValor).
        //    List<DTOSalvarCaracteristicaValor> dtosCaracteristicaValor = new List<DTOSalvarCaracteristicaValor>();
        //    List<CaracteristicaValor> lstCaracteristicaValor = new List<CaracteristicaValor>();
        //    foreach (var itemConfiguracao in vm.Configuracao)
        //    {
        //        foreach (var itemCaracteristica in itemConfiguracao.Caracteristicas)
        //        {
        //            CaracteristicaValor caracteristicaValor = new CaracteristicaValor();
        //            caracteristicaValor.IdCaracteristica = itemCaracteristica.IdCaracteristica.Value;
        //            caracteristicaValor.IdCaracteristicaValor = itemCaracteristica.CaracteristicaValor.IdCaracteristicaValor;
        //            caracteristicaValor.IdCaracteristicaOpcao = itemCaracteristica.CaracteristicaValor.IdCaracteristicaOpcao;
        //            caracteristicaValor.ChaveVersao = itemCaracteristica.CaracteristicaValor.ChaveVersao.Value;
        //            caracteristicaValor.Valor = itemCaracteristica.CaracteristicaValor.Valor;
        //            caracteristicaValor.IdEmpresa = IdEmpresa.Value;

        //            if (itemCaracteristica.NivelAcesso == 4)
        //                caracteristicaValor.IdPessoaEmpresa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa;

        //            dtosCaracteristicaValor.Add(new DTOSalvarCaracteristicaValor()
        //            {
        //                IdEmpresa = IdEmpresa.Value,
        //                IdPessoaOperacao = IdPessoa,
        //                Entidade = caracteristicaValor
        //            });
        //        }
        //    }
        //    var sucesso = new CaracteristicaValorBLL().SalvarCaracteristicaValor(dtosCaracteristicaValor);
        //    return Json(new
        //    {
        //        Sucesso = sucesso
        //    });
        //}
    }
}