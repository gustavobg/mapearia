using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Permissao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class PermissaoController : ControllerExtended
    {
        public PermissaoController() 
        {
            IndexUrl = "/Configuracao/Permissao/Index";
        }

        //private PermissaoBLL bll = new PermissaoBLL();

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarPermissao dto = new DTOSalvarPermissao();
        //    ViewModelToModelMapper.Map<Permissao>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;

        //    CommaSeparatedToList(vm.IdsTela).ForEach(idTela => {
        //        dto.Entidade.Telas.Add(new PermissaoTela() { IdTela = idTela });
        //    });

        //    DTOSalvarPermissaoResponse response = bll.SalvarPermissao(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdPermissao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPermissao, dto.Entidade.ToString(), response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        Permissao entidade = bll.AdquirirPermissao(new DTOAdquirirPermissao() { IdPermissao = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);
        //        vm.IdsTela = string.Join(",", entidade.Telas.Select(x => x.IdTela));

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, (int)entidade.IdPermissao, entidade.ToString());
        //    }
        //    else
        //    {
        //        vm.Ativo = true;
        //    }

        //    // lista permissoes para selecao
        //    List<Permissao> existentes = bll.AdquirirPermissao(new DTOAdquirirPermissao() { Page=null });

        //    vm.PermissaoOptions = Enum.GetValues(typeof(PermissaoEnum)).Cast<PermissaoEnum>()
        //        .Where(x=> !existentes.Exists(y=> (x == y.IdPermissao.Value && x != vm.IdPermissao))).ToList();

        //    List<PermissaoAcao> acoes = new PermissaoAcaoBLL().AdquirirPermissaoAcao(new DTOAdquirirPermissaoAcao());
        //    acoes.ForEach(acao => {
        //        if (!vm.Acoes.Exists(x => x.IdPermissaoAcao == acao.IdPermissaoAcao))
        //        {
        //            vm.Acoes.Add(new NewEditVM.AcoesVM() { Hierarquia = false, DescricaoPermissaoAcao = acao.Descricao, IdPermissaoAcao = acao.IdPermissaoAcao, IdPermissao = vm.IdPermissao });
        //        }
        //    });

        //    return View(vm);
        //}

        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirPermissao dto = new DTOAdquirirPermissao();
        //    dto.Page = vm.Page;
        //    dto.Ativo = vm.Ativo;
        //    dto.Descricao = vm.Descricao;
        //    dto.IdModuloIn = CommaSeparatedToList(vm.IdModuloIn);

        //    ListPaged<Permissao> retorno = bll.AdquirirPermissao(dto);
            
        //    return base.CreateListResult(vm, retorno, "Exportação: Permissão");
        //}
    }
}
