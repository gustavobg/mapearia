﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.PessoaPerfilPessoa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class PessoaPerfilPessoaController : ControllerExtended
    {
        //public PessoaPerfilPessoaController()
        //{
        //    IndexUrl = "/Corporativo/PessoaPerfilPessoa/Index";
        //}

        //private PessoaPerfilPessoaBLL bll = new PessoaPerfilPessoaBLL();

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarPessoaPerfilPessoa dto = new DTOSalvarPessoaPerfilPessoa();
        //    ViewModelToModelMapper.Map<PessoaPerfilPessoa>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarPessoaPerfilPessoaResponse response = bll.SalvarPessoaPerfilPessoa(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdPessoaPerfilPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoaPerfilPessoa, dto.Entidade.ToString(), response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        PessoaPerfilPessoa entidade = bll.AdquirirPessoaPerfilPessoa(new DTOAdquirirPessoaPerfilPessoa() { IdPessoaPerfilPessoa = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdPessoaPerfilPessoa, entidade.ToString());
        //    }

        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirPessoaPerfilPessoa dto = new DTOAdquirirPessoaPerfilPessoa();
        //    dto.Page = vm.Page;

        //    ListPaged<PessoaPerfilPessoa> retorno = bll.AdquirirPessoaPerfilPessoa(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: PessoaPerfilPessoa");
        //}

    }
}
