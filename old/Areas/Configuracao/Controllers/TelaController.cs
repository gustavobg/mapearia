using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tela;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class TelaController : ControllerExtended
    {
        public TelaController()
        {
            IndexUrl = "/Configuracao/Tela/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            TelaIndexVM vm = new TelaIndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            TelaNewEditVM vm = new TelaNewEditVM(); 
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            TelaNewEditVM vm = new TelaNewEditVM();

            if (id.HasValue && id > 0)
            {
                TelaInfo info = new TelaInfo();
                info = TelaBll.Instance.ListarPorIdCompleto(new TelaInfo() { IdTela = id.Value });
                ViewModelToModelMapper.MapBack<TelaNewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdTela, "Tela");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(TelaNewEditVM vm)
        {
            TelaInfo info = new TelaInfo();
            info.Campos = new List<TelaCampoInfo>();
            ViewModelToModelMapper.Map<TelaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsTelaPropriedade))
            {
                info.lstTelaPropriedade = new List<TelaPropriedadeTelaInfo>();

                CommaSeparatedToList(vm.IdsTelaPropriedade).ForEach(idTelaPropriedade =>
                {
                    TelaPropriedadeTelaInfo item = new TelaPropriedadeTelaInfo();
                    item.IdTelaPropriedade = idTelaPropriedade;

                    info.lstTelaPropriedade.Add(item);
                });
            }

            var response = TelaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdTela.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdTela, "Tela", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        #region Listagem

        [JsonFilter(Param = "vm", JsonDataType = typeof(TelaListVM)), CustomAuthorize]
        public ActionResult List(TelaListVM vm)
        {
            TelaInfo info = new TelaInfo();
            info.Descricao = vm.Descricao;
            info.Url = vm.Url;
            info.Ativo = vm.Ativo;
            info.TituloMenu = vm.TituloMenu;
            info.TituloTela = vm.TituloTela;
            info.IdModulo = vm.IdModulo;
            info.IdModuloIn = vm.IdModuloIn;
            info.IdTelaIn = vm.IdTelaIn;
            info.IdTela = vm.IdTela;
            info.IdTelaPropriedadeIn = vm.IdTelaPropriedadeIn;

            ListPaged<TelaInfo> retorno = new ListPaged<TelaInfo>(TelaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tela", true);
        }
        
        [CustomAuthorize]
        public ActionResult ListTelaCampo(string url)
        {
            TelaListVM vm = new TelaListVM();

            var retorno = TelaBll.Instance.ListarTelaECampos(url);

            return Json(retorno);
        }

        [CustomAuthorize]
        public ActionResult AjudaTela(string url)
        {
            AjudaTelaVM vm = new AjudaTelaVM();

            TelaInfo info = new TelaInfo();
            info.Ativo = true;
            info.Url = url;
            vm.Tela = TelaBll.Instance.ListarPorParametros(info).FirstOrDefault();

            return View(vm);
        }

        #endregion
    }
}
