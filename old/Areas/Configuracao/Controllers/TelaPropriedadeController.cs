﻿using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.TelaPropriedade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class TelaPropriedadeController : ControllerExtended
    {
        public TelaPropriedadeController()
        {
            IndexUrl = "/Configuracao/TelaPropriedae/Index";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            TelaPropriedadeInfo info = new TelaPropriedadeInfo();
            info.Descricao = vm.Descricao;
            info.IdTelaPropriedade = vm.IdTelaPropriedade;
            info.IdTelaPropriedadeIn = vm.IdTelaPropriedadeIn;
            info.Ativo = vm.Ativo;

            ListPaged<TelaPropriedadeInfo> retorno = new ListPaged<TelaPropriedadeInfo>(TelaPropriedadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Propriedades da Tela", true);
        }

    }
}
