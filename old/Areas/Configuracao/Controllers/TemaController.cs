﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Bll.Configuracao;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.Controllers
{
    public class TemaController : ControllerExtended
    {
        public TemaController()
        {
            IndexUrl = "/Configuracao/Tema/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            TemaInfo info = new TemaInfo();
            ViewModelToModelMapper.Map<TemaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            var response = TemaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdTema.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdTema, "Tema", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            vm.Ativo = true;

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                TemaInfo info = TemaBll.Instance.ListarPorCodigoCompleto(new TemaInfo { IdTema = id.Value, IdEmpresaLogada = IdEmpresa });
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdTema, "Tema");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            TemaInfo info = new TemaInfo();
            info.Descricao = vm.Descricao;
            info.IdTema = vm.IdTema;
            info.Ativo = vm.Ativo;


            ListPaged<TemaInfo> retorno = new ListPaged<TemaInfo>(TemaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação:Tema", true);
        }
    }
}
