﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoCampoDinamico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public string Descricao { get; set; }

        public bool? Ativo { get; set; }
    }
}