﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Caracteristica
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() 
        {
        }

        public string IdModuloIn { get; set; }
        public int? IdCaracteristicaTabela { get; set; }
        public int? IdCaracteristicaTipo { get; set; }
        public string Descricao { get; set; }
        public bool? Ativo { get; set; }
    }
}