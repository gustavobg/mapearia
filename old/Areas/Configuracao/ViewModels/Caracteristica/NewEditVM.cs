﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Caracteristica
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() {
            Opcoes = new List<OpcoesVM>();
        }

        [ViewModelToModelAttribute]
        public Int32? IdCaracteristica { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresaGrupo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdCaracteristicaTipo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdCaracteristicaTabela { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdModulo { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public String Codigo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? Ordem { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? Obrigatorio { get; set; }

        [ViewModelToModelAttribute]
        public Int32? NivelAcesso { get; set; }

        [ViewModelToModelAttribute]
        public Int32? Tamanho { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoCaracteristica { get; set; }


        [ViewModelToModelAttribute]
        public List<OpcoesVM> Opcoes { get; set; }

        public class OpcoesVM
        {
            [ViewModelToModelAttribute]
            public Int32? IdCaracteristicaOpcao { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public String Valor { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }
        }

    }
}