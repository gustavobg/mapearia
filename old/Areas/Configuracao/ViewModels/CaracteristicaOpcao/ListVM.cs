﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaOpcao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCaracteristicaOpcao { get; set; }
        public int? IdCaracteristica { get; set; }
        public string Valor { get; set; }

    }
}