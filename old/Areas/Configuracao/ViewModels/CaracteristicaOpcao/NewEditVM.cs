﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaOpcao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdCaracteristicaOpcao { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdCaracteristica { get; set; }

        [ViewModelToModelAttribute]
        public String Valor { get; set; }


    }
}