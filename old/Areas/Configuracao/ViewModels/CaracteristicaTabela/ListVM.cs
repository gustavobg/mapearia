﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaTabela
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        
        public int? IdCaracteristicaTabela { get; set; }

        public string Descricao { get; set; }

        public bool? Ativo { get; set; }
    }
}