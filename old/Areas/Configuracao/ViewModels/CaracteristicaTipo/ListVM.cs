﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.CaracteristicaTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        { 
        }

        public int? IdCaracteristicaTipo { get; set; }
        public string Descricao { get; set; }
        public bool? Ativo { get; set; }
    }
}