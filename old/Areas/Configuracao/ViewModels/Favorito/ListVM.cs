﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Favorito
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public string Url { get; set; }
        public int IdPessoa { get; set; }

        public int IdPessoaOperacao { get; set; }
        public int IdEmpresa { get; set; }

    }
}