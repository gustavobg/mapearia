using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.GrupoMenu
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }
        public int? IdGrupoMenu { get; set; }
        public string Descricao { get; set; }
        public bool? Ativo { get; set; }
        public int? IdTema { get; set; }

        public int? IdGrupoUsuario { get; set; }
        public int? IdTela { get; set; }
        public string IdTelaIn { get; set; }
        public string TituloTela { get; set; }
    }
}