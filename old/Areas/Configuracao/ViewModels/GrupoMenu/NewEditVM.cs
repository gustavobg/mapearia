using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using System;
using System.Collections.Generic;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.GrupoMenu
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            Tree = new List<JsTree>();
        }

        [ViewModelToModelAttribute]
		public int? IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        public string IdsModulos { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTema { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAplicacoesTipos { get; set; }

        [ViewModelToModelAttribute]
        public string IdsNaturezaOperacional { get; set; }

        public List<JsTree> Tree { get; set; }
        public List<JsTree> TreeHelper { get; set; }
    }
}