using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.GrupoMenuItem
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

		        [ViewModelToModelAttribute]
		public Int32? IdGrupoMenuItem { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
		public String Descricao { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdTela { get; set; }

        [ViewModelToModelAttribute]
		public String Icone { get; set; }


    }
}