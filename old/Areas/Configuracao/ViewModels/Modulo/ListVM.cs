using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Modulo
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public int? IdModulo { get; set; }
        public int? IdModuloPai { get; set; }
        
        public string IdModuloPaiIn { get; set; }
        public string IdModuloNotIn { get; set; }
        public string IdModuloIn { get; set; }
        public string DescricaoCompleta { get; set; }
        public string Descricao { get; set; }

        public bool? ComPai { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComHierarquia { get; set; }
    }
}