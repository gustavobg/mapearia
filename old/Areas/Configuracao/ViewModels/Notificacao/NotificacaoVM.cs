﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Notificacao
{
    public class IndexVM: VMIndexBase
    {
        public IndexVM()
        {
            Notificacoes = new List<NotificacoesVM>();
        
        }

        [ViewModelToModelAttribute]
        public List<NotificacoesVM> Notificacoes { get; set; }

        public class NotificacoesVM
        {
            [ViewModelToModelAttribute]
            public int? IdNotificacaoEnvio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresaRemetente { get; set; }

            [ViewModelToModelAttribute]
            public int? IdNotificacaoModelo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdNotificacaoTela { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdNotificacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? Situacao { get; set; }

            [ViewModelToModelAttribute]
            public string AssuntoFormatado { get; set; }

            [ViewModelToModelAttribute]
            public string CorpoFormatado { get; set; }

            [ViewModelToModelAttribute]
            public string EmailDestinatario { get; set; }

            [ViewModelToModelAttribute]
            public string TelefoneDestinatario { get; set; }

            [ViewModelToModelAttribute]
            public string DetalheErro { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataProgramada { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataEnviada { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataVisualizacao { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            public string DataHoraEnviada { get; set; }
        }
        
    }
}


