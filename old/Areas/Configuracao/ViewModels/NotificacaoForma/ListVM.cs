﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoForma
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public string Descricao { get; set; }
        public int? IdNotificacaoForma { get; set; }
        public int? IdNotificacaoFormaPai { get; set; }
        public string IdNotificacaoFormaNotIn { get; set; }
        public string IdNotificacaoFormaIn { get; set; }
        public string IdNotificacaoFormaPaiIn { get; set; }
        public bool? ComPai { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComHierarquia { get; set; }
        public bool? ApenasUltimoNivel { get; set; }
    }
}