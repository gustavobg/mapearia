﻿using System;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoForma
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public Int32? IdNotificacaoForma { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdNotificacaoFormaPai { get; set; }

        [ViewModelToModelAttribute]
        public String Formato { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }

    }
}