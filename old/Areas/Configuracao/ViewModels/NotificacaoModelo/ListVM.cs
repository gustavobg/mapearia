﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoModelo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? idNotificacaoModelo { get; set; }
        public int? IdEmpresa { get; set; } 

        public string IdNotificacaoFormaIn { get; set; }
        public string IdNotificacaoTipoIn { get; set; }
        public string IdModuloIn { get; set; }
        public string Assunto { get; set; }

        public bool? Ativo { get; set; }

    }
}