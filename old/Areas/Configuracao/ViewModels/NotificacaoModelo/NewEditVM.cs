﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoModelo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
        }

        [ViewModelToModelAttribute]
        public Int32? IdNotificacaoModelo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresaRemetente { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdNotificacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdNotificacaoForma { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public String Assunto { get; set; }

        [ViewModelToModelAttribute]
        public String Conteudo { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }
    }
}