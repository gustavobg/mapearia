﻿using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTela
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }
    }
}