﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTela
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() 
        {            
        }

        public string IdNotificacaoFormaIn { get; set; }

        public string IdNotificacaoTipoIn { get; set; }

        public string IdTelaIn { get; set; }

        public string AssuntoFormatado { get; set; }

        public bool? Ativo { get; set; }
    }
}