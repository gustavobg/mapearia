﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTela
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            NotificacaoEnvio = new NotificacaoEnvioVM();
        }

        [ViewModelToModelAttribute]
        public int? IdNotificacaoTela { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNotificacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNotificacaoForma { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTela { get; set; }

        [ViewModelToModelAttribute]
        public string TelaCampo { get; set; }
        
        [ViewModelToModelAttribute]
        public DateTime? DataInicio { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataTermino { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo  { get; set; }

        [ViewModelToModelAttribute]
        public bool TodasTelas { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public NotificacaoEnvioVM NotificacaoEnvio { get; set; }

        public class NotificacaoEnvioVM
        {
            [ViewModelToModelAttribute]
            public int? IdNotificacaoEnvio { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresaRemetente { get; set; }
            
            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public string AssuntoFormatado { get; set; }

            [ViewModelToModelAttribute]
            public string CorpoFormatado { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }
        }
    }
}