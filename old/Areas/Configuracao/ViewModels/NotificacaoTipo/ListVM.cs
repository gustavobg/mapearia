﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.NotificacaoTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public string Descricao { get; set; }
        public int? IdNotificacaoTipo { get; set; }
        public int? idNotificacaoTipoPai { get; set; }
        public string IdNotificacaoTipoPaiIn { get; set; }
        public string IdNotificacaoTipoPaiNotIn { get; set; }
        public string IdNotificacaoTipoIn { get; set; }
        public string IdNotificacaoTipoNotIn { get; set; }
        public bool? ComPai { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComHierarquia { get; set; }
        public bool? ApenasUltimoNivel { get; set; }
    }
}