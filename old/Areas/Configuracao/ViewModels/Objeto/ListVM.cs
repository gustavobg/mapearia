﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Objeto
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdObjeto { get; set; }
        
        public string Descricao { get; set; }

        public string IdObjetoIn { get; set; }
        public string IdObjetoTipoIn { get; set; }
        public string IdObjetoTipoNotIn { get; set; }
        public string IdTelaIn { get; set; }
        public string IdTelaNotIn { get; set; }

        public bool? Ativo { get; set; }

    }
}