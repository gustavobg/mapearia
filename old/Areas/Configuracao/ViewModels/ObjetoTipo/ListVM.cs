﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.ObjetoTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdObjetoTipo { get; set; }

        public string Descricao { get; set;}

        public bool? Ativo { get; set; }
    }
}