﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Parametro
{
    public class ConfiguracaoVM : VMIndexBase
    {

        public ConfiguracaoVM(string url)
            : base(url)
        {

            Configuracao = new List<ConfiguracaoModuloVM>();
        }

        public List<ConfiguracaoModuloVM> Configuracao { get; set; }

        public class CaracteristicaVM
        {
            [ViewModelToModelAttribute]
            public Int32? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdEmpresaGrupo { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdCaracteristicaTipo { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdCaracteristicaTabela { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdModulo { get; set; }

            [ViewModelToModelAttribute]
            public String Descricao { get; set; }

            [ViewModelToModelAttribute]
            public String Codigo { get; set; }

            [ViewModelToModelAttribute]
            public Int32? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public Boolean? Obrigatorio { get; set; }

            [ViewModelToModelAttribute]
            public Int32? NivelAcesso { get; set; }

            [ViewModelToModelAttribute]
            public Int32? Tamanho { get; set; }

            [ViewModelToModelAttribute]
            public Boolean? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoModulo { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public List<OpcoesVM> Opcoes { get; set; }

            [ViewModelToModelAttribute(ComplexType=true)]
            public CaracteristicaValorVM CaracteristicaValor { get; set; }

        }

        public class OpcoesVM
        {
            [ViewModelToModelAttribute]
            public Int32? IdCaracteristicaOpcao { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public String Valor { get; set; }
        }

        public class CaracteristicaValorVM
        {
            [ViewModelToModelAttribute]
            public int? IdCaracteristicaValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaOpcao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaTipo { get; set; }

            [ViewModelToModelAttribute]
            public string Codigo { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

        }

        public class ConfiguracaoModuloVM
        {
            [ViewModelToModelAttribute]
            public int? IdModulo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoModulo { get; set; }

            [ViewModelToModelAttribute]
            public List<CaracteristicaVM> Caracteristicas { get; set; }

        }

    }
}