using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Permissao
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public int IdPermissao { get; set; }

        public string IdModuloIn { get; set; }

        public String Descricao { get; set; }
        public bool? Ativo { get; set; }
    }
}