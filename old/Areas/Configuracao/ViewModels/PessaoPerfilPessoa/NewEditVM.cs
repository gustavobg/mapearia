﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.PessoaPerfilPessoa
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdPessoaPerfilPessoa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdPessoaPerfil { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Guid ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public String IdEmpresa { get; set; }


    }
}