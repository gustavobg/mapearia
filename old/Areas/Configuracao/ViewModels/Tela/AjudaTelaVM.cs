using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Configuracao;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tela
{
    public class AjudaTelaVM
    {
        public AjudaTelaVM() {
            
        }

        //public Entity.Tela Tela { get; set; }
        public TelaInfo Tela { get; set; }
    }
}