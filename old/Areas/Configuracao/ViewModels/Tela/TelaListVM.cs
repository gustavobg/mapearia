using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tela
{
    public class TelaListVM : ViewModelListRequestBase
    {
        public TelaListVM() { }

        public int? IdTela { get; set; }
        public int? IdModulo { get; set; }

        public string TituloMenu { get; set; }
        public string TituloTela { get; set; }
        public string Descricao { get; set; }
        public string Url { get; set; }
        public string IdModuloIn { get; set; }
        public string IdTelaIn { get; set; }
        public string IdTelaPropriedadeIn { get; set; }

        public bool? Ativo { get; set; }
    }
}