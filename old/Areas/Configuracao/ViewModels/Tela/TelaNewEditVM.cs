using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;


namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tela
{
    public class TelaNewEditVM : VMNewEditBase
    {
        public TelaNewEditVM() {
            ExibeMenu = true;
            Campos = new List<CampoVM>();
        }

        [ViewModelToModelAttribute]
		public Int32? IdTela { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdModulo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdPermissao { get; set; }

        [ViewModelToModelAttribute]
		public String TituloMenu { get; set; }

        [ViewModelToModelAttribute]
		public String TituloTela { get; set; }

        [ViewModelToModelAttribute]
		public String Descricao { get; set; }

        [ViewModelToModelAttribute]
		public String Url { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? ExibeMenu { get; set; }

        [ViewModelToModelAttribute]
		public Boolean Ativo { get; set; }

        [ViewModelToModelAttribute]
		public String TituloAjuda { get; set; }

        [ViewModelToModelAttribute]
		public String CorpoAjuda { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsTelaPropriedade { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<CampoVM> Campos { get; set; }

        public class CampoVM {
            [ViewModelToModelAttribute]
            public Int32? IdTelaCampo { get; set; }

            [ViewModelToModelAttribute]
            public Int32 IdTela { get; set; }

            [ViewModelToModelAttribute]
            public String Campo { get; set; }

            [ViewModelToModelAttribute]
            public String TituloAjuda { get; set; }

            [ViewModelToModelAttribute]
            public String CorpoAjuda { get; set; }

            [ViewModelToModelAttribute]
            public Boolean Ativo { get; set; }
        }
    }
}