using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.TelaCampo
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public bool? Ativo { get; set; }

        public int? IdTelaCampo { get; set; }

        public int? IdTela { get; set; }

        public string Campo { get; set; }
    }
}