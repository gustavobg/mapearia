﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.TelaPropriedade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdTela { get; set; }
        public int? IdTelaPropriedade { get; set; }

        public string Descricao { get; set; }
        public string IdTelaPropriedadeIn { get; set; }

        public bool? Ativo { get; set; }
    }
}