﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tema
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public Int32? IdTema { get; set; }

        public String Descricao { get; set; }

        public Guid? ChaveVersao { get; set; }

        public String CorPrimaria { get; set; }

        public String CorSecundaria { get; set; }

        public String LogoTelaConexao { get; set; }

        public String LogoInterno { get; set; }

        public String VideoTelaConexao { get; set; }

        public Boolean? Ativo { get; set; }

    }
}