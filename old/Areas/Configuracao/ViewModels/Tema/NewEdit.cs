﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;

namespace HTM.MasterGestor.Web.UI.Areas.Configuracao.ViewModels.Tema
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            ArquivosDigitais = new List<ArquivoDigital>();
        }

        [ViewModelToModelAttribute]
        public Int32? IdTema { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string LogoTelaConexao { get; set; }

        [ViewModelToModelAttribute]
        public string LogoInterno { get; set; }

        [ViewModelToModelAttribute]
        public int? SetCores { get; set; }

        [ViewModelToModelAttribute]
        public string Titulo { get; set; }

        [ViewModelToModelAttribute]
        public string Favicon { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }


    }
}