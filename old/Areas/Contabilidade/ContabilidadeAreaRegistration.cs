﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade
{
    public class ContabilidadeAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Contabilidade";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Contabilidade_default",
                "Contabilidade/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
