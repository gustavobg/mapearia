﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomica;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class AtividadeEconomicaController : ControllerExtended
    {
        public AtividadeEconomicaController()
        {
            IndexUrl = "/Contabilidade/AtividadeEconomica/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                AtividadeEconomicaInfo info = AtividadeEconomicaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAtividadeEconomica, "AtividadeEconomica");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AtividadeEconomicaInfo info = new AtividadeEconomicaInfo();

            ViewModelToModelMapper.Map<AtividadeEconomicaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsAplicacoesTipo))
            {
                info.AplicacoesTipos = new List<AtividadeEconomicaAplicacaoTipoInfo>();

                CommaSeparatedToList(vm.IdsAplicacoesTipo).ForEach(idAplicacaoTipo =>
                {
                    AtividadeEconomicaAplicacaoTipoInfo x = new AtividadeEconomicaAplicacaoTipoInfo();
                    x.IdAplicacaoTipo = idAplicacaoTipo;

                    info.AplicacoesTipos.Add(x);
                });
            }

            var response = AtividadeEconomicaBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAtividadeEconomica.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdAtividadeEconomica, "AtividadeEconomica", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AtividadeEconomicaEmpresaInfo info = new AtividadeEconomicaEmpresaInfo();
            AtividadeEconomicaEmpresaInfo response = new AtividadeEconomicaEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaEmpresaInfo { IdAtividadeEconomica = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AtividadeEconomicaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomica, "AtividadeEconomica", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AtividadeEconomicaEmpresaInfo info = new AtividadeEconomicaEmpresaInfo();
            AtividadeEconomicaEmpresaInfo response = new AtividadeEconomicaEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaEmpresaInfo { IdAtividadeEconomica = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AtividadeEconomicaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomica, "AtividadeEconomica", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AtividadeEconomicaInfo info = new AtividadeEconomicaInfo(IdEmpresa);
            info.IdAtividadeEconomica = vm.IdAtividadeEconomica;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdAtividadeEconomicaIn = vm.IdAtividadeEconomicaIn;
            info.IdAtividadeEconomicaNotIn = vm.IdAtividadeEconomicaNotIn;
            info.IdAtividadeEconomicaSegmentoIn = vm.IdAtividadeEconomicaSegmentoIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AtividadeEconomicaInfo> retorno = new ListPaged<AtividadeEconomicaInfo>(AtividadeEconomicaBll.Instance.ListarPorParametrosComServicos(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:AtividadeEconomica", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListComAplicacaoTipo(ListVM vm)
        {
            AtividadeEconomicaInfo info = new AtividadeEconomicaInfo(IdEmpresa);
            info.IdAtividadeEconomica = vm.IdAtividadeEconomica;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdAtividadeEconomicaIn = vm.IdAtividadeEconomicaIn;
            info.IdAtividadeEconomicaNotIn = vm.IdAtividadeEconomicaNotIn;
            info.IdAtividadeEconomicaSegmentoIn = vm.IdAtividadeEconomicaSegmentoIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AtividadeEconomicaInfo> retorno = new ListPaged<AtividadeEconomicaInfo>(AtividadeEconomicaBll.Instance.ListarPorParametrosComAplicacaoTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Atividade Economica com Tipos de Aplicação", true);
        }
    }
}
