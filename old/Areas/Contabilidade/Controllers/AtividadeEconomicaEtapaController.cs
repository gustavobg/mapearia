﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaEtapa;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class AtividadeEconomicaEtapaController : ControllerExtended
    {
        public AtividadeEconomicaEtapaController()
        {
            IndexUrl = "/Contabilidade/AtividadeEconomicaEtapa/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                AtividadeEconomicaEtapaInfo info = AtividadeEconomicaEtapaBll.Instance.ListarPorIdCompleto(id.Value,IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAtividadeEconomicaEtapa, "AtividadeEconomicaEtapa");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AtividadeEconomicaEtapaInfo info = new AtividadeEconomicaEtapaInfo();

            ViewModelToModelMapper.Map<AtividadeEconomicaEtapaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            // monta lista de Servicos
            if (!string.IsNullOrEmpty(vm.IdsServicos))
            {
                info.Servicos = new List<AtividadeEconomicaEtapaProdutoServicoInfo>();

                CommaSeparatedToList(vm.IdsServicos).ForEach(idProdutoServico =>
                {
                    AtividadeEconomicaEtapaProdutoServicoInfo item = new AtividadeEconomicaEtapaProdutoServicoInfo();
                    item.IdProdutoServico = idProdutoServico;

                    info.Servicos.Add(item);
                });
            }

            // monta lista de Grupo de Produto
            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.Grupos = new List<AtividadeEconomicaEtapaProdutoServicoGrupoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(idProdutoServicoGrupo =>
                {
                    AtividadeEconomicaEtapaProdutoServicoGrupoInfo item = new AtividadeEconomicaEtapaProdutoServicoGrupoInfo();
                    item.IdProdutoServicoGrupo = idProdutoServicoGrupo;

                    info.Grupos.Add(item);
                });
            }

            var response = AtividadeEconomicaEtapaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAtividadeEconomicaEtapa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdAtividadeEconomicaEtapa, "AtividadeEconomicaEtapa", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AtividadeEconomicaEtapaEmpresaInfo info = new AtividadeEconomicaEtapaEmpresaInfo();
            AtividadeEconomicaEtapaEmpresaInfo response = new AtividadeEconomicaEtapaEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaEtapaEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaEtapaEmpresaInfo { IdAtividadeEconomicaEtapa = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AtividadeEconomicaEtapaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaEtapa, "IdAtividadeEconomica", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AtividadeEconomicaEtapaEmpresaInfo info = new AtividadeEconomicaEtapaEmpresaInfo();
            AtividadeEconomicaEtapaEmpresaInfo response = new AtividadeEconomicaEtapaEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaEtapaEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaEtapaEmpresaInfo { IdAtividadeEconomicaEtapa = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AtividadeEconomicaEtapaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaEtapa, "IdAtividadeEconomica", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AtividadeEconomicaEtapaInfo info = new AtividadeEconomicaEtapaInfo(IdEmpresa);
            info.IdAtividadeEconomicaEtapa = vm.IdAtividadeEconomicaEtapa;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdAtividadeEconomicaEtapaIn = vm.IdAtividadeEconomicaEtapaIn;
            info.IdAtividadeEconomicaEtapaNotIn = vm.IdAtividadeEconomicaEtapaNotIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AtividadeEconomicaEtapaInfo> retorno = new ListPaged<AtividadeEconomicaEtapaInfo>(AtividadeEconomicaEtapaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Atividade Economica Etapa", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListPorAtividadeEconomica(ListVM vm)
        {
            AtividadeEconomicaAtividadeEconomicaEtapaInfo info = new AtividadeEconomicaAtividadeEconomicaEtapaInfo();
            info.IdAtividadeEconomicaEtapa = vm.IdAtividadeEconomicaEtapa;
            info.IdEmpresa = IdEmpresa;
            info.DescricaoAtividadeEconomicaEtapa = vm.DescricaoAtividadeEconomicaEtapa;
            info.Ativo = vm.Ativo;
            info.IdAtividadeEconomicaEtapaIn = vm.IdAtividadeEconomicaEtapaIn;
            info.IdAtividadeEconomicaIn = vm.IdAtividadeEconomicaIn;

            ListPaged<AtividadeEconomicaAtividadeEconomicaEtapaInfo> retorno = new ListPaged<AtividadeEconomicaAtividadeEconomicaEtapaInfo>(AtividadeEconomicaAtividadeEconomicaEtapaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Etapas por Atividade Econômica", true);
        }
    }
}
