using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaPeriodo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class AtividadeEconomicaPeriodoController : ControllerExtended
    {
        public AtividadeEconomicaPeriodoController()
        {
            IndexUrl = "/Contabilidade/AtividadeEconomicaPeriodo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                var info = AtividadeEconomicaPeriodoBll.Instance.ListarCompletoPorId(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAtividadeEconomicaPeriodo, "AtividadeEconomicaPeriodo");
            }
            else
            {
                vm.Ativo = true;
            }

            if (vm.DataInicio.HasValue)
                vm.DataInicioExibicao = vm.DataInicio.Value.ToShortDateString();

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new AtividadeEconomicaPeriodoInfo();
            ViewModelToModelMapper.Map<AtividadeEconomicaPeriodoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão

            //Atividade Economica
            CommaSeparatedToList(vm.IdsAtividadeEconomica).ForEach(idAtividadeEconomica =>
            {
                info.AtividadesEconomicas.Add(new AtividadeEconomicaPeriodoAtividadeInfo()
                {
                    IdAtividadeEconomica = idAtividadeEconomica
                });
            });

            //Empresa Usuária Restriçao
            CommaSeparatedToList(vm.IdsPessoa).ForEach(idpessoa =>
            {
                info.PessoasRestritas.Add(new AtividadeEconomicaPeriodoPessoaInfo()
                {
                    IdPessoa = idpessoa
                });
            });

            if (!string.IsNullOrEmpty(vm.DataInicioExibicao))
                info.DataInicio = Convert.ToDateTime(vm.DataInicioExibicao);

            #endregion

            var response = AtividadeEconomicaPeriodoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAtividadeEconomicaPeriodo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAtividadeEconomicaPeriodo, "AtividadeEconomicaPeriodo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AtividadeEconomicaPeriodoEmpresaInfo info = new AtividadeEconomicaPeriodoEmpresaInfo();
            AtividadeEconomicaPeriodoEmpresaInfo response = new AtividadeEconomicaPeriodoEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaPeriodoEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaPeriodoEmpresaInfo { IdAtividadeEconomicaPeriodo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AtividadeEconomicaPeriodoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaPeriodo, "AtividadeEconomicaPeriodo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AtividadeEconomicaPeriodoEmpresaInfo info = new AtividadeEconomicaPeriodoEmpresaInfo();
            AtividadeEconomicaPeriodoEmpresaInfo response = new AtividadeEconomicaPeriodoEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaPeriodoEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaPeriodoEmpresaInfo { IdAtividadeEconomicaPeriodo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AtividadeEconomicaPeriodoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaPeriodo, "AtividadeEconomicaPeriodo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new AtividadeEconomicaPeriodoInfo(IdEmpresa);
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Sigla = vm.Sigla;
            info.IdAtividadeEconomicaPeriodoIn = vm.IdAtividadeEconomicaPeriodoIn;
            info.IdAtividadeEconomicaPeriodoNotIn = vm.IdAtividadeEconomicaPeriodoNotIn;
            info.IdAtividadeEconomicaPeriodoPai = vm.IdAtividadeEconomicaPeriodoPai;
            info.IdAtividadeEconomicaPeriodoPaiIn = vm.IdAtividadeEconomicaPeriodoPaiIn;
            info.Ativo = vm.Ativo;
            info.UltimoNivel = vm.UltimoNivel;
            info.ComAtividadesEconomica = vm.ComAtividadesEconomica;
            info.IdAtividadeEconomicaIn = vm.IdAtividadeEconomicaIn;
            info.RegistroProprio = vm.RegistroProprio;

            var retorno = new ListPaged<AtividadeEconomicaPeriodoInfo>(AtividadeEconomicaPeriodoBll.Instance.ListarHierarquicamente(info, info.ComAtividadesEconomica))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            foreach (var periodoAtividade in retorno)
            {
                var vigencia = string.Format("{0} à {1}", periodoAtividade.DataInicio.Value.ToShortDateString(), periodoAtividade.DataTermino.Value.ToShortDateString());
                var descricaoDetalhada = string.Format("{0} ({1}) - {2}", periodoAtividade.DescricaoCompleta, periodoAtividade.Sigla, vigencia);
                periodoAtividade.Vigencia = vigencia;
                periodoAtividade.DescricaoDetalhada = descricaoDetalhada;
            }

            return base.CreateListResult(vm, retorno, "Exportação: AtividadeEconomicaPeriodo", true);
        }

    }
}