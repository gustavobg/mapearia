﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaProdutoServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class AtividadeEconomicaProdutoServicoController : ControllerExtended
    {
        
        public AtividadeEconomicaProdutoServicoController()
        {

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AtividadeEconomicaProdutoServicoInfo info = new AtividadeEconomicaProdutoServicoInfo();
            info.IdAtividadeEconomica = vm.IdAtividadeEconomica;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.DescricaoProdutoServico = vm.DescricaoProdutoServico;
            info.Natureza = vm.Natureza;
            info.PermiteProducao = vm.PermiteProducao;
            info.DescricaoProdutoServicoBusca = vm.DescricaoProdutoServicoBusca;
            info.IdProdutoServicoIn = vm.IdProdutoServicoIn;

            var retorno = new ListPaged<AtividadeEconomicaProdutoServicoInfo>();
            retorno = new ListPaged<AtividadeEconomicaProdutoServicoInfo>(AtividadeEconomicaProdutoServicoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Produto Serviço Atividade Econômica", true);

        }

    }
}
