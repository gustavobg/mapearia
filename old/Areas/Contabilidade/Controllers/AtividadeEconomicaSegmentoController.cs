﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaSegmento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class AtividadeEconomicaSegmentoController : ControllerExtended
    {
        public AtividadeEconomicaSegmentoController()
        {
            IndexUrl = "/Contabilidade/AtividadeEconomicaSegmento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                AtividadeEconomicaSegmentoInfo info = AtividadeEconomicaSegmentoBll.Instance.ListarPorParametros(new AtividadeEconomicaSegmentoInfo { IdAtividadeEconomicaSegmento = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAtividadeEconomicaSegmento, "AtividadeEconomicaSegmento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AtividadeEconomicaSegmentoInfo info = new AtividadeEconomicaSegmentoInfo();

            ViewModelToModelMapper.Map<AtividadeEconomicaSegmentoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = info.IdEmpresa = IdEmpresa;
            var response = AtividadeEconomicaSegmentoBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAtividadeEconomicaSegmento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdAtividadeEconomicaSegmento, "AtividadeEconomicaSegmento", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            AtividadeEconomicaSegmentoEmpresaInfo info = new AtividadeEconomicaSegmentoEmpresaInfo();
            AtividadeEconomicaSegmentoEmpresaInfo response = new AtividadeEconomicaSegmentoEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaSegmentoEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaSegmentoEmpresaInfo { IdAtividadeEconomicaSegmento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = AtividadeEconomicaSegmentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaSegmento, "AtividadeEconomicaSegmento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            AtividadeEconomicaSegmentoEmpresaInfo info = new AtividadeEconomicaSegmentoEmpresaInfo();
            AtividadeEconomicaSegmentoEmpresaInfo response = new AtividadeEconomicaSegmentoEmpresaInfo();

            if (id > 0)
            {
                info = AtividadeEconomicaSegmentoEmpresaBll.Instance.ListarPorParametros(new AtividadeEconomicaSegmentoEmpresaInfo { IdAtividadeEconomicaSegmento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = AtividadeEconomicaSegmentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdAtividadeEconomicaSegmento, "AtividadeEconomicaSegmento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AtividadeEconomicaSegmentoInfo info = new AtividadeEconomicaSegmentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdAtividadeEconomicaSegmento = vm.IdAtividadeEconomicaSegmento;
            info.IdAtividadeEconomicaSegmentoIn = vm.IdAtividadeEconomicaSegmentoIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<AtividadeEconomicaSegmentoInfo> retorno = new ListPaged<AtividadeEconomicaSegmentoInfo>(AtividadeEconomicaSegmentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Segmento de Atividade Econômica", true);

        }
    }
}
