﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CFOP;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class CFOPController : ControllerExtended
    {
        public CFOPController()
        {
            IndexUrl = "/Contabilidade/CFOP/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FiscalCFOPInfo info = FiscalCFOPBll.Instance.ListarPorParametros(new FiscalCFOPInfo { IdFiscalCFOP = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFiscalCFOP, "FiscalCFOP");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FiscalCFOPInfo info = new FiscalCFOPInfo();

            ViewModelToModelMapper.Map<FiscalCFOPInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            info.lstCFOPDevolucao = new List<FiscalCFOPDevolucaoItemInfo>();

            //CFOP Tipo Estadual = 1
            if (!string.IsNullOrEmpty(vm.IdsFiscalCFOPEstadual))
            {
                CommaSeparatedToList(vm.IdsFiscalCFOPEstadual).ForEach(idfiscalCFOP =>
                {
                    FiscalCFOPDevolucaoItemInfo item = new FiscalCFOPDevolucaoItemInfo();
                    item.IdFiscalCFOPDevolucao = idfiscalCFOP;
                    item.TipoOperacao = 1;

                    info.lstCFOPDevolucao.Add(item);
                });
            }

            //CFOP Tipo Interestadual = 2
            if (!string.IsNullOrEmpty(vm.IdsFiscalCFOPInterestadual))
            {
                CommaSeparatedToList(vm.IdsFiscalCFOPInterestadual).ForEach(idfiscalCFOP =>
                {
                    FiscalCFOPDevolucaoItemInfo item = new FiscalCFOPDevolucaoItemInfo();
                    item.IdFiscalCFOPDevolucao = idfiscalCFOP;
                    item.TipoOperacao = 2;

                    info.lstCFOPDevolucao.Add(item);
                });
            }

            //CFOP Tipo Internacional = 3
            if (!string.IsNullOrEmpty(vm.IdsFiscalCFOPInternacioanal))
            {
                CommaSeparatedToList(vm.IdsFiscalCFOPInternacioanal).ForEach(idfiscalCFOP =>
                {
                    FiscalCFOPDevolucaoItemInfo item = new FiscalCFOPDevolucaoItemInfo();
                    item.IdFiscalCFOPDevolucao = idfiscalCFOP;
                    item.TipoOperacao = 3;

                    info.lstCFOPDevolucao.Add(item);
                });
            }


            var response = FiscalCFOPBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFiscalCFOP.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFiscalCFOP, "FiscalCFOP", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FiscalCFOPEmpresaInfo info = new FiscalCFOPEmpresaInfo();
            FiscalCFOPEmpresaInfo response = new FiscalCFOPEmpresaInfo();

            if (id > 0)
            {
                info = FiscalCFOPEmpresaBll.Instance.ListarPorParametros(new FiscalCFOPEmpresaInfo { IdFiscalCFOP = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FiscalCFOPEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalCFOP, "FiscalCFOP", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FiscalCFOPEmpresaInfo info = new FiscalCFOPEmpresaInfo();
            FiscalCFOPEmpresaInfo response = new FiscalCFOPEmpresaInfo();

            if (id > 0)
            {
                info = FiscalCFOPEmpresaBll.Instance.ListarPorParametros(new FiscalCFOPEmpresaInfo { IdFiscalCFOP = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FiscalCFOPEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalCFOP, "FiscalCFOP", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FiscalCFOPInfo info = new FiscalCFOPInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdFiscalCFOP = vm.IdFiscalCFOP;
            info.IndiceEstadual = vm.IndiceEstadual;
            info.IndiceInterestadual = vm.IndiceInterestadual;
            info.IndiceInternacional = vm.IndiceInternacional;
            info.RegistroProprio = vm.RegistroProprio;

            #region Verificações
            if (!string.IsNullOrEmpty(info.IndiceEstadual))
                info.IndiceEstadual += ".%";

            if (!string.IsNullOrEmpty(info.IndiceInterestadual))
                info.IndiceInterestadual += ".%";

            if (!string.IsNullOrEmpty(info.IndiceInternacional))
                info.IndiceInternacional += ".%";
            #endregion

            ListPaged<FiscalCFOPInfo> retorno = new ListPaged<FiscalCFOPInfo>(FiscalCFOPBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CFOP", true);

        }

    }
}
