﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CentroCusto;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class CentroCustoController : ControllerExtended
    {
        public CentroCustoController()
        {
            IndexUrl = "/Contabilidade/CentroCusto/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CentroCustoInfo info = new CentroCustoInfo();

            ViewModelToModelMapper.Map<CentroCustoInfo>(vm, info);

            CommaSeparatedToList(vm.IdsPessoasRestritas).ForEach(pessoaid =>
                {
                    CentroCustoPessoaRestricaoInfo centroCustoPessoaRestricao = new CentroCustoPessoaRestricaoInfo();
                    centroCustoPessoaRestricao.IdPessoa = pessoaid;

                    info.RestricoesCentroCustoPessoa.Add(centroCustoPessoaRestricao);
                });

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = CentroCustoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            {
                SalvarHistoricoAcesso(IndexUrl, vm.IdCentroCusto.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCentroCusto, "CentroCusto", info.Response.IdHistorico);
                if (vm.SaveNewCrud)
                {
                    vm = new NewEditVM();
                    vm.Response = response.Response;

                    vm.SaveNewCrud = true;
                    vm.Ativo = true;
                    vm.IdPlanoContasVersao = info.IdPlanoContasVersao;
                    return Json(vm);
                }
                return Json(response);
            }
            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CentroCustoInfo info = CentroCustoBll.Instance.ListarPorIdCompleto(new CentroCustoInfo { IdCentroCusto = id });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCentroCusto, "CentroCusto");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CentroCustoInfo info = new CentroCustoInfo();
            info.IdCentroCusto = vm.IdCentroCusto;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.Codigo = vm.Codigo;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.UltimoNivel = vm.UltimoNivel;

            if (vm.ComRestricaoUsuario.HasValue && vm.ComRestricaoUsuario.Value)
            {
                var lstCentroCustoRestrito = UsuarioCentroCustoRestricaoBll.Instance.ListarPorParametros(new UsuarioCentroCustoRestricaoInfo { IdUsuario = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario.Value });
                info.IdCentroCustoNotIn = string.Join(",", lstCentroCustoRestrito.Select(x => x.IdCentroCusto));
            }

            ListPaged<CentroCustoInfo> retorno = new ListPaged<CentroCustoInfo>(CentroCustoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CentroCusto", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarHierarquicamentePorParametro(ListVM vm)
        {
            CentroCustoInfo info = new CentroCustoInfo();
            info.IdCentroCusto = vm.IdCentroCusto;
            info.IdCentroCustoIn = vm.IdCentroCustoIn;
            info.IdEmpresa = IdEmpresa;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.UltimoNivel = vm.UltimoNivel;
            info.DescricaoCompleta = vm.DescricaoCompleta;

            if (vm.ComRestricaoUsuario.HasValue && vm.ComRestricaoUsuario.Value)
            {
                var lstCentroCustoRestrito = UsuarioCentroCustoRestricaoBll.Instance.ListarPorParametros(new UsuarioCentroCustoRestricaoInfo { IdUsuario = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario.Value });
                info.IdCentroCustoNotIn = string.Join(",", lstCentroCustoRestrito.Select(x => x.IdCentroCusto));
            }

            ListPaged<CentroCustoInfo> retorno = new ListPaged<CentroCustoInfo>(CentroCustoBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:PlanoContas", true);

        }

        public string RetornaCodigoSuperior(string codigo)
        {
            if (!string.IsNullOrEmpty(codigo))
            {
                if (codigo.Contains("_"))
                {
                    string codigotemp = codigo;
                    codigo = string.Empty;
                    codigo = codigotemp.Replace("_", "0");
                }
                string codigoSuperior = string.Empty;
                string codigoComposicao = string.Empty;
                var codigoArray = codigo.Split('.');
                string codigoSanada = string.Empty;
                string codigoSuperiorRetorno = string.Empty;

                int quantidade = 0;
                string zeros = string.Empty;
                foreach (var item in codigoArray)
                {
                    quantidade = item.Length;
                    zeros = zeros.PadLeft(quantidade, '0');

                    if (item == zeros)
                        break;

                    codigoComposicao += item + ".";
                }

                codigoSanada = codigoComposicao.Remove(codigoComposicao.Length - 1);
                var codigoSanadaArray = codigoSanada.Split('.');
                int codigodor = 0;
                foreach (var item in codigoSanadaArray)
                {
                    codigodor += 1;
                    if (codigoSanadaArray.Length > codigodor)
                    {
                        codigoSuperiorRetorno += item + ".";
                    }
                }
                //Não tiro mais o ponto final, pois, ajuda no select(desta forma busco o item superior mais fácil)
                //return codigoSuperiorRetorno.Remove(codigoSuperiorRetorno.Length - 1);
                return codigoSuperiorRetorno;
            }
            else
            {
                return string.Empty;
            }

        }

    }
}
