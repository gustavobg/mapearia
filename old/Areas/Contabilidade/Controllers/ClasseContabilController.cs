﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ClasseContabil;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ClasseContabilController : ControllerExtended
    {
        public ClasseContabilController()
        {
            IndexUrl = "/Contabilidade/ClasseContabil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ClasseContabilInfo info = new ClasseContabilInfo();
                info = ClasseContabilBll.Instance.ListarPorParametros(new ClasseContabilInfo { IdClasseContabil = id.Value,IdEmpresa = IdEmpresa }).FirstOrDefault();

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdClasseContabil, "ClasseContabil");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ClasseContabilInfo info = new ClasseContabilInfo();
            ViewModelToModelMapper.Map<ClasseContabilInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = ClasseContabilBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdClasseContabil.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdClasseContabil, "ClasseContabil", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ClasseContabilEmpresaInfo info = new ClasseContabilEmpresaInfo();
            ClasseContabilEmpresaInfo response = new ClasseContabilEmpresaInfo();

            if (id > 0)
            {
                info = ClasseContabilEmpresaBll.Instance.ListarPorParametros(new ClasseContabilEmpresaInfo { IdClasseContabil = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ClasseContabilEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdClasseContabil, "ClasseContabil", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ClasseContabilEmpresaInfo info = new ClasseContabilEmpresaInfo();
            ClasseContabilEmpresaInfo response = new ClasseContabilEmpresaInfo();

            if (id > 0)
            {
                info = ClasseContabilEmpresaBll.Instance.ListarPorParametros(new ClasseContabilEmpresaInfo { IdClasseContabil = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ClasseContabilEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdClasseContabil, "ClasseContabil", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ClasseContabilInfo info = new ClasseContabilInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdClasseContabil = vm.IdClasseContabil;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ClasseContabilInfo> retorno = new ListPaged<ClasseContabilInfo>(ClasseContabilBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Classe Contábil", true);
        }

    }
}
