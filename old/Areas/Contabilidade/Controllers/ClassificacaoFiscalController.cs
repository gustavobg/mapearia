﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ClassificacaoFiscal;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ClassificacaoFiscalController : ControllerExtended
    {
        public ClassificacaoFiscalController()
        {
            IndexUrl = "/Contabilidade/ClassificacaoFiscal/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FiscalClassificacaoInfo info = FiscalClassificacaoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFiscalClassificacao, "FiscalClassificacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FiscalClassificacaoInfo info = new FiscalClassificacaoInfo();

            ViewModelToModelMapper.Map<FiscalClassificacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = FiscalClassificacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFiscalClassificacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFiscalClassificacao, "FinanceiroClassificacao", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FiscalClassificacaoEmpresaInfo info = new FiscalClassificacaoEmpresaInfo();
            FiscalClassificacaoEmpresaInfo response = new FiscalClassificacaoEmpresaInfo();

            if (id > 0)
            {
                info = FiscalClassificacaoEmpresaBll.Instance.ListarPorParametros(new FiscalClassificacaoEmpresaInfo { IdFiscalClassificacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FiscalClassificacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalClassificacao, "FiscalClassificacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FiscalClassificacaoEmpresaInfo info = new FiscalClassificacaoEmpresaInfo();
            FiscalClassificacaoEmpresaInfo response = new FiscalClassificacaoEmpresaInfo();

            if (id > 0)
            {
                info = FiscalClassificacaoEmpresaBll.Instance.ListarPorParametros(new FiscalClassificacaoEmpresaInfo { IdFiscalClassificacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FiscalClassificacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalClassificacao, "FiscalClassificacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FiscalClassificacaoInfo info = new FiscalClassificacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFiscalClassificacao = vm.IdFiscalClassificacao;
            info.IdFiscalClassificacaoIn = vm.IdFiscalClassificacaoIn;
            info.Descricao = vm.Descricao;
            info.Natureza = vm.Natureza;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FiscalClassificacaoInfo> retorno = new ListPaged<FiscalClassificacaoInfo>(FiscalClassificacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Classificação Fiscal", true);
        }

    }
}
