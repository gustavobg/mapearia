﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ContaLancamentoContabil;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using MasterGestor.CORE.Nucleo.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ContaLancamentoContabilController : ControllerExtended
    {
        public ContaLancamentoContabilController()
        {
            IndexUrl = "/Contabilidade/ParametroLancamentoContabil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            //var info = new ProducaoCulturaBll().ListarPorCodigo(parentId);
            //vm.DescricaoExibicao = info.Descricao;

            try
            {
                vm.ParentId = parentId;
                string[] Chave = parentId.Split('-');
                vm.IdTransacao = int.Parse(Chave[0]);
                vm.TransacaoAcaoTipo = int.Parse(Chave[1]);
                vm.DirecionadorTipo = int.Parse(Chave[2]);
                vm.IdClasseContabil = int.Parse(Chave[3]);
            }
            catch (Exception)
            {
                //TODO
            }

            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ContaLancamentoContabilInfo info = new ContaLancamentoContabilInfo();

            ViewModelToModelMapper.Map<ContaLancamentoContabilInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            #region Validação
            if (!string.IsNullOrEmpty(vm.ParentId))
            {
                try
                {
                    string[] Chave = vm.ParentId.Split('-');
                    info.IdTransacao = int.Parse(Chave[0]);
                    info.TransacaoAcaoTipo = int.Parse(Chave[1]);
                    info.DirecionadorTipo = int.Parse(Chave[2]);
                    info.IdClasseContabil = int.Parse(Chave[3]);
                }
                catch (Exception)
                {
                    ContaLancamentoContabilInfo responseError = new ContaLancamentoContabilInfo();
                    responseError.Response.AdicionarMensagem("Operação inválida.");
                    responseError.Response.Identificador = BaseInfoResponse.IdentificadorEnum.Erro;
                    responseError.Response.Sucesso = false;
                    return Json(responseError);
                }
            }
            else
            {
                ContaLancamentoContabilInfo responseError = new ContaLancamentoContabilInfo();
                responseError.Response.AdicionarMensagem("Operação inválida.");
                responseError.Response.Identificador = BaseInfoResponse.IdentificadorEnum.Erro;
                responseError.Response.Sucesso = false;
                return Json(responseError);
            }
            #endregion

            var response = ContaLancamentoContabilBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdContaLancamentoContabil.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdContaLancamentoContabil, "ContaLancamentoContabil", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(string parentId)
        {
            NewEditVM vm = new NewEditVM();
            vm.ParentId = parentId;
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ContaLancamentoContabilInfo info = new ContaLancamentoContabilInfo();

                info = ContaLancamentoContabilBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdContaLancamentoContabil, "ContaLancamentoContabil");
            }
            else
            {
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ContaLancamentoContabilInfo info = new ContaLancamentoContabilInfo();
            info.IdContaLancamentoContabil = vm.IdContaLancamentoContabil;

            if (!string.IsNullOrEmpty(vm.ParentId))
            {
                string[] Chave = vm.ParentId.Split('-');
                info.IdTransacao = int.Parse(Chave[0]);
                info.TransacaoAcaoTipo = int.Parse(Chave[1]);
                info.DirecionadorTipo = int.Parse(Chave[2]);
                info.IdClasseContabil = int.Parse(Chave[3]);
            }

            ListPaged<ContaLancamentoContabilInfo> retorno = new ListPaged<ContaLancamentoContabilInfo>(ContaLancamentoContabilBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Conta de Lançamento Contábil", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListContasAgrupadasPorLancamento(ListVM vm)
        {
            ContaLancamentoContabilInfo info = new ContaLancamentoContabilInfo();
            info.IdContaLancamentoContabil = vm.IdContaLancamentoContabil;

            if (!string.IsNullOrEmpty(vm.ParentId))
            {
                string[] Chave = vm.ParentId.Split('-');
                info.IdTransacao = int.Parse(Chave[0]);
                info.TransacaoAcaoTipo = int.Parse(Chave[1]);
                info.DirecionadorTipo = int.Parse(Chave[2]);
                info.IdClasseContabil = int.Parse(Chave[3]);
            }

            ListPaged<ContaLancamentoContabilInfo> retorno = new ListPaged<ContaLancamentoContabilInfo>(ContaLancamentoContabilBll.Instance.RetornaContasAgrupadaPorDirecionadorDeLancamentoContabil(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Conta de Lançamento Contábil", true);
        }
    }
}
