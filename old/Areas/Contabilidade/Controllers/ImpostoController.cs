﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.Imposto;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ImpostoController : ControllerExtended
    {

        public ImpostoController()
        {
            IndexUrl = "/Contabilidade/Imposto/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FiscalImpostoInfo info = FiscalImpostoBll.Instance.ListarPorParametros(new FiscalImpostoInfo { IdEmpresa = IdEmpresa, IdFiscalImposto = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFiscalImposto, "FiscalImposto");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FiscalImpostoInfo info = new FiscalImpostoInfo();

            ViewModelToModelMapper.Map<FiscalImpostoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = FiscalImpostoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFiscalImposto.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFiscalImposto, "FiscalImposto", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FiscalImpostoEmpresaInfo info = new FiscalImpostoEmpresaInfo();
            FiscalImpostoEmpresaInfo response = new FiscalImpostoEmpresaInfo();

            if (id > 0)
            {
                info = FiscalImpostoEmpresaBll.Instance.ListarPorParametros(new FiscalImpostoEmpresaInfo { IdFiscalImposto = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FiscalImpostoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalImposto, "FiscalImposto", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FiscalImpostoEmpresaInfo info = new FiscalImpostoEmpresaInfo();
            FiscalImpostoEmpresaInfo response = new FiscalImpostoEmpresaInfo();

            if (id > 0)
            {
                info = FiscalImpostoEmpresaBll.Instance.ListarPorParametros(new FiscalImpostoEmpresaInfo { IdFiscalImposto = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FiscalImpostoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFiscalImposto, "FiscalImposto", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FiscalImpostoInfo info = new FiscalImpostoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdFiscalImposto = vm.IdFiscalImposto;
            info.ImpostoSobre = vm.ImpostoSobre == 0 ? null : vm.ImpostoSobre;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FiscalImpostoInfo> retorno = new ListPaged<FiscalImpostoInfo>(FiscalImpostoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:FiscalImposto", true);

        }

    }
}
