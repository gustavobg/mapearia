﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.NCM;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class NCMController : ControllerExtended
    {
        public NCMController()
        {
            IndexUrl = "/Contabilidade/NCM/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FiscalNCMInfo info = new FiscalNCMInfo();

            ViewModelToModelMapper.Map<FiscalNCMInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FiscalNCMBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
            {
                SalvarHistoricoAcesso(IndexUrl, vm.IdFiscalNCM.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFiscalNCM, "FiscalNCM", info.Response.IdHistorico);
                if(vm.SaveNewCrud)
                {
                    vm = new NewEditVM();
                    vm.Response = response.Response;

                    vm.IdPlanoContasVersao = info.IdPlanoContasVersao;
                    vm.SaveNewCrud = true;
                    vm.Ativo = true;
                    return Json(vm);
                }
                return Json(response);
            }
            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FiscalNCMInfo info = FiscalNCMBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFiscalNCM, "FiscalNCM");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FiscalNCMInfo info = new FiscalNCMInfo();
            info.Descricao = vm.Descricao;
            info.IdFiscalNCM = vm.IdFiscalNCM;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.UltimoNivel = vm.UltimoNivel;
            info.CodigoDescricao = vm.CodigoDescricao;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;

            ListPaged<FiscalNCMInfo> retorno = new ListPaged<FiscalNCMInfo>(FiscalNCMBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:FiscalNCM", true);

        }
    }
}
