﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.Operacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Bll.Contabilidade;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class OperacaoController : ControllerExtended
    {
        public OperacaoController()
        {
            IndexUrl = "/Contabilidade/Operacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            OperacaoInfo info = new OperacaoInfo();

            ViewModelToModelMapper.Map<OperacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            var response = OperacaoBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdOperacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdOperacao, "Operacao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                OperacaoInfo info = OperacaoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdOperacao, "Operacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new OperacaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdModuloIn = vm.IdModuloIn;
            info.Ativo = vm.Ativo;
            info.IdOperacaoIn = vm.IdOperacaoIn;
            info.IdModuloIn = vm.IdModuloIn;

            var retorno = new ListPaged<OperacaoInfo>(OperacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            if (retorno.Count() > 0)
            {
                foreach (var item in retorno)
                {
                    if (item.IdModulo.HasValue)
                        item.DescricaoModulo = ModuloBll.Instance.ListarHierarquicamentePorParametro(new ModuloInfo { IdModulo = item.IdModulo }).Select(p => p.DescricaoCompleta).First();
                }
            }

            return base.CreateListResult(vm, retorno, "Exportação:Operacao", true);
        }
    }
}
