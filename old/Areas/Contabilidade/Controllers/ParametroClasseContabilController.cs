﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ParametroLancamentoContabil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Bll.Contabilidade;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ParametroClasseContabilController : ControllerExtended
    {

        public ParametroClasseContabilController()
        {
            IndexUrl = "/Contabilidade/ParametroLancamentoContabil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            try
            {
                string[] parents = parentId.Split('-');

                vm.Chave = parentId;
                vm.IdTransacao = int.Parse(parents[0].ToString());
                vm.TransacaoAcaoTipo = int.Parse(parents[1].ToString());
                vm.DirecionadorTipo = int.Parse(parents[2].ToString());

            }
            catch (Exception)
            {

            }

            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ParametroLancamentoContabilInfo info = new ParametroLancamentoContabilInfo();

            ViewModelToModelMapper.Map<ParametroLancamentoContabilInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = ParametroLancamentoContabilBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdParametroLancamentoContabil.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdParametroLancamentoContabil, "ParametroLancamentoContabil", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(string parentId)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ParametroLancamentoContabilInfo info = new ParametroLancamentoContabilInfo();

                info = ParametroLancamentoContabilBll.Instance.ListaConfiguracaoCompletaPorId(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdParametroLancamentoContabil, "ParametroLancamentoContabil");
            }
            else
            {
                vm.DirecionadorPersonalizado = false;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ParametroLancamentoContabilInfo info = new ParametroLancamentoContabilInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdTransacao = vm.IdTransacao;
            info.TransacaoAcaoTipo = vm.TransacaoAcaoTipo;
            info.DirecionadorTipo = vm.DirecionadorTipo;

            ListPaged<ParametroLancamentoContabilInfo> retorno = new ListPaged<ParametroLancamentoContabilInfo>(ParametroLancamentoContabilBll.Instance.ListarPorParametrosVerificandoConfiguracao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Parametros para Lançamento Contábil", true);
        }

    }
}
