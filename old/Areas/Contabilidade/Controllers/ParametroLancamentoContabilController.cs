﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ParametroLancamentoContabil;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class ParametroLancamentoContabilController : ControllerExtended
    {
        public ParametroLancamentoContabilController()
        {
            IndexUrl = "/Contabilidade/ParametroLancamentoContabil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ParametroLancamentoContabilInfo info = new ParametroLancamentoContabilInfo();
            info.IdEmpresa = IdEmpresa;
            

            ListPaged<ParametroLancamentoContabilInfo> retorno = new ListPaged<ParametroLancamentoContabilInfo>(ParametroLancamentoContabilBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Parametros para Lançamento Contábil", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListTransacaoAcaoTipoPorEmpresa(ListVM vm)
        {
            ParametroLancamentoContabilInfo info = new ParametroLancamentoContabilInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdTransacao = vm.IdTransacao;
            info.TransacaoAcaoTipo = vm.TransacaoAcaoTipo;
            info.DirecionadorTipo = vm.DirecionadorTipo;
            info.DirecionadorPersonalizado = vm.DirecionadorPersonalizado;
            info.IdTransacaoIn = vm.IdTransacaoIn;

            ListPaged<ParametroLancamentoContabilInfo> retorno = new ListPaged<ParametroLancamentoContabilInfo>(ParametroLancamentoContabilBll.Instance.ListaTransacaoAcaoTipoPorEmpresa(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Parametros para Lançamento Contábil", true);
        }
    }
}
