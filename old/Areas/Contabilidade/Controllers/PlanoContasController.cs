﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.PlanoContas;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Library.ExtensionMethod;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Bll.Contabilidade;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class PlanoContasController : ControllerExtended
    {
        public PlanoContasController()
        {
            IndexUrl = "/Contabilidade/PlanoContas/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PlanoContasInfo info = new PlanoContasInfo();

            ViewModelToModelMapper.Map<PlanoContasInfo>(vm, info);
            info.Conta = vm.Conta.Replace('_', '0');

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.IdsPessoa))
            {
                info.lstPessoaEmpresaRestrita = new List<PlanoContasPessoaRestricaoInfo>();
                CommaSeparatedToList(vm.IdsPessoa).ForEach(idPessoa =>
                {
                    PlanoContasPessoaRestricaoInfo item = new PlanoContasPessoaRestricaoInfo();
                    item.IdPessoa = idPessoa;

                    info.lstPessoaEmpresaRestrita.Add(item);
                });
            }
            #endregion

            var response = PlanoContasBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            {
                SalvarHistoricoAcesso(IndexUrl, vm.IdPlanoContas.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdPlanoContas, "PlanoContas", info.Response.IdHistorico);
                if (vm.SaveNewCrud)
                {
                    vm = new NewEditVM();
                    vm.Response = response.Response;

                    vm.SaveNewCrud = true;
                    vm.Ativo = true;
                    vm.IdPlanoContasVersao = info.IdPlanoContasVersao;
                    return Json(vm);
                }
                return Json(response);
            }
            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                PlanoContasInfo info = PlanoContasBll.Instance.ListarPorIdPlanoContasCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPlanoContas, "PlanoContas");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PlanoContasInfo info = new PlanoContasInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPlanoContas = vm.IdPlanoContas;
            info.IdPlanoContasPai = vm.IdPlanoContasPai;
            info.PlanoContasTipo = vm.PlanoContasTipo;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Conta = vm.Conta;
            info.Ativo = vm.Ativo;
            info.CentroCustoObrigatorio = vm.CentroCustoObrigatorio;
            info.UltimoNivel = vm.UltimoNivel;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<PlanoContasInfo> retorno = new ListPaged<PlanoContasInfo>(PlanoContasBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação:PlanoContas");

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarHierarquicamentePorParametro(ListVM vm)
        {
            PlanoContasInfo info = new PlanoContasInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPlanoContas = vm.IdPlanoContas;
            info.IdPlanoContasIn = vm.IdPlanoContasIn;
            info.IdPlanoContasPai = vm.IdPlanoContasPai;
            info.PlanoContasTipo = vm.PlanoContasTipo;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Conta = vm.Conta;
            info.Ativo = vm.Ativo;
            info.CentroCustoObrigatorio = vm.CentroCustoObrigatorio;
            info.UltimoNivel = vm.UltimoNivel;

            ListPaged<PlanoContasInfo> retorno = new ListPaged<PlanoContasInfo>(PlanoContasBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:PlanoContas", true);

        }

        public string RetornaContaSuperior(string conta)
        {
            if (!string.IsNullOrEmpty(conta))
            {
                if (conta.Contains("_"))
                {
                    string contatemp = conta;
                    conta = string.Empty;
                    conta = contatemp.Replace("_", "0");
                }
                string contaSuperior = string.Empty;
                string contaComposicao = string.Empty;
                var contaArray = conta.Split('.');
                string contaSanada = string.Empty;
                string contaSuperiorRetorno = string.Empty;

                int quantidade = 0;
                string zeros = string.Empty;
                foreach (var item in contaArray)
                {
                    quantidade = item.Length;
                    zeros = zeros.PadLeft(quantidade, '0');

                    if (item == zeros)
                        break;

                    contaComposicao += item + ".";
                }

                contaSanada = contaComposicao.Remove(contaComposicao.Length - 1);
                var contaSanadaArray = contaSanada.Split('.');
                int contador = 0;
                foreach (var item in contaSanadaArray)
                {
                    contador += 1;
                    if (contaSanadaArray.Length > contador)
                    {
                        contaSuperiorRetorno += item + ".";
                    }
                    //else
                    //{
                    //    int totalCaracter = item.Length;

                    //    for (int i = 0; i < item.Length; i++)
                    //    {
                    //        if (item[i].ToString() == "0")
                    //        {

                    //        }
                    //        if (item[i].ToString() == "1")
                    //        {

                    //        }
                    //        if (int.Parse(item[i].ToString()) > 1)
                    //        {
                    //            contaSuperiorRetorno += int.Parse(item) - 1 + ".";
                    //        }
                    //    }
                    //}

                }
                //Não tiro mais o ponto final, pois, ajuda no select(desta forma busco o item superior mais fácil)
                //return contaSuperiorRetorno.Remove(contaSuperiorRetorno.Length - 1);
                return contaSuperiorRetorno;
            }
            else
            {
                return string.Empty;
            }

        }
    }
}
