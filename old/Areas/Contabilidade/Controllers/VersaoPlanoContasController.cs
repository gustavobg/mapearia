﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.VersaoPlanoContas;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.Controllers
{
    public class VersaoPlanoContasController : ControllerExtended
    {
        public VersaoPlanoContasController()
        {
            IndexUrl = "/Contabilidade/VersaoPlanoContas/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PlanoContasVersaoInfo info = new PlanoContasVersaoInfo();

            ViewModelToModelMapper.Map<PlanoContasVersaoInfo>(vm, info);

            if (info.PlanoContasTipo == 3)
                info.IdEmpresa = null;
            else
                info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.DataInicioExibicao))
                info.Inicio = Convert.ToDateTime(vm.DataInicioExibicao);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = PlanoContasVersaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPlanoContasVersao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdPlanoContasVersao, "PlanoContasVersao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            vm.Ativo = true;
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                PlanoContasVersaoInfo info = PlanoContasVersaoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPlanoContasVersao, "PlanoContasVersao");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PlanoContasVersaoInfo info = new PlanoContasVersaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.PlanoContasTipo = vm.PlanoContasTipo;
            info.ComNCM = vm.ComNCM;

            ListPaged<PlanoContasVersaoInfo> retorno = new ListPaged<PlanoContasVersaoInfo>(PlanoContasVersaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            foreach (var item in retorno)
            {
                if (item.IdEmpresa == ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value)
                    item.EditarRegistro = true;

                if (item.IdEmpresa == null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa == 1)
                    item.EditarRegistro = true;
            }

            return base.CreateListResult(vm, retorno, "Exportação:Versão do Plano de Contas", true);
        }
    }
}
 