﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaSegmento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEmpresa { get;set; }

        public int? IdAtividadeEconomicaSegmento { get; set; }

        public string Descricao { get; set; }

        public string IdAtividadeEconomicaSegmentoIn { get; set; }

        public string IdAtividadeEconomicaSegmentoNotIn { get; set; }

        public bool? Ativo { get; set; }

        public bool? RegistroProprio { get; set; }
    }
}
