﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomica
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdAtividadeEconomica { get; set; }
        public int? IdEmpresa { get; set; }

        public string Descricao { get; set; }
        public string IdAtividadeEconomicaIn { get; set; }
        public string IdAtividadeEconomicaNotIn { get; set; }
        public string IdAtividadeEconomicaSegmentoIn { get; set; }

        public bool? PossuiSubAtividades { get; set; }
        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}