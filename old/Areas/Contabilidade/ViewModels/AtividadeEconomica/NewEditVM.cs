﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomica
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Etapas = new List<AtividadeEconomicaAtividadeEconomicaEtapaVM>();
            ProdutosServicos = new List<ProdutoServicoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomicaSegmento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNaturezaOperacional { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public bool? PossuiSubAtividades { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAplicacoesTipo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<AtividadeEconomicaAtividadeEconomicaEtapaVM> Etapas { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProdutoServicoVM> ProdutosServicos { get; set; }

        public class ProdutoServicoVM
        {
            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomicaProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? Natureza { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoNatureza { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            bool _ObrigatorioProduto;
            [ViewModelToModelAttribute]
            public bool ObrigatorioProduto { 
            
                get
                {
                    if (Natureza.HasValue && Natureza.Value == 1)
                        return _ObrigatorioProduto = true;
                    else
                        return _ObrigatorioProduto = false;
                }

                set
                {
                    _ObrigatorioProduto = value;
                }
            }

            bool _ObrigatorioServico;
            [ViewModelToModelAttribute]
            public bool ObrigatorioServico
            {
                get
                {
                    if (Natureza.HasValue && Natureza.Value == 2)
                        return _ObrigatorioServico = true;
                    else
                        return _ObrigatorioServico = false;
                }

                set
                {
                    _ObrigatorioServico = value;
                }
            }

            [ViewModelToModelAttribute]
            public string IdProdutoServicoNotIn{ get; set; }

        }
        public class AtividadeEconomicaAtividadeEconomicaEtapaVM
        {
            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomicaAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomica { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

        }

    }
}