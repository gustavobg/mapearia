﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaEtapa
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdAtividadeEconomicaEtapa { get; set; }

        public string IdAtividadeEconomicaEtapaIn { get; set; }
        public string IdAtividadeEconomicaIn { get; set; }
        public string IdAtividadeEconomicaEtapaNotIn { get; set; }
        public string Descricao { get; set; }
        public string DescricaoAtividadeEconomicaEtapa { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}