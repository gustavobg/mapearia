﻿using HTM.MasterGestor.Model.Contabilidade;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaEtapa
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomicaEtapa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public TipoRestricaoServicoEnum? TipoRestricaoServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsServicos { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}