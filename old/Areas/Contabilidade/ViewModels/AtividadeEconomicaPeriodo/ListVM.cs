using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaPeriodo
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
		{
		}

        public int? IdAtividadeEconomicaPeriodo { get; set; }
        public int? IdAtividadeEconomicaPeriodoPai { get; set; }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public string IdAtividadeEconomicaPeriodoIn { get; set; }
        public string IdAtividadeEconomicaPeriodoNotIn { get; set; }

        public string IdAtividadeEconomicaPeriodoPaiIn { get; set; }
        public string IdAtividadeEconomicaIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool ComAtividadesEconomica { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}