using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaPeriodo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
		{
            DataInicio = DateTime.Now;
            //DataTermino = DateTime.Now;
		}

		[ViewModelToModelAttribute]
		public int? IdAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
		public int? IdAtividadeEconomicaPeriodoPai { get; set; }

        [ViewModelToModelAttribute]
		public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
		public DateTime? DataInicio { get; set; }

        [ViewModelToModelAttribute]
		public DateTime? DataTermino { get; set; }

        [ViewModelToModelAttribute]
		public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public bool? UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioExibicao { get; set; }
    }
}