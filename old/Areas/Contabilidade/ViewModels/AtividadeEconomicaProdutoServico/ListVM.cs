﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.AtividadeEconomicaProdutoServico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdAtividadeEconomica { get; set; }
        public int? IdProdutoServico { get; set; }
        public int? Natureza { get; set; } //Natureza [1=Produto, 2=Serviço, 3=Princípio Ativo]

        public string IdProdutoServicoIn { get; set; }
        public string DescricaoAtividadeEconomica { get; set; }
        public string DescricaoProdutoServico { get; set; }
        public string DescricaoProdutoServicoBusca { get; set; }

        public bool PermiteProducao { get; set; }
    }
}