﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CFOP
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFiscalCFOP { get; set; }
        public int? IdModulo { get; set; }

        public string Descricao { get; set; }

        public string IndiceEstadual { get; set; }
        public string IndiceInterestadual { get; set; }
        public string IndiceInternacional { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}