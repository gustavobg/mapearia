﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CFOP
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdFiscalCFOP { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }

        [ViewModelToModelAttribute]
        public string OperacaoEstadual { get; set; }

        [ViewModelToModelAttribute]
        public string OperacaoInterestadual { get; set; }

        [ViewModelToModelAttribute]
        public string OperacaoInternacional { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFiscalCFOPEstadual { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFiscalCFOPInterestadual { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFiscalCFOPInternacioanal { get; set; }

        //Variáveis para a Manipulação na View 
        [ViewModelToModelAttribute]
        public string IndiceEstadual { get; set; }

        [ViewModelToModelAttribute]
        public string IndiceInterestadual { get; set; }
        
        [ViewModelToModelAttribute]
        public string IndiceInternacional { get; set; }
    }
}