﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CentroCusto
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCentroCusto { get; set; }
        public int? IdPlanoContasVersao { get; set; }

        public string IdCentroCustoIn { get; set; }
        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string Codigo { get; set; }

        public bool? Ativo { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? ComRestricaoUsuario { get; set; }

    }
}