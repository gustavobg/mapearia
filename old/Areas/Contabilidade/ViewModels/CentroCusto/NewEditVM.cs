﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.CentroCusto
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCustoPai { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteUsoSemAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteUsoSemOrdemServico { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoasRestritas { get; set; }

        [ViewModelToModelAttribute]
        public string Mascara { get; set; }
    }
}