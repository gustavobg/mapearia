﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ClassificacaoFiscal
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFiscalClassificacao { get; set; }
        public int? Natureza { get; set; }

        public string Descricao { get; set; }
        public string IdFiscalClassificacaoIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}