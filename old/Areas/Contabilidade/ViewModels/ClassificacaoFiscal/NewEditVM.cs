﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ClassificacaoFiscal
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Natureza = 1;
            Impostos = new List<FiscalClassificacaoImpostoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFiscalClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public bool Imobilizado { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<FiscalClassificacaoImpostoVM> Impostos { get; set; }

        public class FiscalClassificacaoImpostoVM
        {
            [ViewModelToModelAttribute]
            public int? IdFiscalClassificacaoImposto { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFiscalClassificacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFiscalImposto { get; set; }

            [ViewModelToModelAttribute]
            public int? PessoaNatureza { get; set; }

            [ViewModelToModelAttribute]
            public bool? Recuperar { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PercentualRecuperacao { get; set; }

            [ViewModelToModelAttribute]
            public int? QuantidadeParcela { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PercentualReducaoBase { get; set; }

            [ViewModelToModelAttribute]
            public int? DestinoIncidencia { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string ImpostoDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string PessoaNaturezaDescricao { get; set; }
        }
    }
}