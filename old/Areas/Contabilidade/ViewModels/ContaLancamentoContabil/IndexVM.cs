﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ContaLancamentoContabil
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }

        public string Chave { get; set; }
        public string ParentId { get; set; }

        public int? IdTransacao { get; set; }
        public int? TransacaoAcaoTipo { get; set; }
        public int? DirecionadorTipo { get; set; }
        public int? IdClasseContabil { get; set; }
    }
}