﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ContaLancamentoContabil
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdContaLancamentoContabil { get; set; }

        public string Chave { get; set; }
        public string ParentId { get; set; }

    }
}