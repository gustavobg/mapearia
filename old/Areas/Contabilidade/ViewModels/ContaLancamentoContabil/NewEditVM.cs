﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ContaLancamentoContabil
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdContaLancamentoContabil { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoPrazoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasContaDebitoFiscal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasContaCreditoFiscal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasContaDebitoGerencial { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasContaCreditoGerencial { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDirecionadorLancamentoContabil { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public string ParentId { get; set; }

    }
}