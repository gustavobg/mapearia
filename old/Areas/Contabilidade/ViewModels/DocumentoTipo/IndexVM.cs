﻿using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.DocumentoTipo
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) 
        { 
        }
    }
}