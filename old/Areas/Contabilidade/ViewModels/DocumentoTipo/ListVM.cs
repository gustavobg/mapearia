﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.DocumentoTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdDocumentoTipo { get; set; }
        
        public string Descricao { get; set; }
        public string IdDocumentoTipoIn { get; set; }

        public bool? Ativo { get; set; }
    }
}