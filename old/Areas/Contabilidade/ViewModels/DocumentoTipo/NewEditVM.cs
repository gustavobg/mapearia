﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.DocumentoTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            
        }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFiscalImposto { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoVencimentoDiaNaoUtil { get; set; }

        [ViewModelToModelAttribute]
        public bool? ListaLivroFiscal { get; set; }

        [ViewModelToModelAttribute]
        public bool? ListaValorLivroFiscal { get; set; }

        [ViewModelToModelAttribute]
        public string ObservacaoLivroFiscal { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}