﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.NCM
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            UltimoNivel = false;
        }

        [ViewModelToModelAttribute]
        public int? IdFiscalNCM { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFiscalNCMPai { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualIncidencia { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string Mascara { get; set; }
    }
}