﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ObjetoTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdObjetoTipoContabil { get; set; }
        public int? Natureza { get; set; }

        public string Descricao { get; set; }
        public string Codigo { get; set; }

        public bool? Ativo { get; set; }

    }
}