﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ObjetoTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdObjetoTipoContabil { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}