﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.Operacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdOperacao { get; set; }
        public string IdOperacaoIn { get; set; }
        public string Descricao { get; set; }
        public string IdModuloIn { get; set; }
        public bool? Ativo { get; set; }
    }
}