﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ParametroLancamentoContabil
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }

        public int? IdTransacao { get; set; }
        public int? TransacaoAcaoTipo { get; set; }
        public int? DirecionadorTipo { get; set; }
        public int? IdClasseContabil { get; set; }

        //public string parentId { get; set; }
        public string Chave { get; set; }
        public string ChaveCompleta { get; set; }
        public string DescricaoExibica { get; set; }
    }
}