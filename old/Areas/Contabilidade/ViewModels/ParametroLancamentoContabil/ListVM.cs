﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ParametroLancamentoContabil
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdParametroLancamentoContabil { get; set; }
        public int? IdTransacao { get; set; }
        public int? TransacaoAcaoTipo { get; set; }
        public int? DirecionadorTipo { get; set; }

        public string IdTransacaoIn { get; set; }

        public bool? DirecionadorPersonalizado { get; set; }
    }
}