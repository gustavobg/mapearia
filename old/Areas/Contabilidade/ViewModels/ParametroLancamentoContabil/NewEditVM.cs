﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.ParametroLancamentoContabil
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            lstDirecionadorLancamentoContabil = new List<VMDirecionadorLancamentoContabil>();
        }

        [ViewModelToModelAttribute]
        public int? IdParametroLancamentoContabil { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdClasseContabil { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? TransacaoAcaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? DirecionadorTipo { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<VMDirecionadorLancamentoContabil> lstDirecionadorLancamentoContabil { get; set; }

        public class VMDirecionadorLancamentoContabil
        {
            [ViewModelToModelAttribute]
            public int? IdDirecionadorLancamentoContabil { get; set; }

            [ViewModelToModelAttribute]
            public int? IdParametroLancamentoContabil { get; set; }

            [ViewModelToModelAttribute]
            public int? TransacaoAcaoTipo { get; set; } //Apenas para filtro na grid de Direcionadores

            [ViewModelToModelAttribute]
            public string DescricaoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public bool PermiteDirecionador { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            #region IdsInternos
            [ViewModelToModelAttribute]
            public string IdsPessoaPerfil { get; set; }

            [ViewModelToModelAttribute]
            public string IdsFinanceiroEntidade { get; set; }

            [ViewModelToModelAttribute]
            public string IdsProdutoServicoGrupo { get; set; }

            [ViewModelToModelAttribute]
            public string IdsCategoriaServicoOrdem { get; set; }

            //[ViewModelToModelAttribute]               Ainda não foi implementado
            //public string IdsClassePatrimonial { get; set; }

            [ViewModelToModelAttribute]
            public string IdsDocumentoTipo { get; set; }

            [ViewModelToModelAttribute]
            public bool Demais { get; set; } // Propriedade APENAS para visualização
            #endregion
        }

        #region Descrições
        [ViewModelToModelAttribute]
        public bool? DirecionadorPersonalizado { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoTransacao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoTransacaoAcaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDirecionadorTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoClasseContabil { get; set; }

        #endregion

    }
}