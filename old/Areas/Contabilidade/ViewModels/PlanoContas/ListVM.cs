﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.PlanoContas
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdPlanoContas { get; set; }
        public int? IdPlanoContasPai { get; set; }
        public int? PlanoContasTipo { get;set;}
        public int? IdPlanoContasVersao { get; set; }

        public string IdPlanoContasIn { get; set; }
        public string Descricao { get; set; }
        public string Codigo { get; set;}
        public string Conta { get; set; }

        public bool? UltimoNivel { get; set; }
        public bool? Ativo { get; set; }
        public bool? CentroCustoObrigatorio { get; set; }

    }
}