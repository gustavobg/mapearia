﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Contabilidade.ViewModels.VersaoPlanoContas
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdPlanoContasVersao { get; set; }
        public int? PlanoContasTipo { get; set; }

        public string Descricao { get; set; }

        public bool? Ativo { get; set; }
        public bool? ComNCM { get; set; }

    }
}