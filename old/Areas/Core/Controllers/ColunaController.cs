﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Coluna;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using MasterGestor.Core.Bll;
using MasterGestor.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Core.Controllers
{
    public class ColunaController : ControllerExtended
    {
        public ColunaController()
        {
            IndexUrl = "Core/Coluna/Index";
        }

        private ColunaBll _BllColuna;
        private ColunaBll BllColuna
        {
            get
            {
                if (_BllColuna == null)
                    _BllColuna = new ColunaBll();
                return _BllColuna = new ColunaBll();
            }
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ColunaInfo();
            info.TABLE_NAME = vm.TABLE_NAME;
            info.COLUMN_NAME = vm.COLUMN_NAME;

            var retorno = new ListPaged<ColunaInfo>(BllColuna.ListarColunasPorTabela(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Coluna", true);
        }
    }
}
