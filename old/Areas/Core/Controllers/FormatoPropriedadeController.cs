﻿using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.FormatoPropriedade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Core.Controllers
{
    public class FormatoPropriedadeController : ControllerExtended
    {

        public FormatoPropriedadeController()
        {
            IndexUrl = "/Core/FormatoPropriedade/Index";
        }

        public ActionResult Index()
        {
            return View();
        }

        #region Unidade

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult Unidade_RetornaValorFormatadoPorCasasDecimais(ListVM vm)
        {
            return Json(new { Data = FormatoPropriedadeBll.Instance.Unidade_RetornaValorFormatadoPorCasasDecimais(vm.Valor.Value, vm.IdUnidade.Value, IdEmpresa) });
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult Unidade_RetornaValorFormatadoComSigla_ComCasasDecimais(ListVM vm)
        {
            return Json(new { Data = FormatoPropriedadeBll.Instance.Unidade_RetornaValorFormatadoComSigla_ComCasasDecimais(vm.Valor.Value, vm.IdUnidade.Value, IdEmpresa) });
        }
        #endregion

        #region Forma de Apresentação

        public ActionResult FormaApresentacao_RetornaValorComFormatadoComSigla(ListVM vm)
        {
            return Json(new
                {
                    Data = FormatoPropriedadeBll.Instance.FormaApresentacao_RetornaValorComFormatadoComSigla(vm.Valor.Value, vm.IdProdutoServicoFormaApresentacao.Value)
                });
        }


        public ActionResult FormaApresentacao_RetornaValorComFormatadoComSiglaAlternativo(ListVM vm)
        {
            return Json(new
            {
                Data = FormatoPropriedadeBll.Instance.FormaApresentacao_RetornaValorComFormatadoComSiglaAlternativo(
                    vm.Valor.Value,
                    vm.IdProdutoServicoFormaApresentacao.Value,
                    vm.CasasDecimais.HasValue ? vm.CasasDecimais.Value : 0)
            });
        }
        #endregion

        #region Financeiro Moeda

        #endregion

    }
}
