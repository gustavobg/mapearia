﻿using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Coluna;
using HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Restricao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using MasterGestor.Core.Bll;
using MasterGestor.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Core.Controllers
{
    public class RestricaoController : ControllerExtended
    {
        public RestricaoController()
        {
            IndexUrl = "Core/Restricao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListEmpresaUsuariaVM))]
        [CustomAuthorize]
        public ActionResult RestricaoPorEmpresaUsuaria(ListEmpresaUsuariaVM vm)
        {
            var response = RestricaoBll.Instance.ListaTodasRestricoesPorEmpresaUsuaria(vm.IdPessoa.Value, vm.TipoIn);

            return Json(new { Data = response });
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListEmpresaUsuariaVM))]
        [CustomAuthorize]
        public ActionResult RestricaoPorPessoa(ListEmpresaUsuariaVM vm)
        {
            var response = RestricaoBll.Instance.ListaTodasRestricoesPorPessoa(vm.IdPessoa.Value, vm.TipoIn);

            return Json(new { Data = response });
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListEmpresaUsuariaVM))]
        [CustomAuthorize]
        public ActionResult RestricaoPorUsuario(ListEmpresaUsuariaVM vm)
        {
            if ((vm.LocalizarUsuarioPelaPessoa.HasValue && vm.LocalizarUsuarioPelaPessoa.Value) && vm.IdPessoa.HasValue)
                vm.IdUsuario = UsuarioBll.Instance.ListarPorParametros(new UsuarioInfo { IdPessoa = vm.IdPessoa.Value }).FirstOrDefault().IdUsuario;

            var response = RestricaoBll.Instance.ListaTodasRestricoesPorUsuario(vm.IdUsuario.Value, vm.TipoIn);

            return Json(new { Data = response });
        }

    }
}
