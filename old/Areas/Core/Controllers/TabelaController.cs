﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Tabela;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using MasterGestor.Core.Bll;
using MasterGestor.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Core.Controllers
{
    public class TabelaController : ControllerExtended
    {
        public TabelaController()
        {
            IndexUrl = "Core/Tabela/Index";
        }

        private TabelaBll _BllTabela;
        private TabelaBll BllTabela
        {
            get
            {
                if (_BllTabela == null)
                    _BllTabela = new TabelaBll();
                return _BllTabela;
            }
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new TabelaInfo();

            info.TABLE_NAME = vm.TABLE_NAME;

            ListPaged<TabelaInfo> retorno = new ListPaged<TabelaInfo>(BllTabela.ListarTodasTabelas(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tabela", true);
        }

    }
}
