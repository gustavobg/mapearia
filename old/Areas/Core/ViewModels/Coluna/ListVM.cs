﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Coluna
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public string TABLE_NAME { get; set; }
        public string COLUMN_NAME { get; set; }
    }
}