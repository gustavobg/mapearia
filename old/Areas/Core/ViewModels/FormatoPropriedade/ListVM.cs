﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.FormatoPropriedade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public decimal? Valor { get; set; }

        public int? IdUnidade { get; set; }
        public int? IdProdutoServicoFormaApresentacao { get; set; }
        public int? CasasDecimais { get; set; }
    }
}