﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Restricao
{
    public class ListEmpresaUsuariaVM : ViewModelListRequestBase
    {
        public ListEmpresaUsuariaVM()
        {

        }

        /// <summary>
        /// Identificador da Empresa Usuária (Tabela PessoaEmprea, quando Matriz = 0)
        /// Identificador da Pessoa
        /// </summary>
        public int? IdPessoa { get; set; }

        /// <summary>
        /// Identificador do Usuário
        /// </summary>
        public int? IdUsuario { get; set; }

        /// <summary>
        /// Tipo utilizado para restringir algum entidade, caso contrário listará todas
        /// </summary>
        public string[] TipoIn { get; set; }

        public bool? LocalizarUsuarioPelaPessoa { get; set; }

    }
}