﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Core.ViewModels.Restricao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        /// <summary>
        /// [0=None, 1= Categoria de Serviço, 2=Centro de Custo, 3=Plano de Contas, 4=Financeiro, 5=Produto Serviço ]
        /// </summary>
        public int? Tipo { get; set; }

        /// <summary>
        /// [0=Usuário, 1=Pessoa, 3=Empresa Usuária]
        /// </summary>
        public int? Categoria { get; set; }

    }
}