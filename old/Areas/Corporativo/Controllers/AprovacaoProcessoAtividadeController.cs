﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoAtividade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class AprovacaoProcessoAtividadeController : ControllerExtended
    {
        public AprovacaoProcessoAtividadeController()
        {
            IndexUrl = "/Corporativo/AprovacaoProcessoAtividade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [CustomAuthorize, HttpGet]
        public ActionResult AprovacoesModal()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            return View(vm);
        }

        [CustomAuthorize, HttpGet]
        public ActionResult AprovacaoConfirmaModal()
        {            
            return View();
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit()
        {            
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AprovacaoProcessoAtividadeInfo info = new AprovacaoProcessoAtividadeInfo();
            info.IdEmpresa = IdEmpresa;

            ListPaged<AprovacaoProcessoAtividadeInfo> retorno = new ListPaged<AprovacaoProcessoAtividadeInfo>(AprovacaoProcessoAtividadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return CreateListResult(vm, retorno, "Exportação: Aprovação de Processo", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListSequenciaParaAprovacao(ListVM vm)
        {
            AprovacaoProcessoAtividadeSequenciaInfo info = new AprovacaoProcessoAtividadeSequenciaInfo();
            info.IdEmpresa = IdEmpresa;
            info.Situacao = vm.Situacao;
            info.IdSubProcessoIn = vm.IdSubProcessoIn;
            info.IdUsuario = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario;
            //info.IdGrupoResponsabilidade =  TODO: Informar Grupo de Responsabilidade

            ListPaged<AprovacaoProcessoAtividadeSequenciaInfo> retorno = new ListPaged<AprovacaoProcessoAtividadeSequenciaInfo>(AprovacaoProcessoAtividadeSequenciaBll.Instance.ListarSequenciaParaAprovacaoPorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return CreateListResult(vm, retorno, "Exportação: Aprovação de Processo", true);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult AcaoAprovarReprovar(AtividadeSequenciaSituacaoVM vm)
        {
            AprovacaoProcessoAtividadeSequenciaSituacaoInfo info = new AprovacaoProcessoAtividadeSequenciaSituacaoInfo();
            ViewModelToModelMapper.Map<AprovacaoProcessoAtividadeSequenciaSituacaoInfo>(vm, info);

            info.IdUsuario = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.DataHora = DateTime.Now;

            var response = AprovacaoProcessoAtividadeSequenciaSituacaoBll.Instance.Salvar(info);
            return Json(response);
        }

        [CustomAuthorize]
        public ActionResult ListAprovacoesPorAtividade(int IdAprovacaoProcessoAtividade)
        {
            var retorno = AprovacaoProcessoAtividadeSequenciaSituacaoBll.Instance.ListarAprovacoesPorIdAprovacaoProcessoAtividade(IdAprovacaoProcessoAtividade);

            return Json(retorno);
        }

    }
}
