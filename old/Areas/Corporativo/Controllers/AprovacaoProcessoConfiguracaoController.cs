﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoConfiguracao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class AprovacaoProcessoConfiguracaoController : ControllerExtended
    {
        public AprovacaoProcessoConfiguracaoController()
        {
            IndexUrl = "/Corporativo/AprovacaoProcessoConfiguracao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            AprovacaoProcessoConfiguracaoInfo info = new AprovacaoProcessoConfiguracaoInfo();
            ViewModelToModelMapper.Map<AprovacaoProcessoConfiguracaoInfo>(vm, info);

            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = AprovacaoProcessoConfiguracaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdAprovacaoProcessoConfiguracao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdAprovacaoProcessoConfiguracao, "AprovacaoProcessoConfiguracao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                AprovacaoProcessoConfiguracaoInfo info = new AprovacaoProcessoConfiguracaoInfo();
                info = AprovacaoProcessoConfiguracaoBll.Instance.ListarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdAprovacaoProcessoConfiguracao, "AprovacaoProcessoConfiguracao");
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            AprovacaoProcessoConfiguracaoInfo info = new AprovacaoProcessoConfiguracaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdSubProcesso = vm.IdSubProcesso;
            info.IdAprovacaoProcessoConfiguracao = vm.IdAprovacaoProcessoConfiguracao;

            ListPaged<AprovacaoProcessoConfiguracaoInfo> retorno = new ListPaged<AprovacaoProcessoConfiguracaoInfo>(AprovacaoProcessoConfiguracaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Configuração de Aprovação de Processo", true);
        }


    }
}
