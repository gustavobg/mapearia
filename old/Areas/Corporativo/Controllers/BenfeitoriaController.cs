﻿

using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Benfeitoria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class BenfeitoriaController : ControllerExtended
    {
        public BenfeitoriaController()
        {
            IndexUrl = "Corporativo/Benfeitoria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            BenfeitoriaInfo info = new BenfeitoriaInfo();
            ViewModelToModelMapper.Map<BenfeitoriaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = BenfeitoriaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdBenfeitoria.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdBenfeitoria, "Benfeitoria", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                BenfeitoriaInfo info = new BenfeitoriaInfo();
                info = BenfeitoriaBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdBenfeitoria, "Benfeitoria");
            }
            else
            {
                vm.Ativo = true;
            }

            //Busca parâmetro
            //var caracteristica = new CaracteristicaBLL().AdquirirCaracteristicaComValor(new DTO.Caracteristica.DTOAdquirirCaracteristica { Codigo = "Centro_Custo_Obrigatorio" }, IdEmpresa: IdEmpresa).FirstOrDefault();
            //if (caracteristica != null)
            //    if (!string.IsNullOrEmpty(caracteristica.CaracteristicaValor.Valor))
            //        vm.CentroCustoObrigatorio = Convert.ToBoolean(caracteristica.CaracteristicaValor.Valor);
            return Json(vm);
        }

        #region List

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            BenfeitoriaInfo info = new BenfeitoriaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdBenfeitoria = vm.IdBenfeitoria;
            info.IdBenfeitoriaTipo = vm.IdBenfeitoriaTipo;
            info.IdBenfeitoriaTipoIn = vm.IdBenfeitoriaTipoIn;
            info.Descricao = vm.Descricao;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.Ativo = vm.Ativo;

            #region Parâmetros para o Modulo de Mapeamento

            if (vm.SemItensRelacionadosComMapeamento.HasValue && vm.SemItensRelacionadosComMapeamento.Value)
            {
                List<string> lst = new List<string>();
                var lstElementoBenfeitoria = MapeamentoElementoBll.Instance.ListarPorParametros(new Model.Mapeamento.MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 1 });
                if (!string.IsNullOrEmpty(info.IdBenfeitoriaNotIn))
                {

                    lst.AddRange(info.IdBenfeitoriaNotIn.Split(','));
                    if (lstElementoBenfeitoria.Count() > 0)
                    {
                        var ids = string.Join(",", lstElementoBenfeitoria.Select(p => p.IdReferencia).ToList());

                        lst.AddRange(ids.Split(','));
                    }
                    info.IdBenfeitoriaNotIn = string.Join(",", lst);
                }
                else
                {
                    info.IdBenfeitoriaNotIn = string.Join(",", lstElementoBenfeitoria.Select(p => p.IdReferencia));
                }

            }

            #endregion

            ListPaged<BenfeitoriaInfo> retorno = new ListPaged<BenfeitoriaInfo>(BenfeitoriaBll.Instance.ListarPorParametrosComDescricaoTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Benfeitoria", true);
        }

        #endregion

    }
}
