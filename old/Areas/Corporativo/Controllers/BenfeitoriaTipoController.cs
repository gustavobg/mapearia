﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.BenfeitoriaTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class BenfeitoriaTipoController : ControllerExtended
    {
        public BenfeitoriaTipoController()
        {
            IndexUrl = "/Corporativo/BenfeitoriaTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                BenfeitoriaTipoInfo info = new BenfeitoriaTipoInfo();
                info = BenfeitoriaTipoBll.Instance.ListarPorParametros(new BenfeitoriaTipoInfo { IdEmpresa = IdEmpresa, IdBenfeitoriaTipo = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdBenfeitoriaTipo, "BenfeitoriaTipo");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            BenfeitoriaTipoInfo info = new BenfeitoriaTipoInfo();
            ViewModelToModelMapper.Map<BenfeitoriaTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = BenfeitoriaTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdBenfeitoriaTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdBenfeitoriaTipo, "BenfeitoriaTipo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            BenfeitoriaTipoEmpresaInfo info = new BenfeitoriaTipoEmpresaInfo();
            BenfeitoriaTipoEmpresaInfo response = new BenfeitoriaTipoEmpresaInfo();

            if (id > 0)
            {
                info = BenfeitoriaTipoEmpresaBll.Instance.ListarPorParametros(new BenfeitoriaTipoEmpresaInfo { IdBenfeitoriaTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = BenfeitoriaTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdBenfeitoriaTipo, "BenfeitoriaTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            BenfeitoriaTipoEmpresaInfo info = new BenfeitoriaTipoEmpresaInfo();
            BenfeitoriaTipoEmpresaInfo response = new BenfeitoriaTipoEmpresaInfo();

            if (id > 0)
            {
                info = BenfeitoriaTipoEmpresaBll.Instance.ListarPorParametros(new BenfeitoriaTipoEmpresaInfo { IdBenfeitoriaTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = BenfeitoriaTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdBenfeitoriaTipo, "BenfeitoriaTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            BenfeitoriaTipoInfo info = new BenfeitoriaTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdBenfeitoriaTipo = vm.IdBenfeitoriaTipo;
            info.IdBenfeitoriaTipoIn = vm.IdBenfeitoriaTipoIn;
            info.IdBenfeitoriaTipoPai = vm.IdBenfeitoriaTipoPai;
            info.Descricao = vm.Descricao;
            info.UltimoNivel = vm.UltimoNivel;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.IdBenfeitoriaTipoNotIn = vm.IdBenefeitoriaTipoNotIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<BenfeitoriaTipoInfo> retorno = new ListPaged<BenfeitoriaTipoInfo>(BenfeitoriaTipoBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Benfeitoria", true);
        }
    }
}
