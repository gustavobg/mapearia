﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaFechamentoProcesso;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaFechamentoProcessoController : ControllerExtended

    {
        public CategoriaFechamentoProcessoController()
        {
            IndexUrl = "/Corporativo/CategoriaFechamentoProcesso/Index";
        }

        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CategoriaFechamentoProcessoInfo info = new CategoriaFechamentoProcessoInfo();
            info.IdCategoriaFechamentoProcesso = vm.IdCategoriaFechamentoProcesso;
            info.Descricao = vm.Descricao;

            ListPaged<CategoriaFechamentoProcessoInfo> retorno = new ListPaged<CategoriaFechamentoProcessoInfo>(CategoriaFechamentoProcessoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Categoria Fechamento de Processo", true);
        }

    }
}
