﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoController : ControllerExtended
    {
        public CategoriaServicoController()
        {
            IndexUrl = "Corporativo/CategoriaServico/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CategoriaServicoInfo info = new CategoriaServicoInfo();
            ViewModelToModelMapper.Map<CategoriaServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões

            //Tipo de Local de Produção
            if (!string.IsNullOrEmpty(vm.IdsProducaoLocalTipo))
            {
                info.TiposLocalProducao = new List<CategoriaServicoProducaoLocalTipoInfo>();

                CommaSeparatedToList(vm.IdsProducaoLocalTipo).ForEach(idProducaoLocalTipo =>
                {
                    CategoriaServicoProducaoLocalTipoInfo tipoLocalProducao = new CategoriaServicoProducaoLocalTipoInfo();
                    tipoLocalProducao.IdProducaoLocalTipo = idProducaoLocalTipo;

                    info.TiposLocalProducao.Add(tipoLocalProducao);
                });
            }

            //Unidade de Produção Tipo
            if (!string.IsNullOrEmpty(vm.IdsProducaoUnidadeTipo))
            {
                info.TiposUnidadesProducao = new List<CategoriaServicoProducaoUnidadeTipoInfo>();

                CommaSeparatedToList(vm.IdsProducaoUnidadeTipo).ForEach(idProducaoUnidadeTipo =>
                {
                    CategoriaServicoProducaoUnidadeTipoInfo producaoUnidadeTipo = new CategoriaServicoProducaoUnidadeTipoInfo();
                    producaoUnidadeTipo.IdProducaoUnidadeTipo = idProducaoUnidadeTipo;

                    info.TiposUnidadesProducao.Add(producaoUnidadeTipo);
                });
            }

            //Equipamentos
            if (!string.IsNullOrEmpty(vm.IdsAutomotivoTipoEquipamento))
            {
                info.TiposEquipamento = new List<CategoriaServicoAutomotivoTipoEquipamentoInfo>();
                CommaSeparatedToList(vm.IdsAutomotivoTipoEquipamento).ForEach(idAutomotivoTipoEquipamento =>
                {
                    CategoriaServicoAutomotivoTipoEquipamentoInfo item = new CategoriaServicoAutomotivoTipoEquipamentoInfo();
                    item.IdAutomotivoTipoEquipamento = idAutomotivoTipoEquipamento;

                    info.TiposEquipamento.Add(item);
                });

            }

            //Parte Equipamento
            if (!string.IsNullOrEmpty(vm.IdsAutomotivoParteEquipamento))
            {
                info.PartesEquipamento = new List<CategoriaServicoAutomotivoParteEquipamentoInfo>();
                CommaSeparatedToList(vm.IdsAutomotivoParteEquipamento).ForEach(idAutomotivoParteEquipamento =>
                {
                    CategoriaServicoAutomotivoParteEquipamentoInfo parte = new CategoriaServicoAutomotivoParteEquipamentoInfo();
                    parte.IdAutomotivoParteEquipamento = idAutomotivoParteEquipamento;

                    info.PartesEquipamento.Add(parte);
                });

            }

            //Tipo de Criação
            if (!string.IsNullOrEmpty(vm.IdsCriacaoTipo))
            {
                info.TiposCriacao = new List<CategoriaServicoCriacaoTipoInfo>();
                CommaSeparatedToList(vm.IdsCriacaoTipo).ForEach(idcriacaotipo =>
                {
                    CategoriaServicoCriacaoTipoInfo criacao = new CategoriaServicoCriacaoTipoInfo();
                    criacao.IdCriacaoTipo = idcriacaotipo;

                    info.TiposCriacao.Add(criacao);
                });

            }

            //Criação Lote
            if (!string.IsNullOrEmpty(vm.IdsCriacaoCategoria))
            {
                info.CategoriasCriacao = new List<CategoriaServicoCriacaoCategoriaInfo>();
                CommaSeparatedToList(vm.IdsCriacaoCategoria).ForEach(idCriacaoCategoria =>
                {
                    CategoriaServicoCriacaoCategoriaInfo item = new CategoriaServicoCriacaoCategoriaInfo();
                    item.IdCriacaoCategoria = idCriacaoCategoria;

                    info.CategoriasCriacao.Add(item);
                });
            }

            //Tipo de Benfeitoria
            if (!string.IsNullOrEmpty(vm.IdsBenfeitoriaTipo))
            {
                info.TiposBenfeitoria = new List<CategoriaServicoBenfeitoriaTipoInfo>();
                CommaSeparatedToList(vm.IdsBenfeitoriaTipo).ForEach(idBenfeitoriaTipo =>
                {
                    CategoriaServicoBenfeitoriaTipoInfo item = new CategoriaServicoBenfeitoriaTipoInfo();
                    item.IdBenfeitoriaTipo = idBenfeitoriaTipo;

                    info.TiposBenfeitoria.Add(item);
                });
            }

            //Etapa 
            if (!string.IsNullOrEmpty(vm.IdsEtapas))
            {
                info.Etapas = new List<CategoriaServicoAtividadeEconomicaEtapaInfo>();

                CommaSeparatedToList(vm.IdsEtapas).ForEach(idatividadeeconomicaetapa =>
                {
                    CategoriaServicoAtividadeEconomicaEtapaInfo etapa = new CategoriaServicoAtividadeEconomicaEtapaInfo();
                    etapa.IdAtividadeEconomicaEtapa = idatividadeeconomicaetapa;

                    info.Etapas.Add(etapa);
                });
            }

            //ProdutoServico (Servico [Natureza == 2])]
            //if (!string.IsNullOrEmpty(vm.IdsProdutoServico))
            //{
            //    info.Servicos = new List<CategoriaServicoProdutoServicoInfo>();

            //    CommaSeparatedToList(vm.IdsProdutoServico).ForEach(idservico =>
            //    {
            //        CategoriaServicoProdutoServicoInfo servico = new CategoriaServicoProdutoServicoInfo();
            //        servico.IdProdutoServico = idservico;

            //        info.Servicos.Add(servico);
            //    });
            //}

            //ProdutoServicoGrupo
            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.GruposProdutos = new List<CategoriaServicoProdutoServicoGrupoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(idprodutoservicogrupo =>
                {
                    CategoriaServicoProdutoServicoGrupoInfo grupo = new CategoriaServicoProdutoServicoGrupoInfo();
                    grupo.IdProdutoServicoGrupo = idprodutoservicogrupo;

                    info.GruposProdutos.Add(grupo);
                });
            }

            //Empresa usuária restrita
            if (!string.IsNullOrEmpty(vm.IdsEmpresaEmissora))
            {
                info.RestricaoEmpresaEmissora = new List<CategoriaServicoPessoaEmpresaEmissoraRestricaoInfo>();

                CommaSeparatedToList(vm.IdsEmpresaEmissora).ForEach(idpessoa =>
                {
                    CategoriaServicoPessoaEmpresaEmissoraRestricaoInfo pessoaEmpresa = new CategoriaServicoPessoaEmpresaEmissoraRestricaoInfo();
                    pessoaEmpresa.IdPessoa = idpessoa;

                    info.RestricaoEmpresaEmissora.Add(pessoaEmpresa);
                });
            }

            //Usuario Restrito
            if (!string.IsNullOrEmpty(vm.IdsUsuarioRestritos))
            {
                info.UsuariosRestritos = new List<CategoriaServicoUsuarioRestricaoInfo>();

                CommaSeparatedToList(vm.IdsUsuarioRestritos).ForEach(idusuario =>
                {
                    CategoriaServicoUsuarioRestricaoInfo usuario = new CategoriaServicoUsuarioRestricaoInfo();
                    usuario.IdUsuario = idusuario;

                    info.UsuariosRestritos.Add(usuario);
                });
            }

            //Atividades Econômicas
            if (!string.IsNullOrEmpty(vm.IdsAtividadeEconomica))
            {
                info.AtividadesEconomicas = new List<CategoriaServicoAtividadeEconomicaInfo>();
                CommaSeparatedToList(vm.IdsAtividadeEconomica).ForEach(idAtividadeEconomica =>
                {
                    CategoriaServicoAtividadeEconomicaInfo item = new CategoriaServicoAtividadeEconomicaInfo();
                    item.IdAtividadeEconomica = idAtividadeEconomica;

                    info.AtividadesEconomicas.Add(item);
                });
            }

            #endregion

            var response = CategoriaServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServico, "CategoriaServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                CategoriaServicoInfo info = new CategoriaServicoInfo();
                info = CategoriaServicoBll.Instance.ListarPorIdCompleto(new CategoriaServicoInfo { IdCategoriaServico = id.Value });

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServico, "CategoriaServico");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CategoriaServicoInfo info = new CategoriaServicoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdCategoriaServico = vm.IdCategoriaServico;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.TipoAplicacaoTipo = vm.TipoAplicacaoTipo;
            info.CodigoDescricao = vm.CodigoDescricao;
            info.IdUsuarioNotIn = vm.IdUsuarioNotIn;
            info.IdEmpresaUsuariaNotIn = vm.IdEmpresaUsuariaNotIn;

            ListPaged<CategoriaServicoInfo> retorno = new ListPaged<CategoriaServicoInfo>(CategoriaServicoBll.Instance.ListarPorParametrosComDescricao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Categoria Serviço", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListComItensRestritos(ListVM vm)
        {
            CategoriaServicoInfo info = new CategoriaServicoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdCategoriaServico = vm.IdCategoriaServico;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.TipoAplicacaoTipo = vm.TipoAplicacaoTipo;
            info.CodigoDescricao = vm.CodigoDescricao;
            info.IdUsuarioNotIn = vm.IdUsuarioNotIn;
            info.IdEmpresaUsuariaNotIn = vm.IdEmpresaUsuariaNotIn;

            ListPaged<CategoriaServicoInfo> retorno = new ListPaged<CategoriaServicoInfo>(CategoriaServicoBll.Instance.ListarPorParametrosComItensRestritos(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Categoria Serviço", true);
        }

        [HttpGet]
        [CustomAuthorize]
        public JsonResult ListarPorId(int id)
        {
            CategoriaServicoInfo info = new CategoriaServicoInfo();
            ListVM vm = new ListVM();
            info = CategoriaServicoBll.Instance.ListarPorIdCompleto(new CategoriaServicoInfo { IdCategoriaServico = id });

            return Json(new { Data = info }, JsonRequestBehavior.AllowGet);
            
        }

        #region Ordem

        #endregion

        #region Produto

        #endregion

        #region Matéria Prima



        #endregion

        #region Equipamento



        #endregion

        #region Mão de Obra



        #endregion

        #region Produção



        #endregion

        #region Plantio



        #endregion
    }
}
