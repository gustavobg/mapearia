﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoEquipamentoController : ControllerExtended
    {
        public CategoriaServicoEquipamentoController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoEquipamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Equipamento(CategoriaServicoEquipamentoNewEditVM vm)
        {
            CategoriaServicoEquipamentoInfo info = new CategoriaServicoEquipamentoInfo();
            ViewModelToModelMapper.Map<CategoriaServicoEquipamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            //Tipo Equipamento
            if (!string.IsNullOrEmpty(vm.IdsAutomotivoTipoEquipamento))
            {
                info.TiposEquipamentos = new List<CategoriaServicoEquipamentoTipoEquipamentoInfo>();

                CommaSeparatedToList(vm.IdsAutomotivoTipoEquipamento).ForEach(idAplicacaoTipo =>
                {
                    CategoriaServicoEquipamentoTipoEquipamentoInfo tipoEquipamento = new CategoriaServicoEquipamentoTipoEquipamentoInfo();
                    tipoEquipamento.IdAutomotivoTipoEquipamento = idAplicacaoTipo;

                    info.TiposEquipamentos.Add(tipoEquipamento);
                });
            }

            //Equipamento
            if (!string.IsNullOrEmpty(vm.IdsAutomotivoEquipamento))
            {
                info.Equipamentos = new List<CategoriaServicoEquipamentoAutomotivoEquipamentoInfo>();

                CommaSeparatedToList(vm.IdsAutomotivoEquipamento).ForEach(idAutomotivoEquipamento =>
                {
                    CategoriaServicoEquipamentoAutomotivoEquipamentoInfo equipamento = new CategoriaServicoEquipamentoAutomotivoEquipamentoInfo();
                    equipamento.IdAutomotivoEquipamento = idAutomotivoEquipamento;

                    info.Equipamentos.Add(equipamento);
                });
            }

            var response = CategoriaServicoEquipamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoEquipamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoEquipamento, "CategoriaServicoMateriaEquipamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Equipamento(int? id)
        {
            CategoriaServicoEquipamentoNewEditVM vm = new CategoriaServicoEquipamentoNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult EquipamentoJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoEquipamentoNewEditVM vm = new CategoriaServicoEquipamentoNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoEquipamentoInfo info = new CategoriaServicoEquipamentoInfo();

                info = CategoriaServicoEquipamentoBll.Instance.ListarPorIdCompleto(new CategoriaServicoEquipamentoInfo { IdCategoriaServico = id.Value });
                if (info != null && info.IdCategoriaServicoEquipamento.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoEquipamentoNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoEquipamento, "CategoriaServicoEquipamento");
                }
                else
                {
                    info = new CategoriaServicoEquipamentoInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoEquipamentoNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

    }
}
