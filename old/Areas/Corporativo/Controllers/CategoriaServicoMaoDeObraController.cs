﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoMaoDeObraController : ControllerExtended
    {
        public CategoriaServicoMaoDeObraController()
        {
            IndexUrl = "Corporativo/CategoriaServicoMaoDeObra/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult MaoDeObra(CategoriaServicoMaoDeObraNewEditVM vm)
        {
            CategoriaServicoMaoDeObraInfo info = new CategoriaServicoMaoDeObraInfo();
            ViewModelToModelMapper.Map<CategoriaServicoMaoDeObraInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = CategoriaServicoMaoDeObraBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoMaoDeObra.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoMaoDeObra, "CategoriaServicoMaoDeObra", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult MaoDeObra(int? id)
        {
            CategoriaServicoMaoDeObraNewEditVM vm = new CategoriaServicoMaoDeObraNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult MaoDeObraJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoMaoDeObraNewEditVM vm = new CategoriaServicoMaoDeObraNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoMaoDeObraInfo info = new CategoriaServicoMaoDeObraInfo();

                info = CategoriaServicoMaoDeObraBll.Instance.ListarPorParametros(new CategoriaServicoMaoDeObraInfo { IdCategoriaServico = id.Value }).FirstOrDefault();
                if (info != null && info.IdCategoriaServicoMaoDeObra.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoMaoDeObraNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoMaoDeObra, "CategoriaServicoMaoDeObra");
                }
                else
                {
                    info = new CategoriaServicoMaoDeObraInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoMaoDeObraNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

    }
}
