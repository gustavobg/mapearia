﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoMateriaPrimaController : ControllerExtended
    {
        public CategoriaServicoMateriaPrimaController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoMateriaPrima/Index";
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult MateriaPrima(CategoriaServicoMateriaPrimaNewEditVM vm)
        {
            CategoriaServicoMateriaPrimaInfo info = new CategoriaServicoMateriaPrimaInfo();
            ViewModelToModelMapper.Map<CategoriaServicoMateriaPrimaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            //ProdutoServicoGrupo
            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.lstProdutoServicoGrupo = new List<CategoriaServicoMateriaPrimaProdutoServicoGrupoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(idprodutoservicogrupo =>
                {
                    CategoriaServicoMateriaPrimaProdutoServicoGrupoInfo item = new CategoriaServicoMateriaPrimaProdutoServicoGrupoInfo();
                    item.IdProdutoServicoGrupo = idprodutoservicogrupo;

                    info.lstProdutoServicoGrupo.Add(item);
                });
            }

            var response = CategoriaServicoMateriaPrimaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoMateriaPrima.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoMateriaPrima, "CategoriaServicoMateriaPrima", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult MateriaPrima(int? id)
        {
            CategoriaServicoMateriaPrimaNewEditVM vm = new CategoriaServicoMateriaPrimaNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult MateriaPrimaJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoMateriaPrimaNewEditVM vm = new CategoriaServicoMateriaPrimaNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoMateriaPrimaInfo info = new CategoriaServicoMateriaPrimaInfo();

                info = CategoriaServicoMateriaPrimaBll.Instance.ListarPorIdCategoriaServicoCompleto(id.Value);
                if (info != null && info.IdCategoriaServicoMateriaPrima.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoMateriaPrimaNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoMateriaPrima, "CategoriaServicoMateriaPrima");
                }
                else
                {
                    info = new CategoriaServicoMateriaPrimaInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoMateriaPrimaNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }
    }
}