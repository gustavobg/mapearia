﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoOrdemController : ControllerExtended
    {
        public CategoriaServicoOrdemController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoOrdem/Index";
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult CategoriaServicoOrdem(CategoriaServicoOrdemNewEditVM vm)
        {
            CategoriaServicoOrdemInfo info = new CategoriaServicoOrdemInfo();
            ViewModelToModelMapper.Map<CategoriaServicoOrdemInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = CategoriaServicoOrdemBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoOrdem.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoOrdem, "CategoriaServicoOrdem", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult CategoriaServicoOrdem(int? id)
        {
            CategoriaServicoOrdemNewEditVM vm = new CategoriaServicoOrdemNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult CategoriaServicoOrdemJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoOrdemNewEditVM vm = new CategoriaServicoOrdemNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoOrdemInfo info = new CategoriaServicoOrdemInfo();

                info = CategoriaServicoOrdemBll.Instance.ListarPorParametros(new CategoriaServicoOrdemInfo { IdCategoriaServico = id.Value }).FirstOrDefault();
                if (info != null && info.IdCategoriaServicoOrdem.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoOrdemNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoOrdem, "CategoriaServicoOrdem");
                }
                else
                {
                    info = new CategoriaServicoOrdemInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoOrdemNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(CategoriaServicoOrdemListVM))]
        [CustomAuthorize]
        public ActionResult List(CategoriaServicoOrdemListVM vm)
        {
            CategoriaServicoOrdemInfo info = new CategoriaServicoOrdemInfo();
            info.IdCategoriaServico = vm.IdCategoriaServico;
            info.IdCategoriaServicoOrdem = vm.IdCategoriaServicoOrdem;

            ListPaged<CategoriaServicoOrdemInfo> retorno = new ListPaged<CategoriaServicoOrdemInfo>(CategoriaServicoOrdemBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Categoria Servico - Configuração Ordem", true);
        }
    }
}
