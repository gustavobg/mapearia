﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoPlantioController : ControllerExtended
    {
        public CategoriaServicoPlantioController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoPlantio/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Plantio(CategoriaServicoPlantioNewEditVM vm)
        {
            CategoriaServicoPlantioInfo info = new CategoriaServicoPlantioInfo();
            ViewModelToModelMapper.Map<CategoriaServicoPlantioInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões
            //Cultura
            if (!string.IsNullOrEmpty(vm.IdsProducaoCultura))
            {
                info.Culturas = new List<CategoriaServicoPlantioProducaoCulturaInfo>();

                CommaSeparatedToList(vm.IdsProducaoCultura).ForEach(idproducaocultura =>
                {
                    CategoriaServicoPlantioProducaoCulturaInfo cultura = new CategoriaServicoPlantioProducaoCulturaInfo();
                    cultura.IdProducaoCultura = idproducaocultura;

                    info.Culturas.Add(cultura);
                });
            }
            #endregion

            var response = CategoriaServicoPlantioBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoPlantio.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoPlantio, "CategoriaServicoPlantio", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Plantio(int? id)
        {
            CategoriaServicoPlantioNewEditVM vm = new CategoriaServicoPlantioNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult PlantioJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoPlantioNewEditVM vm = new CategoriaServicoPlantioNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoPlantioInfo info = new CategoriaServicoPlantioInfo();

                info = CategoriaServicoPlantioBll.Instance.ListarPorIdCategoriaServico(id.Value);
                if (info != null && info.IdCategoriaServicoPlantio.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoPlantioNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoPlantio, "CategoriaServicoPlantio");
                }
                else
                {
                    info = new CategoriaServicoPlantioInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoPlantioNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

    }
}
