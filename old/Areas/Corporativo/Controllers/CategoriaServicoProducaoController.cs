﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoProducaoController : ControllerExtended
    {
        public CategoriaServicoProducaoController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoProducao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Producao(CategoriaServicoProducaoNewEditVM vm)
        {
            CategoriaServicoProducaoInfo info = new CategoriaServicoProducaoInfo();
            ViewModelToModelMapper.Map<CategoriaServicoProducaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões
            //Produto Servico (Produto) [Natureza = 1]
            if (!string.IsNullOrEmpty(vm.IdsProdutoServico))
            {
                info.Produtos = new List<CategoriaServicoProducaoProdutoServicoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServico).ForEach(idProdutoServico =>
                {
                    CategoriaServicoProducaoProdutoServicoInfo produto = new CategoriaServicoProducaoProdutoServicoInfo();
                    produto.IdProdutoServico = idProdutoServico;

                    info.Produtos.Add(produto);
                });
            }
            #endregion

            var response = CategoriaServicoProducaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoProducao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoProducao, "CategoriaServicoProducao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Producao(int? id)
        {
            CategoriaServicoProducaoNewEditVM vm = new CategoriaServicoProducaoNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ProducaoJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoProducaoNewEditVM vm = new CategoriaServicoProducaoNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoProducaoInfo info = new CategoriaServicoProducaoInfo();

                info = CategoriaServicoProducaoBll.Instance.ListarPorIdCompleto(new CategoriaServicoProducaoInfo { IdCategoriaServico = id.Value });
                if (info != null && info.IdCategoriaServicoProducao.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoProducaoNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoProducao, "CategoriaServicoProducao");
                }
                else
                {
                    info = new CategoriaServicoProducaoInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoProducaoNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

    }
}
