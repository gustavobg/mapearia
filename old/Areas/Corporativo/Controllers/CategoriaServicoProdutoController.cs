﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class CategoriaServicoProdutoController : ControllerExtended
    {
        public CategoriaServicoProdutoController()
        {
            IndexUrl = "/Corporativo/CategoriaServicoProduto/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Produto(CategoriaServicoProdutoNewEditVM vm)
        {
            CategoriaServicoProdutoInfo info = new CategoriaServicoProdutoInfo();
            ViewModelToModelMapper.Map<CategoriaServicoProdutoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.lstProdutoServicoGrupo = new List<CategoriaServicoProdutoProdutoServicoGrupoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(idProdutoServicoGrupo =>
                {
                    CategoriaServicoProdutoProdutoServicoGrupoInfo produtoServicoGrupo = new CategoriaServicoProdutoProdutoServicoGrupoInfo();
                    produtoServicoGrupo.IdProdutoServicoGrupo = idProdutoServicoGrupo;

                    info.lstProdutoServicoGrupo.Add(produtoServicoGrupo);
                });
            }
            #endregion

            var response = CategoriaServicoProdutoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCategoriaServicoProduto.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdCategoriaServicoProduto, "CategoriaServicoProduto", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Produto(int? id)
        {
            CategoriaServicoProdutoNewEditVM vm = new CategoriaServicoProdutoNewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ProdutoJson(int? id) // Identificador CategoriaServico [IdCategoriaServico]
        {
            CategoriaServicoProdutoNewEditVM vm = new CategoriaServicoProdutoNewEditVM();
            if (id.HasValue && id > 0)
            {
                //Retornar Cabecalho Categoria de Serviços
                var categoriaServicoInfo = CategoriaServicoBll.Instance.ListarCategoriaServicoExibicao(id.Value);
                CategoriaServicoProdutoInfo info = new CategoriaServicoProdutoInfo();

                info = CategoriaServicoProdutoBll.Instance.ListarPorIdCategoriaServicoCompleto(id.Value);
                if (info != null && info.IdCategoriaServicoProduto.HasValue)
                {
                    ViewModelToModelMapper.MapBack<CategoriaServicoProdutoNewEditVM>(vm, info);
                  SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCategoriaServicoProduto, "CategoriaServicoProduto");
                }
                else
                {
                    info = new CategoriaServicoProdutoInfo();
                    ViewModelToModelMapper.MapBack<CategoriaServicoProdutoNewEditVM>(vm, info);
                }
                vm.Codigo = categoriaServicoInfo.Codigo;
                vm.Descricao = categoriaServicoInfo.Descricao;
                vm.TipoAplicacaoTipo = categoriaServicoInfo.TipoAplicacaoTipo;
            }
            return Json(vm);

        }

    }
}