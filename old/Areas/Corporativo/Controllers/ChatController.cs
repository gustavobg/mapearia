using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Chat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using HTM.MasterGestor.Library.DataAccess;
using System.Data;
using System.Text;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class ChatController : ControllerExtended
    {
        //private ChatBLL bll = new ChatBLL();

        //[CustomAuthorize]
        //public JsonResult SendMessage(int idPessoa, string message)
        //{
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(new MessageResult() { Sucesso = false });
        //    }

        //    DTOSalvarChat dto = new DTOSalvarChat();
        //    dto.Entidade.DataEnvio = DateTime.Now;
        //    dto.Entidade.IdPessoaDe = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;
        //    dto.Entidade.IdPessoaPara = idPessoa;
        //    dto.Entidade.Mensagem = message;

        //    ChatBLL bll = new ChatBLL();
        //    int id = bll.SalvarChat(dto).IdChat;

        //    return GetMessage(id);
        //}

        //[CustomAuthorize]
        //public JsonResult OnlineUsers()
        //{
        //    OnlineUsersResult result = new OnlineUsersResult();

        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(result);
        //    }

        //    DTOAdquirirPessoa dto = new DTOAdquirirPessoa();
        //    dto.IdEmpresa = IdEmpresa;
        //    dto.Ativo = true;
        //    dto.PossuiUsuario = true;
        //    dto.Page.OrderBy = "NomeRazao";

        //    List<Pessoa> pessoas = new PessoaBLL().AdquirirPessoa(dto);

        //    pessoas.ForEach(pessoa =>
        //    {
        //        if (pessoa.IdPessoa == ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa)
        //            return;

        //        OnlineUsersResult.OnlineUser user = new OnlineUsersResult.OnlineUser();
        //        user.Nome = pessoa.Nome;
        //        user.Online = true;
        //        user.Online = pessoa.Usuario.DataUltimaInteracao.HasValue && 
        //                        pessoa.Usuario.DataUltimaInteracao > DateTime.Now.AddSeconds(-80);
        //        user.Id = pessoa.IdPessoa.Value;

        //        user.Imagem = "/content/images/user-generic.png";

        //        result.Users.Add(user);
        //    });

        //    result.Sucesso = true;
        //    return Json(result);
        //}

        //[CustomAuthorize]
        //public JsonResult TotalNewMessages()
        //{
        //    TotalNewMessagesResult result = new TotalNewMessagesResult();
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //        return Json(result);

        //    result.Sucesso = true;

        //    StringBuilder sql = new StringBuilder();
        //    sql.AppendLine("SELECT * FROM (");
        //    sql.AppendLine("SELECT ROW_NUMBER() OVER(PARTITION BY IdPessoaDe ORDER BY DataEnvio ASC) AS Row, DataEnvio, IdPessoaDe, Mensagem ");
        //    sql.AppendLine("FROM CHAT WHERE IdPessoaPara = @IdPessoaPara");
        //    sql.AppendLine("AND IdPessoaDe IN(");
        //    sql.AppendLine("SELECT	IdPessoaDe ");
        //    sql.AppendLine("FROM	Chat ");
        //    sql.AppendLine("WHERE	IdPessoaPara = @IdPessoaPara AND DataLeitura IS NULL");
        //    sql.AppendLine("GROUP BY IdPessoaDe");
        //    sql.AppendLine(") AND  DataLeitura IS NULL) T WHERE row = 1");

        //    List<SqlParameter> param = new List<SqlParameter>();
        //    param.Add(new SqlParameter("@IdPessoaPara", ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa));

        //    result.Info = DataMapper.Map<TotalNewMessagesResult.MessageInfo>(Executer.FillData<DataSet>(sql.ToString(), param).Tables[0]);
        //    result.Info.ForEach(x =>
        //    {
        //        int len = x.Mensagem.Length;
        //        if (len > 50)
        //            len = 50;

        //        x.Mensagem = HttpUtility.HtmlEncode(x.Mensagem.Substring(0, len));
        //    });

        //    return Json(result);
        //}

        //[CustomAuthorize]
        //public JsonResult GetAllMessagesFrom(int idPessoa)
        //{
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(new MessageResult() { Sucesso = false });
        //    }
            
        //    ChatBLL bll = new ChatBLL();

        //    // adquire menor data nao lida
        //    DTOAdquirirChat dto = new DTOAdquirirChat();
        //    dto.NaoLidas = true;
        //    dto.IdPessoaDe = idPessoa;
        //    dto.IdPessoaPara = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa;
        //    dto.Page.OrderBy = "DataEnvio ASC";
        //    dto.Page.PageSize = 1;
        //    dto.Page.CurrentPage = 1;
        //    Chat menorMsg = bll.AdquirirChat(dto).FirstOrDefault();

        //    DateTime data = DateTime.Today;
        //    if (menorMsg != null)
        //        data = new DateTime(menorMsg.DataEnvio.Year, menorMsg.DataEnvio.Month, menorMsg.DataEnvio.Day);

        //    // adquire mensagens
        //    dto = new DTOAdquirirChat();
        //    dto.IdPessoaDeOrIdPessoaPara = new DTOAdquirirChat.DeOrPara();
        //    dto.IdPessoaDeOrIdPessoaPara.IdPessoaDe = idPessoa;
        //    dto.IdPessoaDeOrIdPessoaPara.IdPessoaPara = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;
        //    dto.DataEnvioMaiorIgual = data;
        //    List<Chat> chat = bll.AdquirirChat(dto);

        //    return Json(MontarMessageResult(chat));
        //}

        //[CustomAuthorize]
        //public JsonResult GetMessagesFrom(int idPessoa)
        //{
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(new MessageResult() { Sucesso = false });
        //    }

        //    DTOAdquirirChat dto = new DTOAdquirirChat();
        //    dto.NaoLidas = true;
        //    dto.IdPessoaDe = idPessoa;
        //    dto.IdPessoaPara = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa;

        //    return Json(MontarMessageResult(new ChatBLL().AdquirirChat(dto)));
        //}

        //[CustomAuthorize]
        //public JsonResult GetMessage(int id)
        //{
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(new MessageResult() { Sucesso = false });
        //    }

        //    DTOAdquirirChat dto = new DTOAdquirirChat();
        //    dto.IdChat = id;

        //    return Json(MontarMessageResult(new ChatBLL().AdquirirChat(dto)));
        //}

        //private MessageResult MontarMessageResult(List<Chat> chats)
        //{
        //    MessageResult result = new MessageResult();
        //    result.Sucesso = true;

        //    chats.OrderBy(x => x.DataEnvio).ToList().ForEach(x =>
        //    {
        //        result.Messages.Add(new MessageResult.Message()
        //        {
        //            Id = x.IdChat.Value,
        //            Mensagem = HttpUtility.HtmlEncode(x.Mensagem),
        //            DataEnvio = x.DataEnvio.ToString(x.DataEnvio.Date != DateTime.Today ? "dd/MM/yyyy HH:mm" : "HH:mm"),
        //            Recebida = x.IdPessoaPara == ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa,
        //            IdDe = x.IdPessoaDe,
        //            IdPara = x.IdPessoaPara.Value
        //        });
        //    });

        //    return result;
        //}

        //[CustomAuthorize]
        //public JsonResult ConfirmReading(List<int> ids)
        //{
        //    if (ControladorSessaoUsuario.SessaoCorrente == null)
        //    {
        //        return Json(false);
        //    }

        //    DTOChatMarcarLido dto = new DTOChatMarcarLido();
        //    dto.IdChatIN = ids;
        //    new ChatBLL().ChatMarcarLido(dto);
            
        //    return Json(true);
        //}

        //public class MessageResult
        //{
        //    public MessageResult()
        //    {
        //        Messages = new List<Message>();
        //    }

        //    public bool Sucesso { get; set; }

        //    public List<Message> Messages { get; set; }
        //    public class Message
        //    {
        //        public int Id { get; set; }
        //        public int IdDe { get; set; }
        //        public int IdPara { get; set; }
        //        public string Mensagem { get; set; }
        //        public string DataEnvio { get; set; }
        //        public bool Recebida { get; set; }
        //    }
        //}

        //public class OnlineUsersResult
        //{
        //    public OnlineUsersResult()
        //    {
        //        Users = new List<OnlineUser>();
        //    }

        //    public bool Sucesso { get; set; }

        //    public List<OnlineUser> Users { get; set; }

        //    public class OnlineUser
        //    {
        //        public int Id { get; set; }
        //        public string Nome { get; set; }
        //        public string Imagem { get; set; }
        //        public bool Online { get; set; }
        //    }
        //}

        //public class TotalNewMessagesResult
        //{
        //    public TotalNewMessagesResult()
        //    {
        //        Info = new List<MessageInfo>();
        //    }

        //    public bool Sucesso { get; set; }

        //    public List<MessageInfo> Info { get; set; }

        //    public class MessageInfo
        //    {
        //        [DataMapperColumn]
        //        public DateTime DataEnvio { get; set; }

        //        [DataMapperColumn]
        //        public int IdPessoaDe { get; set; }

        //        [DataMapperColumn]
        //        public string Mensagem { get; set; }

        //        public string TempoEnviado
        //        {
        //            get
        //            {
        //                long tempo = 0;
        //                string str = "";

        //                TimeSpan timespan = DateTime.Now.Subtract(DataEnvio);
        //                if (timespan.TotalMinutes < 60)
        //                {
        //                    tempo = (long)timespan.TotalMinutes;
        //                    if (tempo > 0)
        //                    {
        //                        str = string.Format("minuto{0}", tempo > 1 ? "s" : "");
        //                    }
        //                }
        //                else if (timespan.TotalHours < 24)
        //                {
        //                    tempo = (long)timespan.TotalHours;
        //                    str = string.Format("hora{0}", tempo > 1 ? "s" : "");
        //                }
        //                else
        //                {
        //                    tempo = (long)timespan.TotalDays;
        //                    str = string.Format("dia{0}", tempo > 1 ? "s" : "");
        //                }

        //                return tempo == 0 ? "Agora" : string.Format("{0:N0} {1} atrás", tempo, str);
        //            }
        //        }
        //    }
        //}
    }
}
