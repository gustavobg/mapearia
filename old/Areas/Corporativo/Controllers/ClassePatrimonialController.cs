﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ClassePatrimonial;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class ClassePatrimonialController : ControllerExtended
    {
        
        public ClassePatrimonialController()
        {
            IndexUrl = "/Corporativo/ClassePatrimonial/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ClassePatrimonialInfo info = new ClassePatrimonialInfo();
                info = ClassePatrimonialBll.Instance.ListarPorParametros(new ClassePatrimonialInfo { IdClassePatrimonial = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdClassePatrimonial, "ClassePatrimonial");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ClassePatrimonialInfo info = new ClassePatrimonialInfo();
            ViewModelToModelMapper.Map<ClassePatrimonialInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = ClassePatrimonialBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdClassePatrimonial.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdClassePatrimonial, "ClassePatrimonial", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ClassePatrimonialEmpresaInfo info = new ClassePatrimonialEmpresaInfo();
            ClassePatrimonialEmpresaInfo response = new ClassePatrimonialEmpresaInfo();

            if (id > 0)
            {
                info = ClassePatrimonialEmpresaBll.Instance.ListarPorParametros(new ClassePatrimonialEmpresaInfo { IdClassePatrimonial = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ClassePatrimonialEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdClassePatrimonial, "ClassePatrimonial", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ClassePatrimonialEmpresaInfo info = new ClassePatrimonialEmpresaInfo();
            ClassePatrimonialEmpresaInfo response = new ClassePatrimonialEmpresaInfo();

            if (id > 0)
            {
                info = ClassePatrimonialEmpresaBll.Instance.ListarPorParametros(new ClassePatrimonialEmpresaInfo { IdClassePatrimonial = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ClassePatrimonialEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdClassePatrimonial, "ClassePatrimonial", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ClassePatrimonialInfo info = new ClassePatrimonialInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdClassePatrimonial = vm.IdClassePatrimonial;
            info.IdClassePatrimonialIn = vm.IdClassePatrimonialIn;
            info.IdClassePatrimonialPai = vm.IdClassePatrimonialPai;
            info.Descricao = vm.Descricao;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.Codigo = vm.Codigo;
            info.UltimoNivel = vm.UltimoNivel;
            info.Ativo = vm.Ativo;
            info.IdClassePatrimonialNotIn = vm.IdClassePatrimonialNotIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ClassePatrimonialInfo> retorno = new ListPaged<ClassePatrimonialInfo>(ClassePatrimonialBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Classe Patrimônial", true);
        }

    }
}
