using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Empresa;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class EmpresaController : ControllerExtended
    {
        public EmpresaController()
        {
            IndexUrl = "/Corporativo/Empresa/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            EmpresaInfo info = new EmpresaInfo();

            if (id.HasValue && id > 0)
            {
                info = EmpresaBll.Instance.ListaEmpresaPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEmpresa, "Empresa");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EmpresaInfo info = new EmpresaInfo();
            ViewModelToModelMapper.Map<EmpresaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            // se for inclusão, gera toda estrutura de empresa: usuario, matriz/filial
            info.GerarEstruturaInicial = !vm.IdEmpresa.HasValue;

            // salvar
            if(!vm.IdEmpresa.HasValue)
            {

                var response = EmpresaBll.Instance.Salvar_ComEstruturaInicial(info);
                if (response.Response.Sucesso)
                {
                    SalvarHistoricoAcesso(IndexUrl, vm.IdEmpresa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEmpresa, "Empresa", response.Response.IdHistorico);
                }
                return Json(response);
            }
            else
            {
                var response = EmpresaBll.Instance.Salvar(info);
                if (response.Response.Sucesso)
                {
                    // salvar historico de acesso
                    SalvarHistoricoAcesso(IndexUrl, vm.IdEmpresa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEmpresa, "Empresa", response.Response.IdHistorico);
                }

                return Json(response);
            }
            
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EmpresaInfo info = new EmpresaInfo();
            info.Ativo = vm.Ativo;
            info.IdEmpresa = vm.IdEmpresa;
            info.Nome = vm.Nome;
            info.IdGrupoMenuIn = vm.IdGrupoMenuIn;
            info.IdEmpresaGrupoIn = vm.IdEmpresaGrupoIn;
            info.Ativo = vm.Ativo;
            info.EmpresaModelo = vm.EmpresaModelo;
            info.IdEmpresaNotIn = vm.IdEmpresaNotIn;

            ListPaged<EmpresaInfo> retorno = new ListPaged<EmpresaInfo>(EmpresaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Empresa", true);
        }

    }
}
