﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaRemetente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class EmpresaRemetenteController : ControllerExtended
    {
        //public EmpresaRemetenteController()
        //{
        //    IndexUrl = "/Corporativo/EmpresaRemetente/Index";
        //}

        //private EmpresaRemetenteBLL bll = new EmpresaRemetenteBLL();

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarEmpresaRemetente dto = new DTOSalvarEmpresaRemetente();
        //    ViewModelToModelMapper.Map<EmpresaRemetente>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;
        //    dto.Entidade.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarEmpresaRemetenteResponse response = bll.SalvarEmpresaRemetente(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdEmpresaRemetente.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEmpresaRemetente, "EmpresaRemente", response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        EmpresaRemetente entidade = bll.AdquirirEmpresaRemetente(new DTOAdquirirEmpresaRemetente() { IdEmpresaRemetente = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdEmpresaRemetente, "EmpresaRemente");
        //    }
        //    else
        //    {
        //        vm.Ativo = true;
        //    }

        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirEmpresaRemetente dto = new DTOAdquirirEmpresaRemetente();
        //    dto.Page = vm.Page;
        //    dto.IdEmpresaRemetente = vm.IdEmpresaRemetente;
        //    dto.IdEmpresa = vm.IdEmpresa;
        //    dto.IdEmpresaRemetenteIn = CommaSeparatedToList(vm.IdEmpresaRemetenteIn);
        //    dto.IdEmpresaIn = CommaSeparatedToList(vm.IdEmpresaIn);

        //    ListPaged<EmpresaRemetente> retorno = bll.AdquirirEmpresaRemetente(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: EmpresaRemetente");
        //}

    }
}
