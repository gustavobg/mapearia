﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaUsuaria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class EmpresaUsuariaController : ControllerExtended
    {
        public EmpresaUsuariaController()
        {
            IndexUrl = "/Corporativo/EmpresaUsuaria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return PartialView(vm);
        }

        #region NewEdit

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PessoaInfo info = new PessoaInfo();
            ViewModelToModelMapper.Map<PessoaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = PessoaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoa, "Pessoa", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            PessoaInfo info = new PessoaInfo();
            if (id.HasValue && id > 0)
            {
                info = PessoaBll.Instance.ListarCompletoPorId(new PessoaInfo { IdPessoa = id, IdEmpresa = IdEmpresa });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPessoa, "Pessoa");
            }
            else
            {
                vm.IdPessoaNatureza = 2;
                vm.Corporacao = true;
                vm.Ativo = true;
                vm.PessoaEmpresa.IdFinanceiroMoeda = EmpresaBll.Instance.ListarPorCodigo(IdEmpresa).IdFinanceiroMoeda.Value; //Retornar Moeda Padrão da Empresa.
            }

            return Json(vm);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PessoaInfo info = new PessoaInfo();
            info.IdPessoa = vm.IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.Nomes = vm.Nomes;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.Nome = vm.Nome;
            info.NomeSecundario = vm.NomeSecundario;
            info.Corporacao = true; // Trazer apenas Corporação
            info.Ativo = vm.Ativo;
            info.ComEmpresa = false;
            info.ComEmpresaUsuaria = true;
            info.ComPessoa = false;

            #region Restrição Empresa Usuária (Usuário Logado)

            if (vm.ComRestricaoUsuarioLogado.HasValue && vm.ComRestricaoUsuarioLogado.Value)
            {
                List<int> lstIdsNotIn = new List<int>();
                var restricoesUsuarioLogada = UsuarioBll.Instance.RetornaUsuarioComRestricoes(new UsuarioInfo { IdEmpresa = IdEmpresa, IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value });

                #region Busca de Restrições e Tratativas
                if (restricoesUsuarioLogada != null)
                {
                    //Restrições do Usuário Logado
                    if (!string.IsNullOrEmpty(restricoesUsuarioLogada.IdsPessoaEmpresa))
                    {
                        CommaSeparatedToList(restricoesUsuarioLogada.IdsPessoaEmpresa).ForEach(id =>
                        {
                            lstIdsNotIn.Add(id);
                        });
                    }

                    //Restrições já existentes
                    if (!string.IsNullOrEmpty(vm.IdPessoaNotIn))
                    {
                        if (!string.IsNullOrEmpty(vm.IdPessoaNotIn))
                        {
                            CommaSeparatedToList(vm.IdPessoaNotIn).ForEach(id =>
                            {
                                lstIdsNotIn.Add(id);
                            });
                        }
                    }

                    vm.IdPessoaNotIn = string.Join(",", lstIdsNotIn);
                }

                #endregion

            }

            #endregion

            ListPaged<PessoaInfo> retorno = new ListPaged<PessoaInfo>(PessoaBll.Instance.ListarEmpresaUsuaria(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Pessoa", true);
        }

    }
}
