﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.GrupoResponsabilidade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class GrupoResponsabilidadeController : ControllerExtended
    {
        public GrupoResponsabilidadeController()
        {
            IndexUrl = "Corporativo/GrupoResponsabilidade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                GrupoResponsabilidadeInfo info = new GrupoResponsabilidadeInfo();
                info = GrupoResponsabilidadeBll.Instance.ListarPorParametros(new GrupoResponsabilidadeInfo { IdGrupoResponsabilidade = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdGrupoResponsabilidade, "GrupoResponsabilidade");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }
        
        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            GrupoResponsabilidadeInfo info = new GrupoResponsabilidadeInfo();
            ViewModelToModelMapper.Map<GrupoResponsabilidadeInfo>(vm, info);

            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = GrupoResponsabilidadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdGrupoResponsabilidade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdGrupoResponsabilidade, "GrupoResponsabilidade", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            GrupoResponsabilidadeEmpresaInfo info = new GrupoResponsabilidadeEmpresaInfo();
            GrupoResponsabilidadeEmpresaInfo response = new GrupoResponsabilidadeEmpresaInfo();

            if (id > 0)
            {
                info = GrupoResponsabilidadeEmpresaBll.Instance.ListarPorParametros(new GrupoResponsabilidadeEmpresaInfo { IdGrupoResponsabilidade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = GrupoResponsabilidadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdGrupoResponsabilidade, "GrupoResponsabilidade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            GrupoResponsabilidadeEmpresaInfo info = new GrupoResponsabilidadeEmpresaInfo();
            GrupoResponsabilidadeEmpresaInfo response = new GrupoResponsabilidadeEmpresaInfo();

            if (id > 0)
            {
                info = GrupoResponsabilidadeEmpresaBll.Instance.ListarPorParametros(new GrupoResponsabilidadeEmpresaInfo { IdGrupoResponsabilidade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = GrupoResponsabilidadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdGrupoResponsabilidade, "GrupoResponsabilidade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            GrupoResponsabilidadeInfo info = new GrupoResponsabilidadeInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdGrupoResponsabilidade = vm.IdGrupoResponsabilidade;
            info.Descricao = vm.Descricao;
            info.IdGrupoResponsabilidadeTipo = vm.IdGrupoResponsabilidadeTipo;
            info.IdGrupoResponsabilidadeIn = vm.IdGrupoResponsabilidadeIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<GrupoResponsabilidadeInfo> retorno = new ListPaged<GrupoResponsabilidadeInfo>(GrupoResponsabilidadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Grupo de Responsabilidade", true);
        }

    }
}
