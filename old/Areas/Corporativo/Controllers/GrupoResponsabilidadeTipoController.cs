﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.GrupoResponsabilidadeTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class GrupoResponsabilidadeTipoController : ControllerExtended
    {
        
        public GrupoResponsabilidadeTipoController()
        {
            IndexUrl = "/Corporativo/GrupoResponsabilidadeTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            GrupoResponsabilidadeTipoInfo info = new GrupoResponsabilidadeTipoInfo();
            ViewModelToModelMapper.Map<GrupoResponsabilidadeTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = GrupoResponsabilidadeTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdGrupoResponsabilidadeTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdGrupoResponsabilidadeTipo, "GrupoResponsabilidadeTipo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                GrupoResponsabilidadeTipoInfo info = new GrupoResponsabilidadeTipoInfo();
                info = GrupoResponsabilidadeTipoBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdGrupoResponsabilidadeTipo, "GrupoResponsabilidadeTipo");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            GrupoResponsabilidadeTipoInfo info = new GrupoResponsabilidadeTipoInfo();
            info.IdGrupoResponsabilidadeTipo = vm.IdGrupoResponsabilidadeTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<GrupoResponsabilidadeTipoInfo> retorno = new ListPaged<GrupoResponsabilidadeTipoInfo>(GrupoResponsabilidadeTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Grupo de Responsabilidade", true);
        }

    }
}
