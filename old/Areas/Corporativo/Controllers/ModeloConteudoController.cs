﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ModeloConteudo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class ModeloConteudoController : ControllerExtended
    {
        public ModeloConteudoController()
        {
            IndexUrl = "/Corporativo/ModeloConteudo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            vm.DescricaoExibicao = EmpresaBll.Instance.ListarPorCodigo(int.Parse(parentId)).Nome;
            vm.ParentId = parentId;
            return View(vm);
        }

        #region NewEdit

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();
            EmpresaModeloConteudoInfo info = new EmpresaModeloConteudoInfo();

            if (id.HasValue && id > 0)
            {
                info = EmpresaModeloConteudoBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEmpresa, "EmpresaMolodeConteudo");
            }
            else
            {
                vm.PermiteCadastroIndependente = true;
            }
            return Json(vm);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();
            vm.DescricaoExibicao = EmpresaBll.Instance.ListarPorCodigo(int.Parse(parentId)).Nome;
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EmpresaModeloConteudoInfo info = new EmpresaModeloConteudoInfo();
            ViewModelToModelMapper.Map<EmpresaModeloConteudoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsEmpresa))
            {
                CommaSeparatedToList(vm.IdsEmpresa).ForEach(idEmpresa =>
                {
                    EmpresaModeloConteudoEmpresaInfo relacao = new EmpresaModeloConteudoEmpresaInfo();
                    relacao.IdEmpresa = idEmpresa;

                    info.Relacao_EmpresaConteudo.Add(relacao);
                });
            }

            var response = EmpresaModeloConteudoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdEmpresa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEmpresa, "EmpresaMolodeConteudo", response.Response.IdHistorico);
            return Json(response);
        }

        #endregion


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EmpresaModeloConteudoInfo info = new EmpresaModeloConteudoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaModeloConteudo = vm.IdEmpresaModeloConteudo;
            info.IdTela = vm.IdTela;
            info.IdModulo = vm.IdModulo;
            info.PermiteCadastroIndependente = vm.PermiteCadastroIndependente;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<EmpresaModeloConteudoInfo> retorno = new ListPaged<EmpresaModeloConteudoInfo>(EmpresaModeloConteudoBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Modelo de Conteúdo");
        }
    }
}
