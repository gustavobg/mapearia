﻿using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.NaturezaOperacional;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class NaturezaOperacionalController : ControllerExtended
    {
        public NaturezaOperacionalController()
        {
            IndexUrl = "/Corporativo/NaturezaOperacional/Index";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            NaturezaOperacionalInfo info = new NaturezaOperacionalInfo();
            info.IdNaturezaOperacional = vm.IdNaturezaOperacional;
            info.IdNaturezaOperacionalIn = vm.IdNaturezaOperacionalIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            if (vm.VinculadasAoPlanoTecnico.HasValue && vm.VinculadasAoPlanoTecnico.Value)
            {
                vm.IdNaturezaOperacionalIn = string.Join(",", (GrupoMenuNaturezaOperacionalBll.Instance.ListarPorParametros(new GrupoMenuNaturezaOperacionalInfo
                { IdGrupoMenu = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdGrupoMenu }).Select(p => p.IdNaturezaOperacional.Value)));
            }
            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<NaturezaOperacionalInfo> retorno = new ListPaged<NaturezaOperacionalInfo>(NaturezaOperacionalBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação:Natureza Operacional");

        }

    }
}
