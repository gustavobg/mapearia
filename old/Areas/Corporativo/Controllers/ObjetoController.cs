﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Objeto;


using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Bll.Corporativo;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class ObjetoController : ControllerExtended
    {
        //
        // GET: /Corporativo/Objeto/

        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarPorAplicacaoEquipamento(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdAutomotivoEquipamento = vm.IdAutomotivoEquipamento;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarPorAplicacaoAutomotivoEquipamento(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Equipamento", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarPorAplicacaoBenfeitoria(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdBenfeitoria = vm.IdBenfeitoria;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarPorAplicacaoBenfeitoria(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Benfeitoria", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarPorAplicacaoProducaoLocal(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarPorAplicacaoProducaoLocal(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Local Produção", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarPorAplicacaoCriacaoAnimal(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoAninal = vm.IdCriacaoAnimal;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarPorAplicacaoCriacaoAnimal(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Criação Animal", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarPorAplicacaoCriacaoLote(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoLote = vm.IdCriacaoLote;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarPorAplicacaoCriacaoLote(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Criação Lote", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListarObjetoCentroCustoSemAplicacao(ListVM vm)
        {
            ObjetoInfo info = new ObjetoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdObjeto = vm.IdObjeto;
            info.DescricaoBusca = vm.DescricaoBusca;

            ListPaged<ObjetoInfo> retorno = new ListPaged<ObjetoInfo>(ObjetoBll.Instance.ListarObjetoCentroCustoSemAplicacao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Objeto x Local Produção", true);
        }

    }
}
