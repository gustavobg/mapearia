﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Ordem;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class OrdemController : ControllerExtended
    {

        public OrdemController()
        {
            IndexUrl = "/Corporativo/Ordem/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            OrdemInfo info = new OrdemInfo();
            ViewModelToModelMapper.Map<OrdemInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa; // Empresa Usuária Logada.

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataInicioEstimadaExibicao))
                info.DataInicioEstimativa = Convert.ToDateTime(vm.DataInicioEstimadaExibicao);

            if (!string.IsNullOrEmpty(vm.DataTerminoEstimadaExibicao))
                info.DataTerminoEstimativa = Convert.ToDateTime(vm.DataTerminoEstimadaExibicao);

            if (!string.IsNullOrEmpty(vm.DataInicioRealizadoExibicao))
                info.DataInicioRealizado = Convert.ToDateTime(vm.DataInicioRealizadoExibicao);

            if (!string.IsNullOrEmpty(vm.DataTerminoRealizadoExibicao))
                info.DataTerminoRealizado = Convert.ToDateTime(vm.DataTerminoRealizadoExibicao);

            //responsáveis
            if (!string.IsNullOrEmpty(vm.IdsPessoaResponsavel))
            {
                info.Responsaveis = new List<OrdemPessoaResponsavelInfo>();
                CommaSeparatedToList(vm.IdsPessoaResponsavel).ForEach(idpessoa =>
                {
                    OrdemPessoaResponsavelInfo item = new OrdemPessoaResponsavelInfo();
                    item.IdPessoa = idpessoa;

                    info.Responsaveis.Add(item);
                });
            }

            //Aplicação
            if (!string.IsNullOrEmpty(vm.IdsAplicacao))
            {
                info.Aplicacoes = new List<OrdemAplicacaoInfo>();
                CommaSeparatedToList(vm.IdsAplicacao).ForEach(idAplicacao =>
                {
                    OrdemAplicacaoInfo item = new OrdemAplicacaoInfo();
                    item.IdAplicacaoReferencia = idAplicacao;

                    info.Aplicacoes.Add(item);
                });
            }

            //SubAplicação
            if (!string.IsNullOrEmpty(vm.IdsSubAplicacao))
            {
                info.SubAplicacoes = new List<OrdemSubAplicacaoInfo>();
                CommaSeparatedToList(vm.IdsSubAplicacao).ForEach(idSubAplicacao =>
                {
                    OrdemSubAplicacaoInfo item = new OrdemSubAplicacaoInfo();
                    item.IdSubAplicacao = idSubAplicacao;

                    info.SubAplicacoes.Add(item);
                });
            }

            //Aplicacao - ProducaoLocal
            //if(!string.IsNullOrEmpty(vm.IdsProducaoLocal))
            //{
            //    info.LocaisProducao = new List<OrdemProducaoLocalInfo>();
            //    CommaSeparatedToList(vm.IdsProducaoLocal).ForEach(idProducaoLocal =>
            //    {
            //        OrdemProducaoLocalInfo item = new OrdemProducaoLocalInfo();
            //        item.IdProducaoLocal = idProducaoLocal;

            //        info.LocaisProducao.Add(item);
            //    });
            //}

            //Sub Aplicacao - ProducaoUnidade
            //if(!string.IsNullOrEmpty(vm.IdsProducaoUnidade))
            //{
            //    info.UnidadesProducao = new List<OrdemProducaoUnidadeInfo>();
            //    CommaSeparatedToList(vm.IdsProducaoUnidade).ForEach(idProducaoUnidade =>
            //    {
            //        OrdemProducaoUnidadeInfo item = new OrdemProducaoUnidadeInfo();
            //        item.IdProducaoUnidade = idProducaoUnidade;

            //        info.UnidadesProducao.Add(item);
            //    });
            //}

            #endregion

            var response = OrdemBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdOrdem.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdOrdem, "Ordem", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                OrdemInfo info = OrdemBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                #region Conversão

                if (info.DataInicioEstimativa.HasValue)
                    vm.DataInicioEstimadaExibicao = info.DataInicioEstimativa.Value.ToString("dd/MM/yyyy HH:mm:ss");

                if (info.DataTerminoEstimativa.HasValue)
                    vm.DataTerminoEstimadaExibicao = info.DataTerminoEstimativa.Value.ToString("dd/MM/yyyy HH:mm:ss");

                if (info.DataInicioRealizado.HasValue)
                    vm.DataInicioRealizadoExibicao = info.DataInicioRealizado.Value.ToString("dd/MM/yyyy HH:mm:ss");

                if (info.DataTerminoRealizado.HasValue)
                    vm.DataTerminoRealizadoExibicao = info.DataTerminoRealizado.Value.ToString("dd/MM/yyyy HH:mm:ss");

                #endregion

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdOrdem, "Ordem");
            }
            else
            {
                vm.Ativo = true;
            }

            vm.IdUsuarioLogado = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario.Value;
            vm.IdEmpresaUsuariaLogada = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa.Value;

            return Json(vm);
        }

        [HttpGet]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            OrdemInfo info = new OrdemInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdOrdem = vm.IdOrdem;
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            info.IdCategoriaServicoIn = vm.IdCategoriaServicoIn;
            info.IdNaturezaOperacionalIn = vm.IdNaturezaOperacionalIn;
            info.DescricaoBusca = vm.DescricaoBusca;

            info.IdAplicacaoReferenciaIn = vm.IdAplicacaoReferenciaIn;
            info.IdSubAplicacaoReferenciaIn = vm.IdSubAplicacaoReferenciaIn;

            #region Datas

            if (!string.IsNullOrEmpty(vm.StrDataInicioEstimativa))
                info.DataInicioEstimativa = DateTime.Parse(vm.StrDataInicioEstimativa);

            if (!string.IsNullOrEmpty(vm.StrDataTerminoEstimativa))
                info.DataTerminoEstimativa = DateTime.Parse(vm.StrDataTerminoEstimativa);

            if (!string.IsNullOrEmpty(vm.StrDataInicioRealizado))
                info.DataInicioRealizado = DateTime.Parse(vm.StrDataInicioRealizado);

            if (!string.IsNullOrEmpty(vm.StrDataTerminoRealizado))
                info.DataTerminoRealizado = DateTime.Parse(vm.StrDataTerminoRealizado);

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<OrdemInfo> retorno = new ListPaged<OrdemInfo>(OrdemBll.Instance.ListarPorParametroComDescricoes(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação:Ordem");

        }
    }
}
