﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Contato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Contato.Controllers
{
    public class Controller : ControllerExtended
    {
        //public Controller()
        //{
        //    IndexUrl = "/Configuracao/PessoaContato/Index";
        //}

        //private PessoaContatoBLL bll = new PessoaContatoBLL();

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarPessoaContato dto = new DTOSalvarPessoaContato();
        //    ViewModelToModelMapper.Map<Entity.PessoaContato>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = dto.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarContatoResponse response = bll.SalvarContato(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdPessoaContato.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoaContato, "PessoaContato", response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        Entity.PessoaContato entidade = bll.AdquirirContato(new DTOAdquirirPessoaContato() { IdPessoaContato = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdPessoaContato, "PessoaContato");
        //    }
        //    else
        //    {
        //        vm.Ativo = true;
        //    }

        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirPessoaContato dto = new DTOAdquirirPessoaContato();
        //    dto.Page = vm.Page;

        //    ListPaged<Entity.PessoaContato> retorno = bll.AdquirirContato(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: Contato");
        //}

    }
}
