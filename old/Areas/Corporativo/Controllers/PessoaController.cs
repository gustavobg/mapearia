using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Localizacao.Controller.HTMCEP;




using Newtonsoft.Json;
using MasterGestor.CORE.Nucleo;
using log4net;
using System.Reflection;

using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Bll.Corporativo;
using Manager.CORE.Nucleo.Helpers;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class PessoaController : ControllerExtended
    {
        public PessoaController()
        {
            IndexUrl = "/Corporativo/Pessoa/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New / Edit

        [HttpGet, CustomAuthorize]
        public ActionResult New()
        {
            NewVM vm = new NewVM();

            ViewModelToModelMapper.MapBack<NewVM>(vm, new PessoaInfo());

            return View(vm);
        }

        [HttpPost]
        public ActionResult New(string json)
        {

            NewVM vm = new NewVM();

            vm = JsonConvert.DeserializeObject<NewVM>(json);

            PessoaInfo pessoaInfo = new PessoaInfo();

            ViewModelToModelMapper.Map<PessoaInfo>(vm, pessoaInfo);

            pessoaInfo.PessoaEmpresa = null;

            pessoaInfo.IdPessoaOperacao = IdPessoa;
            pessoaInfo.IdEmpresa = IdEmpresa;
            pessoaInfo.IdEmpresaLogada = IdEmpresa;
            pessoaInfo.Corporacao = false; //Sempre será uma Pessoa Física ou Jurídica.

            // monta lista de IdPessoaPerfilPessoa
            if (!string.IsNullOrEmpty(vm.IdsPerfil))
            {
                pessoaInfo.Perfis = new List<PessoaPerfilPessoaInfo>();

                CommaSeparatedToList(vm.IdsPerfil).ForEach(idPessoaPerfil =>
                {
                    PessoaPerfilPessoaInfo pi = new PessoaPerfilPessoaInfo();
                    pi.IdPessoaPerfil = idPessoaPerfil;

                    pessoaInfo.Perfis.Add(pi);
                });
            }
            var response = PessoaBll.Instance.Salvar(pessoaInfo);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoa, response.ToString(), response.Response.IdHistorico);

            return Json(response);

        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PessoaInfo pessoaInfo = new PessoaInfo();

            //Deixo essa classe nula pois não será cadastrado a PessoaEmpresa (Empresa Usuária)
            ViewModelToModelMapper.Map<PessoaInfo>(vm, pessoaInfo);

            pessoaInfo.PessoaEmpresa = null;

            pessoaInfo.IdPessoaOperacao = IdPessoa;
            pessoaInfo.IdEmpresa = IdEmpresa;
            pessoaInfo.IdEmpresaLogada = IdEmpresa;

            // monta lista de IdPessoaPerfilPessoa
            if (!string.IsNullOrEmpty(vm.IdsPerfil))
            {
                pessoaInfo.Perfis = new List<PessoaPerfilPessoaInfo>();

                CommaSeparatedToList(vm.IdsPerfil).ForEach(idPessoaPerfil =>
                {
                    PessoaPerfilPessoaInfo item = new PessoaPerfilPessoaInfo();
                    item.IdPessoaPerfil = idPessoaPerfil;

                    pessoaInfo.Perfis.Add(item);
                });
            }

            if (!string.IsNullOrEmpty(vm.DataExibicao))
                pessoaInfo.Data = Convert.ToDateTime(vm.DataExibicao);

            var response = PessoaBll.Instance.Salvar(pessoaInfo);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoa, "Pessoa", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            CarregaCaracteristica(vm);

            vm.IdPessoaNatureza = 2;
            vm.Corporacao = false;
            vm.Ativo = true;
            vm.Usuario.UsuarioNuncaAcessou = true;

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            //ViewBag.TiposLogradouro = new HTMCEPController().BuscaListaTipoLogradouro();

            if (id.HasValue && id > 0)
            {
                PessoaInfo info = new PessoaInfo();
                info = PessoaBll.Instance.ListarCompletoPorId(new PessoaInfo { IdPessoa = id, IdEmpresa = IdEmpresa });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                if (info.Data.HasValue)
                    vm.DataExibicao = info.Data.Value.ToShortDateString();

                vm.IdsPerfil = string.Join(",", info.Perfis.Select(x => x.IdPessoaPerfil));
                vm.Usuario.BotaoAtivacao = vm.Usuario.Ativo == false ? false : true;
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPessoa, info.Nome);
            }
            else
            {
                CarregaCaracteristica(vm);

                vm.IdPessoaNatureza = 2;
                vm.Corporacao = false;
                vm.Ativo = true;
                vm.Usuario.UsuarioNuncaAcessou = true;
            }
            return Json(vm);
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PessoaInfo info = new PessoaInfo();
            info.IdPessoa = vm.IdPessoa;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.IdPessoaPerfilIn = vm.IdPessoaPerfilIn;
            info.IdPessoaNaturezaIn = vm.IdPessoaNaturezaIn;
            info.IdPessoaNaturezaNotIn = vm.IdPessoaNaturezaNotIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdEmpresa = IdEmpresa;
            info.Nomes = vm.Nomes;
            info.Documentos = vm.DocumentoTelefone;
            info.Ativo = vm.Ativo;
            info.IdUsuario = vm.IdUsuario;

            info.Corporacao = false; //Retornar apenas pessoas (NOT IN EMPRESA, NOT IN EMPRESAUSUÁRIA)

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<PessoaInfo> retorno = new ListPaged<PessoaInfo>(PessoaBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Pessoa");
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListPessoaPersonalizado(ListVM vm)
        {
            PessoaInfo info = new PessoaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa == 0 ? null : vm.IdPessoa;
            info.Nome = vm.Nomes;
            info.NomeSecundario = vm.ApelidoFantasia;
            info.Ativo = vm.Ativo;
            info.ComEmpresa = vm.ComEmpresa;
            info.ComEmpresaUsuaria = vm.ComEmpresaUsuaria;
            info.ComPessoa = vm.ComPessoa;
            info.Nomes = vm.NomeDocumento;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdPessoaNatureza = vm.IdPessoaNatureza;
            info.IdPessoaPerfilIn = vm.IdPessoaPerfilIn;
            info.PessoaPerfil_PermiteRealizarCompra = vm.PessoaPerfil_PermiteRealizarCompra;


            ListPaged<PessoaInfo> retorno = new ListPaged<PessoaInfo>(PessoaBll.Instance.ListarPessoaPorTipo(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Pessoa", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListPorTipoComRestricaoCompraEstoque(ListVM vm)
        {
            PessoaInfo info = new PessoaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa == 0 ? null : vm.IdPessoa;
            info.Nome = vm.Nomes;
            info.NomeSecundario = vm.ApelidoFantasia;
            info.Ativo = vm.Ativo;
            info.ComEmpresa = vm.ComEmpresa;
            info.ComEmpresaUsuaria = vm.ComEmpresaUsuaria;
            info.ComPessoa = vm.ComPessoa;
            info.Nomes = vm.NomeDocumento;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdPessoaNatureza = vm.IdPessoaNatureza;
            info.IdPessoaNaturezaIn = vm.IdPessoaNaturezaIn;
            info.IdPessoaNaturezaNotIn = vm.IdPessoaNaturezaNotIn;
            info.IdPessoaPerfilIn = vm.IdPessoaPerfilIn;
            info.PessoaPerfil_PermiteRealizarCompra = vm.PessoaPerfil_PermiteRealizarCompra;

            ListPaged<PessoaInfo> retorno = new ListPaged<PessoaInfo>(PessoaBll.Instance.ListarPorTipoComRestricaoCompraEstoque(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Pessoa", true);
        }

        #endregion

        #region Usuário

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Usuario(UsuarioVM vm)
        {
            UsuarioInfo info = new UsuarioInfo();

            ViewModelToModelMapper.Map<UsuarioInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões

            //Plano de Contas (PlanoContas)
            if (!string.IsNullOrEmpty(vm.IdsPlanoContas))
            {
                info.PlanoContasRestricoes = new List<UsuarioPlanoContasRestricaoInfo>();

                CommaSeparatedToList(vm.IdsPlanoContas).ForEach(idPlanoContas =>
                {
                    UsuarioPlanoContasRestricaoInfo restricaoPlanoContas = new UsuarioPlanoContasRestricaoInfo();
                    restricaoPlanoContas.IdPlanoContas = idPlanoContas;
                    info.PlanoContasRestricoes.Add(restricaoPlanoContas);
                });
            }

            //Centro de Custo (CentroCusto)
            if (!string.IsNullOrEmpty(vm.IdsCentroCusto))
            {
                info.CentrosCustosRestritos = new List<UsuarioCentroCustoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsCentroCusto).ForEach(idCentroCusto =>
                {
                    UsuarioCentroCustoRestricaoInfo restricaoCentroCusto = new UsuarioCentroCustoRestricaoInfo();
                    restricaoCentroCusto.IdCentroCusto = idCentroCusto;
                    info.CentrosCustosRestritos.Add(restricaoCentroCusto);
                });
            }

            //Classificação Financeira (FinanceiroClassificacao)
            if (!string.IsNullOrEmpty(vm.IdsFinanceiroClassificacao))
            {
                info.ClassificacoesRestritas = new List<UsuarioFinanceiroClassificacaoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsFinanceiroClassificacao).ForEach(idFinanceiroClassificacao =>
                {
                    UsuarioFinanceiroClassificacaoRestricaoInfo restricaoFinanceiroClassificacao = new UsuarioFinanceiroClassificacaoRestricaoInfo();
                    restricaoFinanceiroClassificacao.IdFinanceiroClassificacao = idFinanceiroClassificacao;
                    info.ClassificacoesRestritas.Add(restricaoFinanceiroClassificacao);
                });
            }

            // Grupo de Produto e Serviço (ProdutoServicoGrupo)
            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.GruposProdutosRestritos = new List<UsuarioProdutoServicoGrupoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(idProdutoServicoGrupo =>
                {
                    UsuarioProdutoServicoGrupoRestricaoInfo restricaoProdutoServicoGrupo = new UsuarioProdutoServicoGrupoRestricaoInfo();
                    restricaoProdutoServicoGrupo.IdProdutoServicoGrupo = idProdutoServicoGrupo;
                    info.GruposProdutosRestritos.Add(restricaoProdutoServicoGrupo);
                });
            }

            //Produto (ProdutoServico)
            if (!string.IsNullOrEmpty(vm.IdsProdutoServico))
            {
                info.ProdutosRestritos = new List<UsuarioProdutoServicoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServico).ForEach(idProdutoServico =>
                {
                    UsuarioProdutoServicoRestricaoInfo restricaoProdutoServico = new UsuarioProdutoServicoRestricaoInfo();
                    restricaoProdutoServico.IdProdutoServico = idProdutoServico;
                    info.ProdutosRestritos.Add(restricaoProdutoServico);
                });
            }

            //Tipo de Aplicação (AplicacaoTipo)
            if (!string.IsNullOrEmpty(vm.IdsAplicacaoTipo))
            {
                info.TiposAplicacaoesRestritos = new List<UsuarioAplicacaoTipoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsAplicacaoTipo).ForEach(idAplicacaoTipo =>
                {
                    UsuarioAplicacaoTipoRestricaoInfo restricaoAplicacaoTipo = new UsuarioAplicacaoTipoRestricaoInfo();
                    restricaoAplicacaoTipo.IdAplicacaoTipo = idAplicacaoTipo;
                    info.TiposAplicacaoesRestritos.Add(restricaoAplicacaoTipo);
                });
            }

            //Local de Produção (ProducaoLocal)
            if (!string.IsNullOrEmpty(vm.IdsProdutoLocal))
            {
                info.LocaisRestritos = new List<UsuarioProdutoLocalRestricaoInfo>();

                CommaSeparatedToList(vm.IdsProdutoLocal).ForEach(idProducaoLocal =>
                {
                    UsuarioProdutoLocalRestricaoInfo restricaoProdutoLocal = new UsuarioProdutoLocalRestricaoInfo();
                    restricaoProdutoLocal.IdProdutoLocal = idProducaoLocal;
                    info.LocaisRestritos.Add(restricaoProdutoLocal);
                });
            }

            //Pessoa Empresa (Empresa Usuária)
            if (!string.IsNullOrEmpty(vm.IdsPessoaEmpresa))
            {
                info.EmpresasUsuariasRestritas = new List<UsuarioPessoaEmpresaRestricaoInfo>();

                CommaSeparatedToList(vm.IdsPessoaEmpresa).ForEach(idpessoa =>
                {
                    UsuarioPessoaEmpresaRestricaoInfo restricaoEmpresaUsuaria = new UsuarioPessoaEmpresaRestricaoInfo();
                    restricaoEmpresaUsuaria.IdPessoa = idpessoa;
                    info.EmpresasUsuariasRestritas.Add(restricaoEmpresaUsuaria);
                });
            }

            //Grupo de Responsabilidade
            if (!string.IsNullOrEmpty(vm.IdsGrupoResponsabilidade))
            {
                info.lstGrupoResponsabilidade = new List<UsuarioGrupoResponsabilidadeInfo>();

                CommaSeparatedToList(vm.IdsGrupoResponsabilidade).ForEach(idGrupoResponsabilidade =>
                {
                    UsuarioGrupoResponsabilidadeInfo itemGrupoResponsabilidade = new UsuarioGrupoResponsabilidadeInfo();
                    itemGrupoResponsabilidade.IdGrupoResponsabilidade = idGrupoResponsabilidade;
                    info.lstGrupoResponsabilidade.Add(itemGrupoResponsabilidade);
                });
            }

            #endregion

            var response = UsuarioBll.Instance.SalvarUsuarioGerandoPerfilComNotificacao(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdUsuario, "Usuario", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Usuario(int? id)
        {
            UsuarioVM vm = new UsuarioVM();
            vm.Ativo = true;
            vm.UsuarioNuncaAcessou = false;

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult UsuarioJson(int? id) // Identificador Pessoa [IdPessoa]
        {
            UsuarioVM vm = new UsuarioVM();
            if (id.HasValue && id > 0)
            {
                UsuarioInfo info = new UsuarioInfo();
                info = UsuarioBll.Instance.RetornaUsuarioComRestricoes(new UsuarioInfo { IdPessoa = id, IdEmpresa = IdEmpresa });
                if (info.IdUsuario.HasValue)
                {
                    ViewModelToModelMapper.MapBack<UsuarioVM>(vm, info);
                    vm.BotaoAtivacao = vm.Ativo == false ? true : false;
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdUsuario, "Usuario");
                }
                else
                {
                    ViewModelToModelMapper.MapBack<UsuarioVM>(vm, info);
                    vm.Ativo = true;
                    vm.UsuarioNuncaAcessou = true;
                    vm.BotaoAtivacao = false;
                }

                if (id.HasValue)
                    vm.NomeExibicao = PessoaBll.Instance.ListarPorCodigo(id.Value).NomePessoaExibicao;
            }
            else
            {
                vm.Ativo = true;
                vm.UsuarioNuncaAcessou = true;
                vm.BotaoAtivacao = false;
            }
            return Json(vm);
        }

        //TODO: Não Utilizar | Refactor
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListUsuario(ListVM vm)
        {
            UsuarioInfo info = new UsuarioInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa;

            List<UsuarioInfo> lstRet = new List<UsuarioInfo>();
            lstRet.Add(UsuarioBll.Instance.RetornaUsuarioComRestricoes(info));

            ListPaged<UsuarioInfo> retorno = new ListPaged<UsuarioInfo>(lstRet)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Usuário", true);
        }

        #endregion

        #region Conta Bancária

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult PessoaContaBancaria(PessoaContaBancariaVM vm)
        {
            PessoaContaBancariaInfo info = new PessoaContaBancariaInfo();

            ViewModelToModelMapper.Map<PessoaContaBancariaInfo>(vm, info);

            var response = PessoaContaBancariaBll.Instance.Salvar(info.Contas, IdPessoa: vm.IdPessoa.Value, IdPessoaOperacao: IdPessoa, IdEmpresaLogada: IdEmpresa);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoa, "PessoaContaBancaria", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult PessoaContaBancaria(int? id)
        {
            PessoaContaBancariaVM vm = new PessoaContaBancariaVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult PessoaContaBancariaJson(int? id) // Identificador Pessoa [IdPessoa]
        {
            PessoaContaBancariaVM vm = new PessoaContaBancariaVM();
            if (id.HasValue && id > 0)
            {
                PessoaContaBancariaInfo info = new PessoaContaBancariaInfo();
                PessoaInfo pessoaInfo = new PessoaInfo();

                pessoaInfo = PessoaBll.Instance.ListarPorCodigo(id.Value);
                info.Contas = PessoaContaBancariaBll.Instance.ListarPorParametros(new PessoaContaBancariaInfo { IdPessoa = id.Value });

                info.IdPessoa = pessoaInfo.IdPessoa;

                if (info.IdPessoa.HasValue)
                {
                    ViewModelToModelMapper.MapBack<PessoaContaBancariaVM>(vm, info);
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPessoa, "PessoaContaBancaria");

                    vm.Codigo = pessoaInfo.Codigo;
                    vm.NomeSecundario = pessoaInfo.NomeSecundario;
                    vm.Nome = pessoaInfo.Nome;
                }
                else
                {
                    ViewModelToModelMapper.MapBack<PessoaContaBancariaVM>(vm, info);
                    vm.Codigo = pessoaInfo.Codigo;
                    vm.NomeSecundario = pessoaInfo.NomeSecundario;
                    vm.Nome = pessoaInfo.Nome;
                }
            }
            else
            {
                vm.Response.Sucesso = false;

            }
            return Json(vm);
        }

        #endregion

        #region Utilitários

        public JsonResult ValidarCPFCNPJ(string documento)
        {
            bool resultado = false;
            string tipoDocumento = string.Empty;

            if (!string.IsNullOrEmpty(documento))
            {
                if (documento.Length == 14)
                {
                    tipoDocumento = "cpf";
                    resultado = StringExtensions.IsCPF(documento);
                }
                else if (documento.Length == 18)
                {
                    tipoDocumento = "cpnj";
                    resultado = StringExtensions.IsCNPJ(documento);
                }
                else
                    resultado = false;
            }
            return Json(new
            {
                DocumentoValido = resultado,
                TipoDocumento = ""
            });
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ReenviarEmailAtivacao(int idPessoa)
        {
            var usuario = UsuarioBll.Instance.ListarPorParametros(new UsuarioInfo { IdPessoa = IdPessoa }).FirstOrDefault();

            var usuarioNotificacao = UsuarioBll.Instance.ListarPorCodigo(usuario.IdUsuario.Value);
            //var success = new NotificacaoGeraEnvioBll().GeraNotificacaoAtivacaoUsuario(usuarioNotificacao);

            //if (success)
            //{
            //    return Json(new { Sucesso = true, Mensagem = "E-mail de ativação de usuário reenviado." });
            //}
            //else
            //{
            //    return Json(new
            //    {
            //        Sucesso = false,
            //        Mensagem = @"Ocorreu um problema ao enviar o e-mail de ativação de usuário. 
            //                                Por favor tente novamente."
            //    });
            //}

            return Json(new { Sucesso = true, Mensagem = "E-mail de ativação de usuário reenviado." });
        }

        private void CarregaCaracteristica(NewEditVM vm)
        {
            var info = new CaracteristicaInfo
            {
                IdEmpresa = IdEmpresa,
                IdCaracteristicaTabela = 1,
                Ativo = true
            };

            var caracteristicas = CaracteristicaBll.Instance.ListarPorParametros(info).OrderBy(p => p.Ordem);

            foreach (var c in caracteristicas)
            {
                vm.Caracteristicas.Add(new NewEditVM.CaracteristicasVM
                {
                    CaracteristicaValor = new NewEditVM.CaracteristicaValorVM(),
                    Codigo = c.Codigo,
                    Descricao = c.Descricao,
                    IdCaracteristica = c.IdCaracteristica,
                    IdCaracteristicaTabela = c.IdCaracteristicaTabela,
                    IdCaracteristicaTipo = c.IdCaracteristicaTipo
                });
            }
        }

        #endregion
    }
}
