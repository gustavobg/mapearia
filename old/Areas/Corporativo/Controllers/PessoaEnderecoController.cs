﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Endereco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class PessoaEnderecoController : ControllerExtended
    {
        public PessoaEnderecoController()
        {
            IndexUrl = "/Corporativo/PessoaEndereco/Index";
        }

        //private PessoaEnderecoBLL bll = new PessoaEnderecoBLL();

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarEndereco dto = new DTOSalvarEndereco();
        //    ViewModelToModelMapper.Map<PessoaEndereco>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarEnderecoResponse response = bll.SalvarEndereco(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdPessoaEndereco.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoaEndereco, "PessoaEndereco", response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        PessoaEndereco entidade = bll.AdquirirEndereco(new DTOAdquirirPessoaEndereco() { IdPessoaEndereco = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdPessoaEndereco, "PessoaEndereco");
        //    }

        //    return View(vm);
        //}


        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    DTOAdquirirPessoaEndereco dto = new DTOAdquirirPessoaEndereco();
        //    dto.IdLocalidade = vm.IdLocalidade;
        //    dto.IdPessoaEndereco = vm.IdPessoaEndereco;
        //    dto.IdPessoa = vm.IdPessoa;
        //    dto.Localidade = vm.Localidade;
        //    dto.LocalidadeEstado = vm.LocalidadeEstado;
        //    dto.Ativo = vm.Ativo;
        //    dto.Page = vm.Page;

        //    ListPaged<PessoaEndereco> retorno = bll.AdquirirEndereco(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: Endereco");
        //}

    }
}
