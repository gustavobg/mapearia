﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EnderecoTipo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Model.Corporativo;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class PessoaEnderecoTipoController : ControllerExtended
    {
        public PessoaEnderecoTipoController()
        {
            IndexUrl = "/Corporativo/PessoaEnderecoTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PessoaEnderecoTipoInfo info = new PessoaEnderecoTipoInfo();
            ViewModelToModelMapper.Map<PessoaEnderecoTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = PessoaEnderecoTipoBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoaEnderecoTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdPessoaEnderecoTipo, "PessoaEnderecoTipo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                PessoaEnderecoTipoInfo info = new PessoaEnderecoTipoInfo();
                info = PessoaEnderecoTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPessoaEnderecoTipo, "PessoaEnderecoTipo");
            }
            else
                vm.Ativo = true;
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PessoaEnderecoTipoInfo info = new PessoaEnderecoTipoInfo();

            info.IdPessoaEnderecoTipo = vm.IdPessoaEnderecoTipo;
            info.IdPessoaEnderecoTipoIn = vm.IdPessoaEnderecoTipoIn;
            info.IdPessoaEnderecoTipoNotIn = vm.IdPessoaEnderecoTipoNotIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<PessoaEnderecoTipoInfo> retorno = new ListPaged<PessoaEnderecoTipoInfo>(PessoaEnderecoTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Pessoa", true);
        }

    }
}
