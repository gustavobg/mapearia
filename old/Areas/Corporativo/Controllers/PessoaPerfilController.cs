﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.PessoaPerfil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Bll.Corporativo;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class PessoaPerfilController : ControllerExtended
    {
        public PessoaPerfilController()
        {
            IndexUrl = "/Corporativo/PessoaPerfil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            PessoaPerfilInfo info = new PessoaPerfilInfo(IdEmpresa);
            if (id.HasValue && id > 0)
            {
                //info = PessoaPerfilBll.Instance.ListarPorCodigo(id.Value);
                info = PessoaPerfilBll.Instance.ListarPorParametros(new PessoaPerfilInfo(IdEmpresa) { IdPessoaPerfil = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdPessoaPerfil, "PessoaPerfil");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            PessoaPerfilInfo info = new PessoaPerfilInfo();

            ViewModelToModelMapper.Map<PessoaPerfilInfo>(vm, info);
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = PessoaPerfilBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdPessoaPerfil.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdPessoaPerfil, "PessoaPerfil", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            PessoaPerfilEmpresaInfo info = new PessoaPerfilEmpresaInfo();
            PessoaPerfilEmpresaInfo response = new PessoaPerfilEmpresaInfo();

            if (id > 0)
            {
                info = PessoaPerfilEmpresaBll.Instance.ListarPorParametros(new PessoaPerfilEmpresaInfo { IdPessoaPerfil = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = PessoaPerfilEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdPessoaPerfil, "PessoaPerfil", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            PessoaPerfilEmpresaInfo info = new PessoaPerfilEmpresaInfo();
            PessoaPerfilEmpresaInfo response = new PessoaPerfilEmpresaInfo();

            if (id > 0)
            {
                info = PessoaPerfilEmpresaBll.Instance.ListarPorParametros(new PessoaPerfilEmpresaInfo { IdPessoaPerfil = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = PessoaPerfilEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdPessoaPerfil, "PessoaPerfil", info.Response.IdHistorico);
            }

            return Json(new { response });
        }
        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            PessoaPerfilInfo info = new PessoaPerfilInfo(IdEmpresa);

            info.IdEmpresa = IdEmpresa;
            info.IdPessoaPerfilIn = vm.IdPessoaPerfilIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.PermiteRealizarCompra = vm.PermiteRealizarCompra;
            info.PossuiConceitoColaboradorFuncionario = vm.PossuiConceitoColaboradorFuncionario;
            info.PermiteUsuarioSistema = vm.PermiteUsuarioSistema;
            info.PermiteRealizarVenda = vm.PermiteRealizarVenda;
            info.PossuiConceitoTransportadora = vm.PossuiConceitoTransportadora;
            info.CNPJ = vm.CNPJ;
            info.DataAbertura = vm.DataAbertura;
            info.EnderecoSede = vm.EnderecoSede;
            info.IE = vm.IE;
            info.IM = vm.IM;
            info.Telefone1PJ = vm.Telefone1PJ;
            info.Telefone2PJ = vm.Telefone2PJ;
            info.EmailPJ = vm.EmailPJ;
            info.CPF = vm.CPF;
            info.RG = vm.RG;
            info.CNH = vm.CNH;
            info.DataNascimento = vm.DataNascimento;
            info.Telefone1PF = vm.Telefone1PF;
            info.Telefone2PF = vm.Telefone2PF;
            info.EmailPF = vm.EmailPF;
            info.EnderecoDomicilio = vm.EnderecoDomicilio;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<PessoaPerfilInfo> retorno = new ListPaged<PessoaPerfilInfo>(PessoaPerfilBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Perfil de Pessoa", true);

        }

    }
}
