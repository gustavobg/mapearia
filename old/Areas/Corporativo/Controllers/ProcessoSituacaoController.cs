﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ProcessoSituacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class ProcessoSituacaoController : ControllerExtended
    {
        public ProcessoSituacaoController()
        {
            IndexUrl = "/Corporativo/ProcessoSituacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(int? parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            var processoInfo = ProcessoBll.Instance.ListarPorCodigo(parentId.Value);
            vm.DescricaoExibicao = processoInfo.Descricao;
            vm.ParentId = parentId;

            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProcessoSituacaoInfo info = new ProcessoSituacaoInfo();
            ViewModelToModelMapper.Map<ProcessoSituacaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = ProcessoSituacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProcessoSituacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProcessoSituacao, "ProcessoSituacao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? parentId)
        {
            NewEditVM vm = new NewEditVM();
            
            var processoInfo = ProcessoBll.Instance.ListarPorCodigo(parentId.Value);
            vm.DescricaoProcesso = processoInfo.Descricao;
            vm.IdProcesso = parentId;

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProcessoSituacaoInfo info = new ProcessoSituacaoInfo();
                info = ProcessoSituacaoBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProcessoSituacao, "ProcessoSituacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProcessoSituacaoInfo info = new ProcessoSituacaoInfo();
            info.IdProcesso = vm.IdProcesso;
            info.IdProcessoSituacao = vm.IdProcessoSituacao;
            info.Descricao = vm.Descricao;
            info.Visivel = vm.Visivel;
            info.Finalizado = vm.Finalizado;
            info.Ativo = vm.Ativo;
            info.IdProcessoSituacaoIn = vm.IdProcessoSituacaoIn;

            ListPaged<ProcessoSituacaoInfo> retorno = new ListPaged<ProcessoSituacaoInfo>(ProcessoSituacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Situaçao Processo", true);
        }

    }
}
