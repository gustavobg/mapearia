﻿using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.SubProcessoAcao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class SubProcessoAcaoController : ControllerExtended
    {
        public SubProcessoAcaoController()
        {
            IndexUrl = "/Corporativo/SubProcessoAcao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            SubProcessoAcaoInfo info = new SubProcessoAcaoInfo();
            info.IdSubProcesso = vm.IdSubProcesso;
            info.IdProcessoSituacaoDestino = vm.IdProcessoSituacao;
            info.DescricaoProcessoSituacaoDestino = vm.DescricaoProcessoSituacao;
            info.DescricaoSubProcesso = vm.DescricaoSubProcesso;
            info.InicioProcesso = vm.InicioProcesso;
            info.Ativo = vm.Ativo;

            ListPaged<SubProcessoAcaoInfo> retorno = new ListPaged<SubProcessoAcaoInfo>(SubProcessoAcaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Sub Processo x Situação Destino", true);

        }

    }
}
