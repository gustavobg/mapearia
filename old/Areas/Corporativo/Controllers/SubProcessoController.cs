﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.SubProcesso;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class SubProcessoController : ControllerExtended
    {
        public SubProcessoController()
        {
            IndexUrl = "/Corporativo/Processo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(int parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            var info = ProcessoBll.Instance.ListarPorCodigo(parentId); //Apenas para retornar a Descrição do Processo
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            vm.DescricaoProcesso = info.Descricao;
            vm.ParentId = parentId;

            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            SubProcessoInfo info = new SubProcessoInfo();

            ViewModelToModelMapper.Map<SubProcessoInfo>(vm, info);

            #region Conversões
            if (!string.IsNullOrEmpty(vm.IdsTela))
            {
                info.lstSubProcessoTela = new List<SubProcessoTelaInfo>();

                CommaSeparatedToList(vm.IdsTela).ForEach(idTela =>
                {
                    SubProcessoTelaInfo item = new SubProcessoTelaInfo();
                    item.IdTela = idTela;

                    info.lstSubProcessoTela.Add(item);
                });
            }

            if (!string.IsNullOrEmpty(vm.IdsTransacao))
            {
                info.lstSubProcessoTransacao = new List<SubProcessoTransacaoInfo>();

                CommaSeparatedToList(vm.IdsTransacao).ForEach(idTransacao =>
                {
                    SubProcessoTransacaoInfo item = new SubProcessoTransacaoInfo();
                    item.IdTransacao = idTransacao;

                    info.lstSubProcessoTransacao.Add(item);
                });
            }
            #endregion

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = SubProcessoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdSubProcesso.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdSubProcesso, "SubProcesso", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int parentId)
        {
            NewEditVM vm = new NewEditVM();
            var info = ProcessoBll.Instance.ListarPorCodigo(parentId); //Apenas para retornar a Descrição do Processo

            vm.DescricaoExibicao = info.Descricao;

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                SubProcessoInfo info = new SubProcessoInfo();

                info = SubProcessoBll.Instance.ListarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdSubProcesso, "SubProcesso");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            SubProcessoInfo info = new SubProcessoInfo();
            info.IdProcesso = vm.IdProcesso;
            info.IdSubProcesso = vm.IdSubProcesso;
            info.Descricao = vm.Descricao;
            info.PermiteTransacao = vm.PermiteTransacao;
            info.PermiteAprovacao = vm.PermiteAprovacao;
            info.Ativo = vm.Ativo;

            ListPaged<SubProcessoInfo> retorno = new ListPaged<SubProcessoInfo>(SubProcessoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Sub Processo", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListDocumentoTipoPorSubProcesso(ListVM vm)
        {
            SubProcessoInfo info = new SubProcessoInfo();
            info.IdProcesso = vm.IdProcesso;
            info.IdSubProcesso = vm.IdSubProcesso;
            info.Descricao = vm.Descricao;
            info.PermiteTransacao = vm.PermiteTransacao;
            info.Ativo = vm.Ativo;
            info.IdDocumentoTipoIn = vm.IdDocumentoTipoIn;

            ListPaged<SubProcessoInfo> retorno = new ListPaged<SubProcessoInfo>(SubProcessoBll.Instance.ListarDocumentoTipoPorSubProcesso(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipos de Documento por Sub Processo", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListDocumentoTipoPorProcesso(ListVM vm)
        {
            SubProcessoInfo info = new SubProcessoInfo();
            info.IdProcesso = vm.IdProcesso;
            info.Descricao = vm.Descricao;
            info.PermiteTransacao = vm.PermiteTransacao;
            info.Ativo = vm.Ativo;
            info.IdDocumentoTipoIn = vm.IdDocumentoTipoIn;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;

            ListPaged<SubProcessoInfo> retorno = new ListPaged<SubProcessoInfo>(SubProcessoBll.Instance.ListarDocumentoTipoPorProcesso(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Viculos de Tipos de Documento por Processo", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListAprovacaoProcessoConfiguracao(ListVM vm)
        {
            SubProcessoInfo info = new SubProcessoInfo();
            info.IdProcesso = vm.IdProcesso;
            info.Descricao = vm.Descricao;
            info.PermiteTransacao = vm.PermiteTransacao;
            info.Ativo = vm.Ativo;
            info.IdEmpresa = IdEmpresa; // Utilizado apenas para este método, pois, processo e subprocesso são Globais
            info.RealizaAprovacaoAprovacaoProcessoConfiguracao = vm.RealizaAprovacao;
            info.IdAprovacaoProcessoConfiguracao = vm.IdAprovacaoProcessoConfiguracao;

            ListPaged<SubProcessoInfo> retorno = new ListPaged<SubProcessoInfo>(SubProcessoBll.Instance.ListarAprovacaoProcessoConfiguracao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Viculos de Tipos de Documento por Processo", true);
        }

    }
}
