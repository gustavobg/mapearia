﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Transacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class TransacaoController : ControllerExtended
    {
        public TransacaoController()
        {
            IndexUrl = "/Corporativo/Transacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            TransacaoInfo info = new TransacaoInfo();
            ViewModelToModelMapper.Map<TransacaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.IdsDocumentoTipo))
            {
                info.lstTransacaoDocumentoTipo = new List<TransacaoDocumentoTipoInfo>();

                CommaSeparatedToList(vm.IdsDocumentoTipo).ForEach(idDocumentoTipo =>
                {
                    TransacaoDocumentoTipoInfo item = new TransacaoDocumentoTipoInfo();
                    item.IdDocumentoTipo = idDocumentoTipo;

                    info.lstTransacaoDocumentoTipo.Add(item);
                });
            }
            #endregion

            var response = TransacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdTransacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdTransacao, "Transacao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                TransacaoInfo info = new TransacaoInfo();

                info = TransacaoBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
            }
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                TransacaoInfo info = new TransacaoInfo();
                info = TransacaoBll.Instance.ListarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdTransacao, "Transacao");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            TransacaoInfo info = new TransacaoInfo();
            info.IdTransacao = vm.IdTransacao;
            info.IdTransacaoNotIn = vm.IdTransacaoNotIn;
            info.IdTransacaoIn = vm.IdTransacaoIn;
            info.MovimentacaoFinanceiroTipoIn = vm.MovimentacaoFinanceiroTipoIn;
            info.IdDocumentoTipoIn = vm.IdDocumentoTipoIn;
            info.IdProcesso = vm.IdProcesso;
            info.IdSubProcesso = vm.IdSubProcesso;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<TransacaoInfo> retorno = new ListPaged<TransacaoInfo>(TransacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Transacao", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListTransacaoDocumentoTipoPagamento(ListVM vm)
        {
            TransacaoDocumentoTipoInfo info = new TransacaoDocumentoTipoInfo();
            info.IdTransacao = vm.IdTransacao;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.DescricaoDocumentoTipo = vm.DescricaoDocumentoTipo;
            info.DescricaoTransacao = vm.DescricaoTransacao;
            info.RepresentaAntecipacao = vm.RepresentaAntecipacao;
            info.Ativo = vm.Ativo;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<TransacaoDocumentoTipoInfo> retorno = new ListPaged<TransacaoDocumentoTipoInfo>(TransacaoDocumentoTipoBll.Instance.ListarDocumentoTipoPagamento(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Transacao X DocumentoTipo (Pagamento)");

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListTransacaoDocumentoTipoRecebimento(ListVM vm)
        {
            TransacaoDocumentoTipoInfo info = new TransacaoDocumentoTipoInfo();
            info.IdTransacao = vm.IdTransacao;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.DescricaoDocumentoTipo = vm.DescricaoDocumentoTipo;
            info.DescricaoTransacao = vm.DescricaoTransacao;
            info.RepresentaAntecipacao = vm.RepresentaAntecipacao;
            info.Ativo = vm.Ativo;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<TransacaoDocumentoTipoInfo> retorno = new ListPaged<TransacaoDocumentoTipoInfo>(TransacaoDocumentoTipoBll.Instance.ListarDocumentoTipoRecebimento(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Transacao X DocumentoTipo (Recebimento)");

        }

        [HttpGet]
        [CustomAuthorize]
        public JsonResult ListPorIdCompleto(int id)
        {
            TransacaoInfo info = new TransacaoInfo();
            info = TransacaoBll.Instance.ListarPorIdCompleto(id);

            return Json(new JsonResult { Data = info }, JsonRequestBehavior.AllowGet);
        }
    }
}
