﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.UsuarioPerfil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.ExtensionMethod;

using System.Configuration;
using Newtonsoft.Json.Converters;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Bll.Corporativo;
using Newtonsoft.Json;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.Controllers
{
    public class UsuarioPerfilController : ControllerExtended
    {
        public UsuarioPerfilController()
        {
            IndexUrl = "/Corporativo/UsuarioPerfil/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {

            UsuarioPerfilInfo info = new UsuarioPerfilInfo();

            if (vm.IdUsuarioPerfil.HasValue && vm.IdUsuarioPerfil > 0)
                info = UsuarioPerfilBll.Instance.ListarPorCodigo(vm.IdUsuarioPerfil.Value);

            ViewModelToModelMapper.Map<UsuarioPerfilInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;


            var response = UsuarioPerfilBll.Instance.Salvar(info);

            SalvarHistoricoAcesso(IndexUrl, vm.IdUsuarioPerfil.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdUsuarioPerfil, "UsuarioPerfil", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                UsuarioPerfilInfo info = UsuarioPerfilBll.Instance.ListarCompletoPorId(id.Value);
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdUsuarioPerfil, "UsuarioPerfil");
            }

            return View(vm);
        }

        [CustomAuthorize, HttpPost]
        public JsonResult GetUsuarioPerfil()
        {
            var vm = new UsuarioPerfilVM("/Corporativo/UsuarioPerfil/UsuarioPerfil");

            var id = ControladorSessaoUsuario.SessaoCorrente.Pessoa.Usuario.IdUsuario;
            if (id.HasValue && id > 0)
            {
                var info = new UsuarioPerfilInfo();
                info.IdUsuario = id;

                var perfilUsuario = UsuarioPerfilBll.Instance.ListarPorIdCompleto(info);
                if (perfilUsuario.ArquivosDigitais[0] != null)
                    perfilUsuario.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);
                else
                    perfilUsuario.ArquivosDigitais = new List<ArquivoDigitalInfo>();

                ViewModelToModelMapper.MapBack<UsuarioPerfilVM>(vm, perfilUsuario);
                vm.SexoOpcao = perfilUsuario.Sexo.HasValue ? (perfilUsuario.Sexo.Value ? "true" : "false") : string.Empty;
            }

            return Json(new
            {
                Nome = vm.Nome,
                NomeSecundario = vm.NomeSecundario,
                Email = vm.Email,
                ArquivoDigital = vm.ArquivosDigitais.FirstOrDefault(),
                Telefone1 = vm.Telefone1,
                Telefone2 = vm.Telefone2,
                Sexo = vm.SexoOpcao
            });
        }


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UsuarioPerfilInfo info = new UsuarioPerfilInfo();

            ListPaged<UsuarioPerfilInfo> retorno = new ListPaged<UsuarioPerfilInfo>(UsuarioPerfilBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                    {
                        CurrentPage = vm.Page.CurrentPage.Value,
                        PageSize = vm.Page.PageSize.Value,
                        GroupBy = vm.Page.GroupBy,
                        OrderBy = vm.Page.OrderBy
                    }
            };

            return base.CreateListResult(vm, retorno, "Exportação:UsuarioPerfil", true);

        }

        [HttpPost, CustomAuthorize]
        public ActionResult UsuarioPerfilJson()
        {
            var vm = new UsuarioPerfilVM("/Corporativo/UsuarioPerfil/UsuarioPerfil");

            var id = ControladorSessaoUsuario.SessaoCorrente.Pessoa.Usuario.IdUsuario;
            if (id.HasValue && id > 0)
            {
                var info = new UsuarioPerfilInfo();
                info.IdUsuario = id;

                var perfilUsuario = UsuarioPerfilBll.Instance.ListarPorIdCompleto(info);
                
                if (perfilUsuario.ArquivosDigitais[0] != null)
                {
                    perfilUsuario.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);
                }

                else
                    perfilUsuario.ArquivosDigitais = new List<ArquivoDigitalInfo>();

                ViewModelToModelMapper.MapBack<UsuarioPerfilVM>(vm, perfilUsuario);
                vm.SexoOpcao = perfilUsuario.Sexo.HasValue ? (perfilUsuario.Sexo.Value ? "true" : "false") : string.Empty;
                if (perfilUsuario.DataNascimento.HasValue)
                    vm.DataNascimentoExibicao = perfilUsuario.DataNascimento.Value.ToShortDateString();
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult UsuarioPerfil()
        {
            var vm = new UsuarioPerfilVM("/Corporativo/UsuarioPerfil/UsuarioPerfil");
            return View(vm);
        }

        [HttpPost]
        public ActionResult Salvar(string json)
        {
            var vm = JsonConvert.DeserializeObject<UsuarioPerfilVM>(json, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy" });
            var info = new UsuarioPerfilInfo();

            ViewModelToModelMapper.Map<UsuarioPerfilInfo>(vm, info);

            info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderPost + "/" + x.Arquivo);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            if (!string.IsNullOrEmpty(vm.SexoOpcao))
                info.Sexo = vm.SexoOpcao == "true" ? true : false;

            if (!string.IsNullOrEmpty(vm.DataNascimentoExibicao))
                info.DataNascimento = Convert.ToDateTime(vm.DataNascimentoExibicao);

            var response = UsuarioPerfilBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            {
                ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil = info;
                if (ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.ArquivosDigitais != null)
                {
                    ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);
                }
                else
                {
                    ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.ArquivosDigitais = new List<ArquivoDigitalInfo>();
                }
            }

            return Json(new
            {
                Sucesso = response.Response.Sucesso,
                ArquivoDigital = response.ArquivosDigitais.Count > 0 ? response.ArquivosDigitais.FirstOrDefault().CaminhoArquivo : ""
            });
        }
    }
}
