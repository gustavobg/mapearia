﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo
{
    public class CorporativoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Corporativo";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Corporativo_default",
                "Corporativo/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
