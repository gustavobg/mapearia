﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoAtividade
{
    public class AtividadeSequenciaSituacaoVM : VMNewEditBase
    {
        public AtividadeSequenciaSituacaoVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdAprovacaoProcessoAtividadeSequenciaSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAprovacaoProcessoAtividadeSequencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUsuario { get; set; }

        [ViewModelToModelAttribute]
        public bool? Aprovado { get; set; }

        [ViewModelToModelAttribute]
        public string Parecer { get; set; }

    }
}