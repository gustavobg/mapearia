﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoAtividade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? Situacao { get; set; }

        public string IdSubProcessoIn { get; set; }
        public string IdDocumentoTipoIn { get; set; }
    }
}