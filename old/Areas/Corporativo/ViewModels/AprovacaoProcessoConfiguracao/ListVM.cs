﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoConfiguracao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }


        public int? IdAprovacaoProcessoConfiguracao { get; set; }
        public int? IdSubProcesso { get; set; }
    }
}