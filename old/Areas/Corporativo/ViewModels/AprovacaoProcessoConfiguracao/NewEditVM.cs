﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.AprovacaoProcessoConfiguracao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstEtapa = new List<EtapaVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdAprovacaoProcessoConfiguracao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdSubProcesso { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteComunicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool RealizaAprovacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EtapaVM> lstEtapa { get; set; }

        public class EtapaVM
        {
            [ViewModelToModelAttribute]
            public int? IdAprovacaoProcessoConfiguracaoEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAprovacaoProcessoConfiguracao { get; set; }

            [ViewModelToModelAttribute]
            public int? Etapa { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeAprovacaoNecessaria { get; set; }

            [ViewModelToModelAttribute]
            public int? TempoLimite { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeTempo { get; set; }

            [ViewModelToModelAttribute]
            public bool? FinalProcesso { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            #region Propriedades Especiais e Descrições

            [ViewModelToModelAttribute]
            public string IdsGrupoResponsabilidade { get; set; }

            [ViewModelToModelAttribute]
            public string IdsUsuario { get; set; }

            [ViewModelToModelAttribute]
            public string GrupoResponsabilidadeExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string UsuarioExibicao { get; set; }

            #endregion
        }
    }
}