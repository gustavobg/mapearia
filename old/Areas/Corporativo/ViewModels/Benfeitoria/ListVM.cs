﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Benfeitoria
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdBenfeitoria { get; set; }
        public int? IdBenfeitoriaTipo { get; set; }
        public int? IdCentroCusto { get; set; }

        public string Descricao { get; set; }
        public string IdBenfeitoriaTipoIn { get; set; }
        public string IdBenfeitoriaTipoNotIn { get; set; }
        public string BuscaAvancada { get; set; }

        public bool? SemItensRelacionadosComMapeamento { get; set; }
        public bool? Ativo { get; set; }
    }
}