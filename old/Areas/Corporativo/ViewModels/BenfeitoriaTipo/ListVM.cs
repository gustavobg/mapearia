﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.BenfeitoriaTipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdBenfeitoriaTipo { get; set; }
        public int? IdBenfeitoriaTipoPai { get; set; }

        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string IdBenfeitoriaTipoIn { get; set; }
        public string IdBenefeitoriaTipoNotIn { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
        public bool? UltimoNivel { get; set; }
    }
}