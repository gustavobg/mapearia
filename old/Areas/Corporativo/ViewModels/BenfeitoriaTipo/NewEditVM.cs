﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.BenfeitoriaTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdBenfeitoriaTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdBenfeitoriaTipoPai { get; set;}

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}