﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CNAE
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCnae { get; set; }

        public string CodigoDescricao { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public string Secao { get; set; }

        public bool? Ativo { get; set; }

    }
}