﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoEquipamentoNewEditVM : VMNewEditBase
    {
        public CategoriaServicoEquipamentoNewEditVM()
        {

        }

        #region Propriedades apenas para Exibição de Informações (CategoriaServico)

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoTipo { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoEquipamento { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoUtilizacaoEquipamento { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? DiretamenteOrdem { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? InformeServico { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoApontamentoOperador { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoRestricaoEquipamento { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoControleExecucao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAutomotivoTipoEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAutomotivoEquipamento { get; set; }
    }
}