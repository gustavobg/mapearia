﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoOrdemListVM : ViewModelListRequestBase
    {
        public CategoriaServicoOrdemListVM()
        {
            
        }

        public int? IdCategoriaServicoOrdem { get; set; }
        public int? IdCategoriaServico { get; set; }

    }
}