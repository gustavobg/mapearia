﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoOrdemNewEditVM : VMNewEditBase
    {
        public CategoriaServicoOrdemNewEditVM()
        {
            PermiteAtividadeEconomicaPeriodo = true;
        }

        #region Propriedades apenas para Exibição de Informações (CategoriaServico)

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoTipo { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoOrdem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteEmissaoOrdem { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoOrdem { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoDocumento { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteCompra { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoGeracaoCodigo { get; set; }

        [ViewModelToModelAttribute]
        public string Prefixo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoDescricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoDescricaoOrdem { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoPeriodoProgramacao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoControleExecucao { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteLancamentoForaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public bool PermitePlanejamento { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoResponsabilidade { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteAprovacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}