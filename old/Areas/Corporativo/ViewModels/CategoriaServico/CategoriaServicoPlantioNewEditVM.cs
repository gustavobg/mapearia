﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoPlantioNewEditVM : VMNewEditBase
    {
        public CategoriaServicoPlantioNewEditVM()
        {

        }

        #region Propriedades apenas para Exibição de Informações (CategoriaServico)

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoTipo { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoPlantio { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomica { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoApontamentoPlantio { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoDocumento { get; set; }
        
        [ViewModelToModelAttribute]
        public bool OrdemObrigatoria { get; set; }
        
        [ViewModelToModelAttribute]
        public int? TipoControleExecucao { get; set; }
        
        //[ViewModelToModelAttribute]
        //public bool PermiteInformeTerminoPlanio { get; set; }
        
        [ViewModelToModelAttribute]
        public bool PermiteInformeEstimativaColheita { get; set; }

        [ViewModelToModelAttribute]
        public int? GeracaoCodigoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string PrefixoCodigo { get; set; }

        [ViewModelToModelAttribute]
        public bool? EspacamentoPermitido { get; set; }
        
        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoCultura { get; set; }
    }
}