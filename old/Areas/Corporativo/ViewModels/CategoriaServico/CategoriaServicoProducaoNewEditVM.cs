﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoProducaoNewEditVM : VMNewEditBase
    {
        public CategoriaServicoProducaoNewEditVM()
        {

        }

        #region Propriedades apenas para Exibição de Informações (CategoriaServico)

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoTipo { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoProducao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public int IdAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public int IdCategoriaServicoDestino { get; set; }

        [ViewModelToModelAttribute]
        public int TipoApontamentoProducao { get; set; }

        [ViewModelToModelAttribute]
        public int TipoDocumento { get; set; }

        [ViewModelToModelAttribute]
        public bool OrdemObrigatoria { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoControleExecucao { get; set; }

        //[ViewModelToModelAttribute]
        //public bool PeriodoAtividadeEconomicaProducao { get; set; }

        //[ViewModelToModelAttribute]
        //public bool PeriodoAtividadeEconomicaEstimativa { get; set; }

        [ViewModelToModelAttribute]
        public bool ColheitaSemPlantio { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteInformeTerminoProducao { get; set; }

        [ViewModelToModelAttribute]
        public bool ProcessamentoDireto { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteEstimativaProducaoProximaColheita { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServico { get; set; }
    }
}