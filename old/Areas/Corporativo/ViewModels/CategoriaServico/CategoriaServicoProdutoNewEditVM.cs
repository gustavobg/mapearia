﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class CategoriaServicoProdutoNewEditVM : VMNewEditBase
    {
        public CategoriaServicoProdutoNewEditVM() { }

        #region Propriedades apenas para Exibição de Informações (CategoriaServico)

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoTipo { get; set; }
        #endregion

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoProduto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoUtilizacaoProduto { get; set; }

        [ViewModelToModelAttribute]
        public bool DiretamenteOrdem { get; set; }

        [ViewModelToModelAttribute]
        public bool InformeServico { get; set; }

        [ViewModelToModelAttribute]
        public bool ReservaProduto { get; set; }

        [ViewModelToModelAttribute]
        public bool SolicitacaoAutomatica { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoRestricaoProduto { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoGrupo { get; set; }
    }
}