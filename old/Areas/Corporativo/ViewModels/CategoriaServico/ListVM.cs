﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCategoriaServico { get; set; }
        public int? TipoAplicacaoTipo { get; set; }
        public int? IdAtividadeEconomicaEtapa { get; set; }
        public int? IdProdutoServico { get; set; }

        public string Descricao { get; set; }
        public string Codigo { get; set; }
        public string CodigoDescricao { get; set; }
        public string IdUsuarioNotIn { get; set; }
        public string IdEmpresaUsuariaNotIn { get; set; }
        public string DescricaoProdutoServico { get; set; }
        public string DescricaoAtividadeEconomicaEtapa { get; set; }

        public bool? Ativo { get; set; }

    }
}