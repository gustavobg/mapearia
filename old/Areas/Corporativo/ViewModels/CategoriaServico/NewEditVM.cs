﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.CategoriaServico
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            EtapasServico = new List<EtapaServicoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNaturezaOperacional { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteApontamentoMovel { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteInformeServico { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoCodigo { get; set; }

        [ViewModelToModelAttribute]
        public string Prefixo { get; set; }

        [ViewModelToModelAttribute]
        public int TipoAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoControleSubAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteControleEtapa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteMultiplasEtapas { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteControleTempoPorEtapa { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoRestricaoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoDestinatario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteControleTempoPorServico { get; set; }

        [ViewModelToModelAttribute]
        public int TipoApropiacaoCusto { get; set; }

        [ViewModelToModelAttribute]
        public bool PlanejamentoObrigatorio { get; set; }

        [ViewModelToModelAttribute]
        public bool TerminoInforme { get; set; }

        [ViewModelToModelAttribute]
        public bool MultiAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoLocalTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAutomotivoTipoEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAutomotivoParteEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCriacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCriacaoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public string IdsBenfeitoriaTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEtapas { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoGrupo { get; set; }

        //[ViewModelToModelAttribute]
        //public string IdsProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEmpresaEmissora { get; set; }

        [ViewModelToModelAttribute]
        public string IdsUsuarioRestritos { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeAplicacaoSubTipo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EtapaServicoVM> EtapasServico { get; set; }

        public class EtapaServicoVM
        {
            [ViewModelToModelAttribute]
            public int? IdCategoriaServicoEtapaServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCategoriaServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoAtividadeEconomicaEtapa { get; set; }
        }

    }
}