using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Chat
{
    public class ChatNewEditVM
    {
        public ChatNewEditVM() { }

		        [ViewModelToModelAttribute]
		public Int32? IdChat { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdPessoaDe { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdPessoaPara { get; set; }

        [ViewModelToModelAttribute]
		public Int32 IdChatGrupoPara { get; set; }

        [ViewModelToModelAttribute]
		public String Mensagem { get; set; }

        [ViewModelToModelAttribute]
		public DateTime DataEnvio { get; set; }

        [ViewModelToModelAttribute]
		public DateTime DataLeitura { get; set; }


    }
}