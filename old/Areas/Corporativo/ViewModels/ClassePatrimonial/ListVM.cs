﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ClassePatrimonial
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdClassePatrimonial { get; set; }
        public int? IdClassePatrimonialPai { get; set; }

        public string IdClassePatrimonialIn { get; set; }
        public string IdClassePatrimonialNotIn { get; set; }
        public string IdsClassePatrimonialInBuscaProfundidade { get; set; }
        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string Codigo { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? Ativo { get; set; }
    }
}