using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Empresa
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public int? IdEmpresa { get; set; }

        public string IdGrupoMenuIn { get; set; }
        public string IdEmpresaGrupoIn { get; set; }
        public string IdEmpresaNotIn { get; set; }
        public string Nome { get; set; }

        public bool? EmpresaModelo { get; set; }
        public bool? Ativo { get; set; }
    }
}