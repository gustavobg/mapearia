using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Empresa
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

            EmpresaRemetente = new EmpresaRemetenteVM();
            EmpresaRemetente.Ativo = true;
            Modelos = new List<EmpresaModeloConteudoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public bool? EmpresaModelo { get; set; }

        [ViewModelToModelAttribute]
        public string UrlSubDominio { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public int IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresaGrupo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public EmpresaRemetenteVM EmpresaRemetente { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EmpresaModeloConteudoVM> Modelos { get; set; }

        public class EmpresaRemetenteVM
        {
            [ViewModelToModelAttribute]
            public int? IdEmpresaRemetente { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomeApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string Email { get; set; }

            [ViewModelToModelAttribute]
            public string EmailResposta { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone { get; set; }

            [ViewModelToModelAttribute]
            public string TelefoneResposta { get; set; }

            [ViewModelToModelAttribute]
            public string Usuario { get; set; }

            [ViewModelToModelAttribute]
            public string Senha { get; set; }

            [ViewModelToModelAttribute]
            public string SMTP { get; set; }

            [ViewModelToModelAttribute]
            public int? Porta { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }
        }

        public class EmpresaModeloConteudoVM
        {
            public EmpresaModeloConteudoVM()
            {
                PermiteCadastroIndependente = true;
            }

            [ViewModelToModelAttribute]
            public int? IdEmpresaModeloConteudo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTela { get; set; }

            [ViewModelToModelAttribute]
            public int? IdModulo { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteCadastroIndependente { get; set; }

            #region Propriedades Especiais e Descrições

            [ViewModelToModelAttribute]
            public string Modelo { get; set; }

            [ViewModelToModelAttribute]
            public string IdsEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public string ModuloDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string ModuloDescricaoPai { get; set; }

            [ViewModelToModelAttribute]
            public string TelaDescricao { get; set; }

            [ViewModelToModelAttribute]
            public bool EditarRegistro { get; set; }

            #endregion
        }
    }
}