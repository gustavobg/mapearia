﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaGrupo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        [ViewModelToModelAttribute]
        public int? IdEmpresaGrupo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
        public string IdGrupoMenuIn { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoGrupoMenu { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

    }
}