﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Collections.Generic;

using System;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaGrupo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            EmpresaRemetente = new EmpresaRemetenteVM();
            Conteudos = new List<ModeloConteudo>();
        }

        [ViewModelToModelAttribute]
        public int? IdEmpresaGrupo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresaReferencia { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public EmpresaRemetenteVM EmpresaRemetente { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ModeloConteudo> Conteudos { get; set; }

        public class EmpresaRemetenteVM
        {
            public EmpresaRemetenteVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdEmpresaRemetente { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAplicativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomeApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string Email { get; set; }

            [ViewModelToModelAttribute]
            public string EmailResposta { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone { get; set; }

            [ViewModelToModelAttribute]
            public string TelefoneResposta { get; set; }

            [ViewModelToModelAttribute]
            public string Usuario { get; set; }

            [ViewModelToModelAttribute]
            public string Senha { get; set; }

            [ViewModelToModelAttribute]
            public string SMTP { get; set; }

            [ViewModelToModelAttribute]
            public int? Porta { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }
        }

        public class ModeloConteudo
        {
            public ModeloConteudo()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdEmpresaGrupoModeloConteudo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresaGrupo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTela { get; set; }

            [ViewModelToModelAttribute]
            public int? IdModulo { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteCadastroIndependente { get; set; }

            //Propriedades Especiais
            [ViewModelToModelAttribute]
            public string ModuloDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string TelaDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string IdsEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public string Modelo { get; set; }

        }
    }
}