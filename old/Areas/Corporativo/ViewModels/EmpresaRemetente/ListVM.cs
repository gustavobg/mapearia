﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaRemetente
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEmpresa { get; set; }

        public int? IdEmpresaRemetente { get; set; }

        public string IdEmpresaRemetenteIn { get; set; }

        public string IdEmpresaIn { get; set; }

        public bool? ComGrupoEmpresa { get; set; }

    }
}