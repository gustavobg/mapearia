﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaRemetente
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresaRemetente { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdAplicativo { get; set; }

        [ViewModelToModelAttribute]
        public String NomeApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public String Email { get; set; }

        [ViewModelToModelAttribute]
        public String EmailResposta { get; set; }

        [ViewModelToModelAttribute]
        public String Telefone { get; set; }

        [ViewModelToModelAttribute]
        public String TelefoneResposta { get; set; }

        [ViewModelToModelAttribute]
        public String Usuario { get; set; }

        [ViewModelToModelAttribute]
        public String Senha { get; set; }

        [ViewModelToModelAttribute]
        public String SMTP { get; set; }

        [ViewModelToModelAttribute]
        public Int32 Porta { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}