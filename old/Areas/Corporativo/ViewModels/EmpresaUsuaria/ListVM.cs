﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaUsuaria
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdPessoa { get; set; }
        public int? IdPessoaNatureza { get; set; }
        
        public string Nome { get; set; }
        public string Nomes { get; set; }
        public string NomeSecundario { get; set; }
        public string IdPessoaIn { get; set; }
        public string IdPessoaNotIn { get; set; }
        public string Codigo { get; set; }
        public string BuscaAvancada  { get; set; }   
             
        public bool? Empresa { get; set; }
        public bool? PossuiUsuario { get; set; }
        public bool? Ativo { get; set; }

        public bool? ComRestricaoUsuarioLogado { get; set; }
    }
}