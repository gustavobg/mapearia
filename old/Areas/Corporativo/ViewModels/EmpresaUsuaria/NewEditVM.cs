﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.EmpresaUsuaria
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {          
            Contatos = new List<ContatoVM>();
            Enderecos = new List<EnderecoVM>();

            PessoaEmpresa = new PessoaEmpresaVM();
            PessoaEmpresa.Ativo = true;
            PessoaEmpresa.Matriz = false;
        }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaPai { get; set; }

        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string NomeSecundario { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string Documento1 { get; set; }

        [ViewModelToModelAttribute]
        public int IdPessoaNatureza { get; set; }

        [ViewModelToModelAttribute]
        public bool? Corporacao { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone1 { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone2 { get; set; }

        [ViewModelToModelAttribute]
        public string Email { get; set; }

        [ViewModelToModelAttribute]
        public string Site { get; set; }

        [ViewModelToModelAttribute]
        public string Documento2 { get; set; }

        [ViewModelToModelAttribute]
        public string Documento3 { get; set; }

        [ViewModelToModelAttribute]
        public String Codigo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? Data { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public PessoaEmpresaVM PessoaEmpresa { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ContatoVM> Contatos { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EnderecoVM> Enderecos { get; set; }        

        public class PessoaEmpresaVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public int? IdClasseContabil { get; set; }

            [ViewModelToModelAttribute]
            public bool Ativo { get; set; }

            [ViewModelToModelAttribute]
            public bool Matriz { get; set; }
        }

        public class ContatoVM
        {
            [ViewModelToModelAttribute]
            public int? IdPessoaContato { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }


            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone1 { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone2 { get; set; }

            [ViewModelToModelAttribute]
            public string Email { get; set; }


            [ViewModelToModelAttribute]
            public bool Ativo { get; set; }

        }

        public class EnderecoVM
        {
            [ViewModelToModelAttribute]
            public int? IdPessoaEndereco { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoaEnderecoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPais { get; set; }

            [ViewModelToModelAttribute]
            public int? IdLogradouro { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTipoLogradouro { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstado { get; set; }

            [ViewModelToModelAttribute]
            public int? IdLocalidade { get; set; }

            [ViewModelToModelAttribute]
            public int? IdBairro { get; set; }

            [ViewModelToModelAttribute]
            public string CEP { get; set; }

            [ViewModelToModelAttribute]
            public string Localidade { get; set; }

            [ViewModelToModelAttribute]
            public string Logradouro { get; set; }

            [ViewModelToModelAttribute]
            public string Bairro { get; set; }

            [ViewModelToModelAttribute]
            public string Numero { get; set; }

            [ViewModelToModelAttribute]
            public string Complemento { get; set; }

            [ViewModelToModelAttribute]
            public string Referencia { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string Sigla { get; set; }
        }
    }
}