﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ModeloConteudo
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }

        public int? IdEmpresa { get; set; }
        public string ParentId { get; set; }

        public string DescricaoExibicao { get; set; }

    }
}