﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ModeloConteudo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdEmpresaModeloConteudo { get; set; }
        public int? IdTela { get; set; }
        public int? IdModulo { get;set;}

        public bool? PermiteCadastroIndependente { get; set; }

    }
}