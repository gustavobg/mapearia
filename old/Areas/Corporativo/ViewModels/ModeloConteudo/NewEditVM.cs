﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ModeloConteudo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresaModeloConteudo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTela { get; set; }

        [ViewModelToModelAttribute]
        public int? IdModulo { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteCadastroIndependente { get; set; }

        #region Propriedades Especiais e Descrições

        [ViewModelToModelAttribute]
        public string Modelo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string ModuloDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string ModuloDescricaoPai { get; set; }

        [ViewModelToModelAttribute]
        public string TelaDescricao { get; set; }

        [ViewModelToModelAttribute]
        public bool EditarRegistro { get; set; }

        #endregion

    }
}