﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.NaturezaOperacional
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdNaturezaOperacional { get; set; }

        public string IdNaturezaOperacionalIn { get; set; }
        public string IdNaturezaOperacionalNotIn { get; set; }
        public string Descricao { get; set; }

        public bool? VinculadasAoPlanoTecnico { get; set; }
        public bool? Ativo { get; set; }
    }
}