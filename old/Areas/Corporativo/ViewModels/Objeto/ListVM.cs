﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Objeto
{
    public class ListVM : ViewModelListRequestBase
    {

        public int? IdObjeto { get; set; }
        public int? IdEmpresa { get; set; }

        public int? IdOrdem { get; set; }

        public int? IdCentroCusto { get; set; }

        public string DescricaoBusca { get; set; }

        #region Aplicação

        public int? IdAutomotivoEquipamento { get; set; }
        public int? IdAplicacao { get; set; }

        public int? IdBenfeitoria { get; set; }

        public int? IdProducaoLocal { get; set; }

        public int? IdCriacaoLote { get; set; }

        public int? IdCriacaoAnimal { get; set; }
        #endregion


    }
}