﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Ordem
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdOrdem { get; set; }
        public int? IdAtividadeEconomicaPeriodo { get; set; }

        public string IdCategoriaServicoIn { get; set; }
        public string IdNaturezaOperacionalIn { get; set; }
        public string IdAplicacaoReferenciaIn { get; set; }
        public string IdSubAplicacaoReferenciaIn { get; set; }

        public string Descricao { get; set; }
        public string DescricaoBusca { get; set; }

        public string StrDataInicioEstimativa { get; set; }
        public string StrDataTerminoEstimativa { get; set; }
        public string StrDataInicioRealizado { get; set; }
        public string StrDataTerminoRealizado { get; set; }

        public bool? Ativo { get; set; }
    }
}