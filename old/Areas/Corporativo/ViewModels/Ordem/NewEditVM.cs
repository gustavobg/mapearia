﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Ordem
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Etapas = new List<OrdemEtapaVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdOrdem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmitente { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaDestinatario { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInicioEstimativa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataTerminoEstimativa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInicioRealizado { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataTerminoRealizado { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<OrdemEtapaVM> Etapas { get; set; }

        public class OrdemEtapaVM
        {

            [ViewModelToModelAttribute]
            public int? IdOrdemEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdOrdem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? Sequencia { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoAtividadeEconomicaEtapa { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }
        }


        #region Ids

        [ViewModelToModelAttribute]
        public string IdsPessoaResponsavel { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsSubAplicacao { get; set; }

        //[ViewModelToModelAttribute]
        //public string IdsProducaoLocal { get; set; }

        //[ViewModelToModelAttribute]
        //public string IdsProducaoUnidade { get; set; }

        #endregion

        #region Descrições Gerais

        [ViewModelToModelAttribute]
        public string DescricaoFormaApropriacaoCusto { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioEstimadaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataTerminoEstimadaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioRealizadoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataTerminoRealizadoExibicao { get; set; }

        #endregion

        #region Propriedades Especiais e/ou exclusivas para Consulta

        [ViewModelToModelAttribute]
        public int? IdUsuarioLogado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresaUsuariaLogada { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServicoOrdem { get; set; }

        #endregion


    }
}