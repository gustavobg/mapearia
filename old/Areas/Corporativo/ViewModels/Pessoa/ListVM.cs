using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
        {
        }

        public int? IdPessoa { get; set; }
        public int? IdPessoaNatureza { get; set; }
        public int? IdUsuario { get; set; }

        public string IdPessoaNotIn { get; set; }
        public string IdPessoaIn { get; set; }
        
        public string NomeRazao { get; set; }
        public string ApelidoFantasia { get; set; }
        public string NomeDocumento { get; set; }

        public string Nomes { get; set; }
        public string DocumentoTelefone { get; set; }
        public string IdPessoaPerfilIn { get; set; }
        public string IdPessoaNaturezaIn { get; set; }
        public string IdPessoaNaturezaNotIn { get; set; }

        public bool? Empresa { get; set; }
        public bool? PossuiUsuario { get; set; }
        public bool? Corporacao { get; set; }

        public bool? Ativo { get; set; }

        public bool? ComEmpresa { get; set; }
        public bool? ComEmpresaUsuaria { get; set; }
        public bool? ComPessoa { get; set; }

        public bool? PessoaPerfil_PermiteRealizarCompra { get; set; }
    }
}