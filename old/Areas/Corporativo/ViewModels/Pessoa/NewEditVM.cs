using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Usuario = new UsuarioVM();
            Contatos = new List<ContatoVM>();
            Enderecos = new List<EnderecoVM>();
            Caracteristicas = new List<CaracteristicasVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaPai { get; set; }

        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string NomeSecundario { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string Documento1 { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaNatureza { get; set; }

        [ViewModelToModelAttribute]
        public bool? Corporacao { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone1 { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone2 { get; set; }

        [ViewModelToModelAttribute]
        public string Email { get; set; }

        [ViewModelToModelAttribute]
        public string Site { get; set; }

        [ViewModelToModelAttribute]
        public string Documento2 { get; set; }

        [ViewModelToModelAttribute]
        public string Documento3 { get; set; }

        [ViewModelToModelAttribute]
        public bool IsentoIE { get; set; }

        [ViewModelToModelAttribute]
        public bool PossuiSimplesNacional { get; set; }

        [ViewModelToModelAttribute]
        public string PalavraChave { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? Data { get; set; }
        
        [ViewModelToModelAttribute]
        public string DataExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCnae { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public UsuarioVM Usuario { get; set; }

        public string IdsPerfil { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ContatoVM> Contatos { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<EnderecoVM> Enderecos { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CaracteristicasVM> Caracteristicas { get; set; }

        public class UsuarioVM
        {
            public UsuarioVM()
            {
                UsuarioNuncaAcessou = true;
            }

            [ViewModelToModelAttribute]
            public int? IdUsuario { get; set; }

            [ViewModelToModelAttribute]
            public string Login { get; set; }

            [ViewModelToModelAttribute]
            public string Email { get; set; }

            [ViewModelToModelAttribute]
            public bool Ativo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdGrupoUsuario { get; set; }

            [ViewModelToModelAttribute]
            public bool UsuarioNuncaAcessou { get; set; }

            [ViewModelToModelAttribute]
            public bool BotaoAtivacao { get; set; }
        }

        public class ContatoVM
        {

            [ViewModelToModelAttribute]
            public int? IdPessoaContato { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone1 { get; set; }

            [ViewModelToModelAttribute]
            public string Telefone2 { get; set; }

            [ViewModelToModelAttribute]
            public string Email { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

        }

        public class EnderecoVM
        {

            public string getLogradouroNumeroComplemento()
            {
                string str = (Logradouro.Length > 0 && (Numero.Length > 0 || Complemento.Length > 0)) ? Logradouro + ", " : "";
                str += (Numero.Length > 0) ? Numero : "";
                str += ((Complemento != null && Complemento.Length > 0)) ? " - " + Complemento : "";
                return str;
            }

            [ViewModelToModelAttribute]
            public int? IdPessoaEndereco { get; set; }

            [ViewModelToModelAttribute]
            public int IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPais { get; set; }

            [ViewModelToModelAttribute]
            public int? IdLogradouro { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTipoLogradouro { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstado { get; set; }

            [ViewModelToModelAttribute]
            public int? IdLocalidade { get; set; }

            [ViewModelToModelAttribute]
            public int? IdBairro { get; set; }

            [ViewModelToModelAttribute]
            public string CEP { get; set; }

            [ViewModelToModelAttribute]
            public string Localidade { get; set; }

            [ViewModelToModelAttribute]
            public string Logradouro { get; set; }

            [ViewModelToModelAttribute]
            public string Bairro { get; set; }

            [ViewModelToModelAttribute]
            public string Numero { get; set; }

            [ViewModelToModelAttribute]
            public string Complemento { get; set; }

            [ViewModelToModelAttribute]
            public string LogradouroNumeroComplemento { get; set; }

            [ViewModelToModelAttribute]
            public string Referencia { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public string Sigla { get; set; }

            [ViewModelToModelAttribute]
            public string[] IdPessoaEnderecoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string IdsPessoaEnderecoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoPessoaEnderecoTipo { get; set; }
        }

        public class CaracteristicasVM
        {
            [ViewModelToModelAttribute]
            public int? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaTabela { get; set; }

            [ViewModelToModelAttribute]
            public string Codigo { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public CaracteristicaValorVM CaracteristicaValor { get; set; }
        }

        public class CaracteristicaValorVM
        {
            [ViewModelToModelAttribute]
            public int? IdCaracteristicaValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristica { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaTabela { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaTabelaResgisto { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCaracteristicaOpcao { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }
        }
    }
}


