﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa
{
    public class NewVM : NewEditVM
    {
        public NewVM()
        {
            //PessoaEndereco = new PessoaEnderecoVM();
        }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string NomeSecundario { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string Documento1 { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaNatureza { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone1 { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone2 { get; set; }

        [ViewModelToModelAttribute]
        public string Documento2 { get; set; }

        [ViewModelToModelAttribute]
        public string Documento3 { get; set; }

        [ViewModelToModelAttribute]
        public string PalavraChave { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        public string IdsPerfil { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        //Propriedades Pessoa //Novas (Implementadas dia 05/04/2016) Solicitação Humberto
        [ViewModelToModelAttribute]
        public bool? PossuiSimplesNacional { get; set; }

        [ViewModelToModelAttribute]
        public bool? IsentoIE { get; set; }

        [ViewModelToModelAttribute]
        public string DataExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCnae { get; set; }

        //Endereço Pessoa
        [ViewModelToModelAttribute]
        public int? IdPessoaEndereco { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaEnderecoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string CEP { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTipoLogradouro { get; set; }

        [ViewModelToModelAttribute]
        public string Logradouro { get; set; }

        [ViewModelToModelAttribute]
        public int? Numero { get; set; }

        [ViewModelToModelAttribute]
        public string Complemento { get; set; }

        [ViewModelToModelAttribute]
        public string Bairro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEstado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdLocalidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPais { get; set; }

        [ViewModelToModelAttribute]
        public string Referencia { get; set; }
    }
}