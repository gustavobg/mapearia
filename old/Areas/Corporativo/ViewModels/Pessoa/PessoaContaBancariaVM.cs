﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa
{
    public class PessoaContaBancariaVM : VMNewEditBase
    {
        public PessoaContaBancariaVM()
        {
            Contas = new List<DadosContaBancariaVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string NomeSecundario { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<DadosContaBancariaVM> Contas { get; set; }

        public class DadosContaBancariaVM
        {
            [ViewModelToModelAttribute]
            public int? IdPessoaContaBancaria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroContaTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroEntidade { get; set; }

            [ViewModelToModelAttribute]
            public string Agencia { get; set; }

            [ViewModelToModelAttribute]
            public string DigitoVerificadorAgencia { get; set; }

            [ViewModelToModelAttribute]
            public string Conta { get; set; }

            [ViewModelToModelAttribute]
            public string DigitoVerificadorConta { get; set; }

            [ViewModelToModelAttribute]
            public bool Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFinanceiroEntidade { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFinanceiroContaTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DetalhesConta { get; set; }
        }
    }
}