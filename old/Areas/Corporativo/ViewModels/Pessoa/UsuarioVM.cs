﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa
{
    public class UsuarioVM : VMNewEditBase
    {
        public UsuarioVM()
        {
            UsuarioNuncaAcessou = true;
        }

        public string NomeExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUsuario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string Login { get; set; }

        [ViewModelToModelAttribute]
        public string Email { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdGrupoUsuario { get; set; }

        [ViewModelToModelAttribute]
        public bool UsuarioNuncaAcessou { get; set; }

        [ViewModelToModelAttribute]
        public bool BotaoAtivacao { get; set; }

        #region Propriedades Específicas para o Cadastramento de Restrições e Atribuição de Grupo de Responsabilidade

        [ViewModelToModelAttribute]
        public string IdsPlanoContas { get; set; }

        [ViewModelToModelAttribute]
        public int? IdVersaoPlanoContasPlanoContas { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdVersaoPlanoContasCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFinanceiroClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdVersaoPlanoContasClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoLocal { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string IdsGrupoResponsabilidade { get; set; }

        #endregion

    }
}