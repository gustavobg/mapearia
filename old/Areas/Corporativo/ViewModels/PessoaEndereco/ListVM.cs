﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Endereco
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdPessoaEndereco { get; set; }
        public int? IdLocalidade { get; set; }
        public int? IdPessoa { get; set; }

        public string Localidade { get; set; }
        public string LocalidadeEstado { get; set; }

        public bool? Ativo { get; set; }
    }
}