﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Endereco
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdPessoaEndereco { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdPais { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdTipoEndereco { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdLogradouro { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdTipoLogradouro { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdEstado { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdLocalidade { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdBairro { get; set; }

        [ViewModelToModelAttribute]
        public String CEP { get; set; }

        [ViewModelToModelAttribute]
        public Int32 Logradouro { get; set; }

        [ViewModelToModelAttribute]
        public String DescricaoLogradouro { get; set; }

        [ViewModelToModelAttribute]
        public String DescricaoBairro { get; set; }

        [ViewModelToModelAttribute]
        public String Numero { get; set; }

        [ViewModelToModelAttribute]
        public String Complemento { get; set; }

        [ViewModelToModelAttribute]
        public String Referencia { get; set; }


    }
}