﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.PessoaPerfil
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public string Descricao { get; set; }
        public string IdPessoaPerfilIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? PermiteRealizarCompra { get; set; }
        public bool? PossuiConceitoColaboradorFuncionario { get; set; }
        public bool? PermiteUsuarioSistema { get; set; }
        public bool? PermiteRealizarVenda { get; set; }
        public bool? PossuiConceitoTransportadora { get; set; }
        public bool? CNPJ { get; set; }
        public bool? DataAbertura { get; set; }
        public bool? EnderecoSede { get; set; }
        public bool? IE { get; set; }
        public bool? IM { get; set; }
        public bool? Telefone1PJ { get; set; }
        public bool? Telefone2PJ { get; set; }
        public bool? EmailPJ { get; set; }
        public bool? CPF { get; set; }
        public bool? RG { get; set; }
        public bool? CNH { get; set; }
        public bool? DataNascimento { get; set; }
        public bool? Telefone1PF { get; set; }
        public bool? Telefone2PF { get; set; }
        public bool? EmailPF { get; set; }
        public bool? EnderecoDomicilio { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}