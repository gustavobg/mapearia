﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.PessoaPerfil
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Perfis = new List<PerfilsVM>();
        }

        [ViewModelToModelAttribute]
        public Int32? IdPessoaPerfil { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdPessoaPerfilPai { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? PermiteRealizarCompra { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? PossuiConceitoColaboradorFuncionario { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? PermiteUsuarioSistema { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? PermiteRealizarVenda { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? PossuiConceitoTransportadora { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? CNPJ { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? DataAbertura { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? EnderecoSede { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? IE { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? IM { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Telefone1PJ { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Telefone2PJ { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? EmailPJ { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? CPF { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? RG { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? CNH { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? DataNascimento { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Telefone1PF { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Telefone2PF { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? EmailPF { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? EnderecoDomicilio { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<PerfilsVM> Perfis { get; set; }

        public class PerfilsVM
        {
            [ViewModelToModelAttribute]
            public Int32? IdPessoaPerfil { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public Int32? IdPessoaPerfilPai { get; set; }


            [ViewModelToModelAttribute]
            public Boolean? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }
        }




    }
}