﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ProcessoSituacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }


        public int? IdProcessoSituacao { get; set; }
        public int? IdProcesso { get; set; }

        public string Descricao { get; set; }
        public string IdProcessoSituacaoIn { get; set; }

        public bool? Visivel { get; set; }
        public bool? Finalizado { get; set; }
        public bool? Ativo { get; set; }
    }
}