﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.ProcessoSituacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public string DescricaoProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcesso { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Visivel { get; set; }

        [ViewModelToModelAttribute]
        public bool? Finalizado { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }
    }
}