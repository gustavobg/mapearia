﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.SubProcesso
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProcesso { get; set; }
        public int? IdSubProcesso { get; set; }
        public int? IdDocumentoTipo { get; set; }
        public int? IdAprovacaoProcessoConfiguracao { get; set; } //Utilizado Apenas para a ActionResult ListAprovacaoProcessoConfiguracao

        public string Descricao { get; set; }
        public string IdDocumentoTipoIn { get; set; }

        public bool? PermiteTransacao { get; set; }
        public bool? PermiteAprovacao { get; set; }
        public bool? RealizaAprovacao { get; set; }
        public bool? Ativo { get; set; }
    }
}