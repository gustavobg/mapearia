﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.SubProcesso
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstAcao = new List<SubProcessoAcaoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdSubProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? Ordem { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteTransacao { get; set; }

        [ViewModelToModelAttribute]
        public bool RepresentaDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteAprovacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsTransacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsTela { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<SubProcessoAcaoVM> lstAcao { get; set; }

        public class SubProcessoAcaoVM
        {   

            [ViewModelToModelAttribute]
            public int? IdSubProcessoAcao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdSubProcesso { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoEvento { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoEventoSistema { get; set; }

            [ViewModelToModelAttribute]
            public bool? InicioProcesso { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProcessoSituacaoDestino { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoPermissaoComentario { get; set; }

            [ViewModelToModelAttribute]
            public bool? AlteraDocumentoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteAlterarSituacao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string IdsProcessoSituacao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProcessoSituacaoDestino { get; set; }
        }


    }
}