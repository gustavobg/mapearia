﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.SubProcessoAcao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdSubProcesso { get; set; }
        public int? IdProcessoSituacao { get; set; }

        public string DescricaoProcessoSituacao { get; set; }
        public string DescricaoSubProcesso { get; set; }

        public bool? InicioProcesso { get; set; }
        public bool? Ativo { get; set; }
    }
}