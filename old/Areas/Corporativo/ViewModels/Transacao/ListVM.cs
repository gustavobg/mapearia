﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Transacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdTransacao { get; set; }
        public int? IdSubProcesso { get; set; }
        public int? IdDocumentoTipo { get; set; }
        public int? IdProcesso { get; set; }

        public string Descricao { get; set; }
        public string DescricaoDocumentoTipo { get; set; }
        public string DescricaoTransacao { get; set; }

        public string IdTransacaoIn { get; set; }
        public string IdTransacaoNotIn { get; set; }
        public string MovimentacaoFinanceiroTipoIn { get; set; }
        public string IdDocumentoTipoIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RepresentaAntecipacao { get; set; }

    }
}