﻿using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Transacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstTransacaoSimultanea = new List<TransacaoSimultaneVM>();
            lstFiscalCFOP = new List<TransacaoFiscalCFOPVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoResumida { get; set; }

        [ViewModelToModelAttribute]
        public bool RealizadaViaSistema { get; set; }

        [ViewModelToModelAttribute]
        public bool VendaPatrimonio { get; set; }

        [ViewModelToModelAttribute]
        public int? MovimentacaoEstoqueTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? CaracteristicaGeracaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public MovimentacaoFinanceiroTipoEnum? MovimentacaoFinanceiroTipo { get; set; }

        [ViewModelToModelAttribute]
        public MovimentacaoFinanceiroObrigacaoTipoEnum? MovimentacaoFinanceiroObrigacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool RepresentaAntecipacao { get; set; } 

        [ViewModelToModelAttribute]
        public bool GeraMovimentoBancario { get; set; }

        [ViewModelToModelAttribute]
        public bool UtilizadoComoAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public int? ControleOperacaoTerceiroTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? TempoLimite { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeTempoLimite { get; set; }

        [ViewModelToModelAttribute]
        public int? MovimentacaoContabilTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool ParametrizacaoContabilPorPrazo { get; set; }

        [ViewModelToModelAttribute]
        public bool ExigeDocumentoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacaoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacaoSeguinte { get; set; }

        [ViewModelToModelAttribute]
        public int? DirecionadorFinanceiroTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? DirecionadorEstoqueTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? DirecionadorTerceirosTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? DirecionadorFiscalTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool RealizaMovimentoFiscal { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdSubProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? HistoricoTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool? ExclusivoItensCompraVenda { get; set; }

        [ViewModelToModelAttribute]
        public bool? GeraMovimentoContabil { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsDocumentoTipo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<TransacaoSimultaneVM> lstTransacaoSimultanea { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<TransacaoFiscalCFOPVM> lstFiscalCFOP { get; set; }

        public class TransacaoSimultaneVM
        {
            [ViewModelToModelAttribute]
            public int? IdTransacaoSimultanea { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTransacao { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public int? MoedaPadrao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Obrigatorio { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoTransacao { get; set; }

            [ViewModelToModelAttribute]
            public string NaturezaDescricao { get; set; }
        }
        public class TransacaoFiscalCFOPVM
        {
            [ViewModelToModelAttribute]
            public int? IdTransacaoFiscalCFOP { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTransacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFiscalCFOP { get; set; }

            [ViewModelToModelAttribute]
            public int? UtilizacaoPrevistaTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoOrigemFiscalCFOP { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFiscalCFOP { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUtilizacaoPrevistaTipo { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }
        }

    }
}