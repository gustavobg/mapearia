using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.UsuarioPerfil
{
		public class NewEditVM : VMNewEditBase
		{
			public NewEditVM() {
                DataNascimento = null;
                Usuario = new UsuarioVM(); 
            }

			[ViewModelToModelAttribute]
			public int? IdUsuarioPerfil { get; set; }

			[ViewModelToModelAttribute]
			public int? IdUsuario { get; set; }

			[ViewModelToModelAttribute]
			public int? IdEmpresaPadrao { get; set; }

			[ViewModelToModelAttribute]
			public DateTime? DataNascimento { get; set; }

			[ViewModelToModelAttribute]
			public bool? Sexo { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteNotificacaoProcessoEmail { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteNotificacaoProcessoSMS { get; set; }

			[ViewModelToModelAttribute]
			public Guid ChaveVersao { get; set; }            

            //[ViewModelToModelAttribute(ComplexType = true)]
            //public UsuarioVM Usuario { get; set; }

            //[ViewModelToModelAttribute(ComplexType = true)]
            //public PessoaVM Pessoa { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public UsuarioVM Usuario { get; set; }

            public class UsuarioVM
            {
                [ViewModelToModelAttribute]
                public int? IdUsuario { get; set; }

                [ViewModelToModelAttribute]
                public string Login { get; set; }

                [ViewModelToModelAttribute]
                public string Email { get; set; }

                [ViewModelToModelAttribute]
                public bool Ativo { get; set; }
            }

            //public class PessoaVM
            //{
            //    [ViewModelToModelAttribute]
            //    public int? IdPessoa { get; set; }

            //    [ViewModelToModelAttribute]
            //    public string Nome { get; set; }

            //    [ViewModelToModelAttribute]
            //    public string NomeSecundario { get; set; }

            //    [ViewModelToModelAttribute]
            //    public int Telefone1 { get; set; }

            //    [ViewModelToModelAttribute]
            //    public int Telefone2 { get; set; }

            //    [ViewModelToModelAttribute]
            //    public string Imagem { get; set; }                
            //}
	}
}
