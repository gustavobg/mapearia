using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using Newtonsoft.Json.Converters;

namespace HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.UsuarioPerfil
{
    public class UsuarioPerfilVM : VMIndexBase
    {
        public UsuarioPerfilVM(string url) : base(url)
        {
            DataNascimento = null;
            ArquivosDigitais = new List<ArquivoDigital>();
        }

        [ViewModelToModelAttribute]
        public int? IdUsuarioPerfil { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUsuario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataNascimento { get; set; }

        public string SexoOpcao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        // Usuario
        [ViewModelToModelAttribute]
        public string Email { get; set; }

        // Pessoa
        [ViewModelToModelAttribute]
        public string Nome { get; set; }

        [ViewModelToModelAttribute]
        public string NomeSecundario { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone1 { get; set; }

        [ViewModelToModelAttribute]
        public string Telefone2 { get; set; }

        [ViewModelToModelAttribute]
        public string DataNascimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteNotificacaoProcessoEmail { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteNotificacaoProcessoSMS { get; set; }

        // Arquivo Digital 
        [ViewModelToModelAttribute(ComplexType=true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }       

    }
}
