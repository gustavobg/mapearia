﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Animal;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class AnimalController : ControllerExtended
    {
        public AnimalController()
        {
            IndexUrl = "/Criacao/Animal/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoAnimalInfo info = new CriacaoAnimalInfo();

            ViewModelToModelMapper.Map<CriacaoAnimalInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.DataInicioControleExibicao))
                info.DataInicioControle = Convert.ToDateTime(vm.DataInicioControleExibicao);
            if (!string.IsNullOrEmpty(vm.DataNascimentoExibicao))
                info.DataNascimento = Convert.ToDateTime(vm.DataNascimentoExibicao);
            if (!string.IsNullOrEmpty(vm.DataTerminoControleExibicao))
                info.DataTerminoControle = Convert.ToDateTime(vm.DataTerminoControle);
            #endregion

            var response = CriacaoAnimalBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoAnimal.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoAnimal, "CriacaoAnimal", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoAnimalInfo info = CriacaoAnimalBll.Instance.ListarPorIdCompleto(new CriacaoAnimalInfo { IdCriacaoAnimal = id.Value });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                #region Conversões
                if (info.DataInicioControle.HasValue)
                    vm.DataInicioControleExibicao = info.DataInicioControle.Value.ToShortDateString();
                if (info.DataNascimento.HasValue)
                    vm.DataNascimentoExibicao = info.DataNascimento.Value.ToShortDateString();
                if (info.DataTerminoControle.HasValue)
                    vm.DataTerminoControleExibicao = info.DataTerminoControle.Value.ToShortDateString();
                #endregion

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoAnimal, "CriacaoAnimal");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoAnimalInfo info = new CriacaoAnimalInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoAnimal = vm.IdCriacaoAnimal;
            info.Descricao = vm.Descricao;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.IdCriacaoRacaIn = vm.IdCriacaoRacaIn;
            info.IdCriacaoCategoriaIn = vm.IdCriacaoCategoriaIn;
            info.IdCriacaoCaracterizacaoSituacaoIn = vm.IdCriacaoCaracterizacaoSituacaoIn;
            info.Ativo = vm.Ativo;

            ListPaged<CriacaoAnimalInfo> retorno = new ListPaged<CriacaoAnimalInfo>(CriacaoAnimalBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoAnimal", true);

        }

    }
}
