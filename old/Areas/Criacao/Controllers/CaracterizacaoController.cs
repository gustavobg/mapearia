﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Caracterizacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class CaracterizacaoController : ControllerExtended
    {
        public CaracterizacaoController()
        {
            IndexUrl = "/Criacao/Caracterizacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoCaracterizacaoInfo info = CriacaoCaracterizacaoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa );
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoCaracterizacao, "CriacaoCaracterizacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoCaracterizacaoInfo info = new CriacaoCaracterizacaoInfo();

            ViewModelToModelMapper.Map<CriacaoCaracterizacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsRacas))
            {
                info.RacasCaracterizadas = new List<CriacaoCaracterizacaoRacaInfo>();
                CommaSeparatedToList(vm.IdsRacas).ForEach(idCriacaoRaca =>
                {
                    CriacaoCaracterizacaoRacaInfo criacaoCaracterizacaoRacaInfo = new CriacaoCaracterizacaoRacaInfo();
                    criacaoCaracterizacaoRacaInfo.IdCriacaoRaca = idCriacaoRaca;

                    info.RacasCaracterizadas.Add(criacaoCaracterizacaoRacaInfo);
                });
            }

            var response = CriacaoCaracterizacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoCaracterizacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoCaracterizacao, "CriacaoCaracterizacao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoCaracterizacaoEmpresaInfo info = new CriacaoCaracterizacaoEmpresaInfo();
            CriacaoCaracterizacaoEmpresaInfo response = new CriacaoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new CriacaoCaracterizacaoEmpresaInfo { IdCriacaoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCaracterizacao, "CriacaoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoCaracterizacaoEmpresaInfo info = new CriacaoCaracterizacaoEmpresaInfo();
            CriacaoCaracterizacaoEmpresaInfo response = new CriacaoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new CriacaoCaracterizacaoEmpresaInfo { IdCriacaoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCaracterizacao, "CriacaoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoCaracterizacaoInfo info = new CriacaoCaracterizacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoCaracterizacao = vm.IdCriacaoCaracterizacao;
            info.Descricao = vm.Descricao;
            info.NaturezaIn = vm.NaturezaIn;
            info.IdCriacaoRacaIn = vm.IdCriacaoRacaIn;
            info.Ativo = vm.Ativo;
            info.IdCriacaoCaracterizacaoTipo = vm.IdCriacaoCaracterizacaoTipo;
            info.IdCriacaoCaracterizacaoTipoIn = vm.IdCriacaoCaracterizacaoTipoIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoCaracterizacaoInfo> retorno = new ListPaged<CriacaoCaracterizacaoInfo>(CriacaoCaracterizacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            foreach (var item in retorno)
            {
                if (item.IdEmpresa != null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value != 1)
                {
                    item.EditarRegistro = true;
                }
                if (item.IdEmpresa == null && ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa == 1)
                {
                    item.EditarRegistro = true;
                }
                if(item.IdEmpresa == ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa)
                {
                    item.EditarRegistro = true;
                }
            }

            return base.CreateListResult(vm, retorno, "Exportação:Caracterização", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarCaracterizacaoPorRaca(ListVM vm)
        {
            CriacaoCaracterizacaoInfo info = new CriacaoCaracterizacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoCaracterizacao = vm.IdCriacaoCaracterizacao;
            info.IdCriacaoCaracterizacaoTipo = vm.IdCriacaoCaracterizacaoTipo;
            info.IdCriacaoRaca = vm.IdCriacaoRaca;
            info.Descricao = vm.Descricao;

            ListPaged<CriacaoCaracterizacaoInfo> retorno = new ListPaged<CriacaoCaracterizacaoInfo>(CriacaoCaracterizacaoBll.Instance.ListarCaracterizacaoPorRaca(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Caracterização", true);

        }
    }
}
