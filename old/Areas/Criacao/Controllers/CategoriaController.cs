﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Categoria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class CategoriaController : ControllerExtended
    {
        public CategoriaController()
        {
            IndexUrl = "/Criacao/Categoria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoCategoriaInfo info = CriacaoCategoriaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoCategoria, "CriacaoCategoria");
            }
            else
            {
                vm.Ativo = true;
            }

            //Busca parâmetro
            //var caracteristica = new CaracteristicaBLL().AdquirirCaracteristicaComValor(new DTO.Caracteristica.DTOAdquirirCaracteristica { Codigo = "Centro_Custo_Obrigatorio" }, IdEmpresa: IdEmpresa).FirstOrDefault();
            //if (caracteristica != null)
            //    if (!string.IsNullOrEmpty(caracteristica.CaracteristicaValor.Valor))
            //        vm.CentroCustoObrigatorio = Convert.ToBoolean(caracteristica.CaracteristicaValor.Valor);

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoCategoriaInfo info = new CriacaoCategoriaInfo();

            ViewModelToModelMapper.Map<CriacaoCategoriaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversões
            CommaSeparatedToList(vm.IdsProdutoServico).ForEach(idProdutoServico =>
            {
                CriacaoCategoriaProdutoServicoInfo item = new CriacaoCategoriaProdutoServicoInfo();
                item.IdProdutoServico = idProdutoServico;

                info.lstCriacaoCategoriaProdutoServico.Add(item);
            });



            #endregion

            var response = CriacaoCategoriaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoCategoria.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoCategoria, "CriacaoCategoria", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoCategoriaEmpresaInfo info = new CriacaoCategoriaEmpresaInfo();
            CriacaoCategoriaEmpresaInfo response = new CriacaoCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCategoriaEmpresaBll.Instance.ListarPorParametros(new CriacaoCategoriaEmpresaInfo { IdCriacaoCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCategoria, "CriacaoCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoCategoriaEmpresaInfo info = new CriacaoCategoriaEmpresaInfo();
            CriacaoCategoriaEmpresaInfo response = new CriacaoCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCategoriaEmpresaBll.Instance.ListarPorParametros(new CriacaoCategoriaEmpresaInfo { IdCriacaoCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCategoria, "CriacaoCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoCategoriaInfo info = new CriacaoCategoriaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoCategoria = vm.IdCriacaoCategoria;
            info.IdCriacaoCategoriaNotIn = vm.IdCriacaoCategoriaNotIn;
            info.IdCriacaoCategoriaIn = vm.IdCriacaoCategoriaIn;
            info.Descricao = vm.Descricao;
            info.IdCriacaoGrupoCategoria = vm.IdCriacaoGrupoCategoria;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.IdCriacaoGrupoCategoriaIn = vm.IdCriacaoGrupoCategoriaIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoCategoriaInfo> retorno = new ListPaged<CriacaoCategoriaInfo>(CriacaoCategoriaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoCategoria", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarCategoriasPorRaca(ListVM vm)
        {
            CriacaoCategoriaInfo info = new CriacaoCategoriaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoCategoria = vm.IdCriacaoCategoria;
            info.IdCriacaoCategoriaIn = vm.IdCriacaoCategoriaIn;
            info.IdCriacaoRaca = vm.IdCriacaoRaca;
            info.Descricao = vm.Descricao;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoCategoriaInfo> retorno = new ListPaged<CriacaoCategoriaInfo>(CriacaoCategoriaBll.Instance.ListarCategoriasPorRaca(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoCategoria", true);
        }
    }
}
