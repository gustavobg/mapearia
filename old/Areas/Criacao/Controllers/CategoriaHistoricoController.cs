﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Historico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class CategoriaHistoricoController : ControllerExtended
    {
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult ListCategoriaAnimal(ListVM vm)
        {
            CriacaoAnimalCategoriaHistoricoInfo info = new CriacaoAnimalCategoriaHistoricoInfo();
            info.IdCriacaoAnimal = vm.IdCriacaoAnimal;

            ListPaged<CriacaoAnimalCategoriaHistoricoInfo> retorno = new ListPaged<CriacaoAnimalCategoriaHistoricoInfo>(CriacaoAnimalCategoriaHistoricoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CategoriaAnimalHistorico", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult ListCategoriaLote(ListVM vm)
        {
            CriacaoLoteCategoriaHistoricoInfo info = new CriacaoLoteCategoriaHistoricoInfo();
            info.IdCriacaoLote = vm.IdCriacaoLote;

            ListPaged<CriacaoLoteCategoriaHistoricoInfo> retorno = new ListPaged<CriacaoLoteCategoriaHistoricoInfo>(CriacaoLoteCategoriaHistoricoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CategoriaLoteHistorico", true);
        }

    }
}
