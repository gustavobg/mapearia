﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.GrupoCategoria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class GrupoCategoriaController : ControllerExtended
    {
        public GrupoCategoriaController()
        {
            IndexUrl = "/Criacao/GrupoCategoria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoGrupoCategoriaInfo info = CriacaoGrupoCategoriaBll.Instance.ListarPorParametros(new CriacaoGrupoCategoriaInfo { IdCriacaoGrupoCategoria = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoGrupoCategoria, "CriacaoGrupoCategoria");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();           
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoGrupoCategoriaInfo info = new CriacaoGrupoCategoriaInfo();

            ViewModelToModelMapper.Map<CriacaoGrupoCategoriaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = CriacaoGrupoCategoriaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoGrupoCategoria.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoGrupoCategoria, "CriacaoGrupoCategoria", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoGrupoCategoriaEmpresaInfo info = new CriacaoGrupoCategoriaEmpresaInfo();
            CriacaoGrupoCategoriaEmpresaInfo response = new CriacaoGrupoCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoGrupoCategoriaEmpresaBll.Instance.ListarPorParametros(new CriacaoGrupoCategoriaEmpresaInfo { IdCriacaoGrupoCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoGrupoCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoGrupoCategoria, "CriacaoGrupoCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoGrupoCategoriaEmpresaInfo info = new CriacaoGrupoCategoriaEmpresaInfo();
            CriacaoGrupoCategoriaEmpresaInfo response = new CriacaoGrupoCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoGrupoCategoriaEmpresaBll.Instance.ListarPorParametros(new CriacaoGrupoCategoriaEmpresaInfo { IdCriacaoGrupoCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoGrupoCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoGrupoCategoria, "CriacaoGrupoCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoGrupoCategoriaInfo info = new CriacaoGrupoCategoriaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoGrupoCategoria = vm.IdCriacaoGrupoCategoria;
            info.IdCriacaoTipo = vm.IdCriacaoTipo;
            info.Descricao = vm.Descricao;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoGrupoCategoriaInfo> retorno = new ListPaged<CriacaoGrupoCategoriaInfo>(CriacaoGrupoCategoriaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoCategoria", true);

        }
    }
}
