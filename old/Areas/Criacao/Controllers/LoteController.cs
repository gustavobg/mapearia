﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Lote;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class LoteController : ControllerExtended
    {
        public LoteController()
        {
            IndexUrl = "/Criacao/Lote/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoLoteInfo info = new CriacaoLoteInfo();

            ViewModelToModelMapper.Map<CriacaoLoteInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversões

            if (!string.IsNullOrEmpty(vm.DataInicioControleExibicao))
                info.DataInicioControle = Convert.ToDateTime(vm.DataInicioControleExibicao);
            if (!string.IsNullOrEmpty(vm.DataTerminoControleExibicao))
                info.DataTerminoControle = Convert.ToDateTime(vm.DataTerminoControleExibicao);
            if (!string.IsNullOrEmpty(vm.DataNascimentoExibicao))
                info.DataNascimento = Convert.ToDateTime(vm.DataNascimentoExibicao);

            #endregion

            var response = CriacaoLoteBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoLote.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoLote, "CriacaoLote", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoLoteInfo info = CriacaoLoteBll.Instance.ListarPorIdCompleto(new CriacaoLoteInfo { IdCriacaoLote = id.Value });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                #region Conversões
                if (info.DataInicioControle.HasValue)
                    vm.DataInicioControleExibicao = info.DataInicioControle.Value.ToShortDateString();
                if (info.DataNascimento.HasValue)
                    vm.DataNascimentoExibicao = info.DataNascimento.Value.ToShortDateString();
                if (info.DataTerminoControle.HasValue)
                    vm.DataTerminoControleExibicao = info.DataTerminoControle.Value.ToShortDateString();
                #endregion

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoLote, "CriacaoLote");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();           
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoLoteInfo info = new CriacaoLoteInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoLote = vm.IdCriacaoLote;
            info.Descricao = vm.Descricao;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.IdCricaoRacaIn = vm.IdCricaoRacaIn;
            info.IdCriacaoCategoriaIn = vm.IdCriacaoCategoriaIn;
            info.IdCriacaoCaracterizacaoSituacaoIn = vm.IdCriacaoCaracterizacaoSituacaoIn;
            info.Ativo = vm.Ativo;

            ListPaged<CriacaoLoteInfo> retorno = new ListPaged<CriacaoLoteInfo>(CriacaoLoteBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação:Lote", true);
        }
    }
}
