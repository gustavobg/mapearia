﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Raca;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class RacaController : ControllerExtended
    {
        public RacaController()
        {
            IndexUrl = "/Criacao/Raca/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoRacaInfo info = CriacaoRacaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoRaca, "CriacaoRaca");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoRacaInfo info = new CriacaoRacaInfo();
            ViewModelToModelMapper.Map<CriacaoRacaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = CriacaoRacaBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoRaca.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoRaca, "CriacaoRaca", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoRacaEmpresaInfo info = new CriacaoRacaEmpresaInfo();
            CriacaoRacaEmpresaInfo response = new CriacaoRacaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoRacaEmpresaBll.Instance.ListarPorParametros(new CriacaoRacaEmpresaInfo { IdCriacaoRaca = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoRacaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoRaca, "CriacaoRaca", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoRacaEmpresaInfo info = new CriacaoRacaEmpresaInfo();
            CriacaoRacaEmpresaInfo response = new CriacaoRacaEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoRacaEmpresaBll.Instance.ListarPorParametros(new CriacaoRacaEmpresaInfo { IdCriacaoRaca = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoRacaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoRaca, "CriacaoRaca", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoRacaInfo info = new CriacaoRacaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoRaca = vm.IdCriacaoRaca;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdCriacaoRacaIn = vm.IdCriacaoRacaIn;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoRacaInfo> retorno = new ListPaged<CriacaoRacaInfo>(CriacaoRacaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoRaca", true);
        }
    }
}
