﻿using HTM.MasterGestor.Web.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Historico;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Bll.Criacao;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class SituacaoHistoricoController : ControllerExtended
    {

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult ListSituacaoAnimal(ListVM vm)
        {
            CriacaoAnimalCaracterizacaoSituacaoHistoricoInfo info = new CriacaoAnimalCaracterizacaoSituacaoHistoricoInfo();
            info.IdCriacaoAnimal = vm.IdCriacaoAnimal;

            ListPaged<CriacaoAnimalCaracterizacaoSituacaoHistoricoInfo> retorno = new ListPaged<CriacaoAnimalCaracterizacaoSituacaoHistoricoInfo>(CriacaoAnimalCaracterizacaoSituacaoHistoricoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:SituacaoAnimalHistorico", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult ListSituacaoLote(ListVM vm)
        {
            CriacaoLoteCaracterizacaoSituacaoHistoricoInfo info = new CriacaoLoteCaracterizacaoSituacaoHistoricoInfo();
            info.IdCriacaoLote = vm.IdCriacaoLote;

            ListPaged<CriacaoLoteCaracterizacaoSituacaoHistoricoInfo> retorno = new ListPaged<CriacaoLoteCaracterizacaoSituacaoHistoricoInfo>(CriacaoLoteCaracterizacaoSituacaoHistoricoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:SituacaoLoteHistorico", true);
        }

    }
}
