﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.TipoCaracterizacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class TipoCaracterizacaoController : ControllerExtended
    {
        public TipoCaracterizacaoController()
        {
            IndexUrl = "/Criacao/TipoCaracterizacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoCaracterizacaoTipoInfo info = CriacaoCaracterizacaoTipoBll.Instance.ListarPorParametros(new CriacaoCaracterizacaoTipoInfo { IdCriacaoCaracterizacaoTipo = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoCaracterizacaoTipo, "CriacaoCaracterizacaoTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoCaracterizacaoTipoInfo info = new CriacaoCaracterizacaoTipoInfo();
            ViewModelToModelMapper.Map<CriacaoCaracterizacaoTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            
            var response = CriacaoCaracterizacaoTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoCaracterizacaoTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoCaracterizacaoTipo, "CriacaoCaracterizacaoTipo", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoCaracterizacaoTipoEmpresaInfo info = new CriacaoCaracterizacaoTipoEmpresaInfo();
            CriacaoCaracterizacaoTipoEmpresaInfo response = new CriacaoCaracterizacaoTipoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCaracterizacaoTipoEmpresaBll.Instance.ListarPorParametros(new CriacaoCaracterizacaoTipoEmpresaInfo { IdCriacaoCaracterizacaoTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoCaracterizacaoTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCaracterizacaoTipo, "CriacaoCaracterizacaoTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoCaracterizacaoTipoEmpresaInfo info = new CriacaoCaracterizacaoTipoEmpresaInfo();
            CriacaoCaracterizacaoTipoEmpresaInfo response = new CriacaoCaracterizacaoTipoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoCaracterizacaoTipoEmpresaBll.Instance.ListarPorParametros(new CriacaoCaracterizacaoTipoEmpresaInfo { IdCriacaoCaracterizacaoTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoCaracterizacaoTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoCaracterizacaoTipo, "CriacaoCaracterizacaoTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoCaracterizacaoTipoInfo info = new CriacaoCaracterizacaoTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoCaracterizacaoTipo = vm.IdCriacaoCaracterizacaoTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdCriacaoCaracterizacaoTipoIn = vm.IdCriacaoCaracterizacaoTipoIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoCaracterizacaoTipoInfo> retorno = new ListPaged<CriacaoCaracterizacaoTipoInfo>(CriacaoCaracterizacaoTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoCaracterizacaoTipo", true);
        }
    }
}

