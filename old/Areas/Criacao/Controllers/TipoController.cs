﻿using HTM.MasterGestor.Bll.Criacao;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Criacao;
using HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Tipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.Controllers
{
    public class TipoController : ControllerExtended
    {
        public TipoController()
        {
            IndexUrl = "/Criacao/Tipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                CriacaoTipoInfo info = CriacaoTipoBll.Instance.ListarPorCodigoCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdCriacaoTipo, "CriacaoTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            CriacaoTipoInfo info = new CriacaoTipoInfo();

            ViewModelToModelMapper.Map<CriacaoTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = CriacaoTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdCriacaoTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdCriacaoTipo, "CriacaoTipo", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            CriacaoTipoEmpresaInfo info = new CriacaoTipoEmpresaInfo();
            CriacaoTipoEmpresaInfo response = new CriacaoTipoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoTipoEmpresaBll.Instance.ListarPorParametros(new CriacaoTipoEmpresaInfo { IdCriacaoTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = CriacaoTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoTipo, "CriacaoTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            CriacaoTipoEmpresaInfo info = new CriacaoTipoEmpresaInfo();
            CriacaoTipoEmpresaInfo response = new CriacaoTipoEmpresaInfo();

            if (id > 0)
            {
                info = CriacaoTipoEmpresaBll.Instance.ListarPorParametros(new CriacaoTipoEmpresaInfo { IdCriacaoTipo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = CriacaoTipoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdCriacaoTipo, "CriacaoTipo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            CriacaoTipoInfo info = new CriacaoTipoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdCriacaoTipoIn = vm.IdCriacaoTipoIn;
            info.IdCriacaoTipo = vm.IdCriacaoTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdCriacaoTipoNotIn = vm.IdCriacaoTipoNotIn;
            info.ComEstagios = vm.ComEstagiosVida == null ? false : true;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<CriacaoTipoInfo> retorno = new ListPaged<CriacaoTipoInfo>(CriacaoTipoBll.Instance.ListarHierarquicamente(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:CriacaoTipo", true);

        }
    }
}
