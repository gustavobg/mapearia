﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao
{
    public class CriacaoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Criacao";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Criacao_default",
                "Criacao/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
