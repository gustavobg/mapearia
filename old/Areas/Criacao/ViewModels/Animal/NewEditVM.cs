﻿using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Animal
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Localizacao = new List<CriacaoAnimalLocalVM>();
            Proprietarios = new List<CriacaoAnimalProprietarioVM>();
            Pessoa = new NewVM();
        }

        public NewVM Pessoa { get; set; }
        [ViewModelToModelAttribute]
        public int? IdCriacaoAnimal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoRaca { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaCriador { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Apelido { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacaoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataNascimento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInicioControle { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataTerminoControle { get; set; }

        [ViewModelToModelAttribute]
        public string DataNascimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioControleExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataTerminoControleExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacaoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoAnimalLocalVM> Localizacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoAnimalProprietarioVM> Proprietarios { get; set; }

        public class CriacaoAnimalLocalVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoAnimalLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoAnimal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoUnidade { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataEntrada { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataSaida { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoLocalUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string Tempo { get; set; }

        }
        public class CriacaoAnimalProprietarioVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoAnimalProprietario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoAnimal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Cota { get; set; }

            [ViewModelToModelAttribute]
            public bool Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }
        }
    }
}
