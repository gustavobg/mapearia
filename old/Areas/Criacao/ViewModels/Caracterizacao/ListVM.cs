﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Caracterizacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoCaracterizacao { get; set; }
        public int? IdCriacaoCaracterizacaoTipo { get; set; }
        public int? Natureza { get; set; }
        public int? IdCriacaoRaca { get; set; }

        public string Descricao { get; set; }
        public string NaturezaIn { get; set; }
        public string IdCriacaoRacaIn { get; set; }
        public string IdCriacaoCaracterizacaoTipoIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
