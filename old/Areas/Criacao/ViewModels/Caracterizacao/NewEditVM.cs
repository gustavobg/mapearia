﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Caracterizacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoUtilizacao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? SituacaoLoteAnimal { get; set; }

        [ViewModelToModelAttribute]
        public bool? ExibeConsulta { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public bool? RepresentaNascimento { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsRacas { get; set; }
    }
}
