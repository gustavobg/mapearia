﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Categoria
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoCategoria { get; set; }
        public int? IdCriacaoGrupoCategoria { get; set; }
        public int? IdCriacaoRaca { get; set; }

        public string Descricao { get; set; }
        public string IdCriacaoTipoIn { get; set; }
        public string IdCriacaoGrupoCategoriaIn { get; set; }
        public string IdCriacaoCategoriaNotIn { get; set; }
        public string IdCriacaoCategoriaIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
