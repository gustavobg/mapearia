﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Categoria
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            //Inicialização das propriedades
            UtilizarDescricaoLote = false;
            FormaAlteracaoManual = true;
            FormaAlteracaoTempo = false;
            FormaAlteracaoServico = false;
        }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoAnimal { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoGrupoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdClassePatrimonial { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCategoriaReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? ControleLancamentoContabilTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? Sexo { get; set; }

        [ViewModelToModelAttribute]
        public int? Classe { get; set; }

        [ViewModelToModelAttribute]
        public bool UtilizarDescricaoLote { get; set; }

        [ViewModelToModelAttribute]
        public bool FormaAlteracaoManual { get; set; }

        [ViewModelToModelAttribute]
        public bool FormaAlteracaoTempo { get; set; }

        [ViewModelToModelAttribute]
        public bool FormaAlteracaoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomica { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeTempoVida { get; set; }

        [ViewModelToModelAttribute]
        public int? TempoVida { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool CentroCustoObrigatorio { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServico { get; set; }
    }
}
