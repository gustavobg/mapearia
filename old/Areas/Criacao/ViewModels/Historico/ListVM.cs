﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Historico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }
        public int? IdCriacaoAnimal { get; set; }
        public int? IdCriacaoLote { get; set; }

    }
}