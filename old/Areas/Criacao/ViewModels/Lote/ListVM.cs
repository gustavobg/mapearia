﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Lote
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoLote { get; set; }

        public string Descricao { get; set; }
        public string IdCriacaoTipoIn { get; set; }
        public string IdCricaoRacaIn { get; set; }
        public string IdCriacaoCategoriaIn { get; set; }
        public string IdCriacaoCaracterizacaoSituacaoIn { get; set; }

        public bool? Ativo { get; set; }
    }
}
