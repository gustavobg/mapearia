﻿using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Lote
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Proprietarios = new List<CriacaoLoteProprietarioVM>();
            Locais = new List<CriacaoLoteLocalVM>();
            Animais = new List<CriacaoLoteAnimalVM>();
            Pessoa = new NewVM();
        }

        public NewVM Pessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoRaca { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacaoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoCaracterizacaoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeInicialLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeAtualLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataNascimento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInicioControle { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataTerminoControle { get; set; }

        [ViewModelToModelAttribute]
        public string DataNascimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioControleExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataTerminoControleExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public int? QuantidadeInicialLote { get; set; }

        [ViewModelToModelAttribute]
        public int? QuantidadeAtualLote { get; set; }

        [ViewModelToModelAttribute]
        public bool? UtilizarDescricaoLote { get; set; }

        [ViewModelToModelAttribute]
        public bool InformeIndividual { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoLoteProprietarioVM> Proprietarios { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoLoteLocalVM> Locais { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoLoteAnimalVM> Animais { get; set; }

        public class CriacaoLoteProprietarioVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoLoteProprietario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoLote { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Cota { get; set; }

            [ViewModelToModelAttribute]
            public bool Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }
        }

        public class CriacaoLoteLocalVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoLoteLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoLote { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoUnidade { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataEntrada { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataSaida { get; set; }

            [ViewModelToModelAttribute]
            public string DataEntradaExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DataSaidaExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoLocalUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string Tempo { get; set; }
        }

        public class CriacaoLoteAnimalVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoLoteAnimal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoLote { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoAnimal { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataEntrada { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataSaida { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCriacaoAnimal { get; set; }

            [ViewModelToModelAttribute]
            public string Tempo { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataNascimentoCriacaoAnimal { get; set; }

            [ViewModelToModelAttribute]
            public string DataEntradaExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DataSaidaExibicao { get; set; }

            private string _DataNascimento;

            public string DataNascimento
            {
                get
                {
                    if (DataNascimentoCriacaoAnimal.HasValue)
                        return DataNascimentoCriacaoAnimal.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    return _DataNascimento;
                }
                set { _DataNascimento = value; }
            }

        }
    }
}
