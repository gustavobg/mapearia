﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Raca
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoRaca { get; set; }

        public string Descricao { get; set; }
        public string IdCriacaoTipoIn { get; set; }
        public string IdCriacaoRacaIn { get;set;}

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}