﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Raca
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            Estagios = new List<CriacaoRacaEstagioVidaVM>();
            Linhagem = new List<CriacaoLinhagemVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdCriacaoRaca { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeCicloVida { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoGrupoCategoria { get; set; }
        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? CicloVida { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeApelido { get; set; }

        [ViewModelToModelAttribute]
        public bool UtilizarDescricaoLote { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<CriacaoRacaEstagioVidaVM> Estagios { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoLinhagemVM> Linhagem { get; set; }

        public class CriacaoRacaEstagioVidaVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoRacaEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoRaca { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? InicioEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidadeEstagioVidaInicio { get; set; }
        }

        public class CriacaoLinhagemVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoLinhagem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCriacaoRaca { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

        }
    }
}