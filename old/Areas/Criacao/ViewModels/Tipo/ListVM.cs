﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Tipo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoTipo { get; set; }

        public string Descricao { get; set; }
        public string IdCriacaoTipoIn { get; set; }
        public string IdCriacaoTipoNotIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? ComEstagiosVida { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}