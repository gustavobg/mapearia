﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.Tipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Estagios = new List<CriacaoTipoEstagioVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdCriacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCriacaoTipoPai { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePadraoLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePadraoVida { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePadraoGestacao { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Prefixo { get; set; }

        [ViewModelToModelAttribute]
        public int? PrefixoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? CicloVida { get; set; }

        [ViewModelToModelAttribute]
        public int? CicloVidaFormato { get; set; }

        [ViewModelToModelAttribute]
        public int? TempoGestacao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<CriacaoTipoEstagioVM> Estagios { get; set; }

        public class CriacaoTipoEstagioVM
        {
            [ViewModelToModelAttribute]
            public int? IdCriacaoTipoEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? InicioEstagioVida { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidadeEstagioVidaInicio { get; set; }
        }

    }
}