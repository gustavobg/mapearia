﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Criacao.ViewModels.TipoCaracterizacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdCriacaoCaracterizacaoTipo { get; set; }
        
        public string Descricao { get; set; }
        public string IdCriacaoCaracterizacaoTipoIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
