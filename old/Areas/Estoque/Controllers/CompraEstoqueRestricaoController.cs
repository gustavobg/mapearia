﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.CompraEstoqueRestricao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class CompraEstoqueRestricaoController : ControllerExtended
    {
        public CompraEstoqueRestricaoController()
        {
            IndexUrl = "/#/Estoque/CompraEstoqueRestricao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoCompraEstoqueRestricaoInfo info = new ProdutoServicoCompraEstoqueRestricaoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoCompraEstoqueRestricaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversão [String to List]
            if (!string.IsNullOrEmpty(vm.IdsPessoaAprovadorSolicitacaoCompra))
            {
                info.AprovadoresSolicitacao = new List<ProdutoServicoCompraEstoqueRestricaoSolicitacaoPessoaInfo>();

                CommaSeparatedToList(vm.IdsPessoaAprovadorSolicitacaoCompra).ForEach(idpessoa =>
                {
                    ProdutoServicoCompraEstoqueRestricaoSolicitacaoPessoaInfo x = new ProdutoServicoCompraEstoqueRestricaoSolicitacaoPessoaInfo();
                    x.IdPessoa = idpessoa;

                    info.AprovadoresSolicitacao.Add(x);
                });
            }
            if (!string.IsNullOrEmpty(vm.IdsPessoasAprovadoresCotacao))
            {
                info.AprovacoresCotacao = new List<ProdutoServicoCompraEstoqueRestricaoCompraPessoaInfo>();
                CommaSeparatedToList(vm.IdsPessoasAprovadoresCotacao).ForEach(idpessoa =>
                {
                    ProdutoServicoCompraEstoqueRestricaoCompraPessoaInfo x = new ProdutoServicoCompraEstoqueRestricaoCompraPessoaInfo();
                    x.IdPessoa = idpessoa;

                    info.AprovacoresCotacao.Add(x);
                });
            }

            if (!string.IsNullOrEmpty(vm.IdsPessoaAutorizada))
            {
                info.PessoasAutorizadas = new List<ProdutoServicoCompraEstoqueRestricaoCompraAutorizacaoPessoaInfo>();
                CommaSeparatedToList(vm.IdsPessoaAutorizada).ForEach(idPessoa =>
                {
                    ProdutoServicoCompraEstoqueRestricaoCompraAutorizacaoPessoaInfo item = new ProdutoServicoCompraEstoqueRestricaoCompraAutorizacaoPessoaInfo();
                    item.IdPessoa = idPessoa;
                    info.PessoasAutorizadas.Add(item);
                });
            }
            #endregion

            var response = ProdutoServicoCompraEstoqueRestricaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServicoCompraEstoqueRestricao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServicoCompraEstoqueRestricao, "ProdutoServicoCompraEstoqueRestricao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            #region Sigla da Moeda
            try
            {
                vm.SiglaMoedaExibicao = FinanceiroMoedaBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value).Sigla;
            }
            catch
            {
                vm.SiglaMoedaExibicao = "R$";
            }
            #endregion

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoCompraEstoqueRestricaoInfo info = new ProdutoServicoCompraEstoqueRestricaoInfo();
                info = ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarPorIdCompleto(new ProdutoServicoCompraEstoqueRestricaoInfo { IdProdutoServicoCompraEstoqueRestricao = id.Value });

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServicoCompraEstoqueRestricao, "ProdutoServicoCompraEstoqueRestricao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoCompraEstoqueRestricaoInfo info = new ProdutoServicoCompraEstoqueRestricaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServicoCompraEstoqueRestricao = vm.IdProdutoServicoCompraEstoqueRestricao;
            info.IdPessoa = vm.IdPessoa;
            info.NomePessoa = vm.NomePessoa;
            info.Ativo = vm.Ativo;

            ListPaged<ProdutoServicoCompraEstoqueRestricaoInfo> retorno = new ListPaged<ProdutoServicoCompraEstoqueRestricaoInfo>(ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Restrição de Uso para Compra e Estoque", true);
        }

        [HttpGet]
        public ActionResult ListarRestricaoPorIdPessoa(int IdPessoa)
        {
            var info = ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarRestricaoPorIdPessoa(IdPessoa);

            return Json(new JsonResult { Data = info }, JsonRequestBehavior.AllowGet);
        }
    }
}