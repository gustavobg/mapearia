﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Movimento;

using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class MovimentoController : ControllerExtended
    {

        public MovimentoController()
        {
            IndexUrl = "/Estoque/Movimento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit
        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                EstoqueMovimentoInfo info = new EstoqueMovimentoInfo();
                info = EstoqueMovimentoBll.Instance.ListarPorIdCompleto(id.Value);

                #region Conversão de Datas

                if (info.DataHoraLancamento.HasValue)
                    vm.DataLancamentoExibicao = info.DataHoraLancamento.Value.ToString();


                if (info.DataHoraMovimentacao.HasValue)
                    vm.DataMovimentoExibicao = info.DataHoraMovimentacao.Value.ToString();

                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEstoqueMovimento.Value, "EstoqueMovimento");

            }
            else
            {
                vm.DataMovimentoExibicao = DateTime.Now.ToString();
                vm.DataLancamentoExibicao = DateTime.Now.ToString();
                vm.IdPessoaMovimento = IdPessoa;
            }

            return Json(vm);
        }


        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EstoqueMovimentoInfo info = new EstoqueMovimentoInfo();
            ViewModelToModelMapper.Map<EstoqueMovimentoInfo>(vm, info);

            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdPessoaEmpresaLogada = IdPessoaEmpresaLogada; //Empresa Logada (Empresa Usuária.)

            #region Conversão de Datas

            if (!string.IsNullOrEmpty(vm.DataLancamentoExibicao))
                info.DataHoraLancamento = DateTime.Parse(vm.DataLancamentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataMovimentoExibicao))
                info.DataHoraMovimentacao = DateTime.Parse(vm.DataMovimentoExibicao);

            #endregion

            var response = new EstoqueMovimentoInfo();
            switch (info.IdTransacao.Value)
            {
                #region Movimento - Entrada
                case 64: //Entrada para Ajustes de Saldo.
                    response = EstoqueMovimentoBll.Instance.EntradaParaAjusteSaldo(info);
                    break;
                case 53: //Entrada por Variações Naturais de Produto 
                    response = EstoqueMovimentoBll.Instance.EntradaPorVariacoesNaturaisProduto(info);
                    break;
                case 51: //Entrada por Diferença de inventário
                    response.Response.Sucesso = false;
                    response.Response.AdicionarMensagem("A Transação 51 não foi implementada.");
                    break;
                #endregion

                #region Movimento - Saída
                case 65: //Saída para Ajuste de Saldo do Estoque
                    response = EstoqueMovimentoBll.Instance.SaidaParaAjusteSaldoEstoque(info);
                    break;
                case 59: //Saída por Perda(ou Roubo)
                    response = EstoqueMovimentoBll.Instance.SaidaPorPerdaRoubo(info);
                    break;
                case 63: //Saída por Variações Naturais do Produto
                    response = EstoqueMovimentoBll.Instance.SaidaPorPerdaRoubo(info);
                    break;
                case 62: //Saida por Diferença Inventário
                    response.Response.Sucesso = false;
                    response.Response.AdicionarMensagem("A Transação 62 não foi implementada.");
                    break;
                #endregion

                default:
                    response.Response.Sucesso = false;
                    response.Response.AdicionarMensagem("Não é possível Localizar a Transação.");
                    break;
            }

            ////if(response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdEstoqueMovimento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEstoqueMovimento, "EstoqueMovimento", response.Response.IdHistorico);

            return Json(response);
            
        }

        #endregion

        [HttpDelete]
        [CustomAuthorize]
        public ActionResult Delete(int id)
        {
            EstoqueMovimentoInfo response = new EstoqueMovimentoInfo();

            response.Response = EstoqueMovimentoBll.Instance.ExcluirMovimentoCompleto(id, IdPessoa, IdEmpresa);

            return Json(response);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstoqueMovimentoInfo info = new EstoqueMovimentoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdEstoqueMovimento = vm.IdEstoqueMovimento;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdProdutoServicoFamiliaIn = vm.IdProdutoServicoFamiliaIn;
            info.IdProdutoLocalIn = vm.IdProdutoLocalIn;
            info.IdCentroCustoIn = vm.IdCentroCustoIn;
            info.IdPessoaIn = vm.IdPessoaIn;

            #region Conversão Datas

            if (vm.DataMovimentacaoInicio.HasValue)
                info.DataMovimentacaoInicio = vm.DataMovimentacaoInicio;
            if (vm.DataMovimentacaoFim.HasValue)
                info.DataMovimentacaoFim = vm.DataMovimentacaoFim;

            #endregion

            ListPaged<EstoqueMovimentoInfo> retorno = new ListPaged<EstoqueMovimentoInfo>(EstoqueMovimentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Extrato e Movimento de Estoque", true);

            //#region Conversão

            //info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            //info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            //info.GrupoBy = vm.Page.GroupBy;
            //info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            //info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            //#endregion

            //ListPaged<EstoqueMovimentoInfo> retorno = new ListPaged<EstoqueMovimentoInfo>(EstoqueMovimentoBll.Instance.ListarPorParametros(info)) { };
            //return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Extrato e Movimento de Estoque");



        }
    }
}
