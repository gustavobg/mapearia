﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.PrincipioAtivo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class PrincipioAtivoController : ControllerExtended
    {
        public PrincipioAtivoController()
        {
            IndexUrl = "/Estoque/PrincipioAtivo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            //Quando Principio Ativo, a Descrição deverá ser obrigotoriamente a Descrição Reduzida(Nome do Produto)
            info.Nomenclatura = 5;
            info.Descricao = info.DescricaoReduzida;
            info.Natureza = 3; // Natureza Princípio Ativo

            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto( id.Value, IdEmpresa );
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProdutoServicoEmpresaInfo info = new ProdutoServicoEmpresaInfo();
            ProdutoServicoEmpresaInfo response = new ProdutoServicoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoEmpresaInfo { IdProdutoServico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProdutoServicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServico, "ProdutoServico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProdutoServicoEmpresaInfo info = new ProdutoServicoEmpresaInfo();
            ProdutoServicoEmpresaInfo response = new ProdutoServicoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoEmpresaInfo { IdProdutoServico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServico, "ProdutoServico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.Natureza = 3; //Princípio Ativo
            info.IdProdutoServicoMarcaIn = vm.IdProdutoServicoMarcaIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            var produtos = ProdutoServicoBll.Instance.ListarPorParametros(info);

            var infoGrupo = new ProdutoServicoGrupoInfo();
            infoGrupo.IdProdutoServicoGrupoIn = string.Join(",", produtos.Select(f => f.IdProdutoServicoGrupo));
            infoGrupo.IdEmpresa = IdEmpresa;
            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(infoGrupo);

            foreach (var produto in produtos)
            {
                var descricaoCompleta = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == produto.IdProdutoServicoGrupo).DescricaoCompleta;
                produto.DescricaoCompletaProdutoServicoGrupo = descricaoCompleta;
            }

            var retorno = new ListPaged<ProdutoServicoInfo>(produtos)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Principio Ativo", true);
        }

    }
}
