﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Produto;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoController : ControllerExtended
    {
        public ProdutoController()
        {
            IndexUrl = "/Estoque/Produto/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region Dados Básicos
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult ProdutoDadosBasicos(NewEditDadosBasicosVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.Natureza = 1;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.IdsFiscalClassificacao))
            {
                info.ClassificacoesFiscaisPrevistas = new List<ProdutoServicoFiscalClassificacaoInfo>();

                CommaSeparatedToList(vm.IdsFiscalClassificacao).ForEach(idFiscalClassificacao =>
                {
                    ProdutoServicoFiscalClassificacaoInfo x = new ProdutoServicoFiscalClassificacaoInfo();
                    x.IdFiscalClassificacao = idFiscalClassificacao;

                    info.ClassificacoesFiscaisPrevistas.Add(x);
                });
            }
            #endregion

            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoDadosBasicos(int? id)
        {

            NewEditDadosBasicosVM vm = new NewEditDadosBasicosVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoDadosBasicosJson(int? id)
        {
            NewEditDadosBasicosVM vm = new NewEditDadosBasicosVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditDadosBasicosVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        #endregion

        #region Caracterização e Ações
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult ProdutoCaracterizacaoAcoes(NewEditVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsFiscalClassificacao))
            {
                info.ClassificacoesFiscaisPrevistas = new List<ProdutoServicoFiscalClassificacaoInfo>();

                CommaSeparatedToList(vm.IdsFiscalClassificacao).ForEach(idFiscalClassificacao =>
                {
                    ProdutoServicoFiscalClassificacaoInfo x = new ProdutoServicoFiscalClassificacaoInfo();
                    x.IdFiscalClassificacao = idFiscalClassificacao;

                    info.ClassificacoesFiscaisPrevistas.Add(x);
                });
            }

            info.Natureza = 1;
            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoCaracterizacaoAcoes(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoCaracterizacaoAcoesJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        #endregion

        #region Composição
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult ProdutoComposicao(NewEditVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsFiscalClassificacao))
            {
                info.ClassificacoesFiscaisPrevistas = new List<ProdutoServicoFiscalClassificacaoInfo>();

                CommaSeparatedToList(vm.IdsFiscalClassificacao).ForEach(idFiscalClassificacao =>
                {
                    ProdutoServicoFiscalClassificacaoInfo x = new ProdutoServicoFiscalClassificacaoInfo();
                    x.IdFiscalClassificacao = idFiscalClassificacao;

                    info.ClassificacoesFiscaisPrevistas.Add(x);
                });
            }

            info.Natureza = 1;
            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoComposicao(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoComposicaoJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        #endregion

        #region Fornecedor
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult ProdutoFornecedor(NewEditVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsFiscalClassificacao))
            {
                info.ClassificacoesFiscaisPrevistas = new List<ProdutoServicoFiscalClassificacaoInfo>();

                CommaSeparatedToList(vm.IdsFiscalClassificacao).ForEach(idFiscalClassificacao =>
                {
                    ProdutoServicoFiscalClassificacaoInfo x = new ProdutoServicoFiscalClassificacaoInfo();
                    x.IdFiscalClassificacao = idFiscalClassificacao;

                    info.ClassificacoesFiscaisPrevistas.Add(x);
                });
            }

            info.Natureza = 1;
            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoFornecedor(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoFornecedorJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }
        #endregion

        #region Ativar Inativar

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProdutoServicoEmpresaInfo info = new ProdutoServicoEmpresaInfo();
            ProdutoServicoEmpresaInfo response = new ProdutoServicoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoEmpresaInfo { IdProdutoServico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProdutoServicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServico, "ProdutoServico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProdutoServicoEmpresaInfo info = new ProdutoServicoEmpresaInfo();
            ProdutoServicoEmpresaInfo response = new ProdutoServicoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoEmpresaInfo { IdProdutoServico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServico, "ProdutoServico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdProdutoServicoMarcaIn = vm.IdProdutoServicoMarcaIn;
            info.IdProdutoServicoNotIn = vm.IdProdutoServicoNotIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.Natureza = 1;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.Codigo = vm.Codigo;
            info.DescricaoReduzida = vm.DescricaoReduzida;
            info.Descricao = vm.Descricao;
            info.VendaOuProducao = vm.VendaOuProducao;
            info.PermiteCompra = vm.PermiteCompra;
            info.ComparOuProduzir = vm.ComparOuProduzir;
            info.Ativo = vm.Ativo;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.PermiteProducao = vm.PermiteProducao;
            info.PermiteUso = vm.PermiteUso;
            info.Armazenamento = vm.Armazenamento;
            info.IdProdutoServicoIn = vm.IdProdutoServicoIn;
            info.IdProdutoServicoFamiliaIn = vm.IdProdutoServicoFamiliaIn;
            info.DescricaoReduzidaCodigoBusca = vm.DescricaoReduzidaCodigoBusca;
            info.DescricaoCodigoBusca = vm.DescricaoCodigoBusca;
            info.RegistroProprio = vm.RegistroProprio;

            if (!string.IsNullOrEmpty(vm.IdProdutoServicoNotIn) && !vm.IdProdutoServico.HasValue)
                vm.IdProdutoServicoNotIn = string.Empty;

            #region Grupo de Produto
            string idsBuscaGrupoProduto = string.Empty;
            if (!string.IsNullOrEmpty(vm.IdGrupoInBuscaSeparada))
            {
                string[] idsTipo = vm.IdGrupoInBuscaSeparada.Split(',');

                List<ProdutoServicoGrupoInfo> lstGrupos = new List<ProdutoServicoGrupoInfo>();
                foreach (var item in idsTipo)
                {
                    var grupoInfo = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupo = int.Parse(item), IdEmpresa = IdEmpresa }).FirstOrDefault();
                    if (grupoInfo != null && grupoInfo.IdProdutoServicoGrupo.HasValue)
                    {
                        lstGrupos.AddRange(ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { ArvoreRight = grupoInfo.Arvore + ".%", IdEmpresa = IdEmpresa }));
                        lstGrupos.Add(grupoInfo);
                    }

                }

                List<ProdutoServicoGrupoInfo> lstItemAItem = new List<ProdutoServicoGrupoInfo>();
                if (lstGrupos.Count() > 0)
                {
                    foreach (ProdutoServicoGrupoInfo item in lstGrupos)
                    {
                        string[] arvore = item.Arvore.Split('.');
                        string idsBusca = string.Empty;
                        bool entraif = false;
                        foreach (string arvoreItem in arvore)
                        {
                            if (int.Parse(arvoreItem) == item.IdProdutoServicoGrupo || entraif == true)
                            {
                                idsBusca += arvoreItem + ",";
                                entraif = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(idsBusca) && idsBusca.Substring(idsBusca.Length - 1) == ",")
                            idsBusca = idsBusca.Remove(idsBusca.Length - 1);
                        lstItemAItem.AddRange(ProdutoServicoGrupoBll.Instance.ListarPorParametros(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupoIn = idsBusca, IdEmpresa = IdEmpresa }));
                    }
                    if (lstItemAItem.Count > 0)
                    {
                        var result = lstItemAItem.Where(p => p.IdProdutoServicoGrupo != null).GroupBy(p => p.IdProdutoServicoGrupo).Select(grp => grp.First()).ToList();
                        idsBuscaGrupoProduto = string.Join(",", result.Select(p => p.IdProdutoServicoGrupo));
                    }
                }

            }
            if (!string.IsNullOrEmpty(idsBuscaGrupoProduto))
                info.IdProdutoServicoGrupoIn = idsBuscaGrupoProduto;
            #endregion

            var produtos = ProdutoServicoBll.Instance.ListarPorParametros(info);

            var infoGrupo = new ProdutoServicoGrupoInfo();
            infoGrupo.IdProdutoServicoGrupoIn = string.Join(",", produtos.Select(f => f.IdProdutoServicoGrupo));
            infoGrupo.IdEmpresa = IdEmpresa;
            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(infoGrupo);

            foreach (var produto in produtos)
            {
                var descricaoCompleta = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == produto.IdProdutoServicoGrupo).DescricaoCompleta;
                produto.DescricaoCompletaProdutoServicoGrupo = descricaoCompleta;
            }

            var retorno = new ListPaged<ProdutoServicoInfo>(produtos)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Produto", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListProdutoServicoPorProdutoLocal(ListVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.Descricao = vm.Descricao;
            info.DescricaoCodigoBusca = vm.DescricaoCodigoBusca;
            info.Natureza = vm.Natureza;
            info.Ativo = vm.Ativo;
            //TODO:
            //info.IdProdutoLocalOrigem = 1;
            //info.IdProdutoLocalDestino = 2;

            ListPaged<ProdutoServicoInfo> retorno = new ListPaged<ProdutoServicoInfo>(ProdutoServicoBll.Instance.ListarProdutoServicoPorProdutoLocal(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Produto x Local", true);
        }
    }
}
