﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoLocal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Library.Collections;
using Newtonsoft.Json;
using System.IO;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Bll.Mapeamento;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoLocalController : ControllerExtended
    {
        public ProdutoLocalController()
        {
            IndexUrl = "/Estoque/ProdutoLocal/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoLocalInfo info = ProdutoLocalBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoLocal, "ProdutoLocal");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoLocalInfo info = new ProdutoLocalInfo();

            ViewModelToModelMapper.Map<ProdutoLocalInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.IdsPessoa))
            {
                info.lstProdutoLocalPessoaEmpresa = new List<ProdutoLocalPessoaEmpresaInfo>();

                CommaSeparatedToList(vm.IdsPessoa).ForEach(idPessoa =>
                {
                    ProdutoLocalPessoaEmpresaInfo item = new ProdutoLocalPessoaEmpresaInfo();
                    item.IdPessoa = idPessoa;

                    info.lstProdutoLocalPessoaEmpresa.Add(item);
                });
            }

            #endregion


            var response = ProdutoLocalBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoLocal.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProdutoLocal, "ProdutoLocal", info.Response.IdHistorico);

            return Json(response);
        }
        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoLocalInfo info = new ProdutoLocalInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdProdutoLocal = vm.IdProdutoLocal;
            info.Descricao = vm.Descricao;
            info.IdProdutoLocalNotIn = vm.IdProdutoLocalNotIn;
            info.IdProdutoLocalIn = vm.IdProdutoLocalIn;
            info.Ativo = vm.Ativo;
            info.BuscaAvancada = vm.BuscaAvancada;

            #region Parâmetros para o Modulo de Mapeamento

            if (vm.SemItensRelacionadosComMapeamento.HasValue && vm.SemItensRelacionadosComMapeamento.Value)
            {
                List<string> lst = new List<string>();
                var lstProdutoLocal = MapeamentoElementoBll.Instance.ListarPorParametros(new Model.Mapeamento.MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 4 });
                if (!string.IsNullOrEmpty(info.IdProdutoLocalNotIn))
                {

                    lst.AddRange(info.IdProdutoLocalNotIn.Split(','));
                    if (lstProdutoLocal.Count() > 0)
                    {
                        var ids = string.Join(",", lstProdutoLocal.Select(p => p.IdReferencia).ToList());

                        lst.AddRange(ids.Split(','));
                    }
                    info.IdProdutoLocalNotIn = string.Join(",", lst);
                }
                else
                {
                    info.IdProdutoLocalNotIn = string.Join(",", lstProdutoLocal.Select(p => p.IdReferencia));
                }

            }

            #endregion

            ListPaged<ProdutoLocalInfo> retorno = new ListPaged<ProdutoLocalInfo>(ProdutoLocalBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: ProdutoLocal", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListLocaisPorEmpresaUsuaria(ListVM vm)
        {
            ProdutoLocalPessoaEmpresaInfo info = new ProdutoLocalPessoaEmpresaInfo();
            info.IdProdutoLocal = vm.IdProdutoLocal;
            info.IdPessoa = vm.IdPessoa;

            ListPaged<ProdutoLocalPessoaEmpresaInfo> retorno = new ListPaged<ProdutoLocalPessoaEmpresaInfo>(ProdutoLocalPessoaEmpresaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Locais por Empresa Usuária", true);
        }
    }
}
