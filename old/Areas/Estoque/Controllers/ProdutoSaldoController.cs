﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoSaldo;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Library.Collections;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoSaldoController : ControllerExtended
    {

        public ProdutoSaldoController()
        {
            IndexUrl = "/Estoque/ProdutoSaldo/Index";
        }

        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoSaldoInfo info = new ProdutoSaldoInfo();

            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdProdutoServicoLote = vm.IdProdutoServicoLote;
            info.IdProdutoLocal = vm.IdProdutoLocal;

            #region

            ListPaged<ProdutoSaldoInfo> retorno = new ListPaged<ProdutoSaldoInfo>(ProdutoSaldoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Produto Saldo", true);

            #endregion
        }
    }
}
