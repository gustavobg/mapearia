﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoCaracterizacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoCaracterizacaoController : ControllerExtended
    {
        public ProdutoServicoCaracterizacaoController()
        {
            IndexUrl = "/Estoque/ProdutoServicoCaracterizacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoCaracterizacaoInfo info = new ProdutoServicoCaracterizacaoInfo();

            ViewModelToModelMapper.Map<ProdutoServicoCaracterizacaoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsProdutoServicoGrupo))
            {
                info.Grupos = new List<ProdutoServicoCaracterizacaoGrupoInfo>();

                CommaSeparatedToList(vm.IdsProdutoServicoGrupo).ForEach(id =>
                {
                    ProdutoServicoCaracterizacaoGrupoInfo grupo = new ProdutoServicoCaracterizacaoGrupoInfo();
                    grupo.IdProdutoServicoGrupo = id;

                    info.Grupos.Add(grupo);
                });
            }


            var response = ProdutoServicoCaracterizacaoBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServicoCaracterizacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdProdutoServicoCaracterizacao, "ProdutoServicoCaracterizacao", info.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                ProdutoServicoCaracterizacaoInfo info = ProdutoServicoCaracterizacaoBll.Instance.ListarPorCodigoCompleto(new ProdutoServicoCaracterizacaoInfo { IdProdutoServicoCaracterizacao = id.Value });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServicoCaracterizacao, "ProdutoServicoCaracterizacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProdutoServicoCaracterizacaoEmpresaInfo info = new ProdutoServicoCaracterizacaoEmpresaInfo();
            ProdutoServicoCaracterizacaoEmpresaInfo response = new ProdutoServicoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoCaracterizacaoEmpresaInfo { IdProdutoServicoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProdutoServicoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoCaracterizacao, "ProdutoServicoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProdutoServicoCaracterizacaoEmpresaInfo info = new ProdutoServicoCaracterizacaoEmpresaInfo();
            ProdutoServicoCaracterizacaoEmpresaInfo response = new ProdutoServicoCaracterizacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoCaracterizacaoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoCaracterizacaoEmpresaInfo  { IdProdutoServicoCaracterizacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoCaracterizacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoCaracterizacao, "ProdutoServicoCaracterizacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoCaracterizacaoInfo info = new ProdutoServicoCaracterizacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServicoCaracterizacao = vm.IdProdutoServicoCaracterizacao;
            info.Descricao = vm.Descricao;
            info.NaturezaCaracterizacao = vm.NaturezaCaracterizacao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProdutoServicoCaracterizacaoInfo> retorno = new ListPaged<ProdutoServicoCaracterizacaoInfo>(ProdutoServicoCaracterizacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:ProdutoServicoCaracterizacao", true);

        }

    }
}
