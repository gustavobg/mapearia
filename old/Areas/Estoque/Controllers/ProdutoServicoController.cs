﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServico;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoController : ControllerExtended
    {
        public ProdutoServicoController()
        {
            IndexUrl = "/Estoque/ProdutoServico/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }
            
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = ProdutoServicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServico, "ProdutoServico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoInfo info = ProdutoServicoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa );
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServico, "ProdutoServico");
            }
            else
            {
                vm.Ativo = true;
            }

            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.Natureza = 1;
            info.IdProdutoServicoMarcaIn = vm.IdProdutoServicoMarcaIn;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.DescricaoReduzida = vm.DescricaoReduzida;
            info.RegistroProprio = vm.RegistroProprio;
            //info.PermiteProducao = vm.PermiteProducao;
            //info.PermiteVenda = vm.PermiteVenda;


            #region Grupo de Produto
            string idsBuscaGrupoProduto = string.Empty;
            if (!string.IsNullOrEmpty(vm.IdTipoInBuscaSeparada))
            {
                string[] idsTipo = vm.IdTipoInBuscaSeparada.Split(',');

                List<ProdutoServicoGrupoInfo> lstGrupos = new List<ProdutoServicoGrupoInfo>();
                foreach (var item in idsTipo)
                {
                    var grupoInfo = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupo = int.Parse(item), IdEmpresa = IdEmpresa }).FirstOrDefault();
                    if (grupoInfo != null && grupoInfo.IdProdutoServicoGrupo.HasValue)
                    {
                        lstGrupos.AddRange(ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { ArvoreRight = grupoInfo.Arvore + ".%", IdEmpresa = IdEmpresa }));
                        lstGrupos.Add(grupoInfo);
                    }

                }

                List<ProdutoServicoGrupoInfo> lstItemAItem = new List<ProdutoServicoGrupoInfo>();
                if (lstGrupos.Count() > 0)
                {
                    foreach (ProdutoServicoGrupoInfo item in lstGrupos)
                    {
                        string[] arvore = item.Arvore.Split('.');
                        string idsBusca = string.Empty;
                        bool entraif = false;
                        foreach (string arvoreItem in arvore)
                        {
                            if (int.Parse(arvoreItem) == item.IdProdutoServicoGrupo || entraif == true)
                            {
                                idsBusca += arvoreItem + ",";
                                entraif = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(idsBusca) && idsBusca.Substring(idsBusca.Length - 1) == ",")
                            idsBusca = idsBusca.Remove(idsBusca.Length - 1);
                        lstItemAItem.AddRange(ProdutoServicoGrupoBll.Instance.ListarPorParametros(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupoIn = idsBusca, IdEmpresa = IdEmpresa }));
                    }
                    if (lstItemAItem.Count > 0)
                    {
                        var result = lstItemAItem.Where(p => p.IdProdutoServicoGrupo != null).GroupBy(p => p.IdProdutoServicoGrupo).Select(grp => grp.First()).ToList();
                        idsBuscaGrupoProduto = string.Join(",", result.Select(p => p.IdProdutoServicoGrupo));
                    }
                }

            }
            if (!string.IsNullOrEmpty(idsBuscaGrupoProduto))
                info.IdProdutoServicoGrupoIn = idsBuscaGrupoProduto;

            #endregion

            var produtos = ProdutoServicoBll.Instance.ListarPorParametros(info);

            var infoGrupo = new ProdutoServicoGrupoInfo();
            infoGrupo.IdProdutoServicoGrupoIn = string.Join(",", produtos.Select(f => f.IdProdutoServicoGrupo));
            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(infoGrupo);

            foreach (var produto in produtos)
            {
                var descricaoCompleta = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == produto.IdProdutoServicoGrupo).DescricaoCompleta;
                produto.DescricaoCompletaProdutoServicoGrupo = descricaoCompleta; 
            }

            var retorno = new ListPaged<ProdutoServicoInfo>(produtos)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Produto", true);
        }

        /// <summary>
        /// Busca Produtos e Princípio Ativo
        /// </summary>
        /// <param name="vm"></param>
        /// <returns></returns>
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarProdutoEPrincipioAtivo(ListVM vm)
        {
            ProdutoServicoInfo info = new ProdutoServicoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdProdutoServicoNotIn = vm.IdProdutoServicoNotIn;
            info.Natureza = 3;
            info.IdProdutoServicoMarcaIn = vm.IdProdutoServicoMarcaIn;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.Codigo = vm.Codigo;
            info.Ativo = vm.Ativo;
            info.ComPrincipioAtivo = vm.ComPrincipioAtivo;
            info.DescricaoReduzida = vm.DescricaoReduzida;

            var produtos = ProdutoServicoBll.Instance.ListarProdutoEPrincipioAtivo(info);

            var infoGrupo = new ProdutoServicoGrupoInfo();
            infoGrupo.IdProdutoServicoGrupoIn = string.Join(",", produtos.Select(f => f.IdProdutoServicoGrupo));
            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(infoGrupo);

            foreach (var produto in produtos)
            {
                var descricaoCompleta = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == produto.IdProdutoServicoGrupo).DescricaoCompleta;
                produto.DescricaoCompletaProdutoServicoGrupo = descricaoCompleta;
            }

            var retorno = new ListPaged<ProdutoServicoInfo>(produtos)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Produto", true);
        }
    }
}
