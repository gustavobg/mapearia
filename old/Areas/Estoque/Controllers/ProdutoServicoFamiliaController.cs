﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFamilia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoFamiliaController : ControllerExtended
    {
        public ProdutoServicoFamiliaController()
        {
            IndexUrl = "/Estoque/ProdutoServicoFamilia/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var info = new ProdutoServicoFamiliaInfo();
            ViewModelToModelMapper.Map<ProdutoServicoFamiliaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var infoResult = ProdutoServicoFamiliaBll.Instance.Salvar(info);
            SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServicoFamilia.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, infoResult.IdProdutoServicoFamilia, "ProdutoServicoFamilia", infoResult.Response.IdHistorico);

            return Json(infoResult);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                var entidade = ProdutoServicoFamiliaBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdProdutoServicoFamilia, "ProdutoServicoFamilia");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ProdutoServicoFamiliaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServicoFamilia = vm.IdProdutoServicoFamilia;
            info.IdProdutoServicoFamiliaIn = vm.IdProdutoServicoFamiliaIn;
            info.Descricao = vm.Descricao;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdUnidadeIn = vm.IdUnidadeIn;
            info.IdUnidadeTipoIn = vm.IdUnidadeTipoIn;
            info.Ativo = vm.Ativo;

            #region Grupo de Produto (Busca Hierarquica)
            //Busca Personalizada
            //1 - Buscar os Grupos de Produto
            //1.1 - Descobrir a Arvore que cada tipo pertence
            string idsBuscaProdutoServicoGrupo = string.Empty;
            if (!string.IsNullOrEmpty(vm.IdProdutoServicoGrupoInBuscaEmProfundidade))
            {
                string[] idsTipo = vm.IdProdutoServicoGrupoInBuscaEmProfundidade.Split(',');

                List<ProdutoServicoGrupoInfo> lstProdutoServicoGrupo = new List<ProdutoServicoGrupoInfo>();
                foreach (var item in idsTipo)
                {
                    var tipoEquipamentoInfo = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupo = int.Parse(item), IdEmpresa = IdEmpresa }).FirstOrDefault();
                    if (tipoEquipamentoInfo != null && tipoEquipamentoInfo.IdProdutoServicoGrupo.HasValue)
                    {
                        lstProdutoServicoGrupo.AddRange(ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(new ProdutoServicoGrupoInfo { ArvoreRight = tipoEquipamentoInfo.Arvore + ".%", IdEmpresa = IdEmpresa }));
                        lstProdutoServicoGrupo.Add(tipoEquipamentoInfo);
                    }

                }

                List<ProdutoServicoGrupoInfo> lstItemAItem = new List<ProdutoServicoGrupoInfo>();
                if (lstProdutoServicoGrupo.Count() > 0)
                {
                    foreach (ProdutoServicoGrupoInfo item in lstProdutoServicoGrupo)
                    {
                        string[] arvore = item.Arvore.Split('.');
                        string idsBusca = string.Empty;
                        bool entraif = false;
                        foreach (string arvoreItem in arvore)
                        {
                            if (int.Parse(arvoreItem) == item.IdProdutoServicoGrupo || entraif == true)
                            {
                                idsBusca += arvoreItem + ",";
                                entraif = true;
                            }
                        }
                        if (!string.IsNullOrEmpty(idsBusca) && idsBusca.Substring(idsBusca.Length - 1) == ",")
                            idsBusca = idsBusca.Remove(idsBusca.Length - 1);
                        lstItemAItem.AddRange(ProdutoServicoGrupoBll.Instance.ListarPorParametros(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupoIn = idsBusca, IdEmpresa = IdEmpresa }));
                    }
                    if (lstItemAItem.Count > 0)
                    {
                        var result = lstItemAItem.Where(p => p.IdProdutoServicoGrupo != null).GroupBy(p => p.IdProdutoServicoGrupo).Select(grp => grp.First()).ToList();
                        idsBuscaProdutoServicoGrupo = string.Join(",", result.Select(p => p.IdProdutoServicoGrupo));
                    }
                }

            }
            if (!string.IsNullOrEmpty(idsBuscaProdutoServicoGrupo))
                info.IdProdutoServicoGrupoIn = idsBuscaProdutoServicoGrupo;

            #endregion

            var familias = ProdutoServicoFamiliaBll.Instance.ListarPorParametros(info);

            var infoGrupo = new ProdutoServicoGrupoInfo();
            infoGrupo.IdProdutoServicoGrupoIn = string.Join(",", familias.Select(f => f.IdProdutoServicoGrupo));
            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(infoGrupo);

            foreach (var familia in familias)
            {
                var descricaoCompleta = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == familia.IdProdutoServicoGrupo).DescricaoCompleta;
                familia.DescricaoCompletaProdutoServicoGrupo = descricaoCompleta; 
            }

            var retorno = new ListPaged<ProdutoServicoFamiliaInfo>(familias)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: ProdutoServicoFamilia", true);
        }
    }
}
