﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFormaApresentacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoFormaApresentacaoController : ControllerExtended
    {
        public ProdutoServicoFormaApresentacaoController()
        {
            IndexUrl = "/Estoque/ProdutoServicoFormaApresentacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                ProdutoServicoFormaApresentacaoInfo info = ProdutoServicoFormaApresentacaoBll.Instance.ListarPorParametros( new ProdutoServicoFormaApresentacaoInfo { IdEmpresa = IdEmpresa, IdProdutoServicoFormaApresentacao = id.Value } ).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdProdutoServicoFormaApresentacao.Value, "ProdutoServicoFormaApresentacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();           
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoFormaApresentacaoInfo info = new ProdutoServicoFormaApresentacaoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoFormaApresentacaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = info.IdEmpresaLogada = IdEmpresa;

            var response = ProdutoServicoFormaApresentacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServicoFormaApresentacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServicoFormaApresentacao, "ProdutoServicoFormaApresentacao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProdutoServicoFormaApresentacaoEmpresaInfo info = new ProdutoServicoFormaApresentacaoEmpresaInfo();
            ProdutoServicoFormaApresentacaoEmpresaInfo response = new ProdutoServicoFormaApresentacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoFormaApresentacaoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoFormaApresentacaoEmpresaInfo { IdProdutoServicoFormaApresentacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoFormaApresentacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoFormaApresentacao, "ProdutoServicoFormaApresentacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProdutoServicoFormaApresentacaoEmpresaInfo info = new ProdutoServicoFormaApresentacaoEmpresaInfo();
            ProdutoServicoFormaApresentacaoEmpresaInfo response = new ProdutoServicoFormaApresentacaoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoFormaApresentacaoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoFormaApresentacaoEmpresaInfo { IdProdutoServicoFormaApresentacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoFormaApresentacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoFormaApresentacao, "ProdutoServicoFormaApresentacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoFormaApresentacaoInfo info = new ProdutoServicoFormaApresentacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServicoFormaApresentacao = vm.IdProdutoServicoFormaApresentacao;
            info.Descricao = vm.Descricao;
            info.Sigla = vm.Sigla;
            info.Ativo = vm.Ativo;
            info.ControlaMovimentacao = vm.ControlaMovimentacao;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<ProdutoServicoFormaApresentacaoInfo> retorno = new ListPaged<ProdutoServicoFormaApresentacaoInfo>(ProdutoServicoFormaApresentacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: ProdutoServicoFormaApresentacao",true);
        }

    }
}
