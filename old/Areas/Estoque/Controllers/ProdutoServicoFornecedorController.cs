﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFornecedor;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoFornecedorController : ControllerExtended
    {
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoFornecedorInfo info = new ProdutoServicoFornecedorInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.BuscaPessoaDetalhada = vm.BuscaPessoaDetalhada;

            ListPaged<ProdutoServicoFornecedorInfo> retorno = new ListPaged<ProdutoServicoFornecedorInfo>(ProdutoServicoFornecedorBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Fornecedores (Produto)", true);
        }

    }
}
