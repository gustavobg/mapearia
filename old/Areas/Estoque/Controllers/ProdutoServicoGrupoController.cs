﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoGrupo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoGrupoController : ControllerExtended
    {
        public ProdutoServicoGrupoController()
        {
            IndexUrl = "/Estoque/ProdutoServicoGrupo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                var entidade = ProdutoServicoGrupoBll.Instance.ListarPorParametros(new ProdutoServicoGrupoInfo { IdProdutoServicoGrupo = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdProdutoServicoGrupo, "ProdutoServicoGrupo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            ProdutoServicoGrupoInfo info = new ProdutoServicoGrupoInfo();
            ViewModelToModelMapper.Map<ProdutoServicoGrupoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = ProdutoServicoGrupoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdProdutoServicoGrupo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdProdutoServicoGrupo, "ProdutoServicoGrupo", response.Response.IdHistorico);

            return Json(response);
        }


        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            ProdutoServicoGrupoEmpresaInfo info = new ProdutoServicoGrupoEmpresaInfo();
            ProdutoServicoGrupoEmpresaInfo response = new ProdutoServicoGrupoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoGrupoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoGrupoEmpresaInfo { IdProdutoServicoGrupo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = ProdutoServicoGrupoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoGrupo, "ProdutoServicoGrupo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            ProdutoServicoGrupoEmpresaInfo info = new ProdutoServicoGrupoEmpresaInfo();
            ProdutoServicoGrupoEmpresaInfo response = new ProdutoServicoGrupoEmpresaInfo();

            if (id > 0)
            {
                info = ProdutoServicoGrupoEmpresaBll.Instance.ListarPorParametros(new ProdutoServicoGrupoEmpresaInfo { IdProdutoServicoGrupo = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = ProdutoServicoGrupoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdProdutoServicoGrupo, "ProdutoServicoGrupo", info.Response.IdHistorico);
            }

            return Json(new { response });
        }
        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            var info = new ProdutoServicoGrupoInfo(IdEmpresa);
            info.IdProdutoServicoGrupo = vm.IdProdutoServicoGrupo;
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdProdutoServicoGrupoNotIn = vm.IdProdutoServicoGrupoNotIn;
            info.IdProdutoServicoGrupoPai = vm.IdProdutoServicoGrupoPai;
            info.IdProdutoServicoGrupoPaiIn = vm.IdProdutoServicoGrupoPaiIn;
            info.Ativo = vm.Ativo;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.Natureza = vm.Natureza;
            info.NaturezaIn = vm.NaturezaIn;
            info.UltimoNivel = vm.UltimoNivel;
            info.RegistroProprio = vm.RegistroProprio;

            var grupos = ProdutoServicoGrupoBll.Instance.ListarHierarquicamente(info);

            // salva item já definido
            ProdutoServicoGrupoInfo itemExcecao = null;
            if (vm.IdGrupoExcecao.HasValue)
            {
                itemExcecao = grupos.FirstOrDefault(g => g.IdProdutoServicoGrupo == vm.IdGrupoExcecao.Value);
            }

            if (vm.UltimoNivel.HasValue && vm.UltimoNivel.Value)
            {
                grupos = RetornaGruposDeUltimoNivel(vm.IdProdutoServicoGrupoNotIn, grupos);
            }

            if (vm.SemRelacionamentoComProduto.HasValue && vm.SemRelacionamentoComProduto.Value)
            {
                RemoveGruposRelacionadosComProdutos(grupos);
            }

            if (itemExcecao != null)
            {
                grupos.Add(itemExcecao);
                grupos = grupos.Distinct().ToList();
            }

            var retorno = new ListPaged<ProdutoServicoGrupoInfo>(grupos)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Grupo de Produto", true);
        }

        private static List<ProdutoServicoGrupoInfo> RetornaGruposDeUltimoNivel(string idProdutoServicoGrupoNotIn, List<ProdutoServicoGrupoInfo> grupos)
        {
            if (grupos.Any())
            {
                grupos.RemoveAll(g => grupos.Any(x => g.IdProdutoServicoGrupo == x.IdProdutoServicoGrupoPai));

                var idProdutoServicoGrupo = 0;
                if (int.TryParse(idProdutoServicoGrupoNotIn, out idProdutoServicoGrupo))
                {
                    grupos.RemoveAll(g => g.IdProdutoServicoGrupoPai == idProdutoServicoGrupo);
                }
            }

            return grupos;
        }

        private static void RemoveGruposRelacionadosComProdutos(List<ProdutoServicoGrupoInfo> grupos)
        {
            if (grupos.Any())
            {
                var infoIds = new ProdutoServicoGrupoInfo();
                infoIds.IdProdutoServicoGrupoIn = string.Join(",", grupos.Select(g => g.IdProdutoServicoGrupo));
                var gruposValidos = ProdutoServicoGrupoBll.Instance.ListarSemRelacionamentoComProduto(infoIds);

                grupos.RemoveAll(g => !gruposValidos.Any(v => v.IdProdutoServicoGrupo == g.IdProdutoServicoGrupo.Value));
            }
        }
    }
}
