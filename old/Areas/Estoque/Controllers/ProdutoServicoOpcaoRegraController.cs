﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoOpcaoRegra;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoOpcaoRegraController : ControllerExtended
    {

        public ProdutoServicoOpcaoRegraController()
        {
            IndexUrl = "/Estoque/ProdutoServicoOpcaoRegra/Index";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoOpcaoRegraInfo info = new ProdutoServicoOpcaoRegraInfo();
            info.IdProdutoServicoOpcaoRegra = vm.IdProdutoServicoOpcaoRegra;
            info.IdProdutoServicoOpcaoRegraIn = vm.IdProdutoServicoOpcaoRegraIn;
            info.Descricao = vm.Descricao;
            info.Observacao = vm.Observacao;
            info.Ordem = vm.Ordem;
            info.Ativo = vm.Ativo;

            ListPaged<ProdutoServicoOpcaoRegraInfo> retorno = new ListPaged<ProdutoServicoOpcaoRegraInfo>(ProdutoServicoOpcaoRegraBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Opções de Regra", true);

        }

    }
}
