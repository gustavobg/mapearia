﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoProdutoLocal;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Estoque;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class ProdutoServicoProdutoLocalController : ControllerExtended
    {
        public ProdutoServicoProdutoLocalController()
        {
            IndexUrl = "/Estoque/ProdutoServicoProdutoLocal/Index";
        }

        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            ProdutoServicoProdutoLocalInfo info = new ProdutoServicoProdutoLocalInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdProdutoLocal = vm.IdProdutoLocal;
            info.DescricaoProdutoLocal = vm.ProdutoLocalDescricao;

            info.IdProdutoServicoIn = vm.IdProdutoServicoIn;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<ProdutoServicoProdutoLocalInfo> retorno = new ListPaged<ProdutoServicoProdutoLocalInfo>(ProdutoServicoProdutoLocalBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Produto x Local");


        }

    }
}
