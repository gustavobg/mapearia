﻿
using HTM.MasterGestor.Bll.Corporativo;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Corporativo;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Requisicao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;


using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class RequisicaoController : ControllerExtended
    {
        public RequisicaoController()
        {
            IndexUrl = "/Estoque/Requisicao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                EstoqueRequisicaoInfo info = new EstoqueRequisicaoInfo();
                info = EstoqueRequisicaoBll.Instance.ListarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEstoqueRequisicao.Value, "EstoqueRequisicao");

            }
            else
            {
                vm.DataMovimentacaoExibicao = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
                vm.DataRequisicaoExibicao = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());

                vm.IdPessoaResponsavel = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa; //Usuário Logado (Responsável)
                vm.UsuarioLogado = ControladorSessaoUsuario.SessaoCorrente.Pessoa.NomeSecundario; //Usuário logado (Responsável)

                //Verifica se o usuário logado pode requisitar

                var pessoaRequisitante = PessoaBll.Instance.ListarPessoaPorTipo(new PessoaInfo
                {
                    IdEmpresa = IdEmpresa,
                    IdPessoa = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value,
                    ComEmpresa = false,
                    ComEmpresaUsuaria = false,
                    ComPessoa = true,
                    PessoaPerfil_PermiteRealizarCompra = true
                }).FirstOrDefault();


                //Verifica se usuário possui restrição
                if (pessoaRequisitante != null && pessoaRequisitante.IdPessoa.HasValue)
                {
                    //Caso não tenha restrição será o usuário logado o Default
                    var restricaoRequisicaoEntrega = ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarPorParametros(new ProdutoServicoCompraEstoqueRestricaoInfo { IdPessoa = pessoaRequisitante.IdPessoa.Value, PermiteSolicitarRequisitar = false }).FirstOrDefault();
                    if (restricaoRequisicaoEntrega == null)
                    {
                        vm.IdPessoaRequisitante = pessoaRequisitante.IdPessoa.Value;
                        vm.UsuarioLogado = pessoaRequisitante.NomeSecundario;
                    }
                }

                vm.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa; //Empresa Usuário Logada
            }



            vm.IdsPessoaNotIn_RestricaoRequisicao = string.Join(",", ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarPorParametros(

                                     new ProdutoServicoCompraEstoqueRestricaoInfo
                                     {
                                         IdEmpresa = IdEmpresa,
                                         PermiteRealizarEntrega = false
                                     })
                                     .Select(p => p.IdPessoa));
            vm.IdsPessoaNotIn_RestricaoEntrega = string.Join(",", ProdutoServicoCompraEstoqueRestricaoBll.Instance.ListarPorParametros(
                                     new ProdutoServicoCompraEstoqueRestricaoInfo
                                     {
                                         IdEmpresa = IdEmpresa,
                                         PermiteRealizarEntrega = false
                                     })
                                     .Select(p => p.IdPessoa));

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EstoqueRequisicaoInfo info = new EstoqueRequisicaoInfo();
            ViewModelToModelMapper.Map<EstoqueRequisicaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaEmpresaLogada = IdPessoaEmpresaLogada;

            #region Conversão (Datas)

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoExibicao))
                info.DataMovimentacao = DateTime.Parse(vm.DataMovimentacaoExibicao);

            if (!string.IsNullOrEmpty(vm.DataRequisicaoExibicao))
                info.DataRequisicao = DateTime.Parse(vm.DataRequisicaoExibicao);

            #endregion

            var response = EstoqueRequisicaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdEstoqueRequisicao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEstoqueRequisicao, "EstoqueRequisicao", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        #region List

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstoqueRequisicaoInfo info = new EstoqueRequisicaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdEstoqueRequisicao = vm.IdEstoqueRequisicao;

            info.IdProdutoServicoGrupo = vm.IdProdutoServicoGrupo;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacao = vm.IdAplicacao;

            #region Conversão Datas

            if (!string.IsNullOrEmpty(vm.DataRequisicaoInicio))
                info.DataRequisicaoInicio = Convert.ToDateTime(vm.DataRequisicaoInicio);

            if (!string.IsNullOrEmpty(vm.DataRequisicaoFim))
                info.DataRequisicaoFim = Convert.ToDateTime(vm.DataRequisicaoFim);

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            ListPaged<EstoqueRequisicaoInfo> retorno = new ListPaged<EstoqueRequisicaoInfo>(EstoqueRequisicaoBll.Instance.ListarPorParametros(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Requisição de Estoque");
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListRequisicaoCompleta(ListVM vm)
        {
            EstoqueRequisicaoInfo info = new EstoqueRequisicaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdEstoqueRequisicao = vm.IdEstoqueRequisicao;
            info.IdEstoqueRequisicaoNotIn = vm.IdEstoqueRequisicaoNotIn;

            info.IdProdutoServicoGrupo = vm.IdProdutoServicoGrupo;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdAplicacaoTipo = vm.IdAplicacaoTipo;
            info.IdAplicacao = vm.IdAplicacao;
            info.IdProcessoSituacaoIn = vm.IdProcessoSituacaoIn;
            info.BuscaAvancada = vm.BuscaAvancada;

            #region Conversão Datas

            if (!string.IsNullOrEmpty(vm.DataRequisicaoInicio))
                info.DataRequisicaoInicio = Convert.ToDateTime(vm.DataRequisicaoInicio);

            if (!string.IsNullOrEmpty(vm.DataRequisicaoFim))
                info.DataRequisicaoFim = Convert.ToDateTime(vm.DataRequisicaoFim);

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            ListPaged<EstoqueRequisicaoInfo> retorno = new ListPaged<EstoqueRequisicaoInfo>(EstoqueRequisicaoBll.Instance.ListarPorParametroCompleto(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Requisição de Estoque");
        }


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListItensPorRequisicao(ListVM vm)
        {
            ListPaged<EstoqueRequisicaoItemInfo> lst = new ListPaged<EstoqueRequisicaoItemInfo>(EstoqueRequisicaoItemBll.Instance.ListarPorParametros(new EstoqueRequisicaoItemInfo { IdEstoqueRequisicao = vm.IdEstoqueRequisicao.Value }))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = 1,
                    PageSize = int.MaxValue,
                    GroupBy = string.Empty,
                    OrderBy = string.Empty
                }
            };

            return base.CreateListResult(vm, lst, "Requisição Item", true);
        }

        #endregion
    }
}
