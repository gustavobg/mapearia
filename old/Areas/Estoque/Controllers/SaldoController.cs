﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Saldo;

using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class SaldoController : ControllerExtended
    {

        public SaldoController()
        {
            IndexUrl = "/Estoque/Saldo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region Produto por Família

        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoPorFamilia(int? id)
        {
            return View();
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoPorFamiliaJson(int? id)
        {
            SaldoVM vm = new SaldoVM();

            EstoqueSaldoInfo info = EstoqueSaldoBll.Instance.ListarProdutoPorFamilia2(new EstoqueSaldoInfo { IdEmpresa_Param = IdEmpresa, IdProdutoServicoFamilia_Param = id.Value });
            ViewModelToModelMapper.MapBack<SaldoVM>(vm, info);

            return Json(vm);
        }


        #endregion

        #region Local por Família 
        [HttpGet, CustomAuthorize]
        public ActionResult LocalPorFamilia(int? id)
        {
            return View();
        }

        [HttpPost, CustomAuthorize]
        public ActionResult LocalPorFamiliaJson(int? id)
        {

            SaldoVM vm = new SaldoVM();
            EstoqueSaldoInfo info = new EstoqueSaldoInfo();

            if (id.HasValue)
            {
                info = EstoqueSaldoBll.Instance.ListarLocalPorFamilia(new EstoqueSaldoInfo { IdEmpresa_Param = IdEmpresa, IdProdutoServicoFamilia_Param = id.Value });
                ViewModelToModelMapper.MapBack<SaldoVM>(vm, info);
            }
            else
            {
                //TODO: Mensagem de Erro;
            }

            return Json(vm);
        }

        #endregion

        #region Local por Produto

        [HttpGet, CustomAuthorize]
        public ActionResult LocalPorProduto(int? id)
        {
            return View();
        }

        [HttpPost, CustomAuthorize]
        public ActionResult LocalPorProdutoJson(int? id)
        {
            SaldoVM vm = new SaldoVM();
            EstoqueSaldoInfo info = new EstoqueSaldoInfo();

            if(id.HasValue)
            {
                info = EstoqueSaldoBll.Instance.ListarLocalPorProduto(new EstoqueSaldoInfo { IdEmpresa_Param = IdEmpresa, IdProdutoServico_Param = id.Value });
                ViewModelToModelMapper.MapBack<SaldoVM>(vm, info);
            }
            else
            {
                //TODO: Mensagemd de Erro;
            }

            return Json(vm);
        }

        #endregion

        #region Produto por Local
        [HttpGet, CustomAuthorize]
        public ActionResult ProdutoPorLocal(int? id)
        {
            return View();
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ProdutoPorLocalJson(int? id)
        {
            SaldoVM vm = new SaldoVM();
            EstoqueSaldoInfo info = new EstoqueSaldoInfo();

            if(id.HasValue)
            {
                info = EstoqueSaldoBll.Instance.ListarProdutoPorLocal(new EstoqueSaldoInfo { IdEmpresa_Param = IdEmpresa, IdProdutoLocal_Param = id.Value });
                ViewModelToModelMapper.MapBack<SaldoVM>(vm, info);
            }
            else
            {
                //TODO: Erro
            }

            return Json(vm);
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstoqueSaldoInfo info = new EstoqueSaldoInfo();
            info.IdEmpresa_Param = IdEmpresa;
            info.Visao_Param = vm.Visao;
            info.BuscaAvancada_Param = vm.BuscaAvancada;

            ListPaged<EstoqueSaldoInfo> retorno = new ListPaged<EstoqueSaldoInfo>(EstoqueSaldoBll.Instance.ListarSaldoPorVisao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return CreateListResult(vm, retorno, "Exportação: Saldo em Estoque", true);

        }
    }
}
