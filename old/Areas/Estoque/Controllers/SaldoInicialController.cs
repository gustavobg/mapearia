﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.SaldoInicial;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MasterGestor.CORE.Nucleo.Base;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class SaldoInicialController : ControllerExtended
    {
        public SaldoInicialController()
        {
            IndexUrl = "/Estoque/SaldoInicial/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New/Edit
        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                EstoqueSaldoInicialInfo info = new EstoqueSaldoInicialInfo();
                info = EstoqueSaldoInicialBll.Instance.LitarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEstoqueSaldoInicial.Value, "SaldoInicial");

            }
            else
            {
                //Usuário Logado
                vm.IdPessoaRealizacao = IdPessoa; //Apenas como 'Facilitador'
                vm.IdPessoa = IdPessoaEmpresaLogada; // Empresa Usuária Logada.
                vm.DataLancamento = DateTime.Now;
                vm.DataReferencia = DateTime.Now;

                //Moeda Padrão da Empresa
                if(ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.HasValue)
                {
                    vm.IdFinanceiroMoeda = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value;
                }
                else
                {
                    vm.Response.Sucesso = false;
                    vm.Response.AdicionarMensagem("Não existe Moeda Padrão configurada para a sua Empresa.Contate o Administrador do Sistema.");
                    vm.Response.Identificador = BaseInfoResponse.IdentificadorEnum.Erro;
                }
            }

            #region Conversão de Datas

            if (vm.DataLancamento.HasValue)
                vm.DataLancamentoExibicao = string.Format("{0} {1}", vm.DataLancamento.Value.ToShortDateString(), vm.DataLancamento.Value.ToShortTimeString());

            if (vm.DataReferencia.HasValue)
                vm.DataReferenciaExibicao = string.Format("{0} {1}", vm.DataReferencia.Value.ToShortDateString(), vm.DataReferencia.Value.ToShortTimeString());

            #endregion;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EstoqueSaldoInicialInfo info = new EstoqueSaldoInicialInfo();
            ViewModelToModelMapper.Map<EstoqueSaldoInicialInfo>(vm, info);

            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            info.IdPessoaEmpresaLogada = IdPessoaEmpresaLogada; //Empresa Logada (Empresa Usuária.)

            #region Conversão (Datas)

            if (!string.IsNullOrEmpty(vm.DataReferenciaExibicao))
                info.DataReferencia = DateTime.Parse(vm.DataReferenciaExibicao);

            if (!string.IsNullOrEmpty(vm.DataLancamentoExibicao))
                info.DataLancamento = DateTime.Parse(vm.DataLancamentoExibicao);

            #endregion

            var response = EstoqueSaldoInicialBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdEstoqueSaldoInicial.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEstoqueSaldoInicial, "EstoqueSaldoIncial", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstoqueSaldoInicialInfo info = new EstoqueSaldoInicialInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoaEmpresa;
            info.IdProdutoServico = vm.IdProdutoServico;
            info.IdPessoaRealizacao = vm.IdPessoaResponsavel;
            info.IdProdutoLocal = vm.IdProdutoLocal;

            #region Conversão de Datas

            if (!string.IsNullOrEmpty(vm.DataMovimentoInicio))
                info.DataMovimentacaoInicio = DateTime.Parse(vm.DataMovimentoInicio);

            if (!string.IsNullOrEmpty(vm.DataMovimentoFim))
                info.DataMovimentacaoFim = DateTime.Parse(vm.DataMovimentoFim);

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<EstoqueSaldoInicialInfo> retorno = new ListPaged<EstoqueSaldoInicialInfo>(EstoqueSaldoInicialBll.Instance.ListarPorParametros(info)) { };
            return CreateListResultByStoredProcedure(vm, retorno, "Exportação: Saldo Inicial ");
        }
    }
}
