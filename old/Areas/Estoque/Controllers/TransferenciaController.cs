﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Transferencia;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class TransferenciaController : ControllerExtended
    {

        public TransferenciaController()
        {
            IndexUrl = "Estoque/Transferencia/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit
        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                EstoqueTransferenciaInfo info = new EstoqueTransferenciaInfo();
                info = EstoqueTransferenciaBll.Instance.ListarPorIdCompleto(id.Value);

                #region Conversão

                if (info.DataLancamento.HasValue)
                    vm.DataLancamentoExibicao = string.Format("{0} {1}", info.DataLancamento.Value.ToShortDateString(), info.DataLancamento.Value.ToShortTimeString());

                if (info.DataMovimentacao.HasValue)
                    vm.DataMovimentacaoExibicao = string.Format("{0} {1}", info.DataMovimentacao.Value.ToShortDateString(), info.DataMovimentacao.Value.ToShortTimeString());

                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEstoqueTransferencia, "EstoqueTransferencia");
            }
            else
            {
                vm.DataLancamentoExibicao = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
                vm.DataMovimentacaoExibicao = string.Format("{0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
                vm.IdPessoaResponsavel = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa; //Usuário Logado
                vm.UsuarioLogado = ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.NomeSecundario; //Nome usuário Logado
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EstoqueTransferenciaInfo info = new EstoqueTransferenciaInfo();
            ViewModelToModelMapper.Map<EstoqueTransferenciaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaEmpresaLogada = IdPessoaEmpresaLogada;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataLancamentoExibicao))
                info.DataLancamento = DateTime.Parse(vm.DataLancamentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoExibicao))
                info.DataMovimentacao = DateTime.Parse(vm.DataMovimentacaoExibicao);

            #endregion

            var response = EstoqueTransferenciaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdEstoqueTransferencia.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEstoqueTransferencia, "IdEstoqueTransferencia", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstoqueTransferenciaInfo info = new EstoqueTransferenciaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdEstoqueTransferencia = vm.IdEstoqueTransferencia;
            info.IdProdutoServicoGrupoIn = vm.IdProdutoServicoGrupoIn;
            info.IdProdutoServicoIn = vm.IdProdutoServicoIn;
            info.IdPessoaEmpresaOrigem = vm.IdPessoaEmpresaOrigem;
            info.IdPessoaEmpresaDestino = vm.IdPessoaEmpresaDestino;
            info.IdProdutoLocalOrigem = vm.IdProdutoLocalOrigem;
            info.IdProdutoLocalDestino = vm.IdProdutoLocalDestino;
            info.IdPessoaBuscaIn = vm.IdPessoaBuscaIn;
            info.BuscaAvancada = vm.BuscaAvancada;

            #region Datas

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoInicio))
                info.DataMovimentacaoInicio = DateTime.Parse(vm.DataMovimentacaoInicio);

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoFim))
                info.DataMovimentacaoFim = DateTime.Parse(vm.DataMovimentacaoFim);
            #endregion

            ListPaged<EstoqueTransferenciaInfo> retorno = new ListPaged<EstoqueTransferenciaInfo>(EstoqueTransferenciaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Transferência entre Estoque", true);

        }

    }
}
