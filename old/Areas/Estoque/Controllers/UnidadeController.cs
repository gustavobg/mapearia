﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Unidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class UnidadeController : ControllerExtended
    {
        public UnidadeController()
        {
            IndexUrl = "/Estoque/Unidade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                UnidadeInfo info = new UnidadeInfo();
                info = UnidadeBll.Instance.ListarPorParametros(new UnidadeInfo { IdUnidade = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdUnidade, "Unidade");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            UnidadeInfo info = new UnidadeInfo();
            ViewModelToModelMapper.Map<UnidadeInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = UnidadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdUnidade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdUnidade, "Unidade", response.Response.IdHistorico);

            return Json(response);
        }
        #endregion

        #region Listagem
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UnidadeInfo info = new UnidadeInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdUnidade = vm.IdUnidade;
            info.IdUnidadeIn = vm.IdUnidadeIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.IdUnidadeTipo = vm.IdUnidadeTipo;
            info.IdUnidadeTipoIn = vm.IdUnidadeTipoIn;
            info.IdUnidadeTipoNotIn = vm.IdUnidadeTipoNotIn;
            info.DescricaoSiglaBusca = vm.DescricaoSiglaBusca;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<UnidadeInfo> retorno = new ListPaged<UnidadeInfo>(UnidadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Unidade", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(CalculoVM)), CustomAuthorize]
        [HttpPost]
        public string RealizaConversaoEntreUnidade(CalculoVM vm)
        {
            var response = UnidadeBll.Instance.RealizaConversaoEntreUnidade(vm.idUnidadeAtual.Value, vm.qtdeUnidadeAtual.Value, vm.idUnidadeOriginal.Value, IdEmpresa);

            return response.ToString();

        }

        #endregion

    }
}
