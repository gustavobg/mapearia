using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeMedidor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class UnidadeMedidorController : ControllerExtended
    {
        public UnidadeMedidorController()
        {
            IndexUrl = "/Estoque/UnidadeMedidor/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            UnidadeMedidorInfo info = new UnidadeMedidorInfo();
            ViewModelToModelMapper.Map<UnidadeMedidorInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = UnidadeMedidorBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdUnidadeMedidor.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdUnidadeMedidor, "UnidadeMedidor", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                UnidadeMedidorInfo info = UnidadeMedidorBll.Instance.ListarPorParametros(new UnidadeMedidorInfo { IdUnidadeMedidor = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdUnidadeMedidor, "UnidadeMedidor");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            UnidadeMedidorEmpresaInfo info = new UnidadeMedidorEmpresaInfo();
            UnidadeMedidorEmpresaInfo response = new UnidadeMedidorEmpresaInfo();

            if (id > 0)
            {
                info = UnidadeMedidorEmpresaBll.Instance.ListarPorParametros(new UnidadeMedidorEmpresaInfo { IdUnidadeMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = UnidadeMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdUnidadeMedidor, "UnidadeMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            UnidadeMedidorEmpresaInfo info = new UnidadeMedidorEmpresaInfo();
            UnidadeMedidorEmpresaInfo response = new UnidadeMedidorEmpresaInfo();

            if (id > 0)
            {
                info = UnidadeMedidorEmpresaBll.Instance.ListarPorParametros(new UnidadeMedidorEmpresaInfo { IdUnidadeMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = UnidadeMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdUnidadeMedidor, "UnidadeMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UnidadeMedidorInfo info = new UnidadeMedidorInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.IdUnidade = vm.IdUnidade;
            info.IdUnidadeIn = vm.IdUnidadeIn;
            info.IdUnidadeMedidor = vm.IdUnidadeMedidor;
            info.IdUnidadeMedidorIn = vm.IdUnidadeMedidorIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<UnidadeMedidorInfo> retorno = new ListPaged<UnidadeMedidorInfo>(UnidadeMedidorBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Medidor de Unidade", true);
        }
    }
}
