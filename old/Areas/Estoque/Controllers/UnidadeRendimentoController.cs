﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeRendimento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Bll.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class UnidadeRendimentoController : ControllerExtended
    {
        
        public UnidadeRendimentoController()
        {
            IndexUrl = "Estoque/UnidadeRendimento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                UnidadeRendimentoInfo info = UnidadeRendimentoBll.Instance.ListarPorParametros(new UnidadeRendimentoInfo { IdUnidadeRendimento = id.Value , IdEmpresa = IdEmpresa}).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdUnidadeRendimento, "UnidadeRendimento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            UnidadeRendimentoInfo info = new UnidadeRendimentoInfo();
            ViewModelToModelMapper.Map<UnidadeRendimentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = UnidadeRendimentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdUnidadeRendimento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdUnidadeRendimento, "UnidadeRendimento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            UnidadeRendimentoEmpresaInfo info = new UnidadeRendimentoEmpresaInfo();
            UnidadeRendimentoEmpresaInfo response = new UnidadeRendimentoEmpresaInfo();

            if (id > 0)
            {
                info = UnidadeRendimentoEmpresaBll.Instance.ListarPorParametros(new UnidadeRendimentoEmpresaInfo { IdUnidadeRendimento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = UnidadeRendimentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdUnidadeRendimento, "UnidadeRendimento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            UnidadeRendimentoEmpresaInfo info = new UnidadeRendimentoEmpresaInfo();
            UnidadeRendimentoEmpresaInfo response = new UnidadeRendimentoEmpresaInfo();

            if (id > 0)
            {
                info = UnidadeRendimentoEmpresaBll.Instance.ListarPorParametros(new UnidadeRendimentoEmpresaInfo { IdUnidadeRendimento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = UnidadeRendimentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdUnidadeRendimento, "UnidadeRendimento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UnidadeRendimentoInfo info = new UnidadeRendimentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdUnidadeRendimento = vm.IdUnidadeRendimento;
            info.Descricao = vm.Descricao;
            info.Sigla = vm.Sigla;
            info.DescricaoSiglaBusca = vm.DescricaoSiglaBusca;
            info.Ativo = vm.Ativo;
            info.Producao = vm.Producao;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<UnidadeRendimentoInfo> retorno = new ListPaged<UnidadeRendimentoInfo>(UnidadeRendimentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Unidade Rendimento", true);
        }

    }
}
