﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Unidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Bll.Estoque;
using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.Controllers
{
    public class UnidadeTipoController : ControllerExtended
    {
        public UnidadeTipoController()
        {
            IndexUrl = "/Estoque/UnidadeTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            UnidadeTipoInfo info = new UnidadeTipoInfo();
            ViewModelToModelMapper.Map<UnidadeTipoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = UnidadeTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            SalvarHistoricoAcesso(IndexUrl, vm.IdUnidadeTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdUnidadeTipo, "UnidadeTipo", response.Response.IdHistorico);

            return Json(response);
        }


        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }


        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                UnidadeTipoInfo info = new UnidadeTipoInfo();
                info = UnidadeTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdUnidadeTipo, "UnidadeTipo");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }


        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UnidadeTipoInfo info = new UnidadeTipoInfo();
            info.IdUnidadeTipo = vm.IdUnidadeTipo;
            info.IdUnidadeTipoNotIn = vm.IdUnidadeTipoNotIn;
            info.IdUnidadeTipoIn = vm.IdUnidadeTipoIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<UnidadeTipoInfo> retorno = new ListPaged<UnidadeTipoInfo>(UnidadeTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Unidade", true);
        }

    }
}
