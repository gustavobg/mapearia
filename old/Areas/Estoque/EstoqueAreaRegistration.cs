﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque
{
    public class EstoqueAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Estoque";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Estoque_default",
                "Estoque/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
