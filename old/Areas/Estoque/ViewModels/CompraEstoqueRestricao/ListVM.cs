﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.CompraEstoqueRestricao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdProdutoServicoCompraEstoqueRestricao { get; set; }

        public int? IdPessoa { get; set; }

        public string NomePessoa { get; set; }

        public bool? Ativo { get; set; }
    }
}