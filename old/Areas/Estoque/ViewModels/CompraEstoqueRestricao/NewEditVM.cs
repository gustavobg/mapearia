﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.CompraEstoqueRestricao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            RestricoesSolicitacao = new List<RestricaoCotaocaoSolicitacaoVM>();
            RestricoesCotacaoAprovacao = new List<RestricaoCompraSolicitacaoEstoqueVM>();
            RestricaoRecebimentoProduto = new RestricaoRecebimentoVM();
        }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCompraEstoqueRestricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteSolicitarRequisitar { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteRealizarEntrega { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaAprovadorSolicitacaoCompra { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoasAprovadoresCotacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaAutorizada { get; set; }

        [ViewModelToModelAttribute]
        public string SiglaMoedaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaPerfilPermitido { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<RestricaoCompraSolicitacaoEstoqueVM> RestricoesCotacaoAprovacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<RestricaoCotaocaoSolicitacaoVM> RestricoesSolicitacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public RestricaoRecebimentoVM RestricaoRecebimentoProduto { get; set; }

        public class RestricaoCompraSolicitacaoEstoqueVM
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricaoCompra { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoGrupo { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoCompras { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoMes { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoAno { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCompletaProdutoServicoGrupo { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoComprasExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoMesExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoAnoExibicao { get; set; }
        }
        public class RestricaoCotaocaoSolicitacaoVM
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricaoSolicitacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoGrupo { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoCompras { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoMes { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorMaximoAno { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCompletaProdutoServicoGrupo { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoComprasExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoMesExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMaximoAnoExibicao { get; set; }
        }
        public class RestricaoRecebimentoVM
        {
            public RestricaoRecebimentoVM()
            {
                Divergencias = new List<DivergenciaRecebimentoVM>();
                RestringeSubstituirFornecedor = true;
                RestringeReceberProdutoSemNota = true;
            }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricaoRecebimento { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricao { get; set; }

            [ViewModelToModelAttribute]
            public bool RestringeSubstituirFornecedor { get; set; }

            [ViewModelToModelAttribute]
            public bool RestringeReceberProdutoSemNota { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public List<DivergenciaRecebimentoVM> Divergencias { get; set; }
        }
        public class DivergenciaRecebimentoVM
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricaoRecebimentoDivergencia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoCompraEstoqueRestricaoRecebimento { get; set; }

            [ViewModelToModelAttribute]
            public int? DivergenciaTipo { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PercentualVariacao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoDivergenciaTipo { get; set; }

            [ViewModelToModelAttribute]
            public string PercentualVariacaoExibicao { get; set; }
        }
    }
}