﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Movimento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEstoqueMovimento { get; set; }

        public string IdProdutoServicoIn { get; set; }
        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdProdutoServicoFamiliaIn { get; set; }
        public string IdProdutoLocalIn { get; set; }
        public string IdCentroCustoIn { get; set; }
        public string IdPessoaIn { get; set; }

        public DateTime? DataMovimentacaoInicio { get; set; }
        public DateTime? DataMovimentacaoFim { get; set; }



    }
}