﻿using HTM.MasterGestor.Model.Estoque;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Movimento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdEstoqueMovimento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public EstoqueMovimentoTipo MovimentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaMovimento { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHoraLancamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHoraMovimentacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoLocal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoLote { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? QtdeProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? QtdeProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute]
        public decimal? Valor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Calculado { get; set; }

        #region Datas (Exibição)

        [ViewModelToModelAttribute]
        public string DataMovimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataLancamentoExibicao { get; set; }

        #endregion

        #region Forma de Apresentação

        [ViewModelToModelAttribute]
        public int CasasDecimaisFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public string SiglaProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public int QtdePorEmbalagemProdutoServico { get; set; }

        #endregion

        #region Unidade do Produto 

        [ViewModelToModelAttribute]
        public int CasasDecimaisUnidadeProduto { get; set; }

        [ViewModelToModelAttribute]
        public string SiglaUnidadeProdutoServico { get; set; }

        #endregion

        #region Moeda

        [ViewModelToModelAttribute]
        public int CasasDecimaisFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute]
        public string SiglaMoeda { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public string IdsCentroCustoRestrito { get; set; }

    }
}