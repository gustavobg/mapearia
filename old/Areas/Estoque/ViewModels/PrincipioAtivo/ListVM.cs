﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.PrincipioAtivo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServico { get; set; }
        public int? Natureza { get; set; }

        public string BuscaDetalhada { get; set; }
        public string Codigo { get; set; }
        public string IdProdutoServicoMarcaIn { get; set; }
        public string IdPessoaIn { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}