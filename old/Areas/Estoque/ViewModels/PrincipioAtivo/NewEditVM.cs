﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.PrincipioAtivo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Natureza = 1; //Natureza 1 == Produto | 2 == Serviço | 3 == Princípio Ativo
            ProdutoServicoIndependente = true;
        }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public int Natureza { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoReduzida { get; set; }

        [ViewModelToModelAttribute]
        public int Nomenclatura { get; set; }

        [ViewModelToModelAttribute]
        public int? Quantidade { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }
        
        [ViewModelToModelAttribute]
        public bool ProdutoServicoIndependente { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

    }
}