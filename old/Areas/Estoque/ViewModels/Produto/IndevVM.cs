﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Produto
{
    public class IndexVM : VMIndexBase
    {
        public string DescricaoExibicao { get; set; }
        public IndexVM(string url) : base(url) { }
    }
}