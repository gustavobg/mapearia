﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Produto
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServico { get; set; }
        public int? Natureza { get; set; }

        public string IdProdutoServicoIn { get; set; }
        public string IdProdutoServicoMarcaIn { get; set; }
        public string IdProdutoServicoNotIn { get; set; }
        public string IdProdutoServicoFamiliaIn { get; set; }
        public string BuscaDetalhada { get; set; }
        public string Codigo { get; set; }
        public string IdPessoaIn { get; set; }
        public string DescricaoReduzida { get; set; }
        public string Descricao { get; set; }
        public string IdGrupoInBuscaSeparada { get; set; }
        public string DescricaoReduzidaCodigoBusca { get; set; }
        public string DescricaoCodigoBusca { get; set; }

        public bool? PermiteProducao { get; set; }
        public bool? PermiteVenda { get; set; }
        public bool? VendaOuProducao { get; set; }
        public bool? PermiteCompra { get; set; }
        public bool? ComparOuProduzir { get; set; }
        public bool? PermiteUso { get; set; }
        public bool? Armazenamento { get; set; }

        public bool? Ativo { get; set; }

        public bool? ComRestricaoCompraRequisicaoEstoque { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}