﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Produto
{
    public class NewEditDadosBasicosVM : VMNewEditBase
    {
        public NewEditDadosBasicosVM()
        {
            ProdutoServicoIndependente = true;
            lstUnidadeComercial = new List<ProdutoServicoUnidadeComercialVM>();
        }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoMarca { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoFamilia { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int Natureza { get; set; }

        [ViewModelToModelAttribute]
        public String Codigo { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public String DescricaoReduzida { get; set; }

        [ViewModelToModelAttribute]
        public String Modelo { get; set; }

        [ViewModelToModelAttribute]
        public Int32 Nomenclatura { get; set; }

        [ViewModelToModelAttribute]
        public Int32? Quantidade { get; set; }

        [ViewModelToModelAttribute]
        public bool ProdutoServicoIndependente { get; set; }

        [ViewModelToModelAttribute]
        public String Observacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCaracterizacaoClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCaracterizacaoSecao { get; set; }

        [ViewModelToModelAttribute]
        public string Caracteristicas { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoEAN { get; set; }
        
        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFiscalClassificacao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ProdutoServicoUnidadeComercialVM> lstUnidadeComercial { get; set; }

        public class ProdutoServicoUnidadeComercialVM
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoUnidadeComercial { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public decimal? FatorConversao { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteFracionar { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeProdutoServicoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeTipoProdutoServicoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoUnidade { get; set; }

            [ViewModelToModelAttribute]
            public string FatorConversaoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }
        }
    }
}