﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoLocal
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoLocal { get; set; }
        public int? IdEmpresa { get; set; }
        
        /// <summary>
        /// Utilizado para busca de Locais por Empresa
        /// </summary>
        public int? IdPessoa { get; set; }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public string BuscaAvancada { get; set; }
        public string IdProdutoLocalIn { get; set; }
        public string IdProdutoLocalNotIn { get; set; }

        public bool? SemItensRelacionadosComMapeamento { get; set; }
        public bool? Ativo { get; set; }
    }
}