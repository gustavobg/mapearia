﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoLocal
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdProdutoLocal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public bool LocalMovel { get; set; }

        [ViewModelToModelAttribute]
        public bool LocalPertenceTerceiro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoa { get; set; }
    }
}