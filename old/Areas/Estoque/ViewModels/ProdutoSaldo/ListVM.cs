﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoSaldo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        /// <summary>
        /// Empresa Usuária
        /// </summary>
        public int? IdPessoa { get; set; }

        /// <summary>
        /// Identificador relacional com a tabela ProdutoServico
        /// </summary>
        public int? IdProdutoServico { get; set; }

        /// <summary>
        /// Identificador relacional com a tabela ProdutoServicoLote
        /// </summary>
        public int? IdProdutoServicoLote { get; set; }

        /// <summary>
        /// Identificador relacional com a tabela ProdutoLocal
        /// </summary>
        public int? IdProdutoLocal { get; set; }

    }
}