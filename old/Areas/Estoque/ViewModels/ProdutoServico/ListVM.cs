﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServico { get; set; }
        public int? Natureza{ get; set; }

        public string BuscaDetalhada { get; set; }
        public string Codigo { get; set; }
        public string IdProdutoServicoNotIn { get; set; }
        public string IdProdutoServicoMarcaIn { get; set; }
        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdPessoaIn { get; set; }
        public string DescricaoReduzida { get; set; }
        public string IdTipoInBuscaSeparada { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComPrincipioAtivo { get; set; }
    }
}