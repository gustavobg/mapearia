﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Corporativo.ViewModels.Pessoa;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServico
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Locais = new List<VMProdutoServicoProdutoLocal>();
            Fornecedores = new List<VMProdutoServicoFornecedor>();
            Composicao = new List<VMProdutoServicoComposicao>();
            FormasApresentacoes = new List<VMProdutoServicoFormaApresentacaoItem>();
            Pessoa = new NewVM();
            Natureza = 1; //Natureza 1 == Produto | 2 == Serviço | 3 == Principio Ativo
        }

        public NewVM Pessoa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoMarca { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoFormaApresentacao { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdProdutoServicoFamilia { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidadeTipo { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidade { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidadePadrao { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdUnidadeTipoPadrao { get; set; }

        [ViewModelToModelAttribute]
        public int Natureza { get; set; }

        [ViewModelToModelAttribute]
        public String Codigo { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public String DescricaoReduzida { get; set; }

        [ViewModelToModelAttribute]
        public String Modelo { get; set; }

        [ViewModelToModelAttribute]
        public Int32 Nomenclatura { get; set; }

        [ViewModelToModelAttribute]
        public Boolean PermiteCompra { get; set; }

        [ViewModelToModelAttribute]
        public Boolean PermiteVenda { get; set; }

        [ViewModelToModelAttribute]
        public Boolean PermiteUso { get; set; }

        [ViewModelToModelAttribute]
        public Boolean PermiteProducao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean PermiteControle { get; set; }

        [ViewModelToModelAttribute]
        public Int32? Quantidade { get; set; }

        [ViewModelToModelAttribute]
        public String Observacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoEAN { get; set; }

        [ViewModelToModelAttribute]
        public bool Armazenamento { get; set; }

        [ViewModelToModelAttribute]
        public bool PermitePesagem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCaracterizacaoClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCaracterizacaoSecao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorPesoBruto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePesoBruto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorPesoBrutoMetro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePesoBrutoMetro { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorPesoLiquido { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadePesoLiquido { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorVolume { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeVolume { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorLargura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeLargura { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorComprimento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeComprimento { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAltura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeAltura { get; set; }

        [ViewModelToModelAttribute]
        public bool ProdutoServicoIndependente { get; set; }

        [ViewModelToModelAttribute]
        public int? ControleLoteRecebimentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? ControleLoteProducaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool? ProdutoIntermediario { get; set; }

        [ViewModelToModelAttribute]
        public bool? MateriaPrima { get; set; }

        [ViewModelToModelAttribute]
        public bool? ProdutoAcabado { get; set; }

        [ViewModelToModelAttribute]
        public bool SolicitaCompraAutomatica { get; set; }

        [ViewModelToModelAttribute]
        public string Caracteristicas { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<VMProdutoServicoProdutoLocal> Locais { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<VMProdutoServicoFornecedor> Fornecedores { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<VMProdutoServicoComposicao> Composicao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<VMProdutoServicoFormaApresentacaoItem> FormasApresentacoes { get; set; }

        public class VMProdutoServicoProdutoLocal
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? QuantidadeMinima { get; set; }

            [ViewModelToModelAttribute]
            public int? QuantidadeIdeal { get; set; }

            [ViewModelToModelAttribute]
            public string Detalhe { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeQuantidadeIdeal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeQuantidadeMinima { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoQuantidadeMinima { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoQuantidadeIdeal { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaQuantidadeIdeal { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaQuantidadeMinima { get; set; }

        }
        public class VMProdutoServicoFornecedor
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; }

            [ViewModelToModelAttribute]
            public string PessoaNome { get; set; }

            [ViewModelToModelAttribute]
            public string PessoaNomeSecundario { get; set; }

        }
        public class VMProdutoServicoComposicao
        {
            //[ViewModelToModelAttribute]
            //public int? IdProdutoServicoComposicao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Valor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeValor { get; set; }

            public int? IdProdutoServicoUnidadeTipo { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorVariacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeValorVariacao { get; set; }

            [ViewModelToModelAttribute]
            public bool? PermiteAlteracaoQuantidade { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServicoReferencia { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }

            //Utilizado Apenas para buscar as unidades, do produto em questão e a Porcentagem [Quando o mesmo aceitar variação]
            [ViewModelToModel]
            public string IdUnidadeIn { get; set; }

            [ViewModelToModel]
            public string ValorUnidade { get; set; }
        }
        public class VMProdutoServicoFormaApresentacaoItem
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacaoItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Valor { get; set; }

            [ViewModelToModelAttribute]
            public int IdUnidade { get; set; }

            [ViewModelToModelAttribute]
            public int IdUnidadeTipo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorUnidadeSigla { get; set; }
        }

    }
}
