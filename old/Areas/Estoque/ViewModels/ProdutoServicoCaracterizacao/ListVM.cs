﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoCaracterizacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }


        public int? IdProdutoServicoCaracterizacao { get; set; }
        public int? IdProdutoServicoGrupo { get; set; }
        public int? NaturezaCaracterizacao { get; set; }

        public string Descricao { get; set; }
        public string IdsProdutoServicoGrupo { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}