﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoCaracterizacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoCaracterizacao { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }
        
        [ViewModelToModelAttribute]
        public string Descricao { get; set; }
        
        [ViewModelToModelAttribute]
        public int? NaturezaCaracterizacao { get; set; }
        
        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProdutoServicoGrupo{ get; set; }
        
        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}