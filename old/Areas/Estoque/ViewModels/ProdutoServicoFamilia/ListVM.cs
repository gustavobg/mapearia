﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFamilia
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServicoFamilia { get; set; }

        public string IdProdutoServicoFamiliaIn { get; set; }
        public string Descricao { get; set; }
        public string IdUnidadeIn { get; set; }
        public string IdUnidadeTipoIn { get; set; }
        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdProdutoServicoGrupoInBuscaEmProfundidade { get; set; }

        public bool? Ativo { get; set; }

    }
}