﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFormaApresentacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServicoFormaApresentacao { get; set; }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public string BuscaDetalhada { get; set; }

        public bool? Ativo { get; set; }
        public bool? ControlaMovimentacao { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
