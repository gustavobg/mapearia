﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoFornecedor
{
    public class ListVM : ViewModelListRequestBase
    {
        public int? IdPessoa { get; set; }

        public string BuscaPessoaDetalhada { get; set; }
        public string IdPessoaIn { get; set; }
        public string IdPessoaNotIn { get; set; }
    }
}