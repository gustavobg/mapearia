﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoGrupo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdProdutoServicoGrupo { get; set; }
        public int? IdProdutoServicoGrupoPai { get; set; }
        public int? IdGrupoExcecao { get; set; }
        public int? Natureza { get; set; }

        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdProdutoServicoGrupoNotIn { get; set; }
        public string IdProdutoServicoGrupoPaiIn { get; set; }
        public string NaturezaIn { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
        public bool? ComPai { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? SemRelacionamentoComProduto { get; set; }
    }
}