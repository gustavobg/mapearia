﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoGrupo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() {

            Natureza = 1; //Apenas para "Setar" o Produto como primeiro item na lista de botões
        }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoGrupoPai { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}