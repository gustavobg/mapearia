﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoOpcaoRegra
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
            
        }

        public int? IdProdutoServicoOpcaoRegra { get; set; }
        public int? Ordem { get; set; }

        public string IdProdutoServicoOpcaoRegraIn { get; set; }
        public string Descricao { get; set; }
        public string Observacao { get; set; }
        
        public bool? Ativo { get; set; }
    }
}