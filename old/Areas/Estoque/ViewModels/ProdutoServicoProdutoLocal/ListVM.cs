﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.ProdutoServicoProdutoLocal
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdProdutoServico { get; set; }
        public int? IdProdutoLocal { get; set; }

        public string IdProdutoServicoIn { get; set; }
        public string ProdutoServicoDescricao { get; set; }
        public string ProdutoLocalDescricao { get; set; }

        public bool? Ativo { get; set; }
    }
}