﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Requisicao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdEstoqueRequisicao { get; set; }
        public int? IdProdutoServicoGrupo { get; set; }
        public int? IdProdutoServico { get; set; }
        public int? IdPessoaRequisitante { get; set; }
        public int? IdPessoaResponsavel { get; set; }
        public int? IdProcessoSituacao { get; set; }
        public int? IdAplicacaoTipo { get; set; }
        public int? IdAplicacao { get; set; }

        public string IdEstoqueRequisicaoNotIn { get; set; }
        public string IdProcessoSituacaoIn { get; set; }
        public string DataRequisicaoInicio { get; set; }
        public string DataRequisicaoFim { get; set; }
        public string BuscaAvancada { get; set; }
    }
}