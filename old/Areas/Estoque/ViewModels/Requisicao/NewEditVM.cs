﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Requisicao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstItem = new List<RequisicaoItemVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdEstoqueRequisicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEstoqueRequisicaoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdSubProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataRequisicao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataMovimentacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaRequisitante { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaResponsavel { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdObjeto { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
        
        [ViewModelToModelAttribute(ComplexType = true)]
        public List<RequisicaoItemVM> lstItem { get; set; }

        #region Propriedades de visualização

        [ViewModelToModelAttribute]
        public string UsuarioLogado { get; set; }

        [ViewModelToModelAttribute]
        public string DataRequisicaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataMovimentacaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoProcessoSituacao { get; set; }

        #endregion

        #region Propriedades Especiais para regras de Negócio

        [ViewModelToModelAttribute]
        public string IdsPessoaNotIn_RestricaoEntrega { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaNotIn_RestricaoRequisicao { get; set; }

        #endregion

        public class RequisicaoItemVM
        {
            public RequisicaoItemVM()
            {
                lstEntregaRealizada = new List<EstoqueRequisicaoEntregaVM>();
                Excluido = false;
            }

            [ViewModelToModelAttribute]
            public int? IdEstoqueRequisicaoItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstoqueRequisicao { get; set; }

            [ViewModelToModelAttribute]
            public int? Numero { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeRealProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? QtdeProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int QtdePorEmbalagemProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string IdsSubAplicacao { get; set; }

            [ViewModelToModelAttribute]
            public string IdsOrdemEtapa { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisUnidadeProduto { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisFormaApresentacao { get; set; }

            #region Propriedades de exibição

            [ViewModelToModelAttribute]
            public string CodigoDescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoLocal { get; set; }
            
            [ViewModelToModelAttribute]
            public string SiglaProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaUnidadeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string Requisitado { get; set; }

            [ViewModelToModelAttribute]
            public string Entregue { get; set; }
            
            [ViewModelToModelAttribute]
            public string Residual { get; set; }

            [ViewModelToModelAttribute]
            public string Situacao { get; set; }

            #endregion

            [ViewModelToModelAttribute]
            public string ApresentacaoSaldo { get; set; }

            [ViewModelToModelAttribute]
            public bool EditarRegistro { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public List<EstoqueRequisicaoEntregaVM> lstEntregaRealizada { get; set; }

        }
        public class EstoqueRequisicaoEntregaVM
        {
            public EstoqueRequisicaoEntregaVM()
            {
                Excluido = false;
            }

            [ViewModelToModelAttribute]
            public int? IdEstoqueRequisicaoEntrega { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstoqueRequisicaoItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoLote { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public DateTime? DataHora { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServicoLote { get; set; }

            [ViewModelToModelAttribute]
            public string DataHoraExibicao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoaEntrega { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoaEntrega { get; set; }

            #region Propriedades Especiais

            [ViewModelToModelAttribute]
            public bool EditarRegistro { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            [ViewModelToModelAttribute]
            public string QtdeDescricaoProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string QtdeDescricaoProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public string Entregue { get; set; }

            [ViewModelToModelAttribute]
            public string ExibicaoEstoqueRequisicaoItem { get; set; }

            #endregion

        }
    }
}