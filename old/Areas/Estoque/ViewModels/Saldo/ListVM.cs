﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Saldo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }
        public int? Visao { get; set; }


        public string BuscaAvancada { get; set; }

    }
}