﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Saldo
{
    public class SaldoVM : VMNewEditBase
    {
        public SaldoVM()
        {
            lstItem = new List<SaldoItem>();
        }

        [ViewModelToModelAttribute]
        public string TipoDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string Header { get; set; }

        [ViewModelToModelAttribute]
        public string ProdutoOuFamilia { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<SaldoItem> lstItem { get; set; }

        public class SaldoItem
        {
            public SaldoItem()
            {

            }

            [ViewModelToModelAttribute]
            public string ProdutoOuFamilia { get; set; }

            [ViewModelToModelAttribute]

            public string ProdutoLocalDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string EmpresaUsuaria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFamilia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoLocal { get; set; }

            #region Forma de Apresentação 

            [ViewModelToModelAttribute]
            public string SiglaProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public decimal QtdeProdutoServicoFormaApresentacao { get; set; }

            #endregion

            #region Quantidade Produto (Unidade)

            [ViewModelToModelAttribute]
            public string Sigla { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimais { get; set; }

            [ViewModelToModelAttribute]
            public decimal? QtdeUnidadeProdutoServico { get; set; }

            #endregion

            #region Valores

            [ViewModelToModelAttribute]
            public string SiglaFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorUnitario { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorTotal { get; set; }

            #endregion


        }
    }
}