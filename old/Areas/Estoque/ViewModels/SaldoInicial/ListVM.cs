﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.SaldoInicial
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEstoqueSaldoInicial { get; set; }
        public int? IdPessoaEmpresa { get; set; }
        public int? IdPessoaResponsavel { get; set; }
        public int? IdProdutoServico { get; set; }
        public int? IdProdutoLocal { get; set; }
        public int? IdProdutoServicoGrupo { get; set; }


        public string DataMovimentoInicio { get; set; }
        public string DataMovimentoFim { get; set; }

    }
}