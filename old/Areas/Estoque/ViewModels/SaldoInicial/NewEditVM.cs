﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.SaldoInicial
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstItem = new List<ItemVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdEstoqueSaldoInicial { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaRealizacao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataReferencia { get; set; }
        [ViewModelToModelAttribute]
        public DateTime? DataLancamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public int IdFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemVM> lstItem { get; set; }

        #region Data

        [ViewModelToModelAttribute]
        public string DataReferenciaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataLancamentoExibicao { get; set; }

        #endregion

        public class ItemVM
        {
            [ViewModelToModelAttribute]
            public int? IdEstoqueSaldoInicialItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstoqueSaldoInicial { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? QtdeProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public decimal? QtdeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorUnitario { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorTotal { get; set; }

            #region Propriedades para Grid View

            [ViewModelToModelAttribute]
            public string DescricaoProdutoLocal { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoProdutoServico { get; set; }
            
            #region Forma de Apresentação

            [ViewModelToModelAttribute]
            public int CasasDecimaisFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int QtdePorEmbalagemProdutoServico { get; set; }

            #endregion

            #region Unidade do Produto 

            [ViewModelToModelAttribute]
            public int CasasDecimaisUnidadeProduto { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaUnidadeProdutoServico { get; set; }

            #endregion

            #region Moeda

            [ViewModelToModelAttribute]
            public int CasasDecimaisFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaMoeda { get; set; }

            #endregion

            #endregion

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

        }
    }
}