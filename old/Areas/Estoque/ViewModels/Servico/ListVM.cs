﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Servico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdProdutoServico { get; set; }
        public int? Natureza { get; set; }

        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdProdutoServicoNotIn { get; set; }
        public string IdProdutoServicoIn { get; set; }
        public string BuscaDetalhada { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        
        public bool? Ativo { get; set; }
        public bool? VendaOuProducao { get; set; }
        public bool? Mecanizado { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}