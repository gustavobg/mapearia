﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Servico
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Impostos = new List<VMProdutoServicoImposto>();
        }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServicoGrupo { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteCompra { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteVenda { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteProducao { get; set; }

        [ViewModelToModelAttribute]
        public bool RealizadoComEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public bool InformarOperador { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<VMProdutoServicoImposto> Impostos { get; set; }

        public class VMProdutoServicoImposto
        {
            [ViewModelToModelAttribute]
            public int? IdProdutoServicoImposto { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFiscalImposto { get; set; }

            [ViewModelToModelAttribute]
            public bool IncideSaida { get; set; }

            [ViewModelToModelAttribute]
            public bool RecuperaEntrada { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ReducaoBase { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ReducaoImposto { get; set; }

            [ViewModelToModelAttribute]
            public string ReducaoBaseDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string ReducaoImpostoDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFiscalImposto { get; set; }
        }
    }
}