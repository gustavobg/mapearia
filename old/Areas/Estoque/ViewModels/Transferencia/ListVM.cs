﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Transferencia
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEstoqueTransferencia { get; set; }

        public string IdProdutoServicoIn { get; set; }
        public string IdProdutoServicoGrupoIn { get; set; }
        public string IdPessoaBuscaIn { get; set; }
        public string DataMovimentacaoInicio { get; set; }
        public string DataMovimentacaoFim { get; set; }
        public string BuscaAvancada { get; set; }


        public int? IdPessoaEmpresaOrigem { get; set; }
        public int? IdPessoaEmpresaDestino { get; set; }
        public int? IdProdutoLocalOrigem { get; set; }
        public int? IdProdutoLocalDestino { get; set; }
        
    }
}