﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Transferencia
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstItem = new List<TransferenciaItemVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdEstoqueTransferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataLancamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataMovimentacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaRequisitante { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaResponsavel { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmpresaOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoLocalOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmpresaDestino { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoLocalDestino { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        #region Propriedades para Exibição e Conversão de Datas

        [ViewModelToModelAttribute]
        public string DataLancamentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataMovimentacaoExibicao { get; set; }

        #endregion

        #region Propriedades Especiais e Descrições

        [ViewModelToModelAttribute]
        public string UsuarioLogado { get; set; }

        #endregion

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<TransferenciaItemVM> lstItem { get; set; }

        public class TransferenciaItemVM
        {
            [ViewModelToModelAttribute]
            public int? IdEstoqueTransferenciaItem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEstoqueTransferencia { get; set; }

            [ViewModelToModelAttribute]
            public int? Numero { get; set; }
            
            [ViewModelToModelAttribute]
            public int? IdProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoLote { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProdutoServicoFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public int? QtdeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdUnidadeProdutoServico { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            #region Descrições e propriedades para Exibição

            [ViewModelToModelAttribute]
            public string ProdutoServicoDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string ProdutoServicoDescricaoLote { get; set; }

            #region Apresentação

            [ViewModelToModelAttribute]
            public string ProdutoServicoFormaApresentacaoDescricao { get; set; }

            [ViewModelToModelAttribute]
            public int CasasDecimaisFormaApresentacao { get; set; }

            [ViewModelToModelAttribute]
            public string ProdutoServicoFormaApresentacaoSigla { get; set; }

            [ViewModelToModelAttribute]
            public string QtdePorEmbalagemProdutoServico { get; set; }

            #endregion

            #region Unidade

            [ViewModelToModelAttribute]
            public int CasasDecimaisUnidadeProduto { get; set; }

            [ViewModelToModelAttribute]
            public string UnidadeProdutoServicoSigla { get; set; }

            #endregion

            #endregion
        }

    }
}