﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Unidade
{
    public class CalculoVM: ViewModelListRequestBase
    {
        public CalculoVM()
        {

        }

        public int? idUnidadeAtual { get; set; }

        public int? idUnidadeOriginal { get; set; }

        public decimal? qtdeUnidadeAtual { get; set; }
    }
}