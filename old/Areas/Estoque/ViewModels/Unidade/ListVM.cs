﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.Unidade
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdUnidade { get; set; }
        public int? IdUnidadeTipo { get; set; }
        public int? CasasDecimais { get; set; }

        public string Descricao { get; set; }
        public string IdUnidadeTipoIn { get; set; }
        public string IdUnidadeTipoNotIn { get; set; }
        public string DescricaoSiglaBusca { get; set; }
        public string IdUnidadeIn { get; set; }

        public bool? Ativo { get; set; }
        public bool ComPorcentagem { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}
