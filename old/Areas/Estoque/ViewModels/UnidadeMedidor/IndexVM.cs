using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeMedidor
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) 
        { 
        }
    }
}