using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeMedidor
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
		{ 
		}

        public int? IdUnidadeMedidor { get; set; }
        public int? IdUnidade { get; set; }

        public string IdUnidadeIn { get; set; }
        public string IdUnidadeMedidorIn { get; set; }
        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public string DescricaoSiglaBusca { get; set; }

        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}