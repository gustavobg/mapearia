using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeMedidor
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
		{
		}

		[ViewModelToModelAttribute]
		public int? IdUnidadeMedidor { get; set; }

        [ViewModelToModelAttribute]
		public int? IdUnidade { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid ChaveVersao { get; set; }
    }
}