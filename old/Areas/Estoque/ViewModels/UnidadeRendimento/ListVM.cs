﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeRendimento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdUnidadeRendimento { get; set; }

        public string Descricao { get; set; }
        public string Sigla { get; set; }
        public string DescricaoSiglaBusca { get; set; }

        public bool? Ativo { get; set; }
        public bool? Producao { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}