﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Estoque.ViewModels.UnidadeRendimento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdUnidadeRendimento { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get;set;}

        [ViewModelToModelAttribute]
        public string Formula { get; set; }

        [ViewModelToModelAttribute]
        public int? CasasDecimais { get; set; }

        [ViewModelToModelAttribute]
        public bool MaoDeObra { get; set; }

        [ViewModelToModelAttribute]
        public bool Equipamento { get; set; }

        [ViewModelToModelAttribute]
        public bool Insumo { get; set; }

        [ViewModelToModelAttribute]
        public bool Producao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}