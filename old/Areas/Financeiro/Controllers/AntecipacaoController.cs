﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Antecipacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class AntecipacaoController : ControllerExtended
    {
        public AntecipacaoController()
        {
            IndexUrl = "/Financeiro/Antecipacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroAntecipacaoInfo info = new FinanceiroAntecipacaoInfo();
            ViewModelToModelMapper.Map<FinanceiroAntecipacaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões

            if (!string.IsNullOrEmpty(vm.DataPrevistaExibicao))
                info.DataPrevista = Convert.ToDateTime(vm.DataPrevistaExibicao);

            #endregion

            var response = FinanceiroAntecipacaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroAntecipacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroAntecipacao, "FinanceiroAntecipacao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroAntecipacaoInfo info = new FinanceiroAntecipacaoInfo();

            if (id.HasValue && id > 0)
            {
                info = FinanceiroAntecipacaoBll.Instance.ListarPorIdFinanceiroAntecipacaoCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroAntecipacao, "FinanceiroAntecipacao");

            }
            else
            {
                info = FinanceiroAntecipacaoBll.Instance.PreparaNovoRegistro(info);

                info.IdFinanceiroMoedaValorAtual = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda;
                info.IdFinanceiroMoedaAntecipacao = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda;

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
            }

            #region Conversões

            if (info.DataPrevista.HasValue)
                vm.DataPrevistaExibicao = info.DataPrevista.Value.ToShortDateString();

            #endregion

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroAntecipacaoInfo info = new FinanceiroAntecipacaoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroAntecipacao = vm.IdFinanceiroAntecipacao;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.Titulo = vm.Titulo;
            info.IdFinanceiroFormaPagamentoIn = vm.IdFinanceiroFormaPagamentoIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdProcessoSituacaoIn = vm.IdProcessoSituacaoIn;

            #region Tipo de Movimento
            //TipoMovimentoFinanceiro == 1 A Pagar
            if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 1)
                if (string.IsNullOrEmpty(info.IdProcessoSituacaoIn)) // caso o usuário tenha informado alguma Situação, Não será necessário informar as situações via server
                    info.IdProcessoSituacaoIn = "2,3,4,5,10,17";

            //TipoMovimentoFinanceiro == 2 A Receber
            if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 2)
                if (string.IsNullOrEmpty(info.IdProcessoSituacaoIn)) // caso o usuário tenha informado alguma Situação, Não será necessário informar as situações via server
                    info.IdProcessoSituacaoIn = "7,8,9,11,18";

            #endregion

            #region Datas Pagamento (Conversão)

            if (!string.IsNullOrEmpty(vm.DataPrevistaInicio))
                info.DataPrevistaInicio = DateTime.Parse(vm.DataPrevistaInicio);

            if (!string.IsNullOrEmpty(vm.DataPrevistaFim))
                info.DataPrevistaFim = DateTime.Parse(vm.DataPrevistaFim);

            if (!string.IsNullOrEmpty(vm.DataEmissaoInicio))
                info.DataEmissaoInicio = DateTime.Parse(vm.DataEmissaoInicio);

            if (!string.IsNullOrEmpty(vm.DataEmissaoFim))
                info.DataEmissaoFim = DateTime.Parse(vm.DataEmissaoFim);

            #endregion
            
            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<FinanceiroAntecipacaoInfo> retorno = new ListPaged<FinanceiroAntecipacaoInfo>(FinanceiroAntecipacaoBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Antecipação Financerira");
        }

    }
}
