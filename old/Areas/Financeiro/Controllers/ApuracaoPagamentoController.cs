﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ApuracaoPagamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ApuracaoPagamentoController : ControllerExtended
    {
        public ApuracaoPagamentoController()
        {
            IndexUrl = "/Financeiro/ApuracaoPagamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroApuracaoPagamentoInfo info = new FinanceiroApuracaoPagamentoInfo();
            ViewModelToModelMapper.Map<FinanceiroApuracaoPagamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaEmpresaLogada = IdPessoaEmpresaLogada;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.DataPagamentoExibicao))
                info.DataPagamento = Convert.ToDateTime(vm.DataPagamentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataInicioExibicao))
                info.DataInicio = Convert.ToDateTime(vm.DataInicioExibicao);

            if (!string.IsNullOrEmpty(vm.DataFimExibicao))
                info.DataFim = Convert.ToDateTime(vm.DataFimExibicao);
            #endregion

            var response = FinanceiroApuracaoPagamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroApuracaoPagamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroApuracaoPagamento, "FinanceiroApuracaoPagamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroApuracaoPagamentoInfo info = new FinanceiroApuracaoPagamentoInfo();
                info = FinanceiroApuracaoPagamentoBll.Instance.ListarPorIdFinanceiroApuracaoPagamento(id.Value);

                #region Conversão
                if (info.DataPagamento.HasValue)
                    vm.DataPagamentoExibicao = info.DataPagamento.Value.ToShortDateString();

                if (info.DataInicio.HasValue)
                    vm.DataInicioExibicao = info.DataInicio.Value.ToShortDateString();

                if (info.DataFim.HasValue)
                    vm.DataFimExibicao = info.DataFim.Value.ToShortDateString();
                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroApuracaoPagamento, "FinanceiroApuracaoPagamento");
            }
            else
            {
                vm.DataPagamentoExibicao = DateTime.Now.ToShortDateString();
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroApuracaoPagamentoInfo info = new FinanceiroApuracaoPagamentoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroModeloApuracaoPagamento = vm.IdFinanceiroModeloApuracaoPagamento;
            info.IdFinanceiroModeloApuracaoPagamentoIn = vm.IdFinanceiroModeloApuracaoPagamentoIn;
            info.IdPessoaIn = vm.IdPessoaIn;

            #region Conversões

            if (!string.IsNullOrEmpty(vm.DataInicio))
                info.DataInicio = Convert.ToDateTime(vm.DataInicio);

            if (!string.IsNullOrEmpty(vm.DataFim))
                info.DataFim = Convert.ToDateTime(vm.DataFim);

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            ListPaged<FinanceiroApuracaoPagamentoInfo> retorno = new ListPaged<FinanceiroApuracaoPagamentoInfo>(FinanceiroApuracaoPagamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Apuração de Pagamento");
        }

    }
}
