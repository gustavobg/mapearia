



using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Classificacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ClassificacaoController : ControllerExtended
    {
        public ClassificacaoController()
        {
            IndexUrl = "/Financeiro/Classificacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit
        
        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit()
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FinanceiroClassificacaoInfo info = FinanceiroClassificacaoBll.Instance.ListarPorIdCompleto( id.Value, IdEmpresa );
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroClassificacao, "FinanceiroClassificacao");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroClassificacaoInfo info = new FinanceiroClassificacaoInfo();

            ViewModelToModelMapper.Map<FinanceiroClassificacaoInfo>(vm, info);
            info.Codigo = vm.Codigo.Replace('_', '0');

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsPessoasRestritas))
            {
                info.EmpresasUsuariasRestritas = new List<FinanceiroClassificacaoPessoaRestricaoInfo>();

                CommaSeparatedToList(vm.IdsPessoasRestritas).ForEach(idPessoa =>
                {
                    FinanceiroClassificacaoPessoaRestricaoInfo x = new FinanceiroClassificacaoPessoaRestricaoInfo();
                    x.IdPessoa = idPessoa;

                    info.EmpresasUsuariasRestritas.Add(x);
                });
            }

            var response = FinanceiroClassificacaoBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
            {
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroClassificacao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFinanceiroClassificacao, "ClassificacaoFinanceira", info.Response.IdHistorico);

                if (vm.SaveNewCrud)
                {
                    vm = new NewEditVM();
                    vm.Response = response.Response;

                    vm.SaveNewCrud = true;
                    vm.Ativo = true;
                    vm.IdPlanoContasVersao = info.IdPlanoContasVersao;

                    return Json(vm);
                }
                return Json(response);
            }
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroClassificacaoEmpresaInfo info = new FinanceiroClassificacaoEmpresaInfo();
            FinanceiroClassificacaoEmpresaInfo response = new FinanceiroClassificacaoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroClassificacaoEmpresaBll.Instance.ListarPorParametros(new FinanceiroClassificacaoEmpresaInfo { IdFinanceiroClassificacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroClassificacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroClassificacao, "FinanceiroClassificacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroClassificacaoEmpresaInfo info = new FinanceiroClassificacaoEmpresaInfo();
            FinanceiroClassificacaoEmpresaInfo response = new FinanceiroClassificacaoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroClassificacaoEmpresaBll.Instance.ListarPorParametros(new FinanceiroClassificacaoEmpresaInfo { IdFinanceiroClassificacao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroClassificacaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroClassificacao, "FinanceiroClassificacao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroClassificacaoInfo info = new FinanceiroClassificacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.Codigo = vm.Codigo;
            info.UltimoNivel = vm.UltimoNivel;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.IdFinanceiroClassificacao = vm.IdFinanceiroClassificacao;
            info.IdFinanceiroClassificacaoPai = vm.IdFinanceiroClassificacaoPai;
            info.IdFinanceiroClassificacaoIn = vm.IdFinanceiroClassificacaoIn;
            info.IdFinanceiroClassificacaoNotIn = vm.IdFinanceiroClassificacaoNotIn;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroClassificacaoInfo> retorno = new ListPaged<FinanceiroClassificacaoInfo>(FinanceiroClassificacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Classificação Financeira ", true);

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListarHierarquicamentePorParametro(ListVM vm)
        {
            FinanceiroClassificacaoInfo info = new FinanceiroClassificacaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Descricao = vm.Descricao;
            info.Codigo = vm.Codigo;
            info.UltimoNivel = vm.UltimoNivel;
            info.DescricaoCompleta = vm.DescricaoCompleta;
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;
            info.IdFinanceiroClassificacao = vm.IdFinanceiroClassificacao;
            info.IdFinanceiroClassificacaoIn = vm.IdFinanceiroClassificacaoIn;
            info.IdFinanceiroClassificacaoPai = vm.IdFinanceiroClassificacaoPai;
            info.IdFinanceiroClassificacaoIn = vm.IdFinanceiroClassificacaoIn;
            info.IdFinanceiroClassificacaoNotIn = vm.IdFinanceiroClassificacaoNotIn;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;
            //info.IdFinanceiroClassificacaoPaiIn = vm.IdFinanceiroClassificacaoPaiIn;

            ListPaged<FinanceiroClassificacaoInfo> retorno = new ListPaged<FinanceiroClassificacaoInfo>(FinanceiroClassificacaoBll.Instance.ListarHierarquicamentePorParametro(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage.HasValue ? vm.Page.CurrentPage.Value : 1,
                    PageSize = vm.Page.PageSize.HasValue ? vm.Page.PageSize.Value : 10,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:Classificação Financeira ", true);

        }

        public string RetornaContaSuperior(string codigo)
        {
            if (!string.IsNullOrEmpty(codigo))
            {
                if (codigo.Contains("_"))
                {
                    string contatemp = codigo;
                    codigo = string.Empty;
                    codigo = contatemp.Replace("_", "0");
                }
                string contaSuperior = string.Empty;
                string contaComposicao = string.Empty;
                var contaArray = codigo.Split('.');
                string contaSanada = string.Empty;
                string contaSuperiorRetorno = string.Empty;

                int quantidade = 0;
                string zeros = string.Empty;
                foreach (var item in contaArray)
                {
                    quantidade = item.Length;
                    zeros = zeros.PadLeft(quantidade, '0');

                    if (item == zeros)
                        break;

                    contaComposicao += item + ".";
                }

                contaSanada = contaComposicao.Remove(contaComposicao.Length - 1);
                var contaSanadaArray = contaSanada.Split('.');
                int contador = 0;
                foreach (var item in contaSanadaArray)
                {
                    contador += 1;
                    if (contaSanadaArray.Length > contador)
                    {
                        contaSuperiorRetorno += item + ".";
                    }
                }
                //Não tiro mais o ponto final, pois, ajuda no select(desta forma busco o item superior mais fácil)
                //return contaSuperiorRetorno.Remove(contaSuperiorRetorno.Length - 1);
                return contaSuperiorRetorno;
            }
            else
            {
                return string.Empty;
            }

        }
    }
}
