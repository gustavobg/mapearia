﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ConciliacaoBancaria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ConciliacaoBancariaController : ControllerExtended
    {
        public ConciliacaoBancariaController()
        {
            IndexUrl = "/Financeiro/ConciliacaoBancaria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            var lst = new List<FinanceiroConciliacaoBancariaInfo>();
            FinanceiroConciliacaoBancariaInfo temp = new FinanceiroConciliacaoBancariaInfo();
            ViewModelToModelMapper.Map<FinanceiroConciliacaoBancariaInfo>(vm, temp);

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataConciliacaoExibicao))
                temp.DataConciliacao = DateTime.Parse(vm.DataConciliacaoExibicao);

            #endregion

            //Monta Lista de Itens Conciliados
            var ids = vm.IdsFinanceiroMovimentacaoBancaria.Split(',');
            foreach (var id in ids)
            {
                FinanceiroConciliacaoBancariaInfo info = new FinanceiroConciliacaoBancariaInfo();
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoa = IdPessoa;
                info.DataConciliacao = temp.DataConciliacao;

                info.IdFinanceiroMovimentacaoBancaria = int.Parse(id);

                lst.Add(info);
            }

            var response = FinanceiroConciliacaoBancariaBll.Instance.Salvar(lst);
            if (response.Response.Sucesso)
            {

            }

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ConciliacaoBancaria()
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ConciliacaoBancariaJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroConciliacaoBancariaInfo info = new FinanceiroConciliacaoBancariaInfo();

            if (id.HasValue && id.Value > 0)
            {
                info = FinanceiroConciliacaoBancariaBll.Instance.ListarPorCodigo(id.Value);

                #region Conversões

                if (info.DataConciliacao.HasValue)
                    vm.DataConciliacaoExibicao = info.DataConciliacao.Value.ToShortDateString();

                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroConciliacaoBancaria, "FinanceiroConciliacaoBancaria");
            }

            return Json(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ExcluirConciliacaoBancaria(int? id)
        {
            var response = FinanceiroConciliacaoBancariaBll.Instance.Excluir(new FinanceiroConciliacaoBancariaInfo { IdFinanceiroConciliacaoBancaria = id.Value });
            return Json(response);
        }

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult PreparaConciliacaoPorChave(string Chave)
        {
            var info = new FinanceiroConciliacaoBancariaInfo();
            info.IdsFinanceiroMovimentacaoBancaria = Chave;

            return Json(info);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroConciliacaoBancariaInfo info = new FinanceiroConciliacaoBancariaInfo();
            info.IdEmpresa = IdEmpresa;

            ListPaged<FinanceiroConciliacaoBancariaInfo> retorno = new ListPaged<FinanceiroConciliacaoBancariaInfo>(FinanceiroConciliacaoBancariaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Conciliação Bancária", true);
        }

    }
}
