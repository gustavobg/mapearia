using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Conta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ContaController : ControllerExtended
    {
        public ContaController()
        {
            IndexUrl = "/Financeiro/Conta/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FinanceiroContaInfo info = FinanceiroContaBll.Instance.ListarPorIdCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroConta, "FinanceiroConta");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroContaInfo info = new FinanceiroContaInfo();

            ViewModelToModelMapper.Map<FinanceiroContaInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            //Adicionado dia 08/03/2016
            info.IdPessoa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa;

            #region Consulta
            if (!string.IsNullOrEmpty(vm.IdsPessoaConsultaRestricao))
            {
                info.lstPessoaConsultaRestricao = new List<FinanceiroContaPessoaConsultaRestricaoInfo>();

                CommaSeparatedToList(vm.IdsPessoaConsultaRestricao).ForEach(idpessoa =>
                {
                    FinanceiroContaPessoaConsultaRestricaoInfo item = new FinanceiroContaPessoaConsultaRestricaoInfo();
                    item.IdPessoa = idpessoa;

                    info.lstPessoaConsultaRestricao.Add(item);
                });
            }

            #endregion

            #region Movimentação
            if (!string.IsNullOrEmpty(vm.IdsPessoaMovimentacaoRestricao))
            {
                info.lstPessoaMovimentacaoRestricao = new List<FinanceiroContaPessoaMovimentacaoRestricaoInfo>();

                CommaSeparatedToList(vm.IdsPessoaMovimentacaoRestricao).ForEach(idpessoa =>
                {
                    FinanceiroContaPessoaMovimentacaoRestricaoInfo item = new FinanceiroContaPessoaMovimentacaoRestricaoInfo();
                    item.IdPessoa = idpessoa;

                    info.lstPessoaMovimentacaoRestricao.Add(item);
                });
            }
            #endregion

            var response = FinanceiroContaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroConta.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFinanceiroConta, "FinanceiroConta", info.Response.IdHistorico);
            return Json(response);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroContaInfo info = new FinanceiroContaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroConta = vm.IdFinanceiroConta;
            info.BuscaDetalhada = vm.BuscaDetalhada;
            info.ContaPreferencial = vm.ContaPreferencial;
            info.IdFinanceiroContaTipo = vm.IdFinanceiroContaTipo;
            info.IdPessoa = (vm.PorEmpresaUsuaria.HasValue && vm.PorEmpresaUsuaria.Value) ? ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdPessoa : null;
            info.IdPessoaTitularConta = vm.IdPessoaTitularConta;
            info.Ativo = vm.Ativo;

            #region Restrição de Consulta
            if (vm.ComRestricaoConsultaUsuarioLogado.HasValue && vm.ComRestricaoConsultaUsuarioLogado.Value == true)
            {
                //1 - Verifico se existe alguma restrição de Conta (IdFinanceiroContaNotIn)
                //2 - Realizo a Busca dos itens (contas restritas para consulta do Usuário logado)
                //3 - Capturo a lista de inteiros e realizo a atribuição para a Propriedade IdFinanceiroContaNotIn

                List<int> ids = new List<int>();
                if(!string.IsNullOrEmpty(vm.IdFinanceiroContaNotIn))
                {
                    CommaSeparatedToList(vm.IdFinanceiroContaNotIn).ForEach(id =>
                    {
                        ids.Add(id);
                    });
                }

                var lstConsultasRestritas = FinanceiroContaPessoaConsultaRestricaoBll.Instance.ListarPorParametros(new FinanceiroContaPessoaConsultaRestricaoInfo { IdPessoa = IdPessoa });
                if(lstConsultasRestritas.Count() > 0)
                    ids.AddRange(lstConsultasRestritas.Select(p => p.IdFinanceiroConta.Value).ToList());

                info.IdFinanceiroContaNotIn = string.Join(",", ids);

            }
            #endregion

            ListPaged<FinanceiroContaInfo> retorno = new ListPaged<FinanceiroContaInfo>(FinanceiroContaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:FinanceiroConta", true);

        }
    }
}
