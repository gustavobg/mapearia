using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContaTipo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ContaTipoController : ControllerExtended
    {
        public ContaTipoController()
        {
            IndexUrl = "/Financeiro/ContaTipo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroContaTipoInfo info = new FinanceiroContaTipoInfo();

            ViewModelToModelMapper.Map<FinanceiroContaTipoInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            //info.IdEmpresa = IdEmpresa; //Retirado a pedido do Humberto dia 08/03/2016

            var response = FinanceiroContaTipoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroContaTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFinanceiroContaTipo, "FinanceiroContaTipo", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FinanceiroContaTipoInfo info = FinanceiroContaTipoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroContaTipo , "FinanceiroContaTipo");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroContaTipoInfo info = new FinanceiroContaTipoInfo();
            info.Descricao = vm.Descricao;
            info.IdFinanceiroContaTipo = vm.IdFinanceiroContaTipo;
            info.CaixaInterno = vm.CaixaInterno;
            info.Ativo = vm.Ativo;

            ListPaged<FinanceiroContaTipoInfo> retorno = new ListPaged<FinanceiroContaTipoInfo>(FinanceiroContaTipoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                    {
                        CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                        PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                        GroupBy = vm.Page.GroupBy,
                        OrderBy = vm.Page.OrderBy
                    }
            };
            return base.CreateListResult(vm, retorno, "Exportação:Tipo de Conta", true);
        }
    }
}
