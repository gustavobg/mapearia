﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContratoOperadoraCartao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ContratoOperadoraCartaoController : ControllerExtended
    {
        public ContratoOperadoraCartaoController()
        {
            IndexUrl = "/Financeiro/ContratoOperadoraCartao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroCartaoContratoOperadoraInfo info = new FinanceiroCartaoContratoOperadoraInfo();
            ViewModelToModelMapper.Map<FinanceiroCartaoContratoOperadoraInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FinanceiroCartaoContratoOperadoraBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroCartaoContratoOperadora.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroCartaoContratoOperadora, "FinanceiroCartaoContratoOperadora", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroCartaoContratoOperadoraInfo info = new FinanceiroCartaoContratoOperadoraInfo();

            if (id.HasValue && id > 0)
            {
                info = FinanceiroCartaoContratoOperadoraBll.Instance.ListarPorIdCompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroCartaoContratoOperadora, "FinanceiroCartaoContratoOperadora");

            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroCartaoContratoOperadoraInfo info = new FinanceiroCartaoContratoOperadoraInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroCartaoContratoOperadora = vm.IdFinanceiroCartaoContratoOperadora;
            info.Descricao = vm.Descricao;
            info.DescricaoContratoBusca = vm.DescricaoContratoBusca;
            info.IdFinanceiroEntidadeIn = vm.IdFinanceiroEntidadeIn;
            info.Ativo = vm.Ativo;

            ListPaged<FinanceiroCartaoContratoOperadoraInfo> retorno = new ListPaged<FinanceiroCartaoContratoOperadoraInfo>(FinanceiroCartaoContratoOperadoraBll.Instance.ListarPorParametrosCompleto(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Contrato com Operadora de Cartão Financeiro", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListEquipamento(ListVM vm)
        {
            FinanceiroCartaoContratoOperadoraEquipamentoInfo info = new FinanceiroCartaoContratoOperadoraEquipamentoInfo();
            info.IdFinanceiroCartaoContratoOperadora = vm.IdFinanceiroCartaoContratoOperadora;
            info.IdFinanceiroCartaoContratoOperadoraEquipamento = vm.IdFinanceiroCartaoContratoOperadoraEquipamento;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<FinanceiroCartaoContratoOperadoraEquipamentoInfo> retorno = new ListPaged<FinanceiroCartaoContratoOperadoraEquipamentoInfo>(FinanceiroCartaoContratoOperadoraEquipamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Equipamentos por Contrato com Operadora de Cartão Financeiro", true);
        }

    }
}
