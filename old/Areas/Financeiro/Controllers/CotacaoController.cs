﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Cotacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class CotacaoController : ControllerExtended
    {
        public CotacaoController()
        {
            IndexUrl = "/Financeiro/Cotacao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroCotacaoMoedaInfo info = new FinanceiroCotacaoMoedaInfo();
                info = FinanceiroCotacaoMoedaBll.Instance.ListarPorParametros(new FinanceiroCotacaoMoedaInfo { IdEmpresa = IdEmpresa, IdFinanceiroCotacaoMoeda = id.Value }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroCotacaoMoeda, "FinanceiroCotacaoMoeda");
            }
            else
            {
                vm.Ativo = true;
                var data = DateTime.Now.AddDays(-1);
                switch (data.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                        data = data.AddDays(-1);
                        break;
                    case DayOfWeek.Sunday:
                        data = data.AddDays(-2);
                        break;
                }
                vm.DataExibicao = data.ToShortDateString();
            }

            try
            {
                vm.IdFinanceiroMoedaReferencia = FinanceiroMoedaBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.Empresa.IdFinanceiroMoeda.Value).IdFinanceiroMoeda.Value;
            }
            catch
            {
                vm.IdFinanceiroMoedaReferencia = null;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroCotacaoMoedaInfo info = new FinanceiroCotacaoMoedaInfo();

            ViewModelToModelMapper.Map<FinanceiroCotacaoMoedaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            //Temporariamente 
            if (!string.IsNullOrEmpty(vm.DataExibicao))
                info.Data = Convert.ToDateTime(vm.DataExibicao);

            var response = FinanceiroCotacaoMoedaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
            {
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroCotacaoMoeda.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroCotacaoMoeda, "FinanceiroCotacaoMoeda", response.Response.IdHistorico);
                if (vm.SaveNewCrud)
                {
                    vm = new NewEditVM();
                    vm.Response = response.Response;

                    vm.SaveNewCrud = true;
                    vm.Ativo = true;
                    vm.DataExibicao = info.DataExibicao;
                    vm.IdFinanceiroMoedaReferencia = info.IdFinanceiroMoedaReferencia;
                    return Json(vm);
                }
            }

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroCotacaoMoedaEmpresaInfo info = new FinanceiroCotacaoMoedaEmpresaInfo();
            FinanceiroCotacaoMoedaEmpresaInfo response = new FinanceiroCotacaoMoedaEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroCotacaoMoedaEmpresaBll.Instance.ListarPorParametros(new FinanceiroCotacaoMoedaEmpresaInfo { IdFinanceiroCotacaoMoeda = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroCotacaoMoedaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroCotacaoMoeda, "FinanceiroCotacaoMoeda", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroCotacaoMoedaEmpresaInfo info = new FinanceiroCotacaoMoedaEmpresaInfo();
            FinanceiroCotacaoMoedaEmpresaInfo response = new FinanceiroCotacaoMoedaEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroCotacaoMoedaEmpresaBll.Instance.ListarPorParametros(new FinanceiroCotacaoMoedaEmpresaInfo { IdFinanceiroCotacaoMoeda = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroCotacaoMoedaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroCotacaoMoeda, "FinanceiroCotacaoMoeda", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroCotacaoMoedaInfo info = new FinanceiroCotacaoMoedaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Natureza = vm.Natureza;
            info.DescricaoDetalhadaBusca = vm.DescricaoDetalhadaBusca;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroCotacaoMoedaInfo> retorno = new ListPaged<FinanceiroCotacaoMoedaInfo>(FinanceiroCotacaoMoedaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Moeda", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult ListaUltimaCotacaoPorMoeda(ListVM vm)
        {
            FinanceiroCotacaoMoedaInfo info = new FinanceiroCotacaoMoedaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroMoeda = vm.IdFinanceiroMoeda;
            info.IdFinanceiroMoedaReferencia = vm.IdFinanceiroMoedaReferencia;

            ListPaged<FinanceiroCotacaoMoedaInfo> retorno = new ListPaged<FinanceiroCotacaoMoedaInfo>(FinanceiroCotacaoMoedaBll.Instance.ListaUltimaCotacaoPorMoeda(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = 1,
                    PageSize = 10,
                    GroupBy = "",
                    OrderBy = "IdFinanceiroCotacaoMoeda"
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Moeda", true);
        }

    }
}
