using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Entidade;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class EntidadeController : ControllerExtended
    {
        public EntidadeController()
        {
            IndexUrl = "/Financeiro/Entidade/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                FinanceiroEntidadeInfo info = FinanceiroEntidadeBll.Instance.ListarPorParametros(new FinanceiroEntidadeInfo { IdFinanceiroEntidade = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroEntidade, "FinanceiroEntidade");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroEntidadeInfo info = new FinanceiroEntidadeInfo();

            ViewModelToModelMapper.Map<FinanceiroEntidadeInfo>(vm, info);
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            var response = FinanceiroEntidadeBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroEntidade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdFinanceiroEntidade, "FinanceiroEntidade", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroEntidadeEmpresaInfo info = new FinanceiroEntidadeEmpresaInfo();
            FinanceiroEntidadeEmpresaInfo response = new FinanceiroEntidadeEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroEntidadeEmpresaBll.Instance.ListarPorParametros(new FinanceiroEntidadeEmpresaInfo { IdFinanceiroEntidade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroEntidadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroEntidade, "FinanceiroEntidade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroEntidadeEmpresaInfo info = new FinanceiroEntidadeEmpresaInfo();
            FinanceiroEntidadeEmpresaInfo response = new FinanceiroEntidadeEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroEntidadeEmpresaBll.Instance.ListarPorParametros(new FinanceiroEntidadeEmpresaInfo { IdFinanceiroEntidade = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroEntidadeEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroEntidade, "FinanceiroEntidade", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroEntidadeInfo info = new FinanceiroEntidadeInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroEntidade = vm.IdFinanceiroEntidade;
            info.Descricao = vm.Descricao;
            info.DescricaoCodigo = vm.DescricaoCodigo;
            info.Ativo = vm.Ativo;
            info.DescricaoComposta = vm.DescricaoComposta;
            info.OperadoraCartao = vm.OperadoraCartao;
            info.RegistroProprio = vm.RegistroProprio;

            var retorno = new ListPaged<FinanceiroEntidadeInfo>(FinanceiroEntidadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação:FinanceiroEntidade", true);

        }
    }
}
