﻿using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Financeiro;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FechamentoProcesso;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class FechamentoProcessoController : ControllerExtended
    {
        public FechamentoProcessoController()
        {
            IndexUrl = "/Financeiro/FechamentoProcesso/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New/Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroFechamentoProcessoInfo info = new FinanceiroFechamentoProcessoInfo();
                info = FinanceiroFechamentoProcessoBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroFechamentoProcesso, "FinanceiroFechamentoProcesso");
            }

            vm.IdPessoaResponsavel = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroFechamentoProcessoInfo info = new FinanceiroFechamentoProcessoInfo();
            ViewModelToModelMapper.Map<FinanceiroFechamentoProcessoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversão
            if (!string.IsNullOrEmpty(vm.DataFechamentoExibicao))
                info.DataHoraFechamento = DateTime.Parse(vm.DataFechamentoExibicao);
            #endregion

            if (!vm.IdFinanceiroFechamentoProcesso.HasValue)
            {
                var response = FinanceiroFechamentoProcessoBll.Instance.SalvarNovoFechamentoProcesso(info);
                if (response.Sucesso)
                {
                    //retorno será IdFinanceiroFechamentoProcesso-IdHistórico
                    //Realizo toda essa maracutáia para salvar o Histórico para Todas as Empresas Usuárias
                    if (response.Bag != null)
                    {
                        string[] ids = response.Bag.ToString().Split(',');
                        foreach (var item in ids)
                        {
                            string[] itemResponse = item.ToString().Split('-');
                            if (item.Length > 0)
                                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Inclusao, int.Parse(itemResponse[0].ToString()), "FinanceiroFechamentoProcesso", int.Parse(itemResponse[1].ToString()));
                        }
                    }
                }

                return Json(response);
            }
            else
            {
                var response = FinanceiroFechamentoProcessoBll.Instance.Salvar(info);
                if (response.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroFechamentoProcesso.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroFechamentoProcesso, "FinanceiroFechamentoProcesso", response.Response.IdHistorico);

                return Json(response);
            }
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ReAbrir(string id)
        {
            NewEditVM vm = new NewEditVM();
            return View("ReAbrir", vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ReAbrirJson(string id)
        {
            NewEditVM vm = new NewEditVM();
            if (!string.IsNullOrEmpty(id))
            {
                string[] chave = id.Split('|');

                vm.DataFechamentoExibicao = chave[0]; //Data do Fechamento
                vm.IdPessoa = int.Parse(chave[1].ToString()); //Empresa Usuária
                vm.IdCategoriaFechamentoProcesso = int.Parse(chave[2].ToString()); //Empresa Usuária
                vm.IdPessoaResponsavel = int.Parse(chave[3].ToString()); //Responsável 
                vm.IdPessoaReAbertura = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult ReAbrir(NewEditVM vm)
        {
            FinanceiroFechamentoProcessoInfo info = new FinanceiroFechamentoProcessoInfo();
            ViewModelToModelMapper.Map<FinanceiroFechamentoProcessoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataFechamentoExibicao))
                info.DataHoraFechamento = DateTime.Parse(vm.DataFechamentoExibicao);
            info.DataHoraReAberto = DateTime.Now;
            info.DataHoraOperacao = DateTime.Now;
            info.IdPessoaReAbertura = vm.IdPessoaReAbertura;

            #endregion

            var response = FinanceiroFechamentoProcessoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroFechamentoProcesso.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroFechamentoProcesso, "FinanceiroFechamentoProcesso", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroFechamentoProcessoInfo info = new FinanceiroFechamentoProcessoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdPessoa = vm.IdPessoa;
            info.IdCategoriaFechamentoProcesso = vm.IdCategoriaFechamentoProcesso;

            ListPaged<FinanceiroFechamentoProcessoInfo> retorno = new ListPaged<FinanceiroFechamentoProcessoInfo>(FinanceiroFechamentoProcessoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Benfeitoria", true);
        }


    }
}
