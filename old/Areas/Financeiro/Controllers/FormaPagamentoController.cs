﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FormaPagamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class FormaPagamentoController : ControllerExtended
    {
        public FormaPagamentoController()
        {
            IndexUrl = "/Financeiro/FormaPagamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroFormaPagamentoInfo info = new FinanceiroFormaPagamentoInfo();
            ViewModelToModelMapper.Map<FinanceiroFormaPagamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            if (!string.IsNullOrEmpty(vm.IdsItens))
            {
                info.Itens = new List<FinanceiroFormaPagamentoItemUtilizacaoInfo>();

                CommaSeparatedToList(vm.IdsItens).ForEach(id =>
                {
                    FinanceiroFormaPagamentoItemUtilizacaoInfo item = new FinanceiroFormaPagamentoItemUtilizacaoInfo();
                    item.IdFinanceiroItemUtilizacao = id;

                    info.Itens.Add(item);
                });
            }

            var response = FinanceiroFormaPagamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroFormaPagamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroFormaPagamento, "FinanceiroFormaPagamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroFormaPagamentoInfo info = new FinanceiroFormaPagamentoInfo();
                info = FinanceiroFormaPagamentoBll.Instance.ListarPorIdcompleto(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroFormaPagamento, "FinanceiroFormaPagamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroFormaPagamentoInfo info = new FinanceiroFormaPagamentoInfo();
            info.IdFinanceiroFormaPagamento = vm.IdFinanceiroFormaPagamento;
            info.Descricao = vm.Descricao;
            info.Cobranca = vm.Cobranca;
            info.Bordero = vm.Bordero;
            info.Antecipacao = vm.Antecipacao;
            info.Titulo = vm.Titulo;
            info.Ativo = vm.Ativo;
            info.IdFinanceiroFormaPagamentoIn = vm.IdFinanceiroFormaPagamentoIn;

            ListPaged<FinanceiroFormaPagamentoInfo> retorno = new ListPaged<FinanceiroFormaPagamentoInfo>(FinanceiroFormaPagamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Financeiro Forma de Pagamento", true);
        }

    }
}
