﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ItemUtilizacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ItemUtilizacaoController : ControllerExtended
    {
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroItemUtilizacaoInfo info = new FinanceiroItemUtilizacaoInfo();

            info.IdFinanceiroItemUtilizacao = vm.IdFinanceiroItemUtilizacao;
            info.IdFinanceiroItemUtilizacaoIn = vm.IdFinanceiroItemUtilizacaoIn;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;

            ListPaged<FinanceiroItemUtilizacaoInfo> retorno = new ListPaged<FinanceiroItemUtilizacaoInfo>(FinanceiroItemUtilizacaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Item Utilização Financeira", true);
        }
       
    }
}
