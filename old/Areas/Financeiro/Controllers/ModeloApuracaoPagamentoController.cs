﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ModeloApuracaoPagamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class ModeloApuracaoPagamentoController : ControllerExtended
    {
        
        public ModeloApuracaoPagamentoController()
        {
            IndexUrl = "/Financeiro/ModeloApuracaoPagamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New / Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroModeloApuracaoPagamentoInfo info = new FinanceiroModeloApuracaoPagamentoInfo();
                info = FinanceiroModeloApuracaoPagamentoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroModeloApuracaoPagamento, "FinanceiroModeloApuracaoPagamento");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroModeloApuracaoPagamentoInfo info = new FinanceiroModeloApuracaoPagamentoInfo();
            ViewModelToModelMapper.Map<FinanceiroModeloApuracaoPagamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Perfil de Pessoa
            if (!string.IsNullOrEmpty(vm.IdsPessoaPerfil))
            {
                info.lstPessoaPerfil = new List<FinanceiroModeloApuracaoPagamentoPessoaPerfilInfo>();

                CommaSeparatedToList(vm.IdsPessoaPerfil).ForEach(idPessoaPerfil =>
                {
                    FinanceiroModeloApuracaoPagamentoPessoaPerfilInfo item = new FinanceiroModeloApuracaoPagamentoPessoaPerfilInfo();
                    item.IdPessoaPerfil = idPessoaPerfil;

                    info.lstPessoaPerfil.Add(item);
                });
            }
            #endregion

            var response = FinanceiroModeloApuracaoPagamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroModeloApuracaoPagamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroModeloApuracaoPagamento, "FinanceiroModeloApuracaoPagamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroModeloApuracaoPagamentoEmpresaInfo info = new FinanceiroModeloApuracaoPagamentoEmpresaInfo();
            FinanceiroModeloApuracaoPagamentoEmpresaInfo response = new FinanceiroModeloApuracaoPagamentoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroModeloApuracaoPagamentoEmpresaBll.Instance.ListarPorParametros(new FinanceiroModeloApuracaoPagamentoEmpresaInfo { IdFinanceiroModeloApuracaoPagamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroModeloApuracaoPagamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroModeloApuracaoPagamento, "FinanceiroModeloApuracaoPagamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroModeloApuracaoPagamentoEmpresaInfo info = new FinanceiroModeloApuracaoPagamentoEmpresaInfo();
            FinanceiroModeloApuracaoPagamentoEmpresaInfo response = new FinanceiroModeloApuracaoPagamentoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroModeloApuracaoPagamentoEmpresaBll.Instance.ListarPorParametros(new FinanceiroModeloApuracaoPagamentoEmpresaInfo { IdFinanceiroModeloApuracaoPagamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroModeloApuracaoPagamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroModeloApuracaoPagamento, "FinanceiroModeloApuracaoPagamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }



        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroModeloApuracaoPagamentoInfo info = new FinanceiroModeloApuracaoPagamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroModeloApuracaoPagamento = vm.IdFinanceiroModeloApuracaoPagamento;
            info.Descricao = vm.Descricao;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.Ativo = vm.Ativo;
            info.ComVariaveis = vm.ComVariaveis;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroModeloApuracaoPagamentoInfo> retorno = new ListPaged<FinanceiroModeloApuracaoPagamentoInfo>(FinanceiroModeloApuracaoPagamentoBll.Instance.ListarPorParametroComVariaveis(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Modelo de Apuração de Pagamento", true);
        }


    }
}
