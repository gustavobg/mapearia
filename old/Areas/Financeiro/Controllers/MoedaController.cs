﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Moeda;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class MoedaController : ControllerExtended
    {
        public MoedaController()
        {
            IndexUrl = "/Financeiro/Moeda/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroMoedaInfo info = new FinanceiroMoedaInfo();
                info = FinanceiroMoedaBll.Instance.ListarPorParametros(new FinanceiroMoedaInfo { IdFinanceiroMoeda = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroMoeda, "FinanceiroMoeda");
            }
            else
            {
                vm.Ativo = true;
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroMoedaInfo info = new FinanceiroMoedaInfo();

            ViewModelToModelMapper.Map<FinanceiroMoedaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FinanceiroMoedaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroMoeda.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroMoeda, "FinanceiroMoeda", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroMoedaEmpresaInfo info = new FinanceiroMoedaEmpresaInfo();
            FinanceiroMoedaEmpresaInfo response = new FinanceiroMoedaEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroMoedaEmpresaBll.Instance.ListarPorParametros(new FinanceiroMoedaEmpresaInfo { IdFinanceiroMoeda = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroMoedaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroMoeda, "FinanceiroMoeda", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroMoedaEmpresaInfo info = new FinanceiroMoedaEmpresaInfo();
            FinanceiroMoedaEmpresaInfo response = new FinanceiroMoedaEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroMoedaEmpresaBll.Instance.ListarPorParametros(new FinanceiroMoedaEmpresaInfo { IdFinanceiroMoeda = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroMoedaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroMoeda, "FinanceiroMoeda", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroMoedaInfo info = new FinanceiroMoedaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroMoeda = vm.IdFinanceiroMoeda;
            info.Descricao = vm.Descricao;
            info.Natureza = vm.Natureza;
            info.Ativo = vm.Ativo;
            info.DescricaoDetalhadaBusca = vm.DescricaoDetalhadaBusca;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroMoedaInfo> retorno = new ListPaged<FinanceiroMoedaInfo>(FinanceiroMoedaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Moeda", true);
        }
    }
}
