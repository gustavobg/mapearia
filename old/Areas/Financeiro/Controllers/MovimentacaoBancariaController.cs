﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.MovimentacaoBancaria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class MovimentacaoBancariaController : ControllerExtended
    {
        public MovimentacaoBancariaController()
        {
            IndexUrl = "/Financeiro/MovimentacaoBancaria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroMovimentacaoBancariaInfo info = new FinanceiroMovimentacaoBancariaInfo();
            ViewModelToModelMapper.Map<FinanceiroMovimentacaoBancariaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            //Se entrar na condição abaixo é porque o registro é novo com toda a certeza!
            if (info.Origem == 2 && info.IdFinanceiroMovimentacaoBancaria == null && info.IdPessoaOperacaoBaixa == null)
                info.IdPessoaOperacaoBaixa = IdPessoa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoExibicao))
                info.DataMovimentacao = DateTime.Parse(vm.DataMovimentacaoExibicao);

            #endregion

            var response = FinanceiroMovimentacaoBancariaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroMovimentacaoBancaria.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroMovimentacaoBancaria, "FinanceiroMovimentacaoBancaria", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroMovimentacaoBancariaInfo info = new FinanceiroMovimentacaoBancariaInfo();

            var infoPre = FinanceiroMovimentacaoBancariaBll.Instance.PreparaNovoRegistro(new FinanceiroMovimentacaoBancariaInfo { IdFinanceiroMoedaEmpresa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda });

            if (id.HasValue && id > 0)
            {
                info.SiglaMoeda = infoPre.SiglaMoeda;
                info.CasasDecimaisMoedaEmpresa = infoPre.CasasDecimaisMoedaEmpresa;

                info = FinanceiroMovimentacaoBancariaBll.Instance.ListarPorIdCompleto(id.Value, info.CasasDecimaisMoedaEmpresa.Value, info.SiglaMoeda);

                #region Conversões

                if (info.DataMovimentacao.HasValue)
                    vm.DataMovimentacaoExibicao = info.DataMovimentacao.Value.ToShortDateString();

                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroMovimentacaoBancaria, "FinanceiroMovimentacaoBancaria");
            }
            else
            {
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, infoPre);
                //Origem [1 == Automático | 2 == Manual]
                vm.Origem = 2;
            }

            vm.IdPlanoContasVersaoCentroCusto = PlanoContasVersaoBll.Instance.RetornaVersaoRecentePorEmpresa(IdEmpresa, 2).IdPlanoContasVersao;
            vm.IdPlanoContasVersaoPlanoContas = PlanoContasVersaoBll.Instance.RetornaVersaoRecentePorEmpresa(IdEmpresa, 1).IdPlanoContasVersao;
            vm.IdPlanoContasClassificacaoFinanceira = PlanoContasVersaoBll.Instance.RetornaVersaoRecentePorEmpresa(IdEmpresa, 4).IdPlanoContasVersao;

            foreach (var item in vm.lstClassificacaoContabil)
            {
                item.IdPlanoContasVersaoCentroCusto = vm.IdPlanoContasVersaoCentroCusto;
                item.IdPlanoContasVersaoPlanoContas = vm.IdPlanoContasVersaoPlanoContas;

                item.CasasDecimaisMoedaEmpresa = infoPre.CasasDecimaisMoedaEmpresa;
                item.SiglaMoeda = infoPre.SiglaMoeda;
            }

            foreach (var item in vm.lstTransferencia)
            {
                item.CasasDecimaisMoedaEmpresa = infoPre.CasasDecimaisMoedaEmpresa;
                item.SiglaMoeda = infoPre.SiglaMoeda;
            }

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            bool conciliado = false;
            bool aconciliar = false;

            FinanceiroMovimentacaoBancariaInfo info = new FinanceiroMovimentacaoBancariaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroContaIn = vm.IdFinanceiroContaIn;
            info.Origem = vm.Origem;

            if (vm.Natureza.Length > 0)
                info.NaturezaIn = string.Join(",", vm.Natureza);


            #region conversões
            if (!string.IsNullOrEmpty(vm.DataMovimentacaoInicio))
                info.DataMovimentacaoInicio = DateTime.Parse(vm.DataMovimentacaoInicio);

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoFim))
                info.DataMovimentacaoFim = DateTime.Parse(vm.DataMovimentacaoFim);
            #endregion

            #region Informações Sobre a Moeda da Empresa Corrente

            var infoPre = FinanceiroMovimentacaoBancariaBll.Instance.PreparaNovoRegistro(new FinanceiroMovimentacaoBancariaInfo { IdFinanceiroMoedaEmpresa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda });
            info.SiglaMoeda = infoPre.SiglaMoeda;
            info.CasasDecimaisMoedaEmpresa = infoPre.CasasDecimaisMoedaEmpresa;

            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            var retorno = new List<FinanceiroMovimentacaoBancariaInfo>(FinanceiroMovimentacaoBancariaBll.Instance.ListarPorParametrosAgrupadoPorData(info));
            var novo = new List<FinanceiroMovimentacaoBancariaInfo>();

            #region Filtro Tipo de Conciliação
            if (vm.TipoConciliacao.Length > 0)
            {
                foreach (var item in vm.TipoConciliacao)
                {
                    if (item == "1")
                        aconciliar = true;
                    else if (item == "2")
                        conciliado = true;
                }

                if (aconciliar)
                    novo.AddRange(retorno.Where(p => p.Conciliado == false || p.Conciliado == null));
                if (conciliado)
                    novo.AddRange(retorno.Where(p => p.Conciliado == true));
            }
            else
                novo.AddRange(retorno);
            #endregion
            ListPaged<FinanceiroMovimentacaoBancariaInfo> x = new ListPaged<FinanceiroMovimentacaoBancariaInfo>(novo)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResultByStoredProcedure(vm, x, "Exportação: Extrato e Movimentação de Conta");
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListConciliacaoPorMovimentacaoBancaria(ListVM vm)
        {
            FinanceiroMovimentacaoBancariaInfo info = new FinanceiroMovimentacaoBancariaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroConta = vm.IdFinanceiroConta;

            #region conversões
            if (!string.IsNullOrEmpty(vm.DataMovimentacaoInicio))
                info.DataMovimentacaoInicio = DateTime.Parse(vm.DataMovimentacaoInicio);

            if (!string.IsNullOrEmpty(vm.DataMovimentacaoFim))
                info.DataMovimentacaoFim = DateTime.Parse(vm.DataMovimentacaoFim);
            #endregion

            #region Informações Sobre a Moeda da Empresa Corrente

            var infoPre = FinanceiroMovimentacaoBancariaBll.Instance.PreparaNovoRegistro(new FinanceiroMovimentacaoBancariaInfo { IdFinanceiroMoedaEmpresa = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda });
            info.SiglaMoeda = infoPre.SiglaMoeda;
            info.CasasDecimaisMoedaEmpresa = infoPre.CasasDecimaisMoedaEmpresa;

            #endregion

            List<FinanceiroMovimentacaoBancariaInfo> lstBusca = new List<FinanceiroMovimentacaoBancariaInfo>();
            lstBusca = FinanceiroMovimentacaoBancariaBll.Instance.ListarConciliacoesPorMovimentacoes(info);

            var retorno = new List<FinanceiroMovimentacaoBancariaInfo>();
            if (vm.ConciliacaoIn != null && vm.ConciliacaoIn.Length > 0)
            {
                foreach (var item in vm.ConciliacaoIn)
                {
                    if (item == "1")
                        retorno.AddRange(lstBusca.Where(p => p.Conciliado == false));
                    else if (item == "2")
                        retorno.AddRange(lstBusca.Where(p => p.Conciliado == true));
                }
            }
            else
                retorno.AddRange(lstBusca);

            ListPaged<FinanceiroMovimentacaoBancariaInfo> lstFiltrados = new ListPaged<FinanceiroMovimentacaoBancariaInfo>(retorno)
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, lstFiltrados, "Exportação: Conciliação Bancária", true);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult Excluir(int id)
        {
            var info = FinanceiroMovimentacaoBancariaBll.Instance.ExcluirMovimentacaoComItensRelacionados(id);
            return Json(info);
        }
    }
}
