﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.MovimentacaoContaHistorico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class MovimentacaoContaHistoricoController : ControllerExtended
    {
        public MovimentacaoContaHistoricoController()
        {
            IndexUrl = "/Financeiro/MovimentacaoContaHistorico/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroMovimentacaoContaHistoricoInfo info = new FinanceiroMovimentacaoContaHistoricoInfo();

            if (id.HasValue && id > 0)
            {
                info = FinanceiroMovimentacaoContaHistoricoBll.Instance.ListarPorParametros(new FinanceiroMovimentacaoContaHistoricoInfo { IdFinanceiroMovimentacaoContaHistorico = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroMovimentacaoContaHistorico, "FinanceiroMovimentacaoContaHistorico");

            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroMovimentacaoContaHistoricoInfo info = new FinanceiroMovimentacaoContaHistoricoInfo();
            ViewModelToModelMapper.Map<FinanceiroMovimentacaoContaHistoricoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;


            var response = FinanceiroMovimentacaoContaHistoricoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroMovimentacaoContaHistorico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroMovimentacaoContaHistorico, "FinanceiroMovimentacaoContaHistorico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroMovimentacaoContaHistoricoEmpresaInfo info = new FinanceiroMovimentacaoContaHistoricoEmpresaInfo();
            FinanceiroMovimentacaoContaHistoricoEmpresaInfo response = new FinanceiroMovimentacaoContaHistoricoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroMovimentacaoContaHistoricoEmpresaBll.Instance.ListarPorParametros(new FinanceiroMovimentacaoContaHistoricoEmpresaInfo { IdFinanceiroMovimentacaoContaHistorico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroMovimentacaoContaHistoricoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroMovimentacaoContaHistorico, "FinanceiroMovimentacaoContaHistorico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroMovimentacaoContaHistoricoEmpresaInfo info = new FinanceiroMovimentacaoContaHistoricoEmpresaInfo();
            FinanceiroMovimentacaoContaHistoricoEmpresaInfo response = new FinanceiroMovimentacaoContaHistoricoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroMovimentacaoContaHistoricoEmpresaBll.Instance.ListarPorParametros(new FinanceiroMovimentacaoContaHistoricoEmpresaInfo { IdFinanceiroMovimentacaoContaHistorico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroMovimentacaoContaHistoricoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroMovimentacaoContaHistorico, "FinanceiroMovimentacaoContaHistorico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroMovimentacaoContaHistoricoInfo info = new FinanceiroMovimentacaoContaHistoricoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroMovimentacaoContaHistorico = vm.IdFinanceiroMovimentacaoContaHistorico;
            info.Descricao = vm.Descricao;
            info.TransferenciaEntreContas = vm.TransferenciaEntreContas;
            info.TransferenciaEntreEntidadesIguais = vm.TransferenciaEntreEntidadesIguais;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroMovimentacaoContaHistoricoInfo> retorno = new ListPaged<FinanceiroMovimentacaoContaHistoricoInfo>(FinanceiroMovimentacaoContaHistoricoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Historico Padrão de Movimentação de Conta", true);
        }
    }
}
