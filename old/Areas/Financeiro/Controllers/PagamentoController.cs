﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class PagamentoController : ControllerExtended
    {
        public PagamentoController()
        {
            IndexUrl = "/Financeiro/Pagamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroPagamentoInfo info = new FinanceiroPagamentoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.Titulo = vm.Titulo;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdFinanceiroFormaPagamentoPrevistaIn = vm.IdFinanceiroFormaPagamentoPrevistaIn;
            info.TipoMovimentoFinanceiro = vm.TipoMovimentoFinanceiro;
            info.IdPessoa = vm.IdPessoa;
            info.Situacao = vm.Situacao;

            var financeiroMoeda = FinanceiroMoedaBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value);
            if (financeiroMoeda != null)
                info.SiglaFianceiroMoeda = financeiroMoeda.Sigla; info.CasasDecimaisFinanceiroMoeda = financeiroMoeda.CasasDecimais.Value;

            #region Tipo Busca

            if (vm.TipoBusca != null)
            {
                if (vm.TipoBusca.Length > 1)
                    info.TipoBusca = null;
                else if (vm.TipoBusca.Length == 1)
                    info.TipoBusca = int.Parse(vm.TipoBusca[0].ToString());
                else
                    info.TipoBusca = null;
            }

            #endregion

            #region Situação

            if (vm.Situacao != null && vm.Situacao.Length > 0)
            {
                List<string> lstSituacaoPagamento = new List<string>();

                #region Pagamento
                if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 1)
                {
                    foreach (var item in vm.Situacao)
                    {
                        if (item == "1")
                            lstSituacaoPagamento.AddRange(new List<string> { "2" });

                        if (item == "2")
                            lstSituacaoPagamento.AddRange(new List<string> { "3" });

                        if (item == "3")
                            lstSituacaoPagamento.AddRange(new List<string> { "4" });
                    }
                }
                #endregion

                #region Recebimento
                if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 2)
                {
                    foreach (var item in vm.Situacao)
                    {
                        if (item == "1")
                            lstSituacaoPagamento.AddRange(new List<string> { "7", "11" });

                        if (item == "2")
                            lstSituacaoPagamento.AddRange(new List<string> { "8", "18" });

                        if (item == "3")
                            lstSituacaoPagamento.AddRange(new List<string> { "9" });
                    }
                }
                if (lstSituacaoPagamento.Count > 0)
                    info.IdProcessoSituacaoIn = string.Join(",", lstSituacaoPagamento);

                #endregion
            }


            #endregion

            #region Conversão Datas

            //Data Vencimento
            if (!string.IsNullOrEmpty(vm.DataVencimentoInicio))
                info.DataVencimentoInicio = DateTime.Parse(vm.DataVencimentoInicio);

            if (!string.IsNullOrEmpty(vm.DataVencimentoFim))
                info.DataVencimentoFim = DateTime.Parse(vm.DataVencimentoFim);

            //Data Emissão
            if (!string.IsNullOrEmpty(vm.DataEmissaoInicio))
                info.DataEmissaoInicio = DateTime.Parse(vm.DataEmissaoInicio);

            if (!string.IsNullOrEmpty(vm.DataEmissaoFim))
                info.DataEmissaoFim = DateTime.Parse(vm.DataEmissaoFim);

            #endregion

            ListPaged<FinanceiroPagamentoInfo> retorno = new ListPaged<FinanceiroPagamentoInfo>(FinanceiroPagamentoBll.Instance.ListarInformacoes(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            TempData["TotalSelecionados"] = retorno.Count();

            return base.CreateListResult(vm, retorno, "Exportação: Pagamentos", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListRegistrosParaAbatimentoCredito(ListVM vm)
        {
            FinanceiroPagamentoInfo info = new FinanceiroPagamentoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdDocumentoTipo = vm.IdDocumentoTipo;
            info.Titulo = vm.Titulo;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdFinanceiroFormaPagamentoPrevistaIn = vm.IdFinanceiroFormaPagamentoPrevistaIn;
            info.TipoMovimentoFinanceiro = vm.TipoMovimentoFinanceiro;
            info.IdPessoa = vm.IdPessoa;

            var financeiroMoeda = FinanceiroMoedaBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value);
            if (financeiroMoeda != null)
                info.SiglaFianceiroMoeda = financeiroMoeda.Sigla; info.CasasDecimaisFinanceiroMoeda = financeiroMoeda.CasasDecimais.Value;

            #region Tipo de Movimento
            //TipoMovimentoFinanceiro == 1 A Pagar
            if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 1)
            {
                info.IdProcessoSituacaoTituloIn = "2";
                info.IdProcessoSituacaoAntecipacaoIn = "10";
            }

            //TipoMovimentoFinanceiro == 2 A Receber
            if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 2)
            {
                info.IdProcessoSituacaoTituloIn = "7";
                info.IdProcessoSituacaoAntecipacaoIn = "11";
            }
            #endregion

            #region Conversão Datas

            if (!string.IsNullOrEmpty(vm.DataVencimentoInicio))
                info.DataVencimentoInicio = DateTime.Parse(vm.DataVencimentoInicio);

            if (!string.IsNullOrEmpty(vm.DataVencimentoFim))
                info.DataVencimentoFim = DateTime.Parse(vm.DataVencimentoFim);
            #endregion

            ListPaged<FinanceiroPagamentoInfo> retorno = new ListPaged<FinanceiroPagamentoInfo>(FinanceiroPagamentoBll.Instance.ListarInformacoes(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Pagamentos", true);
        }

        #region Baixas
        [HttpGet]
        public ActionResult Baixas(string id)
        {
            DetalhesPagamentoVM vm = new DetalhesPagamentoVM();
            return View(vm);
        }

        [CustomAuthorize, ExceptionFilter]
        [HttpPost]
        public ActionResult BaixasJson(string id)
        {
            DetalhesPagamentoVM vm = new DetalhesPagamentoVM();
            string[] keys = id.Split('|');

            string data = keys[0].ToString();
            string tipo = keys[1].ToString();
            int identificador = int.Parse(keys[2].ToString());

            if (tipo == "T")
            {
                var info = FinanceiroPagamentoBll.Instance.ListarBaixasRealizadas(tipo, identificador);
                ViewModelToModelMapper.MapBack<DetalhesPagamentoVM>(vm, info);
            }
            else if (tipo == "A")
            {
                var info = FinanceiroPagamentoBll.Instance.ListaInformacoesAntecipacao(identificador);
                ViewModelToModelMapper.MapBack<DetalhesPagamentoVM>(vm, info);
            }

            return Json(vm);
        }
        #endregion

        #region Detalhes do Título

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult DetalhesTitulo(DetalheTituloVM vm)
        {
            FinanceiroTituloDetalheInfo info = new FinanceiroTituloDetalheInfo();
            ViewModelToModelMapper.Map<FinanceiroTituloDetalheInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoa = IdPessoa;

            var response = FinanceiroTituloDetalheBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
            {
                // TODO:  Encontrar uma forma de guardar o histórico
            }
            return Json(response);
        }

        [HttpGet]
        public ActionResult DetalhesTitulo(string id)
        {
            DetalheTituloVM vm = new DetalheTituloVM();
            return View(vm);
        }

        [CustomAuthorize, ExceptionFilter]
        [HttpPost]
        public ActionResult DetalhesTituloJson(string parentId)
        {
            #region Obsoleto

            //DetalhesPagamentoVM vm = new DetalhesPagamentoVM();

            //string[] keys = parentId.Split('|');

            //string data = keys[0].ToString();
            //string tipo = keys[1].ToString();
            //int id = int.Parse(keys[2].ToString());

            //var moedaEmpresaUsuaria = FinanceiroMoedaBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value);
            //if (tipo == "T")
            //{

            //    var info = FinanceiroPagamentoBll.Instance.ListaInformacoesTituloTemp(id, moedaEmpresaUsuaria.CasasDecimais, moedaEmpresaUsuaria.Sigla);
            //    ViewModelToModelMapper.MapBack<DetalhesPagamentoVM>(vm, info);
            //    //Apenas para SETAR via ServerSide a chave composta do registro.//
            //    vm.ChaveComposta = parentId;
            //    vm.ID = id;
            //    vm.Tipo = TipoDocumentoFinanceiroEnum.Titulo;
            //}

            #endregion
            DetalheTituloVM vm = new DetalheTituloVM();
            string[] keys = parentId.Split('|');

            string data = keys[0].ToString();
            string tipo = keys[1].ToString();
            int id = int.Parse(keys[2].ToString());

            if (tipo == "T")
            {
                var info = FinanceiroTituloDetalheBll.Instance.ListaDetalhePendente(id);
                if (info != null && info.IdFinanceiroTituloDetalhe.HasValue)
                    ViewModelToModelMapper.MapBack<DetalheTituloVM>(vm, info);
                else
                {
                    var infoPre = FinanceiroTituloDetalheBll.Instance.PreparaNovoRegistro(id);
                    if (ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa != null && ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.HasValue)
                        infoPre.IdFinanceiroMoedaValorAtual = ControladorSessaoUsuario.SessaoCorrente.PessoaEmpresa.IdFinanceiroMoeda.Value;

                    ViewModelToModelMapper.MapBack<DetalheTituloVM>(vm, infoPre);

                }
            }


            return Json(vm);
        }

        #endregion

        #region Detalhes data Antecipação

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult DetalhesAntecipacao(DetalheAntecipacaoVM vm)
        {
            FinanceiroAntecipacaoDetalheInfo info = new FinanceiroAntecipacaoDetalheInfo();
            ViewModelToModelMapper.Map<FinanceiroAntecipacaoDetalheInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoa = IdPessoa;
            info.BaixaRealizada = false;

            var response = FinanceiroAntecipacaoDetalheBll.Instance.Salvar(info);

            if (response.Response.Sucesso)
            {
                // TODO:  Encontrar uma forma de guardar o histórico
            }

            return Json(response);
        }

        [HttpGet]
        public ActionResult DetalhesAntecipacao(string id)
        {
            DetalheAntecipacaoVM vm = new DetalheAntecipacaoVM();
            return View(vm);
        }

        [CustomAuthorize, ExceptionFilter]
        [HttpPost]
        public ActionResult DetalhesAntecipacaoJson(string parentId)
        {
            DetalheAntecipacaoVM vm = new DetalheAntecipacaoVM();

            string[] keys = parentId.Split('|');

            string data = keys[0].ToString();
            string tipo = keys[1].ToString();
            int id = int.Parse(keys[2].ToString());

            var info = FinanceiroAntecipacaoDetalheBll.Instance.ListaDetalhePendente(id);
            if (info != null && info.IdFinanceiroAntecipacaoDetalhe.HasValue)
                ViewModelToModelMapper.MapBack<DetalheAntecipacaoVM>(vm, info);

            else
            {
                var infoPre = FinanceiroAntecipacaoDetalheBll.Instance.PreparaNovoRegistro(id);
                ViewModelToModelMapper.MapBack<DetalheAntecipacaoVM>(vm, infoPre);
            }

            return Json(vm);
        }

        #endregion

        #region Finalização - Forma de Pagamento

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult FormaPagamentoTitulo(FormaPagamentoTituloVM vm)
        {
            FinanceiroPagamentoInfo info = new FinanceiroPagamentoInfo();
            ViewModelToModelMapper.Map<FinanceiroPagamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FinanceiroPagamentoBll.Instance.SalvarPagamento(info);
            if (response.Response.Sucesso)
            {
                //TODO: ¿¿ HISTÓRICO ??
            }

            return Json(response);
        }

        [HttpGet]
        public ActionResult FormaPagamentoTitulo(string id)
        {
            FormaPagamentoTituloVM vm = new FormaPagamentoTituloVM();

            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult FormaPagamentoTituloJson(string id)
        {
            FormaPagamentoTituloVM vm = new FormaPagamentoTituloVM();

            string data = string.Empty;
            string tipo = string.Empty;
            if (!string.IsNullOrEmpty(id))
            {
                string[] keys = id.Split('|');
                data = keys[0].ToString();
                tipo = keys[1].ToString();
            }

            #region Quantidade Selecionados
            if (TempData["TotalSelecionados"] != null)
            {
                try
                {
                    vm.TotalFiltrados = int.Parse(TempData["TotalSelecionados"].ToString());
                }
                catch (Exception)
                {
                    vm.TotalFiltrados = 10;
                }
            }
            else
                vm.TotalFiltrados = 10;

            #endregion
            //vm.QuantidadeSelecionados = 1;

            vm.DataBaixa = !string.IsNullOrEmpty(data) ? Convert.ToDateTime(data).ToShortDateString() : DateTime.Now.ToShortDateString();

            return Json(vm);
        }

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult PreparaPagamentoPorChave(string Chave)
        {
            var info = FinanceiroPagamentoBll.Instance.PreparaPagamentoPorChave(Chave);

            return Json(info);
        }

        #endregion

    }
}
