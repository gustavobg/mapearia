﻿using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.SaldoContaCaixa;
using HTM.MasterGestor.Model.Financeiro;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class SaldoContaCaixaController : ControllerExtended
    {
        public SaldoContaCaixaController()
        {
            IndexUrl = "/#/Financeiro/SaldoContaCaixa/Index";

        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroSaldoContaCaixaInfo info = new FinanceiroSaldoContaCaixaInfo();
            info.IdEmpresa_Param = IdEmpresa;

            info.BuscaAvancada_Param = vm.BuscaAvancada;
            info.NaturezaIn_Param = string.Join(",", vm.Natureza);
            info.IdFinanceiroContaIn_Param = vm.IdFinanceiroContaIn;
            info.IdFinanceiroEntidadeIn_Param = vm.IdFinanceiroEntidadeIn;
            info.IdPessoaIn_Param = vm.IdPessoaIn;

            if (!string.IsNullOrEmpty(vm.DataMovimentoInicio))
                info.DataMovimentoInicio_Param = DateTime.Parse(vm.DataMovimentoInicio);

            if (!string.IsNullOrEmpty(vm.DataMovimentoFim))
                info.DataMovimentoFim_Param = DateTime.Parse(vm.DataMovimentoFim);

            ListPaged<FinanceiroSaldoContaCaixaInfo> retorno = new ListPaged<FinanceiroSaldoContaCaixaInfo>(FinanceiroSaldoContaCaixaBll.Instance.ListarSaldoContaCaixaPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Saldo Conta ou Caixa", true);
        }
    }
}
