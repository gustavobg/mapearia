﻿
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Titulo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class TituloController : ControllerExtended
    {
        public TituloController()
        {
            IndexUrl = "/Financeiro/Titulo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroTituloInfo info = new FinanceiroTituloInfo();
            ViewModelToModelMapper.Map<FinanceiroTituloInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversões
            if (!string.IsNullOrEmpty(vm.DataLancamentoExibicao))
                info.DataLancamento = Convert.ToDateTime(vm.DataLancamentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataEmissaoExibicao))
                info.DataEmissao = Convert.ToDateTime(vm.DataEmissaoExibicao);

            if (!string.IsNullOrEmpty(vm.DataVencimentoExibicao))
                info.DataVencimento = Convert.ToDateTime(vm.DataVencimentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataVencimentoOriginalExibicao))
                info.DataVencimentoOriginal = Convert.ToDateTime(vm.DataVencimentoOriginalExibicao);

            if (!string.IsNullOrEmpty(vm.DataLimiteDescontoExibicao))
                info.DataLimiteDesconto = Convert.ToDateTime(vm.DataLimiteDescontoExibicao);

            #endregion

            var response = FinanceiroTituloBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroTitulo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroTitulo, "FinanceiroTitulo", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            FinanceiroTituloInfo info = new FinanceiroTituloInfo();

            if (id.HasValue && id > 0)
            {
                info = FinanceiroTituloBll.Instance.ListarPorIdFinanceiroTituloCompleto(id.Value);
                info.IdFinanceiroMoedaPadrao = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdFinanceiroMoeda.Value;

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroTitulo, "FinanceiroTitulo");

            }
            else
            {
                info = FinanceiroTituloBll.Instance.PreparaNovoRegistro(info);

                info.IdFinanceiroMoedaValorTitulo = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdFinanceiroMoeda.Value;
                info.IdFinanceiroMoedaValorAtual = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdFinanceiroMoeda.Value;
                info.IdFinanceiroMoedaPadrao = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdFinanceiroMoeda.Value;

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
            }

            #region Conversões
            if (info.DataLancamento.HasValue)
                vm.DataLancamentoExibicao = info.DataLancamento.Value.ToShortDateString();

            if (info.DataEmissao.HasValue)
                vm.DataEmissaoExibicao = info.DataEmissao.Value.ToShortDateString();

            if (info.DataVencimento.HasValue)
                vm.DataVencimentoExibicao = info.DataVencimento.Value.ToShortDateString();

            if (info.DataVencimentoOriginal.HasValue)
                vm.DataVencimentoOriginalExibicao = info.DataVencimentoOriginal.Value.ToShortDateString();

            if (info.DataLimiteDesconto.HasValue)
                vm.DataLimiteDescontoExibicao = info.DataLimiteDesconto.Value.ToShortDateString();
            #endregion

            return Json(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroTituloInfo info = new FinanceiroTituloInfo();
            info.IdEmpresa = IdEmpresa;
            info.Titulo = vm.Titulo;
            info.IdPessoa = vm.IdPessoa;
            info.IdProcessoSituacaoIn = vm.IdProcessoSituacaoIn;
            info.IdFinanceiroFormaPagamentoPrevistaIn = vm.IdFinanceiroFormaPagamentoPrevistaIn;

            #region Tipo de Movimento
            //TipoMovimentoFinanceiro == 1 A Pagar
            //if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 1)
            //    if (string.IsNullOrEmpty(info.IdProcessoSituacaoIn)) // caso o usuário tenha informado alguma Situação, Não será necessário informar as situações via server
            //        info.IdProcessoSituacaoIn = "2,3,4,5,10,17";

            //TipoMovimentoFinanceiro == 2 A Receber
            //if (vm.TipoMovimentoFinanceiro.HasValue && vm.TipoMovimentoFinanceiro.Value == 2)
            //    if (string.IsNullOrEmpty(info.IdProcessoSituacaoIn)) // caso o usuário tenha informado alguma Situação, Não será necessário informar as situações via server
            //        info.IdProcessoSituacaoIn = "7,8,9,11,18";

            #endregion

            #region Conversão Datas
            if (!string.IsNullOrEmpty(vm.DataEmissaoInicio))
                info.DataEmissaoInicio = DateTime.Parse(vm.DataEmissaoInicio);

            if (!string.IsNullOrEmpty(vm.DataEmissaoFim))
                info.DataEmissaoFim = DateTime.Parse(vm.DataEmissaoFim);

            if (!string.IsNullOrEmpty(vm.DataVencimentoInicio))
                info.DataVencimentoInicio = DateTime.Parse(vm.DataVencimentoInicio);

            if (!string.IsNullOrEmpty(vm.DataVencimentoFim))
                info.DataVencimentoFim = DateTime.Parse(vm.DataVencimentoFim);
            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            ListPaged<FinanceiroTituloInfo> retorno = new ListPaged<FinanceiroTituloInfo>(FinanceiroTituloBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Título a Pagar e Receber");
        }
        
    }
}
