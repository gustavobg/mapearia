﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.VariavelPagamento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Bll.Financeiro;
using HTM.MasterGestor.Model.Financeiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.Controllers
{
    public class VariavelPagamentoController : ControllerExtended
    {
        public VariavelPagamentoController()
        {
            IndexUrl = "/Financeiro/VariavelPagamento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                FinanceiroVariavelPagamentoInfo info = new FinanceiroVariavelPagamentoInfo();
                info = FinanceiroVariavelPagamentoBll.Instance.ListarPorParametros(new FinanceiroVariavelPagamentoInfo { IdFinanceiroClassificacao = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                var planoContasVersaoClassificacaoFinanceira = PlanoContasVersaoBll.Instance.RetornaVersaoRecentePorEmpresa(IdEmpresa, 4);
                if (!vm.IdFinanceiroClassificacao.HasValue)
                {
                    if (planoContasVersaoClassificacaoFinanceira != null)
                        vm.IdPlanoContasVersao = planoContasVersaoClassificacaoFinanceira.IdPlanoContasVersao;
                }
              SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdFinanceiroVariavelPagamento, "FinanceiroVariavelPagamento");
            }
            else
            {
                vm.Ativo = true;

                var planoContasVersaoClassificacaoFinanceira = PlanoContasVersaoBll.Instance.RetornaVersaoRecentePorEmpresa(IdEmpresa, 4);
                if (planoContasVersaoClassificacaoFinanceira != null)
                    vm.IdPlanoContasVersao = planoContasVersaoClassificacaoFinanceira.IdPlanoContasVersao;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            FinanceiroVariavelPagamentoInfo info = new FinanceiroVariavelPagamentoInfo();
            ViewModelToModelMapper.Map<FinanceiroVariavelPagamentoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = FinanceiroVariavelPagamentoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdFinanceiroVariavelPagamento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdFinanceiroVariavelPagamento, "FinanceiroVariavelPagamento", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            FinanceiroVariavelPagamentoEmpresaInfo info = new FinanceiroVariavelPagamentoEmpresaInfo();
            FinanceiroVariavelPagamentoEmpresaInfo response = new FinanceiroVariavelPagamentoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroVariavelPagamentoEmpresaBll.Instance.ListarPorParametros(new FinanceiroVariavelPagamentoEmpresaInfo { IdFinanceiroVariavelPagamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = FinanceiroVariavelPagamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroVariavelPagamento, "FinanceiroVariavelPagamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            FinanceiroVariavelPagamentoEmpresaInfo info = new FinanceiroVariavelPagamentoEmpresaInfo();
            FinanceiroVariavelPagamentoEmpresaInfo response = new FinanceiroVariavelPagamentoEmpresaInfo();

            if (id > 0)
            {
                info = FinanceiroVariavelPagamentoEmpresaBll.Instance.ListarPorParametros(new FinanceiroVariavelPagamentoEmpresaInfo { IdFinanceiroVariavelPagamento = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = FinanceiroVariavelPagamentoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdFinanceiroVariavelPagamento, "FinanceiroVariavelPagamento", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            FinanceiroVariavelPagamentoInfo info = new FinanceiroVariavelPagamentoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdFinanceiroVariavelPagamento = vm.IdFinanceiroVariavelPagamento;
            info.TipoVariavel = vm.TipoVariavel;
            info.Descricao = vm.Descricao;
            info.IdPlanoContasIn = vm.IdPlanoContasIn;
            info.TipoVariavelIn = vm.TipoVariavelIn;
            info.IdFinanceiroVariavelPagamentoIn = vm.IdFinanceiroVariavelPagamentoIn;
            info.IdFinanceiroClassificacaoIn = vm.IdFinanceiroClassificacaoIn;
            info.IdFinanceiroVariavelPagamentoNotIn = vm.IdFinanceiroVariavelPagamentoNotIn;
            info.Antecipacao = vm.Antecipacao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<FinanceiroVariavelPagamentoInfo> retorno = new ListPaged<FinanceiroVariavelPagamentoInfo>(FinanceiroVariavelPagamentoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Variáveis de Pagamento", true);
        }

    }
}
