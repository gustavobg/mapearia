﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Antecipacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroAntecipacao { get; set; }
        public int? Titulo { get; set; }
        public int? IdDocumentoTipo {get;set;}
        public int? TipoMovimentoFinanceiro { get; set; }

        public string IdFinanceiroFormaPagamentoIn { get; set; }
        public string IdPessoaIn { get; set; }
        public string IdProcessoSituacaoIn { get; set; }

        public string DataPrevistaInicio { get; set; }
        public string DataPrevistaFim { get; set; }
        public string DataEmissaoInicio { get; set; }
        public string DataEmissaoFim { get; set; }

        public string[] Situacao { get; set; }

        public bool? Ativo { get; set; }

    }
}