﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Antecipacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
        {
            TipoMovimentoFinanceiro = 1; // Por padrão Pagamento
            lstAntecipacaoClassificacaoContabil = new List<FinanceiroAntecipacaoClassificacaoContabilVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public long? Titulo { get; set; }

        [ViewModelToModelAttribute]
        public string Serie { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEndereco { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataPrevista { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorCotacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamentoPrevista { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaPagamentoDestinatario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaNatureza { get; set; }

        [ViewModelToModelAttribute]
        public string DocumentoPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartao { get; set; }

        [ViewModelToModelAttribute]
        public int? CodigoBarraTipo { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoBarra { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroClassificacao { get; set; }
        
        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<FinanceiroAntecipacaoClassificacaoContabilVM> lstAntecipacaoClassificacaoContabil { get; set; }

        #region Propriedades Especiais e Descrições

        [ViewModelToModelAttribute]
        public string DataPrevistaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDocumentoOrigem { get; set; }

        #endregion

        public class FinanceiroAntecipacaoClassificacaoContabilVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroAntecipacaoClassificacaoContabil { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroAntecipacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCentroCusto { get; set; }

            [ViewModelToModelAttribute]
            public int? Natureza { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorTitulo { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            #region Propriedades Especiais e Descrições

            [ViewModelToModelAttribute]
            public string NaturezaDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaMoeda { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCompletaPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public string ValorTituloExibicao { get; set; }

            #endregion
        }

    }
}