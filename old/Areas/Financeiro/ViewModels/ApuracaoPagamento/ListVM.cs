﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ApuracaoPagamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroModeloApuracaoPagamento { get; set; }

        public string IdFinanceiroModeloApuracaoPagamentoIn { get; set; }
        public string IdPessoaIn { get; set; }
        public string DataInicio { get; set; }
        public string DataFim { get; set; }
    }
}