﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ApuracaoPagamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Situacao = 1;
            lstVariavelPagamentoValor = new List<ApuracaoPagamentoValorVM>();
            Detalhe = new DetalheApuracaoVM();
            lstAbatimentoCredito = new List<AbatimentoCreditoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroApuracaoPagamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroModeloApuracaoPagamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataPagamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInicio { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataFim { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataReferencia { get; set; }

        [ViewModelToModelAttribute]
        public string MesAnoReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? Situacao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataRealizacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorApurado { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool ComDetalheApuracao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoDiferenca { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoBaixa { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorSaldoExcedido { get; set; }

        #region Propriedades Especiais e Descrições

        [ViewModelToModelAttribute]
        public string DescricaoDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DataPagamentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataInicioExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataFimExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataReferenciaExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        #endregion

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ApuracaoPagamentoValorVM> lstVariavelPagamentoValor { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public DetalheApuracaoVM Detalhe { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<AbatimentoCreditoVM> lstAbatimentoCredito { get; set; }

        public class ApuracaoPagamentoValorVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroApuracaoPagamentoValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroApuracaoPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public int? Referencia { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public bool UtilizaReferencia { get; set; }

            [ViewModelToModelAttribute]
            public decimal Valor { get; set; }

            [ViewModelToModelAttribute]
            public bool? Antecipacao { get; set; }

        }
        public class DetalheApuracaoVM
        {

            [ViewModelToModelAttribute]
            public int? IdFinanceiroApuracaoPagamentoDetalhe { get; set; }

            [ViewModelToModelAttribute]
            public string IdFinanceiroApuracaoPagamentoDetalheIn { get; set; }

            [ViewModelToModelAttribute]
            public string IdFinanceiroApuracaoPagamentoDetalheNotIn { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroApuracaoPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroFormaPagamento { get; set; }

            [ViewModelToModelAttribute]
            public bool? Realizado { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroConta { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoaDestinatarioPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? DocumentoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string Documento { get; set; }

            [ViewModelToModelAttribute]
            public string NumeroCheque { get; set; }
        }
        public class AbatimentoCreditoVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroTitulo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroAntecipacao { get; set; }

            // Descrição: Antecipação | Título
            [ViewModelToModelAttribute]
            public string DescricaoDocumentoFinanceiro { get; set; }

            [ViewModelToModelAttribute]
            public string Numero { get; set; }

            [ViewModelToModelAttribute]
            public string DataEmissaoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DataVencimentoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorBaixadoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorSaldoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorBaixarExibicao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorBaixar { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorBaixado { get; set; }
        }

    }
}