﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Cartao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroCartao { get; set; }

        public string IdPessoaTitularCartaoIn { get; set; }
        public string IdFinanceiroEntidadeIn { get; set; }
        public string BuscaDetalhada { get; set; }

        public bool? Ativo { get; set; }

    }
}