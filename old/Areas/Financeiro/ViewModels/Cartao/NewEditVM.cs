﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Cartao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroEntidade { get; set; }

        [ViewModelToModelAttribute]
        public string Numero { get; set; }

        [ViewModelToModelAttribute]
        public string Validade { get; set; }

        [ViewModelToModelAttribute]
        public int? CodigoSeguranca { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaTitularCartao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? LimiteUso { get; set; }

        [ViewModelToModelAttribute]
        public bool CartaoCredito { get; set; }

        [ViewModelToModelAttribute]
        public bool CartaoDebito { get; set; }

        [ViewModelToModelAttribute]
        public bool Principal { get; set; }

        [ViewModelToModelAttribute]
        public int? DiaVencimentoFatura { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}
