using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Classificacao
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
		{
		}

        public int? IdFinanceiroClassificacao { get; set; }
        public int? IdEmpresa { get; set; }
        public int? IdFinanceiroClassificacaoPai { get; set; }
        public int? IdPlanoContasVersao { get; set; }

        public string Descricao { get; set; }
        public string DescricaoCompleta { get; set; }
        public string Codigo { get; set; }
        public string IdFinanceiroClassificacaoIn { get; set; }
        public string IdFinanceiroClassificacaoNotIn { get; set; }
        public string IdFinanceiroClassificacaoPaiIn { get; set; }

        public bool? ComPai { get; set; }
        public bool? Ativo { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}