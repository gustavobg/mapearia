using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Classificacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
		{
		}

		[ViewModelToModelAttribute]
		public int? IdFinanceiroClassificacao { get; set; }

        [ViewModelToModelAttribute]
		public int? IdFinanceiroClassificacaoPai { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Mascara { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoasRestritas { get; set; }

        [ViewModelToModelAttribute]
		public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }
    }
}