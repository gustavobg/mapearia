﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ConciliacaoBancaria
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        #region Propriedades para exibição

        [ViewModelToModelAttribute]
        public string DataConciliacaoExibicao { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConciliacaoBancaria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMovimentacaoBancaria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroExtratoConta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataConciliacao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsFinanceiroMovimentacaoBancaria { get; set; }

    }
}