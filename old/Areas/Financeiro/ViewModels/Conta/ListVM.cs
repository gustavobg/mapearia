using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Conta
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {
        }

        public int? IdEmpresa { get; set; }
        public int? IdFinanceiroConta { get; set; }
        public int? IdFinanceiroContaTipo { get; set; }
        public int? IdPessoaTitularConta { get; set; }

        public bool? ContaPreferencial { get;set;}
        public bool? PorEmpresaUsuaria { get; set; }
        public bool? ComRestricaoConsultaUsuarioLogado { get; set; }
        public bool? Ativo { get; set; }

        public string Detalhe { get; set; }
        public string BuscaDetalhada { get; set; }
        public string IdFinanceiroContaNotIn { get; set; }
    }
}