using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Conta
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
		{
		}

		[ViewModelToModelAttribute]
		public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroContaTipo { get; set; }

        [ViewModelToModelAttribute]
		public int? IdFinanceiroEntidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaTitularConta { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
		public string Agencia { get; set; }

        [ViewModelToModelAttribute]
        public string AgenciaDigito { get; set; }

        [ViewModelToModelAttribute]
		public string Conta { get; set; }

        [ViewModelToModelAttribute]
        public string ContaDigito { get; set; }

        [ViewModelToModelAttribute]
        public bool ContaPreferencial { get; set; }
        
        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool CaixaInternoContaTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaConsultaRestricao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaMovimentacaoRestricao { get; set; }
    }
}