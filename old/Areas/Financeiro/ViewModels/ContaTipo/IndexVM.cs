using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContaTipo
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url)
        {
        }
    }
}