using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContaTipo
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
		{ 
        }

        public int? IdFinanceiroContaTipo { get; set; }
        public int? IdEmpresa { get; set; }
        public int? Origem { get; set; }

        public string Descricao { get; set; }

        public bool? CaixaInterno { get; set; }
        public bool? Ativo { get; set; }
    }
}