using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContaTipo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
		{
		}

		[ViewModelToModelAttribute]
		public int? IdFinanceiroContaTipo { get; set; }

        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
		public bool CaixaInterno { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }
    }
}