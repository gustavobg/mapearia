﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContratoOperadoraCartao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroCartaoContratoOperadora { get; set; }
        public int? IdFinanceiroCartaoContratoOperadoraEquipamento { get; set; } // Utilizado no método ListEquipamento

        public string Descricao { get; set; }
        public string DescricaoContratoBusca { get; set; }
        public string IdFinanceiroEntidadeIn { get; set; }

        public bool? Ativo { get; set; }
    }
}