﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ContratoOperadoraCartao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() {
            lstEquipamento = new List<FinanceiroContratoOperadoraEquipamentoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartaoContratoOperadora { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroEntidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string NumeroContrato { get; set; }

        [ViewModelToModelAttribute]
        public bool Principal { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<FinanceiroContratoOperadoraEquipamentoVM> lstEquipamento { get; set; }

        public class FinanceiroContratoOperadoraEquipamentoVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroCartaoContratoOperadoraEquipamento { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroCartaoContratoOperadora { get; set; }

            [ViewModelToModelAttribute]
            public string Codigo { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public bool Principal { get; set; }

            [ViewModelToModelAttribute]
            public bool Ativo { get; set; }
        }

    }
}