﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Cotacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroCotacaoMoeda { get; set; }
        public int? Natureza { get; set; }
        public int? PeriodoRapido { get; set; }
        public int? IdFinanceiroMoeda { get; set; }
        public int? IdFinanceiroMoedaReferencia { get; set; }

        public string DescricaoDetalhadaBusca { get; set; }

        public bool? Ativo { get; set; }
        public bool? Origem { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}