﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Cotacao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCotacaoMoeda { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaReferencia { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? Data { get; set; }

        [ViewModelToModelAttribute]
        public string DataExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorCotacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public int CasasDecimais { get; set; }

    }
}