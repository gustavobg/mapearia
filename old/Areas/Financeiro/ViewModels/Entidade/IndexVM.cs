using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Entidade
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url)
        {
        }
    }
}