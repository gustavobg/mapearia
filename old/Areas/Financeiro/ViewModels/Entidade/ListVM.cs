using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Entidade
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM()
		{
		}

        public string Descricao { get; set; }
        public string DescricaoCodigo { get; set; }
        public string DescricaoComposta { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
        public bool SemEntidadesDoSistema { get; set; }
        public bool? OperadoraCartao { get; set; }

        public int? IdFinanceiroContaTipo { get; set; }
        public int? IdFinanceiroEntidade { get; set; }
        public int? IdEmpresa { get; set; }
    }
}