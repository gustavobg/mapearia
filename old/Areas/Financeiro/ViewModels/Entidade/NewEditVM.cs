using HTM.MasterGestor.Web.UI.Infrastructure;
using System;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Entidade
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() 
		{
            //Como padr�o retornar 1 == Brasil
            IdPais = 1;
		}

		[ViewModelToModelAttribute]
		public int? IdFinanceiroEntidade { get; set; }

        [ViewModelToModelAttribute]
		public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPais { get; set; }

        [ViewModelToModelAttribute]
		public string Codigo { get; set; }
        
        [ViewModelToModelAttribute]
		public string Descricao { get; set; }

        [ViewModelToModelAttribute]
		public string DescricaoResumida { get; set; }

        [ViewModelToModelAttribute]
        public bool OperadoraCartao { get; set; }

        [ViewModelToModelAttribute]
		public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
		public Guid? ChaveVersao { get; set; }
    }
}