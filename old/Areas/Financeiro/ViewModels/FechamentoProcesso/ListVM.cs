﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FechamentoProcesso
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdPessoa { get; set; } //Empresa Usuária
        public string BuscaAvancada { get; set; }

        public int? IdCategoriaFechamentoProcesso { get; set; }
    }
}
