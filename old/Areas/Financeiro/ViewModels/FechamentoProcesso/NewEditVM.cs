﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FechamentoProcesso
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFechamentoProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaFechamentoProcesso { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHoraFechamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaResponsavel { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHoraOperacao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHoraReAberto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaReAbertura { get; set; }

        [ViewModelToModelAttribute]
        public string Justificativa { get; set; }

        [ViewModelToModelAttribute]
        public string DataFechamentoAnterior { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string DataFechamentoExibicao { get; set; }

    }
}