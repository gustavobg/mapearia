﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FormaPagamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroFormaPagamento { get; set; }

        public string Descricao { get; set; }
        public string IdFinanceiroFormaPagamentoIn { get; set; }

        public bool? Cobranca { get; set; }
        public bool? Bordero { get; set; }
        public bool? Antecipacao { get; set; }
        public bool? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }
}