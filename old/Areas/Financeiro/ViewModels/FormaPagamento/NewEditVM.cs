﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.FormaPagamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Cobranca = true;
            Bordero = true;
            Titulo = true;
            Antecipacao = true;
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamento { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? NaturezaFinanceira { get; set; }

        [ViewModelToModelAttribute]
        public bool? Cobranca { get; set; }

        [ViewModelToModelAttribute]
        public bool? Bordero { get; set; }

        [ViewModelToModelAttribute]
        public bool? Antecipacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Titulo { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsItens { get; set; }
    }
}