﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ItemUtilizacao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroItemUtilizacao { get; set; }

        public string IdFinanceiroItemUtilizacaoIn { get; set; }
        public string Descricao { get; set; }

        public bool? Ativo { get; set; }

    }
}