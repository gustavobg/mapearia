﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ModeloApuracaoPagamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroModeloApuracaoPagamento { get; set; }
        public int? IdDocumentoTipo { get; set; }

        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool ComVariaveis { get; set; }
        public bool? Ativo { get; set; }
    }
}