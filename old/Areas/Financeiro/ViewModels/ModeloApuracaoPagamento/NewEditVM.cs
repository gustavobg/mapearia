﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.ModeloApuracaoPagamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstVarialPagamento = new List<ItemVariavelPagamentoVM>();
            Antecipacao = false;
            TipoMovimentoFinanceiro = 1; // Por Padrão "A Pagar"
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroModeloApuracaoPagamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdSubProcesso { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoPeriodoReferencia { get; set; }

        [ViewModelToModelAttribute]
        public bool? ExigeAprovacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? NaturezaApuracao { get; set; }

        [ViewModelToModelAttribute]
        public bool Antecipacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaPerfil { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemVariavelPagamentoVM> lstVarialPagamento { get; set; }

        public class ItemVariavelPagamentoVM
        {

            [ViewModelToModelAttribute]
            public int? IdFinanceiroVariavelPagamentoModelo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroModeloApuracaoPagamento { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public bool? UtilizaReferencia { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public string IdsFinanceiroVariavelPagamentoRestricao { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoFinanceiroVariavelPagamento { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoTipoVariavelPagamento { get; set; }
        }

    }
}