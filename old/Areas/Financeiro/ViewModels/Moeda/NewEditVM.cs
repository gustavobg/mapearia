﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Moeda
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            OrigemInformacao = 1;
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public int? UnidadeInformacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProdutoServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEstado { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public int? CasasDecimais { get; set; }

        [ViewModelToModelAttribute]
        public string Local { get; set; }

        [ViewModelToModelAttribute]
        public int? OrigemInformacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        public bool? ExibeOrigemInformacao { get; set; }
    }
}