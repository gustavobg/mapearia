﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.MovimentacaoBancaria
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroConta { get; set; }
        public int? Origem { get; set; }

        public string IdFinanceiroContaIn { get; set; }
        public string DataMovimentacaoInicio { get; set; }
        public string DataMovimentacaoFim { get; set; }

        public string[] Natureza { get; set; }
        public string[] TipoConciliacao { get; set; }
        public string[] ConciliacaoIn { get; set; }
    }
}