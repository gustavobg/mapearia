﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.MovimentacaoBancaria
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            TipoHistorico = 1;
            Natureza = 1;
            lstTransferencia = new List<DetalhesTransferenciaVM>();
            lstClassificacaoContabil = new List<ClassificacaoContabilVM>();
            lstItens = new List<MovimentacaoItensVM>();
        }

        #region Versões para Plano de Contas | Centro de Custo | Classificacao Financeira

        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersaoCentroCusto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersaoPlanoContas { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasClassificacaoFinanceira { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMovimentacaoBancaria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamento { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorBaixado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaOperacaoBaixa { get; set; }

        [ViewModelToModelAttribute]
        public int? Origem { get; set; }

        [ViewModelToModelAttribute]
        public int? Natureza { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string NumeroDocumento { get; set; }

        [ViewModelToModelAttribute]
        public string Documento { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoHistorico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMovimentacaoContaHistorico { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoHistoricoManual { get; set; }
        
        [ViewModelToModelAttribute]
        public DateTime? DataMovimentacao { get; set; }

        [ViewModelToModelAttribute]
        public string DataMovimentacaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? CasasDecimaisMoedaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string SiglaMoeda { get; set; }

        [ViewModelToModelAttribute]
        public bool? Conciliado { get; set; }

        [ViewModelToModelAttribute]
        public string DocumentoOrigem { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<DetalhesTransferenciaVM> lstTransferencia { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ClassificacaoContabilVM> lstClassificacaoContabil { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<MovimentacaoItensVM> lstItens { get; set; }

        public class DetalhesTransferenciaVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroMovimentacaoBancariaTransferencia { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroMovimentacaoBancaria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroConta { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPessoa { get; set; } //Empresa Usuária

            [ViewModelToModelAttribute]
            public decimal? Valor { get; set; }

            [ViewModelToModelAttribute]
            public string DetalheFinanceiroConta { get; set; }

            [ViewModelToModelAttribute]
            public string NomePessoa { get; set; }

            [ViewModelToModelAttribute]
            public string ValorExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaMoeda { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisMoedaEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

        }

        public class ClassificacaoContabilVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroMovimentacaoBancariaClassificacaoContabil { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroMovimentacaoBancaria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCentroCusto { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Valor { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

            #region Descrições e propriedades Especiais

            [ViewModelToModelAttribute]
            public string DescricaoPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCentroCusto { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimaisMoedaEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public string SiglaMoeda { get; set; }

            [ViewModelToModelAttribute]
            public string ValorExibicao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPlanoContasVersaoCentroCusto { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPlanoContasVersaoPlanoContas { get; set; }

            #endregion
        }

        public class MovimentacaoItensVM
        {
            [ViewModelToModelAttribute]
            public string DataHoraBaixa { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }

            [ViewModelToModelAttribute]
            public string DocumentoFinanceiroBaixa { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoTipoMovimentoFinanceiro { get; set; }

            [ViewModelToModelAttribute]
            public string ValorAtual { get; set; }

            [ViewModelToModelAttribute]
            public string ValorDesconto { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMulta { get; set; }

            [ViewModelToModelAttribute]
            public string ValorJuro { get; set; }

            [ViewModelToModelAttribute]
            public string ValorVariacaoCambial { get; set; }

            [ViewModelToModelAttribute]
            public string TotalBaixado { get; set; }
        }
    }
}