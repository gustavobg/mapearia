﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.MovimentacaoContaHistorico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroMovimentacaoContaHistorico { get; set; }

        public string Descricao { get; set; }

        public bool? TransferenciaEntreContas { get; set; }
        public bool? TransferenciaEntreEntidadesIguais { get; set; }
        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

    }
}