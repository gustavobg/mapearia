﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento
{
    public class DetalheAntecipacaoVM: VMNewEditBase
    {
        public DetalheAntecipacaoVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroAntecipacaoDetalhe { get;set; }

        [ViewModelToModelAttribute]
        public int?  IdFinanceiroAntecipacao { get;set; }

        [ViewModelToModelAttribute]
        public int?  IdFinanceiroFormaPagamento { get;set; }

        [ViewModelToModelAttribute]
        public int?  IdFinanceiroMoedaValorAntecipacao { get;set; }

        [ViewModelToModelAttribute]
        public int?  IdFinanceiroMoedaValorAtual { get;set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorCotacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorVariacaoCambial { get; set; }

        [ViewModelToModelAttribute]
        public decimal? NaturezaVariacaoCambial { get;set; }

        [ViewModelToModelAttribute]
        public decimal? Total { get;set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHora { get;set; }

        [ViewModelToModelAttribute]
        public bool? BaixaRealizada { get;set; }

        #region Informação Básicas (Dados da Antecipação)

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public string NomePessoa { get; set; }

        [ViewModelToModelAttribute]
        public string MunicipioPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string DataEmissaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataVencimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string Titulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public string ValorOriginalAntecipacao { get; set; }

        [ViewModelToModelAttribute]
        public string TotalBaixadoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAPagarExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataLimiteDesconto { get; set; }

        #endregion

        #region Saldos

        [ViewModelToModelAttribute]
        public string SaldoEmAberto { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAtualExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAntecipacaoesExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoCreditosExibicao { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public bool PossuiBaixas { get; set; }

    }
}