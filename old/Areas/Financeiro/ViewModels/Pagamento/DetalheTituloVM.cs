﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento
{
    public class DetalheTituloVM : VMNewEditBase
    {
        public DetalheTituloVM()
        {
            lstPagamentoRealizado = new List<PagamentoRealizadoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroTituloDetalhe { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorOriginalTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorOriginalTitulo { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorCotacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorJuro { get; set; }

        [ViewModelToModelAttribute]
        public decimal PercDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal PercMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal PercJuro { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorVariacaoCambial { get; set; }

        [ViewModelToModelAttribute]
        public int? NaturezaVariacaoCambial { get; set; }

        [ViewModelToModelAttribute]
        public decimal? Total { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataHora { get; set; }

        [ViewModelToModelAttribute]
        public bool BaixaRealizada { get; set; }

        [ViewModelToModelAttribute]
        public bool PossuiBaixas { get; set; }

        #region Informações Basicas do Título

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public string NomePessoa { get; set; }

        [ViewModelToModelAttribute]
        public string MunicipioPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string DataEmissaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataVencimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string Titulo { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string ValorOriginalTituloExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string TotalBaixadoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAPagarExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataLimiteDesconto { get; set; }

        #endregion

        #region Descrições e Saldos

        [ViewModelToModelAttribute]
        public string SaldoAtualExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAntecipacaoesExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoCreditosExibicao { get; set; }

        #endregion

        List<PagamentoRealizadoVM> lstPagamentoRealizado = new List<PagamentoRealizadoVM>();

        public class PagamentoRealizadoVM
        {
            [ViewModelToModelAttribute]
            public string DataBaixa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroConta { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroFormaPagamento { get; set; }

            [ViewModelToModelAttribute]
            public string IdDocumentoTipo { get; set; }

            [ViewModelToModelAttribute]
            public string Titulo { get; set; }

            [ViewModelToModelAttribute]
            public int? CasasDecimais { get; set; }

            [ViewModelToModelAttribute]
            public decimal? Valor { get; set; }

            [ViewModelToModelAttribute]
            public string ValorExibicao { get; set; }
        }
    }
}