﻿using HTM.MasterGestor.Model.Financeiro;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento
{
    public class DetalhesPagamentoVM : VMNewEditBase
    {
        public DetalhesPagamentoVM()
        {
            lstPagamentoRealizado = new List<PagamentoRealizadoVM>();
        }

        [ViewModelToModelAttribute]
        public int? ID { get; set; }

        [ViewModelToModelAttribute]
        public long? Titulo { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public string ChaveComposta { get; set; }

        [ViewModelToModelAttribute]
        public string DataVencimento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroTituloTemp { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroAntecipacaoTemp { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeCamposAuxiliares { get; set; }

        /// <summary>
        /// Tipo 1 = Titulo | Tipo 2 = Antecipação | Tipo 3 = Titulo e Antecipação
        /// </summary>
        [ViewModelToModelAttribute]
        public TipoDocumentoFinanceiroEnum? Tipo { get; set; }

        #region Dados Pessoa

        [ViewModelToModelAttribute]
        public string NomePessoa { get; set; }

        [ViewModelToModelAttribute]
        public string MunicipioPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string EmissaoDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamentoPrevista { get; set; }

        [ViewModelToModelAttribute]
        public string LimiteDesconto { get; set; }

        [ViewModelToModelAttribute]
        public string DataEmissaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAtualExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAntecipacaoesExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoCreditosExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? CasasDecimaisFinanceiroMoeda { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<PagamentoRealizadoVM> lstPagamentoRealizado { get; set; }

        #endregion

        #region Valores

        [ViewModelToModelAttribute]
        public decimal? ValorPagamentoParcial { get; set; } 

        [ViewModelToModelAttribute]
        public string ValorOriginalTituloExibicao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorOriginalTitulo { get; set; }

        [ViewModelToModelAttribute]
        public string TotalBaixadoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string ValorAtualExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string ValorTituloExibicao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoAPagar { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAPagarExibicao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? TotalBaixado { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorSaldoAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoEmAberto { get; set; }

        [ViewModelToModelAttribute]
        public decimal ValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorJuros { get; set; }

        [ViewModelToModelAttribute]
        public string ValorBaixadoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorTotal { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorBaixar { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualJuro { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualBaixado { get; set; }



        #endregion

        public class PagamentoRealizadoVM
        {
            [ViewModelToModelAttribute]
            public string ValorAtual { get; set; }

            [ViewModelToModelAttribute]
            public string ValorDesconto { get; set; }

            [ViewModelToModelAttribute]
            public string ValorMulta { get; set; }

            [ViewModelToModelAttribute]
            public string ValorJuro { get; set; }

            [ViewModelToModelAttribute]
            public string ValorVariacaoCambial { get; set; }

            [ViewModelToModelAttribute]
            public string TotalBaixado { get; set; }

            [ViewModelToModelAttribute]
            public string DocumentoFinanceiroBaixa { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoTipoMovimentoFinanceiro { get; set; }

            [ViewModelToModelAttribute]
            public string NomeSecundarioPessoa { get; set; }

            [ViewModelToModelAttribute]
            public string DataHoraBaixa { get; set; }
        }
    }

}