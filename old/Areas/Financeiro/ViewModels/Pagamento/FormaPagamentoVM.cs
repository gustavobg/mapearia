﻿using HTM.MasterGestor.Model.Financeiro;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento
{
    public class FormaPagamentoTituloVM : VMNewEditBase
    {
        public FormaPagamentoTituloVM()
        {
            lstAbatimentoCredito = new List<AbatimentoCreditoVM>();
        }

        #region Propriedades Cabeçalho

        //Quantidade de Itens Selecinados de um total ex:
        [ViewModelToModelAttribute]
        public int? QuantidadeSelecionados { get; set; }

        [ViewModelToModelAttribute]
        public int? TotalFiltrados { get; set; }

        [ViewModelToModelAttribute]
        public string DataBaixa { get; set; }

        [ViewModelToModelAttribute]
        public decimal? TotalBaixar { get; set; }

        [ViewModelToModelAttribute]
        public string[] ItensSelecionados { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoMovimentacao { get; set; }

        /// <summary>
        /// TipoOperacao 1 = A Pagar | TipoOperacao 2 = A Receber  
        /// </summary>
        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        /// <summary>
        /// Tipo 1 = Titulo | Tipo 2 = Antecipação | Tipo 3 = Titulo e Antecipação
        /// </summary>
        [ViewModelToModelAttribute]
        public TipoDocumentoFinanceiroEnum Tipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamentoPrevista { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string NomePessoa { get; set; }

        public string MunicipioPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoAntecipacoesExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string SaldoCreditosExibicao { get; set; }

        #endregion

        #region Forma de Pagamento (ou Recebimento)

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartaoContratoOperadora { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartaoContratoOperadoraEquipamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroContaOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroContaDestino { get; set; }

        [ViewModelToModelAttribute]
        public string DocumentoTransferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdTransacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string NumeroDocumento { get; set; }

        [ViewModelToModelAttribute]
        public int? ChequeDetalheTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartao { get; set; }

        [ViewModelToModelAttribute]
        public int? CodigoBarraTipo { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoBarra { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoDiferenca { get; set; }

        [ViewModelToModelAttribute]
        public decimal? SaldoBaixa { get; set; }

        #endregion

        #region Propriedades para Saldo Excedido
        
        [ViewModelToModelAttribute]
        public int? IdTransacaoSaldoExcedido { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipoSaldoExcedido { get; set; }

        [ViewModelToModelAttribute]
        public int? CodigoSaldoExcedido { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorSaldoExcedido { get; set; }

        #endregion

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<AbatimentoCreditoVM> lstAbatimentoCredito { get; set; } 

        public class AbatimentoCreditoVM
        {
            [ViewModelToModelAttribute]
            public int? IdFinanceiroTitulo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroAntecipacao { get; set; }

            // Descrição: Antecipação | Título
            [ViewModelToModelAttribute]
            public string DescricaoDocumentoFinanceiro { get; set; }

            [ViewModelToModelAttribute]
            public string Numero { get; set; }

            [ViewModelToModelAttribute]
            public string DataEmissaoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string DataVencimentoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorBaixadoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorSaldoExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string ValorBaixarExibicao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorBaixar { get; set; }
        }
    }
}