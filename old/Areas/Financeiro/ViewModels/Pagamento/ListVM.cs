﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Pagamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdDocumentoTipo { get; set; }
        public int? TipoMovimentoFinanceiro { get; set; } //TipoMovimentoFinanceiro == 1 Pagamento | TipoMovimentoFinanceiro == 2 Recebimento
        public int? IdPessoa { get; set; } // Tabela de Título = IdPessoa // Tabela de Antecipação = IdPessoa

        public long? Titulo { get; set; }

        public string IdFinanceiroFormaPagamentoPrevistaIn { get; set; }
        public string IdPessoaIn { get; set; }
        public string DataVencimentoInicio { get; set; }
        public string DataVencimentoFim { get; set; }
        public string DataEmissaoInicio { get; set; }
        public string DataEmissaoFim { get; set; }

        public string[] Situacao { get; set; }
        public string[] IdProcessoSituacaoIn { get; set; }
        public string[] TipoBusca { get; set; }

    }
}