﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.SaldoContaCaixa
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdFinanceiroContaTipo { get; set; }

        public string[] Natureza { get; set; }
        public string BuscaAvancada { get; set; }
        public string IdFinanceiroContaIn { get; set; }
        public string IdFinanceiroEntidadeIn { get; set; }
        public string IdPessoaIn { get; set; }


        public string DataMovimentoInicio { get; set; }
        public string DataMovimentoFim { get; set; }

    }
}