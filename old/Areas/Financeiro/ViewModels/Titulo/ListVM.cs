﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Titulo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdTransacao { get; set; }

        public int? Titulo { get; set; }
        public int? TipoMovimentoFinanceiro { get; set; }
        public int? IdPessoa { get; set; }
        
        public string IdProcessoSituacaoIn { get; set; }
        public string IdFinanceiroFormaPagamentoPrevistaIn { get; set; }
        public string DataEmissaoInicio { get; set; }
        public string DataEmissaoFim { get; set; }
        public string DataVencimentoInicio { get; set; }
        public string DataVencimentoFim { get; set; }
        public string[] Situacao { get; set; }

        public string valor { get; set; }
        public string percentual { get; set; }

    }
}