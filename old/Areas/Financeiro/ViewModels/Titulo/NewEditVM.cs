﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.Titulo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            CodigoBarraTipo = 1;
            TipoMovimentoFinanceiro = 1; // Por Padrão, Pagamento
            lstClassificacaoContabil = new List<FinanceiroTituloClassificacaoContabilVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoMovimentoFinanceiro { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public long? Titulo { get; set; }

        [ViewModelToModelAttribute]
        public string Serie { get; set; }

        [ViewModelToModelAttribute]
        public string Parcela { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProcessoSituacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEndereco { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataLancamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataEmissao { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataVencimento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataVencimentoOriginal { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataLimiteDesconto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipoOrigem { get; set; }

        [ViewModelToModelAttribute]
        public string NumeroOrigem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorTitulo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorAtual { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorCotacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorJuros { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorBaixado { get; set; }

        //Apenas para TESTEEEEEEEE
        [ViewModelToModelAttribute]
        public decimal? ValorBaixado2 { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualDesconto { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualMulta { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PercentualJuro { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdFinanceiroFormaPagamentoPrevista { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaPagamentoDestinatario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaNatureza { get; set; }

        [ViewModelToModelAttribute]
        public string DocumentoPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroConta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroCartao { get; set; }

        [ViewModelToModelAttribute]
        public int? CodigoBarraTipo { get; set; }

        [ViewModelToModelAttribute]
        public string CodigoBarra { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoGuiaRecolhimento { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Competencia { get; set; }

        [ViewModelToModelAttribute]
        public string Identificador { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaValorBaseCalculo { get; set; }

        [ViewModelToModelAttribute]
        public decimal? ValorBaseCalculo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroClassificacao { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<FinanceiroTituloClassificacaoContabilVM> lstClassificacaoContabil { get; set; }

        #region Propriedades Especiais e Descrições

        [ViewModelToModelAttribute]
        public int? IdFinanceiroMoedaPadrao { get; set; }

        [ViewModelToModelAttribute]
        public string NomePessoaCliente { get; set; }

        [ViewModelToModelAttribute]
        public string DataLancamentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataEmissaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataVencimentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataVencimentoOriginalExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataLimiteDescontoExibicao { get; set; }

        #endregion

        public class FinanceiroTituloClassificacaoContabilVM
        {

            [ViewModelToModelAttribute]
            public int? IdFinanceiroTituloClassificacaoContabil { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroTitulo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public int? Natureza { get; set; }

            [ViewModelToModelAttribute]
            public int? IdFinanceiroMoeda { get; set; }

            [ViewModelToModelAttribute]
            public int? IdCentroCusto { get; set; }

            [ViewModelToModelAttribute]
            public decimal? ValorTitulo { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoCompletaPlanoContas { get; set; }

            [ViewModelToModelAttribute]
            public string ValorTituloExibicao { get; set; }

            [ViewModelToModelAttribute]
            public string NaturezaDescricao { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }

        }

    }
}