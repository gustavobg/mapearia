﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.VariavelPagamento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdFinanceiroVariavelPagamento { get; set; }
        public int? TipoVariavel { get; set; }

        public string Descricao { get; set; }
        public string IdPlanoContasIn { get; set; }
        public string TipoVariavelIn { get; set; }
        public string IdFinanceiroVariavelPagamentoIn { get; set; }
        public string IdFinanceiroClassificacaoIn { get; set; }
        public string IdFinanceiroVariavelPagamentoNotIn { get; set; }

        public bool? Antecipacao { get; set; }
        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}