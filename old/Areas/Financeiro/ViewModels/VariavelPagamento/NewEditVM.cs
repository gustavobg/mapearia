﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Financeiro.ViewModels.VariavelPagamento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            Antecipacao = false;
        }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroVariavelPagamento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoVariavel { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasFiscalDebito { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasFiscalCredito { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasGerencialDebito { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPlanoContasGerencialCredito { get; set; }

        [ViewModelToModelAttribute]
        public int? IdFinanceiroClassificacao { get; set; }

        /// <summary>
        /// Apenas para auxilio no filtro de Classificação Financeira
        /// </summary>
        [ViewModelToModelAttribute]
        public int? IdPlanoContasVersao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Antecipacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

    }
}
