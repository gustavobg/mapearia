﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.Estado;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Localizacao;
using HTM.MasterGestor.Bll.Localizacao;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.Controllers
{
    public class EstadoController : ControllerExtended
    {
        public EstadoController()
        {
            IndexUrl = "/Localizacao/Estado/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            EstadoInfo info = new EstadoInfo();
            ViewModelToModelMapper.Map<EstadoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = EstadoBll.Instance.Salvar(info);
            SalvarHistoricoAcesso(IndexUrl, vm.IdEstado.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdEstado, "Estado", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                EstadoInfo info = new EstadoInfo();
                info = EstadoBll.Instance.ListarPorCodigo(id.Value);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdEstado, "Estado");
            }

            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            EstadoInfo info = new EstadoInfo();

            info.IdEstado = vm.IdEstado;
            info.Sigla = vm.Sigla;
            info.NomeResumido = vm.NomeResumido;

            ListPaged<EstadoInfo> retorno = new ListPaged<EstadoInfo>(EstadoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Estado", true);
        }
    }
}
