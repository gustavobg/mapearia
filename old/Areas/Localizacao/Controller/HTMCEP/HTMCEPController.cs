﻿using HTM.MasterGestor.Bll.Localizacao;
using HTM.MasterGestor.Model.Localizacao;
using HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.HTMCEP;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.Controller.HTMCEP
{
    public class HTMCEPController : ControllerExtended
    {
        public HTMCEPController()
        {
            IndexUrl = "/Localizacao/HTMCEP/Index";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult List(ListVM vm)
        {
            LogradouroInfo info = new LogradouroInfo();

            List<LogradouroInfo> retorno = LogradouroBll.Instance.ListaLogradouroPorCEP(info.CEP);

            return base.CreateListResult(vm, retorno, "Exportação: CEP");
        }

        public ActionResult BuscaCEP(string cep)
        {
            ListVM vm = new ListVM();

            string cepN = cep.Replace("-", "").Replace(".", "");
            List<LogradouroInfo> retorno = LogradouroBll.Instance.ListaLogradouroPorCEP(cepN);

            return base.CreateListResult(vm, retorno, "Exportação: CEP");

        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        public ActionResult BuscaTipoLogradouro(ListVM vm)
        {
            LogradouroInfo info = new LogradouroInfo();
            info.IdTipoLogradouro = vm.IdTipoLogradouro;
            info.DescricaoTipoLogradouro = vm.DescricaoTipoLogradouro;

            List<LogradouroInfo> retorno = LogradouroBll.Instance.ListaTiposLogradouro(info);
            return base.CreateListResult(vm, retorno, "Exportação: Tipos de Logradouro");
        }

        public ActionResult BuscaLocalidadesPorEstado(int idEstado)
        {
            ListVM vm = new ListVM();
            List<LogradouroInfo> retorno = LogradouroBll.Instance.ListaLocalidadesPorEstado(idEstado);

            return base.CreateListResult(vm, retorno, "Exportação: Localidades por Estado");

        }

        //public ActionResult BuscaLocalidade(int IdLocalidade)
        //{
        //    ListVM vm = new ListVM();
        //    vm.IdLocalidade = 26;
        //    var a = vm.IdLocalidadeIn;
        //    DTOHTMCEPPesquisarLocalidade dto = new DTOHTMCEPPesquisarLocalidade();
        //    dto.IdLocalidade = vm.IdLocalidade;
        //    List<Entity.Localizacao.HTMCEP> retorno = bll.HTMCEPPesquisarLocalidadePorId(dto);

        //    return base.CreateListResult(vm, retorno, "Exportação: Contato");

        //}

        //public List<SelectListItem> BuscaListaTipoLogradouro()
        //{
        //    List<SelectListItem> lst = new List<SelectListItem>();
        //    DTOHTMCEPPesquisarTipoLogradouro dto = new DTOHTMCEPPesquisarTipoLogradouro();

        //    List<Entity.Localizacao.HTMCEP> retorno = bll.HTMCEPPesquisarTipoLogradouro(dto);

        //    foreach (var item in retorno)
        //    {
        //        lst.Add(new SelectListItem { Text = item.DescricaoTipoLogradouro, Value = item.IdTipoLogradouro.ToString() });
        //    }

        //    return lst;
        //}
    }
}