﻿using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.Localidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Localizacao;
using HTM.MasterGestor.Bll.Localizacao;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.Controllers
{
    public class LocalidadeController : ControllerExtended
    {
        public LocalidadeController()
        {
            IndexUrl = "/Localizacao/Localidade/Index";
        }

        //[CustomAuthorize, ExceptionFilter]
        //public ActionResult Index()
        //{
        //    IndexVM vm = new IndexVM(IndexUrl);
        //    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
        //    return View(vm);
        //}

        //[HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        //public ActionResult NewEdit(NewEditVM vm)
        //{
        //    DTOSalvarLocalidade dto = new DTOSalvarLocalidade();
        //    ViewModelToModelMapper.Map<Localidade>(vm, dto.Entidade);

        //    dto.IdPessoaOperacao = IdPessoa;
        //    dto.IdEmpresa = IdEmpresa.Value;

        //    DTOSalvarLocalidadeResponse response = bll.SalvarLocalidade(dto);
        //    SalvarHistoricoAcesso(IndexUrl, vm.IdLocalidade.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdLocalidade, dto.Entidade.ToString(), response.IdHistorico);

        //    return Json(response);
        //}

        //[HttpGet, CustomAuthorize]
        //public ActionResult NewEdit(int? id)
        //{
        //    NewEditVM vm = new NewEditVM();

        //    if (id.HasValue && id > 0)
        //    {
        //        Localidade entidade = bll.AdquirirLocalidade(new DTOAdquirirLocalidade() { IdLocalidade = id }).FirstOrDefault();
        //        ViewModelToModelMapper.MapBack<NewEditVM>(vm, entidade);

        //      SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, entidade.IdLocalidade, entidade.ToString());
        //    }

        //    return View(vm);
        //}

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            LocalidadeInfo info = new LocalidadeInfo();

            info.IdEstado = vm.IdEstado;
            info.IdLocalidade = vm.IdLocalidade;
            info.NomeResumido = vm.NomeResumido;

            ListPaged<LocalidadeInfo> retorno = new ListPaged<LocalidadeInfo>(LocalidadeBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Localidade", true);
        }

    }
}
