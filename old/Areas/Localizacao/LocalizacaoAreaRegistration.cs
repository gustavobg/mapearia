﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao
{
    public class ConfiguracaoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Localizacao";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Localizacao_default",
                "Localizacao/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
