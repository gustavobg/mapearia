﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.Estado
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdEstado { get; set; }
        public string Sigla { get; set; }
        public string NomeResumido { get; set; }

    }
}