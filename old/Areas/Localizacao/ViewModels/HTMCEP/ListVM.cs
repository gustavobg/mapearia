﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.HTMCEP
{
    public class ListVM : ViewModelListRequestBase
    {
        public string CEP { get; set; }
        public int? IdEstado { get; set; }
        public int? IdLocalidade{ get; set; }
        public int? IdTipoLogradouro { get; set; }

        public string IdLocalidadeIn { get; set; }
        public string DescricaoTipoLogradouro { get; set; }
    }
}