﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Localizacao.ViewModels.Localidade
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdLocalidade { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdEstado { get; set; }

        [ViewModelToModelAttribute]
        public String Nome { get; set; }

        [ViewModelToModelAttribute]
        public String NomeResumido { get; set; }

        [ViewModelToModelAttribute]
        public String Sigla { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdLocalidadeSubordinacao { get; set; }

        [ViewModelToModelAttribute]
        public Int32 CodigoIBGE { get; set; }

        [ViewModelToModelAttribute]
        public String Tipo { get; set; }

        [ViewModelToModelAttribute]
        public Boolean ListaTodosLogradouros { get; set; }


    }
}