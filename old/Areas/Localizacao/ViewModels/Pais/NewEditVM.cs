﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.localizacao.ViewModels.Pais
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdPais { get; set; }

        [ViewModelToModelAttribute]
        public Int32? CodigoPais { get; set; }

        [ViewModelToModelAttribute]
        public String NomeResumido { get; set; }

        [ViewModelToModelAttribute]
        public String Nome { get; set; }

        [ViewModelToModelAttribute]
        public Int32? DDI { get; set; }

        [ViewModelToModelAttribute]
        public String MascaraCEP { get; set; }

        [ViewModelToModelAttribute]
        public String ChaveTexto { get; set; }

        [ViewModelToModelAttribute]
        public String Sigla { get; set; }


    }
}