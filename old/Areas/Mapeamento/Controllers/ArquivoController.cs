﻿using HTM.MasterGestor.Bll.Core;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Arquivo;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO.Compression;
using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Model.Mapeamento;
using System.Transactions;
using System.IO;
using System.ComponentModel.DataAnnotations;
using NetTopologySuite.IO;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class ArquivoController : ControllerExtended
    {
        private const string CAMINHO_MAPEAMENTO_TEMP = "~/temp/Mapeamento/";
        private const string CAMINHO_MAPEAMENTO = "~/Mapeamento/";

        public ArquivoController()
        {
            IndexUrl = "/Mapeamento/Arquivo/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region Shape

        [HttpPost]
        public ActionResult UploadShape(HttpPostedFileBase file, int idMapeamentoCamada, int idMapeamentoMapa)
        {
            List<object> lst = new List<object>();
            List<MapeamentoElementoInfo> lstElemento = new List<MapeamentoElementoInfo>();
            var result = true;
            var message = string.Empty;
            var filePath = string.Empty;
            var tempFolder = string.Empty;
            var tempFolderVirtual = string.Empty;
            var newFile = string.Empty;
            var nameFile = string.Empty;
            string nameFolder = Guid.NewGuid().ToString();
            string friendlyName = Guid.NewGuid().ToString();


            if (file == null)
            {
                result = false;
                message = "Por favor, selecione um arquivo para upload";
            }
            else if (file.ContentLength > 0)
            {
                var maxContentLength = 1024 * 1024 * 2; // 2Mb

                var fileExtension = file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower();
                if (fileExtension.ToLower() == "zip")
                {
                    result = false;
                    message = "Tipo de Arquivo inválido.";
                }
                else if (file.ContentLength > maxContentLength)
                {
                    result = false;
                    message = "Por favor, selecione um arquivo com até 2 MB.";
                }
                else
                {
                    tempFolder = Server.MapPath(CAMINHO_MAPEAMENTO_TEMP);
                    var fileOperations = new FileOperations(tempFolder);
                    newFile = fileOperations.UploadFile(file, friendlyName.ToString());

                    //message = "Envio do arquivo feito com sucesso";
                    if (fileExtension == ".zip")
                    {
                        string zipPath = string.Format("{0}/{1}", tempFolder, newFile);
                        string extractPath = tempFolder + nameFolder;

                        ZipFile.ExtractToDirectory(zipPath, extractPath);

                        //Verifica Se existe os 4 arquivos
                        DirectoryInfo directory = new DirectoryInfo(extractPath);
                        FileInfo[] files = directory.GetFiles();
                        if (files.Length > 0)
                        {
                            bool dbf = false;
                            bool prj = false;
                            bool shp = false;
                            bool shx = false;

                            foreach (var item in files)
                            {
                                if (item.Extension.ToLower() == ".dbf")
                                {
                                    dbf = true;
                                }
                                else if (item.Extension.ToLower() == ".prj")
                                {
                                    prj = true;
                                }
                                else if (item.Extension.ToLower() == ".shp")
                                {
                                    shp = true;
                                    nameFile = item.Name;
                                }
                                else if (item.Extension.ToLower() == ".shx")
                                {
                                    shx = true;
                                }
                            }

                            if (!dbf || !prj || !shp || !shx)
                            {
                                message = "Alguns arquivos não foram encontrados. Contate o Administrador do Sistema.";
                                result = false;
                            }
                        }
                        else
                        {
                            message = "Os arquivos não correspondem ao formato Shapefile, verifique e tente novamente.";
                            result = false;
                        }

                        if (result)
                        {
                            #region Arquivo SHP e Método para Salvar

                            GeometryFactory factory = new GeometryFactory();

                            ShapefileDataReader shapeFileDataReader = new ShapefileDataReader((Path.Combine(extractPath, nameFile)), factory);
                            ShapefileHeader shpHeader = shapeFileDataReader.ShapeHeader;

                            int qtde = 0;
                            while (shapeFileDataReader.Read())
                            {
                                qtde++;
                                #region Lista de Elementos

                                MapeamentoElementoInfo elementoInfo = new MapeamentoElementoInfo();

                                elementoInfo.IdEmpresaLogada = IdEmpresa;
                                elementoInfo.IdPessoaOperacao = IdPessoa;

                                elementoInfo.IdEmpresa = IdEmpresa;
                                elementoInfo.IdMapeamentoMapa = idMapeamentoMapa;
                                elementoInfo.IdMapeamentoCamada = idMapeamentoCamada;
                                elementoInfo.Descricao = string.Format("{0} {1} ({2})", qtde.ToString(), "Elemento", shapeFileDataReader.Geometry.GeometryType);
                                elementoInfo.VisivelConsulta = true;
                                elementoInfo.Ativo = true;

                                elementoInfo.ElementoGeo = new MapeamentoElementoGeoInfo();
                                elementoInfo.ElementoGeo.IdEmpresaLogada = IdEmpresa;
                                elementoInfo.ElementoGeo.IdPessoaOperacao = IdPessoa;
                                elementoInfo.ElementoGeo.IdMapeamentoCamada = idMapeamentoCamada;
                                elementoInfo.ElementoGeo.Estilo = "";
                                elementoInfo.ElementoGeo.wkt = shapeFileDataReader.Geometry.ToString();

                                lstElemento.Add(elementoInfo);

                                #endregion
                            }
                            //Close and free up any resources
                            shapeFileDataReader.Close();
                            shapeFileDataReader.Dispose();
                            #endregion

                            if (lstElemento.Count() > 0)
                            {
                                result = MapeamentoElementoBll.Instance.SalvarComElementoGeo(lstElemento).Sucesso;
                                if (!result)
                                {
                                    message = "Erro ao processar a Importação.";
                                }
                            }
                        }

                        if (result)
                        {
                            //Excluir aquivo .ZIP e a pasta 
                            Directory.Delete(tempFolder, true);
                            result = true;
                            message = "Importação realizada com sucesso.";
                        }
                    }
                }
            }

            return Json(new
            {
                Sucesso = result,
                Mensagem = message,
                FilePath = result ? Url.Content(VirtualPathUtility.Combine(tempFolderVirtual + "/", newFile)) : "",
                Arquivo = newFile
            });
        }

        [HttpPost]
        public ActionResult DownloadShape(int? IdMapeamentoElemento)
        {
            MapeamentoElementoInfo elementoInfo = new MapeamentoElementoInfo();
            MapeamentoElementoGeoInfo elementoGeoInfo = new MapeamentoElementoGeoInfo();
            Guid g = Guid.NewGuid();
            string url = string.Empty;
            string outShpFilename = string.Empty;
            IList<IFeature> features = new List<IFeature>();

            elementoInfo = MapeamentoElementoBll.Instance.ListarPorCodigo(IdMapeamentoElemento.Value);
            elementoGeoInfo = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdMapeamentoElemento = IdMapeamentoElemento.Value }).FirstOrDefault();

            if (elementoGeoInfo != null)
            {

                var wktx = new WKTReader();
                GeoAPI.Geometries.IGeometry geometry = wktx.Read(elementoGeoInfo.wkt);

                AttributesTable attributesTable = new AttributesTable();
                attributesTable.AddAttribute("Name", "value");

                Feature feature = new Feature();
                feature.Geometry = geometry;
                feature.Attributes = attributesTable;

                features.Add(feature);

                //Create a new shapefile from features
                outShpFilename = string.Format("{0}-{1}", g.ToString(), elementoInfo.Descricao);
                NetTopologySuite.Geometries.GeometryFactory outGeomFactory = new NetTopologySuite.Geometries.GeometryFactory();

                if (!Directory.Exists(Server.MapPath(CAMINHO_MAPEAMENTO_TEMP)))
                    Directory.CreateDirectory(Server.MapPath(CAMINHO_MAPEAMENTO_TEMP));

                url = Server.MapPath(CAMINHO_MAPEAMENTO) + outShpFilename;

                ShapefileDataWriter writer = new ShapefileDataWriter(url, outGeomFactory);

                DbaseFileHeader outDbaseHeader = ShapefileDataWriter.GetHeader((Feature)features[0], features.Count);
                writer.Header = outDbaseHeader;

                writer.Write(features);
            }

            return Json(new
            {
                Sucesso = true,
                Mensagem = "Sucesso",
                File = Url.Content(CAMINHO_MAPEAMENTO_TEMP + outShpFilename)
            });
        }

        #endregion

        #region KML/KMZ

        [HttpPost]
        public ActionResult DownloadKML(int IdMapeamentoElemento)
        {
            string url = string.Empty;
            string mensagem = string.Empty;
            bool result = true;
            MapeamentoElementoInfo elementoInfo = new MapeamentoElementoInfo();
            MapeamentoElementoGeoInfo elementoGeoInfo = new MapeamentoElementoGeoInfo();

            elementoInfo = MapeamentoElementoBll.Instance.ListarPorCodigo(IdMapeamentoElemento);
            if (elementoInfo != null)
                elementoGeoInfo = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdMapeamentoElemento = IdMapeamentoElemento }).FirstOrDefault();
            else
            {
                //TODO erro
            }

            if (elementoGeoInfo != null)
            {
                if (elementoGeoInfo.TipoGeometria.ToLower() == "point")
                {
                    url = CreateKmlPoint(elementoInfo, elementoGeoInfo);
                }
                else if (elementoGeoInfo.TipoGeometria.ToLower() == "linestring")
                {
                    url = CreateKmlLine(elementoInfo, elementoGeoInfo);
                }
                else if (elementoGeoInfo.TipoGeometria.ToLower() == "polygon")
                {
                    url = CreateKmlPolygon(elementoInfo, elementoGeoInfo);
                }
            }
            else
            {
                result = false;
                mensagem = "Não foi possível localizar o Elemento";
            }

            return Json(new
            {
                Sucesso = result,
                Mensagem = mensagem,
                File = url
            });
        }

        [HttpPost]
        public ActionResult DownloadKMLPorCamada(int IdMapeamentoCamada)
        {


            return Json(new { });
        }

        [HttpPost]
        public ActionResult UploadKML(HttpPostedFileBase file)
        {
            // Disponibilizar o Arquivo em um http publico e retornar a 'url' para o client [Gustavão]

            List<object> lst = new List<object>();
            var result = true;
            var message = string.Empty;
            var filePath = string.Empty;
            var tempFolder = string.Empty;
            var tempFolderVirtual = string.Empty;
            var newFile = string.Empty;
            Guid friendlyName = new Guid();


            if (file == null)
            {
                result = false;
                message = "Por favor, selecione um arquivo para upload";
            }
            else if (file.ContentLength > 0)
            {
                var maxContentLength = 1024 * 1024 * 2; // 2Mb

                var fileExtension = file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLower();
                if (fileExtension.ToLower() != ".kml" && fileExtension.ToLower() != ".kmz")
                {
                    result = false;
                    message = "Arquivo de tipo inválido";
                }
                else if (file.ContentLength > maxContentLength)
                {
                    result = false;
                    message = "Por favor, selecione um arquivo com menos de " + maxContentLength + "Mb";
                }

                tempFolder = Server.MapPath(CAMINHO_MAPEAMENTO_TEMP);
                var fileOperations = new FileOperations(tempFolder);
                newFile = fileOperations.UploadFile(file, friendlyName.ToString());

            }
            else
            {
                result = false;
                message = "Não foi possível ler o arquivo solicitado";
            }

            return Json(new
            {
                Sucesso = result,
                Mensagem = message,
                File = newFile
            });
        }

        private string CreateKmlPolygon(MapeamentoElementoInfo elementoInfo, MapeamentoElementoGeoInfo elementoGeoInfo)
        {
            SharpKml.Dom.Polygon polygon = new SharpKml.Dom.Polygon();
            string name = Guid.NewGuid().ToString();
            string url = string.Empty;

            List<double[]> lst = new List<double[]>();

            string newWKT = elementoGeoInfo.wkt.Replace("POLYGON ", "").Replace("((", "").Replace("))", "").Replace(", ", ",");
            string[] coordenadas = newWKT.Split(new char[] { ',' });
            SharpKml.Dom.OuterBoundary y = new SharpKml.Dom.OuterBoundary();
            y.LinearRing = new SharpKml.Dom.LinearRing();
            SharpKml.Dom.CoordinateCollection x = new SharpKml.Dom.CoordinateCollection();

            for (int i = 0; i < coordenadas.Length; i++)
            {
                string[] ll = coordenadas[i].Split(' ');

                x.Add(new SharpKml.Base.Vector { Latitude = double.Parse(ll[1].ToString().Replace(".", ",")), Longitude = double.Parse(ll[0].ToString().Replace(".", ",")) });
            }
            y.LinearRing.Coordinates = new SharpKml.Dom.CoordinateCollection(x);
            polygon.OuterBoundary = y;
            polygon.Id = elementoInfo.Descricao;

            polygon.AltitudeMode = SharpKml.Dom.AltitudeMode.RelativeToGround;
            polygon.Extrude = true;


            SharpKml.Dom.Placemark p = new SharpKml.Dom.Placemark();
            p.Name = elementoInfo.Descricao;
            p.Geometry = polygon;
            p.Visibility = true;

            SharpKml.Dom.Kml kml = new SharpKml.Dom.Kml();
            kml.Feature = p;

            SharpKml.Base.Serializer serializer = new SharpKml.Base.Serializer();

            serializer.Serialize(kml);

            try
            {
                if (!Directory.Exists(Server.MapPath(CAMINHO_MAPEAMENTO)))
                    Directory.CreateDirectory(Server.MapPath(CAMINHO_MAPEAMENTO));

                System.IO.File.WriteAllText(Server.MapPath(CAMINHO_MAPEAMENTO + name + ".kml"), serializer.Xml);
            }
            catch (Exception exc)
            {
                url = string.Empty;
            }

            url = Url.Content(CAMINHO_MAPEAMENTO + name + ".kml");
            return url;

        }
        private string CreateKmlPoint(MapeamentoElementoInfo elementoInfo, MapeamentoElementoGeoInfo elementoGeoInfo)
        {
            SharpKml.Dom.Point point = new SharpKml.Dom.Point();
            string name = Guid.NewGuid().ToString();
            string url = string.Empty;

            string newWkt = elementoGeoInfo.wkt.Replace("POINT ", "").Replace("(", "").Replace(")", "");
            string[] coord = newWkt.Split(' ');

            //latitude e longitude estão invertidas, mas assim funciona =/
            double latitude = double.Parse(coord[1].Replace(".", ","));
            double longitude = double.Parse(coord[0].Replace(".", ","));

            point.Coordinate = new SharpKml.Base.Vector(latitude: latitude, longitude: longitude);
            point.AltitudeMode = SharpKml.Dom.AltitudeMode.RelativeToGround;

            SharpKml.Dom.Placemark p = new SharpKml.Dom.Placemark();
            p.Name = elementoInfo.Descricao;
            p.Geometry = point;

            SharpKml.Dom.Kml kml = new SharpKml.Dom.Kml();
            kml.Feature = p;

            SharpKml.Base.Serializer serializer = new SharpKml.Base.Serializer();
            serializer.Serialize(kml);

            try
            {
                if (!Directory.Exists(Server.MapPath(CAMINHO_MAPEAMENTO)))
                    Directory.CreateDirectory(Server.MapPath(CAMINHO_MAPEAMENTO));

                System.IO.File.WriteAllText(Server.MapPath(CAMINHO_MAPEAMENTO + name + ".kml"), serializer.Xml);
            }
            catch (Exception exc)
            {
                url = string.Empty;
            }

            url = Url.Content(CAMINHO_MAPEAMENTO + name + ".kml");

            return url;
        }
        private string CreateKmlLine(MapeamentoElementoInfo elementoInfo, MapeamentoElementoGeoInfo elementoGeoInfo)
        {
            string name = Guid.NewGuid().ToString();
            string url = string.Empty;

            SharpKml.Dom.LineString line = new SharpKml.Dom.LineString();
            SharpKml.Dom.CoordinateCollection x = new SharpKml.Dom.CoordinateCollection();

            string newWKT = elementoGeoInfo.wkt.Replace("LINESTRING ", "").Replace("(", "").Replace(")", "").Replace(", ", ",");
            string[] coordenadas = newWKT.Split(new char[] { ',' });

            for (int i = 0; i < coordenadas.Length; i++)
            {
                string[] ll = coordenadas[i].Split(' ');
                x.Add(new SharpKml.Base.Vector { Latitude = double.Parse(ll[1].ToString().Replace(".", ",")), Longitude = double.Parse(ll[0].ToString().Replace(".", ",")) });
            }

            line.Coordinates = new SharpKml.Dom.CoordinateCollection(x);

            SharpKml.Dom.Placemark p = new SharpKml.Dom.Placemark();
            p.Name = elementoInfo.Descricao;
            p.Geometry = line;
            p.Visibility = true;

            SharpKml.Dom.Kml kml = new SharpKml.Dom.Kml();
            kml.Feature = p;

            SharpKml.Base.Serializer serializer = new SharpKml.Base.Serializer();

            serializer.Serialize(kml);

            try
            {
                if (!Directory.Exists(Server.MapPath(CAMINHO_MAPEAMENTO)))
                    Directory.CreateDirectory(Server.MapPath(CAMINHO_MAPEAMENTO));

                System.IO.File.WriteAllText(Server.MapPath(CAMINHO_MAPEAMENTO + name + ".kml"), serializer.Xml);
            }
            catch (Exception exc)
            {
                url = string.Empty;
            }

            url = Url.Content(CAMINHO_MAPEAMENTO + name + ".kml");

            return url;
        }

        #endregion

    }
}
