﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Camada;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class CamadaController : ControllerExtended
    {
        public CamadaController()
        {
            IndexUrl = "Mapeamento/Camada/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoCamadaInfo info = MapeamentoCamadaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoCamada, "MapeamentoCamada");
            }
            else
            {
                vm.Ativo = true;
                vm.UltimoNivel = true;
                vm.VisivelConsulta = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo();

            ViewModelToModelMapper.Map<MapeamentoCamadaInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = MapeamentoCamadaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoCamada.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            MapeamentoCamadaEmpresaInfo info = new MapeamentoCamadaEmpresaInfo();
            MapeamentoCamadaEmpresaInfo response = new MapeamentoCamadaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaEmpresaInfo { IdMapeamentoCamada = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = MapeamentoCamadaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            MapeamentoCamadaEmpresaInfo info = new MapeamentoCamadaEmpresaInfo();
            MapeamentoCamadaEmpresaInfo response = new MapeamentoCamadaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaEmpresaInfo { IdMapeamentoCamada = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = MapeamentoCamadaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoCamada = vm.IdMapeamentoCamada;
            info.IdMapeamentoCamadaCategoria = vm.IdMapeamentoCamadaCategoria;
            info.Descricao = vm.Descricao;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.VisivelConsulta = vm.VisivelConsulta;
            info.SomenteLeitura = vm.SomenteLeitura;
            info.Ativo = vm.Ativo;
            info.IdMapeamentoCamadaNotIn = vm.IdMapeamentoCamadaNotIn;
            info.IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn;
            info.RegistroProprio = vm.RegistroProprio;
            info.MapeamentoCamadaCategoriaBase = vm.MapeamentoCamadaCategoriaBase;
            info.BuscaAvancada = vm.BuscaAvancada;

            if ((vm.SemCamadaBaseAtual.HasValue && vm.SemCamadaBaseAtual.Value) && vm.IdMapeamentoMapa.HasValue)
            {
                if (!string.IsNullOrEmpty(vm.IdMapeamentoCamadaNotIn))
                {
                    int idcamadanotin = RetornaCamadaPerimetroPorMapa(vm.IdMapeamentoMapa.Value);

                    if (idcamadanotin > 0)
                    {
                        info.IdMapeamentoCamadaNotIn = vm.IdMapeamentoCamadaNotIn + "," + idcamadanotin.ToString();
                    }
                }
                else
                {

                    int idcamadanotin = RetornaCamadaPerimetroPorMapa(vm.IdMapeamentoMapa.Value);
                    info.IdMapeamentoCamadaNotIn = idcamadanotin > 0 ? idcamadanotin.ToString() : "";
                }
            }

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<MapeamentoCamadaInfo> retorno = new ListPaged<MapeamentoCamadaInfo>(MapeamentoCamadaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Camadas ", true);
        }

        [HttpGet]
        public ActionResult ListarPorId(int id)
        {
            MapeamentoCamadaInfo response = new MapeamentoCamadaInfo();

            response = MapeamentoCamadaBll.Instance.ListarPorParametros(new MapeamentoCamadaInfo { IdEmpresa = IdEmpresa, IdMapeamentoCamada = id }).FirstOrDefault();

            return Json(new { Data = response }, JsonRequestBehavior.AllowGet);
        }

        private int RetornaCamadaPerimetroPorMapa(int idMapeamentoMapa)
        {
            int idMapeamentoCamada = 0;

            var response = MapeamentoCamadaBll.Instance.ListarCamdaBasePorEmpresaMapa(idMapeamentoMapa: idMapeamentoMapa, idEmpresa: IdEmpresa);
            if (response != null)
                idMapeamentoCamada = response.IdMapeamentoCamada.Value;

            return idMapeamentoCamada;
        }
        #endregion

    }
}
