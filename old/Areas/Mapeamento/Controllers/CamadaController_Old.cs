﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Camada;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class CamadaController_Old : ControllerExtended
    {
        public CamadaController_Old()
        {
            IndexUrl = "/Mapeamento/Camada/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoCamadaInfo info = MapeamentoCamadaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoCamada, "MapeamentoCamada");
            }
            else
            {
                vm.Ativo = true;
                vm.UltimoNivel = true;
                vm.VisivelConsulta = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo();

            ViewModelToModelMapper.Map<MapeamentoCamadaInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = MapeamentoCamadaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoCamada.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            MapeamentoCamadaEmpresaInfo info = new MapeamentoCamadaEmpresaInfo();
            MapeamentoCamadaEmpresaInfo response = new MapeamentoCamadaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaEmpresaInfo { IdMapeamentoCamada = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = MapeamentoCamadaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            MapeamentoCamadaEmpresaInfo info = new MapeamentoCamadaEmpresaInfo();
            MapeamentoCamadaEmpresaInfo response = new MapeamentoCamadaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaEmpresaInfo { IdMapeamentoCamada = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = MapeamentoCamadaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamada, "MapeamentoCamada", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoCamada = vm.IdMapeamentoCamada;
            info.IdMapeamentoMapa = vm.IdMapeamentoMapa;
            info.Descricao = vm.Descricao;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.VisivelConsulta = vm.VisivelConsulta;
            info.SomenteLeitura = vm.SomenteLeitura;
            info.Ativo = vm.Ativo;
            info.IdMapeamentoCamadaNotIn = vm.IdMapeamentoCamadaNotIn;
            info.RegistroProprio = vm.RegistroProprio;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion
            
            ListPaged<MapeamentoCamadaInfo> retorno = new ListPaged<MapeamentoCamadaInfo>(MapeamentoCamadaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Camadas ", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListSimple(ListVM vm)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoCamada = vm.IdMapeamentoCamada;
            //info.IdMapeamentoMapa = vm.IdMapeamentoMapa;
            info.Descricao = vm.Descricao;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<MapeamentoCamadaInfo> retorno = new ListPaged<MapeamentoCamadaInfo>(MapeamentoCamadaBll.Instance.ListarPorParametros(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Camadas ");
        }

        [HttpPost]
        public string RetornaArvorePorIdMapeamentoMapa(int IdMapeamentoMapa)
        {
            List<MapeamentoCamadaInfo> lst = new List<MapeamentoCamadaInfo>();

            if (IdMapeamentoMapa > 0)
            {
                lst = MapeamentoCamadaBll.Instance.ListarPorParametros(new MapeamentoCamadaInfo {  });
                if (lst.Count > 0)
                {

                    //var x = GerarArvore(lst);
                    //var y = JsonConvert.SerializeObject(x);
                    //var formattedData = y.Replace("css_class", "class").Replace("is_checked", "checked");
                    
                    //return formattedData;

                    return string.Empty;
                }
                else
                {                  
                    return string.Empty;
                }
            }
            else
            {                
                return string.Empty;
            }

        }

        #endregion

        #region Exclusão

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult Excluir(int? id)
        {
            MapeamentoCamadaInfo info = new MapeamentoCamadaInfo();

            if (id.HasValue)
                info.Response = MapeamentoCamadaBll.Instance.Excluir(id.Value, IdEmpresa);
            else
                return Json(new { Sucesso = false, Mensagem = "Identificador Não Informado." });

            return Json(new { Sucesso = info.Response.Sucesso, Data = info.Response });
        }

        #endregion

        private string GetIcon(string tipoGeometria)
        {
            string retorno = "format_shapes";

            switch (tipoGeometria.ToLower())
            {
                default:     
                case "polygon":
                    retorno = "format_shapes";
                    break;
                case "multilinestring":
                    retorno = "timeline";
                    break;
                case "polyline":
                    retorno = "timeline";
                    break;
                case "point":
                    retorno = "place";
                    break;
            }
           
            return retorno;
        } 
              
        //private List<JsTree> GerarArvore(List<MapeamentoCamadaInfo> itens, MapeamentoCamadaInfo itemAtual = null )
        //{
        //    List<JsTree> retorno = new List<JsTree>();
        //    List<MapeamentoCamadaInfo> nivelCorrente;

        //    if (itemAtual == null)
        //    {
        //        nivelCorrente = itens.Where(x => !x.IdMapeamentoCamadaPai.HasValue).OrderBy(x => x.Ordem).ToList();
        //    }
        //    else
        //    {
        //        nivelCorrente = itens.Where(x => x.IdMapeamentoCamadaPai == itemAtual.IdMapeamentoCamada).OrderBy(x => x.Ordem).ToList();
        //    }

        //    foreach (MapeamentoCamadaInfo camadaItem in nivelCorrente)
        //    {
        //        JsTree treeItem = new JsTree();

        //        string idMapeamentoMapa = camadaItem.IdMapeamentoMapa.Value.ToString();

        //        treeItem.data.id = idMapeamentoMapa;
        //        treeItem.text = camadaItem.Descricao + string.Format(" <a href='#' class='btn-camada-opcoes' role='button' data-id='{0}'><i class='material-icons'>&#xE5D4;</i></b>", camadaItem.IdMapeamentoCamada.Value.ToString());
        //        treeItem.type = "folder";
        //        treeItem.icon = "fa fa-folder";                
        //        treeItem.state = new NodeState(false, false, false);                

        //        LiAttr li = new LiAttr();               
        //        li.id = "li-" + idMapeamentoMapa + "-" + camadaItem.IdMapeamentoCamada.Value.ToString();
        //        treeItem.li_attr = li;                 
                
        //        treeItem.children = new List<JsTree>();
        //        treeItem.children.AddRange(GerarArvore(itens, camadaItem));

        //        if (camadaItem.Elementos != null && camadaItem.Elementos.Count > 0)
        //        {
        //            foreach (var elementoItem in camadaItem.Elementos)
        //            {
        //                JsTree nodeElemento = new JsTree();

        //                LiAttr li_children = new LiAttr();
        //                li_children.id = "li-" + idMapeamentoMapa + "-" + camadaItem.IdMapeamentoCamada.Value.ToString() + "-" + elementoItem.IdMapeamentoElemento.ToString();
        //                li_children.is_material = true;
        //                nodeElemento.li_attr = li_children;

        //                nodeElemento.data.id = elementoItem.IdMapeamentoElemento.ToString();
        //                nodeElemento.text = string.Format("<i class=\"material-icons\">{0}</i>", GetIcon(elementoItem.ElementoGeo.TipoGeometria)) + elementoItem.Descricao + string.Format("<a href='#' class='btn-elemento-opcoes' role='button' data-id='{0}'><i class='material-icons'>&#xE5D4;</i></b>", elementoItem.IdMapeamentoElemento.Value.ToString());
        //                nodeElemento.type = "default";
        //                nodeElemento.icon = "material-icons";
        //                nodeElemento.state = new NodeState(false, false, false);

        //                treeItem.children.Add(nodeElemento);
        //            }
        //        }

        //        retorno.Add(treeItem);
        //    }

        //    return retorno;
        //}
    }
}