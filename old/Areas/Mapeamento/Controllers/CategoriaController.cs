﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Categoria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class CategoriaController : ControllerExtended
    {
        public CategoriaController()
        {
            IndexUrl = "/Mapeamento/Categoria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet]
        [CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                MapeamentoCamadaCategoriaInfo info = new MapeamentoCamadaCategoriaInfo();
                info = MapeamentoCamadaCategoriaBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoCamadaCategoria, "MapeamentoCamadaCategoria");
            }
            else
            {
                vm.Ativo = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoCamadaCategoriaInfo info = new MapeamentoCamadaCategoriaInfo();
            ViewModelToModelMapper.Map<MapeamentoCamadaCategoriaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.IdsRestricao))
            {
                info.lstCategoriaRestricao = new List<MapeamentoCamadaCategoriaRestricaoInfo>();

                CommaSeparatedToList(vm.IdsRestricao).ForEach(id =>
                {
                    MapeamentoCamadaCategoriaRestricaoInfo item = new MapeamentoCamadaCategoriaRestricaoInfo();
                    item.IdMapeamentoCamadaCategoriaRestrito = id;

                    info.lstCategoriaRestricao.Add(item);
                });
            }

            #endregion

            var response = MapeamentoCamadaCategoriaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoCamadaCategoria.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdMapeamentoCamadaCategoria, "MapeamentoCamadaCategoria", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            MapeamentoCamadaCategoriaEmpresaInfo info = new MapeamentoCamadaCategoriaEmpresaInfo();
            MapeamentoCamadaCategoriaEmpresaInfo response = new MapeamentoCamadaCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaCategoriaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaCategoriaEmpresaInfo { IdMapeamentoCamadaCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = MapeamentoCamadaCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamadaCategoria.Value, "MapeamentoCamadaCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            MapeamentoCamadaCategoriaEmpresaInfo info = new MapeamentoCamadaCategoriaEmpresaInfo();
            MapeamentoCamadaCategoriaEmpresaInfo response = new MapeamentoCamadaCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoCamadaCategoriaEmpresaBll.Instance.ListarPorParametros(new MapeamentoCamadaCategoriaEmpresaInfo { IdMapeamentoCamadaCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = MapeamentoCamadaCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoCamadaCategoria.Value, "MapeamentoCamadaCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoCamadaCategoriaInfo info = new MapeamentoCamadaCategoriaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoCamadaCategoria = vm.IdMapeamentoCamadaCategoria;
            info.IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn;
            info.IdMapeamentoCamadaCategoriaNotIn = vm.IdMapeamentoCamadaCategoriaNotIn;
            info.IdNaturezaOperacional = vm.IdNaturezaOperacional;
            info.Descricao = vm.Descricao;
            info.BuscaAvancada = vm.BuscaAvancada;
            info.Ativo = vm.Ativo;
            info.Base = vm.Base;

            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<MapeamentoCamadaCategoriaInfo> retorno = new ListPaged<MapeamentoCamadaCategoriaInfo>(MapeamentoCamadaCategoriaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Categoria de Camada", true);
        }

    }
}