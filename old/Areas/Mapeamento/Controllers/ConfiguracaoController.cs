﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Configuracao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class ConfiguracaoController : ControllerExtended
    {
        
        public ConfiguracaoController()
        {
            IndexUrl = "/Mapeamento/Configuracao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult ListSimple(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                vm.IdUnidadeDistanciaExtra = 1;
                vm.IdUnidadeAreaExtra = 20;

                MapeamentoConfiguracaoInfo info = MapeamentoConfiguracaoBll.Instance.ListarPorParametros(new MapeamentoConfiguracaoInfo() { IdMapeamentoMapaPadrao = id.Value }).FirstOrDefault();

                if (info != null)
                {
                    ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                }
            }

            return Json(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoConfiguracaoInfo info = MapeamentoConfiguracaoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoConfiguracao, "MapeamentoConfiguracao");
            }
            else
            {
                vm.IdUnidadeDistanciaExtra = 1;
                vm.IdUnidadeAreaExtra = 20;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoConfiguracaoInfo info = new MapeamentoConfiguracaoInfo();

            ViewModelToModelMapper.Map<MapeamentoConfiguracaoInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = MapeamentoConfiguracaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoConfiguracao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoConfiguracao, "MapeamentoConfiguracao", info.Response.IdHistorico);
            return Json(response);
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoConfiguracaoInfo info = new MapeamentoConfiguracaoInfo();
            info.IdEmpresa = IdEmpresa;

            ListPaged<MapeamentoConfiguracaoInfo> retorno = new ListPaged<MapeamentoConfiguracaoInfo>(MapeamentoConfiguracaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Configurações para Mapeamento", true);
        }

    }
}
