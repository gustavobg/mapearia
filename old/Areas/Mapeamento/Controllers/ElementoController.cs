﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Elemento;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class ElementoController : ControllerExtended
    {

        public ElementoController()
        {
            IndexUrl = "Mapeamento/Elemento/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoElementoInfo info = MapeamentoElementoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoElemento, "MapeamentoElemento");
            }
            else
            {
                vm.Ativo = true;
                vm.VisivelConsulta = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoElementoInfo info = new MapeamentoElementoInfo();

            ViewModelToModelMapper.Map<MapeamentoElementoInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;

            var response = MapeamentoElementoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoElemento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoElemento, "MapeamentoElemento", info.Response.IdHistorico);
            return Json(response);
        }

        #endregion

        #region Save Elemento + Elemento GEO

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult SaveElemento(ElementoVM vm)
        {
            MapeamentoElementoInfo info = new MapeamentoElementoInfo();
            ViewModelToModelMapper.Map<MapeamentoElementoInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;

            var response = MapeamentoElementoBll.Instance.SalvarComElementoGeo(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoElemento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoElemento, "MapeamentoElemento", info.Response.IdHistorico);
            return Json(response);
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoElementoInfo info = new MapeamentoElementoInfo();
            //info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoElemento = vm.IdMapeamentoElemento;
            info.Ativo = vm.Ativo;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion


            ListPaged<MapeamentoElementoInfo> retorno = new ListPaged<MapeamentoElementoInfo>(MapeamentoElementoBll.Instance.ListarPorParametros(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Elementos ");
        }

        #endregion

        #region Excluir

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult Excluir(int? id)
        {
            MapeamentoElementoInfo info = new MapeamentoElementoInfo();

            if (id.HasValue)
                info.Response = MapeamentoElementoBll.Instance.Excluir(id.Value);
            else
                return Json(new { Sucesso = false, Mensagem = "Identificador Não Informado." });

            return Json(new { Sucesso = info.Response.Sucesso, Data = info.Response });
        }

        #endregion

        #region Validações

        [HttpPost]
        [CustomAuthorize]
        public JsonResult ValidarDescricaoElementoPorMapa(string descricao, int idMapeamentoMapa, int? idMapeamentoElemento)
        {
            bool sucesso = true;

            sucesso = MapeamentoCamadaMapaItemBll.Instance.ValidarDescricaoElemento(descricao, IdEmpresa, idMapeamentoMapa, idMapeamentoElemento);

            return new JsonResult() { Data = new { Sucesso = sucesso } };
        }

        #endregion


    }
}
