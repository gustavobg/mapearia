﻿using HTM.MasterGestor.Bll.Agricola;
using HTM.MasterGestor.Bll.Aplicacao;
using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Agricola;
using HTM.MasterGestor.Model.Aplicacao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.ElementoGeo;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.MapaCalor;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class ElementoGeoController : ControllerExtended
    {
        public ElementoGeoController()
        {
            IndexUrl = "/Mapeamento/ElementoGeo/Index";
        }

        #region NewEdit

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoElementoGeoInfo info = MapeamentoElementoGeoBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoElementoGeo, "MapeamentoElementoGeo");
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoElementoGeoInfo info = new MapeamentoElementoGeoInfo();

            ViewModelToModelMapper.Map<MapeamentoElementoGeoInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            var response = MapeamentoElementoGeoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoElemento.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoElemento, "MapeamentoElementoGeo", info.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoElementoGeoInfo info = new MapeamentoElementoGeoInfo();
            info.IdMapeamentoElemento = vm.IdMapeamentoElemento;
            info.IdMapeamentoCamada = vm.IdMapeamentoCamada;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<MapeamentoElementoGeoInfo> retorno = new ListPaged<MapeamentoElementoGeoInfo>(MapeamentoElementoGeoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Elementos (GEO) ", true);
        }

        [HttpPost]
        public ActionResult RetornaElementosGeoPorIdMapeamentoMapa(int? idMapeamentoMapa, string idMapeamentoCamadaCategoriaIn = null)
        {
            List<MapeamentoElementoGeoInfo> lst = new List<MapeamentoElementoGeoInfo>();
            try
            {
                if (idMapeamentoMapa.HasValue)
                {
                    var elementoBase = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdMapeamentoMapa = idMapeamentoMapa, IdEmpresa = IdEmpresa, MapeamentoCamadaCategoriaBase = true });
                    lst = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo() { IdMapeamentoMapa = idMapeamentoMapa, IdEmpresa = IdEmpresa, IdMapeamentoCamadaCategoriaIn = idMapeamentoCamadaCategoriaIn, MapeamentoCamadaCategoriaBase = false }).OrderBy(p => p.Ordem).ToList();
                    if (elementoBase.Count() > 0)
                    {
                        lst.AddRange(elementoBase);
                    }

                    return Json(new { Sucesso = true, Data = lst.OrderBy(p => p.Ordem).ToList() });
                }
                else
                    return Json(new { Sucesso = false });
            }
            catch (Exception)
            {
                return Json(new { Sucesso = false, Mensagem = "Erro ao Listar as Inforamções 'GEO'. " });
            }
        }

        [HttpPost]
        public JsonResult RetornaElementoGeoPorIdsMapeamentoElemento(string idMapeamentoElementoIn)
        {
            List<MapeamentoElementoGeoInfo> lst = new List<MapeamentoElementoGeoInfo>();
            try
            {
                if (!string.IsNullOrEmpty(idMapeamentoElementoIn))
                {
                    lst = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdEmpresa = IdEmpresa, IdMapeamentoElementoIn = idMapeamentoElementoIn });

                    return new JsonResult() { Data = new { Data = lst } };
                }
                else
                {
                    return new JsonResult() { Data = new { Data = "" } };
                }
            }
            catch (Exception)
            {

                return new JsonResult() { Data = new { Data = "" } };
            }
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem.ListVM))]
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RetornaElementoGeoParaMapaMedidorItem(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem.ListVM vm)
        {
            #region Conversão

            QuestionarioMedidorItemInfo info = new QuestionarioMedidorItemInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Codigo = vm.Codigo;
            info.IdQuestionarioMedidorItem = vm.IdQuestionarioMedidorItem;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;
            info.IdQuestionarioMedidorItemIn = vm.IdQuestionarioMedidorItemIn;
            info.IdQuestionarioMedidorItemNotIn = vm.IdQuestionarioMedidorItemNotIn;
            info.IdQuestionarioCategoriaMedidorIn = vm.IdQuestionarioCategoriaMedidorIn;

            #endregion

            var lst = QuestionarioMedidorItemBll.Instance.ListarPorParametros(info);
            if (lst.Count() > 0)
            {
                List<MapeamentoElementoGeoInfo> lstElementoGeo = new List<MapeamentoElementoGeoInfo>();
                List<ProducaoUnidadeInfo> lstProducaoUnidade = new List<ProducaoUnidadeInfo>();

                List<int> lstIdElemento = new List<int>();

                string idProducaoUnidadeIn = string.Join(",", lst.Select(p => p.IdProducaoUnidade.Value));
                string IdsMapeamentoMapaIn = string.Empty;

                if (!string.IsNullOrEmpty(idProducaoUnidadeIn))
                {
                    lstProducaoUnidade = ProducaoUnidadeBll.Instance.ListarPorParametros(new ProducaoUnidadeInfo { IdProducaoUnidadeIn = idProducaoUnidadeIn });

                    var elementos = MapeamentoElementoBll.Instance.ListarElementosComGEO(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 3, IdReferenciaIn = idProducaoUnidadeIn });
                    lstIdElemento = elementos.Select(p => p.IdMapeamentoElemento.Value).ToList();
                    if (lstIdElemento.Count() > 0)
                    {
                        IdsMapeamentoMapaIn = string.Join(",", elementos.GroupBy(p => p.IdMapeamentoMapa.Value).Select(p => p.Key).ToList());
                        lstElementoGeo = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdEmpresa = IdEmpresa, IdMapeamentoElementoIn = string.Join(",", lstIdElemento) });
                    }
                }

                #region inclusão de Elementos por Categoria de Camada

                if (!string.IsNullOrEmpty(vm.IdMapeamentoCamadaCategoriaIn))
                {
                    string IdsElementosNotIn = lstElementoGeo.Count() > 0 ? string.Join(",", lstElementoGeo.Select(p => p.IdMapeamentoElemento.Value)) : string.Empty;

                    var elementosComplementares = MapeamentoElementoBll.Instance.ListarElementosComGEO(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, IdMapeamentoMapaIn = IdsMapeamentoMapaIn, IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn });
                    if (elementosComplementares.Count() > 0)
                    {
                        var elementosGeoComplementares = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdEmpresa = IdEmpresa, IdMapeamentoElementoIn = string.Join(",", elementosComplementares.Select(p => p.IdMapeamentoElemento.Value)), TipoGeometria = "Polygon" });

                        lstElementoGeo.AddRange(elementosGeoComplementares);
                    }
                }

                #endregion

                lstElementoGeo = DescricaoProducaoUnidadeToElementoGeo(lstProducaoUnidade, lstElementoGeo);

                foreach (var item in lst)
                {
                    MapeamentoElementoGeoInfo geo = new MapeamentoElementoGeoInfo();

                    if (!string.IsNullOrEmpty(item.Longitude) && !string.IsNullOrEmpty(item.Latitude))
                    {
                        EstiloElementoVM estiloElemento = new EstiloElementoVM();
                        if (!string.IsNullOrEmpty(item.QuestionarioCategoriaMedidorCor))
                        {
                            estiloElemento.color = item.QuestionarioCategoriaMedidorCor;
                            estiloElemento.fillColor = item.QuestionarioCategoriaMedidorCor;
                        }

                        geo.wkt = string.Format("POINT ({0} {1})", item.Longitude, item.Latitude);
                        geo.TipoGeometria = "Point";
                        geo.Estilo = JsonConvert.SerializeObject(estiloElemento);
                        geo.MapeamentoElementoDescricao = string.Format("{0} - {1}", item.Codigo, item.QuestionarioCategoriaMedidorDescricao);
                        geo.MapeamentoElementoObservacao = string.Format("{0} <br /> {1}", item.ProducaoLocalCompleto2, item.ProducaoUnidadeCompleto);

                        //TODO: Refatorar com URGENCIA :: Éverton França
                        var x = MapeamentoElementoBll.Instance.ListarPorParametros(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 3, IdReferencia = item.IdProducaoUnidade.Value }).FirstOrDefault();
                        geo.idElementoReferencia = x != null && x.IdMapeamentoElemento.HasValue ? x.IdMapeamentoElemento : null;

                        lstElementoGeo.Add(geo);
                    }
                }

                return new JsonResult() { Data = new { Data = lstElementoGeo } };
            }
            else
            {
                return new JsonResult() { Data = new { Data = "[]" } };
            }
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta.ListVM))]
        [HttpPost]
        [CustomAuthorize]
        public JsonResult RetornaElementoGeoParaMapaQuestionarioResposta(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta.ListVM vm)
        {
            #region Conversão VM to INFO

            QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.IdQuestionarioResposta = vm.IdQuestionarioResposta;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdAplicacao = vm.IdAplicacaoReferencia;
            info.IdSubAplicacao = vm.IdSubAplicacaoReferencia;
            info.Situacao = vm.Situacao;

            if (vm.Todos.HasValue && !vm.IdPessoaLancamento.HasValue)
            {
                if (vm.Todos.Value == false)
                    info.IdPessoaLancamento = IdPessoa;
            }
            else
                info.IdPessoaLancamento = vm.IdPessoaLancamento;

            #endregion

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataLancamentoInicio))
            {
                info.DataLancamentoInicio = DateTime.Parse(vm.DataLancamentoInicio);
            }
            if (!string.IsNullOrEmpty(vm.DataLancamentoFim))
            {
                info.DataLancamentoFim = DateTime.Parse(vm.DataLancamentoFim);
            }

            #endregion

            #region Conversão PAGE

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            var lst = QuestionarioRespostaBll.Instance.ListarPorParametros(info);
            if (lst.Count() > 0)
            {
                string idProducaoUnidadeIn = string.Empty;
                string idMapeamentoMapaIn = string.Empty;
                List<ProducaoUnidadeInfo> lstProducaoUnidade = new List<ProducaoUnidadeInfo>();
                List<MapeamentoElementoGeoInfo> lstElementoGeo = new List<MapeamentoElementoGeoInfo>();
                List<int> lstIdElemento = new List<int>();

                var lstSubAplicacao = SubAplicacaoReferenciaBll.Instance.ListarPorParametros(new SubAplicacaoReferenciaInfo { IdSubAplicacaoReferenciaIn = string.Join(",", lst.Select(p => p.IdSubAplicacao.Value)) });
                if (lstSubAplicacao.Count() > 0)
                {
                    idProducaoUnidadeIn = string.Join(",", lstSubAplicacao.Select(p => p.IdProducaoUnidade.Value));
                    lstProducaoUnidade = ProducaoUnidadeBll.Instance.ListarPorParametros(new ProducaoUnidadeInfo { IdEmpresa = info.IdEmpresa.Value, IdProducaoUnidadeIn = idProducaoUnidadeIn });
                }

                if (!string.IsNullOrEmpty(idProducaoUnidadeIn))
                {
                    var elementos = MapeamentoElementoBll.Instance.ListarElementosComGEO(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 3, IdReferenciaIn = idProducaoUnidadeIn });
                    lstIdElemento = elementos.Select(p => p.IdMapeamentoElemento.Value).ToList();

                    if (lstIdElemento.Count() > 0)
                    {
                        lstElementoGeo = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdEmpresa = IdEmpresa, IdMapeamentoElementoIn = string.Join(",", lstIdElemento) });
                    }

                    #region inclusão de Elementos por Categoria de Camada

                    if (!string.IsNullOrEmpty(vm.IdMapeamentoCamadaCategoriaIn))
                    {
                        string IdsElementosNotIn = lstElementoGeo.Count() > 0 ? string.Join(",", lstElementoGeo.Select(p => p.IdMapeamentoElemento.Value)) : string.Empty;
                        idMapeamentoMapaIn = string.Join(",", elementos.GroupBy(p => p.IdMapeamentoMapa.Value).Select(p => p.Key).ToList());

                        var elementosComplementares = MapeamentoElementoBll.Instance.ListarElementosComGEO(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, IdMapeamentoMapaIn = idMapeamentoMapaIn, IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn });
                        if (elementosComplementares.Count() > 0)
                        {
                            var elementosGeoComplementares = MapeamentoElementoGeoBll.Instance.ListarPorParametros(new MapeamentoElementoGeoInfo { IdEmpresa = IdEmpresa, IdMapeamentoElementoIn = string.Join(",", elementosComplementares.Select(p => p.IdMapeamentoElemento.Value)), TipoGeometria = "Polygon" });

                            lstElementoGeo.AddRange(elementosGeoComplementares);
                        }
                    }

                    #endregion

                    lstElementoGeo = DescricaoProducaoUnidadeToElementoGeo(lstProducaoUnidade, lstElementoGeo);
                }

                foreach (var item in lst)
                {
                    MapeamentoElementoGeoInfo geo = new MapeamentoElementoGeoInfo();

                    if (!string.IsNullOrEmpty(item.Longitude) && !string.IsNullOrEmpty(item.Latitude))
                    {
                        EstiloElementoVM estiloElemento = new EstiloElementoVM();

                        if (!string.IsNullOrEmpty(item.QuestionarioConfiguracaoCor))
                        {
                            estiloElemento.color = item.QuestionarioConfiguracaoCor;
                            estiloElemento.fillColor = item.QuestionarioConfiguracaoCor;
                        }

                        geo.wkt = string.Format("POINT ({0} {1})", item.Longitude, item.Latitude);
                        geo.TipoGeometria = "Point";
                        geo.Estilo = JsonConvert.SerializeObject(estiloElemento);
                        geo.MapeamentoElementoDescricao = string.Format("{0} - {1} | {2}", item.Codigo, item.QuestionarioConfiguracaoDescricao, item.DataLancamentoExibicao);
                        geo.MapeamentoElementoObservacao = string.Format("{0}", item.PessoaNomeSecundario);

                        if (item.IdSubAplicacao.HasValue)
                        {
                            //TODO: Refatorar com URGENCIA :: Éverton França
                            var subAplicacao = SubAplicacaoReferenciaBll.Instance.ListarPorCodigo(item.IdSubAplicacao.Value);
                            var referencia = MapeamentoElementoBll.Instance.ListarPorParametros(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 3, IdReferencia = subAplicacao.IdProducaoUnidade.Value }).FirstOrDefault();
                            geo.idElementoReferencia = referencia != null && referencia.IdMapeamentoElemento.HasValue ? referencia.IdMapeamentoElemento : null;
                        }
                        else if (item.IdAplicacao.HasValue)
                        {
                            //TODO: Refatorar com URGENCIA :: Éverton França²
                            var aplicacao = AplicacaoReferenciaBll.Instance.ListarPorCodigo(item.IdAplicacao.Value);
                            var referencia = MapeamentoElementoBll.Instance.ListarPorParametros(new MapeamentoElementoInfo { IdEmpresa = IdEmpresa, TipoObjetoSistema = 2, IdReferencia = aplicacao.IdProducaoLocal.Value }).FirstOrDefault();
                            geo.idElementoReferencia = referencia != null && referencia.IdMapeamentoElemento.HasValue ? referencia.IdMapeamentoElemento : null;
                        }

                        lstElementoGeo.Add(geo);
                    }
                }

                return new JsonResult() { Data = new { Data = lstElementoGeo } };
            }
            else
            {
                return new JsonResult() { Data = new { Data = "[]" } };
            }
        }

        #endregion

        #region Excluir

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult Excluir(int? id)
        {
            MapeamentoElementoGeoInfo info = new MapeamentoElementoGeoInfo();

            if (id.HasValue)
                info.Response = MapeamentoElementoGeoBll.Instance.Excluir(id.Value);
            else
                return Json(new { sucesso = false, mensagem = "Identificador Não Informado." });

            return Json(new { sucesso = info.Response.Sucesso, Data = info.Response });
        }

        #endregion

        private List<MapeamentoElementoGeoInfo> DescricaoProducaoUnidadeToElementoGeo(List<ProducaoUnidadeInfo> lstProducaoUnidade, List<MapeamentoElementoGeoInfo> lstMapeamentoElementoGeo)
        {
            foreach (var elementoGeoItem in lstMapeamentoElementoGeo)
            {
                foreach (var producaoUnidade in lstProducaoUnidade)
                {
                    if (elementoGeoItem.MapeamentoElementoIdReferencia == producaoUnidade.IdProducaoUnidade.Value)
                    {
                        if (!string.IsNullOrEmpty(producaoUnidade.ProducaoLocalCompleto) && !string.IsNullOrEmpty(producaoUnidade.DescricaoCompleta))
                        {
                            elementoGeoItem.MapeamentoElementoObservacao = string.Format("{0} <br> {1} - {2}", producaoUnidade.ProducaoLocalCompleto2, producaoUnidade.Codigo, producaoUnidade.Descricao);
                            break;
                        }
                    }
                }
            }


            return lstMapeamentoElementoGeo;
        }

        public ActionResult DadosMapaCalor()
        {
            List<Info> lst = new List<Info>();

            #region Talhão 11
            // Talhão 11
            lst.Add(new Info
            {
                Ponto = "Ponto 1",
                wkt = "POINT(-47.75482 -20.90982)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 0 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 7 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 2",
                wkt = "POINT(-47.75482 -20.91079)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 2 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 15 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 3",
                wkt = "POINT(-47.755 -20.91165)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 4 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 17 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 4",
                wkt = "POINT(-47.75449 -20.91244)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 3 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 5",
                wkt = "POINT(-47.7533 -20.91227)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 25 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 6",
                wkt = "POINT(-47.75241 -20.91177)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 2 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 30 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 7",
                wkt = "POINT(-47.75225 -20.9124)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 2 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 35 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 8",
                wkt = "POINT(-47.75327 -20.91307)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 0 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 40 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 9",
                wkt = "POINT(-47.75417 -20.91329)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 0 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 60 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 10",
                wkt = "POINT(-47.7553  -20.91336)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 30 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 11",
                wkt = "POINT(-47.75621 -20.91269)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 5 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 40 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 12",
                wkt = "POINT(-47.75621 -20.91269)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 6 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 30 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 13",
                wkt = "POINT(-47.75596 -20.91082)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 9 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 14",
                wkt = "POINT(-47.75691 -20.9107)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 11 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 10 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 15",
                wkt = "POINT(-47.75727 -20.91158)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 7 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 30 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 16",
                wkt = "POINT(-47.75734 -20.91232)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 2 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 10 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 17",
                wkt = "POINT(-47.75711 -20.91327)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 4 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 18",
                wkt = "POINT(-47.75612 -20.9137)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 19",
                wkt = "POINT(-47.75475 -20.91391)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 15 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 20",
                wkt = "POINT(-47.75829 -20.91172)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75738596916199 -20.91385574373971,-47.75810480117798 -20.912923694769464,-47.75905966758728 -20.91169402956494,-47.75904893875122 -20.91159380807868,-47.75627017021179 -20.909950166147077,-47.75612533092499 -20.910015741478674,-47.7560019493103 -20.91003328042816,-47.755822241306305 -20.910073369447883,-47.75572568178177 -20.91011596401959,-47.75557279586792 -20.910151041893076,-47.75556743144989 -20.91018611975839,-47.75548428297043 -20.910206164249157,-47.755422592163086 -20.91017860307365,-47.75533676147461 -20.91016106414115,-47.755197286605835 -20.9101585585792,-47.75513559579849 -20.910120975144885,-47.755039036273956 -20.91008589726435,-47.75501221418381 -20.910023258171552,-47.75501221418381 -20.909968135748258,-47.75500416755676 -20.909885452075326,-47.75498807430267 -20.909822812898813,-47.75498807430267 -20.909757668127472,-47.754950523376465 -20.909727601300386,-47.75487005710602 -20.909712567884593,-47.75483250617981 -20.909649928635872,-47.75477349758148 -20.909624872929044,-47.75464743375778 -20.90961485064515,-47.7546152472496 -20.909679995478516,-47.75454819202423 -20.909737623576753,-47.7544704079628 -20.909807779492553,-47.75446504354477 -20.909885452075326,-47.754188776016235 -20.91046826920906,-47.75412976741791 -20.910658691404933,-47.754178047180176 -20.9109142576562,-47.754220962524414 -20.911315145016022,-47.754204869270325 -20.911595765530233,-47.75400638580322 -20.911633896681213,-47.75379180908203 -20.911663963126088,-47.753673791885376 -20.911623874531593,-47.75350213050842 -20.911533675154768,-47.75326609611511 -20.911363298406098,-47.75300860404968 -20.91116285492452,-47.752729654312134 -20.911002499946342,-47.75253653526306 -20.910872211400324,-47.752418518066406 -20.910912300195765,-47.75227904319763 -20.91105261089543,-47.75208592414856 -20.911293143218018,-47.751978635787964 -20.911433453561347,-47.751731872558594 -20.911734118140668,-47.75164604187012 -20.91184436166872,-47.751485109329224 -20.91204480423932,-47.751485109329224 -20.912205158102868,-47.75161385536194 -20.912305379180513,-47.75174260139465 -20.912505821134847,-47.75187134742737 -20.91258599784154,-47.752182483673096 -20.912786439420763,-47.75238633155823 -20.912946792491205,-47.75264382362366 -20.91316727768306,-47.75287985801697 -20.913357696451467,-47.75306224822998 -20.91350802688729,-47.753437757492065 -20.913728511253844,-47.753663063049316 -20.913828731313277,-47.75387763977051 -20.913938973301278,-47.75412440299988 -20.914109347123294,-47.754456996917725 -20.914249654831355,-47.754703760147095 -20.91433985257435,-47.75501489639282 -20.914389962408126,-47.755422592163086 -20.914309786666042,-47.75562644004822 -20.914259676805475,-47.755926847457886 -20.914139413071794,-47.75623798370361 -20.914059237195747,-47.75658130645752 -20.91392895130572,-47.75690317153931 -20.91389888531502,-47.757182121276855 -20.913908907312592,-47.75738596916199 -20.91385574373971))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 0 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 10 }
                }
            });

            #endregion

            #region Talhão 12

            lst.Add(new Info
            {
                Ponto = "Ponto 27",
                wkt = "POINT(-47.75482 -20.90982)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 8 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 0 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 28",
                wkt = "POINT(-47.75483 -20.91508)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 4 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 0 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 29",
                wkt = "POINT(-47.75161 -20.91343)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 3 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 5 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 30",
                wkt = "POINT(-47.75083 -20.91421)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 3 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 10 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 31",
                wkt = "POINT(-47.75192 -20.91485)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 15 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 32",
                wkt = "POINT(-47.75339 -20.91548)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 1 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 33",
                wkt = "POINT(-47.75482 -20.90982)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 3 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 25 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 34",
                wkt = "POINT(-47.75513 -20.91599)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 3 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 25 }
                }
            });
            lst.Add(new Info
            {
                Ponto = "Ponto 35",
                wkt = "POINT(-47.7546 -20.91682)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 4 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 30 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 36",
                wkt = "POINT(-47.75287 -20.91598)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 7 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 35 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 37",
                wkt = "POINT(-47.75092 -20.91505)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75503635406494 -20.91786362921769,-47.755208015441895 -20.9172021933893,-47.75548696517944 -20.91662092919146,-47.75587320327759 -20.916119837556792,-47.75625944137573 -20.915458394034466,-47.75677442550659 -20.914796947593867,-47.75707483291626 -20.91439606954297,-47.75730013847351 -20.91399519042021,-47.75707483291626 -20.913935058459327,-47.75686025619507 -20.913935058459327,-47.75651693344116 -20.9140152344018,-47.756211161613464 -20.914115454269577,-47.75604486465454 -20.91417558615809,-47.75587320327759 -20.914250750984806,-47.75529384613037 -20.91437602561229,-47.754929065704346 -20.914416113470974,-47.75462865829468 -20.91437602561229,-47.754199504852295 -20.914215674070356,-47.75378108024597 -20.913975146435934,-47.75340557098389 -20.91379475045688,-47.75286912918091 -20.913423935818315,-47.75250434875488 -20.91311325257727,-47.752203941345215 -20.91289276730596,-47.75177478790283 -20.912531972526313,-47.7515172958374 -20.912231309546776,-47.751173973083496 -20.912491884163884,-47.750980854034424 -20.912732414177743,-47.750701904296875 -20.913053120262695,-47.750229835510254 -20.913654442323118,-47.74980068206787 -20.914215674070356,-47.74954319000244 -20.91469672818162,-47.749435901641846 -20.91493725465845,-47.750208377838135 -20.915318087457575,-47.751431465148926 -20.915919400433967,-47.75237560272217 -20.916440536395935,-47.75349140167236 -20.917061888444454,-47.75428533554077 -20.917522889913,-47.75503635406494 -20.91786362921769))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 8 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 40 }
                }
            });

            #endregion

            #region Talhão 13

            lst.Add(new Info
            {
                Ponto = "Ponto 14",
                wkt = "POINT(-47.75723 -20.91609)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75513291358948 -20.917907630882095,-47.75524020195007 -20.917326369419566,-47.75540113449097 -20.916995650684907,-47.75601267814636 -20.915983446387493,-47.756441831588745 -20.915382133668007,-47.756688594818115 -20.915001301031563,-47.756720781326294 -20.914931147545655,-47.75784730911255 -20.91567276845024,-47.75850176811218 -20.916043577526796,-47.75912404060364 -20.916474516652645,-47.75961756706238 -20.916725062086574,-47.76007890701294 -20.916865367346677,-47.76057243347168 -20.917025716054575,-47.760658264160156 -20.917055781418195,-47.7606475353241 -20.91733639118802,-47.76061534881592 -20.917556869924375,-47.76049733161926 -20.91750676114913,-47.76028275489807 -20.917496739392078,-47.75984287261963 -20.91750676114913,-47.75949954986572 -20.917496739392078,-47.75912404060364 -20.917496739392078,-47.75858759880066 -20.917466674116888,-47.75837302207947 -20.91741656531149,-47.757943868637085 -20.917316347650445,-47.75738596916199 -20.917155999253435,-47.75705337524414 -20.917095868560338,-47.75662422180176 -20.91698562889367,-47.75629162788391 -20.916995650684907,-47.75602340698242 -20.91714597747293,-47.75579810142517 -20.91737647825511,-47.75558352470398 -20.91774728311755,-47.75556206703186 -20.918027891592942,-47.75557279586792 -20.91821830418779,-47.75513291358948 -20.917907630882095))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 5 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 15",
                wkt = "POINT(-47.75628 -20.91658)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75513291358948 -20.917907630882095,-47.75524020195007 -20.917326369419566,-47.75540113449097 -20.916995650684907,-47.75601267814636 -20.915983446387493,-47.756441831588745 -20.915382133668007,-47.756688594818115 -20.915001301031563,-47.756720781326294 -20.914931147545655,-47.75784730911255 -20.91567276845024,-47.75850176811218 -20.916043577526796,-47.75912404060364 -20.916474516652645,-47.75961756706238 -20.916725062086574,-47.76007890701294 -20.916865367346677,-47.76057243347168 -20.917025716054575,-47.760658264160156 -20.917055781418195,-47.7606475353241 -20.91733639118802,-47.76061534881592 -20.917556869924375,-47.76049733161926 -20.91750676114913,-47.76028275489807 -20.917496739392078,-47.75984287261963 -20.91750676114913,-47.75949954986572 -20.917496739392078,-47.75912404060364 -20.917496739392078,-47.75858759880066 -20.917466674116888,-47.75837302207947 -20.91741656531149,-47.757943868637085 -20.917316347650445,-47.75738596916199 -20.917155999253435,-47.75705337524414 -20.917095868560338,-47.75662422180176 -20.91698562889367,-47.75629162788391 -20.916995650684907,-47.75602340698242 -20.91714597747293,-47.75579810142517 -20.91737647825511,-47.75558352470398 -20.91774728311755,-47.75556206703186 -20.918027891592942,-47.75557279586792 -20.91821830418779,-47.75513291358948 -20.917907630882095))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 9 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 20 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 16",
                wkt = "POINT(-47.75813 -20.91696)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75513291358948 -20.917907630882095,-47.75524020195007 -20.917326369419566,-47.75540113449097 -20.916995650684907,-47.75601267814636 -20.915983446387493,-47.756441831588745 -20.915382133668007,-47.756688594818115 -20.915001301031563,-47.756720781326294 -20.914931147545655,-47.75784730911255 -20.91567276845024,-47.75850176811218 -20.916043577526796,-47.75912404060364 -20.916474516652645,-47.75961756706238 -20.916725062086574,-47.76007890701294 -20.916865367346677,-47.76057243347168 -20.917025716054575,-47.760658264160156 -20.917055781418195,-47.7606475353241 -20.91733639118802,-47.76061534881592 -20.917556869924375,-47.76049733161926 -20.91750676114913,-47.76028275489807 -20.917496739392078,-47.75984287261963 -20.91750676114913,-47.75949954986572 -20.917496739392078,-47.75912404060364 -20.917496739392078,-47.75858759880066 -20.917466674116888,-47.75837302207947 -20.91741656531149,-47.757943868637085 -20.917316347650445,-47.75738596916199 -20.917155999253435,-47.75705337524414 -20.917095868560338,-47.75662422180176 -20.91698562889367,-47.75629162788391 -20.916995650684907,-47.75602340698242 -20.91714597747293,-47.75579810142517 -20.91737647825511,-47.75558352470398 -20.91774728311755,-47.75556206703186 -20.918027891592942,-47.75557279586792 -20.91821830418779,-47.75513291358948 -20.917907630882095))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 7 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 15 }
                }
            });

            lst.Add(new Info
            {
                Ponto = "Ponto 17",
                wkt = "POINT(-47.75968 -20.91716)",
                ElementoPerimetro = new Info.ElementoGeo() { wkt = "POLYGON((-47.75513291358948 -20.917907630882095,-47.75524020195007 -20.917326369419566,-47.75540113449097 -20.916995650684907,-47.75601267814636 -20.915983446387493,-47.756441831588745 -20.915382133668007,-47.756688594818115 -20.915001301031563,-47.756720781326294 -20.914931147545655,-47.75784730911255 -20.91567276845024,-47.75850176811218 -20.916043577526796,-47.75912404060364 -20.916474516652645,-47.75961756706238 -20.916725062086574,-47.76007890701294 -20.916865367346677,-47.76057243347168 -20.917025716054575,-47.760658264160156 -20.917055781418195,-47.7606475353241 -20.91733639118802,-47.76061534881592 -20.917556869924375,-47.76049733161926 -20.91750676114913,-47.76028275489807 -20.917496739392078,-47.75984287261963 -20.91750676114913,-47.75949954986572 -20.917496739392078,-47.75912404060364 -20.917496739392078,-47.75858759880066 -20.917466674116888,-47.75837302207947 -20.91741656531149,-47.757943868637085 -20.917316347650445,-47.75738596916199 -20.917155999253435,-47.75705337524414 -20.917095868560338,-47.75662422180176 -20.91698562889367,-47.75629162788391 -20.916995650684907,-47.75602340698242 -20.91714597747293,-47.75579810142517 -20.91737647825511,-47.75558352470398 -20.91774728311755,-47.75556206703186 -20.918027891592942,-47.75557279586792 -20.91821830418779,-47.75513291358948 -20.917907630882095))" },
                AlvosValores = new List<Info.AlvoValor>()
                {
                    new Info.AlvoValor { Descricao = "Alvo 1", Valor = 8 }, new Info.AlvoValor { Descricao = "Alvo 2", Valor = 15 }
                }
            });

            #endregion

            return Json(new { lst });
        }

    }
}
