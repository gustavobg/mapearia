﻿using HTM.MasterGestor.Bll.Mapeamento;
using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Mapeamento;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Mapa;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.Controllers
{
    public class MapaController : ControllerExtended
    {
        public MapaController()
        {
            IndexUrl = "Mapeamento/Mapa/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Mapa()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region Mapa Elemento

        [HttpGet, CustomAuthorize]
        public ActionResult MapaElemento(int? id)
        {
            MapaElementoVM vm = new MapaElementoVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult MapaElementoJson(int? id)
        {
            MapaElementoVM vm = new MapaElementoVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoMapaInfo info = MapeamentoMapaBll.Instance.ListarPorParametros(new MapeamentoMapaInfo { IdMapeamentoMapa = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoMapa, "MapeamentoMapa");
            }
            else
            {
                vm.Ativo = true;
                vm.VisivelConsulta = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult MapaElemento(MapaElementoVM vm)
        {
            MapeamentoMapaInfo info = new MapeamentoMapaInfo();

            ViewModelToModelMapper.Map<MapeamentoMapaInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            #region Conversão
            if (!string.IsNullOrEmpty(vm.IdsMapeamentoMapa))
            {
                //CommaSeparatedToList(vm.IdsMapeamentoMapa).ForEach((Action<int>)(id =>
                //{
                //    MapeamentoMapaCamadaInfo item = new MapeamentoMapaCamadaInfo();
                //    item.IdMapeamentoCamada = id;

                //    info.Camadas.Add(item);
                //}));
            }

            #endregion

            var response = MapeamentoMapaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoMapa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoMapa, "MapeamentoMapa", info.Response.IdHistorico);
            return Json(response);
        }

        #endregion

        #region NewEdit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();

            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();
            if (id.HasValue && id > 0)
            {
                MapeamentoMapaInfo info = MapeamentoMapaBll.Instance.ListarPorParametros(new MapeamentoMapaInfo { IdMapeamentoMapa = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdMapeamentoMapa, "MapeamentoMapa");
            }
            else
            {
                vm.Ativo = true;
                vm.VisivelConsulta = true;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            MapeamentoMapaInfo info = new MapeamentoMapaInfo();

            ViewModelToModelMapper.Map<MapeamentoMapaInfo>(vm, info);

            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.IdPessoaOperacao = IdPessoa;

            #region Conversão
            if (!string.IsNullOrEmpty(vm.IdsMapeamentoMapa))
            {
                //CommaSeparatedToList(vm.IdsMapeamentoMapa).ForEach((Action<int>)(id =>
                //{
                //    MapeamentoMapaCamadaInfo item = new MapeamentoMapaCamadaInfo();
                //    item.IdMapeamentoCamada = id;

                //    info.Camadas.Add(item);
                //}));
            }
            #endregion

            var response = MapeamentoMapaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdMapeamentoMapa.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, info.IdMapeamentoMapa, "MapeamentoMapa", info.Response.IdHistorico);
            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            MapeamentoMapaEmpresaInfo info = new MapeamentoMapaEmpresaInfo();
            MapeamentoMapaEmpresaInfo response = new MapeamentoMapaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoMapaEmpresaBll.Instance.ListarPorParametros(new MapeamentoMapaEmpresaInfo { IdMapeamentoMapa = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = MapeamentoMapaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoMapa, "MapeamentoMapa", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            MapeamentoMapaEmpresaInfo info = new MapeamentoMapaEmpresaInfo();
            MapeamentoMapaEmpresaInfo response = new MapeamentoMapaEmpresaInfo();

            if (id > 0)
            {
                info = MapeamentoMapaEmpresaBll.Instance.ListarPorParametros(new MapeamentoMapaEmpresaInfo { IdMapeamentoMapa = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = MapeamentoMapaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdMapeamentoMapa, "MapeamentoMapa", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        #region Listagem

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            MapeamentoMapaInfo info = new MapeamentoMapaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoMapa = vm.IdMapeamentoMapa;
            info.Descricao = vm.Descricao;
            info.VisivelConsulta = vm.VisivelConsulta;
            info.SomenteLeitura = vm.SomenteLeitura;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<MapeamentoMapaInfo> retorno = new ListPaged<MapeamentoMapaInfo>(MapeamentoMapaBll.Instance.ListarPorParametros(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Mapas ");
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult ListAprofundado(ListVM vm)
        {
            MapeamentoMapaInfo info = new MapeamentoMapaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdMapeamentoMapa = vm.IdMapeamentoMapa;
            info.IdMapeamentoMapaIn = vm.IdMapeamentoMapaIn;
            info.IdMapeamentoCamadaIn = vm.IdMapeamentoCamadaIn;
            info.IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn;
            info.Descricao = vm.Descricao;
            info.VisivelConsulta = vm.VisivelConsulta;
            info.SomenteLeitura = vm.SomenteLeitura;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<MapeamentoMapaInfo> retorno = new ListPaged<MapeamentoMapaInfo>(MapeamentoMapaBll.Instance.ListarPorParametrosAprofundado(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };
            return base.CreateListResult(vm, retorno, "Exportação: Mapas", true);
        }

        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public string RetornaMapaPorEmpresa(int idMapeamentoMapa, string idMapeamentoCamadaCategoriaIn)
        {
            List<MapeamentoCamadaMapaItemInfo> lst = new List<MapeamentoCamadaMapaItemInfo>();

            if (idMapeamentoMapa > 0)
            {
                MapeamentoCamadaMapaItemInfo info = new MapeamentoCamadaMapaItemInfo();

                lst = MapeamentoCamadaMapaItemBll.Instance.ListarObjetosParaArvore(idEmpresa: IdEmpresa, idMapeamentoMapa: idMapeamentoMapa);
                if (lst.Count > 0)
                {
                    var x = GerarArvore(lst, idMapeamentoCamadaCategoriaIn: idMapeamentoCamadaCategoriaIn);

                    //var arr = idMapeamentoCamadaCategoriaIn.Split(',');

                    //List<int> lstId = new List<int>();

                    //CommaSeparatedToList(idMapeamentoCamadaCategoriaIn).ForEach(id =>
                    //{
                    //    lstId.Add(id);
                    //});

                    //foreach (JsTree item in x)
                    //{
                    //    bool hide = false;
                    //    foreach (var id in lstId)
                    //    {
                    //        if (int.Parse(item.data.id3) != id)
                    //        {
                    //            hide = true;
                    //            break;
                    //        }
                    //        else
                    //        {
                    //            hide = false;
                    //        }
                    //    }
                    //    if (hide)
                    //        item.li_attr.css_class = "jstree-hide";
                    //}

                    var y = JsonConvert.SerializeObject(x);

                    var formattedData = y.Replace("css_class", "class").Replace("is_checked", "checked");
                    return formattedData;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Retorna arvore para Mapa (Questionário/MedidorItem/Mapa)
        /// </summary>
        /// <param name="vm"> View Model utilizada na controller Questionario/MedidorItem/List </param>
        /// <returns></returns>
        [JsonFilter(Param = "vm", JsonDataType = typeof(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem.ListVM))]
        [HttpPost]
        [CustomAuthorize]
        public string QuestionarioMedidor_RetornaArvorePorProducaoUnidade(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem.ListVM vm)
        {
            #region Conversão

            QuestionarioMedidorItemInfo info = new QuestionarioMedidorItemInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Codigo = vm.Codigo;
            info.IdQuestionarioMedidorItem = vm.IdQuestionarioMedidorItem;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;
            info.IdQuestionarioMedidorItemIn = vm.IdQuestionarioMedidorItemIn;
            info.IdQuestionarioMedidorItemNotIn = vm.IdQuestionarioMedidorItemNotIn;
            info.IdQuestionarioCategoriaMedidorIn = vm.IdQuestionarioCategoriaMedidorIn;

            #endregion

            var lst = QuestionarioMedidorItemBll.Instance.ListarPorParametros(info);
            if (lst.Count() > 0)
            {
                string idProducaoUnidadeIn = string.Join(",", lst.Select(p => p.IdProducaoUnidade.Value));
                var elementos = MapeamentoCamadaMapaItemBll.Instance.ListarElementoPorMapaTematico2(new MapeamentoCamadaMapaItemInfo { IdEmpresa = IdEmpresa, IdProducaoUnidadeIn = idProducaoUnidadeIn, IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn }, MapaTematicoEnum.MedidorItem);

                var tree = GerarArvorePorHierarquia(elementos);
                var tree_str = JsonConvert.SerializeObject(tree);
                var formattedData = tree_str.Replace("css_class", "class").Replace("is_checked", "checked");

                return formattedData;
            }
            else
                return "[]";
        }

        /// <summary>
        /// Retorna arvore para Mapa (Questionario/Resposta/Mapa)
        /// </summary>
        /// <param name="vm">View Model Utilizada na controller Questionario/Resposta/List </param>
        /// <returns></returns>
        [JsonFilter(Param = "vm", JsonDataType = typeof(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta.ListVM))]
        [HttpPost]
        [CustomAuthorize]
        public string QuestionarioResposta_RetornaArvorePorProducaoUnidade(HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta.ListVM vm)
        {
            #region Conversão VM to INFO

            QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.IdQuestionarioResposta = vm.IdQuestionarioResposta;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdAplicacao = vm.IdAplicacaoReferencia;
            info.IdSubAplicacao = vm.IdSubAplicacaoReferencia;
            info.Situacao = vm.Situacao;

            if (vm.Todos.HasValue && !vm.IdPessoaLancamento.HasValue)
            {
                if (vm.Todos.Value == false)
                    info.IdPessoaLancamento = IdPessoa;
            }
            else
                info.IdPessoaLancamento = vm.IdPessoaLancamento;

            #endregion

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataLancamentoInicio))
            {
                info.DataLancamentoInicio = DateTime.Parse(vm.DataLancamentoInicio);
            }
            if (!string.IsNullOrEmpty(vm.DataLancamentoFim))
            {
                info.DataLancamentoFim = DateTime.Parse(vm.DataLancamentoFim);
            }

            #endregion

            #region Conversão PAGE

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            var lst = QuestionarioRespostaBll.Instance.ListarPorParametros(info);
            if (lst.Count() > 0)
            {
                string idSubAplicacaoIn = string.Join(",", lst.Select(p => p.IdSubAplicacao.Value));
                var elementos = MapeamentoCamadaMapaItemBll.Instance.ListarElementoPorMapaTematico2(new MapeamentoCamadaMapaItemInfo { IdEmpresa = IdEmpresa, IdSubAplicacaoReferenciaIn = idSubAplicacaoIn, IdMapeamentoCamadaCategoriaIn = vm.IdMapeamentoCamadaCategoriaIn }, MapaTematicoEnum.QuestionarioResposta);

                var tree = GerarArvorePorHierarquia(elementos);
                var tree_str = JsonConvert.SerializeObject(tree);
                var formattedData = tree_str.Replace("css_class", "class").Replace("is_checked", "checked");

                return formattedData;
            }

            return "[]";
        }

        #endregion

        #region Exclusão

        /// <summary>
        /// Exclui todos os Objetos por MAPA e Empresa
        /// </summary>
        /// <param name="idMapeamentoMapa">Identificador do MAPA</param>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult ExcluirMapa(int idMapeamentoMapa)
        {
            var response = MapeamentoCamadaMapaItemBll.Instance.ExcluirPorMapa(IdEmpresa, idMapeamentoMapa);

            return Json(new { sucesso = response.Sucesso, Data = response });
        }

        /// <summary>
        /// Exclui todos os Objetos por Empresa, Mapa e Camada
        /// </summary>
        /// <param name="idMapeamentoMapa"></param>
        /// <param name="idMapeamentoCamada"></param>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult ExcluirCamadaMapa(int idMapeamentoMapa, int idMapeamentoCamada)
        {
            var response = MapeamentoCamadaMapaItemBll.Instance.ExcluirPorMapaCamada(IdEmpresa, idMapeamentoMapa, idMapeamentoCamada);

            return Json(new { Sucesso = response.Sucesso, Data = response });
        }

        /// <summary>
        /// Exclui o elemento pelo seu próprio identificador.
        /// </summary>
        /// <param name="idMapeamentoElemento"></param>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize, ExceptionFilter]
        public ActionResult ExcluirElemento(int idMapeamentoElemento)
        {
            var response = MapeamentoCamadaMapaItemBll.Instance.ExcluirElemento(IdEmpresa, idMapeamentoElemento);

            return Json(new { Sucesso = response.Sucesso, Data = response });
        }

        #endregion

        #region Salve Tree

        [HttpPost]
        [CustomAuthorize]
        public ActionResult SaveTree(MapaTreeVM vm)
        {
            List<MapeamentoCamadaMapaItemInfo> retorno = new List<MapeamentoCamadaMapaItemInfo>();

            retorno = DesfazerArvore(vm.Tree);

            var response = MapeamentoCamadaMapaItemBll.Instance.SalvarNovaArvore(retorno, idEmpresa: IdEmpresa, idPessoaOperacao: IdPessoa, idMapeamentoMapa: vm.IdMapa);

            return Json(new { Response = "", Sucesso = true });
        }

        #endregion

        #region Métodos privados

        private List<JsTree> GerarArvore(List<MapeamentoCamadaMapaItemInfo> itens, MapeamentoCamadaMapaItemInfo itemAtual = null, string idMapeamentoCamadaCategoriaIn = null)
        {
            List<JsTree> retorno = new List<JsTree>();
            List<MapeamentoCamadaMapaItemInfo> nivelCorrente;

            if (itemAtual == null)
            {
                nivelCorrente = itens.Where(x => !x.IdMapeamentoElemento.HasValue).OrderBy(x => x.Ordem).ToList();
            }
            else
            {
                nivelCorrente = itens.Where(x => x.IdMapeamentoElemento.HasValue && x.IdMapeamentoCamada == itemAtual.IdMapeamentoCamada && !itemAtual.IdMapeamentoElemento.HasValue).OrderBy(x => x.Ordem).ToList();
            }

            foreach (MapeamentoCamadaMapaItemInfo camadaItem in nivelCorrente)
            {
                JsTree treeItem = new JsTree();

                bool IsLayer = !camadaItem.IdMapeamentoElemento.HasValue ? true : false;

                //string idMapeamentoMapa = camadaItem.IdMapeamentoMapa.Value.ToString() + "-" + camadaItem.IdMapeamentoCamada.Value.ToString();
                string idMapeamentoMapa = camadaItem.IdMapeamentoMapa.Value.ToString();
                treeItem.data.id = IsLayer ? idMapeamentoMapa : camadaItem.IdMapeamentoElemento.ToString();
                treeItem.data.id2 = camadaItem.IdMapeamentoCamada.ToString();
                treeItem.data.id3 = camadaItem.IdMapeamentoCamadaCategoria.ToString();
                treeItem.data.reference = camadaItem.IdMapeamentoCamadaMapaItem.Value.ToString();

                if (IsLayer)
                {
                    treeItem.text = "<span class='jstree-text'>" + camadaItem.MapeamentoCamadaDescricao + "</span>" + string.Format(" <a href='#' class='btn-camada-opcoes' role='button' data-id='{0}'><i class='material-icons'>&#xE5D4;</i></b>", camadaItem.IdMapeamentoCamada.Value.ToString());
                }
                else
                {
                    treeItem.text = string.Format("<i class=\"material-icons icon-geometry\">{0}</i>", GetIcon(camadaItem.MapeamentoElementoGeoTipoGeometria)) + "<span class='jstree-text'>" + camadaItem.MapeamentoElementoDescricao + "</span> " + string.Format("<a href='#' class='btn-elemento-opcoes' role='button' data-id='{0}'><i class='material-icons'>&#xE5D4;</i></b>", camadaItem.IdMapeamentoElemento.Value.ToString());
                }

                treeItem.type = IsLayer ? "folder" : "default";
                treeItem.icon = IsLayer ? "fa fa-folder" : "material-icons";
                treeItem.state = new NodeState(false, false, false);

                LiAttr li = new LiAttr();

                if (IsLayer)
                {
                    li.id = "li-" + idMapeamentoMapa + "-" + camadaItem.IdMapeamentoCamada.Value.ToString();
                    if (camadaItem.MapeamentoCamadaCategoriaBase.HasValue && camadaItem.MapeamentoCamadaCategoriaBase.Value)
                    {
                        li.css_class = "jstree-hide";
                    }
                    else
                    {
                        // esconde todos por padrão, somente exibe se idMapeamentoCamadaCategoriaIn for nulo, ou IdMapeamentoCamadaCategoria estiver contido no IN
                        if (string.IsNullOrEmpty(idMapeamentoCamadaCategoriaIn))
                        {
                            li.css_class = "";
                        }
                        else
                        {
                            string[] idMapeamentoCamadaCategoriaInArr = idMapeamentoCamadaCategoriaIn.Split(',');
                            if (idMapeamentoCamadaCategoriaInArr.Contains(camadaItem.IdMapeamentoCamadaCategoria.Value.ToString()))
                            {
                                li.css_class = "";
                            }
                            else
                            {
                                li.css_class = "jstree-hide";
                            }
                        }

                    }
                    treeItem.li_attr = li;
                }
                else
                {
                    li.id = "li-" + idMapeamentoMapa + "-" + camadaItem.IdMapeamentoCamada.Value.ToString() + "-" + camadaItem.IdMapeamentoElemento.ToString();
                    li.is_material = true;
                    treeItem.li_attr = li;
                }

                treeItem.children = new List<JsTree>();
                treeItem.children.AddRange(GerarArvore(itens, camadaItem, idMapeamentoCamadaCategoriaIn));

                retorno.Add(treeItem);
            }

            return retorno;
        }
        private List<JsTree> GerarArvorePorHierarquia(List<MapeamentoCamadaMapaItemInfo> itens, MapeamentoCamadaMapaItemInfo itemAtual = null)
        {
            List<JsTree> retorno = new List<JsTree>();
            List<MapeamentoCamadaMapaItemInfo> nivelCorrente;

            if (itemAtual == null)
            {
                nivelCorrente = itens.Where(x => !x.idPai.HasValue).OrderBy(x => x.Ordem).ToList();
            }
            else
            {
                nivelCorrente = itens.Where(x => x.idPai == itemAtual.id).OrderBy(x => x.Ordem).ToList();
            }

            foreach (MapeamentoCamadaMapaItemInfo camadaItem in nivelCorrente)
            {
                JsTree treeItem = new JsTree();

                string idMapeamentoMapa = camadaItem.IdMapeamentoMapa.Value.ToString();
                //treeItem.data.id2 = camadaItem.IdMapeamentoCamada.ToString();

                string liId = "li-" + idMapeamentoMapa;

                if (camadaItem.IdMapeamentoCamadaCategoria.HasValue)
                {
                    liId += "-" + camadaItem.IdMapeamentoCamadaCategoria.Value.ToString();
                }
                if (camadaItem.IdMapeamentoCamada.HasValue)
                {
                    liId += "-" + camadaItem.IdMapeamentoCamada.Value.ToString();
                }
                if (camadaItem.IdMapeamentoElemento.HasValue)
                {
                    liId += "-" + camadaItem.IdMapeamentoElemento.Value.ToString();
                    treeItem.data.id = camadaItem.IdMapeamentoElemento.Value.ToString();
                }

                if (camadaItem.IdMapeamentoElemento.HasValue)
                {
                    treeItem.text = string.Format("<i class=\"material-icons\">{0}</i> {1}", GetIcon(camadaItem.MapeamentoElementoGeoTipoGeometria), camadaItem.DescricaoExibicao);
                }
                else
                {
                    treeItem.text = camadaItem.DescricaoExibicao;
                }

                treeItem.type = camadaItem.IdMapeamentoElemento.HasValue ? "default" : "folder";
                treeItem.icon = camadaItem.IdMapeamentoElemento.HasValue ? "material-icons" : "fa fa-folder";
                treeItem.state = new NodeState(true, false, false, true);

                LiAttr li = new LiAttr();

                li.id = liId;
                li.is_material = true;
                treeItem.li_attr = li;

                treeItem.children = new List<JsTree>();
                treeItem.children.AddRange(GerarArvorePorHierarquia(itens, camadaItem));

                retorno.Add(treeItem);
            }

            return retorno;
        }
        private List<MapeamentoCamadaMapaItemInfo> DesfazerArvore(List<JsTree> itens)
        {
            List<MapeamentoCamadaMapaItemInfo> retorno = new List<MapeamentoCamadaMapaItemInfo>();

            int ordem = 0;
            foreach (JsTree treeItem in itens)
            {
                ordem++;
                MapeamentoCamadaMapaItemInfo item = new MapeamentoCamadaMapaItemInfo();
                string[] chave = treeItem.li_attr.id.Split('-');

                if (treeItem.type == "folder")
                {
                    item.IdMapeamentoMapa = int.Parse(chave[1]);
                    item.IdMapeamentoCamada = int.Parse(chave[2]);
                    item.Ordem = ordem;
                    retorno.Add(item);
                }

                if (treeItem.children.Count() > 0)
                {
                    //int ordemFilho = 0;
                    foreach (var filho in treeItem.children)
                    {
                        ordem++;
                        //ordemFilho++;
                        MapeamentoCamadaMapaItemInfo itemfilho = new MapeamentoCamadaMapaItemInfo();
                        string[] chaveFilho = filho.li_attr.id.Split('-');

                        if (filho.type == "default")
                        {
                            itemfilho.IdMapeamentoMapa = int.Parse(chaveFilho[1]);
                            itemfilho.IdMapeamentoCamada = int.Parse(chaveFilho[2]);
                            itemfilho.IdMapeamentoElemento = int.Parse(chaveFilho[3]);

                            itemfilho.Ordem = ordem;
                            retorno.Add(itemfilho);
                        }
                    }
                }
            }

            return retorno;
        }
        private string GetIcon(string tipoGeometria)
        {
            string retorno = "format_shapes";

            switch (tipoGeometria.ToLower())
            {
                default:
                case "polygon":
                    retorno = "format_shapes";
                    break;
                case "multilinestring":
                    retorno = "timeline";
                    break;
                case "polyline":
                    retorno = "timeline";
                    break;
                case "point":
                    retorno = "place";
                    break;
            }

            return retorno;
        }

        #endregion
    }
}