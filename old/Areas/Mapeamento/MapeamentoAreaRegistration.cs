﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento
{
    public class MapeamentoAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Mapeamento";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Mapeamento_default",
                "Mapeamento/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
