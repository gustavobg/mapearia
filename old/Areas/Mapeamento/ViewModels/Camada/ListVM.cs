﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Camada
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdMapeamentoCamada { get; set; }
        public int? IdMapeamentoCamadaCategoria { get; set; }
        public int? IdMapeamentoMapa { get; set; }

        public string BuscaAvancada { get; set; }
        public string Descricao { get; set; }
        public string IdMapeamentoCamadaNotIn { get; set; }
        public string IdMapeamentoCamadaCategoriaIn { get; set; }

        public bool? SomenteLeitura { get; set; }
        public bool? VisivelConsulta { get; set; }
        public bool? UltimoNivel { get; set; }
        public bool? MapeamentoCamadaCategoriaBase { get; set; } //Utilizado apenas para retornar camadas com vinculo com Categoria de Mapa Base = true
        public bool? SemCamadaBaseAtual { get; set; }
        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }
    }
}