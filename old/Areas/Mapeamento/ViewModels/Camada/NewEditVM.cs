﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Camada
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamada { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamadaCategoria { get; set; }
        
        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNaturezaOperacional { get; set; }

        [ViewModelToModelAttribute]
        public bool SomenteLeitura { get; set; }

        [ViewModelToModelAttribute]
        public bool VisivelConsulta { get; set; }

        [ViewModelToModelAttribute]
        public bool UltimoNivel { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }
    }
}