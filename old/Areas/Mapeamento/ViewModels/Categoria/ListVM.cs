﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Categoria
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdMapeamentoCamadaCategoria { get; set; }
        public int? IdNaturezaOperacional { get; set; }

        public string IdMapeamentoCamadaCategoriaIn { get; set; }
        public string IdMapeamentoCamadaCategoriaNotIn { get; set; }
        public string Descricao { get; set; }
        public string BuscaAvancada { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Base { get; set; }
        public bool? Ativo { get; set; }        
    }
}