﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Configuracao
{
    public class NewEditVM: VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoConfiguracao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoMapaPadrao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeDistanciaExtra { get; set; }

        [ViewModelToModelAttribute]
        public int? IdUnidadeAreaExtra { get; set; }


    }


}