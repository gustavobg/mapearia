﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Elemento
{
    public class ElementoVM : VMNewEditBase
    {
        public ElementoVM()
        {
            ElementoGeo = new ElementoGeoVM();
        }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoElemento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoMapa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamada { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoObjetoSistema { get; set; }

        [ViewModelToModelAttribute]
        public int? IdReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdReferenciaAuxiliar { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool? VisivelConsulta { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamadaMapaItem { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public ElementoGeoVM ElementoGeo { get; set; }

        public class ElementoGeoVM
        {
            public ElementoGeoVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdMapeamentoElementoGeo { get; set; }

            [ViewModelToModelAttribute]
            public int? IdMapeamentoCamada { get; set; }

            [ViewModelToModelAttribute]
            public int? IdMapeamentoElemento { get; set; }

            [ViewModelToModelAttribute]
            public string Estilo { get; set; }

            [ViewModelToModelAttribute]
            public string Latitude { get; set; }

            [ViewModelToModelAttribute]
            public string Longitude { get; set; }

            [ViewModelToModelAttribute]
            public string Area { get; set; }

            [ViewModelToModelAttribute]
            public string Comprimento { get; set; }

            [ViewModelToModelAttribute]
            public string wkt { get; set; }

            [ViewModelToModelAttribute]
            public string centroidX { get; set; }

            [ViewModelToModelAttribute]
            public string centroidY { get; set; }

            [ViewModelToModelAttribute]
            public string centroid { get; set; }

            [ViewModelToModelAttribute]
            public string TipoGeometria { get; set; }

        }
    }
}