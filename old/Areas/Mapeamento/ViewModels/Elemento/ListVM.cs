﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Elemento
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdMapeamentoElemento { get; set; }

        public bool? Ativo { get; set; }
    }
}