﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Elemento
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoElemento { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamada { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoObjetoSistema { get; set; }

        [ViewModelToModelAttribute]
        public int? IdReferencia { get; set; }

        [ViewModelToModelAttribute]
        public int? IdReferenciaAuxiliar { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool? VisivelConsulta { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool Base { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamadaMapaItem { get; set; }
    }
}