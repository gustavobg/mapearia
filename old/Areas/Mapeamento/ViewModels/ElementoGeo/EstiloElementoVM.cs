﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.ElementoGeo
{
    public class EstiloElementoVM
    {
        public EstiloElementoVM()
        {
            stroke = true;
            fill = true;
            draggable = false;
            editable = false;
        }

        public bool stroke { get; set; }

        public string color { get; set; }

        public decimal? weight { get; set; }

        public bool fill { get; set; }

        public string fillColor { get; set; }

        public decimal? fillOpacity { get; set; }

        public string fillRule { get; set; }

        public string dashArray { get; set; }

        public bool draggable { get; set; }

        public bool editable { get; set; }
    }
}