﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.ElementoGeo
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdMapeamentoElementoGeo { get; set; }

        public int? IdMapeamentoElemento { get; set; }

        public int? IdMapeamentoCamada { get; set; }
    }
}