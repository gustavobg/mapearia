﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.ElementoGeo
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoElementoGeo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoCamada { get; set; }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoElemento { get; set; }

        [ViewModelToModelAttribute]
        public string Estilo { get; set; }

        [ViewModelToModelAttribute]
        public string Latitude { get; set; }

        [ViewModelToModelAttribute]
        public string Longitude { get; set; }

        [ViewModelToModelAttribute]
        public string Area { get; set; }

        [ViewModelToModelAttribute]
        public string Comprimento { get; set; }

        [ViewModelToModelAttribute]
        public string wkt { get; set; }

        [ViewModelToModelAttribute]
        public string centroidX { get; set; }

        [ViewModelToModelAttribute]
        public string centroidY { get; set; }

        [ViewModelToModelAttribute]
        public string centroid { get; set; }

        [ViewModelToModelAttribute]
        public string TipoGeometria { get; set; }

    }
}