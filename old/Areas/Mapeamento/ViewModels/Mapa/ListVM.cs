﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Mapa
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }
        public int? IdMapeamentoMapa { get; set; }

        public string IdMapeamentoMapaIn { get; set; }
        public string IdMapeamentoMapaNotIn { get; set; }
        public string IdMapeamentoCamadaIn { get; set; }
        public string IdMapeamentoCamadaCategoriaIn { get; set; }
        public string Descricao { get; set; }

        public bool? VisivelConsulta { get; set; }
        public bool? SomenteLeitura { get; set; }
        public bool? Ativo { get; set; }
        public bool? RegistroProprio { get; set; }

    }
}