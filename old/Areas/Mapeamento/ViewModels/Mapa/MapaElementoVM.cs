﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Mapa
{
    public class MapaElementoVM : VMNewEditBase
    {
        public MapaElementoVM()
        {

        }

        [ViewModelToModelAttribute]
        public int? IdMapeamentoMapa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool VisivelConsulta { get; set; }

        [ViewModelToModelAttribute]
        public bool SomenteLeitura { get; set; }

        [ViewModelToModelAttribute]
        public int? Ordem { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsMapeamentoMapa { get; set; }
    }
}
