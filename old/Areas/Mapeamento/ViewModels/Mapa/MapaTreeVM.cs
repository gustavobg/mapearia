﻿using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.Mapa
{
    public class MapaTreeVM
    {
        public MapaTreeVM()
        {
            Tree = new List<JsTree>();
        }

        public int IdMapa { get; set; }

        public List<JsTree> Tree { get; set; }
    }
}