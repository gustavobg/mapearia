﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Mapeamento.ViewModels.MapaCalor
{
    public class Info
    {

        /// <summary>
        /// Nome do Ponto
        /// </summary>
        public string Ponto { get; set; }

        /// <summary>
        /// wkt do ponto
        /// </summary>
        public string wkt { get; set; }

        public ElementoGeo ElementoPerimetro { get; set; }

        public List<AlvoValor> AlvosValores { get; set; }
        /// <summary>
        /// Perímetro utilizado
        /// </summary>
        public class ElementoGeo
        {
            public ElementoGeo()
            {

            }

            public string wkt { get; set; }
        }

        /// <summary>
        /// Alvo Valor
        /// </summary>
        public class AlvoValor
        {
            public AlvoValor()
            {

            }

            public string Descricao { get; set; }

            /// <summary>
            /// Nota, Valor, Informação Aferida
            /// </summary>
            public decimal? Valor { get; set; }
        }
    }
}