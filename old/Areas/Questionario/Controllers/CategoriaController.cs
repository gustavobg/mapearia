﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Categoria;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class CategoriaController : ControllerExtended
    {
        public CategoriaController()
        {
            IndexUrl = "/Questionario/Categoria/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioCategoriaInfo info = new QuestionarioCategoriaInfo();
                info = QuestionarioCategoriaBll.Instance.ListarPorParametros(new QuestionarioCategoriaInfo { IdQuestionarioCategoria = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioTipo, "QuestionarioCategoria");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioCategoriaInfo info = new QuestionarioCategoriaInfo();
            ViewModelToModelMapper.Map<QuestionarioCategoriaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioCategoriaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioTipo, "QuestionarioCategoria", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioCategoriaEmpresaInfo info = new QuestionarioCategoriaEmpresaInfo();
            QuestionarioCategoriaEmpresaInfo response = new QuestionarioCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioCategoriaEmpresaBll.Instance.ListarPorParametros(new QuestionarioCategoriaEmpresaInfo { IdQuestionarioCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioCategoria, "QuestionarioCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioCategoriaEmpresaInfo info = new QuestionarioCategoriaEmpresaInfo();
            QuestionarioCategoriaEmpresaInfo response = new QuestionarioCategoriaEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioCategoriaEmpresaBll.Instance.ListarPorParametros(new QuestionarioCategoriaEmpresaInfo { IdQuestionarioCategoria = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioCategoriaEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioCategoria, "QuestionarioCategoria", info.Response.IdHistorico);
            }

            return Json(new { response });
        }



        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioCategoriaInfo info = new QuestionarioCategoriaInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioCategoria = vm.IdQuestionarioCategoria;
            info.IdQuestionarioTipo = vm.IdQuestionarioTipo;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioCategoriaInfo> retorno = new ListPaged<QuestionarioCategoriaInfo>(QuestionarioCategoriaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Categoria de Questionário", true);
        }

    }
}
