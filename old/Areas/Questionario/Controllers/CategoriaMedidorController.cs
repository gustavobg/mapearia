﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.CategoriaMedidor;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class CategoriaMedidorController : ControllerExtended
    {
        public CategoriaMedidorController()
        {
            IndexUrl = "/Questionario/CategoriaMedidor/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioCategoriaMedidorInfo info = new QuestionarioCategoriaMedidorInfo();
                info = QuestionarioCategoriaMedidorBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioCategoriaMedidor, "QuestionarioCategoriaMedidor");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioCategoriaMedidorInfo info = new QuestionarioCategoriaMedidorInfo();
            ViewModelToModelMapper.Map<QuestionarioCategoriaMedidorInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Conversão

            if(!string.IsNullOrEmpty(vm.IdsQuestionarioItemControle))
            {
                info.lstItemControleRelacionado = new List<QuestionarioCategoriaMedidorItemControleInfo>();

                CommaSeparatedToList(vm.IdsQuestionarioItemControle).ForEach(id =>
                {
                    QuestionarioCategoriaMedidorItemControleInfo item = new QuestionarioCategoriaMedidorItemControleInfo();
                    item.IdQuestionarioItemControle = id;

                    info.lstItemControleRelacionado.Add(item);
                });
            }

            #endregion

            var response = QuestionarioCategoriaMedidorBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioCategoriaMedidor.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioCategoriaMedidor, "QuestionarioCategoriaMedidor", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioCategoriaMedidorEmpresaInfo info = new QuestionarioCategoriaMedidorEmpresaInfo();
            QuestionarioCategoriaMedidorEmpresaInfo response = new QuestionarioCategoriaMedidorEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioCategoriaMedidorEmpresaBll.Instance.ListarPorParametros(new QuestionarioCategoriaMedidorEmpresaInfo { IdQuestionarioCategoriaMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioCategoriaMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioCategoriaMedidor, "QuestionarioCategoriaMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioCategoriaMedidorEmpresaInfo info = new QuestionarioCategoriaMedidorEmpresaInfo();
            QuestionarioCategoriaMedidorEmpresaInfo response = new QuestionarioCategoriaMedidorEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioCategoriaMedidorEmpresaBll.Instance.ListarPorParametros(new QuestionarioCategoriaMedidorEmpresaInfo { IdQuestionarioCategoriaMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioCategoriaMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioCategoriaMedidor, "QuestionarioCategoriaMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioCategoriaMedidorInfo info = new QuestionarioCategoriaMedidorInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioCategoriaMedidor = vm.IdQuestionarioCategoriaMedidor;
            info.IdQuestionarioCategoriaMedidorIn = vm.IdQuestionarioCategoriaMedidorIn;
            info.IdQuestionarioTipoMedidor = vm.IdQuestionarioTipoMedidor;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioCategoriaMedidorInfo> retorno = new ListPaged<QuestionarioCategoriaMedidorInfo>(QuestionarioCategoriaMedidorBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo de Questionário", true);
        }

    }
}
