﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.ClassificacaoControle;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class ClassificacaoControleController : ControllerExtended
    {
        public ClassificacaoControleController()
        {
            IndexUrl = "/Questionario/ClassificacaoControle/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioClassificacaoControleInfo info = new QuestionarioClassificacaoControleInfo();
                info = QuestionarioClassificacaoControleBll.Instance.ListarPorCodigo(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioClassificacaoControle, "QuestionarioClassificacaoControle");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioClassificacaoControleInfo info = new QuestionarioClassificacaoControleInfo();
            ViewModelToModelMapper.Map<QuestionarioClassificacaoControleInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioClassificacaoControleBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioClassificacaoControle.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioClassificacaoControle, "QuestionarioClassificacaoControle", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioClassificacaoControleEmpresaInfo info = new QuestionarioClassificacaoControleEmpresaInfo();
            QuestionarioClassificacaoControleEmpresaInfo response = new QuestionarioClassificacaoControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioClassificacaoControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioClassificacaoControleEmpresaInfo { IdQuestionarioClassificacaoControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioClassificacaoControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioClassificacaoControle, "QuestionarioClassificacaoControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioClassificacaoControleEmpresaInfo info = new QuestionarioClassificacaoControleEmpresaInfo();
            QuestionarioClassificacaoControleEmpresaInfo response = new QuestionarioClassificacaoControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioClassificacaoControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioClassificacaoControleEmpresaInfo { IdQuestionarioClassificacaoControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioClassificacaoControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioClassificacaoControle, "QuestionarioClassificacaoControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioClassificacaoControleInfo info = new QuestionarioClassificacaoControleInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioClassificacaoControle = vm.IdQuestionarioClassificacaoControle;
            info.IdQuestionarioCategoriaControle = vm.IdQuestionarioCategoriaControle;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioClassificacaoControleInfo> retorno = new ListPaged<QuestionarioClassificacaoControleInfo>(QuestionarioClassificacaoControleBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Classificação do Controle", true);
        }


    }
}
