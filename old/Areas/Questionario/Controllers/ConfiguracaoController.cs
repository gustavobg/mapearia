﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Configuracao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class ConfiguracaoController : ControllerExtended
    {
        public ConfiguracaoController()
        {
            IndexUrl = "/Questionario/Configuracao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioConfiguracaoInfo info = new QuestionarioConfiguracaoInfo();
                info = QuestionarioConfiguracaoBll.Instance.ListarPorParametros(new QuestionarioConfiguracaoInfo { IdQuestionarioConfiguracao  = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioTipo, "QuestionarioConfiguracao");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioConfiguracaoInfo info = new QuestionarioConfiguracaoInfo();
            ViewModelToModelMapper.Map<QuestionarioConfiguracaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioConfiguracaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioTipo.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioTipo, "QuestionarioConfiguracao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioConfiguracaoEmpresaInfo info = new QuestionarioConfiguracaoEmpresaInfo();
            QuestionarioConfiguracaoEmpresaInfo response = new QuestionarioConfiguracaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioConfiguracaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioConfiguracaoEmpresaInfo { IdQuestionarioConfiguracao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioConfiguracaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioConfiguracao, "QuestionarioConfiguracao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioConfiguracaoEmpresaInfo info = new QuestionarioConfiguracaoEmpresaInfo();
            QuestionarioConfiguracaoEmpresaInfo response = new QuestionarioConfiguracaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioConfiguracaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioConfiguracaoEmpresaInfo { IdQuestionarioConfiguracao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioConfiguracaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioConfiguracao, "QuestionarioConfiguracao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioConfiguracaoInfo info = new QuestionarioConfiguracaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Codigo = vm.Codigo;
            info.IdQuestionarioCategoria = vm.IdQuestionarioCategoria;
            info.IdQuestionarioTipo = vm.IdQuestionarioTipo;
            info.IdNaturezaOperacional = vm.IdNaturezaOperacional;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioConfiguracaoInfo> retorno = new ListPaged<QuestionarioConfiguracaoInfo>(QuestionarioConfiguracaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Configuração de Questionário", true);
        }

    }
}
