﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Contexto;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class ContextoController : ControllerExtended
    {
        public ContextoController()
        {
            IndexUrl = "/Questionario/Contexto/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.ParentId = parentId;
            vm.IdQuestionarioVersao = int.Parse(ids[1]);
            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();
            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (!string.IsNullOrEmpty(parentId))
            {
                string[] ids = parentId.Split('-');

                QuestionarioVersaoContextoInfo info = new QuestionarioVersaoContextoInfo();
                info = QuestionarioVersaoContextoBll.Instance.ListarPorIdQuestionarioVersaoCompleto(int.Parse(ids[1]), IdEmpresa);
                if (info != null && info.IdQuestionarioVersaoContexto.HasValue)
                {
                    ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioVersaoContexto, "QuestionarioVersaoContexto");
                }
                else
                {
                    vm = new NewEditVM();
                    vm.IdQuestionarioVersao = int.Parse(ids[1]);
                }
            }
            else
            {
                string[] ids = parentId.Split('-');
                vm.IdQuestionarioVersao = int.Parse(ids[1]);
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioVersaoContextoInfo info = new QuestionarioVersaoContextoInfo();
            ViewModelToModelMapper.Map<QuestionarioVersaoContextoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão - Tela

            if (!string.IsNullOrEmpty(vm.IdsTela))
            {
                info.lstTela = new List<QuestionarioVersaoContextoTelaInfo>();

                CommaSeparatedToList(vm.IdsTela).ForEach(idTela =>
                {
                    QuestionarioVersaoContextoTelaInfo item = new QuestionarioVersaoContextoTelaInfo();
                    item.IdTela = idTela;

                    info.lstTela.Add(item);
                });
            }

            #endregion

            #region Conversão - Pessoa Perfil

            if (!string.IsNullOrEmpty(vm.IdsPessoaPerfil))
            {
                info.lstPessoaPerfil = new List<QuestionarioVersaoContextoPessoaPerfilInfo>();

                CommaSeparatedToList(vm.IdsPessoaPerfil).ForEach(idPessaoPerfil =>
                {
                    QuestionarioVersaoContextoPessoaPerfilInfo item = new QuestionarioVersaoContextoPessoaPerfilInfo();
                    item.IdPessoaPerfil = idPessaoPerfil;

                    info.lstPessoaPerfil.Add(item);
                });
            }

            #endregion

            #region Conversão - Pessoa Empresa (Empresa Usuária)

            if (!string.IsNullOrEmpty(vm.IdsPessoaEmpresa))
            {
                info.lstPessoaEmpresa = new List<QuestionarioVersaoContextoPessoaEmpresaInfo>();

                CommaSeparatedToList(vm.IdsPessoaEmpresa).ForEach(idPessoa =>
                {
                    QuestionarioVersaoContextoPessoaEmpresaInfo item = new QuestionarioVersaoContextoPessoaEmpresaInfo();
                    item.IdPessoa = idPessoa;

                    info.lstPessoaEmpresa.Add(item);
                });
            }

            #endregion

            #region Conversão Pessoa Perfil Aplicador

            if (!string.IsNullOrEmpty(vm.IdsPessoaPerfilAplicador))
            {
                info.lstPessoaPerfilAplicador = new List<QuestionarioVersaoContextoPessoaPerfilAplicadorInfo>();

                CommaSeparatedToList(vm.IdsPessoaPerfilAplicador).ForEach(idPessoaPerfilAplicador =>
                {
                    QuestionarioVersaoContextoPessoaPerfilAplicadorInfo item = new QuestionarioVersaoContextoPessoaPerfilAplicadorInfo();
                    item.IdPessoaPerfil = idPessoaPerfilAplicador;

                    info.lstPessoaPerfilAplicador.Add(item);
                });
            }

            #endregion

            #region Conversão Categoria de Medidor

            if (!string.IsNullOrEmpty(vm.IdsCategoriaMedidor))
            {
                info.lstCategoriaMedidor = new List<QuestionarioVersaoContextoCategoriaMedidorInfo>();

                CommaSeparatedToList(vm.IdsCategoriaMedidor).ForEach(idCategoriaMedidor =>
                {
                    QuestionarioVersaoContextoCategoriaMedidorInfo item = new QuestionarioVersaoContextoCategoriaMedidorInfo();
                    item.IdQuestionarioCategoriaMedidor = idCategoriaMedidor;

                    info.lstCategoriaMedidor.Add(item);
                });
            }

            #endregion

            #region Conversão Pessoa (Aplicador)

            if (!string.IsNullOrEmpty(vm.IdsPessoa))
            {
                info.lstPessoa = new List<QuestionarioVersaoContextoPessoaInfo>();

                CommaSeparatedToList(vm.IdsPessoa).ForEach(idPessoa =>
                {
                    QuestionarioVersaoContextoPessoaInfo item = new QuestionarioVersaoContextoPessoaInfo();
                    item.IdPessoa = idPessoa;

                    info.lstPessoa.Add(item);
                });
            }

            #endregion

            #region Categoria de Serviço

            if (!string.IsNullOrEmpty(vm.IdsCategoriaServico))
            {
                info.lstCategoriaServico = new List<QuestionarioVersaoContextoParametroCategoriaServicoInfo>();

                CommaSeparatedToList(vm.IdsCategoriaServico).ForEach(idCategoriaServico =>
                {
                    QuestionarioVersaoContextoParametroCategoriaServicoInfo item = new QuestionarioVersaoContextoParametroCategoriaServicoInfo();
                    item.IdCategoriaServico = idCategoriaServico;

                    info.lstCategoriaServico.Add(item);
                });
            }

            #endregion

            #region Tipo Documento

            if (!string.IsNullOrEmpty(vm.IdsDocumentoTipo))
            {
                info.lstDocumentoTipo = new List<QuestionarioVersaoContextoParametroDocumentoTipoInfo>();

                CommaSeparatedToList(vm.IdsDocumentoTipo).ForEach(idDocumentoTipo =>
                {
                    QuestionarioVersaoContextoParametroDocumentoTipoInfo item = new QuestionarioVersaoContextoParametroDocumentoTipoInfo();
                    item.IdDocumentoTipo = idDocumentoTipo;

                    info.lstDocumentoTipo.Add(item);
                });
            }

            #endregion

            #region Cultura

            if (!string.IsNullOrEmpty(vm.IdsProducaoCultura))
            {
                info.lstCultura = new List<QuestionarioVersaoContextoParametroProducaoCulturaInfo>();

                CommaSeparatedToList(vm.IdsProducaoCultura).ForEach(idProducaoCultura =>
                {
                    QuestionarioVersaoContextoParametroProducaoCulturaInfo item = new QuestionarioVersaoContextoParametroProducaoCulturaInfo();
                    item.IdProducaoCultura = idProducaoCultura;

                    info.lstCultura.Add(item);
                });
            }

            #endregion

            #region Aplicação Tipo - Parâmetro

            if (!string.IsNullOrEmpty(vm.IdsAplicacaoTipoParametro))
            {
                info.lstAplicacaoTipoParametro = new List<QuestionarioVersaoContextoParametroAplicacaoTipoInfo>();

                CommaSeparatedToList(vm.IdsAplicacaoTipoParametro).ForEach(idAplicacaoTipo =>
                {
                    QuestionarioVersaoContextoParametroAplicacaoTipoInfo item = new QuestionarioVersaoContextoParametroAplicacaoTipoInfo();
                    item.IdAplicacaoTipo = idAplicacaoTipo;

                    info.lstAplicacaoTipoParametro.Add(item);
                });
            }

            #endregion

            #region Sub Aplicação Tipo - Parâmetro

            if (!string.IsNullOrEmpty(vm.IdsSubAplicacaoTipoParametro))
            {
                info.lstSubAplicacaoTipoParametro = new List<QuestionarioVersaoContextoParametroSubAplicacaoTipoInfo>();

                CommaSeparatedToList(vm.IdsSubAplicacaoTipoParametro).ForEach(idSubAplicacaoTipo =>
                {
                    QuestionarioVersaoContextoParametroSubAplicacaoTipoInfo item = new QuestionarioVersaoContextoParametroSubAplicacaoTipoInfo();
                    item.IdSubAplicacaoTipo = idSubAplicacaoTipo;

                    info.lstSubAplicacaoTipoParametro.Add(item);
                });
            }

            #endregion

            var response = QuestionarioVersaoContextoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioVersaoContexto.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioVersaoContexto, "QuestionarioVersaoContexto", response.Response.IdHistorico);

            return Json(response);
        }


        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioVersaoContextoInfo info = new QuestionarioVersaoContextoInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdQuestionarioVersaoContexto = vm.IdQuestionarioVersaoContexto;

            ListPaged<QuestionarioVersaoContextoInfo> retorno = new ListPaged<QuestionarioVersaoContextoInfo>(QuestionarioVersaoContextoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Contexto por Versão", true);
        }

        private string RetornaDescricaoParent(string parentId)
        {
            string[] ids = parentId.Split('-');

            var configuracao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(int.Parse(ids[0]));
            var versao = QuestionarioVersaoBll.Instance.ListarPorCodigo(int.Parse(ids[1]));

            return string.Format("{0} « {1}", configuracao.Descricao, versao.Descricao);
        }

    }
}
