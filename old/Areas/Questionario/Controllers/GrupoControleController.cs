﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.GrupoControle;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class GrupoControleController : ControllerExtended
    {
        public GrupoControleController()
        {
            IndexUrl = "/Questionario/GrupoControle/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioGrupoControleInfo info = new QuestionarioGrupoControleInfo();
                info = QuestionarioGrupoControleBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioGrupoControle, "QuestionarioGrupoControle");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioGrupoControleInfo info = new QuestionarioGrupoControleInfo();
            ViewModelToModelMapper.Map<QuestionarioGrupoControleInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioGrupoControleBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioGrupoControle.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioGrupoControle, "QuestionarioGrupoControle", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioGrupoControleEmpresaInfo info = new QuestionarioGrupoControleEmpresaInfo();
            QuestionarioGrupoControleEmpresaInfo response = new QuestionarioGrupoControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioGrupoControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioGrupoControleEmpresaInfo { IdQuestionarioGrupoControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioGrupoControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioGrupoControle, "QuestionarioGrupoControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioGrupoControleEmpresaInfo info = new QuestionarioGrupoControleEmpresaInfo();
            QuestionarioGrupoControleEmpresaInfo response = new QuestionarioGrupoControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioGrupoControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioGrupoControleEmpresaInfo { IdQuestionarioGrupoControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioGrupoControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioGrupoControle, "QuestionarioGrupoControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioGrupoControleInfo info = new QuestionarioGrupoControleInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioGrupoControle = vm.IdQuestionarioGrupoControle;
            info.IdQuestionarioCategoriaControle = vm.IdQuestionarioCategoriaControle;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioGrupoControleInfo> retorno = new ListPaged<QuestionarioGrupoControleInfo>(QuestionarioGrupoControleBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Categoria de Controle", true);
        }

    }
}
