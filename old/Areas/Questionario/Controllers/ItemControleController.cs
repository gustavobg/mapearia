﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.ItemControle;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class ItemControleController : ControllerExtended
    {
        public ItemControleController()
        {
            IndexUrl = "/Questionario/ItemControle/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioItemControleInfo info = new QuestionarioItemControleInfo();
                info = QuestionarioItemControleBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);

                #region Arquivo Digital

                if(info.lstArquivoDigital.Count() > 0)
                {
                    foreach (var item in info.lstArquivoDigital)
                    {
                        if (!string.IsNullOrEmpty(item.ArquivoDigital.Arquivo))
                            item.ArquivoDigital.CaminhoArquivo = string.Format("{0}/{1}", ArquivoDigitalFolderGet, item.ArquivoDigital.Arquivo);
                    }
                }

                #endregion

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioItemControle, "QuestionarioItemControle");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioItemControleInfo info = new QuestionarioItemControleInfo();
            ViewModelToModelMapper.Map<QuestionarioItemControleInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            #region Tempo Folder

            if(info.lstArquivoDigital.Count() > 0)
            {
                foreach (var item in info.lstArquivoDigital)
                {
                    item.TempFolder = ArquivoDigitalTempFolder;
                    item.ArquivoDigitalFolder = ArquivoDigitalFolderPost;
                    item.IdEmpresa = IdEmpresa;
                }
            }

            #endregion

            var response = QuestionarioItemControleBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioItemControle.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioItemControle, "QuestionarioItemControle", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioItemControleEmpresaInfo info = new QuestionarioItemControleEmpresaInfo();
            QuestionarioItemControleEmpresaInfo response = new QuestionarioItemControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioItemControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioItemControleEmpresaInfo { IdQuestionarioItemControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioItemControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioItemControle, "QuestionarioItemControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioItemControleEmpresaInfo info = new QuestionarioItemControleEmpresaInfo();
            QuestionarioItemControleEmpresaInfo response = new QuestionarioItemControleEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioItemControleEmpresaBll.Instance.ListarPorParametros(new QuestionarioItemControleEmpresaInfo { IdQuestionarioItemControle = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioItemControleEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioItemControle, "QuestionarioItemControle", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioItemControleInfo info = new QuestionarioItemControleInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioItemControle = vm.IdQuestionarioItemControle;
            info.IdQuestionarioCategoriaControle = vm.IdQuestionarioCategoriaControle;
            info.IdQuestionarioGrupoControle = vm.IdQuestionarioGrupoControle;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            info.IdQuestionarioItemControleIn = vm.IdQuestionarioItemControleIn;
            info.IdQuestionarioItemControleNotIn = vm.IdQuestionarioItemControleNotIn;

            ListPaged<QuestionarioItemControleInfo> retorno = new ListPaged<QuestionarioItemControleInfo>(QuestionarioItemControleBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Item de Controle.", true);
        }

    }
}
