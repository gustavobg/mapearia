﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class MedidorItemController : ControllerExtended
    {
        public MedidorItemController()
        {
            IndexUrl = "/Questionario/MedidorItem/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            var atividadeEconomicaPeriodo = AtividadeEconomicaPeriodoBll.Instance.RetornaRegistroRecentePorEmpresa(IdEmpresa);
            if(atividadeEconomicaPeriodo != null)
            {
                vm.idAtividadeEconomicaPeriodo = atividadeEconomicaPeriodo.IdAtividadeEconomicaPeriodo.Value;
            }

            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioMedidorItemInfo info = new QuestionarioMedidorItemInfo();
                info = QuestionarioMedidorItemBll.Instance.ListarPorParametros(new QuestionarioMedidorItemInfo { IdQuestionarioMedidorItem = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                //Conversão
                if (info.DataInstalacao.HasValue)
                    vm.DataInstalacaoExibicao = info.DataInstalacao.Value.ToShortDateString();

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioMedidorItem, "QuestionarioMedidorItem");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioMedidorItemInfo info = new QuestionarioMedidorItemInfo();
            ViewModelToModelMapper.Map<QuestionarioMedidorItemInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            //Conversão
            if (!string.IsNullOrEmpty(vm.DataInstalacaoExibicao))
                info.DataInstalacao = Convert.ToDateTime(vm.DataInstalacaoExibicao);

            var response = QuestionarioMedidorItemBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioMedidorItem.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioMedidorItem, "QuestionarioMedidorItem", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioMedidorItemEmpresaInfo info = new QuestionarioMedidorItemEmpresaInfo();
            QuestionarioMedidorItemEmpresaInfo response = new QuestionarioMedidorItemEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioMedidorItemEmpresaBll.Instance.ListarPorParametros(new QuestionarioMedidorItemEmpresaInfo { IdQuestionarioMedidorItem = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioMedidorItemEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioMedidorItem, "QuestionarioMedidorItem", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioMedidorItemEmpresaInfo info = new QuestionarioMedidorItemEmpresaInfo();
            QuestionarioMedidorItemEmpresaInfo response = new QuestionarioMedidorItemEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioMedidorItemEmpresaBll.Instance.ListarPorParametros(new QuestionarioMedidorItemEmpresaInfo { IdQuestionarioMedidorItem = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioMedidorItemEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioMedidorItem, "QuestionarioMedidorItem", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioMedidorItemInfo info = new QuestionarioMedidorItemInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.Codigo = vm.Codigo;
            info.IdQuestionarioMedidorItem = vm.IdQuestionarioMedidorItem;
            info.IdProducaoLocal = vm.IdProducaoLocal;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;
            info.IdQuestionarioMedidorItemIn = vm.IdQuestionarioMedidorItemIn;
            info.IdQuestionarioMedidorItemNotIn = vm.IdQuestionarioMedidorItemNotIn;
            info.IdQuestionarioCategoriaMedidorIn = vm.IdQuestionarioCategoriaMedidorIn;

            ListPaged<QuestionarioMedidorItemInfo> retorno = new ListPaged<QuestionarioMedidorItemInfo>(QuestionarioMedidorItemBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Medidor de Item", true);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ListarPorId(int id)
        {
            var info = QuestionarioMedidorItemBll.Instance.ListarPorIdCompleto(id, IdEmpresa);

            return Json(new { Data = info }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Mapa

        [CustomAuthorize, ExceptionFilter]
        public ActionResult MapaMedidor()
        {
            //SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View();
        }

        #endregion
    }
}
