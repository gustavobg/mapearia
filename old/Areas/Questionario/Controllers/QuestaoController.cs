﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Questao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class QuestaoController : ControllerExtended
    {
        public QuestaoController()
        {
            IndexUrl = "/Questionario/Questao/Index";
        }

        #region Classes
        public class QuestionarioVersaoTopicoVM
        {

            public QuestionarioVersaoTopicoVM()
            {
                lstQuestao = new List<QuestionarioVersaoQuestaoVM>();
            }

            public int? IdQuestionarioVersaoTopico { get; set; }

            public string Descricao { get; set; }

            public List<QuestionarioVersaoQuestaoVM> lstQuestao { get; set; }

        }

        public class QuestionarioVersaoQuestaoVM
        {

            public QuestionarioVersaoQuestaoVM()
            {
                RespostaValor = new QuestionarioRespostaValorVM();
            }

            public int? IdQuestionarioVersaoQuestao { get; set; }

            public int? IdQuestionarioVersao { get; set; }

            public int? Sequencia { get; set; }

            public string Descricao { get; set; }

            public int? TipoCampo { get; set; }

            public int? TamanhoCampo { get; set; }

            public string SubDescricao { get; set; }

            public string DescricaoDetalhada { get; set; }

            public int? IdQuestionarioVersaoTopico { get; set; }

            public bool? PermiteNaoSeAplica { get; set; }

            public bool? Obrigatorio { get; set; }

            public bool? QuestaoMesmaLinha { get; set; }

            public int? TipoJustificativa { get; set; }

            public bool? Ativo { get; set; }

            public Guid? ChaveVersao { get; set; }

            public QuestionarioRespostaValorVM RespostaValor { get; set; }

            public List<ListaOpcaoVM> lstOpcao { get; set; }

            public class ListaOpcaoVM
            {
                public ListaOpcaoVM()
                {

                }

                public int? IdQuestionarioVersaoQuestaoLista { get; set; }

                public int? IdQuestionarioVersaoQuestao { get; set; }

                public string Valor { get; set; }

            }
        }

        public class QuestionarioRespostaValorVM
        {
            public QuestionarioRespostaValorVM()
            {

            }

            public int? IdQuestionarioRespostaValor { get; set; }

            public int? IdQuestionarioVersaoQuestao { get; set; }

            public int? IdQuestionarioVersaoQuestaoLista { get; set; }

            public string Valor { get; set; }
        }

        #endregion

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.ParentId = parentId;
            vm.IdQuestionarioVersao = int.Parse(ids[1]);
            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();
            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioVersaoQuestaoInfo info = new QuestionarioVersaoQuestaoInfo();
                info = QuestionarioVersaoQuestaoBll.Instance.ListarPorIdCompleto(idEmpresa: IdEmpresa, idQuestionarioVersaoQuestao: id.Value);
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioVersaoQuestao, "QuestionarioVersaoQuestao");
            }
            else
            {
                vm.Ativo = true;
                vm.PermiteValorNulo = true;
                string[] ids = parentId.Split('-');
                vm.IdQuestionarioVersao = int.Parse(ids[1]);
            }

            vm.QuestionarioPublicado = QuestionarioVersaoBll.Instance.ListarPorParametros(new QuestionarioVersaoInfo { IdEmpresa = IdEmpresa, IdQuestionarioVersao = vm.IdQuestionarioVersao }).FirstOrDefault().Publicado;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioVersaoQuestaoInfo info = new QuestionarioVersaoQuestaoInfo();
            ViewModelToModelMapper.Map<QuestionarioVersaoQuestaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            #region Conversão de Datas

            #endregion

            var response = QuestionarioVersaoQuestaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioVersaoQuestao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioVersaoQuestao, "QuestionarioVersaoQuestao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioVersaoQuestaoEmpresaInfo info = new QuestionarioVersaoQuestaoEmpresaInfo();
            QuestionarioVersaoQuestaoEmpresaInfo response = new QuestionarioVersaoQuestaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoQuestaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoQuestaoEmpresaInfo { IdQuestionarioVersaoQuestao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioVersaoQuestaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersaoQuestao, "QuestionarioVersaoQuestao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioVersaoQuestaoEmpresaInfo info = new QuestionarioVersaoQuestaoEmpresaInfo();
            QuestionarioVersaoQuestaoEmpresaInfo response = new QuestionarioVersaoQuestaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoQuestaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoQuestaoEmpresaInfo { IdQuestionarioVersaoQuestao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioVersaoQuestaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersaoQuestao, "QuestionarioVersaoQuestao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioVersaoQuestaoInfo info = new QuestionarioVersaoQuestaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdQuestionarioVersaoQuestao = vm.IdQuestionarioVersaoQuestao;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioVersaoQuestaoInfo> retorno = new ListPaged<QuestionarioVersaoQuestaoInfo>(QuestionarioVersaoQuestaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Questões por Versão", true);
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult ListarPorIdQuestionarioVersaoCompleto(int idQuestionarioVersao)
        {
            List<QuestionarioVersaoTopicoVM> lstvm = new List<QuestionarioVersaoTopicoVM>();

            var lstResponse = QuestionarioVersaoQuestaoBll.Instance.ListaQuestoesPorIdQuestionarioVersao(idEmpresa: IdEmpresa, idQuestionarioVersao: idQuestionarioVersao, ativo: true);
            var lstTopico = lstResponse.OrderBy(y => y.QuestionarioVersaoTopicoSequencia).GroupBy(x => new { x.IdQuestionarioVersaoTopico, x.QuestionarioVersaoTopicoDescricao }).Select(x => x).ToList();

            foreach (var item in lstTopico)
            {
                QuestionarioVersaoTopicoVM info = new QuestionarioVersaoTopicoVM();

                info.IdQuestionarioVersaoTopico = item.Key.IdQuestionarioVersaoTopico;
                info.Descricao = item.Key.QuestionarioVersaoTopicoDescricao;

                foreach (var questaoItem in lstResponse)
                {
                    if (questaoItem.IdQuestionarioVersaoTopico == info.IdQuestionarioVersaoTopico)
                    {

                        QuestionarioVersaoQuestaoVM questao = new QuestionarioVersaoQuestaoVM();

                        questao.IdQuestionarioVersaoQuestao = questaoItem.IdQuestionarioVersaoQuestao;
                        questao.IdQuestionarioVersao = questaoItem.IdQuestionarioVersao;
                        questao.Sequencia = questaoItem.Sequencia;
                        questao.Descricao = questaoItem.Descricao;
                        questao.TipoCampo = questaoItem.TipoCampo;
                        questao.TamanhoCampo = questaoItem.TamanhoCampo;
                        questao.SubDescricao = questaoItem.SubDescricao;
                        questao.DescricaoDetalhada = questaoItem.DescricaoDetalhada;
                        questao.IdQuestionarioVersaoTopico = questaoItem.IdQuestionarioVersaoTopico;
                        questao.PermiteNaoSeAplica = questaoItem.PermiteNaoSeAplica;
                        questao.Obrigatorio = questaoItem.Obrigatorio;
                        questao.QuestaoMesmaLinha = questaoItem.QuestaoMesmaLinha;
                        questao.TipoJustificativa = questaoItem.TipoJustificativa;
                        questao.Ativo = questaoItem.Ativo;
                        questao.ChaveVersao = questaoItem.ChaveVersao;

                        info.lstQuestao.Add(questao);
                    }
                }

                lstvm.Add(info);
            }
            return Json(new { response = lstvm }, JsonRequestBehavior.AllowGet);
        }

        private string RetornaDescricaoParent(string parentId)
        {
            string[] ids = parentId.Split('-');

            var configuracao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(int.Parse(ids[0]));
            var versao = QuestionarioVersaoBll.Instance.ListarPorCodigo(int.Parse(ids[1]));

            if (versao.Publicado.HasValue && versao.Publicado.Value)
                versao.Descricao = string.Format("{0} ({1})", versao.Descricao, "Publicado");

            return string.Format("{0} « {1}", configuracao.Descricao, versao.Descricao);
        }
    }
}
