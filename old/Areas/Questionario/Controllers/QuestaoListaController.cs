﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.QuestaoLista;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class QuestaoListaController : ControllerExtended
    {
        
        public QuestaoListaController()
        {
            IndexUrl = "";
        }

        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioVersaoQuestaoListaInfo info = new QuestionarioVersaoQuestaoListaInfo();

            info.IdQuestionarioVersaoQuestaoLista = vm.IdQuestionarioVersaoQuestaoLista;
            info.IdQuestionarioVersaoQuestaoListaIn = vm.IdQuestionarioVersaoQuestaoListaIn;
            info.IdQuestionarioVersaoQuestao = vm.IdQuestionarioVersaoQuestao;
            info.Valor = vm.Valor;

            ListPaged<QuestionarioVersaoQuestaoListaInfo> retorno = new ListPaged<QuestionarioVersaoQuestaoListaInfo>(QuestionarioVersaoQuestaoListaBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Opções Questão", true);

        }

    }
}
