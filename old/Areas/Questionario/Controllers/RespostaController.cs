﻿using HTM.MasterGestor.Bll.Contabilidade;
using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.Export;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class RespostaController : ControllerExtended
    {
        public RespostaController()
        {
            IndexUrl = "/Questionario/Resposta/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            var questionarioVersao = QuestionarioVersaoBll.Instance.ListarPorParametros(new QuestionarioVersaoInfo { IdEmpresa = IdEmpresa, Ativo = true, Publicado = true }).OrderBy(p => p.IdQuestionarioVersao).FirstOrDefault();
            if (questionarioVersao != null)
                vm.Id = questionarioVersao.IdQuestionarioVersao.Value.ToString();
            else
                vm.Id = string.Empty;

            var atividadeEconomicaPeriodo = AtividadeEconomicaPeriodoBll.Instance.RetornaRegistroRecentePorEmpresa(IdEmpresa);
            if (atividadeEconomicaPeriodo != null)
            {
                vm.idAtividadeEconomicaPeriodo = atividadeEconomicaPeriodo.IdAtividadeEconomicaPeriodo.Value;
            }

            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
                info = QuestionarioRespostaBll.Instance.ListarPorIdQuestionarioRespostaCompleto(id.Value);
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                #region Conversões

                if (info.DataLancamento.HasValue)
                    vm.DataLancamentoExibicao = info.DataLancamento.ToString();

                if (info.DataReferencia.HasValue)
                    vm.DataReferenciaExibicao = info.DataReferencia.ToString();
                else
                    vm.DataReferenciaExibicao = vm.DataLancamentoExibicao;

                #endregion

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioResposta, "QuestionarioResposta");
            }
            else
            {
                //Usuário
                vm.IdPessoaLancamento = ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value;
                vm.DataLancamentoExibicao = DateTime.Now.ToString();
            }
            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
            ViewModelToModelMapper.Map<QuestionarioRespostaInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;

            #region Conversão

            if (!string.IsNullOrEmpty(vm.DataLancamentoExibicao))
                info.DataLancamento = DateTime.Parse(vm.DataLancamentoExibicao);

            if (!string.IsNullOrEmpty(vm.DataReferenciaExibicao))
                info.DataReferencia = DateTime.Parse(vm.DataReferenciaExibicao);

            #endregion

            //Concluído TODO
            info.Situacao = 3;

            var response = QuestionarioRespostaBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioResposta.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioResposta, "QuestionarioResposta", response.Response.IdHistorico);

            return Json(response);
        }

        #endregion

        [CustomAuthorize, ExceptionFilter]
        public ActionResult MapaResposta()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.IdQuestionarioResposta = vm.IdQuestionarioResposta;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdAplicacao = vm.IdAplicacaoReferencia;
            info.IdSubAplicacao = vm.IdSubAplicacaoReferencia;
            info.Situacao = vm.Situacao;

            if (vm.Todos.HasValue && !vm.IdPessoaLancamento.HasValue)
            {
                if (vm.Todos.Value == false)
                    info.IdPessoaLancamento = IdPessoa;
            }
            else
                info.IdPessoaLancamento = vm.IdPessoaLancamento;

            #region Conversão
            if (!string.IsNullOrEmpty(vm.DataLancamentoInicio))
            {
                info.DataLancamentoInicio = DateTime.Parse(vm.DataLancamentoInicio);
            }
            if (!string.IsNullOrEmpty(vm.DataLancamentoFim))
            {
                info.DataLancamentoFim = DateTime.Parse(vm.DataLancamentoFim);
            }
            #endregion

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<QuestionarioRespostaInfo> retorno = new ListPaged<QuestionarioRespostaInfo>(QuestionarioRespostaBll.Instance.ListarQuestionariosParaExibicao(info)) { };
            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Questionário/Respostas");
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListExport(ListVM vm)
        {
            string table = string.Empty;
            List<string> lstColsClean = new List<string>();

            QuestionarioRespostaInfo info = new QuestionarioRespostaInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdAtividadeEconomicaPeriodo = vm.IdAtividadeEconomicaPeriodo;
            info.IdQuestionarioResposta = vm.IdQuestionarioResposta;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdAplicacao = vm.IdAplicacaoReferencia;
            info.IdSubAplicacao = vm.IdSubAplicacaoReferencia;

            #region Pessoa Lançamento

            if (vm.Todos.HasValue && !vm.IdPessoaLancamento.HasValue)
            {
                if (vm.Todos.Value == false)
                {
                    info.IdPessoaLancamentoSTR = IdPessoa.ToString();
                }
                else
                {
                    info.IdPessoaLancamentoSTR = "null";
                }
            }
            else
            {
                if (vm.IdPessoaLancamento.HasValue)
                {
                    info.IdPessoaLancamentoSTR = vm.IdPessoaLancamento.ToString();
                }
                else
                {
                    info.IdPessoaLancamentoSTR = "null";
                }
            }

            #endregion

            #region Data
            //Data Inicio
            if(!string.IsNullOrEmpty(vm.DataLancamentoInicio))
            {
                info.DataLancamentoInicioSTR = vm.DataLancamentoInicio;
            }
            else
            {
                info.DataLancamentoInicioSTR = new DateTime(1900, 1, 1).ToShortDateString();
            }

            //Data Fim
            if(!string.IsNullOrEmpty(vm.DataLancamentoFim))
            {
                info.DataLancamentoFimSTR = vm.DataLancamentoFim;
            }
            else
            {
                info.DataLancamentoFimSTR = DateTime.Now.ToShortDateString();
            }
            #endregion

            #region Get Cols

            if (vm.IdQuestionarioVersao.HasValue)
            {
                var lstQuestao = QuestionarioVersaoQuestaoBll.Instance.ListarPorParametros(new QuestionarioVersaoQuestaoInfo { IdQuestionarioVersao = info.IdQuestionarioVersao.Value, IdEmpresa = IdEmpresa, Ativo = true })
                    .OrderBy(p => p.SequenciaTopico).ThenBy(p => p.Sequencia).ThenBy(p => p.Descricao).ToList();

                info.cols = string.Join(",", lstQuestao.Select(p => p.cols));

                //Adiciona IdResquinarioRespsta e Colunas (Perguntas)
                lstColsClean.Add("CodigoResposta");
                lstColsClean.Add("EmpresaUsuaria");
                lstColsClean.Add("NomePessoaAplicador");
                lstColsClean.Add("NomePessoaLancamento");
                lstColsClean.Add("DataLancamento");
                lstColsClean.Add("DataReferencia");
                lstColsClean.Add("CodigoMedidor");
                lstColsClean.Add("DescricaoMedidor");
                lstColsClean.Add("DescricaoItem");
                lstColsClean.Add("ProducaoLocal");
                lstColsClean.Add("ProducaoUnidade");
                lstColsClean.Add("CodigoAmostra");
                lstColsClean.Add("DescricaoAmostra");
                lstColsClean.Add("Proprietario");
                //lstColsClean.AddRange(lstQuestao.Select(p => p.Descricao).ToList()); //Alteração realizada no dia 25/11/2016 a pedido do Humberto e Coplana
                lstColsClean.AddRange(lstQuestao.Select(p => p.TopicoQuestao).ToList()); 
            }

            #endregion

            var retorno = QuestionarioRespostaBll.Instance.ListarRespostaExportacao(info);
            table = ConvertDataTableToHTML(retorno, lstColsClean);
            return CreateListExportByHTMlTable(vm, "Exportação: Questionário", table);
        }

        private static string ConvertDataTableToHTML(DataTable dt, List<string> lstCols)
        {
            //add header row
            string html = "<thead>";
            html += "<tr>";
            for (int i = 0; i < lstCols.Count; i++)
                html += "<th>" + lstCols[i] + "</th>";
            html += "</tr>";
            html += "</thead>";

            html += "<tbody>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</tbody>";
            return html;
        }
    }
}
