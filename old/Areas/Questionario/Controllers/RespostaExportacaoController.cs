﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web.MVC;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.RespostaExportacao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class RespostaExportacaoController : ControllerExtended
    {
        public ActionResult Index()
        {
            return View();
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioRespostaValorInfo info = new QuestionarioRespostaValorInfo();

            //info.IdEmpresa = IdEmpresa;
            //info.IdQuestionarioResposta = vm.IdQuestionarioResposta;
            //info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            //info.IdAplicacao = vm.IdAplicacaoReferencia;
            //info.IdSubAplicacao = vm.IdSubAplicacaoReferencia;
            //info.Situacao = vm.Situacao;

            //if (vm.Todos.HasValue && !vm.IdPessoaLancamento.HasValue)
            //{
            //    if (vm.Todos.Value == false)
            //        info.IdPessoaLancamento = IdPessoa;
            //}
            //else
            //    info.IdPessoaLancamento = vm.IdPessoaLancamento;

            info.IdQuestionarioVersao = 5;

            
            ListPaged<QuestionarioRespostaValorInfo> retorno = new ListPaged<QuestionarioRespostaValorInfo>(QuestionarioRespostaValorBll.Instance.ListarPorParametrosParaExportacao(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            #region Exportação Customizada

            bool exportacaoCustomizada = false;
            if (vm.ExportFileType.HasValue)
            {
                exportacaoCustomizada = true;
                var lst = QuestionarioRespostaValorBll.Instance.ListarPorParametrosParaExportacao(new QuestionarioRespostaValorInfo { IdQuestionarioVersao = 5 }).
                    OrderBy(p => p.Questao).ThenBy(x => x.QuestaoSequencia).GroupBy(p => p.Questao);
                if (lst.Count() > 0)
                {
                    vm.columns.Clear();
                    foreach (var item in lst)
                    {
                        ColumnInfo col = new ColumnInfo();

                        col.displayName = item.First().Questao;
                        col.header = true;
                        col.id = "Valor";

                        vm.columns.Add(col);
                    }
                }
            }

            #endregion

            return base.CreateListResult(vm, retorno, "Exportação: Questionário", true);
        }

    }
}
