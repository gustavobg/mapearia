﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.TipoMedidor;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class TipoMedidorController : ControllerExtended
    {
        public TipoMedidorController()
        {
            IndexUrl = "/Questionario/TipoMedidor/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioTipoMedidorInfo info = new QuestionarioTipoMedidorInfo();
                info = QuestionarioTipoMedidorBll.Instance.ListarPorParametros(new QuestionarioTipoMedidorInfo { IdQuestionarioTipoMedidor = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioTipoMedidor, "QuestionarioTipoMedidor");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioTipoMedidorInfo info = new QuestionarioTipoMedidorInfo();
            ViewModelToModelMapper.Map<QuestionarioTipoMedidorInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioTipoMedidorBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioTipoMedidor.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioTipoMedidor, "QuestionarioTipoMedidor", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioTipoMedidorEmpresaInfo info = new QuestionarioTipoMedidorEmpresaInfo();
            QuestionarioTipoMedidorEmpresaInfo response = new QuestionarioTipoMedidorEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioTipoMedidorEmpresaBll.Instance.ListarPorParametros(new QuestionarioTipoMedidorEmpresaInfo { IdQuestionarioTipoMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioTipoMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioTipoMedidor, "QuestionarioTipoMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioTipoMedidorEmpresaInfo info = new QuestionarioTipoMedidorEmpresaInfo();
            QuestionarioTipoMedidorEmpresaInfo response = new QuestionarioTipoMedidorEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioTipoMedidorEmpresaBll.Instance.ListarPorParametros(new QuestionarioTipoMedidorEmpresaInfo { IdQuestionarioTipoMedidor = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioTipoMedidorEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioTipoMedidor, "QuestionarioTipoMedidor", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioTipoMedidorInfo info = new QuestionarioTipoMedidorInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioTipoMedidor = vm.IdQuestionarioTipoMedidor;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioTipoMedidorInfo> retorno = new ListPaged<QuestionarioTipoMedidorInfo>(QuestionarioTipoMedidorBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tipo Medidor", true);
        }

    }
}
