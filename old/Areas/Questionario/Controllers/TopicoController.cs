﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Topico;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class TopicoController : ControllerExtended
    {
        
        public TopicoController()
        {
            IndexUrl = "/Questionario/Topico/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.ParentId = parentId;
            vm.IdQuestionarioVersao = int.Parse(ids[1]);
            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();
            //"IdQuestionarioConfiguracao-IdQuestionarioVersao"
            string[] ids = parentId.Split('-');

            vm.DescricaoExibicao = RetornaDescricaoParent(parentId);
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(string parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioVersaoTopicoInfo info = new QuestionarioVersaoTopicoInfo();
                info = QuestionarioVersaoTopicoBll.Instance.ListarPorIdCompleto(idEmpresa: IdEmpresa, idQuestionarioVersaoTopico: id.Value);
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = ArquivoDigitalFolderGet + "/" + x.Arquivo);

                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioVersaoTopico, "QuestionarioVersaoTopico");
            }
            else
            {
                vm.Ativo = true;
                string[] ids = parentId.Split('-');
                vm.IdQuestionarioVersao = int.Parse(ids[1]);

            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioVersaoTopicoInfo info = new QuestionarioVersaoTopicoInfo();
            ViewModelToModelMapper.Map<QuestionarioVersaoTopicoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresaLogada = IdEmpresa;
            info.IdEmpresa = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            var response = QuestionarioVersaoTopicoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioVersaoTopico.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioVersaoTopico, "QuestionarioVersaoTopico", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioVersaoTopicoEmpresaInfo info = new QuestionarioVersaoTopicoEmpresaInfo();
            QuestionarioVersaoTopicoEmpresaInfo response = new QuestionarioVersaoTopicoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoTopicoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoTopicoEmpresaInfo { IdQuestionarioVersaoTopico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioVersaoTopicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersaoTopico, "QuestionarioVersaoTopico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioVersaoTopicoEmpresaInfo info = new QuestionarioVersaoTopicoEmpresaInfo();
            QuestionarioVersaoTopicoEmpresaInfo response = new QuestionarioVersaoTopicoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoTopicoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoTopicoEmpresaInfo { IdQuestionarioVersaoTopico = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioVersaoTopicoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersaoTopico, "QuestionarioVersaoTopico", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioVersaoTopicoInfo info = new QuestionarioVersaoTopicoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdQuestionarioVersaoTopico = vm.IdQuestionarioVersaoTopico;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioVersaoTopicoInfo> retorno = new ListPaged<QuestionarioVersaoTopicoInfo>(QuestionarioVersaoTopicoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tópicos por Questionário", true);
        }

        private string RetornaDescricaoParent(string parentId)
        {
            string[] ids = parentId.Split('-');

            var configuracao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(int.Parse(ids[0]));
            var versao = QuestionarioVersaoBll.Instance.ListarPorCodigo(int.Parse(ids[1]));

            if (versao.Publicado.HasValue && versao.Publicado.Value)
                versao.Descricao = string.Format("{0} ({1})", versao.Descricao, "Publicado");

            return string.Format("{0} « {1}", configuracao.Descricao, versao.Descricao);
        }

    }
}
