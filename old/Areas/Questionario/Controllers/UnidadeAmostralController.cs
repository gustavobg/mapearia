﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.UnidadeAmostral;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class UnidadeAmostralController : ControllerExtended
    {
        public UnidadeAmostralController()
        {
            IndexUrl = "/Questionario/UnidadeAmostral/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioUnidadeAmostralInfo info = new QuestionarioUnidadeAmostralInfo();
                info = QuestionarioUnidadeAmostralBll.Instance.ListarPorParametros(new QuestionarioUnidadeAmostralInfo { IdQuestionarioUnidadeAmostral = id.Value, IdEmpresa = IdEmpresa }).FirstOrDefault();
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioUnidadeAmostral, "QuestionarioUnidadeAmostral");
            }
            else
                vm.Ativo = true;

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioUnidadeAmostralInfo info = new QuestionarioUnidadeAmostralInfo();
            ViewModelToModelMapper.Map<QuestionarioUnidadeAmostralInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            var response = QuestionarioUnidadeAmostralBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioUnidadeAmostral.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioUnidadeAmostral, "QuestionarioUnidadeAmostral", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioUnidadeAmostralEmpresaInfo info = new QuestionarioUnidadeAmostralEmpresaInfo();
            QuestionarioUnidadeAmostralEmpresaInfo response = new QuestionarioUnidadeAmostralEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioUnidadeAmostralEmpresaBll.Instance.ListarPorParametros(new QuestionarioUnidadeAmostralEmpresaInfo { IdQuestionarioUnidadeAmostral = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioUnidadeAmostralEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioUnidadeAmostral, "QuestionarioUnidadeAmostral", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioUnidadeAmostralEmpresaInfo info = new QuestionarioUnidadeAmostralEmpresaInfo();
            QuestionarioUnidadeAmostralEmpresaInfo response = new QuestionarioUnidadeAmostralEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioUnidadeAmostralEmpresaBll.Instance.ListarPorParametros(new QuestionarioUnidadeAmostralEmpresaInfo { IdQuestionarioUnidadeAmostral = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioUnidadeAmostralEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioUnidadeAmostral, "QuestionarioUnidadeAmostral", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioUnidadeAmostralInfo info = new QuestionarioUnidadeAmostralInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioUnidadeAmostral = vm.IdQuestionarioUnidadeAmostral;
            info.Descricao = vm.Descricao;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;

            ListPaged<QuestionarioUnidadeAmostralInfo> retorno = new ListPaged<QuestionarioUnidadeAmostralInfo>(QuestionarioUnidadeAmostralBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Unidade Amostral ", true);
        }

    }
}
