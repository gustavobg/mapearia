﻿using HTM.MasterGestor.Bll.Questionario;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Versao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.Controllers
{
    public class VersaoController : ControllerExtended
    {
        public VersaoController()
        {
            IndexUrl = "/Questionario/Versao/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index(string parentId)
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);

            string[] ids = parentId.Split('-');
            vm.ParentId = int.Parse(ids[0]);
            vm.DescricaoExibicao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(int.Parse(ids[0])).Descricao;
            return View(vm);
        }

        #region New Edit

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            //Descrição da Configuração do Questionário.
            vm.DescricaoExibicao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(parentId.Value).Descricao;
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? parentId, int? id)
        {
            NewEditVM vm = new NewEditVM();

            if (id.HasValue && id > 0)
            {
                QuestionarioVersaoInfo info = new QuestionarioVersaoInfo();
                info = QuestionarioVersaoBll.Instance.ListarPorIdCompleto(id.Value, IdEmpresa);
                info.ArquivosDigitais.ForEach(x => x.CaminhoArquivo = string.Format("{0}/{1}", ArquivoDigitalFolderGet, x.Arquivo));
                
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                #region Conversões

                if (info.PeriodoInicio.HasValue)
                    vm.PeriodoInicioExibicao = info.PeriodoInicio.Value.ToShortDateString();

                if (info.PeriodoTermino.HasValue)
                    vm.PeriodoTerminoExibicao = info.PeriodoTermino.Value.ToShortDateString();

                #endregion

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, vm.IdQuestionarioVersao, "QuestionarioVersao");
            }
            else
            {
                vm.Ativo = true;
                vm.IdQuestionarioConfiguracao = parentId;
            }

            return Json(vm);
        }

        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            QuestionarioVersaoInfo info = new QuestionarioVersaoInfo();
            ViewModelToModelMapper.Map<QuestionarioVersaoInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;
            info.TempFolder = ArquivoDigitalTempFolder;
            info.ArquivoDigitalFolder = ArquivoDigitalFolderPost;

            #region Conversão de Datas

            if (!string.IsNullOrEmpty(vm.PeriodoInicioExibicao))
                info.PeriodoInicio = DateTime.Parse(vm.PeriodoInicioExibicao);

            if (!string.IsNullOrEmpty(vm.PeriodoTerminoExibicao))
                info.PeriodoTermino = DateTime.Parse(vm.PeriodoTerminoExibicao);

            #endregion

            var response = QuestionarioVersaoBll.Instance.Salvar(info);
            if (response.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdQuestionarioVersao.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdQuestionarioVersao, "QuestionarioVersao", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Ativar(int id)
        {
            QuestionarioVersaoEmpresaInfo info = new QuestionarioVersaoEmpresaInfo();
            QuestionarioVersaoEmpresaInfo response = new QuestionarioVersaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoEmpresaInfo { IdQuestionarioVersao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = true;

                response = QuestionarioVersaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersao, "QuestionarioVersao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        [HttpPost, CustomAuthorize]
        public ActionResult Inativar(int id)
        {
            QuestionarioVersaoEmpresaInfo info = new QuestionarioVersaoEmpresaInfo();
            QuestionarioVersaoEmpresaInfo response = new QuestionarioVersaoEmpresaInfo();

            if (id > 0)
            {
                info = QuestionarioVersaoEmpresaBll.Instance.ListarPorParametros(new QuestionarioVersaoEmpresaInfo { IdQuestionarioVersao = id, IdEmpresa = IdEmpresa }).FirstOrDefault();
                info.IdEmpresaLogada = IdEmpresa;
                info.IdPessoaOperacao = IdPessoa;
                info.IdEmpresa = IdEmpresa;
                info.Ativo = false;

                response = QuestionarioVersaoEmpresaBll.Instance.Salvar(info);
                if (info.Response.Sucesso)
                    SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Edicao, info.IdQuestionarioVersao, "QuestionarioVersao", info.Response.IdHistorico);
            }

            return Json(new { response });
        }

        #endregion

        #region Listagem

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            QuestionarioVersaoInfo info = new QuestionarioVersaoInfo(IdEmpresa);
            info.IdEmpresa = IdEmpresa;
            info.IdQuestionarioConfiguracao = vm.IdQuestionarioConfiguracao;
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.Descricao = vm.Descricao;
            info.Publicado = vm.Publicado;
            info.Ativo = vm.Ativo;
            info.RegistroProprio = vm.RegistroProprio;


            ListPaged<QuestionarioVersaoInfo> retorno = new ListPaged<QuestionarioVersaoInfo>(QuestionarioVersaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Versão do Questionário", true);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM)), CustomAuthorize]
        public ActionResult ListTopico(ListVM vm)
        {
            QuestionarioVersaoTopicoInfo info = new QuestionarioVersaoTopicoInfo();
            info.IdQuestionarioVersao = vm.IdQuestionarioVersao;
            info.IdQuestionarioVersaoTopico = vm.IdQuestionarioVersaoTopico;
            info.Descricao = vm.Descricao;
            info.SequenciaDescricao = vm.SequenciaDescricao;
            info.Ativo = vm.Ativo;

            ListPaged<QuestionarioVersaoTopicoInfo> retorno = new ListPaged<QuestionarioVersaoTopicoInfo>(QuestionarioVersaoTopicoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Tópicos por Versão", true);
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult ListIdQuestionarioVersaoCompleto(int id)
        {
            var info = QuestionarioVersaoBll.Instance.ListarPorIdCompleto(id, IdEmpresa);

            return Json(new { response = info });
        }

        #endregion

        private string RetornaDescricaoParent(string parentId)
        {
            string[] ids = parentId.Split('-');

            var configuracao = QuestionarioConfiguracaoBll.Instance.ListarPorCodigo(int.Parse(ids[0]));
            var versao = QuestionarioVersaoBll.Instance.ListarPorCodigo(int.Parse(ids[1]));

            return string.Format("{0} « {1}", configuracao.Descricao, versao.Descricao);
        }
    }
}
