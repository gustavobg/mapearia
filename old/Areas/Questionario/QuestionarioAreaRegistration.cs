﻿using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario
{
    public class QuestionarioAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Questionario";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Questionario_default",
                "Questionario/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
