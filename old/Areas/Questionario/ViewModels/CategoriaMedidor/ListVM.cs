﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.CategoriaMedidor
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdQuestionarioCategoriaMedidor { get; set; }
        public int? IdQuestionarioTipoMedidor { get; set; }

        public string IdQuestionarioCategoriaMedidorIn { get; set; }
        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

    }
}