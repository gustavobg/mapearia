﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Configuracao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdQuestionarioConfiguracao { get; set; }
        public int? IdQuestionarioCategoria { get; set; }
        public int? IdQuestionarioTipo { get; set; }
        public int? IdNaturezaOperacional { get; set; }

        public string Descricao { get; set; }
        public string Codigo { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}