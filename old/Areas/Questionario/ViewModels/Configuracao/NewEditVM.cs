﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Configuracao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioConfiguracao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoria { get; set; }

        [ViewModelToModelAttribute]
        public int? IdNaturezaOperacional { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string SubDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public string ReferenciaControle { get; set; }

        [ViewModelToModelAttribute]
        public string Cor { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }
    }
}