﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Contexto
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersaoContexto { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoOutraReferencia { get; set; }

        [ViewModelToModelAttribute]
        public bool EmpresaUsuariaObrigatoria { get; set; }

        [ViewModelToModelAttribute]
        public bool InformaHora { get; set; }

        [ViewModelToModelAttribute]
        public bool InformaPessoa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteDataDiferenteLancamento { get; set; }

        [ViewModelToModelAttribute]
        public bool InformaPeriodoAtividade { get; set; }

        [ViewModelToModelAttribute]
        public string ControleQualidadeReferencia { get; set; }
        
        [ViewModelToModelAttribute]
        public int? IdQuestionarioUnidadeAmostral { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteAdicionarPessoa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteConsultaPessoa { get; set; }

        [ViewModelToModelAttribute]
        public bool PermitePessoaDiferenteDoDigitador { get; set; }

        [ViewModelToModelAttribute]
        public string NomeAplicador { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoQuestionarioPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoAplicacaoQuestionario { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteSubAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        #region Ids

        [ViewModelToModelAttribute]
        public string IdsTela { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaPerfil { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string IdsPessoaPerfilAplicador { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCategoriaMedidor { get;set;}

        [ViewModelToModelAttribute]
        public string IdsPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string IdsCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public string IdsDocumentoTipo { get; set; }

        [ViewModelToModelAttribute]
        public string IdsProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public string IdsAplicacaoTipoParametro { get; set; }

        [ViewModelToModelAttribute]
        public string IdsSubAplicacaoTipoParametro { get; set; }

        #endregion
    }
}