﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.GrupoControle
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstItemAvaliacao = new List<ItemAvaliacaoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioGrupoControle { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoriaControle { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemAvaliacaoVM> lstItemAvaliacao { get; set; }

        public class ItemAvaliacaoVM
        {
            public ItemAvaliacaoVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioGrupoControleItemAvaliacao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioGrupoControle { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }
        }
    }
}