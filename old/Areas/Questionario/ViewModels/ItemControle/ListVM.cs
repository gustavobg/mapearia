﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.ItemControle
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdQuestionarioItemControle { get; set; }
        public int? IdQuestionarioCategoriaControle { get; set; }
        public int? IdQuestionarioGrupoControle { get; set; }

        public string IdQuestionarioItemControleIn { get; set; }
        public string IdQuestionarioItemControleNotIn { get; set; }
        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

    }
}