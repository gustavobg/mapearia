﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.ItemControle
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstCultura = new List<ItemControleCuturaVM>();
            lstArquivoDigital = new List<ItemControleArquivoDigitalVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioItemControle { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoriaControle { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioGrupoControle { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoTecnica { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoResumida { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemControleCuturaVM> lstCultura { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ItemControleArquivoDigitalVM> lstArquivoDigital { get; set; }

        #region  Propriedade Modelo de Conteúdo 

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        #endregion

        public class ItemControleCuturaVM
        {
            public ItemControleCuturaVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioItemControleCultura { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioItemControle { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioClassificacaoControle { get; set; }

            [ViewModelToModelAttribute]
            public int? IdProducaoCultura { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            #region Propriedades Especiais e Descrições

            [ViewModelToModelAttribute]
            public string IdsProducaoVariedade { get; set; }

            [ViewModelToModelAttribute]
            public string QuestionarioClassificacaoControleDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string ProducaoCulturaDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoResumida { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoTecnica { get; set; }

            #endregion
        }

        public class ItemControleArquivoDigitalVM
        {
            public ItemControleArquivoDigitalVM()
            {
                ArquivoDigital = new ArquivoDigital();
            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioItemControleGaleria { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioItemControle { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoDetalhada { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public ArquivoDigital ArquivoDigital { get; set; }
        }

    }
}