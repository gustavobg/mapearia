﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url)
            : base(url) { }

        public int? idAtividadeEconomicaPeriodo { get; set; }
    }
}