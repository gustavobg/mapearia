﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdQuestionarioMedidorItem { get; set; }
        public int? IdProducaoLocal { get; set; }

        public string Codigo { get; set; }
        public string IdQuestionarioMedidorItemIn { get; set; }
        public string IdQuestionarioMedidorItemNotIn { get; set; }
        public string IdQuestionarioCategoriaMedidorIn { get; set; }
        public string IdMapeamentoCamadaCategoriaIn { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }

    }
}