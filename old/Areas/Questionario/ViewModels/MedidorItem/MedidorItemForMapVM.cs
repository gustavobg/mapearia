﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem
{
    public class MedidorItemForMapVM
    {
        public MedidorItemForMapVM()
        {

        }

        public int IdQuestionarioMedidorItem { get; set; }

        public string Codigo { get; set; }

        public string Point { get; set; }

        public string DataInstalacao { get; set; }

        public string Cor { get; set; }
    }
}