﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.MedidorItem
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioMedidorItem { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoriaMedidor { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoLocal { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoUnidade { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataInstalacao { get; set; }

        [ViewModelToModelAttribute]
        public string Latitude { get; set; }

        [ViewModelToModelAttribute]
        public string Longitude { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        #region Propriedades Especiais

        [ViewModelToModelAttribute]
        public string DataInstalacaoExibicao { get; set; }

        #endregion

    }
}