﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Questao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstOpcao = new List<ListaOpcaoVM>();
            ArquivosDigitais = new List<ArquivoDigital>();
        }

        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public bool? QuestionarioPublicado { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersaoQuestao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? Sequencia { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoCampo { get; set; }

        [ViewModelToModelAttribute]
        public int? TamanhoCampo { get; set; }

        [ViewModelToModelAttribute]
        public string SubDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersaoTopico { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteNaoSeAplica { get; set; }

        [ViewModelToModelAttribute]
        public bool Obrigatorio { get; set; }

        [ViewModelToModelAttribute]
        public bool QuestaoMesmaLinha { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoJustificativa { get; set; }

        [ViewModelToModelAttribute]
        public bool? PermiteValorNulo { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteFotoQuestao { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ListaOpcaoVM> lstOpcao { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }

        public class ListaOpcaoVM
        {
            public ListaOpcaoVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestaoLista { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestao { get; set; }

            [ViewModelToModelAttribute]
            public int? Sequencia { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }
        }
    }
}