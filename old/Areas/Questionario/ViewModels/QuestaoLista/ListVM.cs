﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.QuestaoLista
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }


        public int? IdQuestionarioVersaoQuestaoLista { get; set; }

        public int? IdQuestionarioVersaoQuestao { get; set; }

        public string IdQuestionarioVersaoQuestaoListaIn { get; set; }
        public string Valor { get;set;}
    }
}