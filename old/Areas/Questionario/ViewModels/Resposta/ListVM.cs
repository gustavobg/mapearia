﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM() { }

        public int? IdQuestionarioResposta { get; set; }
        public int? IdQuestionarioVersao { get; set; }
        public int? IdAplicacaoReferencia { get; set; }
        public int? IdSubAplicacaoReferencia { get; set; }
        public int? IdPessoaLancamento { get; set; }
        public int? IdAtividadeEconomicaPeriodo { get; set; }
        public int? Situacao { get; set; }

        public string DataLancamentoInicio { get; set; }
        public string DataLancamentoFim { get; set; }
        public string Descricao { get; set; }

        public string IdMapeamentoCamadaCategoriaIn { get; set; } //Utilizado para o select de elementos

        public bool? Todos { get; set; }
        public bool? Ativo { get; set; }

    }
}