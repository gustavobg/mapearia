﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Resposta
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            TopicoQuestao = new List<QuestionarioVersaoTopicoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioResposta { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersao { get; set; }

        [ViewModelToModelAttribute]
        public string Codigo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaEmpresa { get; set; }

        //[ViewModelToModelAttribute]
        //public int? IdQuestionarioConfiguracao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaAplicador { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoaLancamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataLancamento { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? DataReferencia { get; set; }

        #region Referência

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public int? IdAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdSubAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdProducaoCultura { get; set; }

        [ViewModelToModelAttribute]
        public int? IdCategoriaServico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdDocumentoTipo { get; set; }

        #endregion

        [ViewModelToModelAttribute]
        public string Latitude { get; set; }

        [ViewModelToModelAttribute]
        public string Longitude { get; set; }

        [ViewModelToModelAttribute]
        public string Proprietario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioMedidorItem { get; set; }

        [ViewModelToModelAttribute]
        public int? Situacao { get; set; }

        #region Datas - Exibição

        [ViewModelToModelAttribute]
        public string DataLancamentoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string DataReferenciaExibicao { get; set; }

        #endregion

        #region Propriedades para Exibição de controls da view

        [ViewModelToModelAttribute]
        public int TipoAplicacaoQuestionario { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeEmpresaUsuaria { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeAtividadeEconomicaPeriodo { get; set; }

        [ViewModelToModelAttribute]
        public bool InformaHora { get; set; }

        [ViewModelToModelAttribute]
        public bool InformaPessoaAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteDataAplicacaoDiferenteDoLancamento { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeSubAplicacao { get; set; }

        #endregion

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<QuestionarioVersaoTopicoVM> TopicoQuestao { get; set; }

        public class QuestionarioVersaoTopicoVM
        {
            public QuestionarioVersaoTopicoVM()
            {
                lstQuestao = new List<QuestionarioVersaoQuestaoVM>();
            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoTopico { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute(ComplexType=true)]
            public List<QuestionarioVersaoQuestaoVM> lstQuestao { get; set; }

        }

        public class QuestionarioVersaoQuestaoVM
        {

            public QuestionarioVersaoQuestaoVM()
            {
                RespostaValor = new QuestionarioRespostaValorVM();
            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersao { get; set; }

            [ViewModelToModelAttribute]
            public int? Sequencia { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoCampo { get; set; }

            [ViewModelToModelAttribute]
            public int? TamanhoCampo { get; set; }

            [ViewModelToModelAttribute]
            public string SubDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoDetalhada { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoTopico { get; set; }

            [ViewModelToModelAttribute]
            public bool PermiteNaoSeAplica { get; set; }

            [ViewModelToModelAttribute]
            public bool Obrigatorio { get; set; }

            [ViewModelToModelAttribute]
            public bool QuestaoMesmaLinha { get; set; }

            [ViewModelToModelAttribute]
            public int? TipoJustificativa { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public Guid? ChaveVersao { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public QuestionarioRespostaValorVM RespostaValor { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public List<ListaOpcaoVM> lstOpcao { get; set; }

        }

        public class QuestionarioRespostaValorVM
        {
            [ViewModelToModelAttribute]
            public int? IdQuestionarioRespostaValor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestaoLista { get; set; }

            [ViewModelToModelAttribute]
            public bool? NaoSeAplica { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }

            [ViewModelToModelAttribute]
            public string Justificativa { get; set; }

            [ViewModelToModelAttribute]
            public string IdsQuestionarioVersaoQuestaoLista { get; set; }
        }

        public class ListaOpcaoVM
        {
            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestaoLista { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoQuestao { get; set; }

            [ViewModelToModelAttribute]
            public string Valor { get; set; }

            [ViewModelToModelAttribute]
            public bool Excluido { get; set; }
        }
    }
}