﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Topico
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }

        public string ParentId { get; set; }
        public string DescricaoExibicao { get; set; }

        public int? IdQuestionarioVersao { get; set; }
    }
}