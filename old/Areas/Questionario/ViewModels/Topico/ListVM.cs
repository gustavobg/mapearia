﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Topico
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }

        public int? IdQuestionarioVersaoTopico { get; set; }
        public int? IdQuestionarioVersao { get; set; }

        public string Descricao { get; set; }

        public bool? RegistroProprio { get; set; }
        public bool? Ativo { get; set; }
    }
}