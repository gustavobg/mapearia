﻿using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Topico
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            ArquivosDigitais = new List<ArquivoDigital>();
        }


        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersaoTopico { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersao { get; set; }

        [ViewModelToModelAttribute]
        public RepresentacaoTipoEnum? RepresentacaoTipo { get; set; }

        [ViewModelToModelAttribute]
        public int? Sequencia { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoriaMedidor { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioItemControle { get; set; }

        [ViewModelToModelAttribute]
        public string SubDescricao { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }

        [ViewModelToModelAttribute]
        public bool? PossuiAvaliacao { get; set; }

        [ViewModelToModelAttribute]
        public decimal? PesoAvaliacao { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteFotoTopico { get; set; }

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }

    }
}