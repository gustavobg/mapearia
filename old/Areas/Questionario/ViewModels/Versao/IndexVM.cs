﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Versao
{
    public class IndexVM : VMIndexBase
    {
        public IndexVM(string url) : base(url) { }

        public int ParentId { get; set; }
        public string DescricaoExibicao { get; set; }

    }
}