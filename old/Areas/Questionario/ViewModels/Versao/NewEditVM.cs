﻿using HTM.MasterGestor.Model.Questionario;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.ViewModels.ArquivoDigital;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Questionario.ViewModels.Versao
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            lstUnidadeAmostral = new List<QuestionarioUnidadeAmostralVM>();
            ArquivosDigitais = new List<ArquivoDigital>();
        }

        public string DescricaoExibicao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioVersao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioConfiguracao { get; set; }

        [ViewModelToModelAttribute]
        public int? IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public string Descricao { get; set; }

        [ViewModelToModelAttribute]
        public string Sigla { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? PeriodoInicio { get; set; }

        [ViewModelToModelAttribute]
        public DateTime? PeriodoTermino { get; set; }

        [ViewModelToModelAttribute]
        public string DescricaoDetalhada { get; set; }

        [ViewModelToModelAttribute]
        public string Observacao { get; set; }

        [ViewModelToModelAttribute]
        public int? FormaAplicacao { get; set; }

        [ViewModelToModelAttribute]
        public bool? RepresentaMedidor { get; set; }

        [ViewModelToModelAttribute]
        public int? IdQuestionarioCategoriaMedidor { get; set; }

        [ViewModelToModelAttribute]
        public int? CoordenadaGeograficaTipo { get; set; }

        [ViewModelToModelAttribute]
        public bool Publicado { get; set; }

        [ViewModelToModelAttribute]
        public bool ExibeTopicoComNumeracao { get;set;}

        [ViewModelToModelAttribute]
        public int? TipoCodificacao { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoVisualizacaoMobile { get; set; }

        [ViewModelToModelAttribute]
        public int? TipoEdicaoMobile { get; set; }

        [ViewModelToModelAttribute]
        public bool PermiteFotoVersao { get;set;}

        [ViewModelToModelAttribute]
        public bool? Ativo { get; set; }

        #region Datas 

        [ViewModelToModelAttribute]
        public string PeriodoInicioExibicao { get; set; }

        [ViewModelToModelAttribute]
        public string PeriodoTerminoExibicao { get; set; }

        #endregion

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<ArquivoDigital> ArquivosDigitais { get; set; }

        [ViewModelToModelAttribute(ComplexType = true)]
        public List<QuestionarioUnidadeAmostralVM> lstUnidadeAmostral { get; set; }

        public class QuestionarioTopicoVM
        {
            public QuestionarioTopicoVM()
            {
                ArquivoDigital = new ArquivoDigital();
            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoTopico { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersao { get; set; }

            [ViewModelToModelAttribute]
            public RepresentacaoTipoEnum? RepresentacaoTipo { get; set; }

            [ViewModelToModelAttribute]
            public int? Sequencia { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioCategoriaMedidor { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioItemControle { get; set; }

            [ViewModelToModelAttribute]
            public string SubDescricao { get; set; }

            [ViewModelToModelAttribute]
            public string DescricaoDetalhada { get; set; }

            [ViewModelToModelAttribute]
            public bool? PossuiAvaliacao { get; set; }

            [ViewModelToModelAttribute]
            public decimal? PesoAvaliacao { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public ArquivoDigital ArquivoDigital { get; set; }
        }
        public class QuestionarioUnidadeAmostralVM
        {
            public QuestionarioUnidadeAmostralVM()
            {

            }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioVersaoUnidadeAmostral { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestinarioVersao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdQuestionarioUnidadeAmostral { get; set; }

            [ViewModelToModelAttribute]
            public int? Sequencia { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string QuestionarioUnidadeAmostralDescricao { get; set; }
        }
    }
}