using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Core;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.GrupoUsuario;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class GrupoUsuarioController : ControllerExtended
    {
        public GrupoUsuarioController()
        {
            IndexUrl = "/Seguranca/GrupoUsuario/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        #region NewEdit
        [HttpPost, ValidateJsonAntiForgeryToken, CustomAuthorize]
        public ActionResult NewEdit(NewEditVM vm)
        {
            GrupoUsuarioInfo info = new GrupoUsuarioInfo();
            ViewModelToModelMapper.Map<GrupoUsuarioInfo>(vm, info);

            info.IdPessoaOperacao = IdPessoa;
            info.IdEmpresa = IdEmpresa;
            info.IdEmpresaLogada = IdEmpresa;

            // arvore de menu
            info.Menus = DesfazerArvore(vm.TreeSelecionados);

            var response = GrupoUsuarioBll.Instance.Salvar(info);
            if (info.Response.Sucesso)
                SalvarHistoricoAcesso(IndexUrl, vm.IdGrupoUsuario.HasValue ? TelaHistoricoAcessoTipoEnum.Edicao : TelaHistoricoAcessoTipoEnum.Inclusao, response.IdGrupoUsuario, "GrupoUsuario", response.Response.IdHistorico);

            return Json(response);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM();

            GrupoMenuInfo grupoMenuInfo = new GrupoMenuInfo();
            grupoMenuInfo = GrupoMenuBll.Instance.ListarPorIdCompleto(new GrupoMenuInfo { IdGrupoMenu = ControladorSessaoUsuario.SessaoCorrente.Empresa.IdGrupoMenu.Value });

            List<JsTree> arvoreDisponiveis = GerarArvoreGrupoMenu(grupoMenuInfo.Itens);

            if (id.HasValue && id > 0)
            {
                GrupoUsuarioInfo info = GrupoUsuarioBll.Instance.ListarPorIdCompleto(new GrupoUsuarioInfo { IdGrupoUsuario = id });
                ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);

                if (info.Menus.Count > 0)
                {
                    var favorito = info.Menus.Find(x => x.Descricao == "Favoritos");
                    if (favorito != null)
                        info.Menus.Remove(favorito);
                    vm.TreeSelecionados = GerarArvore(info.Menus);
                }
                else
                {
                    JsTree treeRoot = new JsTree();
                    treeRoot.text = "[Clique com o botão direito do mouse]";
                    treeRoot.icon = "fa fa-folder";
                    treeRoot.state = new NodeState(true, false, false);
                    treeRoot.type = "folder";
                    vm.TreeSelecionados.Add(treeRoot);
                }

                SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Visualizacao, info.IdGrupoUsuario, "GrupoUsuario");
            }
            else
            {
                // caso inclusão já lista menu todo como facilitador
                vm.TreeSelecionados = arvoreDisponiveis;
                vm.Ativo = true;
            }

            vm.TreeDisponiveis = arvoreDisponiveis;
            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM();
            return View(vm);
        }
        #endregion

        #region Listagens

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            GrupoUsuarioInfo info = new GrupoUsuarioInfo();

            info.IdEmpresa = IdEmpresa;
            info.Ativo = vm.Ativo;
            info.Descricao = vm.Descricao;
            info.IdGrupoUsuario = vm.IdGrupoUsuario;
            info.IdGrupoUsuarioIn = vm.IdsGrupoUsuario;
            info.Modelo = vm.Modelo;

            ListPaged<GrupoUsuarioInfo> retorno = new ListPaged<GrupoUsuarioInfo>(GrupoUsuarioBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Grupo de Usuario", true);
        }

        private List<JsTree> GerarArvoreGrupoMenu(List<GrupoMenuItemInfo> itens, GrupoMenuItemInfo atual = null)
        {
            List<JsTree> retorno = new List<JsTree>();
            List<GrupoMenuItemInfo> nivelCorrente;

            if (atual == null)
            {
                nivelCorrente = itens.Where(x => !x.IdGrupoMenuItemPai.HasValue).OrderBy(x => x.Ordem).ToList();
            }
            else
            {
                nivelCorrente = itens.Where(x => x.IdGrupoMenuItemPai == atual.IdGrupoMenuItem).OrderBy(x => x.Ordem).ToList();
            }

            foreach (GrupoMenuItemInfo grpItem in nivelCorrente)
            {
                JsTree treeItem = new JsTree();
                treeItem.data.id = grpItem.IdTela.HasValue ? grpItem.IdTela.Value.ToString() : grpItem.IdGrupoMenuItem.Value.ToString();
                //treeItem.data.reference = grpItem.IdTela.HasValue ? "tela" : "";
                treeItem.text = grpItem.IdTela.HasValue ? grpItem.Tela.TituloMenu : grpItem.Descricao;
                treeItem.type = grpItem.IdTela.HasValue ? "default" : "folder";
                treeItem.icon = grpItem.IdTela.HasValue ? "fa fa-file-o" : "fa fa-folder";
                treeItem.state = new NodeState(true, false, false);
                treeItem.children = new List<JsTree>();
                treeItem.children.AddRange(GerarArvoreGrupoMenu(itens, grpItem));

                retorno.Add(treeItem);
            }

            return retorno;
        }

        private List<GrupoMenuItemInfo> DesfazerArvoreGrupoMenu(List<JsTree> itens, int ordem = 0)
        {
            List<GrupoMenuItemInfo> retorno = new List<GrupoMenuItemInfo>();

            foreach (JsTree treeItem in itens)
            {
                ordem++;
                GrupoMenuItemInfo grpItem = new GrupoMenuItemInfo();

                if (treeItem.type == "default")
                {
                    grpItem.IdTela = int.Parse(treeItem.data.id);
                }
                else
                {
                    grpItem.Descricao = treeItem.text;
                }
                grpItem.Ordem = ordem;

                grpItem.Filhos.AddRange(DesfazerArvoreGrupoMenu(treeItem.children, ordem));
                retorno.Add(grpItem);
            }

            return retorno;
        }

        private List<JsTree> GerarArvore(List<GrupoUsuarioMenuInfo> itens, GrupoUsuarioMenuInfo atual = null)
        {
            List<JsTree> retorno = new List<JsTree>();
            List<GrupoUsuarioMenuInfo> nivelCorrente;

            if (atual == null)
            {
                nivelCorrente = itens.Where(x => !x.IdGrupoUsuarioMenuPai.HasValue).OrderBy(x => x.Ordem).ToList();
            }
            else
            {
                nivelCorrente = itens.Where(x => x.IdGrupoUsuarioMenuPai == atual.IdGrupoUsuarioMenu).OrderBy(x => x.Ordem).ToList();
            }

            foreach (GrupoUsuarioMenuInfo grpItem in nivelCorrente)
            {
                JsTree treeItem = new JsTree();


                treeItem.data.id = grpItem.IdTela.HasValue ? grpItem.IdTela.Value.ToString() : grpItem.IdGrupoUsuarioMenu.Value.ToString();
                //treeItem.data.reference = grpItem.IdTela.HasValue ? "tela" : "";
                treeItem.text = grpItem.IdTela.HasValue ? grpItem.TelaInfo.TituloMenu : grpItem.Descricao;
                treeItem.type = grpItem.IdTela.HasValue ? "default" : "folder";
                treeItem.icon = string.IsNullOrEmpty(grpItem.Icone) ? (grpItem.IdTela.HasValue ? "fa fa-file-o" : "fa fa-folder") : grpItem.Icone;
                treeItem.state = new NodeState(true, false, false);
                string _treeIcon = treeItem.icon;
                bool _isMaterial = treeItem.icon.Contains("material-icons");
                treeItem.li_attr = new LiAttr(_isMaterial ? _treeIcon.Replace("material-icons", "").Trim() : _treeIcon, _isMaterial);
                treeItem.children = new List<JsTree>();
                treeItem.children.AddRange(GerarArvore(itens, grpItem));
                retorno.Add(treeItem);
            }

            return retorno;
        }

        private List<GrupoUsuarioMenuInfo> DesfazerArvore(List<JsTree> itens, int ordem = 0)
        {
            List<GrupoUsuarioMenuInfo> retorno = new List<GrupoUsuarioMenuInfo>();

            foreach (JsTree treeItem in itens)
            {
                ordem++;
                GrupoUsuarioMenuInfo grpItem = new GrupoUsuarioMenuInfo();

                if (treeItem.type == "default")
                {
                    grpItem.IdTela = int.Parse(treeItem.data.id);
                }
                else
                {
                    grpItem.Descricao = treeItem.text;
                }

                grpItem.Icone = treeItem.icon == "fa fa-folder" || treeItem.icon == "fa fa-file-o" ? "" : treeItem.icon;
                grpItem.Ordem = ordem;

                grpItem.Filhos.AddRange(DesfazerArvore(treeItem.children, ordem));
                retorno.Add(grpItem);
            }

            return retorno;
        }

        #endregion
    }
}
