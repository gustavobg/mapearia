


using HTM.MasterGestor.Bll.Configuracao;
using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Configuracao;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.GrupoUsuarioMenu;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class GrupoUsuarioMenuController : ControllerExtended
    {

        public GrupoUsuarioMenuController()
        {
            IndexUrl = "/Configuracao/GrupoUsuario/Index";
        }

        //[JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        //[CustomAuthorize]
        //public ActionResult List(ListVM vm)
        //{
        //    GrupoUsuarioMenuInfo info = new GrupoUsuarioMenuInfo();
        //    info.IdTela = vm.IdTela;


        //    return base.CreateListResult(vm, retorno, "Exportação: GrupoUsuarioMenu");
        //}

        /// <summary>
        /// Método retorna o HTML do menu [Usuário logado]
        /// </summary>
        /// <returns>HTML do Menu</returns>
        [CustomAuthorize]
        public string RetornaMenuUsuarioLogado()
        {
            string Menu = string.Empty;

            try
            {
                var usuarioInfo = UsuarioBll.Instance.ListarPorCodigo(ControladorSessaoUsuario.SessaoCorrente.UsuarioPerfil.IdUsuario.Value);
                var grupoUsuario = GrupoUsuarioBll.Instance.ListarPorIdCompleto(new GrupoUsuarioInfo { IdGrupoUsuario = usuarioInfo.IdGrupoUsuario });

                List<FavoritoInfo> lstFavorito = new List<FavoritoInfo>();
                lstFavorito = FavoritoBll.Instance.ListarFavoritosPorUsuarioCompleto(usuarioInfo.IdUsuario.Value);

                foreach (var item in lstFavorito)
                    grupoUsuario.Menus.Add(new GrupoUsuarioMenuInfo { Descricao = "", Icone = "material-icons star", Ordem = 0, IdGrupoUsuario = usuarioInfo.IdGrupoUsuario, IdGrupoUsuarioMenu = 99999, IdGrupoUsuarioMenuPai = 1, IdTela = item.IdTela, TelaInfo = item.Tela });

                ControladorSessaoUsuario.SessaoCorrente.GrupoUsuario = grupoUsuario;

                Menu = ControladorSessaoUsuario.SessaoCorrente.Menu(null);
            }
            catch (System.Exception)
            {
                return string.Empty;
            }

            return Menu;
        }

    }
}
