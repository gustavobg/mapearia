﻿using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.RestricaoAcesso;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class RestricaoAcessoController : ControllerExtended
    {
        public RestricaoAcessoController()
        {
            IndexUrl = "/Seguranca/RestricaoAcesso/Index";
        }

        [HttpPost, CustomAuthorize]
        public ActionResult NewEditJson(int? id)
        {
            NewEditVM vm = new NewEditVM("/Seguranca/RestricaoAcesso/NewEdit");
            vm.IdGrupoUsuario = id;

            RestricaoAcaoInfo info = new RestricaoAcaoInfo();

            //Retorna todas as ações / Restrições
            info.Restricoes = RestricaoAcaoBll.Instance.ListarRestricoesPorGrupo(id.Value);

            //Retorna todos os Objetos Restritos para do Grupo.
            info.IdsObjetos = string.Join(",", RestricaoGrupoUsuarioObjetoBll.Instance.ListarPorParametros(new RestricaoGrupoUsuarioObjetoInfo { IdGrupoUsuario = id.Value }).Select(x => x.IdObjeto));

            ViewModelToModelMapper.MapBack<NewEditVM>(vm, info);
            return Json(vm);
        }

        [HttpGet, CustomAuthorize]
        public ActionResult NewEdit(int? id)
        {
            NewEditVM vm = new NewEditVM("/Seguranca/RestricaoAcesso/NewEdit");
            return View(vm);
        }

        [HttpPost, CustomAuthorize]
        public ActionResult SalvarConfiguracao(string json)
        {
            RestricaoAcaoInfo info = new RestricaoAcaoInfo();
            List<RestricaoGrupoUsuarioInfo> lstRestricoesGrupoUsuario = new List<RestricaoGrupoUsuarioInfo>();
            List<int> lstObjetos = new List<int>();

            NewEditVM vm = new NewEditVM("/Seguranca/RestricaoAcesso/NewEdit");
            vm = JsonConvert.DeserializeObject<NewEditVM>(json);

            ViewModelToModelMapper.Map<RestricaoAcaoInfo>(vm, info);

            //Recuperar as Telas e Ações
            foreach (var item in info.Restricoes)
            {
                List<int> lstTela = new List<int>();
                CommaSeparatedToList(item.IdsTelas).ForEach(idTela =>
                    {
                        lstTela.Add(idTela);
                    });

                foreach (var telaItem in lstTela)
                {
                    RestricaoGrupoUsuarioInfo restricaoGrupoUsuarioInfo = new RestricaoGrupoUsuarioInfo();
                    restricaoGrupoUsuarioInfo.IdGrupoUsuario = vm.IdGrupoUsuario;
                    restricaoGrupoUsuarioInfo.IdRestricaoAcao = item.IdRestricaoAcao;
                    restricaoGrupoUsuarioInfo.IdEmpresa = IdEmpresa;
                    restricaoGrupoUsuarioInfo.IdTela = telaItem;

                    restricaoGrupoUsuarioInfo.IdEmpresaLogada = IdEmpresa;
                    restricaoGrupoUsuarioInfo.IdPessoaOperacao = IdPessoa;
                    lstRestricoesGrupoUsuario.Add(restricaoGrupoUsuarioInfo);
                }
            }

            //Recuperar os Objetos restritos
            CommaSeparatedToList(vm.IdsObjetos).ForEach(idObjeto =>
                {
                    lstObjetos.Add(idObjeto);
                });


            var response = RestricaoGrupoUsuarioBll.Instance.Salvar(lstRestricoesGrupoUsuario, lstObjetos, vm.IdGrupoUsuario.Value, ControladorSessaoUsuario.SessaoCorrente.Empresa.IdEmpresa.Value, ControladorSessaoUsuario.SessaoCorrente.Pessoa.IdPessoa.Value);

            return Json(response);
        }

        [HttpPost]
        public JsonResult RetornaRestricoesPorUrl(string url)
        {
            var restricoes = RestricaoGrupoUsuarioBll.Instance.ListarPorParametros(new RestricaoGrupoUsuarioInfo { Url = url, IdGrupoUsuario = ControladorSessaoUsuario.SessaoCorrente.GrupoUsuario.IdGrupoUsuario });

            return Json(JsonConvert.SerializeObject(restricoes));
        }

    }
}
