using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.TelaHistoricoAcesso;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTM.MasterGestor.Model.Core;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class TelaHistoricoAcessoController : ControllerExtended
    {
        public TelaHistoricoAcessoController()
        {
            IndexUrl = "/Seguranca/TelaHistoricoAcesso/Index";
        }

        [CustomAuthorize, ExceptionFilter]
        public ActionResult Index()
        {
            IndexVM vm = new IndexVM(IndexUrl);
            SalvarHistoricoAcesso(IndexUrl, TelaHistoricoAcessoTipoEnum.Listagem);
            return View(vm);
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            //DTOAdquirirTelaHistoricoAcesso dto = new DTOAdquirirTelaHistoricoAcesso();
            Model.Core.TelaHistoricoAcessoInfo info = new Model.Core.TelaHistoricoAcessoInfo();
            info.IdTelaHistoricoAcessoTipoIn = vm.IdTelaHistoricoAcessoTipoIn;
            info.DescricaoReferencia = vm.DescricaoReferencia;
            //dto.DescricaoReferencia = vm.DescricaoReferencia;
            //dto.IdTelaHistoricoAcessoTipoIn = vm.IdTelaHistoricoAcessoTipoIn;

            if (!string.IsNullOrEmpty(vm.DataInicio) && !string.IsNullOrEmpty(vm.DataFim))
            {
                info.DataInicio = DateTime.Parse(vm.DataInicio);
                info.DataFim = DateTime.Parse(vm.DataFim);

                //DateTime dtInicio = DateTime.Parse(vm.DataInicio);
                //DateTime dtFim = DateTime.Parse(vm.DataFim).AddDays(1).AddMilliseconds(-1);
                //TimeSpan timeSpan = TimeSpan.FromMinutes(vm.TimeOffset);
                //dto.DataMenorIgual = new DateTimeOffset(dtFim, timeSpan);
                //dto.DataMaiorIgual = new DateTimeOffset(dtInicio, timeSpan);
            }

            #region Conversão

            info.CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value;
            info.PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value;
            info.GrupoBy = vm.Page.GroupBy;
            info.OrderBy = PagingHelper.RemoveAscDesc(vm.Page.OrderBy).ToUpper();
            info.OrderByDir = PagingHelper.GetAscDesc(vm.Page.OrderBy).ToUpper();

            #endregion

            ListPaged<Model.Core.TelaHistoricoAcessoInfo> retorno = new ListPaged<Model.Core.TelaHistoricoAcessoInfo>(HTM.MasterGestor.Bll.Core.TelaHistoricoAcessoBll.Instance.ListarPorParametros(info)) { };

            return base.CreateListResultByStoredProcedure(vm, retorno, "Exportação: Pessoa");
        }

    }
}
