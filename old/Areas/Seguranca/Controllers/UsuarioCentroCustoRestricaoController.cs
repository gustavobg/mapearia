﻿using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class UsuarioCentroCustoRestricaoController : ControllerExtended
    {
        
        public UsuarioCentroCustoRestricaoController()
        {
            IndexUrl = "Corporativo/Pessoa/Index";
        }

        public JsonResult ListConcatenado(int IdPlanoContasVersao)
        {
            UsuarioCentroCustoRestricaoInfo info = new UsuarioCentroCustoRestricaoInfo();
            info.IdPlanoContasVersao = IdPlanoContasVersao;

            var lst = UsuarioCentroCustoRestricaoBll.Instance.ListarPorParametros(info);

            info.IdsCentroCusto = string.Join(",", lst.Select(x => x.IdCentroCusto));

            return Json(info);
        }

    }
}
