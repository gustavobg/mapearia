


using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Library.Web;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Bll.Seguranca;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class UsuarioController : ControllerExtended
    {
        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UsuarioInfo info = new UsuarioInfo();
            info.IdEmpresa = IdEmpresa;
            info.IdUsuario = vm.IdUsuario;
            info.IdPessoa = vm.IdPessoa;
            info.Login = vm.Login;
            info.IdUsuarioIn = vm.IdUsuarioIn;
            info.IdPessoaIn = vm.IdPessoaIn;
            info.IdPessoaNotIn = vm.IdPessoaNotIn;
            info.Ativo = vm.Ativo;
            info.Nomes = vm.Nomes;

            ListPaged<UsuarioInfo> retorno = new ListPaged<UsuarioInfo>(UsuarioBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Usuário", true);
        }


        //[CustomAuthorize]
        //public JsonResult ValidarDuplicado(string login, int? idPessoa)
        //{
        //    DTOResponseBase response = new DTOResponseBase();

        //    // adquire usuario de acordo com login/idempresa
        //    Usuario usuario = UsuarioBll.Instance.AdquirirUsuario(new DTOAdquirirUsuario() { IdEmpresa = IdEmpresa, Login = login }).FirstOrDefault();

        //    if (usuario == null)
        //    {
        //        response.Sucesso = true;
        //    }
        //    else if (usuario.IdPessoa != idPessoa)
        //    {
        //        response.AdicionarMensagem("Usuário já existente.");
        //        response.Sucesso = false;
        //    }
        //    else
        //    {
        //        response.Sucesso = true;
        //    }

        //    return new JsonResult()
        //    {
        //        Data = response,
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //    };
        //}

        [CustomAuthorize]
        public JsonResult AlterarSenhaUsuario(string novaSenha, int idUsuario)
        {
            bool sucesso = false;
            if (idUsuario > 0)
            {
                var usuarioInfo = UsuarioBll.Instance.ListarPorCodigo(idUsuario);

                if (usuarioInfo != null)
                {
                    usuarioInfo.Senha = novaSenha;
                    usuarioInfo.SenhaAlteradaSistema = true;
                    usuarioInfo.IdEmpresaLogada = IdEmpresa;
                    usuarioInfo.IdPessoaOperacao = IdPessoa;
                    sucesso = UsuarioBll.Instance.Salvar(usuarioInfo).Response.Sucesso;
                }
                else
                    sucesso = false;
            }
            else
                sucesso = false;

            return Json(new { Sucesso = sucesso });
        }
    }
}
