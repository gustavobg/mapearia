﻿using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class UsuarioFinanceiroClassificacaoRestricaoController : ControllerExtended
    {
        
        public UsuarioFinanceiroClassificacaoRestricaoController()
        {
            IndexUrl = "Corporativo/Pessoa/Index";
        }

        public JsonResult ListConcatenado(int IdPlanoContasVersao)
        {
            UsuarioFinanceiroClassificacaoRestricaoInfo info = new UsuarioFinanceiroClassificacaoRestricaoInfo();
            info.IdPlanoContasVersao = IdPlanoContasVersao;

            var lst = UsuarioFinanceiroClassificacaoRestricaoBll.Instance.ListarPorParametros(info);

            info.IdsClassificacoes = string.Join(",", lst.Select(x => x.IdFinanceiroClassificacao));

            return Json(info);
        }

    }
}
