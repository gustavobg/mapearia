﻿using HTM.MasterGestor.Bll.Seguranca;
using HTM.MasterGestor.Library.Collections;
using HTM.MasterGestor.Model.Seguranca;
using HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.UsuarioPlanoContasRestricao;
using HTM.MasterGestor.Web.UI.Base;
using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.Controllers
{
    public class UsuarioPlanoContasRestricaoController : ControllerExtended
    {
        
        public UsuarioPlanoContasRestricaoController()
        {
            IndexUrl = "Seguranca/UsuarioPlanoContasRestricao/Usuario";
        }

        [JsonFilter(Param = "vm", JsonDataType = typeof(ListVM))]
        [CustomAuthorize]
        public ActionResult List(ListVM vm)
        {
            UsuarioPlanoContasRestricaoInfo info = new UsuarioPlanoContasRestricaoInfo();
            info.IdPlanoContasVersao = vm.IdPlanoContasVersao;

            ListPaged<UsuarioPlanoContasRestricaoInfo> retorno = new ListPaged<UsuarioPlanoContasRestricaoInfo>(UsuarioPlanoContasRestricaoBll.Instance.ListarPorParametros(info))
            {
                PageInfo = new PagingSettings
                {
                    CurrentPage = vm.Page.CurrentPage == null ? 1 : vm.Page.CurrentPage.Value,
                    PageSize = vm.Page.PageSize == null ? 10 : vm.Page.PageSize.Value,
                    GroupBy = vm.Page.GroupBy,
                    OrderBy = vm.Page.OrderBy
                }
            };

            return base.CreateListResult(vm, retorno, "Exportação: Restrição de Plano de Contas ", true);
        }

        public JsonResult ListConcatenado(int IdPlanoContasVersao)
        {
            UsuarioPlanoContasRestricaoInfo info = new UsuarioPlanoContasRestricaoInfo();
            UsuarioPlanoContasRestricaoInfo x = new UsuarioPlanoContasRestricaoInfo();

            info.IdPlanoContasVersao = IdPlanoContasVersao;

            var retorno = UsuarioPlanoContasRestricaoBll.Instance.ListarPorParametros(info);
            
            x.IdsPlanosContas = string.Join(",", retorno.Select(y => y.IdPlanoContas));

            return Json(x);
        }

    }
}
