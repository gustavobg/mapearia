using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.GrupoUsuario
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() 
        { 
        }

        public int? IdGrupoUsuario { get; set; }
        public string IdsGrupoUsuario { get; set; }
        public string Descricao { get; set; }
        public bool? Ativo { get; set; }
        public bool? Modelo { get; set; }
    }
}