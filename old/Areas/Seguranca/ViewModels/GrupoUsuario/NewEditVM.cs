using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;
using HTM.MasterGestor.Web.UI.Infrastructure.Javascript;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.GrupoUsuario
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            TreeSelecionados = new List<JsTree>();
        }

        [ViewModelToModelAttribute]
        public Int32? IdGrupoUsuario { get; set; }

        [ViewModelToModelAttribute]
        public Int32? IdGrupoMenu { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdEmpresa { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Ativo { get; set; }

        [ViewModelToModelAttribute]
        public Guid? ChaveVersao { get; set; }

        [ViewModelToModelAttribute]
        public Boolean Modelo { get; set; }

        public List<JsTree> TreeSelecionados { get; set; }
        public List<JsTree> TreeDisponiveis { get; set; }
    }
}