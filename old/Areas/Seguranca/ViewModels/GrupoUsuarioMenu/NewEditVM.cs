using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Web.UI.Infrastructure;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.GrupoUsuarioMenu
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM() { }

        [ViewModelToModelAttribute]
        public Int32? IdGrupoUsuarioMenu { get; set; }

        [ViewModelToModelAttribute]
        public String Descricao { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdTela { get; set; }

        [ViewModelToModelAttribute]
        public String Icone { get; set; }

        [ViewModelToModelAttribute]
        public Int32 IdGrupoUsuarioMenuPai { get; set; }

        [ViewModelToModelAttribute]
        public Int32 Ordem { get; set; }


    }
}