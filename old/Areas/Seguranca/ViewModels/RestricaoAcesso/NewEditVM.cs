﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.RestricaoAcesso
{
    public class NewEditVM : VMIndexBase
    {
        public NewEditVM(string url)
            : base(url)
        {
            Restricoes = new List<RestricaoVM>();
        }

        [ViewModelToModelAttribute]
        public int? IdGrupoUsuario { get; set; }

        [ViewModelToModelAttribute]
        public string IdsObjetos { get; set; }

        [ViewModelToModelAttribute(ComplexType=true)]
        public List<RestricaoVM> Restricoes { get; set; }

        public class RestricaoVM
        {
            public List<NewEditVM> Restricoes { get; set; }

            [ViewModelToModelAttribute]
            public int? IdRestricaoAcao { get; set; }

            [ViewModelToModelAttribute]
            public string Descricao { get; set; }

            [ViewModelToModelAttribute]
            public int? Ordem { get; set; }

            [ViewModelToModelAttribute]
            public bool? Ativo { get; set; }

            [ViewModelToModelAttribute]
            public string IdsTelas { get; set; }

            [ViewModelToModelAttribute(ComplexType = true)]
            public List<RestricaoGrupoUsuarioVM> RestricoesGrupoUsuario { get; set; }

        }

        public class RestricaoGrupoUsuarioVM
        {
            [ViewModelToModelAttribute]
            public int? IdRestricaoGrupoUsuario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdEmpresa { get; set; }

            [ViewModelToModelAttribute]
            public int? IdGrupoUsuario { get; set; }

            [ViewModelToModelAttribute]
            public int? IdRestricaoAcao { get; set; }

            [ViewModelToModelAttribute]
            public int? IdTela { get; set; }
        }

        
    }
}