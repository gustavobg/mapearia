using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.TelaHistoricoAcesso
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() {
            //IdTelaHistoricoAcessoTipoIn = new List<int>();
        }

        public string DescricaoReferencia { get; set; }
        //public List<int> IdTelaHistoricoAcessoTipoIn { get; set; }
        public string IdTelaHistoricoAcessoTipoIn { get; set; }
        
        // campos de data sao tratados como string
        public string DataInicio { get; set; }
        public string DataFim { get; set; }

        // offset do timezone para conversao UTC/GMT
        public int TimeOffset { get; set; }
    }
}