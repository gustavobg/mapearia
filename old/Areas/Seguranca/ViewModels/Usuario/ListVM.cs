using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HTM.MasterGestor.Library.DataAccess;
using HTM.MasterGestor.Library.Log;
using HTM.MasterGestor.Library.Data.Web;
using HTM.MasterGestor.Library.Web.MVC.ViewModel;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.Usuario
{
    public class ListVM : ViewModelListRequestBase
    {
	    public ListVM() { }

        public int? IdUsuario { get; set; }
        public int? IdPessoa { get; set; }

        public string IdUsuarioIn { get; set; }
        public string IdPessoaIn { get;set;}
        public string IdPessoaNotIn { get; set; }

        public string Login { get; set; }
        public string Nomes { get; set; }

        public bool? Ativo { get; set; }

    }
}