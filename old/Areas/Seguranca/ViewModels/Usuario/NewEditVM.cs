﻿using HTM.MasterGestor.Web.UI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.Usuario
{
    public class NewEditVM : VMNewEditBase
    {
        public NewEditVM()
        {
            UsuarioNuncaAcessou = true;
        }

        [ViewModelToModelAttribute]
        public int? IdUsuario { get; set; }

        [ViewModelToModelAttribute]
        public int? IdPessoa { get; set; }

        [ViewModelToModelAttribute]
        public string Login { get; set; }

        [ViewModelToModelAttribute]
        public string Email { get; set; }

        [ViewModelToModelAttribute]
        public bool Ativo { get; set; }

        [ViewModelToModelAttribute]
        public int? IdGrupoUsuario { get; set; }

        [ViewModelToModelAttribute]
        public bool UsuarioNuncaAcessou { get; set; }

        [ViewModelToModelAttribute]
        public bool BotaoAtivacao { get; set; }
    }
}