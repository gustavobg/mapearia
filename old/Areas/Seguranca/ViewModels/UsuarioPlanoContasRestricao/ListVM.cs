﻿using HTM.MasterGestor.Library.Web.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.MasterGestor.Web.UI.Areas.Seguranca.ViewModels.UsuarioPlanoContasRestricao
{
    public class ListVM : ViewModelListRequestBase
    {
        public ListVM()
        {

        }
        
        public int? IdUsuario {get;set;}
        public int? IdPlanoContasVersao { get; set; }
    }
}