﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (ambienteProducao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            ambienteProducao.config({
                saveUrl: '/Agricola/AmbienteProducao/NewEdit',
                getUrl: '/Agricola/AmbienteProducao/NewEditJson',
            });
            var request = ambienteProducao.get({ id: id }),
                form = $('#form-AmbienteProducao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    ambienteProducao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Ambiente de Produção salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});