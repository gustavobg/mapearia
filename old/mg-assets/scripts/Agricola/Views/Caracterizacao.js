﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (caracterizacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            caracterizacao.config({
                saveUrl: '/Agricola/Caracterizacao/NewEdit',
                getUrl: '/Agricola/Caracterizacao/NewEditJson',
            });
            var request = caracterizacao.get({ id: id }),
                form = $('#form-ProducaoCaracterizao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    caracterizacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Caracterização salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});