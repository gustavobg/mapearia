﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (categoriaArea, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            categoriaArea.config({
                saveUrl: '/Agricola/CategoriaArea/NewEdit',
                getUrl: '/Agricola/CategoriaArea/NewEditJson',
            });
            var request = categoriaArea.get({ id: id }),
                form = $('#form-ProducaoAreaCategoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    categoriaArea.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Categoria de Área salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});