﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (configuracao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            configuracao.config({
                saveUrl: '/Agricola/ConfiguracaoEstimativaCultura/NewEdit',
                getUrl: '/Agricola/ConfiguracaoEstimativaCultura/NewEditJson',
            });
            var request = configuracao.get({ parentId: Route.routeOptions.parentId, id: id }),
                 form = $('#form-ConfiguracaoEstimativaCultura-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                var ExibeEstimativaProducao = vm.ExibeEstimativaProducao();

                //Regras Humberto fora do Mockup 13/01/2015
                vm.DadosComplementaresProduto = ko.observable({ 'Ativo': true, 'IdProdutoServicoIn': vm.IdsProdutoServico(), 'IdProdutoServicoNotIn': vm.IdsProdutoServicoNotIn() });
                vm.DadosComplementaresEspacamento = ko.observable({ 'Ativo': true, 'IdProducaoPlantioEspacamentoIn': vm.IdsEspacamentosCultura(), 'IdProducaoPlantioEspacamentoNotIn': vm.IdsEspacamentosNotIn() });
                vm.DadosComplementaresEpocaPlantio = ko.observable({ 'Ativo': true, 'IdProducaoEpocaPlantioIn': vm.IdsEpocaPlantio(), 'IdProducaoEpocaPlantioNotIn': vm.IdsEpocaPlantioNotIn() });

                vm.DesabilitaProduto = ko.observable(vm.IdsProdutoServico().length > 0 ? false : true);
                vm.DesabilitaEspacamento = ko.observable(vm.IdsEspacamentosCultura().length > 0 ? false : true);
                vm.DesabilitaEpocaPlantio = ko.observable(vm.IdsEpocaPlantio().length > 0 ? false : true);


                var ItemExtend = function (vm) {
                    ko.computed(function () {

                        if (ExibeEstimativaProducao == false)
                        {
                            vm.Colheita(1);
                        }

                        if (vm.ProducaoValor() != null)
                        {
                            if (ko.isObservable(vm.ProducaoValor)) {
                                vm.DescricaoEstimativaComposta(vm.ProducaoValor());
                            }
                            else
                            {
                                vm.DescricaoEstimativaComposta(vm.ProducaoValor);
                            }
                        }
                        else if (vm.PerdaValor() != null)
                        {
                            if (ko.isObservable(vm.PerdaValor)) {
                                vm.DescricaoEstimativaComposta(vm.PerdaValor());
                            }
                            else {
                                vm.DescricaoEstimativaComposta(vm.ProducaoValor);
                            }
                        }
                            
                    }, this);
                }
                window.ItemExtend = ItemExtend;

                vm.Save = function () {
                    vm.IdProducaoCultura(Route.routeOptions.parentId);
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        configuracao.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Configuração salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }

});