﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (cultura, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            cultura.config({
                saveUrl: '/Agricola/Cultura/NewEdit',
                getUrl: '/Agricola/Cultura/NewEditJson',
            });
            var request = cultura.get({ id: id }),
                 form = $('#form-ProducaoCultura-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.dadosComplProduto = ko.observable();
                vm.ProducaoCulturaTipoVM = ko.observable();
                vm.ExibeCamposPorTipoCultura = ko.observable(true);

                ko.computed(function () {
                    if (vm.IdAtividadeEconomica() != null)
                        vm.dadosComplProduto({ 'Ativo': true, 'PermiteProducao': true, 'IdAtividadeEconomica': vm.IdAtividadeEconomica(), 'Natureza': 1 });
                });


                //Caso Tipo de Cultura, validação [ Quando SemDefinição == false, ocultar todos os campos exceto: Atividade, Centro de Custo e Ativo  ]
                ko.computed(function () {
                    debugger;
                    if(vm.ProducaoCulturaTipoVM() != null && (vm.ProducaoCulturaTipoVM().SemDefinicao != null && vm.ProducaoCulturaTipoVM().SemDefinicao == true))
                    {
                        debugger;
                        vm.ExibeCamposPorTipoCultura(false);
                        //Propriedades
                        vm.PlantioObrigatorio(false);
                        vm.PermiteVariasColheitas(false);
                        vm.ColheitasPossiveis(null);
                        vm.AvaliacaoProdutividadeTipo(null);
                        vm.EstimativaTipo(null);
                        vm.IdsObjetivos(null);
                        vm.IdsEspacamentos(null);
                        vm.IdsProdutos(null);

                        //Listas
                        vm.VariedadesTipo(null);
                        vm.CulturasEpocas(null);
                    }
                });


                vm.Save = function () {
                    var data = ko.toJSON(vm);
                    if (vm.isValid()) {
                        cultura.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Cultura salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                vmParams = function () {
                    // Parâmetros para select2
                    this.IdUnidadeInSelect = '72,73,74,75';
                    // txtIdsEpocas
                    this.IdProducaoEpocaPlantioNotInSelect = ko.computed(function () {
                        var ids = ko.utils.arrayMap(vm.CulturasEpocas(), function (item) {
                            return item.IdProducaoEpocaPlantio;
                        });
                        return ids.join(',');
                    });
                };

                var vmParams = new vmParams();
                window.vmParams = vmParams;

                ko.computed(function () {
                    var permiteVariasColheitas = vm.PermiteVariasColheitas();
                    if (!permiteVariasColheitas) {
                        vm.ColheitasPossiveis(1);
                    }
                });

                ko.applyBindingsToNode(document.getElementById('pnlCulturaEpocaPlantio'), { collapsible: { vmArray: [vm.CulturasEpocas()] } });
                ko.applyBindingsToNode(document.getElementById('pnlObjetivosProducao'), { collapsible: { vmArray: [vm.IdsObjetivos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlEspacamentos'), { collapsible: { vmArray: [vm.IdsEspacamentos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlVariedadesTipo'), { collapsible: { vmArray: [vm.VariedadesTipo()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProdutos'), { collapsible: { vmArray: [vm.IdsProdutos()] } });

                ko.computed(function () {
                    if (vm.AvaliacaoProdutividadeTipo() == "1" || vm.AvaliacaoProdutividadeTipo() == "2" || vm.AvaliacaoProdutividadeTipo() == null) {
                        $("#ddlEstimativaTipo").val(null);
                        vm.EstimativaTipo(null);
                    }
                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }
});
