﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (culturaTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            culturaTipo.config({
                saveUrl: '/Agricola/CulturaTipo/NewEdit',
                getUrl: '/Agricola/CulturaTipo/NewEditJson',
            });
            var request = culturaTipo.get({ id: id }),
                form = $('#form-ProdutoCulturaTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.SemDefinicao() == false)
                        vm.PermiteVariasColheitas(false);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    culturaTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Cultura salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});