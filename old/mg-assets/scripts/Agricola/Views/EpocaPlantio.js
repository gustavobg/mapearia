﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-collapsible', 'datetimepicker', 'jquery-inputmask'], function (epocaPlantio, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            epocaPlantio.config({
                saveUrl: '/Agricola/EpocaPlantio/NewEdit',
                getUrl: '/Agricola/EpocaPlantio/NewEditJson'
            });

            var request = epocaPlantio.get({ id: id }),
                form = $('#form-ProducaoEpocaPlantio-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Agricola/EpocaPlantio/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Época de Plantio salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});