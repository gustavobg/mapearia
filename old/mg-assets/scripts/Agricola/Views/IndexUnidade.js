﻿define(['crud-controller', 'knockout', 'bootstrap/modal'], function (unidade, ko, modal) {
    var vm = {},
        bind = function () {

            var modalDetalhes = $('#modal-Detalhes').modal('hide');

            unidade.config({                
                getUrl: '/Agricola/Unidade/NewEditJson',
                saveUrl: ''
            });
            vm.unidadeDetalhe = ko.observable();
            vm.getUnidadeDetalhes = function (data, event) {
                event.preventDefault();
                var request = unidade.get({ id: data.IdProducaoUnidade });
                request.done(function (response) {
                    vm.unidadeDetalhe(ko.mapping.fromJS(response));
                    modalDetalhes.modal('show');
                });
            };

            
            
            window.vmIndexAgricolaUnidade = vm;

            ko.applyBindings(vm, document.getElementById('IndexAgricolaUnidade'));

        };

    return {
        bind: bind
    }

});