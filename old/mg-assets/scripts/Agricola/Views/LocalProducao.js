﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-collapsible', 'gridview', 'unidade-tipo', 'partial-pessoa', 'jquery-inputmask', 'mask-coordenada', 'panels', 'component.colorSelector'], function (local, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            local.config({
                saveUrl: '/Agricola/LocalProducao/NewEdit',
                getUrl: '/Agricola/LocalProducao/NewEditJson'
            });

            var request = local.get({ id: id }),
                form = $('#form-ProducaoLocal-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.Descricao() != null && (vm.Sigla() == null || vm.Sigla() == '')) {
                        var str = '';
                        var items = vm.Descricao().split(' ');

                        $.each(items, function (index, value) {
                            var x = value.substring(0, 1);
                            str += value.substring(0, 1);
                        });
                        vm.Sigla(str);
                    }
                });



                function SelecionaAreas() {
                    //debugger;
                    var result = "";
                    var i = 0;
                    var total = vm.Areas().length;
                    ko.utils.arrayForEach(vm.Areas(), function (item) {
                        i += 1;
                        if (total > i) {
                            {
                                //debugger;
                                if (ko.isObservable(item.IdProducaoAreaCategoria)) {
                                    result += item.IdProducaoAreaCategoria() + ", ";
                                }
                                else
                                    result += item.IdProducaoAreaCategoria + ", ";
                            }
                        }
                        else {
                            //debugger;
                            if (ko.isObservable(item.IdProducaoAreaCategoria)) {
                                result += item.IdProducaoAreaCategoria() + "";
                            }
                            else
                                result += item.IdProducaoAreaCategoria + "";

                        }
                    });
                    vm.IdsAreasNotIn(result);
                };

                ko.applyBindings({}, document.getElementById('panels-local-bind'));

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    local.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Local de Produção salvo com Sucesso.');
                    });
                };

                var Pessoa = {
                    NomeSecundario: ko.observable('')
                };

                var vmProducaoAreaCategoria = {
                    Descricao: ko.observable('')
                }


                ko.computed(function () {
                    if (vm.Areas().length > 0) {
                        SelecionaAreas();
                    }
                })

                ko.bindingHandlers.descricaoEquals = {
                    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                        var $element = $(element),
                            value = allBindings().value,
                            $html = $('<div id="popover-mes" class="panel panel-warning">' +
                                          '<div class="panel-body">' +
                                                'Já existe um local de produção com este mesmo nome, confirme se este local já foi cadastrado voltando na tela de pesquisa da tela anterior.' +
                                          '</div>' +
                                          '<div class="panel-footer">' +
                                          '</div>' +
                                      '</div>'),
                            $footer = $html.find('.panel-footer'),
                            $action = $('<button class="btn-primary btn btn-action">Corrigir</button> ').on('click', function () { action(value, element); }).appendTo($footer),
                            $ignore = $('<button class="btn-default btn btn-ignore">Ignorar</button> ').on('click', function () { ignore(value, element); }).appendTo($footer),
                            options = {
                                style: { classes: 'qtip-panel qtip-popover qtip-shadow' },
                                position: { my: 'bottom left', at: 'top right' },
                                content: { text: $html },
                                show: { event: 'focus', solo: true }
                            };

                        $element.qtip($.extend(defaults.qtip.popover, options));

                        function ignore(value, element) {
                            var $element = $(element),
                                api = $element.qtip('api');

                            api.toggle(false);
                        }
                        function action(value, element) {
                            var api = $(element).qtip('api');
                            api.toggle(false);
                            value('');
                            element.focus();
                        }
                    },
                    update: function (element, valueAccessor, allBindings) {
                        debugger;
                        var value = allBindings().value,
                            $element = $(element),
                            $formGroup = $element.closest('.form-group'),
                            api = $element.qtip('api'),
                            isInitial = ko.computedContext.isInitial();

                        api.disable(true);

                        var isEquals = true;
                        debugger;
                        if (value() != null && value() != '') {

                            $.ajax({
                                url: '/Agricola/LocalProducao/VerificaDescricaoLocalPorEmpresa',
                                type: 'POST',
                                data: 'descricao=' + value() + '&idProducaoLocal=' + vm.IdProducaoLocal(),
                                dataType: 'json',
                                async: false,
                                success: function (data) {
                                    debugger;
                                    isEquals = data.data;
                                }
                            });

                            if (!isInitial && isEquals) {
                                api.disable(false);
                                api.toggle(true);
                                $formGroup.addClass('has-warning');
                            }
                        } else {
                            api.toggle(false);
                            api.disable(true);
                            $formGroup.removeClass('has-warning');
                        }

                    }
                }

                var vmParams = function () {
                    this.ProducaoLocalTipo = ko.observable();
                    this.IdUnidade = ko.pureComputed(function () {
                        var producaoLocalTipo = this.ProducaoLocalTipo();
                        if (producaoLocalTipo && producaoLocalTipo.hasOwnProperty('IdUnidade'))
                            return producaoLocalTipo.IdUnidade;
                    }, this);
                };

                vmParams = new vmParams();
                window.vmParams = vmParams;

                window.Pessoa = Pessoa;
                window.vm = vm;

                ko.applyBindings(vm, form[0]);



                ko.applyBindingsToNode(document.getElementById('pnlAreas'), { collapsible: { vmArray: [vm.Areas()] } });
                ko.applyBindingsToNode(document.getElementById('pnlSetores'), { collapsible: { vmArray: [vm.Setores()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProprietarios'), { collapsible: { vmArray: [vm.Proprietarios()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDocumentos'), { collapsible: { vmArray: [vm.CNPJ(), vm.IE(), vm.CCIR(), vm.NIRF(), vm.Matricula(), vm.IdsCaracterizacao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlOutrasInformacoes'), { collapsible: { vmArray: [vm.Observacao()] } });

            });

            return request;
        };

    return {
        bind: bind
    }
});