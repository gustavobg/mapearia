﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (objetivo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            objetivo.config({
                saveUrl: '/Agricola/Objetivo/NewEdit',
                getUrl: '/Agricola/Objetivo/NewEditJson',
            });
            var request = objetivo.get({ id: id }),
                form = $('#form-ProdutoObjetivo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    objetivo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Objetivo da Cultura salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});