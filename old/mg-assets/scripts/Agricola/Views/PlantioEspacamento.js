﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask', 'mask-decimal'], function (plantioEspacamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            plantioEspacamento.config({
                saveUrl: '/Agricola/PlantioEspacamento/NewEdit',
                getUrl: '/Agricola/PlantioEspacamento/NewEditJson',
            });
            var request = plantioEspacamento.get({ id: id }),
                form = $('#form-PlantioEspacamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.Espacamentos = ko.observableArray([
                { id: '1', name: 'Apenas o espaçamento de Entre-Linha' },
                { id: '2', name: 'Linha e Entre-Linha' },
                { id: '3', name: 'Entre-Linha e Entre-Linha-Dupla' },
                ]);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    plantioEspacamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Espaçamento de Plantio salvo com sucesso.');
                    });
                }

                var vmParams = function () {
                    this.ValidaLinha = ko.observable(false);
                    this.ValidaEntreLinha = ko.observable(false);
                    this.ValidaEntreLinhaDupla = ko.observable(false);
                };
                vmParams = new vmParams();

                ko.computed(function () {
                    switch (vm.EspacamentoOpcao()) {
                        case "1":
                            vmParams.ValidaLinha(true);
                            vmParams.ValidaEntreLinha(false);
                            vmParams.ValidaEntreLinhaDupla(false);
                            vm.EntreLinha(null);
                            vm.EntreLinhaDupla(null);
                            break;
                        case "2":
                            vmParams.ValidaLinha(true);
                            vmParams.ValidaEntreLinha(true);
                            vmParams.ValidaEntreLinhaDupla(false);
                            vm.EntreLinhaDupla(null);
                            break;
                        case "3":
                            vmParams.ValidaLinha(true);
                            vmParams.ValidaEntreLinha(true);
                            vmParams.ValidaEntreLinhaDupla(true);
                            break;
                        default:
                            break;
                    }
                });

                window.vmParams = vmParams;
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});