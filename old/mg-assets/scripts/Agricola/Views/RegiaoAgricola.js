﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (regiaoAgricola, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            regiaoAgricola.config({
                saveUrl: '/Agricola/RegiaoAgricola/NewEdit',
                getUrl: '/Agricola/RegiaoAgricola/NewEditJson',
            });
            var request = regiaoAgricola.get({ id: id }),
                form = $('#form-ProducaoRegiao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    regiaoAgricola.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Região Agrícola salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});