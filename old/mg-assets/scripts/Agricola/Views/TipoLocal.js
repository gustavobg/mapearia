﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'component.colorSelector'], function (tipoLocal, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            tipoLocal.config({
                saveUrl: '/Agricola/TipoLocal/NewEdit',
                getUrl: '/Agricola/TipoLocal/NewEditJson',
            });
            var request = tipoLocal.get({ id: id }),
                form = $('#form-TipoLocal-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.PermiteRepresentacaoGeo() != null && vm.PermiteRepresentacaoGeo() == false)
                    {
                        vm.GeracaoAutomaticaMapa(null);
                        vm.Cor(null)
                        vm.IdMapeamentoCamadaCategoria(null);
                        vm.IdMapeamentoCamada(null);
                    }
                });

                vm.IdMapeamentoCamadaCategoria.subscribe(function (newValue) {
                    if(newValue == null && newValue == '')
                    {
                        vm.IdMapeamentoCamada(null);
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    tipoLocal.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Local salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
                ko.applyBindingsToNode(document.getElementById('pnlDadosAdicionais'), { collapsible: { vmArray: [vm.PossuiUnidadeProducao(), vm.Prefixo(), vm.IdUnidade()] } });
            });

            return request;
        };

    return {
        bind: bind
    }

});