﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (tipoMaturacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            tipoMaturacao.config({
                saveUrl: '/Agricola/TipoMaturacao/NewEdit',
                getUrl: '/Agricola/TipoMaturacao/NewEditJson',
            });
            var request = tipoMaturacao.get({ id: id }),
                form = $('#form-ProducaoTipoMaturacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    tipoMaturacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Maturacao salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});