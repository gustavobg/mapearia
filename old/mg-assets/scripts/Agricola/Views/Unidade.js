﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask', 'mask-coordenada', 'unidade-tipo', 'component.colorSelector'], function (unidade, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            unidade.config({
                saveUrl: '/Agricola/Unidade/NewEdit',
                getUrl: '/Agricola/Unidade/NewEditJson',
            });
            var request = unidade.get({ id: id }),
                form = $('#form-ProdutoUnidade-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.ProducaoUnidadeTipoVM = ko.observable();
                vm.ExibeSubUnidade = ko.observable(false);
                vm.SubUnidadeObrigatoria = ko.observable(false);
                vm.DescricaoUnidade = ko.observable('');
                vm.DescricaoUnidadeTipo = ko.observable('');
                vm.ExibeUnidadeMedidaPrimaria = ko.observable(true);
                vm.ExibeUnidadeMedidaSecundaria = ko.observable(true);

                vm.DescricaoUnidadeMedidaPrincipal = ko.observable('');
                vm.SiglaUnidadeMedidaPrincipal = ko.observable('');
                vm.CasasDecimaisUnidadeMedidaPrincipal = ko.observable(0);

                vm.DescricaoUnidadeMedidaSecundaria = ko.observable('');
                vm.SiglaUnidadeMedidaSecundaria = ko.observable('');
                vm.CasasDecimaisUnidadeMedidaSecundaria = ko.observable(0);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    unidade.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Unidade de Produção salva com sucesso.');
                    });
                };

                //var EntidadeBaseValor = function () {
                //    this.IdProducaoUnidadeBase = null;
                //    this.IdProducaoUnidade = null;
                //    this.IdProducaoTipoUnidadeBase = null;
                //    this.Valor = null;
                //    this.IdUnidade = null;
                //    this.DescricaoUnidade = null;
                //};

                //vm.IdProducaoUnidadeTipo.subscribe(function () {
                //    if (confirm("Deseja Importar os dados através do Tipo de Unidade selecionado ?")) {
                //        if (vm.IdProducaoUnidadeTipo() != null || vm.IdProducaoUnidadeTipo() != '') {
                //            $.ajax({
                //                url: '/Agricola/UnidadeTipo/GetTipoUnidadeCompleto',
                //                data: { id: vm.IdProducaoUnidadeTipo() },
                //                type: 'POST',
                //                async: false,
                //                success: function (unidadeTipo) {
                //                    if (unidadeTipo.PossuiUnidadeBase) {   
                //                        var unidadesBase = [];
                //                        for (var i = 0; i < unidadeTipo.Unidades.length; i++) {
                //                            var baseValor = new EntidadeBaseValor();
                //                            baseValor.IdProducaoUnidadeBase = null;
                //                            baseValor.IdProducaoUnidade = vm.IdProducaoUnidade();
                //                            baseValor.IdProducaoTipoUnidadeBase = unidadeTipo.Unidades[i].IdProducaoUnidadeTipoBase;
                //                            baseValor.Valor = null;
                //                            baseValor.IdUnidade = unidadeTipo.Unidades[i].IdUnidade;
                //                            baseValor.DescricaoUnidade = unidadeTipo.Unidades[i].DescricaoUnidade;

                //                            unidadesBase.push(baseValor);
                //                        }
                //                        vm.BasesValores(unidadesBase);
                //                    }
                //                    else {
                //                        $("#pnlUnidadesMedidas").hide();
                //                    }
                //                }
                //            });
                //        }
                //    }
                //});

                ko.computed(function () {
                    if (vm.IdProducaoUnidadeTipo() != null) {
                        $.post('/Agricola/UnidadeTipo/List', JSON.stringify({
                            IdProducaoUnidadeTipo: vm.IdProducaoUnidadeTipo(),
                            Ativo: true,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        })).success(function (data) {
                            ProducaoUnidadeTipo = data.Data[0];
                            if (ProducaoUnidadeTipo.TipoControleSubUnidade == 2 || ProducaoUnidadeTipo.TipoControleSubUnidade == 3) {
                                vm.ExibeSubUnidade(true);
                                vm.DescricaoUnidade(ProducaoUnidadeTipo.DescricaoSubUnidade);
                                if (ProducaoUnidadeTipo.TipoControleSubUnidade == 3)
                                    vm.SubUnidadeObrigatoria(true);
                                else
                                    vm.SubUnidadeObrigatoria(false);
                            }
                            else {
                                vm.ExibeSubUnidade(false);
                                vm.DescricaoUnidade(null);
                                vm.SubUnidadeObrigatoria(false);
                                vm.DescricaoSubUnidade(null);
                            }
                        });
                    }
                });
                ko.computed(function () {
                    if (vm.Nomenclatura() == 1) {
                        vm.Descricao(vm.DescricaoUnidadeTipo());
                    }
                    else if (vm.Nomenclatura() == 2) {

                    }
                });

                ko.computed(function () {
                    if (vm.ProducaoUnidadeTipoVM() != null && vm.ProducaoUnidadeTipoVM().IdUnidadePrincipal != null) {
                        vm.PossuiUnidadeBase(true);
                        vm.ExibeUnidadeMedidaPrimaria(true);

                        //Unidade Principal
                        $.post('/Estoque/Unidade/List', JSON.stringify({
                            IdUnidade: vm.ProducaoUnidadeTipoVM().IdUnidadePrincipal,
                            Ativo: true,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        })).success(function (data) {
                            vm.DescricaoUnidadeMedidaPrincipal(data.Data[0].Descricao);
                            vm.SiglaUnidadeMedidaPrincipal(data.Data[0].Sigla);
                            vm.CasasDecimaisUnidadeMedidaPrincipal(data.Data[0].CasasDecimais);
                        });
                    }
                    else
                    {
                        vm.ExibeUnidadeMedidaPrimaria(false);
                    }

                    //Unidade Secundaria
                    if(vm.ProducaoUnidadeTipoVM() != null && vm.ProducaoUnidadeTipoVM().IdUnidadeSecundaria != null)
                    {
                        vm.ExibeUnidadeMedidaSecundaria(true);

                        $.post('/Estoque/Unidade/List', JSON.stringify({
                            IdUnidade: vm.ProducaoUnidadeTipoVM().IdUnidadeSecundaria,
                            Ativo: true,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        })).success(function (data) {
                            vm.DescricaoUnidadeMedidaSecundaria(data.Data[0].Descricao);
                            vm.SiglaUnidadeMedidaSecundaria(data.Data[0].Sigla);
                            vm.CasasDecimaisUnidadeMedidaSecundaria(data.Data[0].CasasDecimais);
                        });

                    }
                    else
                    {
                        vm.ExibeUnidadeMedidaSecundaria(false);
                    }
                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                ko.applyBindingsToNode(document.getElementById('pnlUnidadesMedidas'), { collapsible: { vmArray: [vm.ValorUnidadePrincipal(), vm.ValorUnidadeSecundaria()] } });
                ko.applyBindingsToNode(document.getElementById('pnlOutrasInformacoes'), { collapsible: { vmArray: [vm.IdProducaoAmbienteProducao(), vm.Observacao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDadosGeograficos'), { collapsible: { vmArray: [vm.Latitude(), vm.Altitude(), vm.Longitude()] } });
            });

            return request;
        };

    return {
        bind: bind
    }

});