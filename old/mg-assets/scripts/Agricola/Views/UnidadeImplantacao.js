﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask', 'gridview', 'unidade-tipo'], function (unidadeImplantacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            unidadeImplantacao.config({
                saveUrl: '/Agricola/UnidadeImplantacao/NewEdit',
                getUrl: '/Agricola/UnidadeImplantacao/NewEditJson',
            });
            var request = unidadeImplantacao.get({ id: id }),
                form = $('#form-UnidadeImplantacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);                

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    unidadeImplantacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Unidade de Implantação salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});