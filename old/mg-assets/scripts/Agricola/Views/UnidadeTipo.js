﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-collapsible', 'component.colorSelector'], function (unidadeTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            unidadeTipo.config({
                saveUrl: '/Agricola/UnidadeTipo/NewEdit',
                getUrl: '/Agricola/UnidadeTipo/NewEditJson'
            });

            var request = unidadeTipo.get({ id: id }),
                form = $('#form-ProdutoUnidadeTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.IdUnidadeTipoPrincipal.subscribe(function (newValue) {
                    vm.IdUnidadePrincipal(null);
                });

                vm.IdUnidadeTipoSecundaria.subscribe(function (newValue) {
                    vm.IdUnidadeSecundaria(null);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    unidadeTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Unidade Salvo com Sucesso.');
                    });
                };

                var vmParams = function () {
                    this.ExibeDescricaoSubUnidade = ko.observable(true);
                };
                var vmParams = new vmParams();

                ko.computed(function () {
                    if (vm.TipoControleSubUnidade() != 1) {
                        vmParams.ExibeDescricaoSubUnidade(true)
                    } else {
                        vmParams.ExibeDescricaoSubUnidade(false)
                        vm.DescricaoSubUnidade('');
                    }
                });

                //Mapeamento
                ko.computed(function () {
                    if (vm.PermiteRepresentacaoGeo() != null && vm.PermiteRepresentacaoGeo() == false) {
                        vm.Cor(null);
                    }
                });

                window.vmParams = vmParams;
                window.vm = vm;

                ko.applyBindings(vm, form[0]);

                ko.applyBindingsToNode(document.getElementById('pnlSubLocais'), { collapsible: { vmArray: [vm.IdsProducaoLocalTipo()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDetalhes'), { collapsible: { vmArray: [vm.Prefixo()] } });
                ko.applyBindingsToNode(document.getElementById('pnlUnidades'), { collapsible: { vmArray: [vm.IdUnidadeTipoPrincipal(), vm.IdUnidadePrincipal(), vm.IdUnidadeTipoSecundaria(), vm.IdUnidadeSecundaria()] } });

                // adaptar
                var tooltip = $("#ddlUnidadeTipo").qtip($.extend(true, defaults.qtip.popoverFixed, {
                    position: {
                        adjust: {
                            y: -17,
                            x: 5
                        }
                    },
                    show: true,
                    content: {
                        text: function (event, api) {
                            window.setTimeout(function () {
                                api.set({ 'content.title': 'Exemplos', 'content.text': getHelp() });
                            }, 10);
                        }
                    },
                    hide: false
                })),
                api = tooltip.qtip('api');

                Route.destroy = function () {
                    api.destroy();
                };

                function getHelp() {
                    var helpText = "";

                    switch ($("#ddlUnidadeTipo").val()) {
                        case "1":
                            helpText = "Locais para desenvolvimento de Agricultura, Fruticultura, Horticultura, Silvicultura, Heveicultura e afins.";
                            break;
                        case "2":
                            helpText = "Locais onde se desenvolve atividades em florestas nativas, como: Castanhas, Sementes, Látex e afins.";
                            break;
                        case "3":
                            helpText = "Locais de uso para criações, como: Pastos, Piquetes e afins.";
                            break;
                        case "4":
                            helpText = "Locais de uso para criações, como: Represas, Tanques (Escavados), Viveiros e afins.";
                            break;
                        case "5":
                            helpText = "Locais de uso para criações, como: Caixas de Abelhas, Tanque Rede, Gaiolas e afins";
                            break;
                        case "6":
                            helpText = "Locais de beneficiamento de pós-colheita, processamento de matéria prima ou industrialização de produtos.";
                            break;
                        case "7":
                            helpText = "Locais onde se plantam mudas.";
                            break;
                        default:
                    }
                    return helpText;
                };


                $("#ddlUnidadeTipo").change(function () {
                    api.set({
                        'content.title': 'Exemplos',
                        'content.text': getHelp()
                    }).toggle(true);

                });
            });

            return request;
        };

    return {
        bind: bind
    }
});