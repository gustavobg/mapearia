﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask'], function (variedade, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            variedade.config({
                saveUrl: '/Agricola/Variedade/NewEdit',
                getUrl: '/Agricola/Variedade/NewEditJson',
            });
            var request = variedade.get({ id: id }),
                form = $('#form-ProducaoVariedade-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    variedade.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Variedade salva com sucesso.');
                    });
                }

                // vm para parâmetros e comportamentos diversos
                vmParams = function () {
                    // Parâmetros para select2
                    this.IdUnidadeIn = '72,73,74,75';
                    // txtIdsEpocas
                    this.IdProducaoEpocaPlantioNotIn = ko.computed(function () {
                        // Remove épocas já adicionadas ao vm da seleção do select2
                        var ids = ko.utils.arrayMap(vm.VariedadesEpocas(), function (item) {
                            return item.IdProducaoEpocaPlantio;
                        });
                        return ids.join(',');
                    });
                    this.VidaEmColheitas = false;
                    this.PermiteVariasCulturas = false;
                };
                var vmParams = new vmParams();
                ko.computed(function () {
                    // Quando usuário escolhe uma cultura que permite várias colheitas, habilita 
                    // painel no modal de ciclo médio com preenchimento de "demais colheitas"
                    var ProducaoCultura = null;
                    $.post('/Agricola/Cultura/List', JSON.stringify({
                        IdProducaoCultura: vm.IdProducaoCultura(),
                        ComCiclos: true,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                        }
                    })).success(function (data) {
                        ProducaoCultura = data.Data[0];
                        if (ProducaoCultura.PermiteVariasColheitas) {
                            $('#pnlVidaEmColheitas').css('display', 'block');
                            vmParams.VidaEmColheitas = true;
                        } else {
                            $('#pnlVidaEmColheitas').css('display', 'none');
                            vmParams.VidaEmColheitas = false;
                        }
                        vmParams.PermiteVariasCulturas = ProducaoCultura.PermiteVariasColheitas;
                    });
                });

                vm.IdProducaoCultura.subscribe(function () {
                    vm.IdProducaoCulturaVariedadeTipo(null);

                    if (vm.IdProducaoVariedade() != null) {
                        if (confirm("Deseja Substituir os dados da Cultura Atual?")) {
                            $.post('/Agricola/Cultura/List', JSON.stringify({
                                IdProducaoCultura: vm.IdProducaoCultura(),
                                ComCiclos: true,
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                                }
                            })).success(function (data) {
                                ProducaoCultura = data.Data[0];
                                vm.VidaEmColheitas(ProducaoCultura.ColheitasPossiveis);
                                if (ProducaoCultura.CulturasEpocas.length > 0) {
                                    $('#pnlVariedadeEpocaPlantio').collapsible('open');
                                    vm.VariedadesEpocas(ProducaoCultura.CulturasEpocas);
                                }
                            });
                        }
                    } else {
                        $.post('/Agricola/Cultura/List', JSON.stringify({
                            IdProducaoCultura: vm.IdProducaoCultura(),
                            ComCiclos: true,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        })).success(function (data) {
                            ProducaoCultura = data.Data[0];
                            vm.VidaEmColheitas(ProducaoCultura.ColheitasPossiveis);
                            vm.VariedadesEpocas(ProducaoCultura.CulturasEpocas);
                            if (vm.VariedadesEpocas().length > 0) {
                                $('#pnlVariedadeEpocaPlantio').collapsible('open');
                            }
                        });
                    }
                });

                window.vmParams = vmParams;
                window.vm = vm;
                ko.applyBindings(vm, form[0]);
                ko.applyBindingsToNode(document.getElementById('pnlVariedadeEpocaPlantio'), { collapsible: { vmArray: [vm.VariedadesEpocas()] } });
            });

            return request;
        };

    return {
        bind: bind
    }

});