﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (aplicacaoReferencia, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            aplicacaoReferencia.config({
                saveUrl: '/Aplicacao/AplicacaoReferencia/NewEdit',
                getUrl: '/Aplicacao/AplicacaoReferencia/NewEditJson',
            });
            var request = aplicacaoReferencia.get({ id: id }),
                form = $('#form-AplicacaoReferencia-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.IdAplicacaoTipo() == 1)
                        vm.DescricaoAplicacaoReferenciaExibicao(vm.DescricaoBenfeitoria());

                    else if (vm.IdAplicacaoTipo() == 2)
                        vm.DescricaoAplicacaoReferenciaExibicao(vm.DescricaoProducaoLocal());

                    else if (vm.IdAplicacaoTipo() == 3 || vm.IdAplicacaoTipo() == 4)
                        vm.DescricaoAplicacaoReferenciaExibicao(vm.DescricaoCriacaoTipo());

                    else if (vm.IdAplicacaoTipo() == 5)
                        vm.DescricaoAplicacaoReferenciaExibicao(vm.DescricaoAutomotivoEquipamento());
                    else
                        vm.DescricaoAplicacaoReferenciaExibicao(null);
                });

                vm.Save = function () {
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        aplicacaoReferencia.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, '');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);
                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});