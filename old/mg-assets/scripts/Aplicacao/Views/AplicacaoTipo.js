﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (tipo, ko, mgFeedbackBase) {
    // [Obsoleto]
    var vm = {},
        bind = function (id) {
            tipo.config({
                saveUrl: '/Aplicacao/AplicacaoTipo/NewEdit',
                getUrl: '/Aplicacao/AplicacaoTipo/NewEditJson'
            });

            var request = tipo.get({ id: id }),
                form = $('#form-AplicacaoTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    tipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Aplicação salva com Sucesso.');
                    });
                };

                var vmItemExtended = function (vm) {
                    ko.computed(function () {
                        if (!vm.UtilizaSubAplicacaoTipo())
                            vm.IdAplicacaoTabelaReferenciaSubAplicacaoTipo(null);
                    });
                }
                window.vmItemExtended = vmItemExtended;
                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlItens'), { collapsible: { vmArray: [vm.Itens()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});