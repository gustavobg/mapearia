﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (corEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            corEquipamento.config({
                saveUrl: '/Automotivo/CorEquipamento/NewEdit',
                getUrl: '/Automotivo/CorEquipamento/NewEditJson',
            });
            var request = corEquipamento.get({ id: id }),
                form = $('#form-AutomotivoCorEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    corEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Cor de Equipamento salva com sucesso.');
                    });
                };

                window.vm = vm;

                //ko.applyBindingsToNode(document.getElementById('pnlObjetos'), { collapsible: { vmArray: [vm.IdsObjetos()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});