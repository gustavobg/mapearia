﻿define(['crud-controller', 'knockout', 'feedback', 'knockout-upload-mapping', 'panels', 'knockout-upload', 'select2', 'jquery-flipper', 'ko-validate', 'partial-pessoa', 'gridview', 'knockout-postbox'], function (equipamento, ko, mgFeedbackBase, vmMappingToUpload, panels) {

    var vm = {},
        bind = function (id) {
            equipamento.config({
                saveUrl: '/Automotivo/Equipamento/SalvarEquipamento', // ? SalvarEquipamento
                getUrl: '/Automotivo/Equipamento/NewEditJson',
            });
            var request = equipamento.get({ id: id }),
                form = $('#form-AutomotivoEquipamento-NewEdit');

            request.done(function (response) {

                function RetornaPartesEquipamento(IdModelo) {
                    var retorno = null;
                    if (IdModelo != null) {
                        $.ajax({
                            type: "POST",
                            data: { idModelo: IdModelo },
                            url: "/Automotivo/ParteEquipamento/RetornaParteEquipamentoPorModelo",
                            dataType: "text",
                            async: false,
                            success: function (result) {
                                if (result != '') {
                                    retorno = result;
                                } else {
                                    retorno = '';
                                    toastr.error("Não existe partes para o Modelo selecionado.");
                                }
                            }
                        });
                    } else {
                        retorno = '';
                    }

                    return retorno;
                }

                vm = ko.mapping.fromJS(response);

                vm.IdsPartesPadrao = '';
                vm.ReverterPadrao = function () {
                    vm.IdsPartes(RetornaPartesEquipamento(vm.IdAutomotivoModeloEquipamento()));
                };
                vm.DescricaModelo = ko.observable('');
                vm.DescricaoTipo = ko.observable('');
                vm.DescricaoModelo = ko.observable('');
                vm.DescricaoMarca = ko.observable('');
                vm.IdAutomotivoEquipamentoPai = ko.observable(null);
               
                ko.applyBindings({}, document.getElementById('panels-bind'));
               
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    delete vm['ArquivosDigitaisToUploader']; // Importante remover propriedades adicionais da VM, para Razor validar a VM
                    delete vm['ArquivosDigitaisFromUploaderJS'];

                    var data = ko.toJSON(vm);

                    equipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Equipamento salvo com sucesso.');
                    });
                };

                vm = new vmMappingToUpload(vm);

                var vmParams = function () {
                    this.VmModeloEquipamento = ko.observable();
                    this.VmTipoEquipamento = ko.observable();
                };

                vmParams = new vmParams();

                ko.computed(function () {
                    //vm.IdsPartes(RetornaPartesEquipamento(vm.IdAutomotivoModeloEquipamento()));
                    if (vm.Nomenclatura() != null || vm.Nomenclatura() != '') {
                        var descricaoModelo = vmParams.VmModeloEquipamento() == null ? '' : vmParams.VmModeloEquipamento().DescricaoReduzida;
                        var descricaoTipo = vmParams.VmTipoEquipamento() == null ? '' : vmParams.VmTipoEquipamento().Descricao;
                        var descricaoMarca = vmParams.VmModeloEquipamento() == null ? '' : vmParams.VmModeloEquipamento().SiglaProdutoServicoMarca;
                        var descricao = '';

                        switch (vm.Nomenclatura()) {
                            case "1":
                                {
                                    descricao = descricaoModelo;
                                }
                                break;
                            case "2":
                                {
                                    descricao = descricaoMarca;
                                    if (descricaoModelo.trim())
                                        descricao += " - " + descricaoModelo;
                                }
                                break;
                            case "3":
                                {
                                    descricao = descricaoTipo;
                                    if (descricaoModelo.length)
                                        descricao += " - " + descricaoModelo;
                                }
                                break;
                            case "4":
                                {
                                    descricao = descricaoTipo;
                                    if (descricaoMarca.trim())
                                        descricao += " - " + descricaoMarca;
                                    if (descricaoModelo.trim())
                                        descricao += " - " + descricaoModelo;
                                }

                        }
                        vm.Descricao(descricao);
                    }
                });

                ko.computed(function () {
                    if (vmParams.VmTipoEquipamento() != null)
                        vm.IdAutomotivoEquipamentoPai(vmParams.VmTipoEquipamento().ItemPai);
                });

                $("#ddlAutomotivoModeloEquipamento").change(function () {
                    vm.IdsPartes(RetornaPartesEquipamento(vm.IdAutomotivoModeloEquipamento()));
                    $("#ddlPartesEquipamento").trigger("change");
                });

                window.vmParams = vmParams;
                window.vm = vm;                

                ko.applyBindingsToNode(document.getElementById('pnlDadosAdicionais'), { collapsible: { vmArray: [vm.AnoModelo(), vm.AnoFabricacao(), vm.DataCompra(), vm.Placa(), vm.Renavam(), vm.Chassis()] } });
                ko.applyBindingsToNode(document.getElementById('pnlImagens'), { collapsible: { vmArray: [vm.ArquivosDigitais()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProprietario'), { collapsible: { vmArray: [vm.Proprietarios()] } });
                ko.applyBindingsToNode(document.getElementById('pnlObservacao'), { collapsible: { vmArray: [vm.Observacao()] } });
                ko.applyBindingsToNode(document.getElementById('partesOuSistema'), { collapsible: { vmArray: [vm.IdsPartes()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});