﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (modeloEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            modeloEquipamento.config({
                saveUrl: '/Automotivo/ModeloEquipamento/NewEdit',
                getUrl: '/Automotivo/ModeloEquipamento/NewEditJson',
            });
            var request = modeloEquipamento.get({ id: id }),
                form = $('#form-AutomotivoModeloEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    modeloEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Modelo de Equipamento salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});