﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (parteEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            parteEquipamento.config({
                saveUrl: '/Automotivo/ParteEquipamento/NewEdit',
                getUrl: '/Automotivo/ParteEquipamento/NewEditJson',
            });
            var request = parteEquipamento.get({ id: id }),
                form = $('#form-AutomotivoParteEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    parteEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Parte de Equipamento salva com sucesso.');
                    });
                };

                window.vm = vm;

                //ko.applyBindingsToNode(document.getElementById('pnlObjetos'), { collapsible: { vmArray: [vm.IdsObjetos()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});