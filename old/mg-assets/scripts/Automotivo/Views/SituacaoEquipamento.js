﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (situacaoEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            situacaoEquipamento.config({
                saveUrl: '/Automotivo/SituacaoEquipamento/NewEdit',
                getUrl: '/Automotivo/SituacaoEquipamento/NewEditJson',
            });
            var request = situacaoEquipamento.get({ id: id }),
                form = $('#form-AutomotivoSituacaoEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    situacaoEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Situação de Equipamento salva com sucesso.');
                    });
                };

                window.vm = vm;              

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});