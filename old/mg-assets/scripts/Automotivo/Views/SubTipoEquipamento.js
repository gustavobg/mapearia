﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (subtipoEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            subtipoEquipamento.config({
                saveUrl: '/Automotivo/SubTipoEquipamento/NewEdit',
                getUrl: '/Automotivo/SubTipoEquipamento/NewEditJson',
            });
            var request = subtipoEquipamento.get({ id: id }),
                form = $('#form-AutomotivoSubTipoEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    subtipoEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Subtipo de Equipamento salvo com sucesso.');
                    });
                };

                window.vm = vm;

                //ko.applyBindingsToNode(document.getElementById('pnlObjetos'), { collapsible: { vmArray: [vm.IdsObjetos()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});