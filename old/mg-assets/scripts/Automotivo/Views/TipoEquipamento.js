﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (tipoEquipamento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            tipoEquipamento.config({
                saveUrl: '/Automotivo/TipoEquipamento/NewEdit',
                getUrl: '/Automotivo/TipoEquipamento/NewEditJson',
            });
            var request = tipoEquipamento.get({ id: id }),
                form = $('#form-AutomotivoTipoEquipamento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'UltimoNivel': true, 'IdAutomotivoTipoEquipamentoNotIn': vm.IdAutomotivoTipoEquipamento() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    tipoEquipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Equipamento salvo com sucesso.');
                    });
                };

                window.vm = vm;

                //ko.applyBindingsToNode(document.getElementById('pnlObjetos'), { collapsible: { vmArray: [vm.IdsObjetos()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});