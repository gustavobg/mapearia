﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview'], function (caracteristica, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            caracteristica.config({
                saveUrl: '/Configuracao/Caracteristica/NewEdit',
                getUrl: '/Configuracao/Caracteristica/NewEditJson',
            });
            var request = caracteristica.get({ id: id }),
                form = $('#form-ConfiguracaoCaracteristica-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    caracteristica.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Característica salva com sucesso.');
                    });
                };

                var vmParams = {
                    ExibeOpcoes: ko.computed(function () {
                        var idCaracteristicaTipo = Number(vm.IdCaracteristicaTipo());
                        return (idCaracteristicaTipo === 8 || idCaracteristicaTipo === 9);
                    })
                };

                window.vmParams = vmParams;
                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlMaisCampos'), { collapsible: { vmArray: [vm.Ordem(), vm.Tamanho()] } });
               
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});