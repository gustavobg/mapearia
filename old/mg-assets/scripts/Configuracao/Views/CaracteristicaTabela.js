﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (caracteristicaTabela, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            caracteristicaTabela.config({
                saveUrl: '/Configuracao/CaracteristicaTabela/NewEdit',
                getUrl: '/Configuracao/CaracteristicaTabela/NewEditJson',
            });
            var request = caracteristicaTabela.get({ id: id }),
                form = $('#form-ConfiguracaoCaracteristicaTabela-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    caracteristicaTabela.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tabela de Característica salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});