﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask'], function (caracteristicaTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            caracteristicaTipo.config({
                saveUrl: '/Configuracao/CaracteristicaTipo/NewEdit',
                getUrl: '/Configuracao/CaracteristicaTipo/NewEditJson',
            });
            var request = caracteristicaTipo.get({ id: id }),
                form = $('#form-ConfiguracaoCaracteristicaTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    caracteristicaTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Característica salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});