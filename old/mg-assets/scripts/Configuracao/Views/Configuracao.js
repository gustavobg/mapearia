﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (configuracao, ko, mgFeedbackBase) {

    var vm = { Response: ko.observable(null) },        
        bind = function (id) {
            ko.bindingHandlers.flipperCustom = {
                init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                    var observable = valueAccessor().observable;
                    $el = $(element);
                    $el.prop('checked', observable() == 'True' ? true : false);
                    $el.flipper({
                        afterTurnOn: function () {
                            observable('True');
                        },
                        afterTurnOff: function () {
                            observable('False');
                        }
                    });
                },
                update: function (element, valueAccessor) {
                    $(element).flipper("refresh");
                }
            };

            configuracao.config({
                saveUrl: '/Configuracao/Parametro/SalvarConfiguracao',
                getUrl: '/Configuracao/Parametro/ConfiguracaoJson',
            });
            var request = configuracao.get({ id: id }),
                form = $('#form-Configuracao-Configuracoes');

            request.done(function (response) {
                vm.Response(ko.mapping.fromJS(response));               
                vm.IdModulo = ko.observable(null);                
                
                vm.Save = function () {                       
                    var data = { 'json': ko.toJSON(vm.Response()) };
                    configuracao.save(JSON.stringify(data)).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração salva com sucesso.', false, function () {                           
                        });
                    });
                };               

                $("#ddlModulo").change(function (e) {                    
                    configuracao.get({ id: $(this).val() }).done(function (response) {
                        vm.Response(ko.mapping.fromJS(response));
                    });

                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind,
        routeOptions: { showBackButton: false, showTitle: true }
    }

});