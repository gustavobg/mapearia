﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (modulo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            modulo.config({
                saveUrl: '/Configuracao/Modulo/NewEdit',
                getUrl: '/Configuracao/Modulo/NewEditJson',
            });
            var request = modulo.get({ id: id }),
                form = $('#form-ConfiguracaoModulo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    modulo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Módulo salvo com sucesso.');
                    });
                };
                
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});