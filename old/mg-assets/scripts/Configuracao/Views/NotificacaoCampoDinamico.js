﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (notificacaoCampoDinamico, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            notificacaoCampoDinamico.config({
                saveUrl: '/Configuracao/NotificacaoCampoDinamico/NewEdit',
                getUrl: '/Configuracao/NotificacaoCampoDinamico/NewEditJson',
            });
            var request = notificacaoCampoDinamico.get({ id: id }),
                form = $('#form-NotificacaoCampoDinamico-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    notificacaoCampoDinamico.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Campo Dinâmico salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});