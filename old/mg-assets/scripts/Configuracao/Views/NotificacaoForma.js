﻿define(['crud-controller', 'knockout', 'feedback',  'toastr',  'select2', 'jquery-flipper', 'ko-validate'], function (notificacaoForma, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            notificacaoForma.config({
                saveUrl: '/Configuracao/NotificacaoForma/NewEdit',
                getUrl: '/Configuracao/NotificacaoForma/NewEditJson',
            });
            var request = notificacaoForma.get({ id: id }),
                form = $('#form-NotificacaoForma-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'IdNotificacaoFormaNotIn': vm.IdNotificacaoForma() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    notificacaoForma.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Forma de Notificação salva com sucesso.');
                    });
                };

                ko.computed(function () {
                    if (!vm.Ativo())
                        toastr.info('Ao inativar uma notificação, todos os seus dependentes serão inativados automaticamente.', '', { timeOut: 5000 });
                });

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});