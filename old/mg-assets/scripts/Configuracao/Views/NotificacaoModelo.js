﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (notificacaoModelo, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            notificacaoModelo.config({
                saveUrl: '/Configuracao/NotificacaoModelo/NewEdit',
                getUrl: '/Configuracao/NotificacaoModelo/NewEditJson'
            });

            var request = notificacaoModelo.get({ id: id }),
                form = $('#form-NotificacaoModelo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Configuracao/NotificacaoModelo/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Modelo de Notificação salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});