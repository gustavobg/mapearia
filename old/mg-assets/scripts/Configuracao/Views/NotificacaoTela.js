﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'datetimepicker', 'summernote'], function (notificacaoTela, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            notificacaoTela.config({
                saveUrl: '/Configuracao/NotificacaoTela/NewEdit',
                getUrl: '/Configuracao/NotificacaoTela/NewEditJson',
            });
            var request = notificacaoTela.get({ id: id }),
                form = $('#form-ConfiguracaoNotificacaoTela-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    notificacaoTela.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Notificação de tela salva com sucesso.');
                    });
                };

                ko.computed(function () {
                    if (vm.TodasTelas() == true)
                        vm.IdTela(null);
                });


                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlDadosAdicionais'), { collapsible: { vmArray: [vm.TelaCampo(), vm.DataInicio(), vm.DataTermino()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});