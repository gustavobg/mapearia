﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (notificacaoTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            notificacaoTipo.config({
                saveUrl: '/Configuracao/NotificacaoTipo/NewEdit',
                getUrl: '/Configuracao/NotificacaoTipo/NewEditJson',
            });
            var request = notificacaoTipo.get({ id: id }),
                form = $('#form-NotificacaoTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'IdNotificacaoTipoNotIn': vm.IdNotificacaoTipo() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    notificacaoTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Notificação salva com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});