﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (objeto, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            objeto.config({
                saveUrl: '/Configuracao/Objeto/NewEdit',
                getUrl: '/Configuracao/Objeto/NewEditJson',
            });
            var request = objeto.get({ id: id }),
                form = $('#form-Objeto-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    objeto.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Objeto salvo com sucesso.');
                    });
                };
                
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});