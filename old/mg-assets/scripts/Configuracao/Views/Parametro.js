﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'summernote'], function (parametro, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            parametro.config({
                saveUrl: '/Configuracao/Parametro/NewEdit',
                getUrl: '/Configuracao/Parametro/NewEditJson',
            });
            var request = parametro.get({ id: id }),
                form = $('#form-ConfiguracaoCaracteristica-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    parametro.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Parâmetro salvo com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});