﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate', 'summernote'], function (tela, mg, ko, mgFeedbackBase, toastr) {

    var vm = null,
        bind = function (id) {
            // configura controller genérica de CRUD
            tela.config({
                saveUrl: '/Configuracao/Tela/NewEdit',
                getUrl: '/Configuracao/Tela/NewEditJson'
            });
            var request = tela.get({ id: id }),
                form = $('#form-TelaNewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.Save = function () {
                    var data = ko.toJSON(vm);
                    if (!vm.isValidShowErrors()) { return; }
                    tela.save(data).done(function (result, status, xhr) {                        
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tela salva com sucesso.', true, function () {
                            Menu.reload();
                        });
                    })
                };

                ko.applyBindingsToNode(document.getElementById('pnlAjuda'), { collapsible: { vmArray: [vm.TituloAjuda(), vm.CorpoAjuda()] } });
                ko.applyBindingsToNode(document.getElementById('pnlCampos'), { collapsible: { vmArray: [vm.Campos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDadosAdicionais'), { collapsible: { vmArray: [vm.IdPermissao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlPropriedadesTela'), { collapsible: { vmArray: [vm.IdsTelaPropriedade()] } });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    };

});