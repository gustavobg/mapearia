﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr',  'knockout-upload-mapping', 'knockout-upload', 'jquery-collapsible', 'jquery-flipper', 'jquery-inputmask', 'ko-validate-rules', 'ko-validate', 'vanillaUniform'], function (tema, mg, ko, mgFeedbackBase, toastr, vmMappingToUpload) {

    var vm = null,
        bind = function (id) {
            // configura controller genérica de CRUD
            tema.config({
                saveUrl: '/Configuracao/Tema/NewEdit',
                getUrl: '/Configuracao/Tema/NewEditJson'
            });
            var request = tema.get({ id: id }),
                form = $('#form-Tema-NewEdit');

            request.done(function (response) {               
                vm = ko.mapping.fromJS(response);
                vm.Save = function () {                    
                    delete vm['ArquivosDigitaisToUploader']; // Importante remover propriedades adicionais da VM, para Razor validar a VM
                    delete vm['ArquivosDigitaisFromUploaderJS'];
                    var data = ko.toJSON(vm);
                    if (!vm.isValidShowErrors()) { return; }                   
                    tema.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tema salvo com sucesso.');
                    })
                };
                vm.Cores = ko.observableArray([
                    { id: 1, primaria: '#0277bd', secundaria: '#4CAF50' },
                    { id: 2, primaria: '#607D8B', secundaria: '#009688' },
                    { id: 3, primaria: '#009688', secundaria: '#00BCD4' },
                    { id: 4, primaria: '#795548', secundaria: '#4CAF50' },
                ]);

                vm = new vmMappingToUpload(vm);
                window.vm = vm;    
                ko.applyBindings(vm, form[0]);            

            });

            return request;
        };

    return {
        bind: bind
    };

});