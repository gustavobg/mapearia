﻿define(['knockout', 'jquery', 'gridview'], function (ko, $) {

    var vm = {},
        bind = function () {
            ko.applyBindings({}, document.getElementById('filtro-grade'));
        };

    bind();

    return {
        routeOptions: {
            showTitle: true,
            showBackButton: false,
            title: 'Histórico de Notificações'
        }
    }

});