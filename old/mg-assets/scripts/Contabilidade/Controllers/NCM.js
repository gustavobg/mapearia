﻿define(['util', 'jquery'], function (mg, $) {

    var saveUrl = '/Contabilidade/NCM/NewEdit',
        getUrl = '/Contabilidade/NCM/NewEditJson',
        saveNewUrl = '/Contabilidade/NCM/SaveNew',
        get = function (data) {
            return mg.getViewModel($.extend(true, { id: mg.getId(data), url: getUrl }, data));
        },
        save = function (data) {
            // deffered
            return $.ajaxJsonAntiforgery(Route.getToken(), {
                data: data, //ko.toJSON(vm),
                url: saveUrl // saveNew ? saveNewEditUrl : newEditUrl,            
            });
        },
        saveNew = function (data) {
            return $.ajaxJsonAntiforgery(Route.getToken(), {
                data: data,
                url: saveNewUrl
            });
        };

    return {
        get: get,
        save: save,
        saveNew: saveNew
    }

});