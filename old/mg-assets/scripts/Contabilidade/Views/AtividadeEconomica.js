﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'button', 'jquery-collapsible'], function (atividadeEconomica, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            atividadeEconomica.config({
                saveUrl: '/Contabilidade/AtividadeEconomica/NewEdit',
                getUrl: '/Contabilidade/AtividadeEconomica/NewEditJson'
            });

            function SelecionaProdutoServico() {
                var result = "";
                var i = 0;
                var total = vm.ProdutosServicos().length;
                ko.utils.arrayForEach(vm.ProdutosServicos(), function (item) {
                    i += 1;
                    if (total > i)
                    {
                        if (ko.isObservable(item.IdProdutoServico))
                            result += item.IdProdutoServico() + ", ";
                        else
                            result += item.IdProdutoServico + ", ";
                    }
                        
                    else
                    {
                        if (ko.isObservable(item.IdProdutoServico))
                            result += item.IdProdutoServico() + "";
                        else
                            result += item.IdProdutoServico + "";
                    }
                        
                });
                return result;
            };

            var request = atividadeEconomica.get({ id: id }),
                form = $('#form-AtividadeEconomica-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    atividadeEconomica.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Atividade Econômica salva com sucesso.');
                    });
                };

                var vmProdutoExtend = function (vm) {

                    ko.computed(function () {
                        if(vm.Natureza() == 1)
                        {
                            vm.DescricaoNatureza("Produto");
                            vm.ObrigatorioServico(false);
                            vm.ObrigatorioProduto(true);
                            vm.IdProdutoServico(null);
                        }
                        else if(vm.Natureza() == 2)
                        {
                            vm.DescricaoNatureza("Serviço");
                            vm.ObrigatorioServico(true);
                            vm.ObrigatorioProduto(false);
                            vm.IdProdutoServico(null);
                        }
                        vm.IdProdutoServicoNotIn(SelecionaProdutoServico());
                    })
                    //vm.Natureza.subscribe(function (newValue) {
                    //    if (newValue != null) {
                    //        if (vm.Natureza() == 1) {
                    //        }
                    //        else if (newValue == 2) {
                    //        }
                    //    }
                    //});
                };

                window.vmProdutoExtend = vmProdutoExtend;

                var vmAtividadeEconomicaAtividadeEconomicaEtapa = {
                    DescricaoAtividadeEconomicaEtapa: ko.observable('')
                };
                window.vmAtividadeEconomicaAtividadeEconomicaEtapa = vmAtividadeEconomicaAtividadeEconomicaEtapa;
                
                ko.applyBindingsToNode(document.getElementById('pnlProdutoServico'), { collapsible: { vmArray: [vm.ProdutosServicos()] }});
                ko.applyBindingsToNode(document.getElementById('pnlEtapas'), { collapsible: { vmArray: [vm.Etapas()] } });
                ko.applyBindingsToNode(document.getElementById('pnltiposaplicacao'), { collapsible: { vmArray: [vm.IdsAplicacoesTipo()] } });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});