﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (atividadeEconomicaEtapa, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            atividadeEconomicaEtapa.config({
                saveUrl: '/Contabilidade/AtividadeEconomicaEtapa/NewEdit',
                getUrl: '/Contabilidade/AtividadeEconomicaEtapa/NewEditJson'
            });

            var request = atividadeEconomicaEtapa.get({ id: id }),
                form = $('#form-AtividadeEconomicaEtapa-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.TipoRestricaoServico != null && vm.TipoRestricaoServico() == '1') {
                        vm.IdsServicos(null);
                        vm.IdsProdutoServicoGrupo(null);
                    }
                    else if (vm.TipoRestricaoServico != null && vm.TipoRestricaoServico() == '2')
                        vm.IdsServicos(null);

                    else if (vm.TipoRestricaoServico != null && vm.TipoRestricaoServico() == '3')
                        vm.IdsProdutoServicoGrupo(null);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    atividadeEconomicaEtapa.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Etapa ou Sub Atividade salva com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});