﻿define(['crud-controller', 'knockout', 'feedback', 'jquery-inputmask', 'select2', 'jquery-flipper', 'ko-validate-rules', 'ko-validate'], function (periodo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            periodo.config({
                saveUrl: '/Contabilidade/AtividadeEconomicaPeriodo/NewEdit',
                getUrl: '/Contabilidade/AtividadeEconomicaPeriodo/NewEditJson'
            });

            var request = periodo.get({ id: id }),
                form = $('#form-AtividadeEconomicaPeriodo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //function dataFormatada(d) {
                //    var data = new Date(d),
                //        dia = data.getDate(),
                //        mes = data.getMonth() + 1,
                //        ano = data.getFullYear(),
                //        hora = data.getHours(),
                //        minutos = data.getMinutes(),
                //        segundos = data.getSeconds();
                //    return [dia, mes, ano].join('/') + ' ' + [hora, minutos, segundos].join(':');
                //}

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    periodo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Período de Atividade salvo com Sucesso.');
                    });
                };

                //vmParam.AtividadeSuperior
                var vmParam = function () {
                    this.AtividadeSuperior = ko.observable();

                    ko.computed(function () {
                        var atividadeSuperior = this.AtividadeSuperior();

                        if (atividadeSuperior && atividadeSuperior.hasOwnProperty('DataInicioPadronizado'))
                            vm.DataInicioExibicao(atividadeSuperior.DataInicioPadronizado);
                        else
                        {
                            var d = new Date();
                            vm.DataInicioExibicao('1/01/' + d.getFullYear()); //Nova Regra 11/12/2015
                        }
                        if (atividadeSuperior && atividadeSuperior.hasOwnProperty('DataTerminoPadronizado'))
                            vm.DataTermino(atividadeSuperior.DataTerminoPadronizado);
                        else
                        {
                            var d = new Date();
                            vm.DataTermino('31/01/' + d.getFullYear()); //Nova Regra 11/12/2015
                        }
                        if (atividadeSuperior && atividadeSuperior.hasOwnProperty('IdsAtividadesEconomicas')) {
                            vm.IdsAtividadeEconomica(atividadeSuperior.IdsAtividadesEconomica);
                            if (atividadeSuperior.hasOwnProperty('IdsAtividadesEconomicas') && vm.IdsAtividadeEconomica)
                                $("#pnlAtividadesEconomicas").collapsible('open');
                            else 
                                $("#pnlAtividadesEconomicas").collapsible('close');
                        }
                    }, this);
                };

                ko.applyBindingsToNode(document.getElementById('pnlPeriodoLancamentos'), { collapsible: { vmArray: [vm.DataInicioExibicao(), vm.DataTermino()] } });
                ko.applyBindingsToNode(document.getElementById('pnlAtividadesEconomicas'), { collapsible: { vmArray: [vm.IdsAtividadeEconomica()] } });

                vmParam = new vmParam();
                window.vmParam = vmParam;
                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});