﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (segmento, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            segmento.config({
                saveUrl: '/Contabilidade/AtividadeEconomicaSegmento/NewEdit',
                getUrl: '/Contabilidade/AtividadeEconomicaSegmento/NewEditJson'
            });

            var request = segmento.get({ id: id }),
                form = $('#form-AtividadeEconomicaSegmento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    segmento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Segmento de Atividade Econômica salva com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});