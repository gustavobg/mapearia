﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (cfop, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            cfop.config({
                saveUrl: '/Contabilidade/CFOP/NewEdit',
                getUrl: '/Contabilidade/CFOP/NewEditJson'
            });

            var request = cfop.get({ id: id }),
                form = $('#form-FiscalCFOP-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    cfop.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'CFOP Salva com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                ko.computed(function () {
                    debugger;
                    if (vm.OperacaoEstadual() != null && vm.OperacaoEstadual() != '') {
                        var opEstadual = vm.OperacaoEstadual();

                        if (opEstadual.substr(0, 1) == '1')
                            vm.IndiceEstadual('5');
                        else if (opEstadual.substr(0, 1) == '5')
                            vm.IndiceEstadual('1');
                    }

                    if (vm.OperacaoInterestadual() != null && vm.OperacaoInterestadual() != '') {
                        var opInterestadual = vm.OperacaoInterestadual();

                        if (opInterestadual.substr(0, 1) == '2')
                            vm.IndiceInterestadual('6');
                        else if (opInterestadual.substr(0, 1) == '6')
                            vm.IndiceInterestadual('2');
                    }
                    if (vm.OperacaoInternacional() != null && vm.OperacaoInternacional() != '') {
                        var opInternacional = vm.OperacaoInternacional();

                        if (opInternacional.substr(0, 1) == '3')
                            vm.IndiceInternacional('7');
                        else if (opInternacional.substr(0, 1) == '7')
                            vm.IndiceInternacional('3');
                    }
                });
            });

            return request;
        };

    return {
        bind: bind
    }
});