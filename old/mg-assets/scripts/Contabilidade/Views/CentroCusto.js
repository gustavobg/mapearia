﻿define(['../Controllers/CentroCusto', 'jquery', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'datetimepicker', 'ko-validate-rules', 'ko-validate', 'vanillaUniform'], function (centro, $, ko, mgFeedbackBase, toastr) {
    bind = function (id) {

        var vmPai = { CentroCusto: ko.observable(null) },
            request = centro.get({ id: id }),
            formId = 'form-CentroCusto-NewEdit',
            formElement = $('#' + formId);

        request.done(function (response) {

            vmPai.CentroCusto(ko.mapping.fromJS(response));
            vm = vmPai.CentroCusto(); // simplifica acesso a vm           

            vm.UltimoNivel.subscribe(function (newValue) {
                if (newValue == true)
                    vm.CentroCusto().PermiteUsoSemAplicacao(true);
            });

            vm.Save = function () {
                if (!vm.isValidShowErrors()) { return; }

                vmPai.CentroCusto().SaveNewCrud(Route.saveNew());

                var data = ko.toJSON(vmPai.CentroCusto());

                centro.save(data).done(function (result, status, xhr) {
                    mgFeedbackBase.feedbackCrudRoute(result, 'Centro de Custo salvo com sucesso.', !(Route.saveNew()), function () {
                        // refresh binds
                        if (Route.saveNew() === true) {
                            vmPai.CentroCusto(ko.mapping.fromJS(result));
                            // Route.quiet true ignore default actions after route change                    
                            Route.quiet = true;
                            Route.redirectRouteAction('New');
                            Route.saveNew(true);
                            Route.quiet = false;
                            // TODO: verificar bug velocity e remover essa linha
                            formElement.find('.panel-action').not('.panel-selected').velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 500);
                        }
                    });

                });
            };

            vmPai.CentroCusto().AplicarMascara = ko.computed(function () {
                if (vmPai.CentroCusto().IdPlanoContasVersao() != null) {
                    $.ajax({
                        url: '/Contabilidade/VersaoPlanoContas/List',
                        type: 'post',
                        dataType: 'json',
                        data: JSON.stringify({
                            IdPlanoContasVersao: vmPai.CentroCusto().IdPlanoContasVersao(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        }),
                        async: false,
                        success: function (data) {
                            vmPai.CentroCusto().Mascara(data.Data[0].Mascara);
                        }
                    });
                }
                //Validação, sempre que Ultimo nível é permitir selecionar Atividade Economica 
                if (!vmPai.CentroCusto().UltimoNivel())
                    vmPai.CentroCusto().IdAtividadeEconomica(null);
            });

            ko.computed(function () {
                if (vmPai.CentroCusto().Codigo() != null) {
                    $.ajax({
                        url: '/Contabilidade/CentroCusto/RetornaCodigoSuperior',
                        type: 'post',
                        data: 'codigo=' + vmPai.CentroCusto().Codigo(),
                        async: false,
                        success: function (codigo) {
                            if (codigo != vmPai.CentroCusto().Codigo() && codigo != "") {
                                $.ajax({
                                    url: '/Contabilidade/CentroCusto/ListarHierarquicamentePorParametro',
                                    type: 'post',
                                    dataType: 'json',
                                    data: JSON.stringify({
                                        IdPlanoContasVersao: vmPai.CentroCusto().IdPlanoContasVersao(),
                                        Ativo: true,
                                        Codigo: codigo,
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Codigo asc'
                                        }
                                    }),
                                    async: false,
                                    success: function (data) {
                                        if (data.Data[0] != null) {
                                            $("#ddlCentroCustoSuperior").val(data.Data[0].IdCentroCusto);
                                            $("#ddlCentroCustoSuperior").trigger('change');
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });

            window.vm = vmPai;
            ko.applyBindings(vmPai, formElement[0]);
        });

        return request;
    };

    // retorna métodos essenciais
    // obrigatórios: get, bind
    return {
        bind: bind,
        routeOptions: { showSaveNew: true }
    };

});