﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (classeContabil, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            classeContabil.config({
                saveUrl: '/Contabilidade/ClasseContabil/NewEdit',
                getUrl: '/Contabilidade/ClasseContabil/NewEditJson'
            });

            var request = classeContabil.get({ id: id }),
                form = $('#form-ClasseContabil-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Contabilidade/ClasseContabil/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Classe Contábil salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});