﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (classificacao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            classificacao.config({
                saveUrl: '/Contabilidade/ClassificacaoFiscal/NewEdit',
                getUrl: '/Contabilidade/ClassificacaoFiscal/NewEditJson'
            });

            var request = classificacao.get({ id: id }),
                form = $('#form-FiscalClassificacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Contabilidade/ClassificacaoFiscal/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Classificacao salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});