﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (conta, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            conta.config({
                saveUrl: '/Contabilidade/ContaLancamentoContabil/NewEdit',
                getUrl: '/Contabilidade/ContaLancamentoContabil/NewEditJson',
            });
            var request = conta.get({ id: id }),
                 form = $('#form-ContaLancamentoContabil-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //Regra para preenchimento automático dos campos de Visão Gerencial
                vm.IdPlanoContasContaDebitoFiscal.subscribe(function (newValue) {
                    vm.IdPlanoContasContaDebitoGerencial(newValue);
                });

                vm.IdPlanoContasContaCreditoFiscal.subscribe(function (newValue) {
                    vm.IdPlanoContasContaCreditoGerencial(newValue);
                });

                vm.Save = function () {
                    vm.ParentId(Route.routeOptions.parentId);
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        conta.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Conta para Lançamento salvo com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }

});