﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (documentoTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            documentoTipo.config({
                saveUrl: '/Contabilidade/DocumentoTipo/NewEdit',
                getUrl: '/Contabilidade/DocumentoTipo/NewEditJson'
            });

            var request = documentoTipo.get({ id: id }),
                form = $('#form-DocumentoTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    documentoTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Documento salvo com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});