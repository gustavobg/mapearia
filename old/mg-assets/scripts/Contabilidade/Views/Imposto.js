﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (imposto, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            imposto.config({
                saveUrl: '/Contabilidade/Imposto/NewEdit',
                getUrl: '/Contabilidade/Imposto/NewEditJson'
            });

            var request = imposto.get({ id: id }),
                form = $('#form-FiscalImposto-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                if (vm.ImpostoCategoria() != '1')
                {
                    vm.ImpostoOrigem(null);
                    vm.ImpostoRetidoEntrada(false);
                    vm.ImpostoRetidoSaida(false);
                    vm.ImpostoSobre(null);
                }

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    imposto.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Imposto cadastrado com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});