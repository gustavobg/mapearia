﻿define(['../Controllers/NCM', 'jquery', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'datetimepicker', 'ko-validate-rules', 'ko-validate', 'vanillaUniform'], function (ncm, $, ko, mgFeedbackBase, toastr) {
    bind = function (id) {

        var vmPai = { FiscalNCM: ko.observable(null) },
            request = ncm.get({ id: id }),
            formId = 'form-FiscalNCM-NewEdit',
            formElement = $('#' + formId);

        request.done(function (response) {

            vmPai.FiscalNCM(ko.mapping.fromJS(response));
            vm = vmPai.FiscalNCM(); // simplifica acesso a vm           

            vm.Save = function () {
                if (!vm.isValidShowErrors()) { return; }

                vmPai.FiscalNCM().SaveNewCrud(Route.saveNew());

                var data = ko.toJSON(vmPai.FiscalNCM());

                ncm.save(data).done(function (result, status, xhr) {
                    mgFeedbackBase.feedbackCrudRoute(result, 'NCM salvo com sucesso.', !(Route.saveNew()), function () {
                        // refresh binds
                        if (Route.saveNew() === true) {
                            vmPai.FiscalNCM(ko.mapping.fromJS(result));
                            // Route.quiet true ignore default actions after route change                    
                            Route.quiet = true;
                            Route.redirectRouteAction('New');
                            Route.saveNew(true);
                            Route.quiet = false;
                            // TODO: verificar bug velocity e remover essa linha
                            formElement.find('.panel-action').not('.panel-selected').velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 500);
                        }
                    });
                });
            };

            window.vm = vmPai;
            ko.applyBindings(vmPai, formElement[0]);
        });

        return request;
    };

    // retorna métodos essenciais
    // obrigatórios: get, bind
    return {
        bind: bind,
        routeOptions: { showSaveNew: true }
    };

});