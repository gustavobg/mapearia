﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (objetoTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            objetoTipo.config({
                saveUrl: '/Contabilidade/ObjetoTipo/NewEdit',
                getUrl: '/Contabilidade/ObjetoTipo/NewEditJson'
            });

            var request = objetoTipo.get({ id: id }),
                form = $('#form-ObjetoTipoContabil-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    objetoTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Objeto Contábil salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});