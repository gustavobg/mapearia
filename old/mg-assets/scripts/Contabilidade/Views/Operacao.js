﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (operacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            operacao.config({
                saveUrl: '/Contabilidade/Operacao/NewEdit',
                getUrl: '/Contabilidade/Operacao/NewEditJson'
            });

            var request = operacao.get({ id: id }),
                form = $('#form-Operacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    operacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Operação Salva com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});