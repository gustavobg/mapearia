﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (configuracao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            configuracao.config({
                saveUrl: '/Contabilidade/ParametroclasseContabil/NewEdit',
                getUrl: '/Contabilidade/ParametroclasseContabil/NewEditJson',
            });
            var request = configuracao.get({ id: id }),
                 form = $('#form-ParametroLancamentoContabil-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                var vmDirecionadorLancamentoExtend = function (vm) {
                    ko.computed(function () {
                        //Regra: limpar campos caso Permite Direcionador == true
                        if (vm.PermiteDirecionador() == true) {
                            vm.IdsPessoaPerfil(null);
                            vm.IdsFinanceiroEntidade(null);
                            vm.IdsProdutoServicoGrupo(null);
                            vm.IdsCategoriaServicoOrdem(null);
                            vm.IdsDocumentoTipo(null);
                            vm.DescricaoExibicao("Demais");
                            vm.Demais(true);
                        }
                        else
                        {                            
                            vm.Demais(false);
                        }

                    });
                };
                window.vmDirecionadorLancamentoExtend = vmDirecionadorLancamentoExtend;

                vm.Save = function () {
                    //vm.IdProducaoCultura(Route.routeOptions.parentId);
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        configuracao.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Configuração salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }

});






















//define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'gridview', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'form-submit'], function (classeContabil, ko, mgFeedbackBase, toastr) {

//    var vm = {},
//        bind = function (id) {
//            classeContabil.config({
//                saveUrl: '/Contabilidade/ParametroClasseContabil/NewEdit',
//                getUrl: '/Contabilidade/ParametroClasseContabil/NewEditJson'
//            });

//            var request = classeContabil.get({ id: id }),
//                form = $('#form-ParametroLancamentoContabil-NewEdit');

//            request.done(function (response) {
//                vm = ko.mapping.fromJS(response);

//                var vmDirecionadorLancamentoExtend = function (vm) {

//                    ko.computed(function () {

//                        //vm.KO = "Teste";
//                        //Regra: limpar campos caso Permite Direcionador == true
//                        if (vm.PermiteDirecionador()) {
//                            vm.IdsPessoaPerfil(null);
//                            vm.IdsFinanceiroEntidade(null);
//                            vm.IdsProdutoServicoGrupo(null);
//                            vm.IdsCategoriaServicoOrdem(null);
//                            vm.IdsDocumentoTipo(null);
//                        }
//                    });
//                };
//                window.vmDirecionadorLancamentoExtend = vmDirecionadorLancamentoExtend;

//                vm.Save = function () {
//                    $.ajaxJsonAntiforgery(form, {
//                        data: ko.toJSON(vm),
//                        url: '/Contabilidade/ParametroClasseContabil/NewEdit/',
//                        success: function (result) {
//                            mgFeedbackBase.feedbackCrudRoute(result, 'XXX salva com sucesso.');
//                        }
//                    });
//                };

//                window.vm = vm;
//                ko.applyBindings(vm, form[0]);
//            });

//            return request;
//        };

//    return {
//        bind: bind
//    }
//});