﻿define(['../Controllers/PlanoContas', 'jquery', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'datetimepicker', 'ko-validate-rules', 'ko-validate', 'vanillaUniform'], function (plano, $, ko, mgFeedbackBase, toastr) {
    bind = function (id) {

        var vmPai = { PlanoContas: ko.observable(null) },
            request = plano.get({ id: id }),
            formId = 'form-PlanoContas-NewEdit',
            formElement = $('#' + formId);

        request.done(function (response) {

            vmPai.PlanoContas(ko.mapping.fromJS(response));
            vm = vmPai.PlanoContas(); // simplifica acesso a vm           

            vm.Save = function () {
                if (!vm.isValidShowErrors()) { return; }

                vmPai.PlanoContas().SaveNewCrud(Route.saveNew());

                var data = ko.toJSON(vmPai.PlanoContas());

                plano.save(data).done(function (result, status, xhr) {
                    mgFeedbackBase.feedbackCrudRoute(result, 'Plano de Contas salvo com sucesso.', !(Route.saveNew()), function () {
                        // refresh binds
                        if (Route.saveNew() === true) {
                            vmPai.PlanoContas(ko.mapping.fromJS(result));
                            // Route.quiet true ignore default actions after route change                    
                            Route.quiet = true;
                            Route.redirectRouteAction('New');
                            Route.saveNew(true);
                            Route.quiet = false;
                            // TODO: verificar bug velocity e remover essa linha
                            formElement.find('.panel-action').not('.panel-selected').velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 500);
                        }
                    });
                   
                });
            };

            vmPai.PlanoContas().AplicarMascara = ko.computed(function () {
                if (vmPai.PlanoContas().IdPlanoContasVersao() != null) {
                    $.ajax({
                        url: '/Contabilidade/VersaoPlanoContas/List',
                        type: 'post',
                        dataType: 'json',
                        data: JSON.stringify({
                            IdPlanoContasVersao: vmPai.PlanoContas().IdPlanoContasVersao(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        }),
                        async: false,
                        success: function (data) {
                            vmPai.PlanoContas().Mascara(data.Data[0].Mascara);
                        }
                    });
                }
            });

            ko.computed(function () {
                if (vmPai.PlanoContas().Conta() != null)
                {
                    $.ajax({
                        url: '/Contabilidade/PlanoContas/RetornaContaSuperior',
                        type: 'post',
                        data: 'conta=' + vmPai.PlanoContas().Conta(),
                        async: false,
                        success: function (conta) {
                            if (conta != vmPai.PlanoContas().Conta() && conta != "") {
                                $.ajax({
                                    url: '/Contabilidade/PlanoContas/ListarHierarquicamentePorParametro',
                                    type: 'post',
                                    dataType: 'json',
                                    data: JSON.stringify({
                                        IdPlanoContasVersao: vmPai.PlanoContas().IdPlanoContasVersao(),
                                        Ativo: true,
                                        Conta: conta,
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Conta asc'
                                        }
                                    }),
                                    async: false,
                                    success: function (data) {
                                        $("#ddlPlanoContasSuperior").val(data.Data[0].IdPlanoContas);
                                        $("#ddlPlanoContasSuperior").trigger('change');
                                    }
                                });
                            }
                        }
                    });
                }
            });

            window.vm = vmPai;
            ko.applyBindings(vmPai, formElement[0]);
        });

        return request;
    };

    // retorna métodos essenciais
    // obrigatórios: get, bind
    return {
        bind: bind,
        routeOptions: { showSaveNew: true }
    };

});