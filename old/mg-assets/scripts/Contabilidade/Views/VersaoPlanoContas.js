﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'alteraCEP', 'button',  'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (planoContasVersao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            planoContasVersao.config({
                saveUrl: '/Contabilidade/VersaoPlanoContas/NewEdit',
                getUrl: '/Contabilidade/VersaoPlanoContas/NewEditJson'
            });

            var request = planoContasVersao.get({ id: id }),
                form = $('#form-PlanoContasVersao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Contabilidade/VersaoPlanoContas/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Versão salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});