﻿define(['util', 'jquery'], function (mg, $) {   

    var saveUrl = '/UsuarioPerfil/Salvar/',
        getUrl = '/UsuarioPerfil/UsuarioPerfilJson/',
        get = function (data) {
            return mg.getViewModel($.extend(true, { id: mg.getId(data), url: getUrl }, data));
        },
        save = function (data) {
            // deffered
            return $.post(saveUrl, { json: data });
        };

    return {
        get: get,
        save: save
    }

});