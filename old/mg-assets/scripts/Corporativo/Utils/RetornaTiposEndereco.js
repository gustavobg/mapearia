﻿define(['knockout'], function (ko) {
    "use strict";

    // binding handler para retornar tipos de endereco
    ko.bindingHandlers.retornaTiposEnderecoText = {
        update: function (element, valueAccessor) {
            var tiposEndereco = ['Sede ou domícílio', 'Cobrança', 'Entrega ou Recebimento'],
                idsTipoEndereco = valueAccessor().idsTipoEndereco,
                textoConcatenadoArr = [];

            ko.utils.arrayForEach(idsTipoEndereco, function (id) {
                textoConcatenadoArr.push(tiposEndereco[Number(id) + -1]);
            });
            ko.applyBindingsToNode(element, { text: textoConcatenadoArr.length > 0 ? textoConcatenadoArr.join(', ') : '' });
        }
    };
});