﻿/// <reference path="/App_Assets/publish/scripts/debug/require.js" data-main="/mg-assets/scripts/require-config-in.js" />
define(['knockout', 'feedback', '../Views/AprovacaoProcessoAtividadeAprovacoes', '../Views/AprovacaoProcessoAtividadeConfirma', 'bootstrap/modal', 'jquery-flipper', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (ko, mgFeedbackBase, aprovacaoModalObj, aprovacaoModalConfirmaObj) {
    "use strict";
    // aprovacaoProcessoAtividade
    var bind = function (routeUrl) {
            var aprovacaoModal = new aprovacaoModalObj('modal-AprovacaoProcessoAtividadeNewEdit'),
                aprovacaoModalConfirma = new aprovacaoModalConfirmaObj('modal-AprovacaoProcessoConfirmaNewEdit'),
                documentosRota = routeUrl.split(','),
                documentos = [],
                IdAprovacaoProcessoAtividade = null,
                IdAprovacaoProcessoAtividadeSequencia = null,
                getObjetoRotaAprovacao = function (routeUrl) {
                    // o id será a url da rota do documento
                    // ex: |1|2|Financeiro|ApuracaoPagamento|1|12+|Financeiro|ApuracaoPagamento|1|12|Financeiro|ApuracaoPagamento|1|12
                    var arr = routeUrl.replace(/^\|/gi, '').split('|'),
                        id = null,
                        parentId = null,
                        modulo = null,
                        area = null,
                        url = null;

                    IdAprovacaoProcessoAtividade = arr[0];
                    IdAprovacaoProcessoAtividadeSequencia = arr[1];

                    if (arr.length >= 4) {
                        debugger;
                        id = arr[4];
                        modulo = arr[2];
                        area = arr[3];
                        url = '/' + modulo + '/' + area + '/NewEdit';
                    } else {
                        throw 'Parâmetro de rota "' + routeUrl + '" inválido';
                    }
                    if (arr.length == 6 || arr.length > 5) {
                        // has parentId
                        id = arr[5];
                        parentId = arr[4];
                        url += '?parentId=' + parentId;
                        Route.routeOptions.parentId = parentId;
                    }
                    return { id: id, modulo: modulo, area: area, getUrl: url, idAprovacaoProcessoAtividade: IdAprovacaoProcessoAtividade, idAprovacaoProcessoAtividadeSequencia: IdAprovacaoProcessoAtividadeSequencia };
                };

            documentosRota.forEach(function (documento) {
                documentos.push(getObjetoRotaAprovacao(documento));
            });

            var request = $.Deferred(),
                vmAprova = {
                    IdAprovacaoProcessoAtividade: IdAprovacaoProcessoAtividade,
                    IdAprovacaoProcessoAtividadeSequencia: IdAprovacaoProcessoAtividadeSequencia,
                    Aprovado: true, // aprovar : reprovar
                    Confirmar: function (situacao) {
                        var paramAprovacao = {
                            IdAprovacaoProcessoAtividade: IdAprovacaoProcessoAtividade,
                            IdAprovacaoProcessoAtividadeSequencia: IdAprovacaoProcessoAtividadeSequencia,
                            Aprovado: situacao
                        };
                        vmAprova.Aprovado = situacao;
                        aprovacaoModalConfirma.exibeConfirmacao(paramAprovacao);
                    }
                };

            if (documentos.length > 0) {
                var documento = documentos[0];
                // TODO: Para rota de produtos, ou categorias, será implementado no html da view (routeOptions, um índice com os nomes dos títulos que serão utilizados na aba)
                // injeta view do documento
                $.get(documento.getUrl, function (htmlResponse) {
                    $('#documento-AprovacaoProcessoAtividade').html(htmlResponse);
                    // retorna controller do documento
                    require([documento.modulo + '/Views/' + documento.area], function (documentoView) {
                        documentoView.bind(documento.id).done(function () {
                            request.resolve();
                        });
                    });
                });
                if (documentos.length > 1) {
                    documentos.shift(); // remove a rota já injetada do loop
                }
            };

            vmAprova.exibeAprovacoes = function () {
                aprovacaoModal.exibeAprovacoes(ko.unwrap(vmAprova.IdAprovacaoProcessoAtividade));
            };

            ko.applyBindings(vmAprova, document.getElementById('AprovacaoProcessoAtividade-NewEdit'));

            return request.promise();
        };


    return {
        bind: bind,
        routeOptions: {
            showActionPanel: false,
            actionTitle: 'Aprovação'
        }
    }
});