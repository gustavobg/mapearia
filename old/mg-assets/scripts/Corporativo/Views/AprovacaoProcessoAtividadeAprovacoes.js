﻿define(['knockout', 'jquery', 'text!/Corporativo/AprovacaoProcessoAtividade/AprovacoesModal', 'toastr', 'gridview', 'bootstrap/modal'], function (ko, $, aprovacaoModalHtml, toastr) {
    var aprovacaoModal = function (idContainerModal) {
        // cria modal ao instanciar       
        var element = $('#' + idContainerModal).html(aprovacaoModalHtml).find('.modal').modal('hide'),
            vm = {
                ListaAprovacoes: ko.observableArray([]),
                IdAprovacaoProcessoAtividade: ko.observable(null)
            };

        ko.computed(function () {
            // ListAprovacoesPorAtividade
            var idAprovacaoProcessoAtividade = ko.unwrap(vm.IdAprovacaoProcessoAtividade);
            if (idAprovacaoProcessoAtividade !== null) {
                $.ajax({
                    url: 'Corporativo/AprovacaoProcessoAtividade/ListAprovacoesPorAtividade',
                    type: 'POST',
                    data: { 'IdAprovacaoProcessoAtividade': idAprovacaoProcessoAtividade }
                }).done(function (response) {                    
                    if (response !== null && Array.isArray(response)) {
                        vm.ListaAprovacoes(response);
                    } else {
                        toastr.error('Ocorreu um erro ao buscar as aprovações');
                    }
                });
            }
        });

        ko.applyBindings(vm, document.getElementById(idContainerModal));

        // exibe aprovações
        this.exibeAprovacoes = function (IdAprovacaoProcessoAtividade) {
            vm.IdAprovacaoProcessoAtividade(IdAprovacaoProcessoAtividade);
            element.modal('show');
        }
    }
    return aprovacaoModal;
});