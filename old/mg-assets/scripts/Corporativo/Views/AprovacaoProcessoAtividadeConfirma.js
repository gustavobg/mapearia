﻿define(['crud-controller', 'knockout', 'jquery', 'text!/Corporativo/AprovacaoProcessoAtividade/AprovacaoConfirmaModal', 'feedback', 'toastr', 'gridview', 'bootstrap/modal'], function (aprovacaoProcessoAtividade, ko, $, aprovacaoModalConfirmaHtml, mgFeedbackBase, toastr) {
    var aprovacaoModalConfirma = function (idContainerModal) {   
        // cria modal ao instanciar       
        var self = this,
            element = $('#' + idContainerModal).html(aprovacaoModalConfirmaHtml).find('.modal').modal('hide'),
            vmAprovacao = {
                IdAprovacaoProcessoAtividade: ko.observable(null),
                IdAprovacaoProcessoAtividadeSequencia: ko.observable(),
                Parecer: ko.observable(),
                Aprovado: ko.observable(true), // aprovar : reprovar
                IdUsuario: SessaoUsuario.IdUsuario,
                ConfirmaAprovacao: function () {
                    // salva aprovação ou reprovação
                    var data = ko.toJSON(vmAprovacao);

                    // set token
                    Route.setToken(element.find('input[name="__RequestVerificationToken"]')[0].value);

                    aprovacaoProcessoAtividade.config({
                        saveUrl: '/Corporativo/AprovacaoProcessoAtividade/AcaoAprovarReprovar' // método para aprovar/reprovar
                    });

                    aprovacaoProcessoAtividade.save(data).done(function (result, status, xhr) {                    
                        mgFeedbackBase.feedbackCrudRoute(result, 'Processo ' + (vmAprovacao.Aprovado ? 'Aprovado' : 'Reprovado') + ' com sucesso.', true, function () {
                            self.ocultaAprovacoes();
                        });
                    });
                }
            };

        ko.applyBindings(vmAprovacao, document.getElementById(idContainerModal));
        
        // exibe modal de confirmação de aprovação/reprovação
        this.exibeConfirmacao = function (paramAprovacao) {
            vmAprovacao.IdAprovacaoProcessoAtividade(paramAprovacao.IdAprovacaoProcessoAtividade);
            vmAprovacao.IdAprovacaoProcessoAtividadeSequencia(paramAprovacao.IdAprovacaoProcessoAtividadeSequencia);
            vmAprovacao.Parecer('');
            vmAprovacao.Aprovado(paramAprovacao.Aprovado); // aprovar : reprovar

            element.modal('show').on('shown.bs.modal', function () {
                element.find('#AprovacaoProcessoAtividade-parecer').focus();
            });
            element.modal('show');
        }
        this.ocultaAprovacoes = function () {
            element.modal('hide');
        }
    }
    return aprovacaoModalConfirma;
});