﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (aprovacaoProcessoConfig, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            aprovacaoProcessoConfig.config({
                saveUrl: '/Corporativo/AprovacaoProcessoConfiguracao/NewEdit',
                getUrl: '/Corporativo/AprovacaoProcessoConfiguracao/NewEditJson'
            });

            var request = aprovacaoProcessoConfig.get({ id: id }),
                form = $('#form-AprovacaoProcessoConfiguracao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                if (Route.routeOptions.parentId != null) {
                    vm.IdSubProcesso(Route.routeOptions.parentId);
                }

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    aprovacaoProcessoConfig.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração salva com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});