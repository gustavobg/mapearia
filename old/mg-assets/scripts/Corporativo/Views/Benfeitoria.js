﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'component.colorSelector'], function (benfeitoria, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            benfeitoria.config({
                saveUrl: '/Corporativo/Benfeitoria/NewEdit',
                getUrl: '/Corporativo/Benfeitoria/NewEditJson'
            });

            var request = benfeitoria.get({ id: id }),
                form = $('#form-Benfeitoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/Benfeitoria/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Benfeitoria salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});