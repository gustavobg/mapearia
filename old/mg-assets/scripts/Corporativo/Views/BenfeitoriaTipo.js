﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (benfeitoriaTipo, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            benfeitoriaTipo.config({
                saveUrl: '/Corporativo/BenfeitoriaTipo/NewEdit',
                getUrl: '/Corporativo/BenfeitoriaTipo/NewEditJson'
            });

            var request = benfeitoriaTipo.get({ id: id }),
                form = $('#form-BenfeitoriaTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);                
                vm.DadosComplTipoSuperior = ko.observable({ 'Ativo': true, 'IdBenfeitoriaTipoNotIn': vm.IdBenfeitoriaTipo(), 'UltimoNivel': false });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/BenfeitoriaTipo/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Benfeitoria salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});