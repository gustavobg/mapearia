﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (cnae, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            cnae.config({
                saveUrl: '/Corporativo/CNAE/NewEdit',
                getUrl: '/Corporativo/CNAE/NewEditJson'
            });

            var request = cnae.get({ id: id }),
                form = $('#form-Cnae-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);
                    cnae.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'CNAE salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});