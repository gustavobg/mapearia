﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (categoriaServico, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            categoriaServico.config({
                saveUrl: '/Corporativo/CategoriaServico/NewEdit',
                getUrl: '/Corporativo/CategoriaServico/NewEditJson'
            });

            var request = categoriaServico.get({ id: id }),
                form = $('#form-CategoriaServico-NewEdit');

            var getUnique = function (value, index, self) {
                return self.indexOf(value) === index;
            };

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.AtividadeEconomicaParam = ko.observableArray();
                vm.IdsAplicacoesTipoString = ko.observableArray();
                vm.dadosComplementaresLocalProducao = ko.observable();
                vm.dadosComplementaresAtividadeEconomicaEtapa = ko.observable();

                ko.computed(function () {
                    // TODO: Atividade economica trazer objetos selecionados, acumular os Id's da propriedade 'IdsAplicacoesTipo'
                    var idsAplicacoesTipo = [],
                        atividadeEconomica = ko.unwrap(vm.AtividadeEconomicaParam),
                        idsAplicacoesTipoConcat = [];

                    ko.utils.arrayForEach(atividadeEconomica, function (item) {
                        if (item.IdsAplicacoesTipo.length > 0) {
                            idsAplicacoesTipo = idsAplicacoesTipo.concat(item.IdsAplicacoesTipo.split(','));
                        }
                    });

                    idsAplicacoesTipo = idsAplicacoesTipo.filter(getUnique);
                    vm.IdsAplicacoesTipoString(idsAplicacoesTipo.join(','));

                }, this);

                //Preenchimento da propriedade dadosComplementaresAtividadeEconomicaEtapa
                ko.computed(function () {
                    if (vm.IdsAtividadeEconomica() != null)
                        vm.dadosComplementaresAtividadeEconomicaEtapa({ 'Ativo': true, 'IdAtividadeEconomicaIn': vm.IdsAtividadeEconomica() });
                    else {
                        debugger;
                        vm.dadosComplementaresAtividadeEconomicaEtapa({ 'Ativo': true });
                    }

                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/CategoriaServico/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Categoria Serviço salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                //Regra de Exibição Para o Tipo de AplicacaoTipo
                ko.computed(function () {
                    if (vm.TipoAplicacaoTipo() == '1') {
                        vm.IdsCriacaoTipo(null);  //Criação Tipo
                        vm.IdsAutomotivoParteEquipamento(null); //Parte Equipamento
                        vm.IdsProducaoLocalTipo(null); //Tipo Local Produção
                        vm.IdsProducaoUnidadeTipo(null); //Unidade Produção
                    }

                    if (vm.TipoAplicacaoTipo() == '2' || vm.TipoAplicacaoTipo() == '3') {
                        vm.IdsCriacaoTipo(null); //Criação Tipo
                        vm.IdsAutomotivoParteEquipamento(null); //Parte Equipamento
                        vm.dadosComplementaresLocalProducao({ 'Ativo': true });
                        if (vm.TipoAplicacaoTipo() == '2')
                            vm.IdsBenfeitoriaTipo(null); // Tipo de Benfeitoria
                    }
                    if (vm.TipoAplicacaoTipo() == '4' || vm.TipoAplicacaoTipo() == '5' || vm.TipoAplicacaoTipo() == '6') {
                        vm.IdsProducaoLocalTipo(null); //Tipo Local Produção
                        vm.IdsProducaoUnidadeTipo(null); //Unidade Produção
                        vm.IdsAutomotivoParteEquipamento(null); //Parte Equipamento
                        vm.IdsBenfeitoriaTipo(null); // Tipo de Benfeitoria
                    }
                    if (vm.TipoAplicacaoTipo() == '7') {
                        vm.IdsProducaoLocalTipo(null); //Tipo Local Produção
                        vm.IdsProducaoUnidadeTipo(null); //Unidade Produção
                        vm.IdsCriacaoTipo(null); //Criação Tipo
                        vm.IdsBenfeitoriaTipo(null); // Tipo de Benfeitoria
                    }

                    if (vm.TipoControleSubAplicacao() == '1') {
                        vm.IdsCriacaoCategoria(null);
                        vm.IdsProducaoUnidadeTipo(null); //Unidade Produção
                        vm.IdsAutomotivoParteEquipamento(null); //Parte Equipamento
                        vm.MultiAplicacao(false);
                    }
                });

                ko.computed(function () {
                    if (vm.TipoRestricaoServico() == 2) {
                        //vm.IdsProdutoServico(null);
                    }

                    if (vm.TipoRestricaoServico() == 3)
                        vm.IdsProdutoServicoGrupo(null);

                });
                ko.computed(function () {
                    if (vm.TipoDestinatario() != '1' && vm.TipoDestinatario() != '2')
                        vm.IdPessoaEmpresa(null);
                })
                ko.computed(function () {
                    if (vm.PermiteControleEtapa() == true)
                        vm.IdsProdutoServicoGrupo(null);
                });

            });

            return request;
        };

    return {
        bind: bind
    }
});