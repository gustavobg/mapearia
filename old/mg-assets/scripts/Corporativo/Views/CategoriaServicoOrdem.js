﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (ordem, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            ordem.config({
                saveUrl: '/Corporativo/CategoriaServicoOrdem/CategoriaServicoOrdem',
                getUrl: '/Corporativo/CategoriaServicoOrdem/CategoriaServicoOrdemJson',
            });

            var request = ordem.get({ id: id }),
                form = $('#form-CategoriaServicoOrdem-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    ordem.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração de Ordem salva com sucesso.'); 
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                vm.PermiteEmissaoOrdem.subscribe(function (newValue) {
                    if (newValue == false) {
                        //vm.TipoOrdem(null);
                        vm.TipoDocumento(null);
                        vm.Prefixo(null);
                        vm.TipoDescricao(null);
                        vm.TipoPeriodoProgramacao(null);
                    }
                });
                ko.computed(function () {
                    if (vm.TipoDescricao() == '2' || vm.TipoDescricao() == '3')
                        vm.TipoDescricaoOrdem(1);
                }); //Regra de exibição de Campos Descrição de uma ordem e Descrição da Ordem
            });
            return request;
        };
    return {
        bind: bind
    };
});