﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (classePatrimonial, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            classePatrimonial.config({
                saveUrl: '/Corporativo/ClassePatrimonial/NewEdit',
                getUrl: '/Corporativo/ClassePatrimonial/NewEditJson'
            });

            var request = classePatrimonial.get({ id: id }),
                form = $('#form-ClassePatrimonial-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.DadosComplementares = ko.observable({'Ativo':true, 'UltimoNivel': false, 'IdClassePatrimonialNotIn': vm.IdClassePatrimonial() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/ClassePatrimonial/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Classe Patrimônial salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});