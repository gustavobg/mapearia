﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask'], function (empresa, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            empresa.config({
                saveUrl: '/Corporativo/Empresa/NewEdit',
                getUrl: '/Corporativo/Empresa/NewEditJson',
            });
            var request = empresa.get({ id: id }),
                form = $('#form-CorporativoEmpresa-NewEdit');

            //Função une os arrays e ordena.
            var concatArraysUniqueWithSort = function (thisArray, otherArray) {
                var newArray = thisArray.concat(otherArray).sort(function (a, b) {
                    return a > b ? 1 : a < b ? -1 : 0;
                });

                return newArray.filter(function (item, index) {
                    return newArray.indexOf(item) === index;
                });
            };

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.EmpresaGrupoDescricao = ko.observable();

                vm.IdEmpresaGrupo.subscribe(function (newValue) {
                    if (newValue != null) {
                        if (confirm("Deseja Importar os dados do Grupo selecionado ?")) {

                            $.ajax({
                                url: '/Corporativo/EmpresaGrupo/NewEditJson',
                                type: 'post',
                                dataType: 'json',
                                data: 'id=' + newValue,
                                async: false,
                                success: function (data) {

                                    if (data != null) {

                                        //Busca Grupo Empresa com medelo
                                        $.each(data.Conteudos, function (data, empresaGrupo) {

                                            //realiza Verificação da tela + Empresa modelo
                                            ko.utils.arrayForEach(vm.Modelos(), function (empresaAtual) {

                                                if (Number(empresaAtual.IdTela()) == Number(empresaGrupo.IdTela)) {

                                                    var arrModeloEmpresaAtualId = [];
                                                    var arrModeloEmpresaGrupoId = [];

                                                    var arrModeloEmpresaAtualNome = [];
                                                    var arrModeloEmpresaGrupoNome = [];

                                                    if (empresaAtual.IdsEmpresa() != null && empresaAtual.IdsEmpresa().lenght > 0) {
                                                        arrModeloEmpresaAtualId = empresaAtual.IdsEmpresa().split(',');
                                                        arrModeloEmpresaAtualNome = empresaAtual.Modelo().split(',');
                                                    }

                                                    if (empresaGrupo.IdsEmpresa != null || empresaGrupo.IdsEmpresa.lenght > 0) {
                                                        arrModeloEmpresaGrupoId = empresaGrupo.IdsEmpresa.split(',');
                                                        arrModeloEmpresaGrupoNome = empresaGrupo.Modelo.split(',');
                                                    }

                                                    var x = concatArraysUniqueWithSort(arrModeloEmpresaAtualId, arrModeloEmpresaGrupoId);
                                                    var y = concatArraysUniqueWithSort(arrModeloEmpresaAtualNome, arrModeloEmpresaGrupoNome);

                                                    empresaAtual.IdsEmpresa(x.join(","));
                                                    empresaAtual.Modelo(y.join(","));
                                                }

                                            });
                                        });
                                    }
                                }
                            });
                        }
                    }
                    else {
                        alert('A remoção do Grupo implicará em não receber mais atualizações para a Empresa ' + vm.Nome())
                    }
                });

                var ModeloConteudoExtend = function (vm) {
                    vm.EmpresaModeloDadosCompl = ko.observable({ 'Ativo': true, 'EmpresaModelo': true, 'IdEmpresaNotIn': id });
                }
                window.ModeloConteudoExtend = ModeloConteudoExtend;

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);
                    empresa.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Empresa salva com sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});