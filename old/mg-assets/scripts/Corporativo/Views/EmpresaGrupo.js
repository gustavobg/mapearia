﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask'], function (empresaGrupo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            empresaGrupo.config({
                saveUrl: '/Corporativo/EmpresaGrupo/NewEdit',
                getUrl: '/Corporativo/EmpresaGrupo/NewEditJson',
            });
            var request = empresaGrupo.get({ id: id }),
                form = $('#form-CorporativoEmpresaGrupo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    empresaGrupo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Grupo de Empresa salvo com sucesso.');
                    });
                };
                
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});