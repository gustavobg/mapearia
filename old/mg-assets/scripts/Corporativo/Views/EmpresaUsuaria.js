﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'alteraCEP', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate', '../Utils/RetornaTiposEndereco'], function (empresaUsuaria, mg, ko, mgFeedbackBase, toastr) {
    
    var vm = {},
        bind = function (id) {
            empresaUsuaria.config({
                saveUrl: '/Corporativo/EmpresaUsuaria/NewEdit',
                getUrl: '/Corporativo/EmpresaUsuaria/NewEditJson',
            });
            var request = empresaUsuaria.get({ id: id }),
                form = $('#form-CorporativoPessoa-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.PessoaNatureza = {
                    PessoaFisica: 1,
                    PessoaJuridica: 2
                };
                vm.ValidarDocumento = function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica) {
                        return { cpf: true };
                    }
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica) {
                        return { cnpj: true };
                    }
                };
                vm.Save = function () {
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        empresaUsuaria.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Empresa usuária salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                EnderecoExtend = function (vm) {
                    this.Endereco = ko.observable(null);
                    this.ArrTextoTipoEndereco = ko.observableArray([]);
                    this.Sigla = ko.pureComputed(function () {
                        var Endereco = this.Endereco();
                        if (Endereco && Endereco.hasOwnProperty('Sigla'))
                            return Endereco.Sigla;
                    }, this);
                };

                vm.LabelNome = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "Nome";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Razão Social";
                });
                vm.LabelNomeSecundario = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "Apelido";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Nome Fantasia";
                });
                vm.LabelDocumento1 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "CPF";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "CNPJ";
                });
                vm.LabelDocumento2 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "RG";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Insc. Estadual";
                });
                vm.LabelDocumento3 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "CNH";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Insc. Municipal";
                });
                vm.DocumentoMaskOptions = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return defaults.mask.cpf;
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return defaults.mask.cnpj;
                });

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                window.EnderecoExtend = EnderecoExtend;

                ko.applyBindingsToNode(document.getElementById('pnlMaisCampos'), { collapsible: { vmArray: [vm.Documento1(), vm.Documento2(), vm.Documento3()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDadosAdicionais'), { collapsible: { vmArray: [vm.Telefone1(), vm.Telefone2(), vm.Email(), vm.Site(), vm.Data()] } });
                ko.applyBindingsToNode(document.getElementById('pnlEnderecos'), { collapsible: { vmArray: [vm.Enderecos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlContatos'), { collapsible: { vmArray: [vm.Contatos()] } });

                ko.applyBindings(vm, form[0]);
            });
            
            return request;
        };

    return {
        bind: bind
    }

});