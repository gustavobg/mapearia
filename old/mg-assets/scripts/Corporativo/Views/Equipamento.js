﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (equipamento, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            equipamento.config({
                saveUrl: '/Corporativo/CategoriaServicoEquipamento/Equipamento',
                getUrl: '/Corporativo/CategoriaServicoEquipamento/EquipamentoJson',
            });

            var request = equipamento.get({ id: id }),
                form = $('#form-CategoriaServicoEquipamento-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.TipoUtilizacaoEquipamento() == '1') {
                        vm.DiretamenteOrdem(false);
                        vm.InformeServico(false);
                        vm.TipoApontamentoOperador(null);
                        vm.TipoRestricaoEquipamento(null);
                        vm.IdsAutomotivoEquipamento(null);
                        vm.IdsAutomotivoTipoEquipamento(null);
                        vm.TipoControleExecucao(null);
                    }

                });
                ko.computed(function () {
                    if (vm.TipoRestricaoEquipamento() == '2') {
                        vm.IdsAutomotivoEquipamento(null);
                    }
                    if (vm.TipoRestricaoEquipamento() == '3') {
                        vm.IdsAutomotivoTipoEquipamento(null);
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    equipamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração para Equipamentos salvo com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });
            return request;
        };
    return {
        bind: bind
    };
});