﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (grupoResponsabilidade, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            grupoResponsabilidade.config({
                saveUrl: '/Corporativo/GrupoResponsabilidade/NewEdit',
                getUrl: '/Corporativo/GrupoResponsabilidade/NewEditJson'
            });

            var request = grupoResponsabilidade.get({ id: id }),
                form = $('#form-GrupoResponsabilidade-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    grupoResponsabilidade.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Grupo de Responsabilidade salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});