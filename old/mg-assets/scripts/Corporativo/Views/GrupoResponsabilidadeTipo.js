﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (grupoResponsabilidadeTipo, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            grupoResponsabilidadeTipo.config({
                saveUrl: '/Corporativo/GrupoResponsabilidadeTipo/NewEdit',
                getUrl: '/Corporativo/GrupoResponsabilidadeTipo/NewEditJson'
            });

            var request = grupoResponsabilidadeTipo.get({ id: id }),
                form = $('#form-GrupoResponsabilidadeTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    grupoResponsabilidadeTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});