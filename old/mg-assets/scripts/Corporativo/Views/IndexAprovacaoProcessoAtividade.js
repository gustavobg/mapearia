﻿define(['knockout', 'jquery', '../Views/AprovacaoProcessoAtividadeAprovacoes', '../Views/AprovacaoProcessoAtividadeConfirma'], function (ko, $, aprovacaoModalObj, aprovacaoModalConfirmaObj) {
    var bind = function () {      
        // get context and append a function
        var bindContext = document.getElementById('filtro-grade'),
            aprovacaoModal = new aprovacaoModalObj('AprovacaoProcessoAtividadeIndex-aprovacao'),
            aprovacaoModalConfirma = new aprovacaoModalConfirmaObj('AprovacaoProcessoAtividadeConfirmaIndex-aprovacao');

        bindContext = ko.contextFor(bindContext)['$data']['indexVM'];
        bindContext.exibeAprovacoes = function (ctx) {
            aprovacaoModal.exibeAprovacoes(ctx.IdAprovacaoProcessoAtividade);
        };
        bindContext.aprovaProcesso = function (ctx) {
            var paramAprovacao = {
                IdAprovacaoProcessoAtividade: ctx.IdAprovacaoProcessoAtividade,
                IdAprovacaoProcessoAtividadeSequencia: ctx.IdAprovacaoProcessoAtividadeSequencia,
                Aprovado: true
            };
            aprovacaoModalConfirma.exibeConfirmacao(paramAprovacao);
        };
        bindContext.reprovaProcesso = function (ctx) {           
            var paramAprovacao = {
                IdAprovacaoProcessoAtividade: ctx.IdAprovacaoProcessoAtividade,
                IdAprovacaoProcessoAtividadeSequencia: ctx.IdAprovacaoProcessoAtividadeSequencia,
                Aprovado: false
            };
            aprovacaoModalConfirma.exibeConfirmacao(paramAprovacao);
        }
    }
    return { bind: bind };    
});