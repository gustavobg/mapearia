﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (maodeobra, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            maodeobra.config({
                saveUrl: '/Corporativo/CategoriaServicoMaoDeObra/MaoDeObra',
                getUrl: '/Corporativo/CategoriaServicoMaoDeObra/MaoDeObraJson',
            });

            var request = maodeobra.get({ id: id }),
                form = $('#form-CategoriaServicoMaoDeObra-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    maodeobra.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração para Mão de Obra salva com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                ko.computed(function () {
                    if(vm.TipoUtilizacaoMaoDeObra() == '1')
                    {
                        vm.DiretamenteOrdem(false);
                        vm.InformeServico(false);
                        vm.TipoRestricaoMaoDeObra(1);
                        vm.TipoControleExecucao(1);
                    }

                })

            });
            return request;
        };
    return {
        bind: bind
    };
});