﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (materiaPrima, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            materiaPrima.config({
                saveUrl: '/Corporativo/CategoriaServicoMateriaPrima/MateriaPrima',
                getUrl: '/Corporativo/CategoriaServicoMateriaPrima/MateriaPrimaJson',
            });

            var request = materiaPrima.get({ id: id }),
                form = $('#form-CategoriaServicoMateriaPrima-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.TipoUtilizacaoMateriaPrima() == '1') {
                        vm.DiretamenteOrdem(false);
                        vm.InformeServico(false);
                        vm.ReservaMateriaPrima(false);

                        //Box abaixo (Detalhes de Utilização)
                        vm.TipoUsoMateriaPrima('1');
                        vm.TipoRestricaoMateriaPrima('1');
                        //Produtos (Por enquanto, não foi feito ... será que vou ter que fazer ??? 10/12/2015)
                    }
                });

                if (vm.TipoRestricaoMateriaPrima() == '1' || vm.TipoRestricaoMateriaPrima() == '3') {
                    vm.IdsProdutoServicoGrupo('');
                }

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    materiaPrima.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração para Matéria Prima salva com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });
            return request;
        };
    return {
        bind: bind
    };
});