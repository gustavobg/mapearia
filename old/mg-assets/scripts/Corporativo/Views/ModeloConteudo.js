﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (modeloConteudo, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            modeloConteudo.config({
                saveUrl: '/Corporativo/ModeloConteudo/NewEdit',
                getUrl: '/Corporativo/ModeloConteudo/NewEditJson'
            });

            var request = modeloConteudo.get({ parentId: Route.routeOptions.parentId, id: id }),
                form = $('#form-ModeloConteudo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.EmpresaModeloDadosCompl = ko.observable({'Ativo':true, 'EmpresaModelo':true, 'IdEmpresaNotIn': vm.IdEmpresa });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/ModeloConteudo/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Modelo de Conteúdo salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});