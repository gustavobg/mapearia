﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'alteraCEP', 'button', 'vanillaUniform', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (ordem, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            // configura controller genérica de CRUD
            ordem.config({
                saveUrl: '/Corporativo/Ordem/NewEdit',
                getUrl: '/Corporativo/Ordem/NewEditJson'
            });

            var request = ordem.get({ id: id }),
                form = $('#form-Ordem-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.dadosComplementaresCategoriaServico = ko.observable({ 'Ativo': true, 'IdUsuarioNotIn': vm.IdUsuarioLogado() != null ? vm.IdUsuarioLogado() : null, 'IdEmpresaUsuariaLogada': vm.IdEmpresaUsuariaLogada() != null ? vm.IdEmpresaUsuariaLogada() : null });
                vm.dadosComplementaresEmpresaUsuariaEmitente = ko.observable();
                vm.dadosComplementaresEmpresaUsuariaDestinataria = ko.observable();
                vm.dadosComplementaresAtividadeEconomicaPeriodo = ko.observable();
                vm.dadosComplementaresAplicacao = ko.observable();
                vm.dadosComplementaresSubAplicacao = ko.observable();
                vm.configuracaoOrdemServico = ko.observable(null);
                vm.dadosComplementaresAtividadeEconomicaEtapa = ko.observable();
                vm.dadosComplementaresProdutoServico = ko.observable();

                //Propriedades para visualização e obrigatoriedade de Campos
                vm.DescricaoObrigatoria = ko.observable(false);

                vm.ResponsavelVisivel = ko.observable(false);
                vm.ResponsavelObrigatorio = ko.observable(false);

                vm.ExibeDataInicioEstimada = ko.observable(false);
                vm.ExibeDataTerminoEstimada = ko.observable(false);
                vm.ExibeDataInicioRealizado = ko.observable(false);
                vm.ExibeDataTerminoRealizado = ko.observable(false);

                vm.ExibePeriodoAtividadeEconomica = ko.observable(false);

                vm.ExibeSubAplicacao = ko.observable(false);
                vm.SubAplicacaoObrigatoria = ko.observable(false);

                //CategoriaServicoVM e DescricaoFormaApropriacaoCusto
                var vmParam = function () {
                    this.CategoriaServicoVM = ko.observable();
                    ko.computed(function () {
                        var categoriaServicoVM = this.CategoriaServicoVM();
                    }, this);
                };
                vmParam = new vmParam();
                window.vmParam = vmParam;

                //CategoriaServicoVM, Configrações da Ordem, utilização da propriedade e regras.
                ko.computed(function () {
                    if (vmParam.CategoriaServicoVM() != null) {
                        //Busca Configurações da Ordem [Categoria de Servico >> Ordem de Servico]
                        $.ajax({
                            type: 'POST',
                            url: '/Corporativo/CategoriaServicoOrdem/CategoriaServicoOrdemJson/' + vmParam.CategoriaServicoVM().IdCategoriaServico,
                            success: function (response) {
                                if (response != null) {
                                    console.log(response);
                                    vm.configuracaoOrdemServico(response);
                                }
                            },
                            async: false
                        });

                        //Dados Complementares para o Modal de Sequencia, Atvidade e Serviço
                        vm.dadosComplementaresProdutoServico({ 'IdProdutoServicoIn': vmParam.CategoriaServicoVM().IdsProdutoServicoRestrito, 'IdProdutoServicoGrupoIn':vmParam.CategoriaServicoVM().IdsProdutoServicoGrupo });
                        vm.dadosComplementaresAtividadeEconomicaEtapa({ 'IdAtividadeEconomicaEtapaIn': vmParam.CategoriaServicoVM().IdsAtividadeEconomicaEtapa });

                        vm.DescricaoFormaApropriacaoCusto(vmParam.CategoriaServicoVM().DescricaoTipoApropriacaoCusto);

                        //Restrição para Empresa Emitente
                        vm.dadosComplementaresEmpresaUsuariaEmitente({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaNotIn': vmParam.CategoriaServicoVM().IdsEmpresaEmissora != null ? vmParam.CategoriaServicoVM().IdsEmpresaEmissora : null });

                        //Regra para exibição do seletor de Período de Atividade Econômica
                        if (vmParam.CategoriaServicoVM().IdsAtividadeEconomica != null) {
                            vm.ExibePeriodoAtividadeEconomica(true);
                            vm.dadosComplementaresAtividadeEconomicaPeriodo({ 'Ativo': true, 'IdAtividadeEconomicaIn': vmParam.CategoriaServicoVM().IdsAtividadeEconomica, 'UltimoNivel': true });
                        }

                        //Exibição e obrigatoriedade da Aplicação
                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 1)//Benfeitoria
                        {
                            vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '1', 'IdBenfeitoriaTipoIn': vmParam.CategoriaServicoVM().IdsBenfeitoriaTipo });
                            vm.SubAplicacaoObrigatoria(false);
                            vm.ExibeSubAplicacao(false);
                        }

                        //Benfeitoria e/ou Local de Produção
                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 2 || vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 3) {

                            if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 3)
                                vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdProducaoLocalTipoIn': vmParam.CategoriaServicoVM().IdsProducaoLocalTipo, 'IdAplicacaoTipoIn': '1,2', 'IdBenfeitoriaTipoIn': vmParam.CategoriaServicoVM().IdsBenfeitoriaTipo });
                            else
                                vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdProducaoLocalTipoIn': vmParam.CategoriaServicoVM().IdsProducaoLocalTipo, 'IdAplicacaoTipoIn': '2' });

                            //if (vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 1) {
                            //    vm.ExibeSubAplicacao(false);
                            //    vm.SubAplicacaoObrigatoria(false);
                            //}
                            //else if (vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 2) {
                            //    vm.ExibeSubAplicacao(true);
                            //    vm.SubAplicacaoObrigatoria(false);

                            //    vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdProducaoUnidadeTipoIn': vmParam.CategoriaServicoVM().IdsProducaoUnidadeTipo, 'IdAplicacaoTipoIn': vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 2 ? '2' : '1,2' });
                            //}
                            //else if (vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 3) {
                            //    vm.ExibeSubAplicacao(true);
                            //    vm.SubAplicacaoObrigatoria(true);

                            //    vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdProducaoUnidadeTipoIn': vmParam.CategoriaServicoVM().IdsProducaoUnidadeTipo, 'IdAplicacaoTipoIn': vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 2 ? '2' : '1,2' });
                            //}
                            //else {
                            //    vm.dadosComplementaresSubAplicacao({ 'Ativo': true });
                            //}
                        }

                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 4)//Lote (Tipo de Criação)
                        {
                            vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '3', 'IdCriacaoTipoIn': vmParam.CategoriaServicoVM().IdsCriacaoTipo });
                            vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '3', 'IdCriacaoCategoriaIn': vmParam.CategoriaServicoVM().IdsCriacaoCategoria });
                        }

                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 5)//Animal (Tipo de Criação)
                        {
                            vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '4', 'IdCriacaoTipoIn': vmParam.CategoriaServicoVM().IdsCriacaoTipo });
                            vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '4', 'IdCriacaoCategoriaIn': vmParam.CategoriaServicoVM().IdsCriacaoCategoria });

                        }

                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 6)//Lote e Animal (Tipo de Criação)
                        {
                            vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '3,4', 'IdCriacaoTipoIn': vmParam.CategoriaServicoVM().IdsCriacaoTipo });
                            vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '3,4', 'IdCriacaoCategoriaIn': vmParam.CategoriaServicoVM().IdsCriacaoCategoria });
                        }

                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo == 7)//Equipamento (Parte Equipamento)
                        {
                            vm.dadosComplementaresAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '5', 'IdAutomotivoTipoEquipamentoIn': vmParam.CategoriaServicoVM().IdsAutomotivoTipoEquipamento });
                            vm.dadosComplementaresSubAplicacao({ 'Ativo': true, 'IdAplicacaoTipoIn': '5', 'IdAutomotivoParteEquipamentoIn': vmParam.CategoriaServicoVM().IdsAutomotivoParteEquipamento });
                        }

                        //Visibilidade e Obrigatoriedade do campo sub aplicação (Com excessão a Benfeitoria, pois não existe Sub Aplicação)
                        if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo != 1 && vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 1) {
                            vm.ExibeSubAplicacao(false);
                            vm.SubAplicacaoObrigatoria(false);
                            vm.IdsSubAplicacao(null);
                        }
                        else if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo != 1 && vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 2) {
                            vm.ExibeSubAplicacao(true);
                            vm.SubAplicacaoObrigatoria(false);
                        }
                        else if (vmParam.CategoriaServicoVM().TipoAplicacaoTipo != 1 && vmParam.CategoriaServicoVM().TipoControleSubAplicacao == 3) {
                            vm.ExibeSubAplicacao(true);
                            vm.SubAplicacaoObrigatoria(true);
                        }

                        //Regras para Empresa Usuária Destinatária 
                        if (vmParam.CategoriaServicoVM().TipoDestinatario != null) {

                            // || Regras contidas até dia 25/04/2016 || //
                            //1 - Para Qualquer Empresa
                            //2 - Para Qualquer Empresa Usuária e Qualquer Pessoa
                            //3 - Para a Empresa Usuária Selecionada
                            //4 - Para a Empresa Usuária Selecionada e Qualquer Pessoa
                            //5 - Para Qualquer Pessoa

                            switch (vmParam.CategoriaServicoVM().TipoDestinatario) {
                                case 1:
                                    if (vmParam.CategoriaServicoVM().IdPessoaEmpresa != null)
                                        vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaIn': vm.IdEmpresaUsuariaLogada() });
                                    else
                                        vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false });
                                    break;
                                case 2:
                                    if (vmParam.CategoriaServicoVM().IdPessoaEmpresa != null)
                                        vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': true, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaIn': vm.IdEmpresaUsuariaLogada() });
                                    else
                                        vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': true, 'ComEmpresaUsuaria': true, 'ComEmpresa': false });
                                    break;
                                case 3:
                                    vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaIn': vm.IdEmpresaUsuariaLogada() });
                                    break;
                                case 4:
                                    vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false, 'IdPessoaIn': vm.IdEmpresaUsuariaLogada() });
                                    break;
                                case 5:
                                    vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false });
                                    break;
                                default:
                                    vm.dadosComplementaresEmpresaUsuariaDestinataria({ 'Ativo': true, 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false });
                                    break;
                            }
                        }
                    }
                });

                //regras e exibição de campos de acordo com as configurações da Ordem de Serviço
                ko.computed(function () {
                    if (vm.configuracaoOrdemServico() != null) {
                        if (vm.configuracaoOrdemServico().PermiteEmissaoOrdem == false) {
                            alert('TODO : Mensagem de erro. Não é Permitido emitir ordem para esta categoria de Serviço');
                        }

                        vm.IdCategoriaServicoOrdem(vm.configuracaoOrdemServico().IdCategoriaServicoOrdem);

                        //Responsável visível
                        if (vm.configuracaoOrdemServico().TipoResponsabilidade == 2 || vm.configuracaoOrdemServico().TipoResponsabilidade == 3)
                            vm.ResponsavelVisivel(true);

                        //Responsável Obrigatório
                        if (vm.configuracaoOrdemServico().TipoResponsabilidade == 3)
                            vm.ResponsavelObrigatorio(true);

                        //Responsável configurado
                        if (vm.configuracaoOrdemServico().TipoResponsabilidade == null || vm.configuracaoOrdemServico().TipoResponsabilidade == 0) {
                            vm.ResponsavelVisivel(false);
                            vm.ResponsavelObrigatorio(false);
                            vm.IdsPessoaResponsavel(null);
                        }

                        //Descrição Visível e obrigatório
                        if (vm.configuracaoOrdemServico().TipoDescricao == 2) {
                            vm.DescricaoObrigatoria(true);
                        }
                        else {
                            vm.DescricaoObrigatoria(false);
                            vm.Descricao(null);
                        }

                        //Período Estimado, exibição e regras
                        if (vm.configuracaoOrdemServico().TipoPeriodoProgramacao == 1) {
                            vm.ExibeDataInicioEstimada(false);
                            vm.ExibeDataTerminoEstimada(false);

                            vm.DataInicioEstimadaExibicao(null);
                            vm.DataTerminoEstimadaExibicao(null);
                        }
                        if (vm.configuracaoOrdemServico().TipoPeriodoProgramacao == 2) {
                            vm.ExibeDataInicioEstimada(true);
                            vm.ExibeDataTerminoEstimada(true);
                        }
                        if (vm.configuracaoOrdemServico().TipoPeriodoProgramacao == 3) {
                            vm.ExibeDataInicioEstimada(true);
                            vm.ExibeDataTerminoEstimada(false);
                        }
                        if (vm.configuracaoOrdemServico().TipoPeriodoProgramacao == 4) {
                            vm.ExibeDataInicioEstimada(false);
                            vm.ExibeDataTerminoEstimada(true);
                        }

                        //Período Realização, exibição e regras
                        if (vm.configuracaoOrdemServico().TipoControleExecucao == 1) {
                            vm.ExibeDataInicioRealizado(false);
                            vm.ExibeDataTerminoRealizado(false);

                            vm.DataInicioRealizadoExibicao(null);
                            vm.DataTerminoRealizadoExibicao(null);
                        }
                        if (vm.configuracaoOrdemServico().TipoControleExecucao == 2) {
                            vm.ExibeDataInicioRealizado(true);
                            vm.ExibeDataTerminoRealizado(true);
                        }
                        if (vm.configuracaoOrdemServico().TipoControleExecucao == 3) {
                            vm.ExibeDataInicioRealizado(true);
                            vm.ExibeDataTerminoRealizado(false);
                        }
                        if (vm.configuracaoOrdemServico().TipoControleExecucao == 4) {
                            vm.ExibeDataInicioRealizado(false);
                            vm.ExibeDataTerminoRealizado(true);
                        }
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/Ordem/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Ordem salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind,
    }
});