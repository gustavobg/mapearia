﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'alteraCEP', 'button', 'vanillaUniform', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate', '../Utils/RetornaTiposEndereco'], function (pessoa, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            // configura controller genérica de CRUD
            pessoa.config({
                saveUrl: '/Corporativo/Pessoa/NewEdit',
                getUrl: '/Corporativo/Pessoa/NewEditJson'
            });

            var request = pessoa.get({ id: id }),
                form = $('#form-CorporativoPessoa-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                function VerificaEnderecoPrincipal() {
                    var PossuiEnderecoPrincipal = false;

                    var total = vm.Enderecos().length;
                    //ko.utils.arrayForEach(vm.Enderecos(), function (item) {
                    //    if (ko.isObservable(item.IdsPessoaEnderecoTipo)) {
                    //        if (item.IdsPessoaEnderecoTipo() != null) {
                    //            var x = item.IdsPessoaEnderecoTipo().split(',');
                    //            for (var i = 0; i < x.length; i++) {
                    //                if (x[i] == '1') {
                    //                    PossuiEnderecoPrincipal = true;
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //    }
                    //    else {
                    //        var x = item.IdsPessoaEnderecoTipo.split(',');
                    //        for (var i = 0; i < x.length; i++) {
                    //            if (x[i] == '1') {
                    //                PossuiEnderecoPrincipal = true;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //});
                    return PossuiEnderecoPrincipal;
                };


                var EnderecoExtend = function (vm) {
                    var PossuiEnderecoPrincipal = VerificaEnderecoPrincipal();
                    this.DadosComplementaresEndereco = ko.observable();
                    this.PossuiEnderecoPrincipal = ko.observable(false);
                    this.ArrTextoTipoEndereco = ko.observableArray([]);

                    //this.TextoEntregaRecebimento = ko.observable('Entrega ou Recebimento');
                    //this.TextoCobranca = ko.observable('Cobrança');
                    //this.TextoSedeDomicilio = ko.observable('Sede ou Domicílio');

                    if (PossuiEnderecoPrincipal == true)
                        this.PossuiEnderecoPrincipal(true);
                    else
                        this.PossuiEnderecoPrincipal(false);

                    this.Endereco = ko.observable(null);
                    this.Sigla = ko.pureComputed(function () {
                        var Endereco = this.Endereco();
                        if (Endereco && Endereco.hasOwnProperty('Sigla'))
                            return Endereco.Sigla;
                    }, this);

                }

                var EntidadeCaracteristica = function () {
                    this.IdCaracteristica = null;
                    this.Descricao = null;
                    this.CaracteristicaValor = EntidadeCaracteristicaValor();
                };
                var EntidadeCaracteristicaValor = function () {
                    this.IdCaracteristicaValor = null;
                    this.Valor = null;
                };

                vm.Usuario.UsuarioObrigatorio = ko.observable(false);

                window.vmUsuario = vm.Usuario;

                ko.computed(function () {
                    var vmUsuario = vm.Usuario;
                    if (vmUsuario.IdUsuario() === null)
                        vmUsuario.UsuarioObrigatorio(false);

                    if (vmUsuario.Login() != null && vmUsuario.Login().length > 0 ||
                        vmUsuario.Email() != null && vmUsuario.Email().length > 0 ||
                        vmUsuario.IdGrupoUsuario() != null && vmUsuario.IdGrupoUsuario().length > 0)
                        vmUsuario.UsuarioObrigatorio(true);
                    else
                        vmUsuario.UsuarioObrigatorio(false);
                }, vmUsuario)

                //var vmPessoaEmpresa = {
                //    @Html.ObservableFor(model => model.PessoaEmpresa.Ativo),
                //    @Html.ObservableFor(model => model.PessoaEmpresa.Matriz)
                //};

                vm.PessoaNatureza = {
                    PessoaFisica: 1,
                    PessoaJuridica: 2
                };
                vm.ValidarDocumento = function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica) {
                        return { cpf: true };
                    }
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica) {
                        return { cnpj: true };
                    } else {
						return {}
                    }
                };

                vm.PessoaNatureza = {
                    PessoaFisica: 1,
                    PessoaJuridica: 2
                };

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/Pessoa/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Pessoa salva com sucesso.');
                        }
                    });
                };

                vm.TextoTipo = ko.computed(function () {
                    if (vm.Corporacao())
                        return "Corporação";
                    else
                        return "Indivíduo";
                });

                vm.LabelNome = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "Nome";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Razão Social";
                });
                vm.LabelNomeSecundario = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "Apelido";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Nome Fantasia";
                });
                vm.LabelDocumento1 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "CPF";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "CNPJ";
                });
                vm.LabelDocumento2 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "RG";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Insc. Estadual";
                });
                vm.LabelDocumento3 = ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica)
                        return "CNH";
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica)
                        return "Insc. Municipal";
                });

                window.vm = vm;
                window.EnderecoExtend = EnderecoExtend;

                ko.applyBindingsToNode(document.getElementById('pnlEnderecos'), { collapsible: { vmArray: [vm.Enderecos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlContatos'), { collapsible: { vmArray: [vm.Contatos()] } });
                // Collapsibles de campos personalizados
                ko.applyBindingsToNode(document.getElementById('pnlCamposPersonalizados'), { collapsibleCamposPersonalizados: { vmArray: [vm.Caracteristicas()] } });

                ko.computed(function () {
                    if (vm.IsentoIE() == true) {
                        vm.Documento2('ISENTO');
                    }
                });

                ko.applyBindings(vm, form[0]);

                //$('#txtNome').blur(function () {
                //    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica && vm.NomeSecundario() == '') {
                //        vm.NomeSecundario(vm.Nome());
                //    }
                //})

                ko.computed(function () {
                    if ((vm.Nome() != null && vm.Nome() != '') && (vm.NomeSecundario() == null || vm.NomeSecundario() == '')) {
                        vm.NomeSecundario(vm.Nome());
                    }
                });


                ko.computed(function () {
                    if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaFisica) {
                        $("#txtDocumento1").inputmask(defaults.mask.cpf);
                        $("#lblData").text("Data de Nascimento");
                    }
                    else if (vm.IdPessoaNatureza() == vm.PessoaNatureza.PessoaJuridica) {
                        $("#txtDocumento1").inputmask(defaults.mask.cnpj);
                        $("#lblData").text("Data de Fundação");
                    }
                });
            });

            return request;
        };

    return {
        bind: bind,
    }
});