﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'unidade-tipo', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit', 'gridview'], function (contaBancaria, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            contaBancaria.config({
                saveUrl: '/Corporativo/Pessoa/PessoaContaBancaria',
                getUrl: '/Corporativo/Pessoa/PessoaContaBancariaJson',
            });

            var request = contaBancaria.get({ id: id }),
                form = $('#form-PessoaContaBancaria-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);
                vm.IdPessoa(id);

                var ContaBancariaExtend = function (vm) {
                    ko.computed(function () {
                        var ag = "";
                        var c = "";
                        debugger;
                        if ((vm.Agencia() != null && vm.Agencia().length > 0) && (vm.Conta() != null && vm.Conta().length > 0))
                        {
                            if ((vm.DigitoVerificadorAgencia() != null && vm.DigitoVerificadorAgencia().length > 0))
                                ag = vm.Agencia() + " - " + vm.DigitoVerificadorAgencia();
                            else
                                ag = vm.Agencia();

                            if ((vm.DigitoVerificadorConta() != null && vm.DigitoVerificadorConta().length > 0))
                                c = vm.Conta() + " - " + vm.DigitoVerificadorConta();
                            else
                                c = vm.Conta();

                            vm.DetalhesConta(ag + " / " + c);
                        }
                    },this);
                }

                window.ContaBancariaExtend = ContaBancariaExtend;
                vm.Save = function () {
                    //if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    contaBancaria.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Conta salva com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });
            return request;
        };

    return {
        bind: bind
    };
});