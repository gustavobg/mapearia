﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (pessoaPerfil, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            pessoaPerfil.config({
                saveUrl: '/Corporativo/PessoaPerfil/NewEdit',
                getUrl: '/Corporativo/PessoaPerfil/NewEditJson'
            });

            var request = pessoaPerfil.get({ id: id }),
                form = $('#form-CorporativoPessoaPerfil-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);
                    pessoaPerfil.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Perfil salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});