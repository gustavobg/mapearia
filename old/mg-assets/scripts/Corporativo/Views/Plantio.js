﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (producao, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            producao.config({
                saveUrl: '/Corporativo/CategoriaServicoPlantio/Plantio',
                getUrl: '/Corporativo/CategoriaServicoPlantio/PlantioJson',
            });

            var request = producao.get({ id: id }),
                form = $('#form-CategoriaServicoPlantio-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    producao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração para Plantio salva com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                ko.computed(function () {
                    if (vm.TipoApontamentoPlantio() == '1') {
                        vm.OrdemObrigatoria(false);
                        vm.TipoControleExecucao(1);
                        //vm.PermiteInformeTerminoPlanio(false);
                        vm.PermiteInformeEstimativaColheita(false);
                    }
                    else if (vm.TipoApontamentoPlantio() == '2') {
                        vm.OrdemObrigatoria(false);
                    }
                });

                vm.IdAtividadeEconomica.subscribe(function (newValue) {
                    if (newValue != null || newValue != '') {
                        $.ajax({
                            url: '/Agricola/Cultura/List',
                            type: 'post',
                            dataType: 'json',
                            data: JSON.stringify({
                                IdAtividadeEconomica: newValue,
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                                }
                            }),
                            async: false,
                            success: function (data) {
                                if (data.Data.length > 0) {
                                    debugger;
                                    var idsProducaoCultura = $.map(data.Data, function (p) {
                                        return p.IdProducaoCultura;
                                    }).join(',');
                                    vm.IdsProducaoCultura(idsProducaoCultura);
                                }
                            }
                        });
                    }
                })
            });
            return request;
        };
    return {
        bind: bind
    };
});