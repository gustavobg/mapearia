﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (processo, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            processo.config({
                saveUrl: '/Corporativo/Processo/NewEdit',
                getUrl: '/Corporativo/Processo/NewEditJson'
            });

            var request = processo.get({ id: id }),
                form = $('#form-Processo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/Processo/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Processo salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});