﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (processoSituacao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            processoSituacao.config({
                saveUrl: '/Corporativo/ProcessoSituacao/NewEdit',
                getUrl: '/Corporativo/ProcessoSituacao/NewEditJson',
            });
            var request = processoSituacao.get({ id: id }),
                 form = $('#form-ProcessoSituacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    vm.IdProcesso(Route.routeOptions.parentId);
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        processoSituacao.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Situação salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }

});