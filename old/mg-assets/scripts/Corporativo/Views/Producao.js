﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (producao, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            producao.config({
                saveUrl: '/Corporativo/CategoriaServicoProducao/Producao',
                getUrl: '/Corporativo/CategoriaServicoProducao/ProducaoJson',
            });

            var request = producao.get({ id: id }),
                form = $('#form-CategoriaServicoProducao-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.TipoApontamentoProducao() == '1') {
                        vm.OrdemObrigatoria(false);
                        vm.TipoDocumento(1);
                        vm.TipoControleExecucao('1');
                        //vm.PeriodoAtividadeEconomicaProducao(false);
                        //vm.PeriodoAtividadeEconomicaEstimativa(false);
                        vm.ColheitaSemPlantio(false);
                        vm.PermiteInformeTerminoProducao(false);
                        vm.ProcessamentoDireto(false);
                        vm.IdCategoriaServicoDestino(null);
                    }

                });
                vm.IdAtividadeEconomica.subscribe(function (newValue) {
                    if (newValue != null || vm.IdAtividadeEconomica() != '') {
                        $.ajax({
                            url: '/Contabilidade/AtividadeEconomicaProdutoServico/List',
                            type: 'post',
                            dataType: 'json',
                            data: JSON.stringify({
                                IdAtividadeEconomica: newValue,
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'DescricaoProdutoServico asc'
                                }
                            }),
                            async: false,
                            success: function (data) {
                                if (data.Data.length > 0) {
                                    var idsProdutoServico = $.map(data.Data, function (p) {
                                        return p.IdProdutoServico;
                                    }).join(',');
                                    vm.IdsProdutoServico(idsProdutoServico);
                                }
                            }
                        });
                    }
                });
                ko.computed(function () {
                    

                })

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    producao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração para Produção salva com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });
            return request;
        };
    return {
        bind: bind
    };
});