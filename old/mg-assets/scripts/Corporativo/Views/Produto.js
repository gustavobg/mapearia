﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (produto, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            produto.config({
                saveUrl: '/Corporativo/CategoriaServicoProduto/Produto',
                getUrl: '/Corporativo/CategoriaServicoProduto/ProdutoJson',
            });

            var request = produto.get({ id: id }),
                form = $('#form-CategoriaServicoProduto-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.TipoUtilizacaoProduto.subscribe(function (newValue) {
                    if (newValue == '1') {
                        vm.DiretamenteOrdem(false);
                        vm.InformeServico(false);
                        vm.ReservaProduto(false);
                        vm.SolicitacaoAutomatica(false);

                        vm.IdsProdutoServicoGrupo(null);
                        //Produtos (Por enquanto, não foi feito ... será que vou ter que fazer ??? 10/12/2015)
                    }
                    if (vm.TipoRestricaoProduto() == '1' || vm.TipoRestricaoProduto() == '3') {
                        vm.IdsProdutoServicoGrupo(null);
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdCategoriaServico(id);
                    var data = ko.toJSON(vm);
                    produto.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Configuração de Produtos, Insumos e Materiais salva com sucesso.', false, function () { window.location = '/#/Corporativo/CategoriaServico/List' });  //TODO: refatorar para utilizar rotas customizadas
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
                
            });
            return request;
        };
    return {
        bind: bind
    };
});