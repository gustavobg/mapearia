﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (subProcesso, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            subProcesso.config({
                saveUrl: '/Corporativo/SubProcesso/NewEdit',
                getUrl: '/Corporativo/SubProcesso/NewEditJson',
            });
            var request = subProcesso.get({ id: id }),
                 form = $('#form-SubProcesso-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);


                var vmAcaoExtend = function (vm) {
                    debugger;
                    this.dadosCompleSituacao = ko.observable({ 'Ativo': true, 'IdProcesso': Route.routeOptions.parentId });

                    vm.InicioProcesso.subscribe(function (newValue) {
                        if (newValue != null)
                            vm.IdsProcessoSituacao(null);
                    });
                } 
                window.vmAcaoExtend = vmAcaoExtend;


                //vm.PermiteTransacao.subscribe(function (newValue) {
                //    if (newValue == false) {
                //        vm.IdsTransacao(null);
                //        vm.RepresentaDocumentoTipo(false);
                //    }
                //});
                vm.RepresentaDocumentoTipo.subscribe(function (newValue) {
                    if (newValue == false)
                        vm.IdDocumentoTipo(null);
                });

                vm.Save = function () {
                    vm.IdProcesso(Route.routeOptions.parentId);
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        subProcesso.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Sub Processo salvo com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };
    return {
        bind: bind
    }

});