﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'unidade-tipo', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (transacao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            transacao.config({
                saveUrl: '/Corporativo/Transacao/NewEdit',
                getUrl: '/Corporativo/Transacao/NewEditJson'
            });

            var request = transacao.get({ id: id }),
                form = $('#form-Transacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                var IdTransacaoPai = vm.IdTransacao();

                //Dados Complentares para o Select ddlTransacaoSeguinte
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'IdTransacaoNotIn': vm.IdTransacao() });

                var vmTransacaoSimultaneaExtend = function (vm) {
                    this.dadosComplementaresTransacaoSimultanea = ko.observable({ 'Ativo': true, 'MovimentacaoFinanceiroTipoIn': '2,3', 'IdTransacaoNotIn': IdTransacaoPai });
                    this.Trans = ko.observable();
                    this.DescricaoTransacao = ko.pureComputed(function () {
                        var transacao = this.Trans();
                        if (transacao && transacao.hasOwnProperty('Descricao'))
                            return transacao.Descricao;
                    }, this);

                    this.NaturezaDescricao = ko.pureComputed(function () {
                        var transacao = this.Trans();
                        if (transacao && transacao.hasOwnProperty('MovimentacaoFinanceiroObrigacaoTipo')) {
                            var tipo = transacao.MovimentacaoFinanceiroObrigacaoTipo;
                            if (tipo != null) {
                                switch (tipo) {
                                    case 1:
                                        return "Credora (-)";
                                        break;
                                    case 2:
                                        return "Devedora (+)";
                                        break;
                                }
                            }
                        }
                    }, this);
                };
                window.vmTransacaoSimultaneaExtend = vmTransacaoSimultaneaExtend;

                var vmTransacaoCFOPExtend = function (vm) {
                    this.vmFiscalCFOP = ko.observable(null);
                    this.DescricaoOrigemFiscalCFOP = ko.pureComputed(function () {
                        var FiscalCFOP = this.vmFiscalCFOP();
                        if (FiscalCFOP && (FiscalCFOP.hasOwnProperty("OperacaoEstadual") || FiscalCFOP.hasOwnProperty("OperacaoInterEstadual") || FiscalCFOP.hasOwnProperty("OperacaoInternacional"))) {
                            if (FiscalCFOP.OperacaoEstadual.length > 0)
                                return "0 - Dentro do Estado";

                            if (FiscalCFOP.OperacaoInterEstadual.length > 0)
                                return "1 - Interestadual";

                            if (FiscalCFOP.OperacaoInternacional.length > 0)
                                return "2 - Internacional";
                        }

                    }, this);
                    this.DescricaoUtilizacaoPrevistaTipo = ko.pureComputed(function () {
                        if (vm.UtilizacaoPrevistaTipo() == '1')
                            return "Uso na Produção";
                        if (vm.UtilizacaoPrevistaTipo() == '2')
                            return "Origem do Produto";
                    }, this)
                }
                window.vmTransacaoCFOPExtend = vmTransacaoCFOPExtend;

                //Regra de Exibição de Campos (Fazer na BLL [TRANSAÇÃO BLL] ) - Na BLL 
                //Regra obsoleta, Humberto soliticou a remoção no dia 16/05/2016
                //ko.computed(function () {
                //if (vm.RealizadaViaSistema() == true) {
                //    vm.IdSubProcesso(null);
                //    vm.IdsDocumentoTipo(null);
                //    vm.IdTransacaoSeguinte(null);
                //}
                //});

                vm.IdSubProcesso.subscribe(function (newValue) {
                    if (newValue != null) {
                        vm.IdsDocumentoTipo(null);
                        vm.IdDocumentoTipo(null);
                    }
                });
                //Regra para a Página, não precisa fixar na BLL
                //Quando alterar alguma informação do campo IdsDocumentoTipo, limpar o dependente (IdDocumentoTipo) //Pedido do Humnberto (12/01/2015) JIRA MG-395
                vm.IdsDocumentoTipo.subscribe(function (newValue) {
                    if (newValue != null)
                        vm.IdDocumentoTipo(null);
                });

                //Trazer campo "Descrição Resumida" igual ao campo "Descrição", e permitir o usuário alterar. // Alteração solicitada pelo Humberto 04/01/2016
                vm.Descricao.subscribe(function (newValue) {
                    if (newValue != null) {
                        vm.DescricaoResumida(newValue);
                    }
                });

                //Regra Ações Em Produtos, Insumos, Materiais e Serviços - Regra na BLL
                vm.VendaPatrimonio.subscribe(function (newValue) {
                    if (newValue == true) {
                        vm.DirecionadorEstoqueTipo(null);
                        vm.MovimentacaoEstoqueTipo('1');
                    }

                });
                ko.computed(function () {
                    if (vm.MovimentacaoEstoqueTipo() != null && vm.MovimentacaoEstoqueTipo() != '') {
                        if (vm.MovimentacaoEstoqueTipo() != '2' && vm.MovimentacaoEstoqueTipo() != '3')
                            vm.CaracteristicaGeracaoTipo(null);
                        if (vm.MovimentacaoEstoqueTipo() == '1')
                            vm.DirecionadorEstoqueTipo(null);
                    }
                });

                //Regra de Exibição Ações Financeiras - Regra na BLL
                ko.computed(function () {
                    if (vm.MovimentacaoFinanceiroTipo() != '2' && vm.MovimentacaoFinanceiroTipo() != '3')
                        vm.MovimentacaoFinanceiroObrigacaoTipo(null);
                    if (vm.MovimentacaoFinanceiroTipo() == '1') {
                        vm.DirecionadorFinanceiroTipo(null);
                        vm.HistoricoTipo(null);
                        vm.UtilizadoComoAntecipacao(false);
                        vm.RepresentaAntecipacao(false)
                    }

                    //if (!vm.MovimentacaoFinanceiroTipo() != '1')
                    //    vm.GeraMovimentoBancario(false);
                });

                //Regra de Exibição Ações com Terceiros (Implementar regra na BLL) - Regra na BLL
                ko.computed(function () {
                    if (vm.ControleOperacaoTerceiroTipo() == '1') {
                        vm.DirecionadorTerceirosTipo(null);
                        vm.TempoLimite(null);
                        vm.IdUnidadeTempoLimite(null);
                    }
                });

                vm.ControleOperacaoTerceiroTipo.subscribe(function (newValue) {
                    if (newValue != null && newValue != '1') {
                        vm.IdUnidadeTempoLimite(72);
                    }
                });

                //Ações Fiscais
                ko.computed(function () {
                    if (vm.RealizaMovimentoFiscal() == false) //Regra na BLL
                    {
                        vm.ExigeDocumentoOrigem(false);
                        vm.IdTransacaoOrigem(null);
                        vm.DirecionadorFiscalTipo(null);
                    }

                    //if (vm.MovimentacaoContabilTipo() == '1')
                    //    vm.DirecionadorFiscalTipo(null);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Corporativo/Transacao/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Transação salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });
            return request;
        };

    return {
        bind: bind
    }
});