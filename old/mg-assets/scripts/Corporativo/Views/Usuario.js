﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'unidade-tipo', 'jquery-flipper', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (usuario, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            usuario.config({
                saveUrl: '/Corporativo/Pessoa/Usuario',
                getUrl: '/Corporativo/Pessoa/UsuarioJson',
            });

            var request = usuario.get({ id: id }),
                form = $('#form-Usuario-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.UsuarioObrigatorio = ko.observable(false);

                ko.computed(function () {
                    //if (vm.IdUsuario() === null)
                    //    vm.UsuarioObrigatorio(false);

                    //if (vm.Login() != null && vm.Login().length > 0 ||
                    //    vm.Email() != null && vm.Email().length > 0 ||
                    //    vm.IdGrupoUsuario() != null && vm.IdGrupoUsuario().length > 0)
                    //    vm.UsuarioObrigatorio(true);

                    //else
                    //    vm.UsuarioObrigatorio(false);

                    //if (vm.Login() == null) {
                    //    $("#btn-salvar").attr('disabled', 'disabled');
                    //}
                    //else {
                    //    $("#btn-salvar").removeAttr('disabled', 'disabled');
                    //}
                }, vm)

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.IdPessoa(id);
                    var data = ko.toJSON(vm);
                    usuario.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Usuário salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                $('#btnReenviarEmailAtivacao').click(function (e) {
                    e.preventDefault();

                    $.post('/Corporativo/Pessoa/ReenviarEmailAtivacao',
                        {
                            idPessoa: vm.IdPessoa()
                        },
                        function (result) {
                            if (result.Sucesso) {
                                toastr.info(result.Mensagem);
                            }
                            else {
                                toastr.error(result.Mensagem);
                            }
                        }
                    );
                });

                vm.IdVersaoPlanoContasPlanoContas.subscribe(function (newValue) {
                    vm.IdsPlanoContas(null);
                    $.ajax({
                        url: '/Seguranca/UsuarioPlanoContasRestricao/ListConcatenado',
                        type: 'post',
                        data: 'IdPlanoContasVersao=' + newValue,
                        async: false,
                        success: function (data) {
                            vm.IdsPlanoContas(data.IdsPlanosContas);
                        }
                    });
                });
                vm.IdVersaoPlanoContasCentroCusto.subscribe(function (newValue) {
                    vm.IdsCentroCusto(null);
                    $.ajax({
                        url: '/Seguranca/UsuarioCentroCustoRestricao/ListConcatenado',
                        type: 'post',
                        data: 'IdPlanoContasVersao=' + newValue,
                        async: false,
                        success: function (data) {
                            vm.IdsCentroCusto(data.IdsCentroCusto);
                        }
                    });
                });
                vm.IdVersaoPlanoContasClassificacao.subscribe(function (newValue) {
                    vm.IdsFinanceiroClassificacao(null);
                    $.ajax({
                        url: '/Seguranca/UsuarioFinanceiroClassificacaoRestricao/ListConcatenado',
                        type: 'post',
                        data: 'IdPlanoContasVersao=' + newValue,
                        async: false,
                        success: function (data) {
                            vm.IdsFinanceiroClassificacao(data.IdsClassificacoes);
                        }
                    });
                })

            });

            return request;

        };

    return {
        bind: bind
    };
});