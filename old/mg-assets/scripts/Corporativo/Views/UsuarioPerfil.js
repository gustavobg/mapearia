﻿define(['../Controllers/UsuarioPerfil', 'util', 'knockout', 'feedback', 'toastr',  'knockout-upload-mapping', 'usuario-perfil-bind', 'jquery-flipper', 'knockout-upload', 'jquery-inputmask', 'ko-validate-rules', 'ko-validate'], function (usuarioPerfil, mg, ko, mgFeedbackBase, toastr, vmMappingToUpload, usuarioPerfilBind) {

    var vm = null,
        bind = function (id) {
            var request = usuarioPerfil.get({ id: id }),
                form = $('#form-UsuarioPerfil');

            request.done(function (response) {               
                vm = ko.mapping.fromJS(response);
                vm.Save = function () {                    
                    var data = ko.toJSON(vm);

                    if (!vm.isValidShowErrors()) { return; }

                    usuarioPerfil.save(data).done(function (result, status, xhr) {                        
                        if (result.Sucesso) {
                            toastr.success('Perfil do usuário alterado com sucesso.');
                            usuarioPerfilBind.AtualizaUsuarioPerfil();
                            // atualiza dados para pegar caminho do upload da imagem no servidor
                            Route.reload();
                        }
                        else {
                            toastr.error("Erro ao salvar perfil do usuário");
                        }
                    })
                };

                vm = new vmMappingToUpload(vm);
                window.vm = vm;    
                ko.applyBindings(vm, form[0]);

                $("#ddlSexo").select2();

                if (vm.DataNascimento() != null) {
                    $("#txtDataNascimento").trigger("change");
                }

            });

            return request;
        };

    return {
        bind: bind,
        routeOptions: { showBackButton: false, showTitle: true }
    };

});