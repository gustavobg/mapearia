﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'partial-pessoa', 'datetimepicker', 'bootstrap/modal', 'jquery-inputmask'], function (animal, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            animal.config({
                saveUrl: '/Criacao/Animal/NewEdit',
                getUrl: '/Criacao/Animal/NewEditJson'
            });

            var request = animal.get({ id: id }),
                form = $('#form-CriacaoAnimal-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    animal.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Animal salvo com Sucesso.');
                    });
                };

                vm.ExibeDataFim = ko.observable(null);
                vm.DadosComplementaresSituacao = ko.observable(null);
                vm.DadosComplementaresOrigem = ko.observable(null);
                ko.computed(function () {
                    if (vm.IdCriacaoRaca() != null && vm.IdCriacaoCategoria() != null)
                    {
                        vm.DadosComplementaresSituacao({ 'Ativo': true, 'IdCriacaoCaracterizacaoTipo': 1, 'IdCriacaoRaca': vm.IdCriacaoRaca() });
                        vm.DadosComplementaresOrigem({ 'Ativo': true, 'IdCriacaoCaracterizacaoTipo': 3, 'IdCriacaoRaca': vm.IdCriacaoRaca() });
                    }
                        
                });

                var vmParam = function () {
                    vm.ExibeDataFim(false);
                    this.CaracterizacaoSituacao = ko.observable();
                    ko.computed(function () {
                        var situacao = this.CaracterizacaoSituacao();
                        if(situacao && situacao.hasOwnProperty('SituacaoLoteAnimal'))
                        {
                            if (situacao.SituacaoLoteAnimal == null)
                                vm.ExibeDataFim(false);

                            if (situacao.SituacaoLoteAnimal == false)
                                vm.ExibeDataFim(true);
                                
                            else
                                vm.DataTerminoControleExibicao(null);
                        }
                    },this);
                };
                vmParam = new vmParam();
                window.vmParam = vmParam;

                function RetornaInformacoesRaca(IdCriacaoRaca) {
                    var CriacaoRaca = null;
                    $.post('/Criacao/Raca/List', JSON.stringify({
                        IdCriacaoRaca: IdCriacaoRaca,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                        }
                    })).success(function (data) {
                        CriacaoRaca = data.Data[0];
                        if (CriacaoRaca != null) {
                            //Retorna o tipo de animal pra buscar as categorias por tipo.
                            vm.IdCriacaoTipo(CriacaoRaca.IdCriacaoTipo);

                            if (CriacaoRaca.PrefixoTipoCriacaoRaca == 3 || CriacaoRaca.PrefixoTipoCriacaoRaca == 4) {
                                if (CriacaoRaca.PrefixoCriacaoTipo != null && vm.IdCriacaoAnimal() == null)
                                    vm.Codigo(CriacaoRaca.PrefixoCriacaoTipo + "*");
                                else
                                    vm.Codigo(CriacaoRaca.PrefixoCriacaoTipo);
                            }
                        }
                        if (CriacaoRaca.ExibeApelido == true)
                            vm.ExibeApelidoAnimal(true);
                        else {
                            vm.ExibeApelidoAnimal(false);
                            vm.Apelido = null;
                        }
                    });
                };

                vm.IdCriacaoTipo = ko.observable(null);
                vm.ExibeApelidoAnimal = ko.observable(null);
                vm.IdCriacaoRaca.subscribe(function () {
                    RetornaInformacoesRaca(vm.IdCriacaoRaca());
                });

                ko.bindingHandlers.date = {
                    update: function (element, valueAccessor) {
                        var value = valueAccessor();
                        var date = moment(value);
                        $(element).text(date.format("L"));
                    }
                };

                var vmProducaoLocalDescricao = {
                    Descricao: ko.observable('')
                };

                var vmProducaoUnidadeDescricao = {
                    Descricao: ko.observable('')
                };

                var vmProprietario = {
                    NomePessoa: ko.observable('')
                };

                var vmHistoricoCategoria = function () {
                    var self = this;
                    this.Data = ko.observableArray([]);

                    this.VerHistorico = function () {
                        var historico = null;
                        $.post('/Criacao/CategoriaHistorico/ListCategoriaAnimal', JSON.stringify({
                            IdCriacaoAnimal: vm.IdCriacaoAnimal(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'DataHoraMovimentacao desc'
                            }
                        })).success(function (data) {
                            self.Data(data.Data);
                        });
                        $("#modal-HistoricoCategoria").modal("show");
                    }
                };

                var vmHistoricoSituacao = function () {
                    var self = this;
                    this.Data = ko.observableArray([]);

                    this.VerHistorico = function () {
                        var historico = null;
                        $.post('/Criacao/SituacaoHistorico/ListSituacaoAnimal', JSON.stringify({
                            IdCriacaoAnimal: vm.IdCriacaoAnimal(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'DataHoraMovimentacao desc'
                            }
                        })).success(function (data) {
                            self.Data(data.Data);
                        });
                        $("#modal-HistoricoSituacao").modal("show");
                    };
                };

                ko.applyBindingsToNode(document.getElementById('pnlLocalizacao'), { collapsible: { vmArray: [vm.Localizacao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProprietarios'), { collapsible: { vmArray: [vm.Proprietarios()] } });               

                vmHistoricoSituacao = new vmHistoricoSituacao();
                vmHistoricoCategoria = new vmHistoricoCategoria();
                window.vmHistoricoSituacao = vmHistoricoSituacao;
                window.vmHistoricoCategoria = vmHistoricoCategoria;
                window.vmProducaoLocalDescricao = vmProducaoLocalDescricao;
                window.vmProducaoUnidadeDescricao = vmProducaoUnidadeDescricao;
                window.vmProprietario = vmProprietario;
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
                ko.applyBindings(vmHistoricoCategoria, document.getElementById("modal-HistoricoCategoria"));
                ko.applyBindings(vmHistoricoSituacao, document.getElementById("modal-HistoricoSituacao"));
                
            });

            return request;
        };

    return {
        bind: bind
    }
});