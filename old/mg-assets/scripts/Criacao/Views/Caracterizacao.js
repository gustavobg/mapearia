﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (caracterizacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            caracterizacao.config({
                saveUrl: '/Criacao/Caracterizacao/NewEdit',
                getUrl: '/Criacao/Caracterizacao/NewEditJson'
            });

            var request = caracterizacao.get({ id: id }),
                form = $('#form-CriacaoCaracterizacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                   
                    var data = ko.toJSON(vm);

                    caracterizacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Caracterização salva com Sucesso.');
                    });
                };

                vm.ExibeSituacaoLoteAnimal = ko.computed(function () {
                    if (vm.IdCriacaoCaracterizacaoTipo() == 1) {
                        //vm.SituacaoLoteAnimal(true);
                        return true;
                    }
                    else {
                        vm.SituacaoLoteAnimal(null);
                        vm.ExibeConsulta(null);
                        return false;
                    }
                });

                vm.ExibeOrigem = ko.computed(function () {
                    if (vm.IdCriacaoCaracterizacaoTipo() == 3)
                        return true;
                    else {
                        vm.DescricaoOrigem(null);
                        vm.RepresentaNascimento(null);
                        return false;
                    }
                });

                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlSituacao'), { collapsible: { vmArray: [vm.SituacaoLoteAnimal(), vm.ExibeConsulta()] } });
                ko.applyBindingsToNode(document.getElementById('pnlOrigem'), { collapsible: { vmArray: [vm.DescricaoOrigem()] } });
                ko.applyBindingsToNode(document.getElementById('pnlObservacao'), { collapsible: { vmArray: [vm.Observacao()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});