﻿define(['crud-controller', 'knockout', 'feedback', 'button', 'select2', 'jquery-flipper', 'ko-validate', 'unidade-tipo'], function (categoria, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            categoria.config({
                saveUrl: '/Criacao/Categoria/NewEdit',
                getUrl: '/Criacao/Categoria/NewEditJson'
            });

            var request = categoria.get({ id: id }),
                form = $('#form-CriacaoCategoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    categoria.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Categoria salva com Sucesso.');
                    });
                };

                vm.FormaAlteracaoTempo.subscribe(function (newValue) {
                    debugger;
                    if (newValue == false) {
                        vm.IdUnidadeTempoVida(null);
                        vm.TempoVida(null);
                    }
                    else {
                        //Humberto solicitou esta implementação, trazer como default a unidade de Tempo Dia (D) 06/01/2016
                        if (vm.IdUnidadeTempoVida() == null)
                            vm.IdUnidadeTempoVida(72) // Unidade que Representa Dia (D)
                    }
                });

                vm.FormaAlteracaoServico.subscribe(function (newValue) {
                    if (newValue == false) {
                        vm.IdsProdutoServico(null);
                    }
                });

                vm.ControleLancamentoContabilTipo.subscribe(function (newValue) {
                    debugger;
                    if (newValue == '2')
                        vm.IdProdutoServicoAnimal(null);
                    if (newValue == '1')
                        vm.IdClassePatrimonial(null);
                });

                //vm.FormaAlteracaoManual.subscribe(function (newValue) {
                //    vm.FormaAlteracaoTempo(false);
                //    vm.FormaAlteracaoServico(false);
                //});

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});