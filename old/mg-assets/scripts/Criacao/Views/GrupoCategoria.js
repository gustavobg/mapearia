﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (grupoCategoria, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            grupoCategoria.config({
                saveUrl: '/Criacao/GrupoCategoria/NewEdit',
                getUrl: '/Criacao/GrupoCategoria/NewEditJson'
            });

            var request = grupoCategoria.get({ id: id }),
                form = $('#form-CriacaoGrupoCategoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                   
                    var data = ko.toJSON(vm);

                    grupoCategoria.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Categoria salva com Sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});