﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-collapsible', 'gridview', 'unidade-tipo', 'partial-pessoa', 'button', 'jquery-inputmask'], function (lote, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            lote.config({
                saveUrl: '/Criacao/Lote/NewEdit',
                getUrl: '/Criacao/Lote/NewEditJson'
            });

            var request = lote.get({ id: id }),
                form = $('#form-CriacaoLote-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    lote.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Lote salvo com Sucesso.');
                    });
                };

                vm.UtilizarDescricaoLote = ko.observable();
                vm.DescricaoLabelDataNascimento = ko.observable('Nascimento');
                vm.IdCriacaoTipo = ko.observable(null);

                function RetornaInformacoesRaca(idCriacaoRaca) {
                    var CriacaoRaca = null;
                    $.post('/Criacao/Raca/List', JSON.stringify({
                        IdCriacaoRaca: idCriacaoRaca,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                        }
                    })).success(function (data) {
                        CriacaoRaca = data.Data[0];
                        if (CriacaoRaca != null) {
                            //Retorna o tipo de animal pra buscar as categorias por tipo.
                            vm.IdCriacaoTipo(CriacaoRaca.IdCriacaoTipo);

                            if (CriacaoRaca.PrefixoTipoCriacaoRaca == 2 || CriacaoRaca.PrefixoTipoCriacaoRaca == 4) {
                                if (CriacaoRaca.PrefixoCriacaoTipo != null)
                                    vm.Codigo(CriacaoRaca.PrefixoCriacaoTipo + "*")
                            }

                            if (CriacaoRaca.UtilizarDescricaoLote == true)
                                vm.UtilizarDescricaoLote(true);
                            else {
                                vm.UtilizarDescricaoLote(false);
                                if (vm.IdCriacaoCategoria() != null)
                                    vm.Descricao(vmCategoriaAnimal.Descricao());
                                else
                                    vm.Descricao(null);
                            }

                            if (vm.IdCriacaoLote() == null) {
                                vm.IdUnidadeInicialLote(CriacaoRaca.IdUnidadePadraoLoteCriacaoTipo);
                                vm.IdUnidadeAtualLote(CriacaoRaca.IdUnidadePadraoLoteCriacaoTipo);
                            }
                        }
                    });
                };

                function RetornaInformacoesOrigem(idCriacaoCaracterizacao) {
                    var info = null;
                    $.post('/Criacao/Caracterizacao/List', JSON.stringify({
                        IdCriacaoCaracterizacao: idCriacaoCaracterizacao,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: "Descricao asc"
                        }
                    })).success(function (data) {
                        info = data.Data[0];
                        if (info != null) {
                            if (info.IdCriacaoCaracterizacaoTipo == 3 && info.RepresentaNascimento == true)
                                vm.DescricaoLabelDataNascimento(info.DescricaoOrigem)
                        }
                    });
                };

                var vmProducaoLocalDescricao = {
                    Descricao: ko.observable('')
                };
                var vmProducaoUnidadeDescricao = {
                    Descricao: ko.observable('')
                };

                var vmProprietario = {
                    NomePessoa: ko.observable('')
                };

                var vmCriacaoAnimal = {
                    Descricao: ko.observable('')
                };
                var vmCategoriaAnimal = {
                    Descricao: ko.observable('')
                };

                var vmHistoricoCategoria = function () {
                    var self = this;
                    this.Data = ko.observableArray([]);

                    this.VerHistorico = function () {
                        var historico = null;
                        $.post('/Criacao/CategoriaHistorico/ListCategoriaLote', JSON.stringify({
                            IdCriacaoLote: vm.IdCriacaoLote(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'DataHoraMovimentacao desc'
                            }
                        })).success(function (data) {
                            self.Data(data.Data);
                        });
                        $("#modal-HistoricoCategoria").modal("show");
                    }
                };

                var vmHistoricoSituacao = function () {
                    var self = this;
                    this.Data = ko.observableArray([]);

                    this.VerHistorico = function () {
                        var historico = null;
                        $.post('/Criacao/SituacaoHistorico/ListSituacaoLote', JSON.stringify({
                            IdCriacaoLote: vm.IdCriacaoLote(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'DataHoraMovimentacao desc'
                            }
                        })).success(function (data) {
                            self.Data(data.Data);
                        });
                        $("#modal-HistoricoSituacao").modal("show");
                    }
                };

                var vmHistoricoCategoria = new vmHistoricoCategoria();
                var vmHistoricoSituacao = new vmHistoricoSituacao();

                if (vm.IdCriacaoCaracterizacaoOrigem() != null)
                    RetornaInformacoesOrigem(vm.IdCriacaoCaracterizacaoOrigem());

                if (vm.IdCriacaoRaca() != null)
                    RetornaInformacoesRaca(vm.IdCriacaoRaca());

                vm.IdCriacaoRaca.subscribe(function (newValue) {
                    RetornaInformacoesRaca(newValue);
                });

                vm.IdCriacaoCaracterizacaoOrigem.subscribe(function (newValue) {
                    RetornaInformacoesOrigem(newValue);
                });

                ko.computed(function () {
                    if (vm.UtilizarDescricaoLote() == false && vm.IdCriacaoCategoria() != null) {
                        //Apenas para passar a Descrição da Categoria para o Lote, uma vez que a Descrição é obrigatória. (Regra do Humberto dia 04/08/2015)
                        vm.Descricao($('#s2id_ddlCriacaoCategoria').find('span').text());
                        //vm.Descricao(vmCategoriaAnimal.Descricao());
                    }
                });

                //Busca Unidade do Produto  (Regra do Humberto dia 30/12/2015)
                // 1- Pela Categoria, procuro o IdProduto
                // 2- Pelo Produto, procuro a Unidade
                // 3- Caso não tenha unidade, verifico a família
                // 4- Caso o Produto e a Família não tenha Unidade Cadastrada, busco a unidade no Tipo de Animal.
                vm.IdCriacaoCategoria.subscribe(function (newValue) {
                    var idProdutoServico = 0;
                    var idProdutoServicoFamilia = 0;
                    if (newValue != null) {
                        if (vm.IdCriacaoCategoria() != null) {
                            $.ajax({
                                type: 'POST',
                                url: '/Criacao/Categoria/List',
                                data: JSON.stringify({
                                    IdCriacaoCategoria: vm.IdCriacaoCategoria(),
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao desc'
                                    }
                                }),
                                success: function (data) {
                                    if (data.Data[0].IdProdutoServico != null) {
                                        $.ajax({
                                            type: 'POST',
                                            url: '/Estoque/Produto/List',
                                            data: JSON.stringify({
                                                IdProdutoServico: data.Data[0].IdProdutoServico,
                                                Page: {
                                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao desc'
                                                }
                                            }),
                                            success: function (dataProduto) {
                                                if (dataProduto.Data[0].IdUnidade != null) {
                                                    vm.IdUnidadeInicialLote(dataProduto.Data[0].IdUnidade);
                                                    vm.IdUnidadeAtualLote(dataProduto.Data[0].IdUnidade);
                                                    idProdutoServico = dataProduto.Data[0].IdProdutoServico;
                                                }
                                                else {
                                                    idProdutoServicoFamilia = dataProduto.Data[0].IdProdutoServicoFamilia;
                                                }
                                            },
                                            dataType: 'JSON',
                                            async: false
                                        });
                                    }
                                },
                                dataType: 'JSON',
                                async: false
                            });

                            if (vm.IdUnidadeInicialLote() == null && vm.IdUnidadeAtualLote() == null) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/Estoque/ProdutoServicoFamilia/List',
                                    data: JSON.stringify({
                                        IdProdutoServicoFamilia: idProdutoServicoFamilia,
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao desc'
                                        }
                                    }),
                                    success: function (dataFamilia) {
                                        if (dataFamilia.Data[0].IdUnidade != null && dataFamilia.Data[0].IdProdutoServicoFamilia > 0) {
                                            vm.IdUnidadeInicialLote(dataFamilia.Data[0].IdUnidade);
                                            vm.IdUnidadeAtualLote(dataFamilia.Data[0].IdUnidade);
                                        }
                                    },
                                    dataType: 'JSON',
                                    async: false
                                });
                            }

                            if (vm.IdUnidadeInicialLote() == null && vm.IdUnidadeAtualLote() == null && vm.IdCriacaoTipo() != null) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/Criacao/Tipo/List',
                                    data: JSON.stringify({
                                        IdCriacaoCategoria: vm.IdCriacaoCategoria(),
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao desc'
                                        }
                                    }),
                                    success: function (data) {
                                        debugger;
                                        vm.IdUnidadeInicialLote(data.Data[0].IdUnidadePadraoLote);
                                        vm.IdUnidadeAtualLote(data.Data[0].IdUnidadePadraoLote);
                                    },
                                    dataType: 'JSON',
                                    async: false
                                });
                            }
                        }
                    }
                });

                //--

                ko.applyBindingsToNode(document.getElementById('pnlLocalizacao'), { collapsible: { vmArray: [vm.Locais()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProprietarios'), { collapsible: { vmArray: [vm.Proprietarios()] } });

                var vmParams = {
                    vmProducaoLocalDescricao: vmProducaoLocalDescricao,
                    vmProducaoUnidadeDescricao: vmProducaoUnidadeDescricao,
                    vmProprietario: vmProprietario,
                    vmCriacaoAnimal: vmCriacaoAnimal,
                    vmCategoriaAnimal: vmCategoriaAnimal,
                    vmHistoricoSituacao: vmHistoricoSituacao,
                    vmHistoricoCategoria: vmHistoricoCategoria
                };

                window.vmParams = vmParams;

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
                ko.applyBindings(vmHistoricoCategoria, document.getElementById("modal-HistoricoCategoria"));
                ko.applyBindings(vmHistoricoSituacao, document.getElementById("modal-HistoricoSituacao"));

            });

            return request;
        };

    return {
        bind: bind
    }
});