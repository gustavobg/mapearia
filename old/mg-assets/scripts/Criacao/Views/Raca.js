﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'jquery-collapsible', 'unidade-tipo'], function (raca, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            raca.config({
                saveUrl: '/Criacao/Raca/NewEdit',
                getUrl: '/Criacao/Raca/NewEditJson'
            });

            var request = raca.get({ id: id }),
                form = $('#form-CriacaoRaca-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                   
                    var data = ko.toJSON(vm);

                    raca.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Raça salva com Sucesso.');
                    });
                };

                vm.IdCriacaoTipo.subscribe(function (newValue) {

                    var EstagiosTipo = null;
                    $.post('/Criacao/Tipo/List', JSON.stringify({
                        IdCriacaoTipo: newValue,
                        ComEstagiosVida: true,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                        }
                    })).success(function (data) {
                        EstagiosTipo = data.Data[0];
                        if (EstagiosTipo != null) {
                            if (confirm("Deseja Importar os dados de Estágios de Vida do tipo de Raça " + EstagiosTipo.Descricao + " e substituir as informações atuais ?")) {
                                vm.IdUnidadeCicloVida(EstagiosTipo.IdUnidadePadraoVida);
                                vm.CicloVida(EstagiosTipo.CicloVida);
                                vm.Estagios(EstagiosTipo.Estagios);
                            }
                        }
                    });
                });

                var vmCicloVida = {
                    DescricaoUnidade: ko.observable('')
                };

                window.vmCicloVida = vmCicloVida;
                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlMaisCampos'), { collapsible: { vmArray: [vm.Observacao(), vm.ExibeApelido()] } });
                ko.applyBindingsToNode(document.getElementById('pnlCicloVida'), { collapsible: { vmArray: [vm.Estagios(), vm.ExibeApelido()] } });
                ko.applyBindingsToNode(document.getElementById('pnlLinhagem'), { collapsible: { vmArray: [vm.Linhagem()] } });

                ko.applyBindings(vm, form[0]);


            });

            return request;
        };

    return {
        bind: bind
    }
});