﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'unidade-tipo'], function (tipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            tipo.config({
                saveUrl: '/Criacao/Tipo/NewEdit',
                getUrl: '/Criacao/Tipo/NewEditJson'
            });

            var request = tipo.get({ id: id }),
                form = $('#form-CriacaoTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'IdCriacaoTipoNotIn': vm.IdCriacaoTipo() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    tipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Animal salvo com sucesso.');
                    });
                };

                var vmCicloVida = {
                    DescricaoUnidade: ko.observable('')
                };

                if (vm.IdCriacaoTipo() == null)
                    vm.IdUnidadePadraoLote(79);

                vm.IdUnidadePadraoVida.subscribe(function (newValue) {
                    vm.IdUnidadePadraoGestacao = newValue;
                    $("#TempoGestacao .campo-unidade").val(newValue);
                    $("#TempoGestacao .campo-unidade").trigger("change");
                });

                vm.ExibePrefixo = ko.computed(function () {
                    if (vm.PrefixoTipo() == "2" || vm.PrefixoTipo() == "3")
                        return true;
                    else {
                        vm.Prefixo(null);
                        return false;
                    }
                });

                ko.applyBindingsToNode(document.getElementById('pnlMaisCampos'), { collapsible: { vmArray: [vm.IdCriacaoTipoPai(), vm.Observacao(), vm.Prefixo()] } })
                ko.applyBindingsToNode(document.getElementById('pnlEstagios'), { collapsible: { vmArray: [vm.Estagios] } })

                window.vmCicloVida = vmCicloVida;
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});