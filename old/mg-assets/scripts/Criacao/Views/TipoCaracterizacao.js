﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (tipoCaracterizacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            tipoCaracterizacao.config({
                saveUrl: '/Criacao/TipoCaracterizacao/NewEdit',
                getUrl: '/Criacao/TipoCaracterizacao/NewEditJson'
            });

            var request = tipoCaracterizacao.get({ id: id }),
                form = $('#form-CriacaoCaracterizacaoTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                   
                    var data = ko.toJSON(vm);

                    tipoCaracterizacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Caracterização salva com Sucesso.');
                    });
                };

                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});