﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'select2', 'gridview',  'jquery-flipper', 'mask-decimal', 'jquery-inputmask', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (restricaoCompraEstoque, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            restricaoCompraEstoque.config({
                saveUrl: '/Estoque/CompraEstoqueRestricao/NewEdit',
                getUrl: '/Estoque/CompraEstoqueRestricao/NewEditJson'
            });
            

            var request = restricaoCompraEstoque.get({ id: id }),
                form = $('#form-CompraEstoqueRestricao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.dadosComplePessoa = ko.observable({ 'Ativo': true, 'ComEmpresa': false, 'ComEmpresaUsuaria': false, 'ComEmpresaUsuaria': false, 'ComPessoa': true, 'IdPessoaNatureza': 1, 'PessoaPerfil_PermiteRealizarCompra': true, "IdPessoaNaturezaNotIn":4 });
                vm.dadosComplePessoaAutorizacao = ko.observable({ 'Ativo': true, 'IdPessoaNotIn': vm.IdPessoa() });
                vm.dadosComplePessoaAprovador = ko.observable({ 'Ativo': true, 'IdPessoaNotIn': vm.IdPessoa() });

                var vmRestricoesSolicitacaoExtend = function (vm) {
                    ko.computed(function () {
                        var siglaMoeda = $("#txtSiglaMoeda").val();

                        if (vm.ValorMaximoCompras() != null)
                        {
                            vm.ValorMaximoComprasExibicao(siglaMoeda + " " + vm.ValorMaximoCompras());
                        }

                        if (vm.ValorMaximoMes() != null) {
                            vm.ValorMaximoMesExibicao(siglaMoeda + " " + vm.ValorMaximoMes());
                        }

                        if (vm.ValorMaximoAno() != null) {
                            vm.ValorMaximoAnoExibicao(siglaMoeda + " " + vm.ValorMaximoAno());
                        }


                    });

                };
                var vmRestricoesCotacaoAprovacaoExtend = function (vm) {
                    ko.computed(function () {
                        var siglaMoeda = $("#txtSiglaMoeda").val();

                        if (vm.ValorMaximoCompras() != null) {
                            vm.ValorMaximoComprasExibicao(siglaMoeda + " " + vm.ValorMaximoCompras());
                        }

                        if (vm.ValorMaximoMes() != null) {
                            vm.ValorMaximoMesExibicao(siglaMoeda + " " + vm.ValorMaximoMes());
                        }

                        if (vm.ValorMaximoAno() != null) {
                            vm.ValorMaximoAnoExibicao(siglaMoeda + " " + vm.ValorMaximoAno());
                        }
                    });

                };
                var vmDivergenciasExtend = function (vm) {
                    ko.computed(function () {
                        if (vm.PercentualVariacao() != null) {
                            vm.PercentualVariacaoExibicao(vm.PercentualVariacao() + ",00 %");
                        }
                        if (vm.DivergenciaTipo() != '5' && vm.DivergenciaTipo() != '8' && vm.DivergenciaTipo() != '9'){
                            vm.PercentualVariacao(null);
                            vm.PercentualVariacaoExibicao(null);
                        }
                        if(vm.DivergenciaTipo()!= null)
                        {
                            vm.DescricaoDivergenciaTipo($('#ddlDivergenciaTipo option:selected').text()); //Capturar a Descrição do Tipo de Divergência.//
                        }
                    });

                };

                window.vmRestricoesSolicitacaoExtend = vmRestricoesSolicitacaoExtend;
                window.vmRestricoesCotacaoAprovacaoExtend = vmRestricoesCotacaoAprovacaoExtend;
                window.vmDivergenciasExtend = vmDivergenciasExtend;
                
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Estoque/CompraEstoqueRestricao/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Restrição salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});