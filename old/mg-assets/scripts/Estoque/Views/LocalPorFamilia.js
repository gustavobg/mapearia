﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'gridview', 'mask-decimal', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (localPorFamilia, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            localPorFamilia.config({
                saveUrl: '/Estoque/Saldo/LocalPorFamilia',
                getUrl: '/Estoque/Saldo/LocalPorFamiliaJson',
            });

            var request = localPorFamilia.get({ id: id }),
                form = $('#form-LocalPorFamilia-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                Route.setHeaderInfo(vm.Header());

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;
        };

    return {
        bind: bind
    };
});