﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'gridview', 'mask-decimal', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (localPorProduto, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            localPorProduto.config({
                saveUrl: '/Estoque/Saldo/LocalPorProduto',
                getUrl: '/Estoque/Saldo/LocalPorProdutoJson',
            });

            var request = localPorProduto.get({ id: id }),
                form = $('#form-LocalPorProduto-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);
                Route.setHeaderInfo(vm.Header());

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;
        };

    return {
        bind: bind
    };
});