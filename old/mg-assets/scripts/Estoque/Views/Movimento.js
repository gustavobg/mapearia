﻿define(['crud-controller', 'knockout', 'feedback', 'moment', 'jquery-inputmask', 'button', 'select2', 'gridview', 'datetimepicker', 'mask-decimal', 'jquery-flipper', 'ko-validate-rules', 'ko-validate', 'jquery-qtip'], function (movimento, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            movimento.config({
                saveUrl: '/Estoque/Movimento/NewEdit',
                getUrl: '/Estoque/Movimento/NewEditJson'
            });

            var request = movimento.get({ id: id }),
                form = $('#form-EstoqueMovimento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //[Teste] Retorna Restrições da Empresa Usuária selecionada
                //ko.computed(function () {
                //    if(vm.IdPessoa() != null)
                //    {
                //        debugger;
                //        $.ajax({
                //            url: '/Core/Restricao/RestricaoPorEmpresaUsuaria',
                //            type: 'post',
                //            dataType: 'json',
                //            data: JSON.stringify({
                //                IdPessoa: vm.IdPessoa(),
                //                Page: {
                //                    PageSize: 99999, CurrentPage: 1, OrderBy: 'IdPessoa asc'
                //                }
                //            }),
                //            async: false,
                //            success: function (data) {
                //                debugger;
                //                alert(data);
                //            }
                //        });
                //    }
                //});

                vm.ProdutoServicoVM = ko.observable();
                vm.UnidadeProdutoVM = ko.observable();
                vm.FinanceiroMoedaVM = ko.observable();
                vm.PessoaEmpresaVM = ko.observable();
                vm.TransacaoVM = ko.observable();

                vm.dadosComplementaresTransacao = ko.observable({ 'Ativo': true, 'IdTransacaoIn': '64,53,64,59,63,51,62' });
                vm.dadosComplementaresCentroCusto = ko.observable({ 'Ativo': true, 'UltimoNivel': false, 'ComRestricaoUsuario': true });

                //Quantidade por Embalagem
                vm.QtdePorEmbalagemProdutoServico = ko.pureComputed(function () {
                    var produtoServico = vm.ProdutoServicoVM();
                    if (produtoServico && produtoServico.hasOwnProperty('Quantidade')) {
                        return produtoServico.Quantidade;
                    }
                }, this);

                //Calculo qtde x Forma apresentacao
                vm.QtdeProdutoServico = ko.computed({
                    read: function () {
                        return parseFloat(vm.QtdeProdutoServicoFormaApresentacao()) * parseFloat(vm.QtdePorEmbalagemProdutoServico());
                    },
                    write: function (value) {
                        var qtdeProduto = parseFloat(value);
                        var qtdePorEmbalgem = parseFloat(vm.QtdePorEmbalagemProdutoServico());

                        vm.QtdeProdutoServicoFormaApresentacao(qtdeProduto / qtdePorEmbalgem);
                    }
                });

                vm.IdProdutoServicoFormaApresentacao = ko.pureComputed(function () {
                    var produtoServico = vm.ProdutoServicoVM();
                    if (produtoServico && produtoServico.hasOwnProperty('IdProdutoServicoFormaApresentacao')) {
                        return produtoServico.IdProdutoServicoFormaApresentacao;
                    }
                }, this);
                vm.IdUnidadeProdutoServico = ko.pureComputed(function () {
                    var produtoServico = vm.ProdutoServicoVM();
                    if (produtoServico && produtoServico.hasOwnProperty('IdUnidade')) {
                        return produtoServico.IdUnidade;
                    }
                }, this);
                vm.CasasDecimaisFormaApresentacao = ko.pureComputed(function () {
                    var produtoServico = vm.ProdutoServicoVM();
                    if (produtoServico && (produtoServico.hasOwnProperty('CasasDecimaisFormaApresentacao') && produtoServico.hasOwnProperty('PermiteFracionarFormaApresentacao'))) {
                        {
                            if (produtoServico.PermiteFracionarFormaApresentacao === true) {
                                if (produtoServico.CasasDecimaisFormaApresentacao != null)
                                    return produtoServico.CasasDecimaisFormaApresentacao;
                                else
                                    return 3;
                            }
                            else
                                return 3;
                        }
                    }
                    else
                        return 3;
                });
                vm.CasasDecimaisUnidadeProduto = ko.pureComputed(function () {
                    var produtoServico = vm.ProdutoServicoVM();
                    var unidadeProduto = vm.UnidadeProdutoVM();

                    if (produtoServico && produtoServico.hasOwnProperty('PermiteFracionarUnidadeControle')) {
                        {
                            if (produtoServico.PermiteFracionarUnidadeControle === true) {
                                if (unidadeProduto && unidadeProduto.hasOwnProperty('CasasDecimais')) {
                                    if (unidadeProduto.CasasDecimais != null)
                                        return unidadeProduto.CasasDecimais;
                                    else
                                        return 2;
                                }
                                else
                                    return 2;

                            }
                            else return 2;
                        }
                    }
                    else
                        return 0;
                }, this);
                vm.CasasDecimaisFinanceiroMoeda = ko.pureComputed(function () {
                    var moeda = vm.FinanceiroMoedaVM();
                    if (moeda && moeda.hasOwnProperty('CasasDecimais')) {
                        return moeda.CasasDecimais;
                    }
                }, this);

                //IdFinanceiroMoeda
                ko.computed(function () {
                    if (vm.IdPessoa() != null) {
                        $.ajax({
                            type: "Post",
                            data: {
                                id: vm.IdPessoa()
                            },
                            url: "/Corporativo/EmpresaUsuaria/NewEditJson",
                            dataType: "json",
                            async: false,
                            success: function (result) {
                                if (result != null) {
                                    vm.PessoaEmpresaVM(ko.mapping.fromJS(result));
                                    if (vm.PessoaEmpresaVM != null) {
                                        //Seta o IdFinanceiroMoeda para a Empresa selecionada.
                                        vm.IdFinanceiroMoeda(vm.PessoaEmpresaVM().PessoaEmpresa.IdFinanceiroMoeda());
                                    }
                                }

                            }
                        });
                    }
                });

                //IdDocumentoTipo
                ko.computed(function () {
                    if (vm.TransacaoVM() != null)
                        vm.IdDocumentoTipo(vm.TransacaoVM().IdDocumentoTipo);
                });
                //Descrobre o MovimentoTipo
                ko.computed(function () {
                    if (vm.IdTransacao() != null) {
                        switch (vm.IdTransacao()) {
                            case 46:
                            case 46:
                            case 64:
                            case 53:
                            case 42:
                            case 51:
                            case 45:
                                vm.MovimentoTipo(1);
                                break;
                            case 41:
                            case 59:
                            case 65:
                            case 63:
                            case 43:
                            case 55:
                            case 44:
                                vm.MovimentoTipo(2);
                                break;
                        }
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    movimento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Movimento salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;
        };

    return {
        bind: bind
    }
});