﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (principioAtivo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            principioAtivo.config({
                saveUrl: '/Estoque/PrincipioAtivo/NewEdit',
                getUrl: '/Estoque/PrincipioAtivo/NewEditJson',
            });
            var request = principioAtivo.get({ id: id }),
                form = $('#form-PrincipioAtivo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    principioAtivo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Princípio Ativo salvo com sucesso.');
                    });
                };

                ko.applyBindingsToNode(document.getElementById('pnlObservacao'), { collapsible: { vmArray: [vm.Observacao()] }});

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});