﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'unidade-tipo',  'jquery-inputmask', 'jquery-flipper', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (produtoCaracterizacaoAcoes, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            //ProdutoCaracterizacaoAcoes
            produtoCaracterizacaoAcoes.config({
                saveUrl: '/Estoque/Produto/ProdutoCaracterizacaoAcoes',
                getUrl: '/Estoque/Produto/ProdutoCaracterizacaoAcoesJson',
            });
            var request = produtoCaracterizacaoAcoes.get({ id: id }),
                form = $('#form-ProdutoCaracterizacaoAcoes-NewEdit');

            request.done(function (response) {
                Route.setHeaderInfo(response.Descricao);
                vm = ko.mapping.fromJS(response);
                vm.ExibeFracionamentos = ko.observable(false);

                var vmLocalExtend = function (vm) {
                    ko.computed(function () {
                        if (vm.QuantidadeIdeal() == null) {
                            vm.DescricaoQuantidadeIdeal('');
                        }
                        if (vm.QuantidadeMinima() == null) {
                            vm.DescricaoQuantidadeMinima('');
                        }
                    });
                };
                window.vmLocalExtend = vmLocalExtend;

                //Regras forma de Apresentação
                vm.PermiteFracionarFormaApresentacao.subscribe(function (newValue) {
                    debugger;
                    if(newValue == true)
                        vm.CasasDecimaisFormaApresentacao(3);
                    else
                        vm.CasasDecimaisFormaApresentacao(null);
                });

                //Exibe fracionamentos

                ko.computed(function() { 
                    if(vm.PermiteCompra() == true || vm.Armazenamento() == true || vm.PermiteProducao() == true || vm.PermiteUso() == true ||  vm.PermiteVenda() == true)
                        vm.ExibeFracionamentos(true);
                    else
                    {
                        vm.PermiteFracionarFormaApresentacao(null);
                        vm.CasasDecimaisFormaApresentacao(null);
                        vm.CasasDecimaisFormaApresentacao(null);
                        vm.ExibeFracionamentos(false);
                    }

                });

                vm.LoteValidade.ExibeControlePorRegraRecebimento = ko.observable(false);
                vm.LoteValidade.ExibeTempoRecebimento = ko.observable(false);
                vm.LoteValidade.ExibeControlePorRegraProducao = ko.observable(false);
                vm.LoteValidade.ExibeTempoProducao = ko.observable(false);

                vm.ExibeQuantidadeEmbalagem = ko.observable(false);
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    produtoCaracterizacaoAcoes.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Produto salvo com sucesso.');
                    });
                }

                var vmParams = function () {
                    this.ExibeProdutoServicoFamilia = ko.observable(false);
                    this.ExibeLocais = ko.observable(false);
                    this.ProdutoServicoFamilia = ko.observable();

                    ko.computed(function () {
                        var produto = this.ProdutoServicoFamilia();
                        if (produto && produto.hasOwnProperty('IdUnidade'))
                            vm.IdUnidade(produto.IdUnidade);

                        if (produto && produto.hasOwnProperty('IdUnidadeTipo'))
                            vm.IdUnidadeTipo(produto.IdUnidadeTipo);

                    }, this);
                }
                vmParams = new vmParams();
                window.vmParams = vmParams;

                // inicialização de componentes
                if (!vm.IdProdutoServicoFamilia() === null) {
                    vmParams.ExibeProdutoServicoFamilia(true)
                }
                ko.computed(function () {
                    var permiteArmazenamento = vm.Armazenamento();
                    if (permiteArmazenamento) {
                        vmParams.ExibeLocais(true);
                    } else {
                        vmParams.ExibeLocais(false);
                        vm.ControlaSaldo(false);
                        vm.Locais.removeAll();
                    }
                });

                ko.computed(function () {

                    if (vm.LoteValidade.LoteRecebimentoTipo() == 3)
                        vm.LoteValidade.ExibeControlePorRegraRecebimento(true);
                    else
                        vm.LoteValidade.ExibeControlePorRegraRecebimento(false);

                    if (vm.LoteValidade.LoteProducaoTipo() == 3)
                        vm.LoteValidade.ExibeControlePorRegraProducao(true);
                    else
                        vm.LoteValidade.ExibeControlePorRegraProducao(false);

                    if (vm.LoteValidade.ControleValidadeRecebimentoTipo() == 3)
                        vm.LoteValidade.ExibeTempoRecebimento(true);
                    else
                        vm.LoteValidade.ExibeTempoRecebimento(false);

                    if (vm.LoteValidade.ControleValidadeProducaoTipo() == 3)
                        vm.LoteValidade.ExibeTempoProducao(true);
                    else
                        vm.LoteValidade.ExibeTempoProducao(false);
                });

                vm.LoteValidade.ControleValidadeRecebimentoTipo.subscribe(function (newValue) {
                    if(newValue == 3)
                        vm.LoteValidade.IdUnidadeValidadeRecebimento(72);
                    else
                    {
                        vm.LoteValidade.IdUnidadeValidadeRecebimento(null);
                        vm.LoteValidade.ValorUnidadeRecebimento(null);
                    }
                });

                vm.LoteValidade.ControleValidadeProducaoTipo.subscribe(function (newValue) {
                    if (newValue == 3)
                        vm.LoteValidade.IdUnidadeValidadeProducao(72);
                    else
                    {
                        vm.LoteValidade.IdUnidadeValidadeProducao(null);
                        vm.LoteValidade.ValorUnidadeProducao(null);
                    }
                });
                               

                //ko.applybindingstonode(document.getelementbyid('pnllotevalidade'), { collapsible: { vmarray: [vm.lotevalidade.idsregrarecebimento(), vm.lotevalidade.idsregraproducao() ] } });
                ko.applyBindingsToNode(document.getElementById('pnlCaracteristicasFisicas'), { collapsible: { vmArray: [vm.IdUnidadePesoBruto(), vm.IdUnidadePesoLiquido()] } });
                ko.applyBindingsToNode(document.getElementById('pnlCaracteristicasFisicasMaisInfo'), { collapsible: { vmArray: [vm.IdUnidadeVolume(), vm.IdUnidadePesoBrutoMetro(), vm.IdUnidadeLargura(), vm.IdUnidadeComprimento(), vm.IdUnidadeAltura()] } });
                ko.applyBindingsToNode(document.getElementById('pnlLocais'), { collapsible: { vmArray: [vm.Locais()] } });
                ko.applyBindingsToNode(document.getElementById('pnlFiscal'), { collapsible: { vmArray: [vm.IdsFiscalClassificacao(), vm.IdFiscalClassificacaoPadrao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlComprasRecebimento'), { collapsible: { vmArray: [vm.SolicitaCompraAutomatica(), vm.IdsFiscalClassificacao(), vm.IdFiscalClassificacaoPadrao(), vm.LoteValidade.LoteRecebimentoTipo(), vm.LoteValidade.IdsRegraRecebimento(), vm.LoteValidade.ControleValidadeRecebimentoTipo(), vm.LoteValidade.ValorUnidadeRecebimento(), vm.LoteValidade.IdUnidadeValidadeRecebimento()] } });
                ko.applyBindingsToNode(document.getElementById('pnlLoteRecebimento'), { collapsible: { vmArray: [vm.LoteValidade.IdsRegraRecebimento(), vm.LoteValidade.LoteRecebimentoTipo(), vm.LoteValidade.ControleValidadeRecebimentoTipo(), vm.LoteValidade.ValorUnidadeRecebimento()] } });
                ko.applyBindingsToNode(document.getElementById('pnlEstimativaProducao'), { collapsible: { vmArray: [vm.UnidadeEstimativaTipo()] } });
                ko.applyBindingsToNode(document.getElementById('pnlLoteProducao'), { collapsible: { vmArray: [vm.LoteValidade.IdsRegraProducao(), vm.LoteValidade.ValorUnidadeProducao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlProducao'), { collapsible: { vmArray: [vm.PermitePesagem(), vm.UnidadeEstimativaTipo(), vm.LoteValidade.IdsRegraProducao(), vm.LoteValidade.ValorUnidadeProducao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlUsoConsumo'), { collapsible: { vmArray: [vm.PermiteFracionarUsoConsumo(), vm.RestricaoAcessoConsumo.ControlaTempoReentrada(), vm.RestricaoAcessoConsumo.ValorTempoReentrada(), vm.RestricaoAcessoConsumo.IdUnidadeReentrada()] } });
                ko.applyBindingsToNode(document.getElementById('pnlControleAreasAgricolas'), { collapsible: { vmArray: [vm.RestricaoAcessoConsumo.ControlaTempoReentrada(), vm.RestricaoAcessoConsumo.ValorTempoReentrada()] } });
                ko.applyBindingsToNode(document.getElementById('pnlVendasEnvio'), { collapsible: { vmArray: [vm.PermiteFracionarVenda(), vm.IdFiscalClassificacaoPrevista()] } });
                ko.applyBindingsToNode(document.getElementById('pnlControleConsumoHumanoAnimal'), { collapsible: { vmArray: [vm.RestricaoAcessoConsumo.ValorTempoMinimo()] } });
                //ko.applyBindingsToNode(document.getElementById('pnlDescricao'), { collapsible: { vmArray: [vm.PermiteCompra(), vm.Armazenamento(), vm.PermiteProducao(), vm.PermiteUso(), vm.PermiteVenda()] } });
                //ko.applyBindingsToNode(document.getElementById('pnlLoteProducao'), { collapsible: { vmArray: [vm.LoteValidade.IdsRegraProducao(), vm.LoteValidade.ValorUnidadeProducao() ] } });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                //Regras Ações/Mudanças de valores
                // Clona valores da unidade tipo
                vm.ValorPesoBruto.subscribe(function (v) {                    
                    var valorPesoLiquido = ko.utils.unwrapObservable(vm.ValorPesoLiquido);

                    if (valorPesoLiquido == null || Number(valorPesoLiquido) == 0)
                        vm.ValorPesoLiquido(v);
                });
                vm.ValorVolume.subscribe(function (v) {
                    var valorPesoBrutoMetro = ko.utils.unwrapObservable(vm.ValorPesoBrutoMetro);

                    if (valorPesoBrutoMetro == null || Number(valorPesoBrutoMetro) == 0)
                        vm.ValorPesoBrutoMetro(v);
                });
                vm.ValorLargura.subscribe(function (v) {
                    var valorComprimento = ko.utils.unwrapObservable(vm.ValorComprimento);
                    var valorAltura = ko.utils.unwrapObservable(vm.ValorAltura);

                    if (valorComprimento == null || Number(valorComprimento) == 0)
                        vm.ValorComprimento(v);

                    if (valorAltura == null || Number(valorAltura) == 0)
                        vm.ValorAltura(v);
                });

                // Clona unidades selecionadas unidade tipo
                vm.IdUnidadePesoBruto.subscribe(function (v) {
                    var idUnidadePesoLiquido = ko.utils.unwrapObservable(vm.IdUnidadePesoLiquido);

                    if (idUnidadePesoLiquido == null)
                        vm.IdUnidadePesoLiquido(v);
                });
                vm.IdUnidadeVolume.subscribe(function (v) {
                    var idUnidadePesoBrutoMetro = ko.utils.unwrapObservable(vm.IdUnidadePesoBrutoMetro);

                    if (idUnidadePesoBrutoMetro == null)
                        vm.IdUnidadePesoBrutoMetro(v);
                });
                vm.IdUnidadeLargura.subscribe(function (v) {
                    var idUnidadeComprimento = ko.utils.unwrapObservable(vm.IdUnidadeComprimento);
                    var idUnidadeAltura = ko.utils.unwrapObservable(vm.IdUnidadeAltura);

                    if (idUnidadeComprimento == null)
                        vm.IdUnidadeComprimento(v);

                    if (idUnidadeAltura == null)
                        vm.IdUnidadeAltura(v);
                });
                

                ko.computed(function () {
                    if (vm.PermiteCompra() == false) {
                        vm.SolicitaCompraAutomatica(false);
                        vm.IdsFiscalClassificacao(null);
                        vm.IdFiscalClassificacaoPadrao(null);

                        vm.LoteValidade.LoteRecebimentoTipo(null);
                        vm.LoteValidade.IdsRegraRecebimento(null);
                        vm.LoteValidade.ControleValidadeRecebimentoTipo(1);
                        vm.LoteValidade.ValorUnidadeRecebimento(null);
                        vm.LoteValidade.IdUnidadeValidadeRecebimento(null);
                    }

                    if (vm.PermiteProducao() == false) {
                        vm.PermitePesagem(false);
                        vm.UnidadeEstimativaTipo(null);

                        vm.LoteValidade.LoteProducaoTipo(null);
                        vm.LoteValidade.ControleValidadeProducaoTipo(1);
                        vm.LoteValidade.ValorUnidadeProducao(null);
                        vm.LoteValidade.IdUnidadeValidadeProducao(null);
                    }

                    if (vm.PermiteUso() == false) {
                        vm.PermiteFracionarUsoConsumo(false);

                        vm.RestricaoAcessoConsumo.ControlaTempoReentrada(false);
                        vm.RestricaoAcessoConsumo.ValorTempoReentrada(null);
                        vm.RestricaoAcessoConsumo.IdUnidadeReentrada(null);
                    }

                    if (vm.PermiteVenda() == false) {
                        vm.PermiteFracionarVenda(false);
                        vm.PermiteFracionarVenda(false);
                        vm.IdFiscalClassificacaoPrevista(null);

                        vm.RestricaoAcessoConsumo.ControlaTempoMinimo(false);
                        vm.RestricaoAcessoConsumo.ValorTempoMinimo(null);
                        vm.RestricaoAcessoConsumo.IdUnidadeMinimo(null);

                        vm.ValorPesoBruto(null);
                        vm.IdUnidadePesoBruto(null);

                        vm.ValorPesoLiquido(null);
                        vm.IdUnidadePesoLiquido(null);

                        vm.ValorVolume(null);
                        vm.IdUnidadeVolume(null);

                        vm.ValorPesoBrutoMetro(null);
                        vm.IdUnidadePesoBrutoMetro(null);

                        vm.ValorLargura(null);
                        vm.IdUnidadeLargura(null);

                        vm.ValorComprimento(null);
                        vm.IdUnidadeComprimento(null);

                        vm.ValorAltura(null);
                        vm.IdUnidadeAltura(null);
                    }
                });

                //Regra de exibição alterada mais uma vez 30/12/2015
                ko.computed(function () {
                    if (vm.PermiteProducao() == true) {
                        $("#fildsetClasse").css("display", "block");
                        $("#divMateriaPrima").css("display", "block");
                        $("#divProdutoIntermediario").css("display", "block");
                        $("#divProdutoAcabado").css("display", "block");
                    }
                    else if(vm.PermiteProducao() == false)
                    {
                        if (vm.PermiteCompra() == false) {
                            $("#fildsetClasse").css("display", "none");

                            $("#divMateriaPrima").css("display", "none");
                            $("#divProdutoIntermediario").css("display", "none");
                            $("#divProdutoAcabado").css("display", "none");
                        }
                        else if (vm.PermiteCompra() == true)
                        {
                            $("#fildsetClasse").css("display", "block");
                            $("#divMateriaPrima").css("display", "block");
                            //$("#divProdutoIntermediario").css("display", "block");
                            //$("#divProdutoAcabado").css("display", "block");
                        }
                    }
                });
                vm.RestricaoAcessoConsumo.ControlaTempoReentrada.subscribe(function (newValue) {
                    if (newValue == true)
                        vm.RestricaoAcessoConsumo.IdUnidadeReentrada(72);
                    else {
                        vm.RestricaoAcessoConsumo.ValorTempoReentrada(null);
                        vm.RestricaoAcessoConsumo.IdUnidadeReentrada(null);
                    }
                });
                vm.RestricaoAcessoConsumo.ControlaTempoMinimo.subscribe(function (newValue) {
                    if (newValue == true)
                        vm.RestricaoAcessoConsumo.IdUnidadeMinimo(72);
                    else {
                        vm.RestricaoAcessoConsumo.ValorTempoMinimo(null);
                        vm.RestricaoAcessoConsumo.IdUnidadeMinimo(null);
                    }
                });

                

                //Exibição dos Campos/Painéis de Ações
                if (vm.PermiteCompra() == true) {
                    $("#fildsetClasse").css("display", "block");
                    $("#divMateriaPrima").css("display", "block");
                }
                else {
                    if (vm.PermiteProducao() == false)
                        $("#fildsetClasse").css("display", "none");
                }

                if (vm.PermiteProducao() == true) {
                    $("#fildsetClasse").css("display", "block");
                    $("#divMateriaPrima").css("display", "block");
                    $("#divProdutoIntermediario").css("display", "block");
                    $("#divProdutoAcabado").css("display", "block");
                }
                else {
                    if (vm.PermiteCompra() == false)
                        $("#fildsetClasse").css("display", "none");

                    $("#divProdutoIntermediario").css("display", "none");
                    $("#divProdutoAcabado").css("display", "none");
                }

            });

            return request;
        };

    return {
        bind: bind
    };
});
