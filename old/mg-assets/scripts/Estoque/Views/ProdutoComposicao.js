﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (produtoComposicao, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            //ProdutoCaracterizacaoAcoes
            produtoComposicao.config({
                saveUrl: '/Estoque/Produto/ProdutoComposicao',
                getUrl: '/Estoque/Produto/ProdutoComposicaoJson',
            });
            var request = produtoComposicao.get({ id: id }),
                form = $('#form-ProdutoComposicao-NewEdit');

            request.done(function (response) {

                Route.setHeaderInfo(response.Descricao);

                vm = ko.mapping.fromJS(response);

                var vmComposicaoExtend = function (vm) {                   
                    vm.PermiteAlteracaoQuantidade.subscribe(function (value) {
                        debugger;
                        if (!value) {
                            vm.ValorVariacao(null);
                            vm.IdUnidadeValorVariacao(null);
                        } else {                            
                            if (vm.IdProdutoServico() != null) {
                                $.post('/Estoque/ProdutoServico/ListarProdutoEPrincipioAtivo', JSON.stringify({
                                    IdProdutoServico: vm.IdProdutoServico(),
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                                    }
                                })).success(function (data) {
                                    if (data.Data[0].IdUnidade != null) {
                                        debugger;
                                        var xyx = data.Data[0];
                                        var ids = [];
                                        ids[0] = xyx.IdUnidade;
                                        ids[1] = 91 // Procentagem Desenvolvimento
                                        //ids[2] = 96 // Procentagem Homologação

                                        vm.IdUnidadeValor(xyx.IdUnidade);
                                        vm.IdUnidadeIn(ids);
                                        vm.IdUnidadeValorVariacao(xyx.IdUnidade);
                                    }
                                });
                            }
                        }
                    });

                    ko.computed(function () {
                        
                        if (vm.IdProdutoServico() != null) {
                            $.post('/Estoque/ProdutoServico/ListarProdutoEPrincipioAtivo', JSON.stringify({
                                IdProdutoServico: vm.IdProdutoServico(),
                                Natureza: 3,
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                                }
                            })).success(function (data) {
                                if (data.Data[0].IdUnidade != null) {
                                    var xyx = data.Data[0];
                                    var ids = [];
                                    ids[0] = xyx.IdUnidade;
                                    ids[1] = 91 // Procentagem

                                    vm.IdUnidadeValor(xyx.IdUnidade);
                                    vm.IdUnidadeIn(ids);
                                    vm.IdUnidadeValorVariacao(xyx.IdUnidade);
                                }
                            });
                        }

                    });
                    vm.IdProdutoServico.subscribe(function (newValue) {
                        if (newValue == null) {
                            vm.IdUnidadeValor(null);
                            vm.Valor(null);
                            vm.IdUnidadeValorVariacao(null);
                            vm.ValorVariacao(null);
                        }
                    });
                };
                window.vmComposicaoExtend = vmComposicaoExtend;

                var vmDescricaoProdutoComposicao = {
                    UnidadeSigla: ko.observable(''),
                    UnidadeSiglaVariacao: ko.observable('')
                };
                window.vmDescricaoProdutoComposicao = vmDescricaoProdutoComposicao;

                vm.Save = function () {
                    //if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    produtoComposicao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Composição do produto salva com sucesso.');
                    });
                }; 

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;

        };

    return {
        bind: bind
    };
});