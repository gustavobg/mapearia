﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'mask-decimal', 'jquery-inputmask', 'gridview', 'select2', 'unidade-tipo', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (produtoDadosBasicos, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            produtoDadosBasicos.config({
                saveUrl: '/Estoque/Produto/ProdutoDadosBasicos',
                getUrl: '/Estoque/Produto/ProdutoDadosBasicosJson',
            });
            var request = produtoDadosBasicos.get({ id: id }),
                form = $('#form-ProdutoDadosBasicos-NewEdit');

            request.done(function (response) {

                var vmDescricaoFormaApresentacao = {
                    Descricao: ko.observable('')
                };
                var vmDescricaoProdutoServicoMarca = {
                    Descricao: ko.observable('')
                };

                var vmUnidadeSigla = {
                    Sigla: ko.observable('')
                }

                window.vmDescricaoProdutoServicoMarca = vmDescricaoProdutoServicoMarca;
                window.vmDescricaoFormaApresentacao = vmDescricaoFormaApresentacao;
                window.vmUnidadeSigla = vmUnidadeSigla;

                vm = ko.mapping.fromJS(response);

                //vm.ExibeControlePorRegraRecebimento = ko.observable(false);
                //vm.ExibeTempoRecebimento = ko.observable(false);
                //vm.ExibeControlePorRegraProducao = ko.observable(false);
                //vm.ExibeTempoProducao = ko.observable(false);
                //vm.LoteValidade = ko.observable(vmLoteValidade);
                vm.ExibeQuantidadeEmbalagem = ko.observable(false);

                vm.Save = function () {
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        produtoDadosBasicos.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Produto salvo com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                var vmParams = function () {
                    this.ExibeProdutoServicoFamilia = ko.observable(false);
                    //this.ExibeDados = ko.observable(true);
                    this.ExibeLocais = ko.observable(false);
                    this.ProdutoServicoFamilia = ko.observable();

                    ko.computed(function () {
                        var produto = this.ProdutoServicoFamilia();

                        if (produto && produto.hasOwnProperty('IdUnidade'))
                            vm.IdUnidade(produto.IdUnidade);

                        if (produto && produto.hasOwnProperty('IdUnidadeTipo'))
                            vm.IdUnidadeTipo(produto.IdUnidadeTipo);

                    }, this);
                }
                vmParams = new vmParams();
                window.vmParams = vmParams;

                var vmParamsUnidade = function () {
                    this.UnidadePadrao = ko.observable();

                    ko.computed(function () {
                        var unidade = this.UnidadePadrao();
                        if (unidade && unidade.hasOwnProperty('Sigla'))
                            vmUnidadeSigla.Sigla(unidade.Sigla);
                    }, this);
                }

                vmParamsUnidade = new vmParamsUnidade();
                window.vmParamsUnidade = vmParamsUnidade;

                // inicialização de componentes
                if (!vm.IdProdutoServicoFamilia() === null) {
                    vmParams.ExibeProdutoServicoFamilia(true)
                    //vmParams.ExibeDados(false)
                }
                //ko.computed(function() {
                //    var permiteArmazenamento = vm.Armazenamento();
                //    if (permiteArmazenamento) {
                //        vmParams.ExibeLocais(true);
                //    } else {
                //        vmParams.ExibeLocais(false);
                //        vm.Locais.removeAll();
                //    }
                //});

                // regras para produto/serviço independente ou família

                ko.computed(function () {
                    if (vm.IdProdutoServicoFormaApresentacao() != null) {
                        $.post('/Estoque/ProdutoServicoFormaApresentacao/List', JSON.stringify({
                            IdProdutoServicoFormaApresentacao: vm.IdProdutoServicoFormaApresentacao(),
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao asc'
                            }
                        })).success(function (data) {
                            vmDescricaoFormaApresentacao.Descricao(data.Data[0].Sigla);
                            if (!data.Data[0].PermiteVariosItens) {
                                vm.Quantidade(1);
                                vm.ExibeQuantidadeEmbalagem(false);
                            }
                            else
                                vm.ExibeQuantidadeEmbalagem(true);
                        });
                    }
                }); //Busca Informações sobre referentes a Forma de Apresentação.

                ko.computed(function () {
                    if (vm.ProdutoServicoIndependente()) {
                        // independente
                        vmParams.ExibeProdutoServicoFamilia(false); // este campo também atribui/remove obrigatoriedade
                        //vmParams.ExibeDados(true); // ao exibir dados atribui-se obrigatoriedade aos campos do panel
                        // passo o valor nulo para determinadas propriedades.
                        vm.IdProdutoServicoFamilia(null);
                    }
                    else {
                        // família de produtos
                        //vmParams.ExibeDados(false); // ao esconder dados remove-se a obrigatoriedade dos campos do panel
                        vmParams.ExibeProdutoServicoFamilia(true); // este campo também atribui/remove obrigatoriedade
                        // passo o valor nulo para determinadas propriedades
                        vm.IdUnidadeTipo(null);
                        vm.IdUnidade(null);
                        //vm.Quantidade(null);
                    }
                });

                var vmNomenclatura = ko.computed(function () {
                    if (vm.Nomenclatura() != null || vm.Nomenclatura() != '') {
                        var descricaoReduzida = vm.DescricaoReduzida() == null ? '' : vm.DescricaoReduzida();
                        var modelo = vm.Modelo() == null ? '' : vm.Modelo();
                        var formaApresentacao = vmDescricaoFormaApresentacao.Descricao() == null ? '' : vmDescricaoFormaApresentacao.Descricao();
                        var marca = vmDescricaoProdutoServicoMarca.Descricao() == null ? '' : vmDescricaoProdutoServicoMarca.Descricao();
                        var descricao = "";

                        switch (vm.Nomenclatura()) {
                            case "1":
                                {
                                    descricao = descricaoReduzida;
                                    if (modelo.trim())
                                        descricao += " - " + modelo;
                                    if (marca.trim())
                                        descricao += " - " + marca;
                                    if (formaApresentacao.trim())
                                        descricao += " (" + formaApresentacao + ")";
                                }
                                break;
                            case "2":
                                {
                                    descricao = descricaoReduzida;
                                    if (modelo.trim())
                                        descricao += " - " + modelo;
                                    if (formaApresentacao.trim())
                                        descricao += " (" + formaApresentacao + ")";
                                    if (marca.trim())
                                        descricao += " - " + marca;
                                }
                                break;
                            case "3":
                                {
                                    descricao = descricaoReduzida;

                                    if (marca.trim())
                                        descricao += " - " + marca;
                                }
                                break;
                            case "4":
                                {
                                    descricao = descricaoReduzida;

                                    if (modelo.trim())
                                        descricao += " - " + modelo;
                                }
                                break;
                            case "5":
                                {
                                    descricao = descricaoReduzida;
                                }
                                break;
                            case "6":
                                {
                                    descricao = descricaoReduzida;

                                    if (modelo.trim())
                                        descricao += " - " + modelo;
                                    if (marca.trim())
                                        descricao += " - " + marca;
                                }
                                break;
                            case "7":
                                {
                                    descricao = descricaoReduzida;

                                    if (marca.trim())
                                        descricao += " - " + marca;
                                    if (modelo.trim())
                                        descricao += " - " + modelo;
                                }
                        }
                        vm.Descricao(descricao)
                    }
                });

                window.vmNomenclatura = vmNomenclatura;

                ko.applyBindingsToNode(document.getElementById('pnlObservacao'), { collapsible: { vmArray: [vm.Observacao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlDescricao'), { collapsible: { vmArray: [] } }); //Não coloquei nenhuma property, pois, o Humberto quer que sempre venha fechado (11/12/2015)
                ko.applyBindingsToNode(document.getElementById('pnlCaracterizacao'), { collapsible: { vmArray: [vm.IdProdutoServicoCaracterizacaoClassificacao(), vm.IdProdutoServicoCaracterizacaoSecao()] } });
                ko.applyBindingsToNode(document.getElementById('pnlCodigosAuxiliares'), { collapsible: { vmArray: [vm.CodigoEAN()] } });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});