﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr',  'partial-pessoa',  'unidade-tipo', 'jquery-flipper', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (produtoFornecedor, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            produtoFornecedor.config({
                saveUrl: '/Estoque/Produto/ProdutoFornecedor',
                getUrl: '/Estoque/Produto/ProdutoFornecedorJson',
            });

            var request = produtoFornecedor.get({ id: id }),
                form = $('#form-ProdutoFornecedor-NewEdit');

            request.done(function (response) {

                Route.setHeaderInfo(response.Descricao);

                vm = ko.mapping.fromJS(response);

                var vmFornecedorExtend = function (vm) {
                    // overwrite properties
                    this.Fornecedor = ko.observable(null);
                    this.PessoaNome = ko.pureComputed(function () {
                        var fornecedor = this.Fornecedor();
                        if (fornecedor && fornecedor.hasOwnProperty('Nome'))
                            return fornecedor.Nome;
                    }, this);
                    this.PessoaNomeSecundario = ko.pureComputed(function () {
                        var fornecedor = this.Fornecedor();
                        if (fornecedor && fornecedor.hasOwnProperty('NomeSecundario'))
                            return fornecedor.NomeSecundario;
                    }, this);
                };
                window.vmFornecedorExtend = vmFornecedorExtend;

                vm.LoteValidade.ExibeControlePorRegraRecebimento = ko.observable(false);
                vm.LoteValidade.ExibeTempoRecebimento = ko.observable(false);
                vm.LoteValidade.ExibeControlePorRegraProducao = ko.observable(false);
                vm.LoteValidade.ExibeTempoProducao = ko.observable(false);                

                vm.ExibeQuantidadeEmbalagem = ko.observable(false);
                vm.Save = function () {
                    //if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    produtoFornecedor.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Produto salvo com sucesso.');
                    });
                };                
                //ko.applyBindingsToNode(document.getElementById('pnlFornecedores'), { collapsible: { vmArray: [vm.Fornecedores()] } });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;

        };

    return {
        bind: bind
    };
});