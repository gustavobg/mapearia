﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'component.colorSelector'], function (produtoLocal, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            produtoLocal.config({
                saveUrl: '/Estoque/ProdutoLocal/NewEdit',
                getUrl: '/Estoque/ProdutoLocal/NewEditJson',
            });
            var request = produtoLocal.get({ id: id }),
                form = $('#form-ProdutoLocal-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.dadosComplAplicacaoTipo = ko.observable({ 'Ativo': true, 'IdAplicacaoTipoIn': '1,2' });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    produtoLocal.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Local de Armazenagem salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});