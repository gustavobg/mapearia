﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'gridview','mask-decimal', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (produtoFamilia, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            produtoFamilia.config({
                saveUrl: '/Estoque/Saldo/ProdutoPorFamilia',
                getUrl: '/Estoque/Saldo/ProdutoPorFamiliaJson',
            });

            var request = produtoFamilia.get({ id: id }),
                form = $('#form-ProdutoPorFamilia-NewEdit');

            request.done(function (response) {
                
                vm = ko.mapping.fromJS(response);
                debugger;
                Route.setHeaderInfo(vm.Header());

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;
        };

    return {
        bind: bind
    };
});