﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'gridview', 'mask-decimal', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (produtoPorLocal, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            produtoPorLocal.config({
                saveUrl: '/Estoque/Saldo/ProdutoPorLocal',
                getUrl: '/Estoque/Saldo/ProdutoPorLocalJson',
            });

            var request = produtoPorLocal.get({ id: id }),
                form = $('#form-ProdutoPorLocal-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                Route.setHeaderInfo(vm.Header());

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

            });

            return request;
        };

    return {
        bind: bind
    };
});