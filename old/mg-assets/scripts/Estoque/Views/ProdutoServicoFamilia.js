﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (familia, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            familia.config({
                saveUrl: '/Estoque/ProdutoServicoFamilia/NewEdit',
                getUrl: '/Estoque/ProdutoServicoFamilia/NewEditJson',
            });
            var request = familia.get({ id: id }),
                form = $('#form-ProdutoServicoFamilia-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    familia.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Família de Produto salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});