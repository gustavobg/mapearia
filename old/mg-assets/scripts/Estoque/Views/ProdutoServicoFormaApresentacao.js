﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (formaApresentacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            formaApresentacao.config({
                saveUrl: '/Estoque/ProdutoServicoFormaApresentacao/NewEdit',
                getUrl: '/Estoque/ProdutoServicoFormaApresentacao/NewEditJson',
            });
            var request = formaApresentacao.get({ id: id }),
                form = $('#form-ProdutoServicoFormaApresentacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    formaApresentacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Embalagem salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});