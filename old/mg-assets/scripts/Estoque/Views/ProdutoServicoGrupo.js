﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'button'], function (produtoServicoGrupo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            produtoServicoGrupo.config({
                saveUrl: '/Estoque/ProdutoServicoGrupo/NewEdit',
                getUrl: '/Estoque/ProdutoServicoGrupo/NewEditJson',
            });
            var request = produtoServicoGrupo.get({ id: id }),
                form = $('#form-ProdutoServicoGrupo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.DadosComplementares = ko.observable({ 'Ativo': true, 'UltimoNivel': false, 'SemRelacionamentoComProduto': true, 'IdGrupoExcecao': vm.IdProdutoServicoGrupoPai(), 'IdProdutoServicoGrupoNotIn': vm.IdProdutoServicoGrupo() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    produtoServicoGrupo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Grupo de Produto salvo com sucesso.');
                    });
                }

                vm.UltimoNivel.subscribe(function (newValue) {
                    if (newValue == false)
                        vm.Natureza(null);
                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});