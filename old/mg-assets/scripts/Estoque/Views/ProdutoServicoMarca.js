﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (marca, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            marca.config({
                saveUrl: '/Estoque/ProdutoServicoMarca/NewEdit',
                getUrl: '/Estoque/ProdutoServicoMarca/NewEditJson',
            });
            var request = marca.get({ id: id }),
                form = $('#form-ProdutoServicoMarca-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    marca.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Marca salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});