﻿define(['crud-controller', 'knockout', 'feedback', 'moment', 'bootbox', 'toastr', 'jquery-inputmask', 'button', 'select2', 'gridview', 'datetimepicker', 'mask-decimal', 'jquery-flipper', 'ko-validate-rules', 'ko-validate', 'jquery-qtip', 'panels'], function (requisicao, ko, mgFeedbackBase, moment, bootbox, toastr) {

    var vm = {},
        bind = function (id) {
            requisicao.config({
                saveUrl: '/Estoque/Requisicao/NewEdit',
                getUrl: '/Estoque/Requisicao/NewEditJson'
            });

            var request = requisicao.get({ id: id }),
                form = $('#form-EstoqueRequisicao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                var AplicacaoTipo = 0;
                var IdAplicacaoReferencia = 0;
                var IdEmpresaUsuaria = 0;

                vm.PermiteRequisitar = ko.observable(true);
                vm.PermiteEntregar = ko.observable(true);
                vm.RealizaAprovacao = ko.observable(false);

                vm.PessoaRequisitanteVM = ko.observable();
                vm.AplicacaoVM = ko.observable();
                //vm.DadosPessoaRequisitanteVM = ko.observable();
                vm.DocumentoTipoDadosCompleObservable = ko.observable({ 'Ativo': true, 'IdDocumentoTipoIn': '34,46' });
                vm.AplicacaoDadosCompleObservable = ko.observable({ 'Ativo': true, 'IdAplicacaoTipoIn': '1,2,3,4,5' });
                vm.dadosCompleRequisitante = ko.observable({ 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false, 'Ativo': true, 'IdPessoaNatureza': 1, 'PessoaPerfil_PermiteRealizarCompra': true, 'IdPessoaNaturezaNotIn': '4' });
                vm.ExibeObjetoSemAplicacao = ko.observable(false);
                vm.ItemSelecionado = ko.observable({ lstEntregaRealizada: ko.observableArray([]), ExibeAdicionarNovo: ko.observable(true) });
                vm.ItemValido = ko.observable(false);
                vm.preSelecaoRequisitante = function (data, dfd) {
                    // sobrescrita do objeto options do select2
                    var d = data.Data[0].IdPessoa;
                    vm.dadosCompleRequisitante({ 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false, 'Ativo': true, 'IdPessoaNatureza': 1, 'IdPessoaNotIn': d, 'PessoaPerfil_PermiteRealizarCompra': true, 'IdPessoaNaturezaNotIn': '4' });
                    dfd.resolve({});
                }
                vm.dadosCompleRequisicaoOrigem = ko.observable({ 'Ativo': true, 'IdProcessoSituacaoIn': '31,32' });

                //Empresa Usuária
                ko.computed(function () {
                    if (vm.IdPessoa() != null)
                        IdEmpresaUsuaria = vm.IdPessoa();
                });

                ko.computed(function () {
                    if (vm.PessoaRequisitanteVM() != null) {
                        if (vm.PessoaRequisitanteVM().PermiteRealizarEntrega != null)
                            vm.PermiteEntregar(vm.PessoaRequisitanteVM().PermiteRealizarEntrega);
                        else
                            vm.PermiteEntregar(true);

                        if (vm.PessoaRequisitanteVM().PermiteSolicitarRequisitar != null)
                            vm.PermiteRequisitar(vm.PessoaRequisitanteVM().PermiteSolicitarRequisitar);
                        else
                            vm.PermiteRequisitar(true);
                    }
                    else {
                        vm.PermiteRequisitar(true);
                        vm.PermiteEntregar(true)
                    }

                    if (vm.PermiteRequisitar() == false) {
                        alert('Não pode Inserir itens');
                    }
                    if (vm.PermiteEntregar() == false) {
                        alert('Não Permite entregar itens');
                    }
                });

                //Verifica se Requisição irá passar por fluxo de Aprovação
                ko.computed(function () {

                    if (vm.IdDocumentoTipo() != null) {

                        //localiza Transacao e SubProcesso
                        switch (vm.IdDocumentoTipo()) {
                            case '34':
                                vm.IdTransacao(46);
                                vm.IdSubProcesso(13);
                                break;
                            case '46':
                                vm.IdTransacao(41);
                                vm.IdSubProcesso(14);
                        }

                        //Verifica se passará por Aprovação
                        //ko.computed(function () {
                        //    if (vm.IdSubProcesso() != null) {
                        //        $.ajax({
                        //            url: '/Corporativo/AprovacaoProcessoConfiguracao/List',
                        //            type: 'post',
                        //            dataType: 'json',
                        //            data: JSON.stringify({
                        //                IdSubProcesso: vm.IdSubProcesso(),
                        //                Page: {
                        //                    PageSize: 99999, CurrentPage: 1, OrderBy: 'IdAprovacaoProcessoConfiguracao asc'
                        //                }
                        //            }),
                        //            async: false,
                        //            success: function (data) {
                        //                if (data.Data[0] != null) {
                        //                    vm.RealizaAprovacao(data.Data[0].RealizaAprovacao);
                        //                    vm.IdProcessoSituacao(28);
                        //                }
                        //                else
                        //                {
                        //                    vm.RealizaAprovacao(false);

                        //                }

                        //            }
                        //        });
                        //    }
                        //});
                    }
                });

                //Aplicação - Validação e Exibição de campos
                ko.computed(function () {
                    if (vm.IdAplicacao() != null)
                        vm.ExibeObjetoSemAplicacao(false);
                    else
                        vm.ExibeObjetoSemAplicacao(true);
                });
                vm.IdAplicacao.subscribe(function (newValue) {
                    vm.IdObjeto(null);
                });

                //Validação Requisição Origem
                ko.computed(function () {
                    if (vm.IdDocumentoTipo() != null && vm.IdDocumentoTipo() != 34)
                        vm.IdEstoqueRequisicaoOrigem(null);
                });

                //AplicacaoVM => Manipulação
                ko.computed(function () {
                    if (vm.AplicacaoVM() != null) {
                        if (vm.AplicacaoVM().IdAplicacaoTipo != null) {
                            AplicacaoTipo = vm.AplicacaoVM().IdAplicacaoTipo;
                            IdAplicacaoReferencia = vm.AplicacaoVM().IdAplicacaoReferencia;
                        }
                    }
                });

                var idAplicacaoOld = ko.unwrap(vm.IdAplicacao);
                vm.IdAplicacao.subscribe(function (v) {
                    var itens = ko.unwrap(vm.lstItem);
                    if (idAplicacaoOld !== null && idAplicacaoOld != v && itens.length > 0) {
                        bootbox.confirm('Ao alterar a aplicação os itens serão removidos e terão de ser inseridos novamente, deseja continuar?', function (resp) {
                            if (resp) {
                                vm.lstItem([]);
                            } else {
                                vm.IdAplicacao(idAplicacaoOld);
                            }
                        });
                    }
                    idAplicacaoOld = v;
                });

                ko.computed(function () {
                    if (vm.IdEstoqueRequisicaoOrigem() != null && vm.IdTransacao() == 46) {
                        $.ajax({
                            url: '/Estoque/Requisicao/ListItensPorRequisicao',
                            type: 'post',
                            dataType: 'json',
                            data: JSON.stringify({
                                IdEstoqueRequisicao: vm.IdEstoqueRequisicaoOrigem(),
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'IdEstoqueRequisicao asc'
                                }
                            }),
                            async: false,
                            success: function (data) {
                                if (data.Data[0] != null) {
                                    vm.RealizaAprovacao(data.Data != null);
                                    vm.lstItem(data.Data);
                                }
                                else {
                                    alert("Erro");
                                    //TODO: Gustavo
                                }
                            }
                        });
                    }
                });

                // ** Regras do panel de Requisicao e Lista de Entregas

                var vmItemExtended = function (vmItem) {
                    // Cria propriedade caso não exista lstEntregaRealizada
                    if (!vmItem.hasOwnProperty('lstEntregaRealizada')) {
                        vmItem.lstEntregaRealizada = ko.observableArray([]);
                    } else {
                        if (ko.unwrap(vmItem.lstEntregaRealizada).length > 0) {
                            window.setTimeout(function () {
                                // Abre painel de entrega realizada se existirem entregas para serem exibidas
                                window.PanelSet.open(null, 1);
                            }, 100);
                        }
                    }

                    // campos de regras de tela					
                    this.EditaItemRequisicao = ko.observable(true);
                    this.ExibeEntregaRapida = ko.observable(true);
                    this.EntregaTotal = ko.observable(true);
                    this.SaldoEntregar = ko.observable(100); // TODO: Éverton irá implementar                    
                    this.UsuarioPermiteEntrega = ko.observable(vm.PermiteEntregar()); // TODO: Éverton irá implementar, restrição de usuário     //DONE: Implementado.
                    this.UsuarioPermiteRequisicao = ko.observable(vm.PermiteRequisitar()); // TODO: Éverton irá implementar, restrição de usuário  //DONE: Implementado.                      
                    vmItem.ExibeAdicionarNovo = ko.observable(true);
                    vmItem.ExibeQuantidadeRequisicao = ko.observable(true);
                    this.RequisicaoRealizada = ko.observable(false);
                    this.dadosCompleUnidadeItem = ko.observable();
                    this.ControlaSaldo = ko.observable(false);
                    // ---  


                    /*
                	 * Regra para sinalizar que uma requisição já foi movimentada
                	 *	a) TODO: Éverton verificar. Utilizei como critério se a propriedade IdEstoqueRequisicaoItem não está nula, corrigir se for o caso                	 
                	 */
                    ko.computed(function () {
                        this.RequisicaoRealizada(ko.unwrap(vmItem.IdEstoqueRequisicaoItem) !== null);
                    }, this);

                    /* TODO: Campo Entrega total terá de possuir regra para ajustar valor SIM/NÃO de entregas já movimentadas, provavelmente observar o id do status
					 * Utilizei temporariamente um valor da descrição só pra regra constar aqui, depois alterar para campos com os ids respectivos
					 */ //DONE: Adicionei a regra do Usuário Permite Entregar.
                    ko.computed(function () {
                        var situacao = ko.unwrap(vmItem.Situacao);
                        var requisicaoRealizada = ko.unwrap(this.RequisicaoRealizada);
                        var usuarioPermiteEntrega = ko.unwrap(this.UsuarioPermiteEntrega); //NEW

                        //NEW
                        if (!usuarioPermiteEntrega) {
                            this.EntregaTotal(false);
                        } else if (requisicaoRealizada === true) {
                            if (situacao === 'Entregue Parcial' || situacao === '' || situacao === null) {
                                this.EntregaTotal(false);
                            }
                        } else {
                            this.EntregaTotal(true);
                        }
                    }, this);

                    /*
                	 * Regra para botão "adicionar novo" no panel de entregas, exibe se:
                	 *	a) TODO: Éverton implementar. Usuário permite entregar // DONE: Regra implementada
                	 *	b) Saldo a entregar for > 0  //DONE: Gustavo, retirei essa regra do Saldo + Permite Entrega, pois, se não pode entregar devemos esquecer do saldo. ?! Num sei ?!
                	 */
                    ko.computed(function () {
                        var usuarioPermiteEntrega = ko.unwrap(this.UsuarioPermiteEntrega);
                        var saldoEntregar = ko.unwrap(this.SaldoEntregar);

                        if (!usuarioPermiteEntrega) {
                            vmItem.ExibeAdicionarNovo(false);
                        } else if (usuarioPermiteEntrega && saldoEntregar > 0) {
                            vmItem.ExibeAdicionarNovo(true);
                        } else {
                            vmItem.ExibeAdicionarNovo(false);
                        }
                    }, this);

                    /*
                	 *  Regra ALPHA: -> Tem nome pq faço uma referência dela em seguida.
                	 *  Regra para habilitar "edição de requisição", será verdadeira se:
                	 *	a) TODO: Éverton implementar. Usuário permite requisitar //DONE
                	 *	b) Se usuario escolher "SIM" no campo entrega total (desde que esse botão esteja disponível, descrito nas regras de ExibeEntregaRapida)
                	 *  c) TODO: Éverton implementar. Se não foi realizado a entrega total da requisição (após movimentado o estoque)
                	 *  d) TODO: Éverton implementar. Se usuário não tiver atingindo limite da cota de requisição 
                	 */
                    ko.computed(function () {
                        //var EntregaTotal = ko.unwrap(this.EntregaTotal);
                        var possuiEntregas = ko.unwrap(vmItem.lstEntregaRealizada).length > 0;
                        var usuarioPermiteRequisicao = ko.unwrap(this.UsuarioPermiteRequisicao);
                        var requisicaoRealizada = ko.unwrap(this.RequisicaoRealizada);

                        if (usuarioPermiteRequisicao && !possuiEntregas && !requisicaoRealizada) {
                            this.EditaItemRequisicao(true);
                        } else {
                            this.EditaItemRequisicao(false);
                        }
                    }, this);


                    /* Éverton, são 19:45 e vou sair de férias.
                	 * Aqui, tem que fazer uma regra que não permite que valor seja menor ou inferir a "soma das entregas realizadas", ou seja, não pode alterar requisição para menos do que já foi entregue.
                	 * Além de tudo isso, cumprir os requisitos estabelecidos na regra ALPHA
                	 */
                    vmItem.SaldoEntregarRegra = ko.computed(function () {
                        var requisicaoRealizada = ko.unwrap(this.RequisicaoRealizada);
                        var possuiEntregas = ko.unwrap(vmItem.lstEntregaRealizada).length > 0;
                        var saldoEntregar = ko.unwrap(this.SaldoEntregar);
                        var usuarioPermiteRequisicao = ko.unwrap(this.UsuarioPermiteRequisicao);

                        // TODO;
                        return 0;
                    });

                    /*
                	 * Regra para habilitar modo leitura de "Quantidade da requisição", será verdadeira se:                	 
                	 *	a) Se as regras descritas em ALPHA foram cumrpidas EditaItemRequisicao                	          	
                	 */
                    vmItem.ExibeQuantidadeRequisicao = ko.computed(function () {
                        var saldoEntregar = ko.unwrap(this.SaldoEntregar);
                        var usuarioPermiteRequisicao = ko.unwrap(this.UsuarioPermiteRequisicao);
                        var situacao = ko.unwrap(vmItem.Situacao);

                        if (situacao === 'Entregue Parcial' || situacao === null) {
                            if (usuarioPermiteRequisicao && saldoEntregar > 0) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }

                    }, this);

                    /*
                	 * Regra para habilitar exibição de "Entrega rápida", será verdadeira se:                	 
                	 *	a) TODO: Éverton implementar. Usuário permite entregar //DONE
                	 *	b) TODO: Éverton implementar. Se não foi realizado a entrega total da requisição (após movimentado o estoque)
                	 *  c) Se não foi realizada alguma entrega parcial (após movimentado o estoque)
                	 *  d) TODO: Éverton implementar. Se usuário não tiver atingindo limite da cota de entrega
                	 *  e) Se campos de requisição estão válidos
                	 */
                    ko.computed(function () {
                        var usuarioPermiteEntrega = ko.unwrap(this.UsuarioPermiteEntrega);
                        var requisicaoRealizada = ko.unwrap(this.RequisicaoRealizada);
                        var saldoEntregar = ko.unwrap(this.SaldoEntregar);
                        var possuiEntregas = ko.unwrap(vmItem.lstEntregaRealizada).length > 0;

                        //NEW
                        if (!usuarioPermiteEntrega) {
                            this.ExibeEntregaRapida(false);
                        } else if (requisicaoRealizada) {
                            if (usuarioPermiteEntrega && saldoEntregar > 0 && !possuiEntregas) {
                                this.ExibeEntregaRapida(true);
                            } else {
                                this.ExibeEntregaRapida(false);
                            }
                        } else {
                            this.ExibeEntregaRapida(true);
                        }

                    }, this);

                    ko.computed(function () {
                        vm.ItemSelecionado(ko.unwrap(vmItem));
                    }).extend({
                        notify: 'always', rateLimit: 500, deferred: true
                    });

                    this.ProdutoServicoVM = ko.observable();
                    this.DadosCompleSubAplicacao = ko.observable();
                    this.UnidadeProdutoServicoItemVM = ko.observable();
                    this.dadosCompleProduto = ko.observable({ 'Ativo': true, 'IdProdutoServicoGrupoNotIn': '', 'Armazenamento': true, 'PermiteUso': true });

                    //Busca Quantidade por Forma de Apresentanção (Embalagem)
                    vmItem.QtdePorEmbalagemProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('Quantidade')) {
                            return produtoServico.Quantidade;
                        }
                    }, this);

                    vmItem.QtdeFormaApresentacaoSaldo = ko.observable();
                    vmItem.QtdeProdutoServicoSaldo = ko.observable();

                    // Calculo Quantidade por Forma de Aprensentação x Qtde Produto.
                    vmItem.QtdeProdutoServico = ko.computed({
                        read: function () {
                            return parseFloat(vmItem.QtdeProdutoServicoFormaApresentacao()) * parseFloat(vmItem.QtdePorEmbalagemProdutoServico());
                        },
                        write: function (value) {
                            var qtdeProduto = parseFloat(value);
                            var qtdePorEmbalgem = parseFloat(vmItem.QtdePorEmbalagemProdutoServico());

                            vmItem.QtdeProdutoServicoFormaApresentacao(qtdeProduto / qtdePorEmbalgem);
                        }
                    });

                    //Retorna Saldo por Empresa Usuária Origem, Local de Armazenagem Origem, Produto e Lote[Lote = TODO]
                    ko.computed(function () {
                        if (vmItem.IdProdutoServico() != null && vmItem.IdProdutoLocal() != null) {
                            $.ajax({
                                url: '/Estoque/ProdutoSaldo/List',
                                type: 'post',
                                dataType: 'json',
                                data: JSON.stringify({
                                    IdPessoa: IdEmpresaUsuaria,
                                    IdProdutoLocal: vmItem.IdProdutoLocal(),
                                    IdProdutoServico: vmItem.IdProdutoServico(),
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'IdProdutoLocal asc'
                                    }
                                }),
                                async: false,
                                success: function (data) {
                                    if (data.Data[0] != null) {
                                        vmItem.QtdeFormaApresentacaoSaldo(data.Data[0].QtdeProdutoServicoFormaApresentacao);
                                        vmItem.QtdeProdutoServicoSaldo(data.Data[0].QtdeUnidadeProdutoServico);
                                    }
                                }
                            });
                        }
                    });


                    // TODO: implementar busca dos itens atraves da VM 'vm.dadosCompleRequisitante' 
                    this.ExibeSubAplicacao = ko.pureComputed(function () {
                        var idAplicacao = ko.unwrap(vm.IdAplicacao);
                        return idAplicacao != null && idAplicacao.length > 0;
                    });

                    ko.computed(function () {
                        if (ko.unwrap(this.EntregaTotal) === false) {
                            this.UsuarioPermiteEntrega = ko.observable(vm.PermiteEntregar());
                            window.PanelSet.open(null, 1);
                        } else {
                            this.UsuarioPermiteEntrega = ko.observable(vm.PermiteEntregar());
                            window.PanelSet.close(null, 1);
                        }
                    }, this);

                    vmItem.IdProdutoServicoFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();

                        if (produtoServico && produtoServico.hasOwnProperty('IdProdutoServicoFormaApresentacao')) {
                            return produtoServico.IdProdutoServicoFormaApresentacao;
                        }
                    }, this);
                    vmItem.IdUnidadeProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('IdUnidade')) {
                            vmItem.dadosCompleUnidadeItem({ 'Ativo': true, 'IdUnidadeTipo': produtoServico.IdUnidadeTipo });
                            return produtoServico.IdUnidade;
                        }
                    }, this);

                    // Requisitado
                    vmItem.Requisitado = ko.pureComputed(function () {
                        if (vmItem.QtdeProdutoServicoFormaApresentacao() != null && vmItem.SiglaProdutoServicoFormaApresentacao() != null)
                            return vmItem.QtdeProdutoServicoFormaApresentacao() + ' ' + vmItem.SiglaProdutoServicoFormaApresentacao();
                    }, this);

                    if (AplicacaoTipo > 0 && IdAplicacaoReferencia > 0) {
                        this.DadosCompleSubAplicacao({
                            'Ativo': true, 'IdAplicacaoTipoIn': AplicacaoTipo, 'IdAplicacaoReferenciaIn': IdAplicacaoReferencia
                        });
                    }

                    vmItem.CasasDecimaisUnidadeProduto = ko.pureComputed(function () {
                        var unidadeProduto = this.UnidadeProdutoServicoItemVM();
                        if (unidadeProduto != null && unidadeProduto.hasOwnProperty('CasasDecimais'))
                            return unidadeProduto.CasasDecimais;
                    }, this);
                    vmItem.CasasDecimaisFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && (produtoServico.hasOwnProperty('CasasDecimaisFormaApresentacao') && produtoServico.hasOwnProperty('PermiteFracionarFormaApresentacao'))) {
                            {
                                if (produtoServico.PermiteFracionarFormaApresentacao === true) {
                                    if (produtoServico.CasasDecimaisFormaApresentacao != null)
                                        return produtoServico.CasasDecimaisFormaApresentacao;
                                    else
                                        return 0;
                                }
                                else
                                    return 0;
                            }
                        }
                        else
                            return 3;
                    }, this);

                    this.ControlaSaldo = ko.pureComputed(function () {
                        debugger;
                        var produtoServico = this.ProdutoServicoVM();

                        if ((vmItem.DescricaoProdutoServico() === null || vmItem.DescricaoProdutoServico() == '') && produtoServico != null) {
                            vmItem.DescricaoProdutoServico(produtoServico.Descricao);
                        }
                        
                        if (produtoServico && (produtoServico.hasOwnProperty('ControlaSaldo') && produtoServico.hasOwnProperty('ControlaSaldo'))) {
                            return produtoServico.ControlaSaldo;
                        }
                        else
                            return false;
                    }, this);

                    //ko.computed(function () {
                    //    var produtoServico = this.ProdutoServicoVM();
                    //    if ((vmItem.DescricaoProdutoServico() === null || vmItem.DescricaoProdutoServico() == '') && produtoServico != null) {
                    //        vmItem.DescricaoProdutoServico(produtoServico.Descricao);
                    //    }
                    //});
                };

                // ** Regras do modal da Entrega

                var vmEntregaExtended = function (vmEntrega) {
                    var setValue = function (prop, valor) {
                        var valorCampo = ko.unwrap(prop);
                        if (valorCampo == null) {
                            prop(valor);
                        }
                    }
                    var item = ko.mapping.toJS(vm.ItemSelecionado);

                    // campos de regras de tela
                    vmEntrega.UsuarioPermiteEntrega = ko.observable(vm.PermiteEntregar()); // TODO: Éverton irá implementar, restrição de usuário //DONE                 	
                    vmEntrega.SaldoEntregar = ko.observable(100); // TODO: Éverton irá implementar

                    /*
                     * Regra para "item de lista de entregas" permitir ser editável:
                     *	a) Item não pode ter sido salvo (movimentado)
                     *	b) Usuário deve ter permissão para entrega
                     */
                    setValue(vmEntrega.QtdeDescricaoProdutoServico, item.QtdeProdutoServicoFormaApresentacao);
                    setValue(vmEntrega.QtdeDescricaoProdutoServicoFormaApresentacao, item.QtdeProdutoServicoFormaApresentacao);
                    setValue(vmEntrega.IdProdutoServicoFormaApresentacao, item.IdProdutoServicoFormaApresentacao);
                    setValue(vmEntrega.IdUnidadeProdutoServico, item.IdUnidadeProdutoServico);
                    setValue(vmEntrega.QtdeProdutoServicoFormaApresentacao, item.QtdeDescricaoProdutoServicoFormaApresentacao);
                    setValue(vmEntrega.QtdeProdutoServico, item.QtdeDescricaoProdutoServico);
                    setValue(vmEntrega.ExibicaoEstoqueRequisicaoItem, item.Requisitado);
                    setValue(vmEntrega.ExibicaoEstoqueRequisicaoItem, item.IdProdutoServico);
                }

                window.vmItemExtended = vmItemExtended;
                window.vmEntregaExtended = vmEntregaExtended;

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) {
                        return;
                    };
                    var data = ko.toJSON(vm);

                    requisicao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Requisição salva com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                var panelSetVm = {
                    Requisicao: {
                        saveItem: function (templateContext) {

                            var ItemContextoGrid = ko.unwrap(templateContext).itemSelected();

                            if (!ItemContextoGrid.isValidShowErrors())
                                return;

                            window.setTimeout(function () {
                                PanelSet.closeAll();
                            }, 100);

                            var ItemSelecionado = ko.unwrap(vm.ItemSelecionado);
                            var EntregaTotal = ko.unwrap(ItemSelecionado.EntregaTotal);

                            if (EntregaTotal && ItemSelecionado.lstEntregaRealizada().length === 0) {
                                var item = ko.mapping.toJS(ItemSelecionado);
                                item.DataHoraExibicao = moment().format('DD/MM/YYYY HH:mm');
                                item.DescricaoProdutoServicoLote = '';
                                item.QtdeDescricaoProdutoServico = item.QtdeProdutoServicoFormaApresentacao;
                                item.QtdeDescricaoProdutoServicoFormaApresentacao = item.QtdeProdutoServicoFormaApresentacao;
                                item.ExibicaoEstoqueRequisicaoItem = item.Requisitado;
                                item.NomeSecundarioPessoaEntrega = '';
                                ItemContextoGrid.lstEntregaRealizada([item]);
                                toastr.success('Requisição e entrega de itens cadastradas com sucesso');
                            } else {
                                toastr.success('Requisição de itens cadastrada com sucesso');
                            }

                        },
                        beforeClose: function (templateContext) {
                            window.setTimeout(function () {
                                PanelSet.closeAll();
                            }, 100);
                        }
                    },
                    Entrega: {
                        itemSelected: vm.ItemSelecionado,
                        habilitaEntrega: ko.computed(function () {
                            return vm.ItemSelecionado().hasOwnProperty('isValid') ? vm.ItemSelecionado().isValid() : false;
                        }),
                        //EntregaStatus: ko.computed(function () {
                        //	return vm.ItemSelecionado().hasOwnProperty('EntregaStatus') ? ko.unwrap(vm.EntregaStatus) : false;
                        //}),
                        //EntregaStatus: ko.observable(false),
                        saveItem: function () {
                        }
                    }
                }
                window.panelSetVm = panelSetVm;
                ko.applyBindings(panelSetVm, document.getElementById('panels-requisicao-bind'));


                ko.computed(function () {
                    var item = ko.unwrap(vm.ItemSelecionado);
                    var isValid = ko.unwrap(item.isValid);
                    var lstEntregaLength = ko.unwrap(item.lstEntregaRealizada).length;

                    vm.ItemValido(isValid && lstEntregaLength === 0);

                }).extend({ deferred: true, rateLimit: 50 });

                ko.computed(function () {
                    var item = ko.unwrap(vm.ItemSelecionado);
                    var isValid = ko.unwrap(item.isValid);
                    vm.ItemValido(isValid);
                }).extend({ deferred: true, rateLimit: 50 });

            });

            return request;
        };

    return {
        bind: bind
    }
});