﻿define(['crud-controller', 'knockout', 'feedback', 'moment', 'jquery-inputmask', 'button', 'select2', 'gridview', 'datetimepicker', 'mask-decimal', 'jquery-flipper', 'ko-validate-rules', 'ko-validate', 'jquery-qtip'], function (saldo, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            saldo.config({
                saveUrl: '/Estoque/SaldoInicial/NewEdit',
                getUrl: '/Estoque/SaldoInicial/NewEditJson'
            });

            var request = saldo.get({ id: id }),
                form = $('#form-SaldoInicial-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                vm.IdTransacao(52);
                vm.DisableEmpresaUsuaria = ko.observable(vm.IdEstoqueSaldoInicial() != null ? true : false);


                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    saldo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Saldo Inicial salvo com sucesso.');
                    });
                };

                var vmItemExtended = function (vm) {
                    this.ProdutoServicoVM = ko.observable();
                    this.UnidadeProdutoVM = ko.observable();
                    this.FinanceiroMoedaVM = ko.observable();

                    vm.QtdeProdutoServico = ko.computed({
                        read: function () {
                            return parseFloat(vm.QtdeProdutoServicoFormaApresentacao()) * parseFloat(vm.QtdePorEmbalagemProdutoServico());
                        },
                        write: function (value) {
                            var qtdeProduto = parseFloat(value);
                            var qtdePorEmbalgem = parseFloat(vm.QtdePorEmbalagemProdutoServico());

                            vm.QtdeProdutoServicoFormaApresentacao(qtdeProduto / qtdePorEmbalgem);
                        }
                    });

                    // calcular valor total
                    ko.computed(function () {
                        var valorUnitario = Number(ko.unwrap(vm.ValorUnitario));
                        var quantidade = Number(ko.unwrap(vm.QtdeProdutoServico));

                        if (valorUnitario > 0 && quantidade > 0 && !ko.computedContext.isInitial()) {
                            vm.ValorTotal(valorUnitario * quantidade);
                        }
                    }, this);

                    // calcular valor unitario
                    ko.computed(function () {
                        var valorTotal = ko.unwrap(vm.ValorTotal);
                        var quantidade = Number(vm.QtdeProdutoServico.peek());

                        if (valorTotal != null && Number(valorTotal) != 0) {
                            vm.ValorUnitario(valorTotal / quantidade);
                        }
                    });

                    vm.IdProdutoServicoFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('IdProdutoServicoFormaApresentacao')) {
                            return produtoServico.IdProdutoServicoFormaApresentacao;
                        }
                    }, this);
                    vm.IdUnidadeProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('IdUnidade')) {
                            return produtoServico.IdUnidade;
                        }
                    }, this);
                    vm.CasasDecimaisFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && (produtoServico.hasOwnProperty('CasasDecimaisFormaApresentacao') && produtoServico.hasOwnProperty('PermiteFracionarFormaApresentacao'))) {
                            {
                                if (produtoServico.PermiteFracionarFormaApresentacao === true) {
                                    if (produtoServico.CasasDecimaisFormaApresentacao != null)
                                        return produtoServico.CasasDecimaisFormaApresentacao;
                                    else
                                        return 0;
                                }
                                else
                                    return 0;
                            }
                        }
                        else
                            return 0;
                    }, this);
                    vm.CasasDecimaisUnidadeProduto = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        var unidadeProduto = this.UnidadeProdutoVM();

                        if (produtoServico && produtoServico.hasOwnProperty('PermiteFracionarUnidadeControle')) {
                            {
                                if (produtoServico.PermiteFracionarUnidadeControle === true) {
                                    if (unidadeProduto && unidadeProduto.hasOwnProperty('CasasDecimais')) {
                                        if (unidadeProduto.CasasDecimais != null)
                                            return unidadeProduto.CasasDecimais;
                                        else
                                            return 0;
                                    }
                                    else
                                        return 0;

                                }
                                else return 0;
                            }
                        }
                        else
                            return 0;
                    }, this);
                    vm.CasasDecimaisFinanceiroMoeda = ko.pureComputed(function () {
                        var moeda = this.FinanceiroMoedaVM();
                        if (moeda && moeda.hasOwnProperty('CasasDecimais')) {
                            return moeda.CasasDecimais;
                        }
                    }, this);

                    vm.QtdePorEmbalagemProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('Quantidade')) {
                            return produtoServico.Quantidade;
                        }
                    }, this);

                    //Validação de campos
                    vm.IdProdutoServico.subscribe(function (newValue) {
                        vm.IdProdutoLocal(null);
                    });
                }

                ko.bindingHandlers.dicaSaldo = {
                    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                        var $element = $(element),
                            value = allBindings().value,
                            $html = $('<div id="popover-mes" class="panel panel-warning">' +
                                          '<div class="panel-body">' +
                                                'Atenção! A quantidade de itens é maior do que zero, <br /> porém o valor total é 0.' +
                                          '</div>' +
                                          //'<div class="panel-footer">' +
                                          //'</div>' +
                                      '</div>'),
                            $footer = $html.find('.panel-footer'),
                            //$action = $('<button class="btn-primary btn btn-action">Corrigir o valor total</button> ').on('click', function () { action(value, element); }).appendTo($footer),
                            //$ignore = $('<button class="btn-default  btn btn-ignore">Entendi</button> ').on('click', function () { ignore(value, element); }).appendTo($footer),
                            options = {
                                style: { classes: 'qtip-panel qtip-popover qtip-shadow' },
                                position: { my: 'bottom left', at: 'top right' },
                                content: { text: $html },
                                show: { event: 'focus', solo: true }
                            };

                        $element.qtip($.extend(defaults.qtip.popover, options));

                        function ignore(value, element) {
                            var $element = $(element),
                                api = $element.qtip('api');

                            api.toggle(false);
                        }
                        function action(value, element) {
                            var api = $(element).qtip('api');
                            api.toggle(false);
                            value('');
                            //element.focus();
                        }
                    },
                    update: function (element, valueAccessor, allBindings, viewModel) {
                        var value = allBindings().value,
							valorTotal = ko.unwrap(valueAccessor().valorTotal),
							valorTotalInt = Number(valorTotal),
							quantidade = Number(ko.unwrap(valueAccessor().quantidade)),
                            $element = $(element),
                            $formGroup = $element.closest('.form-group'),
                            api = $element.qtip('api'),
                            isInitial = ko.computedContext.isInitial();

                        api.disable(true);

                        if (valorTotal != null && quantidade > 0 && valorTotalInt === 0) {
                            if (!isInitial) {
                                api.disable(false);
                                api.toggle(true);
                                $formGroup.addClass('has-warning');
                            }
                        } else {
                            api.toggle(false);
                            api.disable(true);
                            $formGroup.removeClass('has-warning');
                        }
                    }
                }

                ko.bindingHandlers.dicaMes = {
                    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                        var $element = $(element),
                            value = allBindings().value,
                            $html = $('<div id="popover-mes" class="panel panel-warning">' +
                                          '<div class="panel-body">' +
                                                'A data de lançamento esta inferior a 30 dias da data atual. Deseja corrigir a data informada ou ignorar?' +
                                          '</div>' +
                                          '<div class="panel-footer">' +
                                          '</div>' +
                                      '</div>'),
                            $footer = $html.find('.panel-footer'),
                            $action = $('<button class="btn-primary btn btn-action">Corrigir a data</button> ').on('click', function () { action(value, element); }).appendTo($footer),
                            $ignore = $('<button class="btn-default btn btn-ignore">Ignorar</button> ').on('click', function () { ignore(value, element); }).appendTo($footer),
                            options = {
                                style: { classes: 'qtip-panel qtip-popover qtip-shadow' },
                                position: { my: 'bottom left', at: 'top right' },
                                content: { text: $html },
                                show: { event: 'focus', solo: true }
                            };

                        $element.qtip($.extend(defaults.qtip.popover, options));

                        function ignore(value, element) {
                            var $element = $(element),
                                api = $element.qtip('api');

                            api.toggle(false);
                        }
                        function action(value, element) {
                            var api = $(element).qtip('api');
                            api.toggle(false);
                            value('');
                            element.focus();
                        }
                    },
                    update: function (element, valueAccessor, allBindings) {
                        var value = allBindings().value,
                            $element = $(element),
                            $formGroup = $element.closest('.form-group'),
                            api = $element.qtip('api'),
                            isInitial = ko.computedContext.isInitial();

                        api.disable(true);

                        if (value() != null && value() != '' && moment(value(), 'DD/MM/YYYY').isBefore(moment().subtract(30, 'day'))) {
                            if (!isInitial) {
                                api.disable(false);
                                api.toggle(true);
                                $formGroup.addClass('has-warning');
                            }
                        } else {
                            api.toggle(false);
                            api.disable(true);
                            $formGroup.removeClass('has-warning');
                        }

                    }
                }

                window.vmItemExtended = vmItemExtended;

                window.vm = vm;
                ko.applyBindings(vm, form[0]);



            });

            return request;
        };

    return {
        bind: bind
    }
});