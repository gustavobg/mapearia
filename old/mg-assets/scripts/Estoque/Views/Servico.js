﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'gridview', 'mask-decimal'], function (servico, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            servico.config({
                saveUrl: '/Estoque/Servico/NewEdit',
                getUrl: '/Estoque/Servico/NewEditJson',
            });
            var request = servico.get({ id: id }),
                form = $('#form-Servico-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                var vmImpostoExtend = function (vm) {
                    ko.computed(function () {
                        if (vm.ReducaoBase() != null)
                            vm.ReducaoBaseDescricao(vm.ReducaoBase() + "%");
                        if (vm.ReducaoImposto() != null)
                            vm.ReducaoImpostoDescricao(vm.ReducaoImposto() + "%");
                    });
                };

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    servico.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Serviço salvo com sucesso.');
                    });
                }

                window.vmImpostoExtend = vmImpostoExtend;
                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlObservacao'), { collapsible: { vmArray: [vm.Observacao()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});