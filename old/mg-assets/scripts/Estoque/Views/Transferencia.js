﻿define(['crud-controller', 'knockout', 'feedback', 'moment', 'jquery-inputmask', 'button', 'select2', 'gridview', 'datetimepicker', 'mask-decimal', 'jquery-flipper', 'ko-validate-rules', 'ko-validate', 'jquery-qtip'], function (transferencia, ko, mgFeedbackBase, moment) {

    var vm = {},
        bind = function (id) {
            transferencia.config({
                saveUrl: '/Estoque/Transferencia/NewEdit',
                getUrl: '/Estoque/Transferencia/NewEditJson'
            });

            var request = transferencia.get({ id: id }),
                form = $('#form-EstoqueTransferencia-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                var IdEmpresaUsuariaOrigem = 0;
                var IdProdutoLocalOrigem = 0;

                //Array para Restrições
                vm.arrEmpresaOrigemProdutoLocalRestricao = ko.observable();
                vm.arrEmpresaDestinoProdutoLocalRestricao = ko.observable();
                vm.arrUsuarioProdutoLocalRestricao = ko.observable();
                vm.arrUsuarioProdutoServicoGrupoRestricao = ko.observable();
                vm.arrUsuarioProdutoServicoRestricao = ko.observable();
                vm.arrUsuarioPessoaEmpresaRestricao = ko.observable();

                vm.IdDocumentoTipo(35); //Transferência entre Locais

                vm.TransacaoDadosCompleObservable = ko.observable({ 'Ativo': true, 'IdTransacaoIn': '43,44,55' });
                vm.ProdutoLocalOrigem = ko.observable();
                vm.ProdutoLocalDestino = ko.observable();

                vm.infoOrigemMovel = ko.observable();
                vm.infoOrigemTerceiros = ko.observable();

                vm.infoDestinoMovel = ko.observable();
                vm.infoDestinoTerceiros = ko.observable();

                //Locais
                vm.dadosComplProdutoLocalOrigem = ko.observable();
                vm.dadosComplProdutoLocalDestino = ko.observable();

                //Empresas
                vm.dadosComplEmpresaOrigem = ko.observable();
                vm.dadosComplEmpresaDestino = ko.observable();

                ko.computed(function () {
                    if (vm.IdProdutoLocalOrigem() != null)
                        IdProdutoLocalOrigem = vm.IdProdutoLocalOrigem();

                    if (vm.IdPessoaEmpresaOrigem() != null)
                        IdEmpresaUsuariaOrigem = vm.IdPessoaEmpresaOrigem();
                });

                // Local de Origem
                ko.computed(function () {
                    if (vm.ProdutoLocalOrigem() != null) {
                        // Local Móvel
                        if (vm.ProdutoLocalOrigem().LocalMovel == true)
                            vm.infoOrigemMovel('Sim');
                        else if (vm.ProdutoLocalOrigem().LocalMovel == false)
                            vm.infoOrigemMovel('Não');
                        else
                            vm.infoOrigemMovel('');

                        //Pertence a Terceiros
                        if (vm.ProdutoLocalOrigem().LocalPertenceTerceiro == true)
                            vm.infoOrigemTerceiros('Sim');
                        else if (vm.ProdutoLocalOrigem().LocalPertenceTerceiro == false)
                            vm.infoOrigemTerceiros('Não');
                        else
                            vm.infoOrigemTerceiros('');
                    }

                });
                // Local Destino
                ko.computed(function () {
                    if (vm.ProdutoLocalDestino() != null) {
                        // Local Móvel
                        if (vm.ProdutoLocalDestino().LocalMovel == true)
                            vm.infoDestinoMovel('Sim');
                        else if (vm.ProdutoLocalDestino().LocalMovel == false)
                            vm.infoDestinoMovel('Não');
                        else
                            vm.infoDestinoMovel('');


                        //Pertence a Terceiros
                        if (vm.ProdutoLocalDestino().LocalPertenceTerceiro == true)
                            vm.infoDestinoTerceiros('Sim');
                        else if (vm.ProdutoLocalDestino().LocalPertenceTerceiro == false)
                            vm.infoDestinoTerceiros('Não');
                        else
                            vm.infoDestinoTerceiros('');
                    }
                });

                //Empresas
                ko.computed(function () {

                    if (vm.IdPessoaRequisitante() != null) {
                        var arr = new Array();

                        if (vm.IdPessoaRequisitante() != null) {
                            $.ajax({
                                url: '/Core/Restricao/RestricaoPorUsuario',
                                type: 'post',
                                dataType: 'json',
                                data: JSON.stringify({
                                    IdPessoa: vm.IdPessoaRequisitante(),
                                    LocalizarUsuarioPelaPessoa: true,
                                    TipoIn: Array("5", "6"), //Apenas Tipo 5 e 6 [Produto e Serviço, Empresa]
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'IdPessoa asc'
                                    }
                                }),
                                async: false,
                                success: function (data) {
                                    if (data.Data != null) {
                                        var info = data.Data;

                                        //Produto Local Restrição
                                        if (info.UsuarioProdutoLocalRestricao != null) {
                                            arr = new Array();
                                            for (var i = 0; i < info.UsuarioProdutoLocalRestricao.length; i++) {
                                                arr[i] = info.UsuarioProdutoLocalRestricao[i].IdProdutoLocal;
                                            }
                                            if (arr != null) {
                                                vm.arrUsuarioProdutoLocalRestricao(arr.join());
                                            }
                                        }

                                        //Produto Serviço Restriçao
                                        if (info.UsuarioProdutoServicoGrupoRestricao != null) {
                                            arr = new Array();
                                            for (var i = 0; i < info.UsuarioProdutoServicoGrupoRestricao.length; i++) {
                                                arr[i] = info.UsuarioProdutoServicoGrupoRestricao[i].IdProdutoServicoGrupo;
                                            }
                                            if (arr != null) {
                                                vm.arrUsuarioProdutoServicoGrupoRestricao(arr.join());
                                            }
                                        }

                                        //Produto Restrição
                                        if (info.UsuarioProdutoServicoRestricao != null) {
                                            arr = new Array();
                                            for (var i = 0; i < info.UsuarioProdutoServicoRestricao.length; i++) {
                                                arr[i] = info.UsuarioProdutoServicoRestricao[i].IdProdutoServico;
                                            }
                                            if (arr != null) {
                                                vm.arrUsuarioProdutoServicoRestricao(arr.join());
                                            }
                                        }

                                        //Pessoa Empresa (Empresa Usuária)
                                        if (info.UsuarioPessoaEmpresaRestricao != null) {
                                            arr = new Array();
                                            for (var i = 0; i < info.UsuarioPessoaEmpresaRestricao.length; i++) {
                                                arr[i] = info.UsuarioPessoaEmpresaRestricao[i].IdPessoa;
                                            }
                                            if (arr != null) {
                                                vm.arrUsuarioPessoaEmpresaRestricao(arr.join());
                                            }
                                        }

                                    }
                                }
                            });
                        }
                        else {
                            //vm.dadosComplProdutoLocalDestino({ 'Ativo': true });
                        }
                    }

                    if (vm.IdPessoaEmpresaOrigem() != null) {
                        debugger;
                        //Retorna Locais que a Empresa selecionada poderá ter restrições
                        ko.computed(function () {
                            var arr = new Array();
                            if (vm.IdPessoaEmpresaOrigem() != null) {
                                $.ajax({
                                    url: '/Estoque/ProdutoLocal/ListLocaisPorEmpresaUsuaria',
                                    type: 'post',
                                    dataType: 'json',
                                    data: JSON.stringify({
                                        IdPessoa: vm.IdPessoaEmpresaOrigem(),
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'IdPessoa asc'
                                        }
                                    }),
                                    async: false,
                                    success: function (data) {
                                        if (data.Data[0] != null) {
                                            for (var i = 0; i < data.Data.length; i++) {
                                                arr[i] = data.Data[i].IdPessoa;
                                            }
                                            if (arr != null) {
                                                debugger;
                                                vm.arrEmpresaOrigemProdutoLocalRestricao(arr);
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                vm.dadosComplProdutoLocalOrigem({ 'Ativo': true });
                            }
                        });
                    }

                    if (vm.IdPessoaEmpresaDestino() != null) {
                        debugger;
                        //Retorna Locais que a Empresa selecionada poderá ter restrições
                        ko.computed(function () {
                            var arr = new Array();
                            if (vm.IdPessoaEmpresaDestino() != null) {
                                $.ajax({
                                    url: '/Estoque/ProdutoLocal/ListLocaisPorEmpresaUsuaria',
                                    type: 'post',
                                    dataType: 'json',
                                    data: JSON.stringify({
                                        IdPessoa: vm.IdPessoaEmpresaDestino(),
                                        Page: {
                                            PageSize: 99999, CurrentPage: 1, OrderBy: 'IdPessoa asc'
                                        }
                                    }),
                                    async: false,
                                    success: function (data) {
                                        if (data.Data[0] != null) {
                                            for (var i = 0; i < data.Data.length; i++) {
                                                arr[i] = data.Data[i].IdPessoa;
                                            }
                                            if (arr != null) {
                                                vm.arrEmpresaDestinoProdutoLocalRestricao(arr);
                                            }
                                        }
                                    }
                                });
                            }
                            else {
                                vm.dadosComplProdutoLocalDestino({ 'Ativo': true });
                            }
                        });
                    }

                });

                ko.computed(function () {

                    if (vm.arrUsuarioPessoaEmpresaRestricao() != null) {
                        vm.dadosComplEmpresaOrigem({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaNotIn': vm.arrUsuarioPessoaEmpresaRestricao() });
                        vm.dadosComplEmpresaDestino({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false, 'IdPessoaNotIn': vm.arrUsuarioPessoaEmpresaRestricao() });
                    }
                    else {
                        vm.dadosComplEmpresaOrigem({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false });
                        vm.dadosComplEmpresaDestino({ 'Ativo': true, 'ComPessoa': false, 'ComEmpresaUsuaria': true, 'ComEmpresa': false });
                    }

                    //Estrutura o dados complementares Complentares Local Destino
                    var localDestinoNotIn = (vm.arrEmpresaDestinoProdutoLocalRestricao() != null && vm.arrEmpresaDestinoProdutoLocalRestricao().length > 0) ? vm.arrEmpresaDestinoProdutoLocalRestricao().join() : '';
                    if (localDestinoNotIn != null || localDestinoNotIn != "") {
                        if (vm.arrUsuarioProdutoLocalRestricao() != null && vm.arrUsuarioProdutoLocalRestricao() != "")
                            localDestinoNotIn = localDestinoNotIn.length > 0 ? (localDestinoNotIn + ',' + vm.arrUsuarioProdutoLocalRestricao()) : vm.arrUsuarioProdutoLocalRestricao();

                        vm.dadosComplProdutoLocalDestino({ 'Ativo': true, 'IdProdutoLocalNotIn': localDestinoNotIn });
                    }
                    else {

                        vm.dadosComplProdutoLocalDestino({ 'Ativo': true });
                    }

                    var localOrigemNotIn = (vm.arrEmpresaOrigemProdutoLocalRestricao() != null && vm.arrEmpresaOrigemProdutoLocalRestricao().length > 0) ? vm.arrEmpresaOrigemProdutoLocalRestricao().join() : '';
                    if (localOrigemNotIn != null || localOrigemNotIn != "") {

                        if (vm.arrUsuarioProdutoLocalRestricao() != null && vm.arrUsuarioProdutoLocalRestricao() != '')
                            localOrigemNotIn = localDestinoNotIn.length > 0 ? (localDestinoNotIn + ',' + vm.arrUsuarioProdutoLocalRestricao()) : vm.arrUsuarioProdutoLocalRestricao();

                        vm.dadosComplProdutoLocalOrigem({ 'Ativo': true, 'IdProdutoLocalNotIn': localOrigemNotIn });
                    }
                    else {
                        vm.dadosComplProdutoLocalOrigem({ 'Ativo': true });
                    }

                });

                var vmItemExtended = function (vm) {
                    this.ProdutoServicoVM = ko.observable();
                    this.UnidadeProdutoVM = ko.observable();
                    vm.QtdeFormaApresentacaoSaldo = ko.observable();
                    vm.QtdeProdutoServicoSaldo = ko.observable();
                    vm.unidadeProdutoDadosComple = ko.observable();
                    this.dadosComplProduto = ko.observable({ 'Ativo': true });

                    //Realiza Calculo Qtde x Forma Apresentação
                    vm.QtdeProdutoServico = ko.computed({
                        read: function () {
                            return parseFloat(vm.QtdeFormaApresentacao()) * parseFloat(vm.QtdePorEmbalagemProdutoServico());
                        },
                        write: function (value) {
                            var qtdeProduto = parseFloat(value);
                            var qtdePorEmbalgem = parseFloat(vm.QtdePorEmbalagemProdutoServico());

                            vm.QtdeFormaApresentacao(qtdeProduto / qtdePorEmbalgem);
                        }
                    });

                    vm.IdProdutoServicoFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();

                        if (produtoServico && produtoServico.hasOwnProperty('IdProdutoServicoFormaApresentacao')) {
                            return produtoServico.IdProdutoServicoFormaApresentacao;
                        }
                    }, this);
                    vm.unidadeProdutoDadosComple = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('IdUnidade')) {
                            return { 'Ativo': true, 'IdUnidadeTipo': produtoServico.IdUnidadeTipo };
                        }

                    }, this);
                    vm.CasasDecimaisFormaApresentacao = ko.pureComputed(function () {
                        var produtoServico = vm.ProdutoServicoVM();
                        if (produtoServico && (produtoServico.hasOwnProperty('CasasDecimaisFormaApresentacao') && produtoServico.hasOwnProperty('PermiteFracionarFormaApresentacao'))) {
                            {
                                if (produtoServico.PermiteFracionarFormaApresentacao === true) {
                                    if (produtoServico.CasasDecimaisFormaApresentacao != null)
                                        return produtoServico.CasasDecimaisFormaApresentacao;
                                    else
                                        return 0;
                                }
                                else
                                    return 0;
                            }
                        }
                        else
                            return 0;
                    }, this);
                    vm.CasasDecimaisUnidadeProduto = ko.pureComputed(function () {
                        var produtoServico = vm.ProdutoServicoVM();
                        var unidadeProduto = vm.UnidadeProdutoVM();

                        if (produtoServico && produtoServico.hasOwnProperty('PermiteFracionarUnidadeControle')) {
                            {
                                if (produtoServico.PermiteFracionarUnidadeControle === true) {
                                    if (unidadeProduto && unidadeProduto.hasOwnProperty('CasasDecimais')) {
                                        if (unidadeProduto.CasasDecimais != null)
                                            return unidadeProduto.CasasDecimais;
                                        else
                                            return 0;
                                    }
                                    else
                                        return 0;

                                }
                                else return 0;
                            }
                        }
                        else
                            return 0;
                    }, this);
                    vm.QtdePorEmbalagemProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('Quantidade')) {
                            return produtoServico.Quantidade;
                        }
                    }, this);
                    vm.IdUnidadeProdutoServico = ko.pureComputed(function () {
                        var produtoServico = this.ProdutoServicoVM();
                        if (produtoServico && produtoServico.hasOwnProperty('IdUnidade'))
                            return produtoServico.IdUnidade;
                    }, this);

                    //Retorna Saldo por Empresa Usuária Origem, Local de Armazenagem Origem, Produto e Lote[Lote = TODO]
                    ko.computed(function () {
                        debugger;
                        if (vm.IdProdutoServico() != null) {
                            $.ajax({
                                url: '/Estoque/ProdutoSaldo/List',
                                type: 'post',
                                dataType: 'json',
                                data: JSON.stringify({
                                    IdPessoa: IdEmpresaUsuariaOrigem,
                                    IdProdutoLocal: IdProdutoLocalOrigem,
                                    IdProdutoServico: vm.IdProdutoServico(),
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'IdProdutoLocal asc'
                                    }
                                }),
                                async: false,
                                success: function (data) {
                                    debugger;
                                    if (data.Data[0] != null) {
                                        debugger;
                                        vm.QtdeFormaApresentacaoSaldo(data.Data[0].QtdeProdutoServicoFormaApresentacao);
                                        vm.QtdeProdutoServicoSaldo(data.Data[0].QtdeUnidadeProdutoServico);
                                    }
                                }
                            });
                        }
                    });

                    //Conversão da Unidade
                    vm.IdUnidadeProdutoServico.subscribe(function (newValue) {
                        if (vm.QtdeProdutoServico() != null) {
                            var produtoServico = vm.ProdutoServicoVM();
                            if (newValue != null && produtoServico != null) {
                                if (vm.IdProdutoServico() != null) {
                                    $.ajax({
                                        url: '/Estoque/Unidade/RealizaConversaoEntreUnidade',
                                        type: 'post',
                                        dataType: 'json',
                                        data: JSON.stringify({
                                            idUnidadeAtual: newValue,
                                            idUnidadeOriginal: produtoServico.IdUnidade,
                                            qtdeUnidadeAtual: vm.QtdeProdutoServico(),
                                            Page: {
                                                PageSize: 99999, CurrentPage: 1, OrderBy: 'IdUnidadeAtual asc'
                                            }
                                        }),
                                        async: false,
                                        success: function (data) {
                                            if (data != null) {
                                                vm.QtdeProdutoServico(data);
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    });
                }

                window.vmItemExtended = vmItemExtended;
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    transferencia.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Transferência salva com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});