﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'mask-decimal'], function (unidade, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            unidade.config({
                saveUrl: '/Estoque/Unidade/NewEdit',
                getUrl: '/Estoque/Unidade/NewEditJson',
            });
            var request = unidade.get({ id: id }),
                form = $('#form-EstoqueUnidade-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    unidade.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Unidade salva com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});