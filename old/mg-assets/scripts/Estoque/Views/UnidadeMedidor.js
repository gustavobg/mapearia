﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (unidadeMedidor, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            unidadeMedidor.config({
                saveUrl: '/Estoque/UnidadeMedidor/NewEdit',
                getUrl: '/Estoque/UnidadeMedidor/NewEditJson',
            });
            var request = unidadeMedidor.get({ id: id }),
                form = $('#form-UnidadeMedidor-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    unidadeMedidor.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Medidor de Unidade salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});