﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr',  'alteraCEP',  'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'datetimepicker', 'ko-validate-rules', 'ko-validate'], function (unidadeRendimento, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            unidadeRendimento.config({
                saveUrl: '/Estoque/UnidadeRendimento/NewEdit',
                getUrl: '/Estoque/UnidadeRendimento/NewEditJson',
            });
            var request = unidadeRendimento.get({ id: id }),
                form = $('#form-UnidadeRendimento-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    var data = ko.toJSON(vm);

                    if (vm.isValid()) {
                        unidadeRendimento.save(data).done(function (result, status, xhr) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Unidade de Rendimento salva com sucesso.');
                        })
                    } else {
                        vm.showErrors();
                    }
                }

                ko.validate.setValidationProperties(vm);
                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});