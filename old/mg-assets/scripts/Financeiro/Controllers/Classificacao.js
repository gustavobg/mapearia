﻿define(['util', 'jquery'], function (mg, $) {

    // Financeiro/Classificacao

    var saveUrl = '/Financeiro/Classificacao/NewEdit',
        getUrl = '/Financeiro/Classificacao/NewEditJson',
        get = function (data) {
            return mg.getViewModel($.extend(true, { id: mg.getId(data), url: getUrl }, data));
        },
        save = function (data) {
            // deffered
            return $.ajaxJsonAntiforgery(Route.getToken(), {
                data: data, //ko.toJSON(vm),
                url: saveUrl // saveNew ? saveNewEditUrl : newEditUrl,            
            });
        }
    return {
        get: get,
        save: save
    }

});