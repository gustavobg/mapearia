﻿define(['knockout'], function (ko) {

    'use strict';

    var retorno = function (o) {
        // configurações
        var defaults = {
            porcentagemFixa: true
        };

        o = ko.utils.extend(defaults, o);

        var oldValorAtual = 0;
        var RealizarCalculoPercentual = function (valor, percentual) {
            return (valor * percentual) / 100;
        };
        var verificaValor = function (valor, valorAnterior, valorAtual, valorAtualAnterior) {
            if ((valor != null || valor != '') && valorAtual != null && valorAnterior != valor) {
                return true;
            } else {
                return false;
            }
        };
        this.calculaValorPercentual = function (oldValor, oldPercentual, obsValor, obsPercentual, vm) {
            var valorAtual = ko.unwrap(vm.ValorAtual),
            valor = ko.unwrap(obsValor),
            percentual = ko.unwrap(obsPercentual),
            resultado;

            percentual = isNaN(Number(percentual)) ? 0 : Number(percentual);
            valor = isNaN(Number(valor)) ? 0 : Number(valor);

            if (valorAtual != oldValorAtual) {
                var tmpPercentual = (valor * 100) / valorAtual,
                    tmpValor = RealizarCalculoPercentual(valorAtual, percentual);

                // Os valores serão atualizados com base no percentual ou valor
                if (o.porcentagemFixa === true) {
                    obsValor(tmpValor);
                } else {
                    obsPercentual(tmpPercentual);
                }

                percentual = tmpPercentual;
                valor = tmpValor;

            } else {

                if (verificaValor(percentual, oldPercentual, valorAtual, oldValorAtual)) {
                    // verifica se o percentual de desconto foi alterado
                    resultado = RealizarCalculoPercentual(valorAtual, percentual);                  
                    obsValor(resultado);
                    valor = resultado;

                } else if (verificaValor(valor, oldValor, valorAtual, oldValorAtual)) {
                    // verifica se o valor de desconto foi alterado para alterar o percentual
                    resultado = (valor * 100) / valorAtual;

                    obsPercentual(resultado);
                    percentual = resultado;
                }
            }

            oldValorAtual = valorAtual;

            return {
                percentual: percentual,
                valor: valor
            }
        };
    };

    return retorno;

})