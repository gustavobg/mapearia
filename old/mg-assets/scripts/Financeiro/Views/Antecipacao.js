﻿define(['crud-controller', 'knockout', 'feedback', 'button', 'gridview', 'datetimepicker', 'mask-decimal', 'select2', 'jquery-flipper', 'ko-validate-rules', 'ko-validate'], function (antecipacao, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            antecipacao.config({
                saveUrl: '/Financeiro/Antecipacao/NewEdit',
                getUrl: '/Financeiro/Antecipacao/NewEditJson'
            });

            var request = antecipacao.get({ id: id }),
                form = $('#form-FinanceiroAntecipacao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    debugger;
                    if (vm.TipoMovimentoFinanceiro() == 1) {
                        if (vm.IdProcessoSituacao() != null && vm.IdProcessoSituacao() != 2)
                            form.addClass("read-mode panel-document");
                    }
                    else if (vm.TipoMovimentoFinanceiro() == 2) {
                        if (vm.IdProcessoSituacao() != null && vm.IdProcessoSituacao() != 7)
                            form.addClass("read-mode panel-document");
                    }

                });


                //Dados Complementares
                vm.ExibeInformacaoCotacao = ko.observable(false);
                vm.ExibeContaDetalhesPagamento = ko.observable(false);
                //vm.ExibeDestinatarioPagamento = ko.observable(false);
                vm.ExibeCartaoFinanceiro = ko.observable(false);
                vm.ExibeCodigoBarra = ko.observable(false);
                vm.TipoOperacao = ko.observable(false);

                vm.TipoMovimentoFinanceiro.subscribe(function (newValue) {
                    vm.IdDocumentoTipo(null);
                });

                function ListaUltimaCotacao(idMoeda, idMoedaReferencia) {
                    var resultado = '';
                    $.ajax({
                        url: '/Financeiro/Cotacao/ListaUltimaCotacaoPorMoeda',
                        type: 'post',
                        data: JSON.stringify({
                            IdFinanceiroMoeda: idMoeda,
                            IdFinanceiroMoedaReferencia: idMoedaReferencia,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'IdFinanceiroCotacaoMoeda asc'
                            }
                        }),
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            resultado = data.Data[0].ValorCotacao;
                        }
                    });

                    return resultado;
                }


                //Exibição e Obrigatoriedade dos Valores de Cotação
                ko.computed(function () {
                    if (vm.IdFinanceiroMoedaAntecipacao() != vm.IdFinanceiroMoedaValorAtual())
                        vm.ExibeInformacaoCotacao(true);
                });

                var vmParam = function () {
                    this.MoedaAntecipacao = ko.observable();
                    this.MoedaValorContabilizacao = ko.observable();
                    this.CasasDecimaisContabilizacao = ko.observable(2);
                    this.PessoaDestinatarioPagamentoVM = ko.observable();
                };
                vmParam = new vmParam();
                window.vmParam = vmParam;

                //Ao alterar a Pessoa, deve-se apagar o Endereço informado. [regra Humberto]
                vm.IdPessoa.subscribe(function (newValue) {
                    vm.IdPessoaEndereco(null);
                });

                //Modal Contabilização
                var vmClassificacaoContabilExtend = function (vm) {

                    //Valores de Exibição
                    ko.computed(function () {

                        //Natureza
                        if (vm.Natureza() == 1)
                            vm.NaturezaDescricao('Somar');
                        else if (vm.Natureza() == 2)
                            vm.NaturezaDescricao('Deduzir');

                        //Casas Decimais para o valor do Título
                        if (vmParam.MoedaValorContabilizacao() != null)
                            vmParam.CasasDecimaisContabilizacao(vmParam.MoedaValorContabilizacao().CasasDecimais);

                        //Exibição do valor do Título formatado.
                        if (vm.ValorTitulo() != null && vm.IdFinanceiroMoeda() != null && vmParam.MoedaValorContabilizacao() != null)
                            vm.ValorTituloExibicao(vmParam.MoedaValorContabilizacao().Sigla + " " + vm.ValorTitulo());
                    });
                };
                window.vmClassificacaoContabilExtend = vmClassificacaoContabilExtend;

                //Formas de Pagamento (Exibição de Itens)
                ko.computed(function () {

                    //Exibe Conta (ddlFinanceiroConta)
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 1 || vm.IdFinanceiroFormaPagamentoPrevista() == 2 || vm.IdFinanceiroFormaPagamentoPrevista() == 5 || vm.IdFinanceiroFormaPagamentoPrevista() == 6 || vm.IdFinanceiroFormaPagamentoPrevista() == 7)
                        vm.ExibeContaDetalhesPagamento(true);
                    else {
                        vm.ExibeContaDetalhesPagamento(false)
                        vm.IdFinanceiroConta(null);
                    }

                    if ((vm.IdFinanceiroFormaPagamentoPrevista() == 2 || vm.IdFinanceiroFormaPagamentoPrevista() == 5) && vm.TipoMovimentoFinanceiro() == 1) {
                        debugger;
                        vm.IdPessoaPagamentoDestinatario(vm.IdPessoa()); //Regra Humberto dia 02/02/2016 [ Trazer por padrão a Pessoa definida no cabeçalho. ]
                    }

                    else {
                        vm.IdPessoaPagamentoDestinatario(null);
                        vm.IdPessoaNatureza(null);
                        vm.DocumentoPessoa(null);
                    }

                    //Exibe Cartão Financeiro
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 3)
                        vm.ExibeCartaoFinanceiro(true);
                    else {
                        vm.ExibeCartaoFinanceiro(false);
                        vm.IdFinanceiroCartao(null);
                    }

                    //Quando Cartão não Financeiro (Econômico)
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 4) {
                        vm.ExibeCartaoFinanceiro(false);
                        vm.ExibeCodigoBarra(false);
                        vm.CodigoBarraTipo(null);
                        vm.CodigoBarra(null);
                    }

                    //Exibe Código de Barra 
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 6) {
                        vm.ExibeCodigoBarra(true);
                        vm.CodigoBarraTipo(1);
                    }
                    else {
                        vm.ExibeCodigoBarra(false);
                        vm.CodigoBarraTipo(null);
                        vm.CodigoBarra(null);
                    }
                });

                ko.computed(function () {
                    if (vmParam.PessoaDestinatarioPagamentoVM() != null && vm.IdPessoaPagamentoDestinatario() != null) {
                        debugger;
                        if (vmParam.PessoaDestinatarioPagamentoVM().IdPessoaNatureza == 1) {
                            vm.IdPessoaNatureza(1);
                            vm.DocumentoPessoa(vmParam.PessoaDestinatarioPagamentoVM().Documento1);
                        }
                        else if (vmParam.PessoaDestinatarioPagamentoVM().IdPessoaNatureza == 2) {
                            vm.IdPessoaNatureza(2);
                            vm.DocumentoPessoa(vmParam.PessoaDestinatarioPagamentoVM().Documento2);
                        }

                    }
                });

                vm.ValorAntecipacao.subscribe(function (newValue) {
                    if (newValue != null) {
                        if (vm.IdFinanceiroMoedaAntecipacao() != vm.IdFinanceiroMoedaValorAtual()) {
                            //Busca Cotação (DE/PARA)
                            var cotacao = ListaUltimaCotacao(vm.IdFinanceiroMoedaAntecipacao(), vm.IdFinanceiroMoedaValorAtual());
                            var valorConvertido = cotacao * vm.ValorAntecipacao(); //Valor Convertido quando moeda é diferente 
                            vm.ValorCotacao(cotacao);
                            vm.ValorAtual(valorConvertido); //Campo Valor Atual, o valor que será o valor do Título quando o boleto será impresso.
                        } else {
                            vm.ValorAtual(newValue);
                        }
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    antecipacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Antecipação Financeira salva com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});