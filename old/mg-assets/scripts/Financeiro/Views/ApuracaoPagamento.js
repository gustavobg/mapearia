﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal', 'datetimepicker', 'daterangepicker'], function (apuracaoPagamento, ko, mgFeedbackBase, toastr) {
    var vm = {},
        bind = function (id) {
            apuracaoPagamento.config({
                saveUrl: '/Financeiro/ApuracaoPagamento/NewEdit',
                getUrl: '/Financeiro/ApuracaoPagamento/NewEditJson'
            });

            var request = apuracaoPagamento.get({ id: id }),
                form = $('#form-FinanceiroModeloApuracaoPagamento-NewEdit'),
                isEdit = id != '' ? true : false;

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
                if (vm.IdProcessoSituacao() == 2 || vm.IdProcessoSituacao() == 17) {
                    form.addClass("read-mode panel-document");
                    $('#btn-salvar').hide();
                }

                vm.DadosComplementaresFormaPagamento = ko.observable({ 'Ativo': true, 'IdFinanceiroFormaPagamentoIn': '1,2,5' });
                vm.TipoPeriodoReferencia = ko.observable(0);
                vm.DadosComplementaresPessoa = ko.observable();

                vm.DadosModeloApuracao = ko.observableArray(null);
                vm.DadosModeloApuracaoSelected = ko.observable(null);
                vm.TotalAcrescimos = ko.observable(0);
                vm.TotalDescontos = ko.observable(0);
                vm.TotalOutros = ko.observable(0);
                vm.ValorApurado(0.0);
                vm.ValorPagar = ko.observable(0);
                vm.SaldoBaixa = ko.observable(0);
                vm.TotalBaixar = ko.observable(0);
                vm.dadosModelo = ko.observable(null);

                vm.IdFinanceiroModeloApuracaoPagamento.subscribe(function (newValue) {
                    vm.IdPessoa(null);
                });

                var getPagamentoValor = function (list, index, prop) {
                    return list && list.length > 0 && list[index] != null ? ko.unwrap(list[index][prop]) : null;
                };

                var listValorPagamentoChange = function () {
                    var dadosModeloApuracao = ko.unwrap(vm.DadosModeloApuracao),
                        totalAcrescimos = 0,
                        totalDescontos = 0,
                        totalOutros = 0,
                        lstVariavelPagamentoValor = [];

                    // soma valores e cria lista de valores para salvar
                    ko.utils.arrayForEach(dadosModeloApuracao, function (item) {
                        var valor = ko.unwrap(item.Valor),
                            tipoFinanceiroPagamento = ko.unwrap(item.TipoFinanceiroVariavelPagamento);

                        // soma acrescimos/descontos
                        if (valor !== null) {
                            if (tipoFinanceiroPagamento == 1) {
                                totalAcrescimos += Number(valor);
                            } else if (tipoFinanceiroPagamento == 2 || tipoFinanceiroPagamento == 3) {
                                totalDescontos += Number(valor);
                            } else if (tipoFinanceiroPagamento == 4) {
                                totalOutros += Number(valor);
                            }
                        }
                        // monta lista para salvar
                        lstVariavelPagamentoValor.push({
                            IdFinanceiroVariavelPagamento: item.IdFinanceiroVariavelPagamento,
                            IdFinanceiroApuracaoPagamentoValor: item.IdFinanceiroApuracaoPagamentoValor,
                            TipoFinanceiroVariavelPagamento: item.TipoFinanceiroVariavelPagamento,
                            DescricaoFinanceiroVariavelPagamento: item.DescricaoFinanceiroVariavelPagamento,
                            UtilizaReferencia: item.UtilizaReferencia,
                            Referencia: item.Referencia,
                            Valor: item.Valor,
                            Antecipacao: item.Antecipacao
                        });
                    });
                    vm.lstVariavelPagamentoValor(lstVariavelPagamentoValor);
                    vm.TotalAcrescimos(totalAcrescimos);
                    vm.TotalDescontos(totalDescontos);
                    vm.TotalOutros(totalOutros);

                    var valorApuradoTratado = 0;

                    if ((totalAcrescimos - totalDescontos) < 0) {
                        valorApuradoTratado = Number(totalAcrescimos - totalDescontos) * -1;
                    } else {
                        valorApuradoTratado = (totalAcrescimos - totalDescontos);
                    }


                    vm.ValorApurado(valorApuradoTratado);

                };

                ko.computed(function () {
                    /*  
                        - Esse método pega as informações do modelo selecionado (objeto via select2) e adapta para 
                        exibir na lista de acréscimos/descontos (DadosModeloApuracao). 
                        - São atribuídas novas propriedades que são necessárias para salvar a apuração.
                        - São inscritos métodos que observa se os valores da apuração são alterados e efetua a soma total dos mesmos.
                    */
                    var dadosModeloApuracaoSelected = ko.unwrap(vm.DadosModeloApuracaoSelected);
                    var lstVariavelPagamentoValor = ko.unwrap(vm.lstVariavelPagamentoValor);

                    if (vm.DadosModeloApuracaoSelected() != null && vm.DadosModeloApuracaoSelected().TipoPeriodoReferencia != null)
                        vm.TipoPeriodoReferencia(vm.DadosModeloApuracaoSelected().TipoPeriodoReferencia);

                    if (dadosModeloApuracaoSelected != null && dadosModeloApuracaoSelected.hasOwnProperty('lstVarialPagamento')) {
                        var data = ko.unwrap(dadosModeloApuracaoSelected.lstVarialPagamento);
                        data = ko.utils.arrayMap(data, function (item, index) {
                            if (!item.hasOwnProperty('IdFinanceiroApuracaoPagamentoValor')) {
                                item.IdFinanceiroApuracaoPagamentoValor = getPagamentoValor(lstVariavelPagamentoValor, index, 'IdFinanceiroApuracaoPagamentoValor');
                            }
                            if (!item.hasOwnProperty('Referencia')) {
                                item.Referencia = ko.observable(getPagamentoValor(lstVariavelPagamentoValor, index, 'Referencia'));
                            }
                            if (!item.hasOwnProperty('Valor')) {
                                item.Valor = ko.observable(getPagamentoValor(lstVariavelPagamentoValor, index, 'Valor'));
                            }
                            item.Valor.subscribe(function () { listValorPagamentoChange() });
                            item.Referencia.subscribe(function () { listValorPagamentoChange() });
                            return item;
                        });

                        vm.dadosModelo(dadosModeloApuracaoSelected);
                        vm.DescricaoDocumentoTipo(vm.dadosModelo().DescricaoDocumentoTipo);
                        //seleciona os perfis de Pessoa permitidos para o modelo selecionado.
                        vm.DadosComplementaresPessoa({ 'ComPessoa': true, 'ComEmpresaUsuaria': false, 'ComEmpresa': false, 'Ativo': true, 'IdPessoaPerfilIn': dadosModeloApuracaoSelected.IdsPessoaPerfil, "IdPessoaNaturezaNotIn": 4 });
                        vm.DadosModeloApuracao(data);
                        listValorPagamentoChange();
                    }
                }).extend({ rateLimit: { method: "notifyWhenChangesStop", deferred: true } });

                //Atribuindo IdPessoa para Os Detalhes de Pagamento
                //ko.computed(function () {
                //    if (vm.Detalhe.IdFinanceiroFormaPagamento() != null)
                //        vm.Detalhe.IdPessoaDestinatarioPagamento(vm.IdPessoa());
                //});

                vm.ExibeAntecipacoes = ko.observable(false);
                ko.computed(function () {
                    var dadosModelo = ko.unwrap(vm.dadosModelo);
                    if (dadosModelo != undefined) {
                        var Antecipacao = dadosModelo.Antecipacao,
                            ExigeAprovacao = dadosModelo.ExigeAprovacao;

                        if (ko.unwrap(vm.IdPessoa) != null && ko.unwrap(vm.lstAbatimentoCredito) != null && Antecipacao === false && ExigeAprovacao === false) {
                            vm.ExibeAntecipacoes(true);
                        } else {
                            vm.ExibeAntecipacoes(false);
                        }
                    }
                });


                //Retorna as Antecipações 
                //
                vm.IdPessoa.subscribe(function (newValue) {
                    debugger;
                    if (newValue != null) {
                        $.ajax({
                            type: "POST",
                            url: "/Financeiro/Pagamento/ListRegistrosParaAbatimentoCredito",
                            data: JSON.stringify({
                                IdPessoa: vm.IdPessoa(),
                                TipoMovimentoFinanceiro: vm.dadosModelo().Antecipacao == false ? 1 : 2,
                                Page: {
                                    PageSize: 99999, CurrentPage: 1, OrderBy: 'IdEmpresa asc'
                                }
                            })
                        }).success(function (data) {
                            if (data.Data.length > 0) {
                                var arrAbatimentoCredito = [];
                                ko.utils.arrayForEach(data.Data, function (item) {
                                    arrAbatimentoCredito.push({
                                        IdFinanceiroTitulo: item.Tipo == 1 ? item.ID : null,
                                        IdFinanceiroAntecipacao: item.Tipo == 2 ? item.ID : null,
                                        Numero: item.Titulo,
                                        DescricaoDocumentoFinanceiro: item.TipoDescricao,
                                        DataEmissaoExibicao: item.DataEmissaoExibicao,
                                        DataVencimentoExibicao: item.DataVencimento,
                                        ValorBaixadoExibicao: item.ValorBaixadoExibicao,
                                        ValorSaldoExibicao: item.ValorSaldoAtualExibicao,
                                        ValorBaixar: item.ValorBaixado,
                                        ValorBaixado: item.ValorBaixado,
                                        ValorBaixarExibicao: item.ValorBaixadoExibicao
                                    });
                                });
                                vm.lstAbatimentoCredito(arrAbatimentoCredito);
                            }
                        });
                    }
                });

                function calculaSaldos() {

                }

                var vmAbatimentoCreditoExtend = function (vm) {
                    ko.computed(function () {
                        if (vm.ValorBaixar() != null) {
                            vm.ValorBaixarExibicao("R$ " + vm.ValorBaixar());
                        }
                    });
                };
                ko.computed(function () {
                    // calcula saldo da baixa e diferença
                    var lstAbatimentoCredito = ko.unwrap(vm.lstAbatimentoCredito),
                        totalSaldoDaBaixa = 0,
                        totalBaixa = 0,
                        totalDiferenca = 0;

                    ko.utils.arrayForEach(lstAbatimentoCredito, function (item) {
                        totalSaldoDaBaixa += Number(item.ValorBaixar);
                        totalBaixa += Number(item.ValorBaixado);
                    });


                    totalDiferenca = vm.ValorApurado() - totalSaldoDaBaixa;
                    vm.SaldoBaixa(totalSaldoDaBaixa);
                    vm.ValorPagar(totalDiferenca);
                    vm.TotalBaixar(totalBaixa);


                    // Verificação, se valor negativo converto para positivo para gerar o Saldo Excedente.
                    if (vm.ValorPagar() > 0) {
                        vm.ValorSaldoExcedido(vm.ValorPagar());
                    }
                });

                window.vmAbatimentoCreditoExtend = vmAbatimentoCreditoExtend;
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    console.log(data);
                    apuracaoPagamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Apuração de Pagamento e Acerto salva sucesso.');
                    });
                };
                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});