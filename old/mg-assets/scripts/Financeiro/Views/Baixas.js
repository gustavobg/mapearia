﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'knockout-extension', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (formaPagamento, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            formaPagamento.config({
                saveUrl: '',
                getUrl: '/Financeiro/Pagamento/BaixasJson'
            });

            var request = formaPagamento.get({ id: id }),
                form = $('#form-FinanceiroPagamentoBaixas-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //vm.Save = function () {
                //    if (!vm.isValidShowErrors()) { return; };
                //    var data = ko.toJSON(vm);
                //    formaPagamento.save(data).done(function (result, status, xhr) {
                //        mgFeedbackBase.feedbackCrudRoute(result, '');
                //    });
                //}

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});