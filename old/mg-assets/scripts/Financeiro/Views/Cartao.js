﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (financeiroCartao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            financeiroCartao.config({
                saveUrl: '/Financeiro/Cartao/NewEdit',
                getUrl: '/Financeiro/Cartao/NewEditJson'
            });

            var request = financeiroCartao.get({ id: id }),
                form = $('#form-FinanceiroCartao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //Regra de preenchimento do Campo DiaVencimentoFatura
                ko.computed(function () {
                    if (!vm.CartaoCredito())
                        vm.DiaVencimentoFatura(null);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    vm.LimiteUso(vm.LimiteUso().replace('.', ''));
                    var data = ko.toJSON(vm);
                    financeiroCartao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Cartão salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});