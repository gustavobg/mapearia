﻿define(['../Controllers/Classificacao', 'jquery', 'knockout', 'feedback', 'toastr', 'jquery-flipper', 'jquery-inputmask', 'gridview', 'select2', 'datetimepicker', 'ko-validate-rules', 'ko-validate', 'vanillaUniform'], function (classificacaoFinanceira, $, ko, mgFeedbackBase, toastr) {
  
    bind = function (id) {
        
        var vmPai = { Classificacao: ko.observable(null) },
            request = classificacaoFinanceira.get({ id: id }),
            formId = 'form-FinanceiroClassificacao-NewEdit',
            formElement = $('#' + formId);

        request.done(function (response) {

            vmPai.Classificacao(ko.mapping.fromJS(response));
            //vm = vmPai.Classificacao(); // simplifica acesso a vm      
            var retornaContaSuperior = function (newValue) {
                $.ajax({
                    url: '/Financeiro/Classificacao/RetornaContaSuperior',
                    type: 'post',
                    data: 'codigo=' + newValue,
                    async: false,
                    success: function (codigo) {
                        if (codigo != newValue && codigo != "") {
                            $.ajax({
                                url: '/Financeiro/Classificacao/ListarHierarquicamentePorParametro',
                                type: 'post',
                                dataType: 'json',
                                data: JSON.stringify({
                                    IdPlanoContasVersao: vmPai.Classificacao().IdPlanoContasVersao(),
                                    Ativo: true,
                                    Codigo: codigo,
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'Codigo asc'
                                    }
                                }),
                                async: false,
                                success: function (data) {
                                    if (data.Data.length > 0)
                                        vmPai.Classificacao().IdFinanceiroClassificacaoPai(data.Data[0].IdFinanceiroClassificacao)
                                }
                            });
                        }
                    }
                });
            };

            vmPai.Classificacao().Save = function () {
                if (!vmPai.isValidShowErrors()) { return; }

                vmPai.Classificacao().SaveNewCrud(Route.saveNew());

                var data = ko.toJSON(vmPai.Classificacao());               

                classificacaoFinanceira.save(data).done(function (result, status, xhr) {
                    
                    mgFeedbackBase.feedbackCrudRoute(result, 'Classificação salva com sucesso.', !(Route.saveNew()), function () {
                        // refresh binds
                        if (Route.saveNew() === true) {
                            vmPai.Classificacao(ko.mapping.fromJS(result));                            
                            Route.redirectRouteAction('New');
                            Route.saveNew(true);                            
                            // rebind subscribe
                            vmPai.Classificacao().Codigo.subscribe(function (newValue) {
                                retornaContaSuperior(newValue);
                            });
                        }
                    });
                });
               

            };

            var vmParam = function () {
                this.PlanoContasVersao = ko.observable();
                ko.computed(function () {
                    var versaoPlano = this.PlanoContasVersao();
                    if (versaoPlano && versaoPlano.hasOwnProperty('Mascara'))
                        vmPai.Classificacao().Mascara(versaoPlano.Mascara);
                }, this);
            };

            vmParam = new vmParam();
            window.vmParam = vmParam;

            vmPai.Classificacao().Codigo.subscribe(function (newValue) {
                retornaContaSuperior(newValue);
            });

            window.vm = vmPai;
            ko.applyBindings(vmPai, formElement[0]);
        });

        return request;
    };

    // retorna métodos essenciais
    // obrigatórios: get, bind
    return {        
        bind: bind,
        routeOptions: { showSaveNew: true }
    };

});