﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (conciliacoaBancaria, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            conciliacoaBancaria.config({
                saveUrl: '/Financeiro/ConciliacaoBancaria/NewEdit',
                getUrl: '/Financeiro/ConciliacaoBancaria/ConciliacaoBancariaJson',
            });
            var request = conciliacoaBancaria.get({ id: id }),
                form = $('#form-FinanceiroConciliacaoBancaria-NewEdit');

            request.done(function (response) {
                
                //Apenas o Itens Selecionados na página de Listagem.
                ko.contextFor(document.getElementById('route-container-1').getElementsByTagName('grid-view-index')[0])['$data'].indexVM.api().selectedData().done(function (r) {
                    var arrItensSelecionados = r[0]
                    if (arrItensSelecionados && arrItensSelecionados.length > 0) {

                        //Post para preparar Novo Pagamento
                        debugger;
                        $.ajax({
                            type: "POST",
                            url: "/Financeiro/ConciliacaoBancaria/PreparaConciliacaoPorChave",
                            data: 'Chave=' + arrItensSelecionados
                        }).success(function (data) {
                            debugger;
                            vm.IdsFinanceiroMovimentacaoBancaria(data.IdsFinanceiroMovimentacaoBancaria);
                        });
                    }
                    else {
                        //vm.ItensSelecionados(1);
                        //vm.QuantidadeSelecionados(1);
                    }
                }).fail(function (r) {
                    //TODO: Mensagem Erro!
                    //
                    //vm.ItensSelecionados(1);
                    //vm.QuantidadeSelecionados(1);
                    alert(':( ');

                });


                Route.setHeaderInfo('Detalhes');
                vm = ko.mapping.fromJS(response);
                vm.Save = function () {
                    //if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    conciliacoaBancaria.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Conciliação realizada com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });
            return request;
        };

    return {
        bind: bind
    };
});