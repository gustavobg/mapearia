﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (conta, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            conta.config({
                saveUrl: '/Financeiro/Conta/NewEdit',
                getUrl: '/Financeiro/Conta/NewEditJson'
            });

            var request = conta.get({ id: id }),
                form = $('#form-FinanceiroConta-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    conta.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Conta salva com Sucesso.');
                    });                   
                };

                ko.computed(function () {
                    if (vm.IdFinanceiroContaTipo() != null || vm.IdFinanceiroContaTipo() != "") {
                        $.post('/Financeiro/ContaTipo/List', JSON.stringify({
                            IdFinanceiroContaTipo: vm.IdFinanceiroContaTipo(),
                            Ativo:true,
                            Page: { PageSize: 9999, CurrentPage: 1 }
                        })).done(function (result) {
                            vm.CaixaInternoContaTipo(result.Data[0].CaixaInterno);
                            if (vm.CaixaInternoContaTipo()) {
                                vm.Agencia(null);
                                vm.AgenciaDigito(null);
                                vm.Conta(null);
                                vm.ContaDigito(null);
                                vm.IdFinanceiroEntidade(null);
                                vm.IdPessoaTitularConta(null);
                            }
                            else {
                                vm.Descricao(null);
                                vm.IdPessoa(null);
                            }
                        });
                    }
                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);               
            });

            return request;
        };

    return {
        bind: bind
    }
});