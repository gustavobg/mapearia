﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (contaTipo, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            contaTipo.config({
                saveUrl: '/Financeiro/ContaTipo/NewEdit',
                getUrl: '/Financeiro/ContaTipo/NewEditJson'
            });

            var request = contaTipo.get({ id: id }),
                form = $('#form-FinanceiroContaTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    contaTipo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Conta salvo com Sucesso.');
                    });                   
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);               
            });

            return request;
        };

    return {
        bind: bind
    }
});