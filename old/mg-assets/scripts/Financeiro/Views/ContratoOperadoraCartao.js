﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'gridview', 'select2' , 'jquery-flipper', 'ko-validate'], function (contrato, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            contrato.config({
                saveUrl: '/Financeiro/ContratoOperadoraCartao/NewEdit',
                getUrl: '/Financeiro/ContratoOperadoraCartao/NewEditJson'
            });

            var request = contrato.get({ id: id }),
                form = $('#form-FinanceiroContratoOperadoraCartao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    contrato.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Contrato Salvo com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});