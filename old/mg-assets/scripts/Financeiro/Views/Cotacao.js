﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'mask-decimal', 'jquery-flipper', 'vanillaUniform', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (cotacao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            cotacao.config({
                saveUrl: '/Financeiro/Cotacao/NewEdit',
                getUrl: '/Financeiro/Cotacao/NewEditJson'
            });          

            var request = cotacao.get({ id: id }),
                vmPai = { Cotacao: ko.observable(null) },
                form = $('#form-FinanceiroCotacaoMoeda-NewEdit');

            request.done(function (response) {
               
                vmPai.Cotacao(ko.mapping.fromJS(response));

                vmPai.Cotacao().Save = function () {
                    if (!vmPai.isValidShowErrors()) { return; };

                    vmPai.Cotacao().SaveNewCrud(Route.saveNew());

                    var data = ko.toJSON(vmPai.Cotacao());

                    cotacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Cotação salva com sucesso.', !(Route.saveNew()), function () {
                            vmPai.Cotacao(ko.mapping.fromJS(result));
                            // Route.quiet true ignore default actions after route change                    
                            Route.quiet = true;
                            Route.redirectRouteAction('New');
                            Route.saveNew(true);
                        });
                    });                  
                };

                var vmParams = function () {
                    this.FinanceiroMoeda = ko.observable();
                    ko.computed(function () {
                        var moeda = this.FinanceiroMoeda();
                        if (moeda && moeda.hasOwnProperty('CasasDecimais'))
                            vmPai.Cotacao().CasasDecimais(moeda.CasasDecimais);
                    }, this);
                }
                vmParams = new vmParams();
                window.vmParams = vmParams;

                window.vm = vmPai;
                ko.applyBindings(vmPai, form[0]);

                $("#ddlFinanceiroMoeda").select2('open');
            });

            return request;
        };

    return {
        bind: bind,
        routeOptions: { showSaveNew: true }
    }
});