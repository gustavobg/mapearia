﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', '../Utils/CalcularValorPercentual', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'mask-decimal', 'ko-validate-rules', 'form-submit'], function (detalhesTitulo, mg, ko, mgFeedbackBase, toastr, valorPercentual) {

    var vm = {},
        bind = function (id) {
            detalhesTitulo.config({
                saveUrl: '/Financeiro/Pagamento/DetalhesAntecipacao',
                getUrl: '/Financeiro/Pagamento/DetalhesAntecipacaoJson',
            });
            var request = detalhesTitulo.get({ parentId: id }),
                form = $('#form-FinanceiroDetalhesAntecipacao-NewEdit');

            request.done(function (response) {

                Route.setHeaderInfo('Detalhes Antecipação');
                vm = ko.mapping.fromJS(response);
                vm.PercentualBaixar = ko.observable();

                function ListaUltimaCotacao(idMoeda, idMoedaReferencia) {
                    var resultado = '';
                    $.ajax({
                        url: '/Financeiro/Cotacao/ListaUltimaCotacaoPorMoeda',
                        type: 'post',
                        data: JSON.stringify({
                            IdFinanceiroMoeda: idMoeda,
                            IdFinanceiroMoedaReferencia: idMoedaReferencia,
                            Page: {
                                PageSize: 99999, CurrentPage: 1, OrderBy: 'IdFinanceiroCotacaoMoeda asc'
                            }
                        }),
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            resultado = data.Data[0].ValorCotacao;
                        }
                    });

                    return resultado;
                }



                if (vm.TipoMovimentoFinanceiro() == 1) {
                    form.addClass("read-mode panel-document");
                    $('#btnSalvarSelecionar').hide();
                }
                

                // regra para buscar a ultima Cotação válida, quando moeda é diferente 
                vm.IdFinanceiroMoedaValorAntecipacao.subscribe(function (newValue) {
                    if (newValue != null) {
                        debugger;
                        if (vm.IdFinanceiroMoedaValorAntecipacao() != vm.IdFinanceiroMoedaValorAtual()) {
                            //Busca Cotação (DE/PARA)
                            var cotacao = ListaUltimaCotacao(vm.IdFinanceiroMoedaValorAntecipacao(), vm.IdFinanceiroMoedaValorAtual());
                            var valorConvertido = cotacao * vm.ValorAntecipacao(); //Valor Convertido quando moeda é diferente 
                            vm.ValorCotacao(cotacao);
                            vm.ValorAtual(valorConvertido); //Campo Valor Atual, o valor que será o valor do Título quando o boleto será impresso.

                        }
                    }
                });


                vm.param = {
                    MoedaValorAtual: ko.observable(''),
                    SiglaValorAtual: ko.observable('')
                };

                ko.computed(function () {
                    if (ko.unwrap(vm.param.MoedaValorAtual) != null) {
                        vm.param.SiglaValorAtual(ko.unwrap(vm.param.MoedaValorAtual).Sigla);
                    }
                });

                // Região Cálculos
                //ko.computed(function () {
                //    if (vm.ValorPagamentoParcial() != null)
                //        vm.SaldoEmAberto(vm.ValorOriginalTitulo() - vm.TotalBaixado() - (vm.ValorPagamentoParcial()));
                //});

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    detalhesTitulo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Alteração realizada com sucesso.');
                    });
                };

                ko.applyBindings(vm, form[0]);
            });

            return request;

        };

    return {
        bind: bind,
        routeOptions: {
            showActionPanel: false
        }
    };
});