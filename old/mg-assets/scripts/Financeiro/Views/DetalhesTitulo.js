﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', '../Utils/CalcularValorPercentual', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'mask-decimal', 'ko-validate-rules', 'form-submit'], function (detalhesTitulo, mg, ko, mgFeedbackBase, toastr, valorPercentual) {

    var vm = {},
        bind = function (id) {
            detalhesTitulo.config({
                saveUrl: '/Financeiro/Pagamento/DetalhesTitulo',
                getUrl: '/Financeiro/Pagamento/DetalhesTituloJson',
            });

            function ListaUltimaCotacao(idMoeda, idMoedaReferencia) {
                var resultado = '';
                $.ajax({
                    url: '/Financeiro/Cotacao/ListaUltimaCotacaoPorMoeda',
                    type: 'post',
                    data: JSON.stringify({
                        IdFinanceiroMoeda: idMoeda,
                        IdFinanceiroMoedaReferencia: idMoedaReferencia,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'IdFinanceiroCotacaoMoeda asc'
                        }
                    }),
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        resultado = data.Data[0].ValorCotacao;
                    }
                });

                return resultado;
            }

            var request = detalhesTitulo.get({ parentId: id }),
                form = $('#form-FinanceiroDetalhesTitulo-NewEdit');

            request.done(function (response) {

                Route.setHeaderInfo('Detalhes Título');
                vm = ko.mapping.fromJS(response);
                vm.SaldoEmAberto = ko.observable(null);
                vm.param = {
                    MoedaValorAtual: ko.observable(''),
                    SiglaValorAtual: ko.observable('')
                };               

                ko.computed(function () {                    
                    if (ko.unwrap(vm.param.MoedaValorAtual) != null) {                        
                        vm.param.SiglaValorAtual(ko.unwrap(vm.param.MoedaValorAtual).Sigla);
                    }
                });

                var getValorInteiro = function (obs) {
                    var valor = Number(ko.unwrap(obs));
                    return valor = valor == null ? 0 : valor;
                }

                //Regra de Exibição de Campos (Moeda e Valor Atual) 
                ko.computed(function () {                   
                    if (getValorInteiro(vm.IdFinanceiroMoedaValorTitulo) != getValorInteiro(vm.IdFinanceiroMoedaValorAtual)) {                       
                        vm.ValorTitulo.valueHasMutated();
                    }                   
                });

                // regra para buscar a ultima Cotação válida, quando moeda é diferente 
                vm.ValorTitulo.subscribe(function (newValue) {                   
                    if (newValue != null) {
                        if (getValorInteiro(vm.IdFinanceiroMoedaValorTitulo) != getValorInteiro(vm.IdFinanceiroMoedaValorAtual)) {
                            //Busca Cotação (DE/PARA)
                            var cotacao = ListaUltimaCotacao(vm.IdFinanceiroMoedaValorTitulo(), vm.IdFinanceiroMoedaValorAtual());
                            var valorConvertido = cotacao * vm.ValorTitulo(); //Valor Convertido quando moeda é diferente 
                            vm.ValorCotacao(cotacao);
                            vm.ValorAtual(valorConvertido); //Campo Valor Atual, o valor que será o valor do Título quando o boleto será impresso.
                        } else {
                            vm.ValorAtual(newValue);
                        }
                    }
                });


                // Calcula valor e percentual de Desconto
                var oldValorDesconto = 0,
                    oldPercentualDesconto = 0,
                    resultadoDesconto = null;

                ko.computed(function () {
                    
                    var oldValor = oldValorDesconto,
                        oldPercentual = oldPercentualDesconto,
                        obsValor = vm.ValorDesconto,
                        obsPercentual = vm.PercDesconto;

                    if (ko.computedContext.isInitial()) {
                        resultadoDesconto = new valorPercentual({ porcentagemFixa: false });
                    }
                    var resultado = resultadoDesconto.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorDesconto = resultado.valor;
                    oldPercentualDesconto = resultado.percentual;
                });

                // Calcula valor e percentual de Multa
                var oldValorMulta = 0,
                    oldPercentualMulta = 0,
                    resultadoMulta = null;

                ko.computed(function () {
                    var oldValor = oldValorMulta,
                        oldPercentual = oldPercentualMulta,
                        obsValor = vm.ValorMulta,
                        obsPercentual = vm.PercMulta;

                    if (ko.computedContext.isInitial()) {
                        resultadoMulta = new valorPercentual({ porcentagemFixa: false });
                    }
                    var resultado = resultadoMulta.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorMulta = resultado.valor;
                    oldPercentualMulta = resultado.percentual;
                });

                // Calcula valor e percentual de Juros
                var oldValorJuros = 0,
                    oldPercentualJuros = 0,
                    resultadoJuros = null;

                ko.computed(function () {
                    var oldValor = oldValorJuros,
                        oldPercentual = oldPercentualJuros,
                        obsValor = vm.ValorJuro,
                        obsPercentual = vm.PercJuro;

                    if (ko.computedContext.isInitial()) {
                        resultadoJuros = new valorPercentual({ porcentagemFixa: false });
                    }
                    var resultado = resultadoJuros.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorJuros = resultado.valor;
                    oldPercentualJuros = resultado.percentual;
                });

                // Ao inserir algum valor em ValorVariacaoCambial, setar variação cambial padrão, notificação são removidas após uso
                var tmpSubscribeValorVariacaoCambial = vm.ValorVariacaoCambial.subscribe(function (v) {                   
                    var naturezaVariacaoCambial = ko.unwrap(vm.NaturezaVariacaoCambial);
                    if (naturezaVariacaoCambial == null) {
                        v = getValorInteiro(v);
                        if (v > 0) {
                            vm.NaturezaVariacaoCambial('1');
                            tmpSubscribeValorVariacaoCambial.dispose();
                        }
                    } else {
                        tmpSubscribeValorVariacaoCambial.dispose();
                    }
                })

                // Região Cálculos
                ko.computed(function () {
                    // Calcula valor a baixar
                    var valorAtual = getValorInteiro(vm.ValorAtual);
                    var valorDesconto = getValorInteiro(vm.ValorDesconto);
                    var valorMulta = getValorInteiro(vm.ValorMulta);
                    var valorJuros = getValorInteiro(vm.ValorJuro);
                    var valorVariacaoCambial = getValorInteiro(vm.ValorVariacaoCambial);
                    var naturezaVariacaoCambial = ko.unwrap(vm.NaturezaVariacaoCambial); // null, 1, 2
                                        
                    var total = valorAtual - valorDesconto + valorMulta + valorJuros;
                    if (naturezaVariacaoCambial != null) {                                                
                        total += naturezaVariacaoCambial == 1 ? valorVariacaoCambial : -valorVariacaoCambial;
                    }
                    // Variação Cambial, adicionar ou remover de acordo com flag
                    vm.Total(total);
                })


                //ko.computed(function () {
                //    if (vm.ValorPagamentoParcial() != null)
                //        vm.SaldoEmAberto(vm.ValorOriginalTitulo() - vm.TotalBaixado() - (vm.ValorPagamentoParcial()));
                //});

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    detalhesTitulo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Alteração realizada com sucesso.');
                    });
                };                
                ko.applyBindings(vm, form[0]);
            });

            return request;

        };

    return {
        bind: bind,
        routeOptions: {
            showActionPanel: false
        }
    };
});