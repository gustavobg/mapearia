﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (entidade, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            entidade.config({
                saveUrl: '/Financeiro/Entidade/NewEdit',
                getUrl: '/Financeiro/Entidade/NewEditJson'
            });

            var request = entidade.get({ id: id }),
                form = $('#form-FinanceiroEntidade-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    entidade.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Entidade Financeira salva com sucesso.');
                    });                   
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);               
            });

            return request;
        };

    return {
        bind: bind
    }
});