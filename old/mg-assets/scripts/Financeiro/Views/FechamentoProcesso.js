﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (fechamento, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            fechamento.config({
                saveUrl: '/Financeiro/FechamentoProcesso/NewEdit',
                getUrl: '/Financeiro/FechamentoProcesso/NewEditJson'
            });

            var request = fechamento.get({ id: id }),
                form = $('#form-FinanceiroFechamentoProcesso-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    fechamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Data de Fechamento salva com Sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});