﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (formaPagamentoTitulo, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            formaPagamentoTitulo.config({
                saveUrl: '/Financeiro/Pagamento/FormaPagamentoTitulo',
                getUrl: '/Financeiro/Pagamento/FormaPagamentoTituloJson',
            });
            var request = formaPagamentoTitulo.get({ id: id }),
                form = $('#form-FinanceiroFormaPagamentoTitulo-NewEdit'),
                dfd = $.Deferred(),
                dfdRejectAction = { callback: function () { window.location.href = '/#/Financeiro/Pagamento/Index'; } };

            request.done(function (response) {

                Route.setHeaderInfo('Forma de Pagamento');

                vm = ko.mapping.fromJS(response);
                vm.FormaPagamentoDadosCompl = ko.observable(null);
                vm.SaldoDiferenca = ko.observable(0);
                vm.SaldoBaixa = ko.observable(0);
                vm.dadosComplTransacaoSaldoExcedido = ko.observable();
                vm.SaldoExcedidoExibicao = ko.observable(false);

                //Caso o id preenchido, realiza o preparo para o novo pagamento/recebimento
                // TODO: Passar parâmetro via url de rota
                if (id == null) {
                    // Apenas o Itens Selecionados na página de Listagem.
                    if (mg.getApiFromList().hasOwnProperty('selectedData')) {
                        // verifica se consegue recuperar a api, se os itens da página inicial foram selecionados
                        mg.getApiFromList().selectedData().done(function (r) {
                            debugger;
                            var arrItensSelecionados = r[0]
                            if (arrItensSelecionados && arrItensSelecionados.length > 0) {
                                // Post para preparar Novo Pagamento
                                $.ajax({
                                    type: "POST",
                                    url: "/Financeiro/Pagamento/PreparaPagamentoPorChave",
                                    data: 'Chave=' + arrItensSelecionados
                                }).success(function (data) {
                                    vm.TotalBaixar(data.TotalBaixar);
                                    vm.IdFinanceiroFormaPagamentoPrevista(data.IdFinanceiroFormaPagamentoPrevista);
                                    vm.NomePessoa(data.NomePessoa);
                                    vm.MunicipioPessoa(data.MunicipioPessoa);
                                    vm.Tipo(data.Tipo);
                                    vm.IdPessoa(data.IdPessoa);
                                    vm.TipoMovimentoFinanceiro(data.TipoMovimentoFinanceiro);
                                    if(arrItensSelecionados.length == 1)
                                    {
                                        if(data.TipoMovimentoFinanceiro == 1)
                                            vm.IdFinanceiroContaOrigem(data.IdFinanceiroContaOrigem);
                                        else if(data.TipoMovimentoFinanceiro == 2)
                                            vm.IdFinanceiroContaDestino(data.IdFinanceiroContaDestino);
                                    }
                                });

                                vm.ItensSelecionados(arrItensSelecionados);
                                vm.QuantidadeSelecionados(arrItensSelecionados.length); //item que apresentado no textbox Título Selecionados
                            }
                            else {
                                vm.ItensSelecionados(null);
                                vm.QuantidadeSelecionados(1);
                            }                            
                        }).fail(function (r) {
                            // TODO: Mensagem Erro!
                            vm.ItensSelecionados(null);
                            vm.QuantidadeSelecionados(null);
                            dfd.reject(dfdRejectAction);
                        });
                    } else {
                        // TODO: redireciona para a página de escolha de títulos
                        toastr.info('Selecione os títulos a pagar/receber');
                        dfd.reject(dfdRejectAction);
                    }
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/Financeiro/Pagamento/PreparaPagamentoPorChave",
                        data: 'Chave=' + id
                    }).success(function (data) {
                        vm.TotalBaixar(data.TotalBaixar);
                        vm.IdFinanceiroFormaPagamentoPrevista(data.IdFinanceiroFormaPagamentoPrevista);
                        vm.NomePessoa(data.NomePessoa);
                        vm.MunicipioPessoa(data.MunicipioPessoa);
                        vm.Tipo(data.Tipo);
                        vm.IdPessoa(data.IdPessoa);
                        vm.TipoMovimentoFinanceiro(data.TipoMovimentoFinanceiro);
                        if (data.TipoMovimentoFinanceiro == 1)
                            vm.IdFinanceiroContaOrigem(data.IdFinanceiroContaOrigem);
                        else if (data.TipoMovimentoFinanceiro == 2)
                            vm.IdFinanceiroContaDestino(data.IdFinanceiroContaDestino);
                    });

                    vm.ItensSelecionados(id);
                    vm.QuantidadeSelecionados(1); //item que é apresentado no textbox Título Selecionados
                }

                ko.computed(function () {                   
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 8 && vm.Tipo() == 1) {
                        if (vm.IdPessoa() != null) {
                            $.ajax({
                                type: "POST",
                                url: "/Financeiro/Pagamento/ListRegistrosParaAbatimentoCredito",
                                data: JSON.stringify({
                                    IdPessoa: vm.IdPessoa(),
                                    TipoMovimentoFinanceiro: vm.TipoMovimentoFinanceiro() == 1 ? 2 : 1, //TipoMovimentoFinanceiro == 1 Pagamento, TipoMovimentoFinanceiro == 2 Recebimento. Desta forma buscamos o valor inverso da Operação Atual
                                    Page: {
                                        PageSize: 99999, CurrentPage: 1, OrderBy: 'IdEmpresa asc'
                                    }
                                })
                            }).success(function (data) {
                                //TODO: Popular grid Abatimento de Crédito
                                if (data.Data.length > 0) {
                                    ko.utils.arrayForEach(data.Data, function (item) {
                                        vm.lstAbatimentoCredito.push({
                                            IdFinanceiroTitulo: item.Tipo == 1 ? item.ID : null,
                                            IdFinanceiroAntecipacao: item.Tipo == 2 ? item.ID : null,
                                            Numero: item.Titulo,
                                            DescricaoDocumentoFinanceiro: item.TipoDescricao,
                                            DataEmissaoExibicao: item.DataEmissaoExibicao,
                                            DataVencimentoExibicao: item.DataVencimento,
                                            ValorBaixadoExibicao: item.ValorBaixadoExibicao,
                                            ValorSaldoExibicao: item.ValorSaldoAtualExibicao,
                                            ValorBaixar: item.ValorBaixar,
                                            ValorBaixarExibicao: item.ValorBaixarExibicao
                                        });
                                    });
                                }
                            });
                        }
                    }
                });

                var vmAbatimentoCreditoExtend = function (vm) {
                    ko.computed(function () {
                        if (vm.ValorBaixar() != null)
                            vm.ValorBaixarExibicao("R$ " + vm.ValorBaixar());
                    });
                };
                ko.computed(function () {
                    // calcula saldo da baixa e diferença
                    var lstAbatimentoCredito = ko.unwrap(vm.lstAbatimentoCredito),
                        totalSaldoDaBaixa = 0,
                        totalDiferenca = 0;

                    ko.utils.arrayForEach(lstAbatimentoCredito, function (item) {
                        totalSaldoDaBaixa += item.ValorBaixar;
                    });

                    totalDiferenca = vm.TotalBaixar.peek() - totalSaldoDaBaixa;
                    vm.SaldoBaixa(totalSaldoDaBaixa);
                    vm.SaldoDiferenca(totalDiferenca);

                    //Verificação, se valor negativo converto para positivo para gerar o Saldo Excedente.
                    if (vm.SaldoDiferenca() > 0) {
                        vm.ValorSaldoExcedido(vm.SaldoDiferenca());

                        if (vm.TipoMovimentoFinanceiro() == 1) {
                            vm.dadosComplTransacaoSaldoExcedido({ 'Ativo': true, 'IdTransacaoIn': '19,18,10,21' });
                            vm.IdTransacaoSaldoExcedido(null);
                            vm.IdDocumentoTipoSaldoExcedido(null);
                        }

                        else if (vm.TipoMovimentoFinanceiro() == 2) {
                            vm.dadosComplTransacaoSaldoExcedido({ 'Ativo': true, 'IdTransacaoIn': '7,8' });
                            vm.IdTransacaoSaldoExcedido(null);
                            vm.IdDocumentoTipoSaldoExcedido(null);
                        }

                    }

                    else {
                        vm.ValorSaldoExcedido(vm.SaldoDiferenca() * -1);

                        if (vm.TipoMovimentoFinanceiro() == 1) {
                            vm.dadosComplTransacaoSaldoExcedido({ 'Ativo': true, 'IdTransacaoIn': '7,8' });
                            vm.IdTransacaoSaldoExcedido(null);
                            vm.IdDocumentoTipoSaldoExcedido(null);
                        }

                        else if (vm.TipoMovimentoFinanceiro() == 2) {
                            vm.dadosComplTransacaoSaldoExcedido({ 'Ativo': true, 'IdTransacaoIn': '19,18,10,21' });
                            vm.IdTransacaoSaldoExcedido(null);
                            vm.IdDocumentoTipoSaldoExcedido(null);
                        }

                    }
                });

                window.vmAbatimentoCreditoExtend = vmAbatimentoCreditoExtend;

                //SaldoExcedidoExibicao Obrigatoriedade
                ko.computed(function () {
                    if (vm.SaldoDiferenca() != 0)
                        vm.SaldoExcedidoExibicao(true);
                    else
                        vm.SaldoExcedidoExibicao(false);
                });
                

                //Preenchimento da propriedade FormaPagamentoDadosCompl
                ko.computed(function () {
                    if (vm.Tipo() != null) {
                        //Caso Tipo == Título, exibir apenas as Formas de Pagamento Titulo = true
                        if (vm.Tipo() == 1)
                            vm.FormaPagamentoDadosCompl({ 'Ativo': true, 'Titulo': true });

                        else if (vm.Tipo() == 2) //Caso Tipo == Antecipacao, exibir apenas as Formas de Pagamento Anteciapcao = true
                            vm.FormaPagamentoDadosCompl({ 'Ativo': true, 'Antecipacao': true });

                        else if (vm.Tipo() == 3) //Caso Tipo == 3 Exibir itens Comuns entre Título e Antecipacao
                            vm.FormaPagamentoDadosCompl({ 'Ativo': true, 'Antecipacao': true, 'Titulo': true });
                    }
                });

                vm.TitulosSelecionadosExibicao = ko.observable(vm.QuantidadeSelecionados() + "/" + vm.TotalFiltrados());

                //Valores
                vm.LimiteUsoCartao = ko.observable(0);
                vm.SaldoDisponivelCartao = ko.observable(0);
                vm.IdFinanceiroContaCartao = ko.observable(0);
                vm.SaldoAtualContaCartao = ko.observable(0);
                vm.SaldoAtualContaOrigem = ko.observable(0);
                //Valores

                //Informações Box Cartão Débito/Crédito
                vm.IdFinanceiroCartaoContratoOperadora.subscribe(function (newValue) {
                    if (newValue != null)
                        vm.IdFinanceiroCartaoContratoOperadoraEquipamento(null);
                });

                var vmParam = function () {
                    this.InfoCartao = ko.observable();

                };
                vmParam = new vmParam();
                window.vmParam = vmParam;

                vm.IdFinanceiroFormaPagamentoPrevista.subscribe(function (newValue) {
                    if (newValue == 6) {
                        vm.CodigoBarraTipo(1);
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    formaPagamentoTitulo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Pagamento/Recebimento realizado com sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);

                dfd.resolve();
            });

            return dfd;

        };

    return {
        bind: bind,
        routeOptions: {
            showActionPanel: false
        }
    };
});