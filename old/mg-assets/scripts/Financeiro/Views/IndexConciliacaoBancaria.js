﻿define(['crud-controller', 'knockout', 'bootstrap/modal'], function (conciliacaoBancaria, ko, modal) {
    var vm = {},
        bind = function () {

            conciliacaoBancaria.config({
                getUrl: '/Financeiro/ConciliacaoBancaria/ConciliacaoBancariaJson',
                saveUrl: ''
            });
            //vm.unidadeDetalhe = ko.observable();
            vm.getUnidadeDetalhes = function (data, event) {
                event.preventDefault();
                var request = conciliacaoBancaria.get({ id: data.IdFinanceiroConciliacaoBancaria });
                request.done(function (response) {
                });
            };
        };

    return {
        bind: bind
    }

});