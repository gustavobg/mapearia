﻿define(['crud-controller', 'knockout', 'bootstrap/modal', 'util', 'vanillaUniform', 'mask-decimal'], function (pagamento, ko, modal, mg) {
    var vm = {},
        bind = function () {

            pagamento.config({
                getUrl: '/Financeiro/Pagamento/FormaPagamentoJson',
                saveUrl: ''
            });
            vm.totalBaixar = ko.observable(0);
            vm.unidadeDetalhe = ko.observable();
            vm.getUnidadeDetalhes = function (data, event) {
                event.preventDefault();
                var request = pagamento.get({ id: data.IdProducaoUnidade });
                request.done(function (response) {
                    //vm.unidadeDetalhe(ko.mapping.fromJS(response));
                    //modalDetalhes.modal('show');
                });
            };

            //vm.TipoBusca.subscribe = function (v) {
            //    console.log(v);
            //};
            var gridview = pagamento.getGridApiFromIndex(),
                gridviewVM = pagamento.getGridViewModel();
           
            ko.computed(function () {
                var tipoBusca = ko.unwrap(gridview.filterData().TipoBusca);
                if (tipoBusca.indexOf("3") >= 0) {
                    gridviewVM.useSelection(false);
                    gridview.resetSelected();
                } else {
                    gridviewVM.useSelection(true);
                }
            });

            gridview.selectedData().done(function (arr, dataObservable, selectedValuesObservable) {                
                ko.computed(function () {
                    var useSelection = ko.unwrap(gridviewVM.useSelection),
                        selectedValuesArr = ko.unwrap(selectedValuesObservable),
                        totalBaixar = 0;                    

                    if (useSelection) {
                        console.log(selectedValuesArr);
                        ko.utils.arrayForEach(dataObservable(), function (item) {
                            // percorre toda a tabela, se item estiver selecionado retorna o total
                            if (item.hasOwnProperty('Chave')) {
                                if (selectedValuesArr.indexOf(item.Chave) > -1) {
                                    totalBaixar += item.ValorBaixar;
                                }
                            }
                        });
                    }
                    vm.totalBaixar(totalBaixar);
                });
               
            });

            // temp
            window.get = pagamento.getGridApiFromIndex();

            ko.applyBindings(vm, document.getElementById('indexBindAction'));

            //window.vmIndexAgricolaUnidade = vm;
            //ko.applyBindings(vm, document.getElementById('IndexPagamento'));
        };

    return {
        bind: bind
    }

});