﻿define(['crud-controller', 'jquery', 'util', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (modeloApuracao, $, mg, ko, mgFeedbackBase, toastr) {
    "use strict";
    var vm = {},
        bind = function (id) {
            modeloApuracao.config({
                saveUrl: '/Financeiro/ModeloApuracaoPagamento/NewEdit',
                getUrl: '/Financeiro/ModeloApuracaoPagamento/NewEditJson'
            });
            var request = modeloApuracao.get({ id: id }),
                form = $('#form-FinanceiroModeloApuracaoPagamento-NewEdit');

            var getUnique = function (value, index, self) {
                return self.indexOf(value) === index;
            };

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.IdSubProcesso.subscribe(function () {
                    vm.IdDocumentoTipo(null);
                });

                vm.TipoMovimentoFinanceiro.subscribe(function () {
                    vm.IdDocumentoTipo(null);
                });

                vm.dadosComplDocumentoTipo = ko.observable();
                vm.IdFinanceiroVariavelPagamentoNotIn = ko.observableArray([]);
                vm.IdFinanceiroVariavelPagamentoVariavelNotIn = ko.observableArray([]);
                vm.VariavelPagamentoTipoCompl = ko.observable({});

                ko.computed(function () {
                    var NaturezaApuracao = ko.unwrap(vm.NaturezaApuracao),
                        Antecipacao = ko.unwrap(vm.Antecipacao),
                        dadosCompl = {};

                    vm.dadosComplDocumentoTipo({ 'Ativo': true, 'RepresentaAntecipacao': vm.Antecipacao() });

                    if (NaturezaApuracao == 1) {
                        dadosCompl = Antecipacao ? { 'TipoVariavelIn': '1', 'Antecipacao': true } : { 'TipoVariavelIn': '1, 2, 4', 'Antecipacao': false };
                    } else {
                        dadosCompl = Antecipacao ? { 'TipoVariavelIn': '1, 4', 'Antecipacao': true } : { 'TipoVariavelIn': '1, 3, 4', 'Antecipacao': false };
                    }
                    vm.VariavelPagamentoTipoCompl(dadosCompl);
                });

                vm.VariavelPagamentoCompl = ko.observable({ 'Ativo': true });
                ko.computed(function () {
                    // percorre lista lstVarialPagamento
                    var arrItensRestritosSelecionados = [],
                        arrVariaveisSelecionadas = [];

                    ko.utils.arrayForEach(ko.unwrap(vm.lstVarialPagamento), function (item) {
                        var IdsRestricao = ko.unwrap(item.IdsFinanceiroVariavelPagamentoRestricao),
                            IdFinanceiroVariavelPagamento = ko.unwrap(item.IdFinanceiroVariavelPagamento),
                            Ativo = ko.unwrap(item.Ativo);
                        
                        if (IdsRestricao !== null && IdsRestricao.length > 0 && Ativo) {
                            IdsRestricao.split(',').forEach(function (value) {
                                // lista da lista
                                arrItensRestritosSelecionados.push(value);
                            });
                        }
                        if (Ativo) {
                            // variaveis selecionadas
                            arrVariaveisSelecionadas.push(IdFinanceiroVariavelPagamento);
                        }
                    });
                    vm.IdFinanceiroVariavelPagamentoNotIn(arrItensRestritosSelecionados);
                    vm.IdFinanceiroVariavelPagamentoVariavelNotIn(arrVariaveisSelecionadas);
                });
                ko.computed(function () {
                    var arrIds = ko.unwrap(vm.IdFinanceiroVariavelPagamentoNotIn),
                        arrIdsVariavelVariavel = ko.unwrap(vm.IdFinanceiroVariavelPagamentoVariavelNotIn); // irá unir com arrIds

                    if (Array.isArray(arrIdsVariavelVariavel) && arrIdsVariavelVariavel.length > 0) {
                        arrIds = arrIds.concat(arrIdsVariavelVariavel);
                    }
                    debugger;
                        
                    var dadosCompl = arrIds.length > 0 ? { 'Ativo': true, 'IdFinanceiroVariavelPagamentoNotIn': arrIds.filter(getUnique).join(',') } : { 'Ativo': true },
                        dadosTipoCompl = ko.unwrap(vm.VariavelPagamentoTipoCompl);
                    
                    vm.VariavelPagamentoCompl($.extend(true, dadosCompl, dadosTipoCompl));
                });

                var vmItemVariavelPagamentoExtend = function (vm) {
                    this.VariavelPagamento = ko.observable(null);

                    ko.computed(function () {                      
                        var variavelPagamento = this.VariavelPagamento();
                        if (variavelPagamento !== null) {
                            if (variavelPagamento !== null) {                              
                                vm.DescricaoTipoVariavelPagamento(variavelPagamento.DescricaoTipoVariavel);
                                vm.DescricaoFinanceiroVariavelPagamento(variavelPagamento.Descricao);
                            }
                        }
                    }, this);
                };
                vm.vmItemVariavelPagamentoExtend = vmItemVariavelPagamentoExtend;

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    modeloApuracao.save(data).done(function (result) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Modelo de Apuração salvo com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;

        };

    return {
        bind: bind
    };
});