﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (moeda, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            moeda.config({
                saveUrl: '/Financeiro/Moeda/NewEdit',
                getUrl: '/Financeiro/Moeda/NewEditJson'
            });

            var request = moeda.get({ id: id }),
                form = $('#form-FinanceiroMoeda-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                ko.computed(function () {
                    if (vm.IdProdutoServico() == null || vm.IdProdutoServico() == '') {
                        vm.IdEstado(null);
                        vm.Local(null);
                    }

                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Financeiro/Moeda/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Moeda, Índice, Taxa e Produto salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
               
            });

            return request;
        };

    return {
        bind: bind
    }
});