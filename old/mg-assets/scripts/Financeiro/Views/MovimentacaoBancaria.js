﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'mask-decimal', 'jquery-flipper', 'gridview', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (movimentacao, ko, mgFeedbackBase, toastr) {


    var vm = {},
        bind = function (id) {
            movimentacao.config({
                saveUrl: '/Financeiro/MovimentacaoBancaria/NewEdit',
                getUrl: '/Financeiro/MovimentacaoBancaria/NewEditJson'
            });

            var request = movimentacao.get({ id: id }),
                form = $('#form-FinanceiroMovimentacaoBancaria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.dadosComplClassificacaoFinanceira = ko.observable({ 'Ativo': true, 'IdPlanoContasVersao': vm.IdPlanoContasClassificacaoFinanceira(), 'UltimoNivel': true });

                //vm Extend Transferência, manipulação dos itens do Modal.
                var vmTransferenciaExtend = function (vm) {

                    ko.computed(function () {
                        if (vm.Valor() != null)
                            vm.ValorExibicao(vm.SiglaMoeda() + " " + vm.Valor());
                    });
                };
                window.vmTransferenciaExtend = vmTransferenciaExtend;

                //vm Extend Classificação, manipulação dos itens do Modal.
                var vmClassificacaoExtend = function (vm) {

                    ko.computed(function () {
                        if (vm.Valor() != null)
                            vm.ValorExibicao(vm.SiglaMoeda() + " " + vm.Valor());
                    });
                };
                window.vmClassificacaoExtend = vmClassificacaoExtend;

                //Regra para preenchimento de campos de acordo com o tipo de Histório.
                vm.TipoHistorico.subscribe(function (newValue) {
                    //if (newValue == 1)
                    //    vm.DescricaoHistoricoManual(null);
                    //if (newValue == 2)
                    //    vm.IdFinanceiroMovimentacaoContaHistorico(null);
                    //if(newValue == 3)

                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    movimentacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Movimentação salva com sucesso.');
                    });
                }

                ko.bindingHandlers.dicaMes = {
                    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                        var $element = $(element),
                            value = allBindings().value,
                            $html = $('<div id="popover-mes" class="panel panel-warning">' +
                                          '<div class="panel-body">' +
                                                'A data de lançamento esta inferior a 3 meses da data atual. Deseja corrigir a data informada ou ignorar?' +
                                          '</div>' +
                                          '<div class="panel-footer">' +
                                          '</div>' +
                                      '</div>'),
                            $footer = $html.find('.panel-footer'),
                            $action = $('<button class="btn-primary btn btn-action">Corrigir a data</button> ').on('click', function () { action(value, element); }).appendTo($footer),
                            $ignore = $('<button class="btn-default btn btn-ignore">Ignorar</button> ').on('click', function () { ignore(value, element); }).appendTo($footer),
                            options = {
                                style: { classes: 'qtip-panel qtip-popover qtip-shadow' },
                                position: { my: 'bottom left', at: 'top right' },
                                content: { text: $html },
                                show: { event: 'focus', solo: true }
                            };

                        $element.qtip($.extend(defaults.qtip.popover, options));

                        function ignore(value, element) {
                            var $element = $(element),
                                api = $element.qtip('api');

                            api.toggle(false);
                        }
                        function action(value, element) {
                            var api = $(element).qtip('api');
                            api.toggle(false);
                            value('');
                            element.focus();
                        }
                    },
                    update: function (element, valueAccessor, allBindings) {
                        var value = allBindings().value,
                            $element = $(element),
                            $formGroup = $element.closest('.form-group'),
                            api = $element.qtip('api');

                        if (value() != null && value() != '' && moment(value(), 'DD/MM/YYYY').isBefore(moment().month(-3))) {
                            api.disable(false);
                            api.toggle(true);
                            $formGroup.addClass('has-warning');
                        } else {
                            api.disable(true);
                            $formGroup.removeClass('has-warning');
                        }
                    }
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});