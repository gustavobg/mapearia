﻿define(['crud-controller', 'knockout', 'feedback', 'toastr',  'button',  'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (movimentacao, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            movimentacao.config({
                saveUrl: '/Financeiro/MovimentacaoContaHistorico/NewEdit',
                getUrl: '/Financeiro/MovimentacaoContaHistorico/NewEditJson'
            });

            var request = movimentacao.get({ id: id }),
                form = $('#form-FinanceiroMovimentacaoContaHistorico-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //Regra de Exibição e manipulação de conteúdo
                vm.TransferenciaEntreContas.subscribe(function (newValue) {
                    if (newValue == false)
                        vm.TransferenciaEntreEntidadesIguais(false);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    movimentacao.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Histórico Padrão salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});