﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'mask-decimal'], function (reabrir, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            reabrir.config({
                saveUrl: '/Financeiro/FechamentoProcesso/ReAbrir',
                getUrl: '/Financeiro/FechamentoProcesso/ReAbrirJson'
            });

            var request = reabrir.get({ id: id }),
                form = $('#form-FinanceiroFechamentoProcessoReabrir-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    reabrir.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Justificativa.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});