﻿define(['crud-controller', 'knockout', 'feedback', 'moment', '../Utils/CalcularValorPercentual', 'jquery-inputmask', 'button', 'select2', 'gridview', 'datetimepicker', 'mask-decimal', 'jquery-flipper', 'ko-validate-rules', 'ko-validate', 'jquery-qtip'], function (titulo, ko, mgFeedbackBase, moment, valorPercentual) {

    var vm = {},
        bind = function (id) {
            titulo.config({
                saveUrl: '/Financeiro/Titulo/NewEdit',
                getUrl: '/Financeiro/Titulo/NewEditJson'
            });

            function ListaUltimaCotacao(idMoeda, idMoedaReferencia) {
                var resultado = '';
                $.ajax({
                    url: '/Financeiro/Cotacao/ListaUltimaCotacaoPorMoeda',
                    type: 'post',
                    data: JSON.stringify({
                        IdFinanceiroMoeda: idMoeda,
                        IdFinanceiroMoedaReferencia: idMoedaReferencia,
                        Page: {
                            PageSize: 99999, CurrentPage: 1, OrderBy: 'IdFinanceiroCotacaoMoeda asc'
                        }
                    }),
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        resultado = data.Data[0].ValorCotacao;
                    }
                });

                return resultado;
            }

            var request = titulo.get({ id: id }),
                form = $('#form-FinanceiroTitulo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.dadosComplSituacao = ko.observable(null);
                vm.dadosComplSituacao = ko.observable(null); vm.CasasDecimais = ko.observable(2);
                vm.CasasDecimaisValorTitulo = ko.observable(2);
                vm.CasasDecimaisValorAtual = ko.observable(2);
                vm.SiglaValorAtual = ko.observable('');
                vm.ExibeValorAtual = ko.observable(false);
                vm.ExibeContaDetalhesPagamento = ko.observable(false);
                vm.ExibeDestinatarioPagamento = ko.observable(false);
                vm.ExibeCartaoFinanceiro = ko.observable(false);
                vm.ExibeCodigoBarra = ko.observable(false);

                vm.TotalClassificado = ko.observable();
                vm.DiferencaClassifido = ko.observable();

                //Regra de Exibição de Campos (Moeda e Valor Atual) 
                ko.computed(function () {
                    if (vm.IdFinanceiroMoedaValorTitulo() != vm.IdFinanceiroMoedaValorAtual()) {
                        vm.ExibeValorAtual(true);
                        vm.ValorTitulo.valueHasMutated();
                    } else {
                        vm.ExibeValorAtual(false);
                        vm.ValorAtual(vm.ValorTitulo());
                    }
                });

                // regra para buscar a ultima Cotação válida, quando moeda é diferente 
                vm.ValorTitulo.subscribe(function (newValue) {
                    if (newValue != null) {
                        if (vm.IdFinanceiroMoedaValorAtual() != vm.IdFinanceiroMoedaValorTitulo()) {
                            //Busca Cotação (DE/PARA)
                            var cotacao = ListaUltimaCotacao(vm.IdFinanceiroMoedaValorTitulo(), vm.IdFinanceiroMoedaValorAtual());
                            var valorConvertido = cotacao * vm.ValorTitulo(); //Valor Convertido quando moeda é diferente 
                            vm.ValorCotacao(cotacao);
                            vm.ValorAtual(valorConvertido); //Campo Valor Atual, o valor que será o valor do Documento quando o boleto será impresso.
                        }
                    }
                });

                var vmParam = function () {
                    this.MoedaValorTitulo = ko.observable();
                    this.MoedaValorAtual = ko.observable();
                    this.MoedaValorContabilizacao = ko.observable();
                    this.CasasDecimaisContabilizacao = ko.observable(2);
                };

                vmParam = new vmParam();
                window.vmParam = vmParam;

                ko.computed(function () {
                    if (vmParam.MoedaValorTitulo() != null)
                        vm.CasasDecimaisValorTitulo(vmParam.MoedaValorTitulo().CasasDecimais);

                    if (vmParam.MoedaValorAtual() != null) {
                        vm.CasasDecimaisValorAtual(vmParam.MoedaValorAtual().CasasDecimais);
                        vm.SiglaValorAtual(vmParam.MoedaValorAtual().Sigla);
                    }
                });

                //Regra de preenchimento dos Campos, box Detalhes Previstos para Quitação
                vm.IdFinanceiroFormaPagamentoPrevista.subscribe(function (newValue) {
                    if (newValue != null) {

                        vm.IdPessoaNatureza(null);
                        vm.DocumentoPessoa(null);
                        vm.IdFinanceiroConta(null);
                        vm.IdFinanceiroCartao(null);
                        vm.CodigoBarraTipo(null);
                        vm.CodigoBarra(null);
                    }
                });
                //Regra para Exibição de Campos, box Detalhes Previsto apra Quitação
                ko.computed(function () {

                    // Formas de Pagamento
                    //1	Em Espécie (Dinheiro)
                    //2	Cheque 
                    //3	Cartão de Crédito e Débito
                    //4	Crédito não Financeiro (Econômico)
                    //5	Transferência Bancária
                    //6	Boleto Bancário ou DDA
                    //7	Guia de Recolhimento
                    //8	Abatimento de Crédito

                    //Tipo Operação == 1 A Pagar ||||| Tipo Operacao == 2 A Receber

                    //Exibe Conta (ddlFinanceiroConta)
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 1 || vm.IdFinanceiroFormaPagamentoPrevista() == 2 || vm.IdFinanceiroFormaPagamentoPrevista() == 5 || vm.IdFinanceiroFormaPagamentoPrevista() == 6 || vm.IdFinanceiroFormaPagamentoPrevista() == 7) {

                        //Verificação para a forma de Pagamento Guia de Recolhimento e Tipo de Operção A Receber
                        if (vm.IdFinanceiroFormaPagamentoPrevista() == 7 && vm.TipoMovimentoFinanceiro() == 2) {
                            vm.ExibeContaDetalhesPagamento(false)
                            vm.IdFinanceiroConta(null);
                        }
                        else {
                            vm.ExibeContaDetalhesPagamento(true);
                        }

                    }

                    else {
                        vm.ExibeContaDetalhesPagamento(false)
                        vm.IdFinanceiroConta(null);
                    }

                    //Exibe  Destinatário para Pagamento e Documentos
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 2 || vm.IdFinanceiroFormaPagamentoPrevista() == 5)
                        vm.ExibeDestinatarioPagamento(true)
                    else {
                        vm.ExibeDestinatarioPagamento(false)
                        vm.IdPessoaPagamentoDestinatario(null);
                        vm.IdPessoaNatureza(null);
                        vm.DocumentoPessoa(null);
                    }

                    //Exibe Cartão Financeiro
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 3 || vm.IdFinanceiroFormaPagamentoPrevista() == 4)
                        vm.ExibeCartaoFinanceiro(true);
                    else {
                        vm.ExibeCartaoFinanceiro(false);
                        vm.IdFinanceiroCartao(null);
                    }

                    //Exibe Código de Barra 
                    if (vm.IdFinanceiroFormaPagamentoPrevista() == 6) {
                        vm.ExibeCodigoBarra(true);
                        vm.CodigoBarraTipo(1);
                    }

                    else {
                        vm.ExibeCodigoBarra(false);
                        vm.CodigoBarraTipo(null);
                        vm.CodigoBarra(null);
                    }

                });


                //Regra de preenchimento dos Campos de Data
                vm.DataVencimentoExibicao.subscribe(function (newValue) {
                    if (newValue != null) {
                        if (vm.IdFinanceiroTitulo() == null) {
                            vm.DataLimiteDescontoExibicao(newValue);
                            vm.DataVencimentoOriginal(newValue);

                            if (vm.IdFinanceiroTitulo() == null) //Alterar a Data de vencimento Original apenas quando novo Título
                                vm.DataVencimentoOriginalExibicao(newValue);
                        }
                    }
                });


                // Calcula valor e percentual de Desconto
                var oldValorDesconto = 0,
                    oldPercentualDesconto = 0,
                    resultadoDesconto = null;

                ko.computed(function () {
                    var oldValor = oldValorDesconto,
                        oldPercentual = oldPercentualDesconto,
                        obsValor = vm.ValorDesconto,
                        obsPercentual = vm.PercentualDesconto;

                    if (ko.computedContext.isInitial()) {
                        resultadoDesconto = new valorPercentual({ porcentagemFixa: true });
                    }
                    var resultado = resultadoDesconto.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorDesconto = resultado.valor;
                    oldPercentualDesconto = resultado.percentual;
                });

                // Calcula valor e percentual de Multa
                var oldValorMulta = 0,
                    oldPercentualMulta = 0,
                    resultadoMulta = null;

                ko.computed(function () {
                    var oldValor = oldValorMulta,
                        oldPercentual = oldPercentualMulta,
                        obsValor = vm.ValorMulta,
                        obsPercentual = vm.PercentualMulta;

                    if (ko.computedContext.isInitial()) {
                        resultadoMulta = new valorPercentual({ porcentagemFixa: true });
                    }
                    var resultado = resultadoMulta.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorMulta = resultado.valor;
                    oldPercentualMulta = resultado.percentual;
                });

                // Calcula valor e percentual de Juros
                var oldValorJuros = 0,
                    oldPercentualJuros = 0,
                    resultadoJuros = null;

                ko.computed(function () {
                    var oldValor = oldValorJuros,
                        oldPercentual = oldPercentualJuros,
                        obsValor = vm.ValorJuros,
                        obsPercentual = vm.PercentualJuro;

                    if (ko.computedContext.isInitial()) {
                        resultadoJuros = new valorPercentual({ porcentagemFixa: true });
                    }
                    var resultado = resultadoJuros.calculaValorPercentual(oldValor, oldPercentual, obsValor, obsPercentual, vm);

                    oldValorJuros = resultado.valor;
                    oldPercentualJuros = resultado.percentual;
                });

                //Modal Contabilização
                var vmClassificacaoContabilExtend = function (vm) {

                    if (vm.Natureza() == null)
                        vm.Natureza(1);

                    //Valores de Exibição
                    ko.computed(function () {
                        //Natureza
                        if (vm.Natureza() == 1)
                            vm.NaturezaDescricao('Somar');
                        else if (vm.Natureza() == 2)
                            vm.NaturezaDescricao('Deduzir');

                        //Casas Decimais para o valor do Título
                        if (vmParam.MoedaValorContabilizacao() != null)
                            vmParam.CasasDecimaisContabilizacao(vmParam.MoedaValorContabilizacao().CasasDecimais);

                        //Exibição do valor do Título formatado.
                        if (vm.ValorTitulo() != null && vm.IdFinanceiroMoeda() != null && vmParam.MoedaValorContabilizacao() != null)
                            vm.ValorTituloExibicao(vmParam.MoedaValorContabilizacao().Sigla + " " + vm.ValorTitulo());
                    });
                };
                window.vmClassificacaoContabilExtend = vmClassificacaoContabilExtend;


                //Totalizador Grid Classificação Contábil
                ko.computed(function () {
                    if (vm.lstClassificacaoContabil() != null) {

                    }

                })

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);

                    titulo.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Título salvo com sucesso.');
                    });
                };


                ko.bindingHandlers.dicaMes = {
                    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
                        var $element = $(element),
                            value = allBindings().value,
                            $html = $('<div id="popover-mes" class="panel panel-warning">' +
                                          '<div class="panel-body">' +
                                                'A data de lançamento esta inferior a 3 meses da data atual. Deseja corrigir a data informada ou ignorar?' +
                                          '</div>' +
                                          '<div class="panel-footer">' +
                                          '</div>' +
                                      '</div>'),
                            $footer = $html.find('.panel-footer'),
                            $action = $('<button class="btn-primary btn btn-action">Corrigir a data</button> ').on('click', function () { action(value, element); }).appendTo($footer),
                            $ignore = $('<button class="btn-default btn btn-ignore">Ignorar</button> ').on('click', function () { ignore(value, element); }).appendTo($footer),
                            options = {
                                style: { classes: 'qtip-panel qtip-popover qtip-shadow' },
                                position: { my: 'bottom left', at: 'top right' },
                                content: { text: $html },
                                show: { event: 'focus', solo: true }
                            };

                        $element.qtip($.extend(defaults.qtip.popover, options));

                        function ignore(value, element) {
                            var $element = $(element),
                                api = $element.qtip('api');

                            api.toggle(false);
                        }
                        function action(value, element) {
                            var api = $(element).qtip('api');
                            api.toggle(false);
                            value('');
                            element.focus();
                        }
                    },
                    update: function (element, valueAccessor, allBindings) {
                        var value = allBindings().value,
                            $element = $(element),
                            $formGroup = $element.closest('.form-group'),
                            api = $element.qtip('api'),
                            isInitial = ko.computedContext.isInitial();

                        api.disable(true);

                        if (value() != null && value() != '' && moment(value(), 'DD/MM/YYYY').isBefore(moment().subtract(3, 'month'))) {
                            if (!isInitial) {
                                api.disable(false);
                                api.toggle(true);
                                $formGroup.addClass('has-warning');
                            }
                        } else {
                            api.toggle(false);
                            api.disable(true);
                            $formGroup.removeClass('has-warning');
                        }

                    }
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});