﻿define(['crud-controller', 'util', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'unidade-tipo', 'jquery-inputmask', 'gridview', 'select2', 'antiforgery', 'ko-validate-rules', 'form-submit'], function (variavelPagamento, mg, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            variavelPagamento.config({
                saveUrl: '/Financeiro/VariavelPagamento/NewEdit',
                getUrl: '/Financeiro/VariavelPagamento/NewEditJson',
            });
            var request = variavelPagamento.get({ id: id }),
                form = $('#form-FinanceiroVariavelPagamento-NewEdit');

            request.done(function (response) {

                vm = ko.mapping.fromJS(response);

                vm.DadosComplementaresClassificacaoFinanceira = ko.observable(null);

                ko.computed(function () {
                    if (vm.IdPlanoContasVersao() != null)
                        vm.DadosComplementaresClassificacaoFinanceira({ 'Ativo': true, 'IdPlanoContasVersao': vm.IdPlanoContasVersao(), 'UltimoNivel': true });
                    else
                        vm.DadosComplementaresClassificacaoFinanceira({ 'Ativo': true, 'UltimoNivel': true });
                });

                //Regra de preenchimento do campo Antecipação
                ko.computed(function () {
                    if (vm.TipoVariavel() != null && vm.TipoVariavel() != 1)
                        vm.Antecipacao(false);
                });

                // Regra para auto preenchimento dos campos
                vm.IdPlanoContasFiscalDebito.subscribe(function (newValue) {
                    vm.IdPlanoContasGerencialDebito(newValue);
                });
                vm.IdPlanoContasFiscalCredito.subscribe(function (newValue) {
                    vm.IdPlanoContasGerencialCredito(newValue);
                });
                //



                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    var data = ko.toJSON(vm);
                    variavelPagamento.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Variável de Pagamento Salva com Sucesso.');
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;

        };

    return {
        bind: bind
    };
});