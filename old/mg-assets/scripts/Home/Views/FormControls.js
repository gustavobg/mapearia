﻿define(['knockout', 'jquery', 'vanillaUniform/plugin', 'vanillaUniform', 'jquery-flipper', 'button', 'unidade-tipo'], function (ko, $, vanillaUniform) {

    var viewModel = {
        querComer: ko.observable(true),
        sabores: ko.observableArray(["morango", "creme"]), // Initially checks the Cherry and Almond checkboxes
        cores: ko.observableArray(["azul", "roxo", "lilás", "violeta"]),
        corSelecionada: ko.observable('roxo'),
        IdPessoaIn: ko.observable('47, 50, 66'),
        PessoasSelecionadas: ko.observable([]),
        IdUnidadeTipo: ko.observable(1),
        IdUnidade: ko.observable(12), // km
        IdUnidadeTipoIn: ko.observableArray([6, 1]),
        IdUnidadeIn: [4, 6, 12, 78], // km, Ft, ln, MHW
        IdUnidadeSO: ko.observable(52),
        IdUnidadeTipoSO: ko.observable(52),
        UnidadeApres1: ko.observable(),
        UnidadeApres2: ko.observable(),
        UnidadeApres3: ko.observable(),
        Valor: ko.observable(2000.987654321),
        ValorXXX: ko.observable(9999.1015),
        ValorMoeda: ko.observable(300.52),
        ValorNulo: ko.observable(null),
        dados: ko.observableArray(
        [
            { IdUnidadeTipo: ko.observable(6), IdUnidade: ko.observable(78), Valor: ko.observable(2000.152), Dica: '// Energia - MWH' },
            { IdUnidadeTipo: ko.observable(2), IdUnidade: ko.observable(30), Valor: ko.observable(30.5), Dica: '// Area - Alq' },
            { IdUnidadeTipo: ko.observable(3), IdUnidade: ko.observable(52), Valor: ko.observable(310.1), Dica: '// Volume - Mi3' }
        ])
    };

    window.vm = viewModel;

    $('.vanilla-nativo').each(function () {
        var el = $(this)[0];
        new vanillaUniform(el, {});

        el.addEventListener('vanillaUniform.change', function (e) {
            console.log(e.currentTarget.value);
        });
    });

    ko.applyBindings(viewModel, document.getElementById('bind'));

    return {
        routeOptions: {}
    }

});