﻿define(['jquery', 'knockout', 'chance', 'ko-gridview-init'], function ($, ko, chance) {
    chance = new chance();
    var userRandomData = function () {
        var i = 0,
            data = [];
        for (i; i <= 100;) {
            data.push({ id: i, name: chance.name(), age: chance.age() });
            i = i + 1;
        }
        return data;
    }

    var model = {
        options: {
            id: 'gridview-modeldata',
            data: userRandomData(),
            columns: [
                {
                    id: 'id',
                    displayName: 'Customer Id',
                    header: true
                },
                {
                    id: 'name',
                    displayName: 'Customer Name',
                    header: true
                },
                {
                    id: 'age',
                    displayName: 'Customer Age',
                    groupable: true,
                    header: true
                }
            ],
            orderByCol: 'id',
            orderByDir: 'desc',
            //groupByCol: 'age',
            //groupByDir: 'desc'
        }
    };
     
    
    $('#gridview-modeldata').attr({ 'params': model.options });

    ko.applyBindings(model, document.getElementById('gridview'));

    return {
        routeOptions: {
            title: 'Gridview refatorada',
            showBackButton: false,
            showBreadCrumb: false,
            showTitle: true
        }
    }
});