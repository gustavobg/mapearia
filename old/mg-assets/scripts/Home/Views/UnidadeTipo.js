﻿define(['knockout', 'qunit', 'unidade-tipo', 'ko-validate'], function (ko, qunit) {    

    // load QUnit CSS
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = '/App_Assets/publish/css/debug/qunit.css';
    document.getElementsByTagName("head")[0].appendChild(link);

    //O host cria uma janela temporária e insere os valores da VM (JS) num campo hidden,
    //quando o usuário "salva" uma informação, o host insere as informações na VM original                
    function vmDados() {
        var self = this;

        this.IdUnidadeTipo = ko.observable(1);
        this.IdUnidade = ko.observable(12); // km
        this.IdUnidadeTipoIn = ko.observableArray([6, 1]);
        this.IdUnidadeIn = [4, 6, 12, 78]; // km, Ft, ln, MHW
        this.IdUnidadeSO = ko.observable(52);
        this.IdUnidadeTipoSO = ko.observable(52);
        this.UnidadeApres1 = ko.observable();
        this.UnidadeApres2 = ko.observable();
        this.UnidadeApres3 = ko.observable();
        this.Valor = ko.observable(2000.987654321);
        this.ValorXXX = ko.observable(9999.1015);
        this.ValorMoeda = ko.observable(300.52),
        this.ValorNulo = ko.observable(null);
        this.Save = function () {
            if (self.isValid()) {
                alert('Sucesso!');
            }
        };

        this.dados = ko.observableArray(
        [
            { IdUnidadeTipo: ko.observable(6), IdUnidade: ko.observable(78), Valor: ko.observable(2000.152), Dica: '// Energia - MWH' },
            { IdUnidadeTipo: ko.observable(2), IdUnidade: ko.observable(30), Valor: ko.observable(30.5), Dica: '// Area - Alq' },
            { IdUnidadeTipo: ko.observable(3), IdUnidade: ko.observable(52), Valor: ko.observable(310.1), Dica: '// Volume - Mi3' }
        ]);

        this.Save = function () {
        }
    };

    var vmDados = new vmDados();
    window.vmDados = vmDados;

    ko.applyBindings(vmDados, document.getElementById('grid-area'));

    var dadosTeste = {
        idUnidade: ko.observable(78),        
        idUnidade1: ko.observable(52), // Mi3
        idUnidadeTipo1: ko.observable(2), // Area
        idUnidade2: ko.observable(30), // Mi3
        valor2: ko.observable(563.12),
        idUnidadeTipo2: ko.observable(3), // Volume
        unidadeApresentacao: ko.observable(''), 
        valor: ko.observable(15500.00),
        validateObservable: ko.observable(false)
    };

    window.vmDadosTeste = dadosTeste;

    ko.applyBindings(dadosTeste, document.getElementById('qunit-fixture-visible'));

    // Testing
    // Validar: Retornos de nome de apresentação
    // Recupera unidades
    // Conversão de casas decimais
    // Filtros de unidade e tipo de unidade
    // Validações de campo: Não permitir submeter valor se valor se unidade, ou vice-versa.
    // Verificar validação de obrigatoriedade
    module("knockout-unidade-tipo");   

    test('configuracoes setup', function () {       
        equal(ko.components.getComponentNameForNode(document.getElementById('component-teste-propriedades')), 'unidade-tipo', 'Carregou o componente unidade-tipo?');
        ok(window.Unidades.length > 0, 'Carregou as unidades?');        
    });

    test('propriedades', function (assert) {
        var c = ko.contextFor(document.getElementById('component-teste-propriedades').children[0])['$data'], // contexto do componente
            done = assert.async(),
            done2 = assert.async();

        assert.equal(c.unidadeSelecionadaCasasDecimais(), 0, 'Valor de casas decimais deve ser 0.');
        assert.equal(c.unidadeSelecionadaSigla().trim(), '(MWH)', 'Sigla está correta');

        // obs: Aguarda operação de máscara
        window.setTimeout(function () {
            assert.equal(c.unidadeSelecionadaApresentacao(), '15.500 (MWH)', 'Apresentação está correta');

            window.setTimeout(function () {
                c.idUnidade(63);
                c.valor(15535.6712);
                assert.equal(c.unidadeSelecionadaCasasDecimais(), 2, 'Ao alterar para idUnidade 63, o valor de casas decimais deve ser 2.');
                done();

                window.setTimeout(function () {                    
                    assert.equal(c.unidadeSelecionadaApresentacao(), '15.535,67 (£)', 'Ao alterar o valor para 15535.6712, a apresentação será 15.535,67 (£)');
                    done2();
                }, 100);

            }, 100);

        }, 100);
    });

    test('teste de filtros por tipo ou unidade', function (assert) {
        var c = ko.contextFor(document.getElementById('component-teste-filtros').children[0])['$data'], // contexto do componente
            i = 0,
            unidadesFiltro = c.unidadesFiltro(),
            unidadesFiltroLength = unidadesFiltro.length,
            validFiltroTipos = true;

        // inicialmente filtramos para exibir todos os tipos de massa com a unidade Mi3 selecionada
        // verifica se todos os tipos filtrados correspondem ao selecionado
        for (i; i < unidadesFiltroLength;) {
            // ignora primeiro valor do índice, pois ele poderá ser nulo
            if (i != 0) {
                var item = unidadesFiltro[i];
                console.log(item);
                validFiltroTipos = item.IdUnidadeTipo != 2 ? false : true;
            }
            i = i + 1;
        }

        ok(validFiltroTipos, 'O filtro de tipo de unidade selecionado trouxe a lista com os tipos correspondentes');
        equal(c.idUnidade(), 52, 'O filtro de unidade selecionada trouxe a unidade correspondente selecionada');
        equal(c.unidadeSelecionadaTipoDescricao(), 'Volume (Capacidade)', 'O filtro de tipo de unidade selecionado trouxe a descrição do tipo correspondente');
        
    });

    test('teste de comportamento do elemento DOM e validação', function (assert) {
        var element = $('#component-teste-elementos'),
            campoValor = element.find('.campo-valor'),
            campoUnidade = element.find('.campo-unidade'),
            botaoLimpar = element.find('.input-clear-button'),
            c = ko.contextFor(element[0].children[0])['$data'],
            done = assert.async();
       
        window.setTimeout(function () {            
            campoValor.val('');
            campoValor.change();
            equal(c.valor(), null, 'Valor observable alterou ao alterar elemento para "", obs: Converte para nulo');

            campoValor.val(15984.10); // Alq não tem casas decimais, portanto será convertido para 15984
            campoValor.change();
            equal(c.valor(), 15984, 'Valor observable alterou ao modificar o input do elemento para 15984');

            campoUnidade.val(42); // Bar, permite 5 casas decimais
            campoUnidade.change();
            campoValor.val(15984.952112); // Irá remover o último caractere, pois aceita somente 5
            campoValor.change();
            equal(c.valor(), 15984.95211, 'Valor observable alterou ao modificar o input do elemento para 15984.952112 e dropdown do tipo para 42');

            // validação            
            campoValor.val(''); // Irá remover o último caractere, pois aceita somente 5
            campoValor.change();
            ok(dadosTeste.isValid(), 'Valor é válido, pois campo é opcional');
            dadosTeste.validateObservable(true);
            ok(!dadosTeste.isValid(), 'Valor é inválido, pois campo é obrigatório');
            campoValor.val(0); // Irá remover o último caractere, pois aceita somente 5
            campoValor.change();
            ok(dadosTeste.isValid(), 'Valor é válido, pois foi inserido o valor 0, que é considerado não nulo');
            botaoLimpar.click();
            ok(!dadosTeste.isValid(), 'Valor é inválido, pois foi clicado no botão para limpar o campo');
            done();
        }, 200); 
    });

    qunit.load();
    qunit.start();

    return {
        routeOptions: {}
    }
});
