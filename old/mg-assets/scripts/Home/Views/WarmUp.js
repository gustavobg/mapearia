﻿define(['jquery', 'knockout', 'toastr'], function ($, ko, toastr) {

    //var viewModel = {
    //    querComer: ko.observable(true),
    //    sabores: ko.observableArray(["morango", "creme"]), // Initially checks the Cherry and Almond checkboxes
    //    cores: ko.observableArray(["azul", "roxo", "lilás", "violeta"]),
    //    corSelecionada: ko.observable('roxo')
    //};

    //window.vm = viewModel;

    workers.pauseTasks();

    var telas = [],
        element = null,
        tituloTela = '',
        link = '',
        count = 0,
        totalCount = 0,
        toastElement = toastr.success('Carregando', '', { timeOut: 0, extendedTimeOut: 0, progressBar: true }).first(),
        toastElementProgress = toastElement.find('.toast-progress'),
        toastElementMessage = toastElement.find('.toast-message');

    var telaVm = function (tituloTela, link) {
        var self = this;
        this.tituloTela = tituloTela;
        this.link = link;
        this.status = ko.observable('Aquecendo');
        this.mensagem = ko.observable('');

        ko.computed(function () {
            $.get(self.link.replace(/\/#\//gi, '')).done(function () {
                self.status('Ok');
            }).fail(function (err) {
                self.status('Erro');
                self.mensagem(err);
            }).always(function () {
                count++;
                toastElementProgress.css({ width: Math.floor((count * 100) / totalCount) + '%' });

                if (totalCount == count) {                    
                    workers.resumeTasks();
                    toastElementMessage.html('Tudo quente! Agora tá uma bala essas ' + count + ' telas');
                    window.setTimeout(function () {                        
                        toastElement.click();
                    }, 4000);
                    
                }
            });
        });
    }

    window.toastr = toastr;

    var vm = {
        telas: ko.observableArray([])      
    };

    $.post('Configuracao/Tela/List', JSON.stringify({ 'Ativo': true, Page: { PageSize: 99999, CurrentPage: 1, OrderBy: 'Descricao' } })).done(function (response) {
        var i = 0,
            length = response.Data.length,
            tela = null;

        for (i; i < length;) {
            tela = response.Data[i];
            //if (tela.Url.indexOf('/Index') > 0) {
            telas.push(new telaVm(tela.TituloMenu, tela.Url));
            //}
            i = i + 1;
        }
        totalCount = telas.length;
        vm.telas(telas);
    });
    

    ko.applyBindings(vm, document.getElementById('warmUp'));

    return {
        routeOptions: {
            title: 'Aquecendo Controllers',
            showTitle: true
        }
    }

});