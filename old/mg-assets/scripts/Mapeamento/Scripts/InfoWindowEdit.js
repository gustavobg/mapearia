﻿define(['jquery', 'text!../Views/InfoWindowEdit.html', 'text!../Views/InfoElemento.html', '../Views/Elemento', 'jquery-inputmask'], function ($, infoWindowEditTemplate, infoElementoTemplate, Elemento) {

	// todo: verificar passagem de parâmetro instanceMap..

	var infoWindowEdit = null;
	var infoWindowSelectedObj = null; // elemento selectionado infoWindow
	var infoWindowEvents = null;
	var infoWindowElement = null;
	//var instanceMap = null;

	var init = function (instanceMap, callback) {
		// somente uma instancia de infoWindow
		infoWindowEdit = new google.maps.InfoWindow({ content: '' });
		google.maps.event.addListener(infoWindowEdit, 'domready', function () { callback(infoWindowSelectedObj) });
		//instanceMap = instanceMap;

		return infoWindowEdit; // retorna instância
	};

	var getContentInfoWindow = function (obj) {
		// todo: Inserir informações de área
		return infoWindowEditTemplate
	};

	var getOverlayPosition = function (obj) {
		if (obj.type === 'point') {
			return obj.getPosition();
		} else if (obj.type === 'marker') {
			return obj.position;
		} else {
			return obj.getPath().getArray()[0];
		}
	};

	var setupEvents = function (element) {

		var wkt = new Wkt.Wkt();

		infoWindowElement = element;
		// amarra os eventos nos elementos gráficos
		// posiciona InfoWindow de edição		
		infoWindowEvents = google.maps.event.addListener(element, 'click', function () {
			var iwOptions = {
				position: getOverlayPosition(element),
				content: getContentInfoWindow(element)
			}
			infoWindowEdit.setOptions(iwOptions);
			infoWindowEdit.open(instanceMap);
			infoWindowSelectedObj = element;

			$('#iw-element-info').append(infoElementoTemplate);
			
			var vm = {
				Descricao: ko.observable(),
				Observacao: ko.observable(null),
				ElementoArea: ko.observable(),
			};
		
			Elemento.setModel(element, wkt.fromObject(element));
			Elemento.getModel().done(function (r) {
				var r = r[0];
				vm.Descricao(r['Descricao']);
				vm.Observacao(r['Observacao']);
				var vmElemento = Elemento.getViewModel();
				vm.ElementoArea(ko.unwrap(vmElemento.ElementoArea));
			});

			ko.applyBindings(vm, document.getElementById('iw-edit-area'));

		});
	};

	var close = function () {
		infoWindowEdit.close(instanceMap);
	};

	var lock = function () {		
		google.maps.event.removeListener(infoWindowEvents);
	};

	var unlock = function () {
		setupEvents(infoWindowElement);
	};

	return {
		init: init,
		setupEvents: setupEvents,
		close: close,
		lock: lock,
		unlock: unlock
	}

	
})