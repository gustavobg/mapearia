﻿define(['jquery', 'text!../Views/DrawingTools.html'], function ($, DrawingToolsTemplate) {
	// define barra de ferramentas do mapa e suas ações
	var instanceMap = null;
	var drawToolsInstance = null;
	var toolbar = {
		element: $('#toolbar'),
		visible: ko.observable(true),
		mode: ko.observable('view'),
		hide: function () {
			toolbar.visible(false);
		},
		show: function () {
			toolbar.visible(true);
		},
		deselectAll: function () {
			toolbar.element.find('button').removeClass('selected');
		},
		toggleSelected: function (element) {
			toolbar.deselectAll();
			element.classList.add('selected');
		},
		toggleMode: function () {
			var mode = ko.unwrap(toolbar.mode);
			toolbar.mode(mode === 'view' ? 'edit' : 'view');
		},
		resetSelection: function () {
			toolbar.deselectAll();
			drawToolsInstance.set('drawingMode', null);
		},
		toolbarAction: function (data, event) {
			var btnElement = event.currentTarget;
			var action = btnElement.dataset.action;

			switch (action) {
				case 'move':
					drawToolsInstance.set('drawingMode', null);
					toolbar.toggleSelected(btnElement);
					break;
				case 'addMarker':
					drawToolsInstance.set('drawingMode', google.maps.drawing.OverlayType.MARKER);
					break;
				case 'addPolygon':
					drawToolsInstance.set('drawingMode', google.maps.drawing.OverlayType.POLYGON);
					toolbar.toggleSelected(btnElement);
					break;
				case 'addPolyline':
					drawToolsInstance.set('drawingMode', google.maps.drawing.OverlayType.POLYLINE);
					toolbar.toggleSelected(btnElement);
					break;
			    case 'measure-polyline':
					drawToolsInstance.set('drawingMode', null);
					toolbar.toggleSelected(btnElement);
					measure(google.maps.drawing.OverlayType.POLYLINE);
					break;
			    case 'measure-polygon':
			        drawToolsInstance.set('drawingMode', null);
			        toolbar.toggleSelected(btnElement);
			        measure(google.maps.drawing.OverlayType.POLYGON);
			        break;
				case 'print':
					drawToolsInstance.set('drawingMode', null);
					toolbar.toggleSelected(btnElement);
					printMap();
					break;
				default:
					drawToolsInstance.set('drawingMode', null);
					toolbar.deselectAll();
					break;
			}
		}
	};

	toolbar.init = function (map, overlayCallback) {
		var drawerInstance = new google.maps.drawing.DrawingManager({
			drawingMode: null,//google.maps.drawing.OverlayType.MARKER,
			drawingControl: false,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: [
					google.maps.drawing.OverlayType.MARKER,
					google.maps.drawing.OverlayType.POLYGON,
					google.maps.drawing.OverlayType.POLYLINE
				]
			}
		});

		drawerInstance.setMap(map);
		instanceMap = map;

		var toolbarElement = document.getElementById('toolbar');
		toolbarElement.innerHTML = DrawingToolsTemplate;
		ko.applyBindings(toolbar, toolbarElement);

		drawToolsInstance = drawerInstance;

		return drawerInstance;
	}

	function measure(modeDraw) {
		// Implementar método para medir aqui
	    // instancia do mapa: instanceMap	    

	    var drawerManager = new google.maps.drawing.DrawingManager({
	        drawingMode: modeDraw,//google.maps.drawing.OverlayType.MARKER,
	        drawingControl: false	        
	    });
	    drawerManager.setMap(instanceMap);

	    google.maps.event.addListener(drawerManager, 'overlaycomplete', function (event) {
	        var content = [];
	        var position;
	        var element = event.overlay;
	        var wkt = new Wkt.Wkt();            

            wkt.fromObject(event.overlay);

	        if (event.type == google.maps.drawing.OverlayType.POLYLINE) {
	            var metro = google.maps.geometry.spherical.computeLength(event.overlay.getPath().getArray());
	            position = event.overlay.getPath().getArray()[0];
	            
	            content.push("<b>Google Maps metros: </b>" + metro.toFixed(3) + "<br>");
	            //content.push("<b>Turf metros: </b>" + turf.along(wkt.toJson(), 1, 'kilometers').toFixed(3) + "<br>");
	            
	        } else {
	            var metro = google.maps.geometry.spherical.computeArea(event.overlay.getPath().getArray());
	            position = event.overlay.getPath().getArray()[0];
	            content.push("<b>metros ²: </b>" + metro.toFixed(3) + "<br>");
	            //content.push("<b>Turf metros: </b>" + turf.area(wkt.toJson()).toFixed(3) + "<br>");
	        }

	        var infoWindow = new google.maps.InfoWindow({
	        	content: content.join(''),
	        	map: instanceMap
	        });
	       
	        google.maps.event.addListener(infoWindow, 'closeclick', function () {
				// remove elemento ao fechar 
	        	element.setMap(null);
	        	toolbar.deselectAll();
	        });

	        infoWindow.setPosition(position);
	        infoWindow.open(instanceMap);

	        drawerManager.set('drawingMode', null);
	    });
	}

	function printMap() {
		// Implementar método para imprimir		
		window.print();
	}

	return toolbar;
	
});