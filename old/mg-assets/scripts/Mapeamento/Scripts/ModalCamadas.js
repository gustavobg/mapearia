﻿define(['jquery', '../Views/Camada', 'text!/Mapeamento/Camada/NewEdit', 'bootstrap/modal'], function ($, camada, camadaTemplate) {
	// insere modal de camadas
	var modalCamadas = null;
	var form = $('#form-MapeamentoCamada-NewEdit');
	var params = {};

	var init = function (params) {			
		modalCamadas = $('#modal-mapeamento-camadas').modal('hide');		
		document.getElementById('modal-mapeamento-camadas-conteudo').innerHTML = camadaTemplate;		
		params = params;
		$('#btn-salvar-camada', '#modal-mapeamento-camadas').on('click', function (e) {			
			$('#form-MapeamentoCamada-NewEdit').submit();
		});		
		return camada.bind(0, params);
	};

	return {
		init: init,
		open: function () {
			modalCamadas.modal('show');
		},
		openEdit: function (id) {
			modalCamadas.modal('show');
			camada.edit(id);
		},
		close: function () {
			modalCamadas.modal('hide');
		},		
		resetModel: function () {
			camada.resetModel();
		},
		camada: camada
	};

})