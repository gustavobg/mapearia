﻿define(['jquery', 'crud-controller', 'feedback', 'text!/Mapeamento/Camada/NewEdit', 'util', 'bootstrap/modal'], function ($, mapas, mgFeedbackBase, camadaTemplate, utils) {
	var modalMapa = null;
	var params = { external: false, successCallback: function () { } };
	var id = null;
	var _firstBind = true;
	var initMapa = function (options) {		
		params = $.extend(true, params, options);
		// insere modal de mapa
		modalMapa = $('#modal-mapeamento-mapa').modal('hide');
	};
	var vm = { data: ko.observable(null) };
	var defaultResponse = {};	

	var _bindMapa = function (id) {
		// parâmetros customizados para rota injetada
		mapas.config({
			saveUrl: '/Mapeamento/Mapa/NewEdit',
			getUrl: '/Mapeamento/Mapa/NewEditJson'
		});

		var request = mapas.get({ id: id }),
			form = $('#form-MapeamentoMapa-NewEdit');

		request.done(function (response) {
			var responseData = ko.mapping.fromJS(response);
			defaultResponse = response;

			vm.data(responseData);

			vm.Save = function () {				

				if (!vm.data().isValidShowErrors()) { return; };

				var data = ko.toJSON(vm.data());

				mapas.save(data).done(function (result, status, xhr) {
					mgFeedbackBase.feedbackCrudRoute(result, 'Mapa criado com sucesso.', false, params.successCallback);
				});
			};

			if (!utils.isBound('form-MapeamentoMapa-NewEdit')) {
				ko.applyBindings(vm, form[0]);
			}
		});

		return request;
	};
	

	return {
		init: initMapa,
		open: function (id) {
			modalMapa.modal('show');
			_bindMapa(id);
		},
		close: function () {
			modalMapa.modal('hide');
		},
		resetModel: function () {
			vm.data(ko.mapping.fromJS(defaultResponse));
		}
	};

})