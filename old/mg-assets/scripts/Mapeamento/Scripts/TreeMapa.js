﻿define(['jquery', 'toastr', 'tree-util', 'jquery-ui/sortable', 'jstree', 'jstree.types', 'jstree.search', 'jstree.checkbox', 'jstree.state', 'jstree.dnd', 'jquery-qtip'], function ($, toastr, treeUtil) {

	var treeElement = null;
	var _mapService = null;

	var requestTree = function (id) {		
		return $.ajax({
			url: '/Mapeamento/Mapa/RetornaMapaPorEmpresa',
			type: 'POST',
			data: 'IdMapeamentoMapa=' + id + '&idMapeamentoCamadaCategoriaIn=' + ko.unwrap(mapaElemento.IdMapeamentoCamadaCategoriaFiltro)
		});
	}

	var createTree = function (data, id) {
		treeElement = $("#treeItensMapa").jstree({
			'core': {
				'multiple': true,
				'check_callback': function (operation, node, node_parent, node_position, more) {
					// operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
					// in case of 'rename_node' node_position is filled with the new node name
					if (operation === "move_node") {
						return true;
						//if (node_parent.type === 'folder') {
						//	return false;
						//} else {
						//	return true;
						//}
					}
					if (operation === "copy_node") {
						return true;
					}
					// Evita criação de nó filho na tela
					//if (operation === "create_node" && node_parent.type === "default") {
					//	alert('Não pode ser criado uma tela aninhada');
					//	return false;
					//}
					return true;
				},
				'dblclick_toggle': false,
				'data': data,
				'strings': {
					'Loading ...': 'Carregando árvore...'
				}
			},
			'checkbox': {
				three_state: true,
				//keep_selected_style: false,
				whole_node: false,
				tie_selection: false
			},
			'search': {
				'show_only_matches': true
			},
			'state': {
				'key': 'TreeMapa' + id,
				'events': 'check_node.jstree uncheck_node.jstree select_node.jstree changed.jstree',
				'filter': function (state) {					
					//state.core.selected = [];
					return state;
				}
			},
			'types': {
				"#": {
					"valid_children": ['folder']
				},				
				'folder': {
					"valid_children": [],
					"icon": "fa fa-folder"
				},
				'default': {
					"icon": "fa fa-file-o",
					"valid_children": []
				}
			},
			'plugins': ['checkbox', 'search', 'dnd', 'state', 'types']
		}).on('click.jstree', function () {
			// hide all qtips
			$('.qtip').qtip('hide');
			toolbar.cancelSelection();
		}).on('move_node.jstree', function (e, node) {
			var vm = {
				IdMapa: Route.routeOptions.id,
				Tree: treeElement.jstree('get_json')
			};
			$.ajax({
				url: '/Mapeamento/Mapa/SaveTree',
				type: 'POST',
				data: ko.toJSON(vm),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8'
			}).done(function () {
				updateTree().done(function () {
					treeElement.removeClass('is-loading');
					mapaElemento.MapaUpdate(Route.routeOptions.id).done(function () {
						treeElement.jstree().restore_state();
					});
				});
			}).error(function () {
			}).progress(function () {
				treeElement.addClass('is-loading');
			});

		}).on('copy_node.jstree', function (e, node) {
		}).on('before_open.jstree', function (e, n) {
		}).on('check_node.jstree', function (e, n) {
			

			if (n.node.type === 'folder') {
				treeElement.jstree('open_node', n.node);
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false });
				children.shift()
				children.forEach(function (d) {
					_mapService.layerShow(d.data.id);				
				});
			} else {				
				_mapService.layerShow(n.node.data.id);
			}
		}).on('uncheck_node.jstree', function (e, n) {
			if (n.node.type === 'folder') {
				treeElement.jstree('close_node', n.node);
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false });
				children.shift()
				children.forEach(function (d) {
					_mapService.layerHide(d.data.id);
				});
			} else {
				_mapService.layerHide(n.node.data.id);
			}
		}).on('select_node.jstree', function (e, n) {		
			if (n.node.type != 'folder') {
				var id = n.node.data.id;
				console.log(id);
				_mapService.layerSelectZoom(id);
			} else {
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false }),
					arrIds = [];
				children.shift();
				children.forEach(function (d) {
					arrIds.push(d.data.id);
				});				
				_mapService.layersSelectZoom(arrIds);
			}
		}).on('deselect_node.jstree', function (e, n) {			
		}).on('load_node.jstree', function (node, status) {
			_setupEvents();			
		}).on('delete_node.jstree', function (e, node) {
		}).on('ready.jstree', function (e, node) {			
			treeUtil.searchEvents(treeElement, $('#txtPesquisaTree'));			
			if (!treeElement.jstree().restore_state()) {
				// se estado da árvore estiver vazio, trazer todos os checboxes ativos por padrão
				treeElement.jstree().open_all();
				var elementos = treeElement.jstree(true).get_json('#', {});
				elementos.forEach(function (node) {
					treeElement.jstree().check_node(node);
				});
			}
			_mapService.layersSelectZoom();
			_setupEvents();
			
		}).on('refresh.jstree', function () {
			treeElement.jstree().restore_state();
			_setupEvents();
		});
	};

	var options = {		
		removerCamada: function () { },
		editarCamada: function () { }
	};

	var id = null;

	var init = function (mapService, id, o) {
		_mapService = mapService;
		id = id;
		$.extend(true, options, o);
		// carrega árvore pela primeira vez
		return requestTree(id).done(function (response) {
			if (response.length > 0) {
				createTree(JSON.parse(response), id);				
			} else {
				createTree([], id);
			}
			//if (response.hasOwnProperty('Sucesso') && response.Sucesso == true) {
			//	var treeData = response.hasOwnProperty('Data') ? response.Data : [];
			//	createTree(treeData);				
			//} else {
			//	toastr.error('Ocorreu um erro ao carregar a árvore de camadas');
			//}
		});
	};

	

	var _setupEvents = function () {
		$('body').on('click', 'a[role=button]', function (e) {
			e.preventDefault();
		});
		$('body').on('mouseover focus', 'a[role=button]', function () {
			// qTip Perfomance optimization			
			var $this = $(this);

			if ($this.hasClass('btn-elemento-opcoes')) {
				if (!$this.data('qtip-attached') && $this.not('.select2-offscreen') && $this.not('.no-tooltip')) {
					$this.qtip($.extend(true, defaults.qtip.dropdown, {
						content: {
							text: '<div id="dropdown-camada">' +
									'<div class="list-group">' +
										'<a href="#" id="editar-elemento" class="list-group-item">Editar Elemento</a>' +
										'<a href="#" id="remover-elemento" class="list-group-item">Excluir Elemento</a>' +
										'<a href="#" id="exportar-kml" class="list-group-item">Exportar KML do elemento</a>' +
									'</div>' +
								  '</div>'
						},
						events: {
							render: function (event, api) {
								var IdMapeamentoElemento = $(event.originalEvent.currentTarget).data('id');

								var tooltip = api.elements.tooltip;
								tooltip.on('click', 'a', function (e) {
									e.preventDefault();
									var action = $(this).attr('id');
									switch (action) {
										case 'editar-elemento':
											options.editarElemento(IdMapeamentoElemento);
											api.toggle(false);
											break;
										case 'exportar-kml':
											options.exportarKML(IdMapeamentoElemento);
											api.toggle(false);
											break;
										case 'remover-elemento':
											var dfd = options.removerElemento(IdMapeamentoElemento);
											api.toggle(false);
											break;
										default:
									}
								});
							}
						}
					}));
					$this.data('qtip-attached', true);
					// the qtip handler for the event may not be called since we just added it, so you   
					// may have to trigger it manually the first time.
					$this.trigger('mouseover');
				}
			}


			if ($this.hasClass('btn-camada-opcoes')) {
				if (!$this.data('qtip-attached') && $this.not('.select2-offscreen') && $this.not('.no-tooltip')) {
					$this.qtip($.extend(true, defaults.qtip.dropdown, {
						content: {
							text: '<div id="dropdown-camada">' +
									'<div class="list-group">' +
										//'<a href="#" id="editar-camada" class="list-group-item">Editar Camada</a>' +
										'<a href="#" id="remover-camada" class="list-group-item">Excluir Camada</a>' +
									'</div>' +
								  '</div>'
						},
						events: {
							render: function (event, api) {
								var id = $(event.originalEvent.currentTarget).data('id');
								var tooltip = api.elements.tooltip;
								tooltip.on('click', 'a', function (e) {
									e.preventDefault();
									var action = $(this).attr('id');
									switch (action) {
										case 'editar-camada':
											options.editarCamada(id);
											api.toggle(false);
											break;
										case 'remover-camada':
											options.removerCamada(id);
											api.toggle(false);
											break;
										default:
									}
								});
							}
						}
					}));
					$this.data('qtip-attached', true);
					// the qtip handler for the event may not be called since we just added it, so you   
					// may have to trigger it manually the first time.
					$this.trigger('mouseover');
				}
			}
		});
	};

	var updateTree = function () {
		var dfd = $.Deferred();
		var idTree = Route.routeOptions.id;
		requestTree(idTree).done(function (response) {
			if (response.length > 0) {
				treeElement.on('redraw.jstree', function () {
					dfd.resolve(true);
				});
				treeElement.jstree(true).settings.core.data = JSON.parse(response);
				treeElement.jstree(true).refresh(true, true);			
			} else {
				treeElement.on('redraw.jstree', function () {
					dfd.resolve(false);
				});
				treeElement.jstree(true).settings.core.data = [];
				treeElement.jstree(true).refresh(true, false);				
			}			
		});
		return dfd.promise();
	};

	return {
		init: init,
		update: updateTree,
		selectElement: function (idElement) {
			treeElement.jstree().select_node(idElement);
		},
		getChildrenElementCount: function (nodeId) {
			return treeElement.jstree().get_json(nodeId, { flat: true, no_data: false });
		},
		deselectAll: function () {			
			treeElement.jstree().deselect_all();
		},		
		getSelectedElementLayer: function () {
			var selected = treeElement.jstree().get_selected();
			if (selected.length > 0) {
				return treeElement.jstree().get_node(selected[0]);
			} else {
				return null;
			}
		},
		checkElement: function (idElement) {
			treeElement.jstree().check_node(idElement);
		}
	}
});