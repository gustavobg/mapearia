﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'bootbox', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (camada, ko, mgFeedbackBase, toastr, bootbox) {

	var vm = {
			data: ko.observable(null),
			dadosComplCamadaSuperior: ko.observable()
		},
		defaultResponse = {},
		afterBind = function () {},
        bind = function (id, params) {
			// parâmetros customizados para rota injetada
        	var params = $.extend(true, { external: false, IdMapeamentoMapa: null, successCallback: function () {} }, params);

            camada.config({
                saveUrl: '/Mapeamento/Camada/NewEdit',
                getUrl: '/Mapeamento/Camada/NewEditJson'
            });

            var request = camada.get({ id: id }),
                form = $('#form-MapeamentoCamada-NewEdit');

            afterBind = function () {           	

            	var d = ko.unwrap(vm.data);

            	//if (params.external) {
            	//	d.IdMapeamentoMapa(params.IdMapeamentoMapa);
            	//}

            	vm.DescricaoMapeamentoCategoriaCamada = ko.observable('');
            	vm.dadosComplCamadaSuperior({ 'Ativo': true, 'UltimoNivel': false, 'IdMapeamentoCamadaNotIn': d.IdMapeamentoCamada() });

            	vm.ExibeCategoria = ko.pureComputed(function () {
            		if (d.UltimoNivel() != null && d.UltimoNivel() == true) {
            			return true;
            		}
            		else {
            			d.IdMapeamentoCamadaCategoria(null);
            			return false;
            		}
            	}, this);

            	ko.computed(function () {
            		if (vm.DescricaoMapeamentoCategoriaCamada() != null && (d.Descricao() == null || d.Descricao() == ''))
            			d.Descricao(vm.DescricaoMapeamentoCategoriaCamada());
            	});
            };

            request.done(function (response) {
            	defaultResponse = response;

            	vm.data(ko.mapping.fromJS(response));

            	afterBind();

            	vm.Save = function () {
            		var d = ko.unwrap(vm.data);
                	if (!d.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                    	data: ko.toJSON(d),
                        url: '/Mapeamento/Camada/NewEdit/',
                        success: function (result) {
                        	if (params.external) {
                        		mgFeedbackBase.feedbackCrudRoute(result, 'Camada salva com sucesso.', false);
                        		params.successCallback(result);
                        	} else {
                        		mgFeedbackBase.feedbackCrudRoute(result, 'Camada salva com sucesso.');
                        	}
                        }
                    });
                };

            	ko.applyBindings(vm, form[0]);
            });

            return request;
        };

	var removeCamada = function (idMapeamentoCamada) {
		
	}

    return {
    	bind: bind,
    	edit: function (id) {
    		camada.config({    			
    			getUrl: '/Mapeamento/Camada/NewEditJson'
    		});
    		camada.get({ id: id }).done(function (response) {
    			vm.data(ko.mapping.fromJS(response));
    			afterBind();
    		});
    	},
    	remove: function (idMapeamentoCamada, ignoreConfirm) {
    		var dfd = $.Deferred();

    		if (ignoreConfirm) {
    			$.ajax({
    				type: 'POST',
    				url: 'Mapeamento/Mapa/ExcluirCamadaMapa',
    				data: 'idMapeamentoCamada=' + idMapeamentoCamada + '&idMapeamentoMapa=' + Route.routeOptions.id
    			}).done(function (response) {
    				if (response.hasOwnProperty('Sucesso')) {
    					if (response.Sucesso === true) {
    						//toastr.success('Camada removida com sucesso');
    						dfd.resolve(true);
    					} else {
    						toastr.error('Ocorreu um erro ao remover a camada');
    						dfd.resolve(false);
    					}
    				} else {
    					toastr.error('Ocorreu um erro ao remover a camada');
    					dfd.resolve(false);
    				}
    			});

    		} else {

    			bootbox.confirm({
    				title: "Deseja remover essa camada?",
    				message: "Ao remover essa camada, irá remover também todas as camadas, áreas, linhas e marcadores filhos.",
    				className: 'modal-sm',
    				buttons: {
    					cancel: {
    						label: '<i class="fa fa-times"></i> Cancelar'
    					},
    					confirm: {
    						label: '<i class="fa fa-check"></i> Confirmar'
    					}
    				},
    				callback: function (result) {
    					if (result) {
    						$.ajax({
    							type: 'POST',
    							url: 'Mapeamento/Mapa/ExcluirCamadaMapa',
    							data: 'idMapeamentoCamada=' + idMapeamentoCamada + '&idMapeamentoMapa=' + Route.routeOptions.id
    						}).done(function (response) {
    							if (response.hasOwnProperty('Sucesso')) {
    								if (response.Sucesso === true) {
    									toastr.success('Camada removida com sucesso');
    									dfd.resolve(true);
    								} else {
    									toastr.error('Ocorreu um erro ao remover a camada');
    									dfd.resolve(false);
    								}
    							} else {
    								toastr.error('Ocorreu um erro ao remover a camada');
    								dfd.resolve(false);
    							}
    						});
    					} else {
    						dfd.resolve(false);
    					}
    				}
    			});
    		}
    		return dfd.promise();
    	},
    	resetModel: function () {
    		vm.data(ko.mapping.fromJS(defaultResponse));
    		afterBind();
    	}
    }
});