﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'ko-validate-rules', 'ko-validate'], function (categoria, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            categoria.config({
                saveUrl: '/Mapeamento/Categoria/NewEdit',
                getUrl: '/Mapeamento/Categoria/NewEditJson'
            });

            var request = categoria.get({ id: id }),
                form = $('#form-Categoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.ExibeCategoria = ko.observable(true);
                vm.dadosCompleCategoriaRestricaoVM = ko.observable({ 'Ativo': true, 'IdMapeamentoCamadaCategoriaNotIn': vm.IdMapeamentoCamadaCategoria() });

                vm.Save = function () {
                	if (!vm.isValidShowErrors()) { return; };

                	categoria.save(ko.toJSON(vm)).done(function (result, status, xhr) {
                		mgFeedbackBase.feedbackCrudRoute(result, 'Categoria salva com sucesso.');
                	});
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});