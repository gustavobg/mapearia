﻿define(['jquery', 'crud-controller', 'knockout', 'feedback', 'toastr', 'util', 'select2', 'ko-validate-rules', 'ko-validate', 'bootstrap/modal'], function ($, configuracao, ko, mgFeedbackBase, toastr, utils) {

	var vm = {},		
		modalConfiguracoes = null,
		idMapa = null,
        _bind = function (id) {
        	configuracao.config({
        		saveUrl: '/Mapeamento/Configuracao/NewEdit',
        		getUrl: '/Mapeamento/Configuracao/ListSimple'
        	});

        	var request = configuracao.get({ id: id }),
                form = $('#form-ConfiguracaoMapeamento-NewEdit');

        	request.done(function (response) {
        		vm = ko.mapping.fromJS(response);
        		vm.IdUnidadeAreaExtraCompl = ko.observable({ Ativo: true, IdUnidadeIn: '20,28,30,31' });
        		vm.IdUnidadeMedidaExtraCompl = ko.observable({ Ativo: true, IdUnidadeIn: '1,12,13' });

        		vm.Save = function () {
        			if (!vm.isValidShowErrors()) { return; };

        			configuracao.save(ko.toJSON(vm)).done(function (result, status, xhr) {
        				mgFeedbackBase.feedbackCrudRoute(result, 'Configuracao salva com sucesso.', false);
        				modalConfiguracoes.modal('hide');
        			});
        		}
        		if (!utils.isBound('form-ConfiguracaoMapeamento-NewEdit')) {
        			ko.applyBindings(vm, form[0]);
        		}        		

        	});

        	return request;
        },
        init = function (id) {        	
        	idMapa = id;
        	modalConfiguracoes = $('#modal-preferencias').modal('hide');
        	$('#btn-preferencias').on('click', function () {        		
        		openEdit();
        	});
        },
        openEdit = function () {
        	_bind(idMapa).done(function () {
        		modalConfiguracoes.modal();
        	});        		
        }
	return {
		init: init,		
		openEdit: openEdit
	}
});