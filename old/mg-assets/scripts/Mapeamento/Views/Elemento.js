﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'component.areaPositionInfo', 'map.layerEdit', 'bootbox', 'jquery-flipper', 'jquery-inputmask', 'select2', 'ko-validate-rules', 'ko-validate'], function (controller, ko, mgFeedbackBase, toastr, area, layerEditorService, bootbox) {
	var _mapService = null;
	// validação Usuário
	ko.validate.rules['validarDescricao'] = {
		valid: true,
		message: 'Descrição do elemento já existe, escolha outro nome.',
		validator: function (descricao, options) {
			ko.computed(function () {
				
				var tipoObjetoSistema = ko.unwrap(options.tipoObjetoSistema);
				if (ko.validate.utils.isEmptyVal(descricao)) {
					ko.validate.rules.validarDescricao.valid = true;
					return true;
				}
				if (tipoObjetoSistema !== null && tipoObjetoSistema !== '') {
					return true;
				}

				$.ajax({
					url: '/Mapeamento/Elemento/ValidarDescricaoElementoPorMapa',
					dataType: 'json',
					type: 'POST',
					contentType: 'application/json; charset=utf-8',
					data: JSON.stringify({ descricao: ko.unwrap(descricao), idMapeamentoMapa: ko.unwrap(options.idMapeamentoMapa), idMapeamentoElemento: ko.unwrap(options.idMapeamentoElemento) }),
					async: false
				}).success(function (result) {
					ko.validate.rules.validarDescricao.valid = result.Sucesso;
				});
			});
		}
	};

	// binding handler
	ko.bindingHandlers.navTabs = {
		init: function (element, valueAccessor, allBindings, viewModel) {
			var $element = $(element);
			var tabs = $element.find('li');
			var tabsLinks = tabs.find('a');
			var navContent = $element.next('.nav-content');
			var navTabs = navContent.find('.nav-tab');

			tabsLinks.on('click', function (e) {
				var link = $(this);
				e.preventDefault();
				tabs.removeClass('active');
				link.parent().addClass('active');
				navTabs.hide();
				$(link.attr('href')).show();
			});
		}
	};	
	
	var vm = {
		IdMapeamentoElemento: ko.observable(null),		
		IdMapeamentoElementoGeo: ko.observable(null),
		Elemento: ko.observable(),
		fillColor: ko.observable(),
		strokeWeight: ko.observable(),
		fillOpacity: ko.observable(),
		iconName: ko.observable(),
		layer: null,
		ValidaDescricao: function (TipoObjetoSistema) {			
			return false;
		},		
		dadosComplCamadaSuperior: ko.observable(),
		dadosComplLocalProducao: ko.observable({ Ativo: true, SemItensRelacionadosComMapeamento: true, ProducaoLocalTipoPermiteRepresentacaoGeo: true }),
		dadosComplProducaoLocalUnidade: ko.observable({ 'Ativo': true }),
		vmDataProducaoLocal: ko.observable(),
		vmDataProducaoUnidade: ko.observable(),
		ProducaoLocalDisabled: ko.observable(false),
		LocalProducaoUnidadeDisabled: ko.observable(false),
		DescricaoCamadaSuperior: ko.observable(),
		usaCorPredefinida: ko.observable(false)
	},
	vmRequest = $.Deferred(),
	defaultResponse = {},
	remove = function (IdMapeamentoElemento, IdMapeamentoCamada) {
		var dfd = $.Deferred();
		bootbox.confirm({
			title: "Deseja remover esse elemento?",
			message: "Ao remover o elemento a ação não poderá ser desfeita.",
			className: 'modal-sm',
			buttons: {
				cancel: {
					label: '<i class="fa fa-times"></i> Cancelar'
				},
				confirm: {
					label: '<i class="fa fa-check"></i> Confirmar'
				}
			},
			callback: function (result) {				
				if (result) {
					$.ajax({
						type: 'POST',
						url: '/Mapeamento/Mapa/ExcluirElemento',
						data: 'idMapeamentoElemento=' + IdMapeamentoElemento
					}).done(function (response) {
						if (response.hasOwnProperty('Sucesso')) {
							if (response.Sucesso === true) {
								toastr.success('Elemento removido com sucesso');
								dfd.resolve(true, IdMapeamentoElemento, IdMapeamentoCamada);
							} else {
								toastr.error('Ocorreu um erro ao remover o elemento');
								dfd.resolve(false);
							}
						} else {
							toastr.error('Ocorreu um erro ao remover o elemento');
							dfd.resolve(false);
						}
					});
				} else {
					dfd.resolve(false);
				}
			}
		});		
		return dfd.promise();
	},		
		isBound = function (id) {
			return ko.contextFor(id) && ko.contextFor(id).hasOwnProperty('$data') ? true : false;
		},
        bind = function (mapService, IdMapeamentoElemento, IdMapeamentoElementoGeo, vmLayerEdit) {
        	_mapService = mapService;
        	vm.fillColor = vmLayerEdit.fillColor; // observable
        	vm.fillOpacity = vmLayerEdit.fillOpacity;
        	vm.strokeWeight = vmLayerEdit.strokeWeight;
        	vm.iconName = vmLayerEdit.iconName;
        	vm.DescricaoCamadaSuperior('');        

        	var elemento = new controller.instance({
        		saveUrl: '/Mapeamento/Elemento/NewEdit',
        		getUrl: '/Mapeamento/Elemento/NewEditJson'
        	});

        	var elementoGeo = new controller.instance({
        		saveUrl: '/Mapeamento/ElementoGeo/NewEdit',
        		getUrl: '/Mapeamento/ElementoGeo/NewEditJson'
        	});

        	vm.layer = vmLayerEdit.layer; // observable
        	

        	if (IdMapeamentoElemento === null) {
        		vm.IdMapeamentoElemento(layer.getFeatureId());
        	} else {
        		vm.IdMapeamentoElemento(IdMapeamentoElemento);
        	}

        	vmRequest = $.when(elemento.get({ id: IdMapeamentoElemento }), elementoGeo.get({ id: IdMapeamentoElementoGeo })),
                form = $('#element-edit');

        	vmRequest.done(function (response) {        		
        		var vmElemento = ko.mapping.fromJS(response[0]);
        		//vmElemento.IdMapeamentoCamada(null);
        		
        		vm.Elemento(vmElemento);

        		// quanto tipo de objeto sistema for local de produção, e cor vinculada "ProducaoLocalTipoCor", utiliza-la (vmDataProducaoLocal)
        		// quanto tipo de objeto sistema for unidade de produção, e cor vinculada "ProducaoUnidadeTipoCor" utiliza-la (vmDataProducaoUnidade)
        		ko.computed(function () {
        			var IdMapeamentoElemento = vm.Elemento().IdMapeamentoElemento.peek();
        			var TipoObjetoSistema = vm.Elemento().TipoObjetoSistema();
        			var dataProducaoLocal = ko.unwrap(vm.vmDataProducaoLocal);
        			var dataProducaoUnidade = ko.unwrap(vm.vmDataProducaoUnidade);

        			if (IdMapeamentoElemento === null) { // somente novos elementos
        				var layer = vm.layer.peek();
        				if (TipoObjetoSistema == 2 && dataProducaoLocal && dataProducaoLocal.ProducaoLocalTipoCor !== null) {
        					var corSelecionada = dataProducaoLocal.ProducaoLocalTipoCor;
        					vm.fillColor(corSelecionada);
        					layer.setStyle({ fillColor: corSelecionada, color: corSelecionada });
        					vm.usaCorPredefinida(true);
        				} else if (TipoObjetoSistema == 3 && dataProducaoUnidade && dataProducaoUnidade.ProducaoUnidadeTipoCor !== null) {
        					var corSelecionada = dataProducaoUnidade.ProducaoUnidadeTipoCor;
        					vm.fillColor(corSelecionada);
        					layer.setStyle({ fillColor: corSelecionada, color: corSelecionada });
        					vm.usaCorPredefinida(true);
        				}
        				vm.usaCorPredefinida(false);
        			}
        		}).extend({ rateLimit: { timeout: 50, method: "notifyWhenChangesStop" } });

        		vm.Elemento().TipoObjetoSistema.subscribe(function (v) {
        			if (v) {
        				vm.Elemento().IdReferencia(null);
        				vm.Elemento().IdReferenciaAuxiliar(null);
        			}
        		});
        	
        		// ao pre-selecionar uma camada pela arvore, selecionar na edição do elemento
        		ko.computed(function () {
        			var IdMapeamentoElemento = vm.Elemento().IdMapeamentoElemento.peek();
        			var IdMapeamentoCamada = vm.Elemento().IdMapeamentoCamada.peek();

        			if (IdMapeamentoElemento === null) {
        				var categoriaSelecionada = treeMapa.getSelectedElementLayer();
        				if (categoriaSelecionada && (IdMapeamentoCamada === '' || IdMapeamentoCamada === null)) {
        					vm.DescricaoCamadaSuperior('');
        					categoriaSelecionada = categoriaSelecionada.data.id2;
        					vm.Elemento().IdMapeamentoCamada(categoriaSelecionada);
        				}        				
        			}
        		});
        		// ao alterar a categoria da camada, sugere uma descrição
        		ko.computed(function () {        			
        			var v = ko.unwrap(vm.DescricaoCamadaSuperior);        			
        			if (vm.Elemento().IdMapeamentoElemento.peek() === null) {
        				vm.Elemento().Descricao(v);
        			}
        		});

        		ko.computed(function () {        			
        			var TipoObjetoSistema = ko.unwrap(vm.Elemento().TipoObjetoSistema); // 2        			
        			if (TipoObjetoSistema == 2 && mapaElemento.IdReferencia !== null) {
        				vm.dadosComplLocalProducao({ Ativo: true, SemItensRelacionadosComMapeamento: true, ProducaoLocalTipoPermiteRepresentacaoGeo: true, IdProducaoLocal: mapaElemento.IdReferencia });
        				//vm.ProducaoLocalDisabled(true);
        				//vm.Elemento().IdReferencia(mapaElemento.IdReferencia);
        			} else {
        				//vm.ProducaoLocalDisabled(false);
        			}
        		});

        		
				
        	
        		vm.IdMapeamentoElementoGeo(IdMapeamentoElementoGeo);
        		var isPolygon = ko.unwrap(vm.layer).getType() === mapUtils.type.POLYGON;
        		
        		ko.computed(function () {        			
        			var TipoObjetoSistema = ko.unwrap(vm.Elemento().TipoObjetoSistema); // 3
        			if (TipoObjetoSistema == 3 && mapaElemento.IdReferencia !== null) {
        				vm.dadosComplProducaoLocalUnidade({ 'Ativo': true, 'IdProducaoLocal': mapaElemento.IdReferencia });
        				//vm.LocalProducaoUnidadeDisabled(true);
        				//vm.Elemento().IdReferenciaAuxiliar(mapaElemento.IdReferencia);
        			}
        		});

        		ko.computed(function () {
        			var perimetro = mapaElemento.getLayerPerimetro();
        			if (perimetro !== null) {
        				if (perimetro.feature.properties.IsProducaoLocal === true && perimetro.feature.properties.MapeamentoElementoIdReferencia !== null && ko.unwrap(vm.Elemento().TipoObjetoSistema) == 3) {
        					vm.dadosComplProducaoLocalUnidade({
        						'Ativo': true,
        						'IdProducaoLocalIn': perimetro.feature.properties.MapeamentoElementoIdReferencia
        					});
        				}
        			}
        		});
        		
        		vm.dadosComplCamadaSuperior({ 'Ativo': true, 'UltimoNivel': true, 'IdMapeamentoCamadaCategoriaIn': ko.unwrap(mapaElemento.IdMapeamentoCamadaCategoriaFiltro), 'IdMapeamentoMapa': Route.routeOptions.id, 'SemCamadaBaseAtual': true, 'MapeamentoCamadaCategoriaBase': isPolygon ? mapaElemento.permiteDesenharPerimetro() : false });
        		
        		if (!isBound(form[0])) {
        			defaultResponse = response[0];
        			vm.Save = save;
        			ko.applyBindings(vm, form[0]);
        		}
        	});

        	return vmRequest;

        };
		var save = function (ignoreFeedback) {

			ignoreFeedback = ignoreFeedback && ignoreFeedback === true ? ignoreFeedback : false;

			var elementoSave = new controller.instance({
				saveUrl: '/Mapeamento/Elemento/SaveElemento'
			});

			if (!vm.Elemento().isValidShowErrors()) { return; };

			// Informações Elemento
			var dataElemento = ko.toJS(vm.Elemento);

			if (dataElemento.IdMapeamentoElemento) {
				var layer = _mapService.getLayerById(dataElemento.IdMapeamentoElemento);
			} else {
				// new layer
				var layer = ko.unwrap(layerEditorService.getLayer());
				// set temp id
				dataElemento.IdMapeamentoElemento = layer.getFeatureId();
			}				
			var a = area.getAreaByLayer(layer);
			var wkt = new Wkt.Wkt();
			var estilo = {};

			estilo = _mapService.getLayerStyles(layer);
			estilo.draggable = false;
			estilo.editable = false;
			
			// Informações Geo
			dataElemento.ElementoGeo = {};
			dataElemento.ElementoGeo.IdMapeamentoElementoGeo = ko.unwrap(vm.IdMapeamentoElementoGeo);
			dataElemento.ElementoGeo.Area = a.area_m2;
			dataElemento.ElementoGeo.Comprimento = a.perimeter_m;			
			dataElemento.ElementoGeo.Estilo = JSON.stringify(estilo);
			dataElemento.IdMapeamentoMapa = Route.routeOptions.id;
			
			var layerSaved = layerEditorService.save({
				id: dataElemento.IdMapeamentoElemento,
				layerGroupId: dataElemento.IdMapeamentoCamada,
				properties: {					
					IdMapeamentoCamada: dataElemento.IdMapeamentoCamada,
					IdMapeamentoElemento: dataElemento.IdMapeamentoElemento,
					IdMapeamentoElementoGeo: dataElemento.ElementoGeo.IdMapeamentoElementoGeo,
					MapeamentoElementoDescricao: dataElemento.MapeamentoElementoDescricao,
					MapeamentoElementoObservacao: dataElemento.MapeamentoElementoObservacao,
					status: 'save-pending'
				}
			});

			wkt.fromJson(layerSaved.toGeoJSON());
			dataElemento.ElementoGeo.wkt = wkt.write();

			elementoSave.save(ko.toJSON(dataElemento)).done(function (result, status, xhr) {
				if (!ignoreFeedback) {
					if (result.hasOwnProperty('Response') && result.Response.Sucesso == true) {
						mgFeedbackBase.feedbackCrudRoute(result, 'Elemento salvo com sucesso.', false);											
						_mapService.getInstance().fire('mg.elemento.save');
						// update ids
						var updatedFeature = layerSaved.toGeoJSON();
						updatedFeature.IdMapeamentoElementoGeo = result.IdMapeamentoElementoGeo;
						updatedFeature.properties.IdMapeamentoElemento = result.IdMapeamentoElemento;
						updatedFeature.properties.IdMapeamentoCamada = result.IdMapeamentoCamada;
						updatedFeature.properties.MapeamentoElementoDescricao = result.Descricao;
						updatedFeature.properties.MapeamentoElementoObservacao = result.Observacao;
						updatedFeature.properties.status = 'saved';

						var isElementoBase = result.ElementoGeo.MapeamentoCamadaCategoriaBase;

						_mapService.layerUpdate(updatedFeature, layerSaved, layer.getLayerGroupId(), result.IdMapeamentoElemento);

						mapaElemento.MapaUpdate(Route.routeOptions.id).done(function () {
							treeMapa.update().done(function () {
								window.setTimeout(function () {
									treeMapa.deselectAll();
									if (!isElementoBase) {
										treeMapa.checkElement('li-' + Route.routeOptions.id + '-' + result.IdMapeamentoCamada);
										treeMapa.selectElement('li-' + Route.routeOptions.id + '-' + result.IdMapeamentoCamada + '-' + result.IdMapeamentoElemento);
									}
								}, 100);
							});
						});
						

					} else {
						//toastr.error('Ocorreu um erro ao salvar o objeto do mapa');
						toastr.error(result.Response.MensagemFinal, '', { timeOut: "12000" });
						_mapService.layerRemove(layerSaved.getFeatureId(), layerSaved.getLayerGroupId());
						toolbar.setMode(mapUtils.toolbarMode.DRAW);
					}
				} 
			});
		};

	var resetModel = function () {
		vm.Elemento(ko.mapping.fromJS(defaultResponse));	
	};

	return {
		bind: bind,
		//setModel: setModel,
		//getModel: getModel,
		//getViewModel: getViewModel,
		remove: remove,		
		//resetModel: resetModel,
		//updateWkt: updateWkt,
		save: save
	}
});