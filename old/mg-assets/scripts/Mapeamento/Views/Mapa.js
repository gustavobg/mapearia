﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (MapaCrud, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            MapaCrud.config({
                saveUrl: '/Mapeamento/Mapa/NewEdit',
                getUrl: '/Mapeamento/Mapa/NewEditJson'
            });

            var request = MapaCrud.get({ id: id }),
                form = $('#form-MapeamentoMapaCrud-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);
                    MapaCrud.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Mapa salvo com sucesso.');
                    });
                }

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});