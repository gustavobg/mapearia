﻿define([
	'jquery',
	'knockout',
	'crud-controller',
	'map.service', 
	'map.toolbar', 
	'leaflet.omnivore', 
	'../Views/Elemento', 
	'util', 
	'../Scripts/ModalCamadas', 
	'../Scripts/ModalMapa', 
	'../Scripts/TreeMapa', 
	'../Views/Configuracao', 
	'map.layerEdit',
	'toastr',
	'intro',
	'vanillaUniform',
	'bootbox',
	'map.utils',
	'panels', 
	'component.areaPositionInfo', 
	'component.colorSelector', 
	'component.iconSelector', 
	'knockout-upload-simple',
	'bootstrap-select'
], function ($, ko, mapaController, mapService, toolbar, omnivore, Elemento, utils, modalCamadas, modalMapa, treeMapa, configuracaoMapa, layerEditorService, toastr, introJs, vanillaUniform, bootbox) {

	var vmMapa = {};	
	var _hasPerimeter = false;
	var _hasElements = false;	

	var limparLocalStorage = function (id) {
		// todo
		//localStorage.setItem('IdMapeamentoCamadaCategoriaFiltro-' + Route.routeOptions.id, valorFiltro);
		//localStorage.setItem('VisibilidadePerimetro-' + Route.routeOptions.id, true);
		//localStorage.setItem('MapaPrimeiroAcesso-' + Route.routeOptions.id, true);
	};

	var togglePanel = function () {
		var container = $('#route-map');
		if (container.hasClass('aside-closed')) {
			container.removeClass('aside-closed');
		} else {
			container.addClass('aside-closed');
		}
		window.setTimeout(function () {
			mapService.getInstance().invalidateSize();
		}, 200);
	};

	var perimetroSelecionar = function (data, e) {
		e.preventDefault();
		var link = $(e.currentTarget);
		mapService.layerSelectZoom(link.data('id'));
	};
	
	var uploadKmlSuccess = function (r, e) {
		try {
			var layer = omnivore.kml(window.location.protocol + "//" + window.location.host + '/temp/Mapeamento/' + r.File)
				.on('ready', function () {
					if (layer.getLayers().length > 0) {
						mapService.getInstance().flyToBounds(layer.getBounds(), { animate: false });
					} else {
						toastr.warning('Nenhum elemento encontrado no KML');
					}
				}).addTo(mapInstance);

		} catch (err) {
			toastr.error('Erro ao importar KML')
		}
	};	
	
	var init = function (idMapa) {

		var mapInstance = null;
		var panelEdit = null;
		var initialized = false;		

		vmMapa = {
			listaMapasUsuario: ko.observable(null),
			togglePanel: togglePanel,
			elementosCount: 0,
			perimetroParams: ko.observable({ disabled: true, visible: false }),
			perimetroVisivel: ko.observable(false),
			perimetroDisabled: ko.observable(true),
			perimetroText: ko.observable('Nenhum perímetro definido'),
			perimetroSelecionar: perimetroSelecionar,
			IdMapeamentoCamadaCategoriaFiltro: ko.observable(valorPadraoFiltro()),
			exibeModalEscolhaCamada: ExibeModalEscolhaCamada,
			uploadKmlSuccess: uploadKmlSuccess,
			uploadKmlError: function (r, e) {
				toastr.error(r.Mensagem);
			},
			title: ko.observable(''),
			editMode: function () {
				var edit = $('#modo-edicao'),
					view = $('#modo-consulta');
				view.hide();
				edit.show();
				toolbar.setMode(mapUtils.toolbarMode.DRAW);
			},
			ElementoRemove: ElementoRemove,
			MapaUpdate: MapaUpdate,			
			permiteDesenharPerimetro: function () {
				if (_hasPerimeter) {
					return false;
				} else if (_hasElements) {
					return false;
				} else {
					return null;
				}				
			},
			getLayerPerimetro: getLayerPerimetro
		};

		window.mapaElemento = vmMapa;
		
		mapService.init({
			attributionControl: false,
			minZoom: 6,
			maxZoom: 17,
			useSingleLayerGroup: true,
			checkPolygonIntersections: false, // desativa crop do cliente
			draggablePolygons: false,
			eachLayerFeature: eachLayerFeature,
			showMeasurements: false
		});



		var formatDistance = function (a) {
			// m					
			var m = a;
			if (m > 1000) {
				return (m / 1000).toFixed(2) + ' km'
			} else {
				return m.toFixed(2) + ' m'
			}
		};

		var formatArea = function (a) {
			var m2 = a;
			if (m2 > 10000) {
				return (m2 / 10000).toFixed(2) + ' ha'
			} else {
				return (m2).toFixed(2) + ' km';
			}
		};


		var eachLayerFeature = function (layer, feature) {
			return layer;
		};

		L.DrawToolbar.include({
			getModeHandlers: function (map) {
				return [
					{
						enabled: this.options.marker,
						handler: new L.Draw.Marker(map, this.options.marker),
						title: L.drawLocal.draw.toolbar.buttons.marker
					},
					{
						enabled: this.options.polyline,
						handler: new L.Draw.Polyline(map, this.options.polyline),
						title: L.drawLocal.draw.toolbar.buttons.polyline
					},
					{
						enabled: this.options.polygon,
						handler: new L.Draw.Polygon(map, this.options.polygon),
						title: L.drawLocal.draw.toolbar.buttons.polygon
					},
					{
						enabled: this.options.rectangle,
						handler: new L.Draw.Rectangle(map, this.options.rectangle),
						title: L.drawLocal.draw.toolbar.buttons.rectangle
					},
					{
						enabled: this.options.circle,
						handler: new L.Draw.Circle(map, this.options.circle),
						title: L.drawLocal.draw.toolbar.buttons.circle
					}					
				];
			}
		});

		mapInstance = mapService.getInstance();
		layerEditorService.init(mapService);
		toolbar.init(mapService, mapService.getFeatureGroup());
		toolbar.setMode(mapUtils.toolbarMode.VIEW);

		window.toolbar = toolbar;

		window.layerEditorService = layerEditorService;

		panelEdit = $('#aside-edit').panelSet({
			modal: false,
			afterCloseAll: function () {
			}
		}).on('click', '.panel-close', function () {
			layerEditorService.cancel();
		});

		if (!initialized) {
			utils.insertCSS('/mg-assets/styles/mapeamento/mapeamento.css');
			utils.insertCSS('/mg-assets/styles/mapeamento/bootstrap-colorpicker.css');
			initialized = true;
		}
		/*	
			Foco: Importar GeoJson, editar elementos e visualizar.
			Recursos de Edição:
				- Navegação de camadas e elementos (árvore);
				- Desenhos de polígono, pontos e linhas;
				- Detecção de colisão entre elementos de uma mesma camada, com tomada de decisão.
				- Multiseleção de camadas, com opção de criar multipoligonos, duplicar, remover.
				- Editar estilos de features, aplicar estilos por camada.
		
				*/

		// map events	
		var layerEditCancelEvent = function (event, layer) {
			panelEdit.panelSet('closeAll');

			// todo: otimizar atualizações de mapa
			MapaUpdate(Route.routeOptions.id).done(function () {
				treeMapa.update().done(function () {
					window.setTimeout(function () {
					}, 100);
				});
			});			
		};

		var layerEditSaveEvent = function (event, layer) {
			panelEdit.panelSet('closeAll');			
		};

		var layerEditOpenEvent = function (event, layer) {
		};

		mapInstance
			.on('mapearia.layerEdit.cancel', layerEditCancelEvent)
			.on('mapearia.layerEdit.save', layerEditSaveEvent)
			.on('mapearia.featureAdd', function () { })
			.on('mapearia.featureUpdate', function () { })
			.on('mapearia.layerEdit.open', function (layer) {
				layerEditorService.setLayer(layer);
				treeMapa.selectElement(layer.getFeatureId());

				panelEdit.panelSet('openFirst');
				var id = layer.getFeatureId(),
					idMapeamentoElementoGeo = layer.feature.properties.IdMapeamentoElementoGeo;

				Elemento.bind(mapService, id, idMapeamentoElementoGeo, layerEditorService.viewModel);

				if (layer.getType() === mapUtils.type.POLYGON) {
					toolbar.setMode(mapUtils.toolbarMode.EDIT);
				} else {
					toolbar.setMode(mapUtils.toolbarMode.VIEW);
				}
			})
			.on('mg.elemento.save', function () { panelEdit.panelSet('closeAll'); });

			bindMapa(idMapa);

			ko.computed(function () {
				var valorFiltro = ko.unwrap(mapaElemento.IdMapeamentoCamadaCategoriaFiltro);
				var isInitial = ko.computedContext.isInitial();
				if (!isInitial) {
					UpdateMapAndTree();
					localStorage.setItem('IdMapeamentoCamadaCategoriaFiltro-' + Route.routeOptions.id, valorFiltro);
				}
			});


		//}, 150);
	};

	var valorPadraoFiltro = function () {
		var valorFiltro = null;
		if (window.localStorage) {
			valorFiltro = localStorage.getItem('IdMapeamentoCamadaCategoriaFiltro-' + Route.routeOptions.id);			
		}
		if (valorFiltro === null) {
			return '';
		} else {
			return valorFiltro
		}
	};

	var ExibeModalEscolhaCamada = function () {
		var modalHtml = document.getElementById('modal-EscolhaCamada');
		var vm = function () {
			this.IdMapeamentoMapa = ko.observable(Route.routeOptions.id);
			this.IdMapeamentoCamadaPai = ko.observable(null);
			this.NomeArquivo = ko.observable('');
			this.dadosComplCamadaSuperior = ko.observable();
		};

		vm = new vm();
		
		vm.uploadShapeSuccess = function (r, e) {
			if (r.Sucesso === true) {
				toastr.success(r.Mensagem);
				modalHtml.modal('hide');

				MapaUpdate(Route.routeOptions.id).done(function () {
					treeMapa.update().done(function () {
						window.setTimeout(function () {
							treeMapa.checkElement('li-' + Route.routeOptions.id + '-' + ko.unwrap(vm.IdMapeamentoCamadaPai));
						}, 100);
					});
				});
				// reset
				vm.IdMapeamentoCamadaPai(null);
				vm.NomeArquivo(null);
			} else {
				toastr.error(r.Mensagem);
			}
		};
		vm.uploadSubmit = ko.observable();
		vm.uploadShapeError = function (r, e) {
			toastr.error(r.Mensagem);
		};
		vm.SaveEscolhaCamada = function () {
			if (vm.isValidShowErrors()) {
				ko.unwrap(vm.uploadSubmit)();
			}
		};

		vm.dadosComplCamadaSuperior({ 'Ativo': true, 'UltimoNivel': true, 'IdMapeamentoCamadaCategoriaIn': ko.unwrap(mapaElemento.IdMapeamentoCamadaCategoriaFiltro), 'IdMapeamentoCamadaNotIn': vm.IdMapeamentoCamadaPai(), 'IdMapeamentoMapa': vm.IdMapeamentoMapa() });

		if (utils.isBound('modal-EscolhaCamada')) {
			vm.IdMapeamentoMapa(Route.routeOptions.id);			
		} else {
			
			ko.applyBindings(vm, modalHtml);
		}
		modalHtml = $(modalHtml).modal();
	};

	var UpdateMapAndTree = function () {
		MapaUpdate(Route.routeOptions.id).done(function () {
			treeMapa.update().done(function () {
				window.setTimeout(function () {
					treeMapa.deselectAll();
					mapService.layersSelectZoom();
				}, 100);
			});
		});
	}
	

	var ElementoRemove = function (idMapeamentoElemento) {
		var layer = null;
		var idMapeamentoCamada = null;
		if (typeof (idMapeamentoElemento) === 'object') {
			layer = idMapeamentoElemento;
			idMapeamentoElemento = layer.getFeatureId();
			idMapeamentoCamada = layer.getLayerGroupId();
			if (layer.feature.properties.status === 'save-pending') {
				mapService.layerRemove(idMapeamentoElemento, idMapeamentoCamada);
				return;
			}
		}
		// remover do servidor logo após remove elemento do mapa
		Elemento.remove(idMapeamentoElemento).then(function (removed, idMapeamentoElemento) {
			if (removed) {				
				treeMapa.update().done(function () {
					window.setTimeout(function () {
						treeMapa.deselectAll();
						mapService.layersSelectZoom();
					}, 100);
				});
				var layerInfo = mapService.layerRemove(idMapeamentoElemento);

				VerificarVisibilidadePerimetro();
				
				var childrenElements = treeMapa.getChildrenElementCount('li-' + Route.routeOptions.id + '-' + layerInfo.layerGroupId);
				if (childrenElements) {
					var hasChildren = childrenElements.length > 2 ? true : false; // ele mesmo é botão de opções
					// remove camada pai se não tiver mais elementos
					if (!hasChildren) {
						CamadaRemove(layerInfo.layerGroupId, true);
					}
				}
				//if (vmMapa.getElementosCount() === 0) {
				//	toolbar.setMode(mapUtils.toolbarMode.EDIT);
				//	initPerimeterHelp();
				//} else {
				//	toolbar.setMode(mapUtils.toolbarMode.DRAW);
				//}
			}
		});
	};

	var CamadaRemove = function (id, overrideConfirmation) {
		modalCamadas.camada.remove(id, overrideConfirmation).done(function (removed) {
			if (removed) {
				MapaUpdate(Route.routeOptions.id).done(function () {
					treeMapa.update().done(function () {
						window.setTimeout(function () {
							treeMapa.deselectAll();
							mapService.layersSelectZoom();
						}, 100);
					});
				});
			}
		});
	}

	var initPerimeterHelp = function () {
		var polygonTool = document.getElementById('map').getElementsByClassName('leaflet-draw-toolbar-top')[0];
		polygonTool.setAttribute('data-intro', 'Antes de começar a desenhar os elementos do mapa, desenhe o perímetro da sua propriedade utilizando a ferramenta de área');
		polygonTool.setAttribute('data-step', '1');

		var tour = introJs()
		tour.setOption('tooltipPosition', 'auto');
		tour.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
		tour.setOption('overlayOpacity', 0.4);
		tour.setOption('hintButtonLabel', 'Entendi');
		tour.setOption('nextLabel', 'Próximo &rarr;');
		tour.setOption('prevLabel', '&larr; Anterior');
		tour.setOption('skipLabel', 'Pular');
		tour.setOption('showStepNumbers', false);
		tour.setOption('showBullets', false);
		tour.setOption('doneLabel', 'Entendi');

		tour.start();
	};

	var _layerPerimetro = null;
	var getLayerPerimetro = function () {
		if (_layerPerimetro && _layerPerimetro.id && _layerPerimetro.layerGroupId) {
			return mapService.getLayerById(_layerPerimetro.id, _layerPerimetro.layerGroupId);
		} else {
			return null;
		}
	}

	var VerificarVisibilidadePerimetro = function (visibilidadePerimetroLocal) {
		var layerPerimetro = getLayerPerimetro();
		var arvorePerimetro = document.getElementById('arvore-perimetro');
		if (layerPerimetro) {
			// adiciona perímetro fora da árvore
			arvorePerimetro.classList.remove('sem-perimetro');
			vmMapa.perimetroDisabled(false);
			vmMapa.perimetroVisivel(visibilidadePerimetroLocal !== undefined ? visibilidadePerimetroLocal : true);
			//mapa.perimetroText(layerPerimetro.feature.properties.text);
			vmMapa.perimetroText('Perímetro');
			
			arvorePerimetro.getElementsByClassName('btn-elemento-perimetro-zoom')[0].setAttribute('data-id', layerPerimetro.feature.properties.IdMapeamentoElemento);
			arvorePerimetro.getElementsByClassName('btn-elemento-opcoes')[0].setAttribute('data-id', layerPerimetro.feature.properties.IdMapeamentoElemento);			
		} else {
			arvorePerimetro.classList.add('sem-perimetro');
			vmMapa.perimetroDisabled(true);
			vmMapa.perimetroVisivel(false);
		}
	};

	var MapaUpdate = function (idMapa) {
		var dfd = $.Deferred();
		if (idMapa !== 'New') {			
			// recupera informações do servidor			
			$.ajax({
				url: '/Mapeamento/ElementoGeo/RetornaElementosGeoPorIdMapeamentoMapa?IdMapeamentoMapa=' + idMapa + '&idMapeamentoCamadaCategoriaIn=' + ko.unwrap(vmMapa.IdMapeamentoCamadaCategoriaFiltro),
				type: 'POST'
			}).done(function (response) {
				if (response.hasOwnProperty('Data')) {
					mapService.getFeatureGroup().clearLayers();
					
					var data = response.Data;
					var dataLength = data.length;
					var estilo = null;
					var i = 0;
					for (i; i < dataLength;) {
						estilo = data[i].Estilo && data[i].Estilo.length > 0 ? JSON.parse(data[i].Estilo) : {};
						addWkt(data[i].wkt, {
							MapeamentoElementoObservacao: data[i].MapeamentoElementoObservacao,
							MapeamentoElementoDescricao: data[i].MapeamentoElementoDescricao,
							IdMapeamentoElementoGeo: data[i].IdMapeamentoElementoGeo,
							IdMapeamentoElemento: data[i].IdMapeamentoElemento,
							IdMapeamentoCamada: data[i].IdMapeamentoCamada,
							MapeamentoCamadaCategoriaBase: data[i].MapeamentoCamadaCategoriaBase,
							IsProducaoLocal: data[i].isProducaoLocal,
							MapeamentoElementoIdReferencia: data[i].MapeamentoElementoIdReferencia,
							styles: estilo,
							text: data[i].MapeamentoElementoDescricao
						}, data[i].IdMapeamentoCamada);
						i = i + 1;
					}
					
					_hasPerimeter = false;
					_hasElements = false;

					mapService.getAllFeatureLayers().eachLayer(function (layer) {
						layer.hide();
						if (layer.getType() !== mapUtils.type.POINT) {
							if (layer.feature.properties.MapeamentoCamadaCategoriaBase === true) {
								_layerPerimetro = { id: layer.getFeatureId(), layerGroupId: layer.getLayerGroupId() };
								_hasPerimeter = true;
							} else {
								layer.bringToBack();
								_hasElements = true;
							}
						} else {
							_hasElements = true;
						}
					});					

					var layerPerimetro = getLayerPerimetro();
					if (layerPerimetro) {
						layerPerimetro.bringToBack();

						var visibilidadePerimetroLocal = localStorage.getItem('VisibilidadePerimetro-' + Route.routeOptions.id);

						if (visibilidadePerimetroLocal === null) {
							visibilidadePerimetroLocal = true;
						} else {
							visibilidadePerimetroLocal = (visibilidadePerimetroLocal === 'true');
						}
						VerificarVisibilidadePerimetro(visibilidadePerimetroLocal);
					}

					ko.computed(function () {
						var pVisivel = ko.unwrap(vmMapa.perimetroVisivel);
						var layerPerimetro = getLayerPerimetro();
						if (layerPerimetro) {
							if (pVisivel == true) {
								layerPerimetro.show();
								localStorage.setItem('VisibilidadePerimetro-' + Route.routeOptions.id, true);
							} else {
								layerPerimetro.hide();
								localStorage.setItem('VisibilidadePerimetro-' + Route.routeOptions.id, false);
							}
						}
					});

					toolbar.setMode(mapUtils.toolbarMode.DRAW);

					//if (_elementosCount === 0) {
					//	toolbar.setMode(mapUtils.toolbarMode.EDIT);
					//	initPerimeterHelp();						
					//} else {
					//	toolbar.setMode(mapUtils.toolbarMode.DRAW);
					//};

					dfd.resolve();
				}
			}).error(function () {
				dfd.fail();
			});
		};
		return dfd.promise();
	};

	var normalizeStylesProperties = function (obj) {
		// todo: 
	};

	var addWkt = function (wktString, properties, layerGroupId) {
		// Todas as informações são inseridas através de features e suas respectivas propriedades
		var wktInstance = new Wkt.Wkt();		

		try { // Catch any malformed WKT strings
			wktInstance.read(wktString);
		} catch (e1) {
			try {
				wktInstance.read(wktString.value.replace('\n', '').replace('\r', '').replace('\t', ''));
			} catch (e2) {
				if (e2.name === 'WKTError') {
					console.error('WKT inválido', layerGroupId, properties);
					return;
				} else {
					console.error('WKT inválido', layerGroupId, properties);
					return;
				}
			}
		}

		if (wktInstance.type !== 'geometrycollection') {
			var geoJson = {
				id: properties.IdMapeamentoElemento,
				layerGroupId: layerGroupId,
				geometry: wktInstance.toJson(),
				properties: properties,
				type: 'Feature'
			};
			mapService.featureAdd(geoJson);
		}
	};


	var bindMapa = function (idMapa) {

		MapaUpdate(idMapa).done(function () {
			// edição de mapa
			//mapaElemento.editMode();
			vmMapa.title('Carregando..');

			treeMapa.init(mapService, idMapa, {
				editarCamada: function (id) {
					modalCamadas.openEdit(id);
					treeMapa.update();
				},
				removerCamada: function (id) {
					CamadaRemove(id);
				},
				exportarKML: function (id) {
					$.post('Mapeamento/Arquivo/DownloadKML', { IdMapeamentoElemento: id }).done(function (r) {
						window.location = r.File;
					});
				},
				editarElemento: function (idElemento, idMapeamentoCamada) {
					mapService.layerEdit(idElemento, idMapeamentoCamada);
				},
				removerElemento: function (idMapeamentoElemento, idMapeamentoCamada) {
					ElementoRemove(idMapeamentoElemento, idMapeamentoCamada);
				}
			}).done(function () {
				if (treeMapa.getSelectedElementLayer() === null) {
					mapService.layersSelectZoom(); // caso usuário não tenha selecionado nenhuma camada ou elemento, centraliza mapa de acordo com todos os elementos
				}
				// recupera descrição do mapa
				mapaController.config({					
					getUrl: '/Mapeamento/Mapa/NewEditJson'
				});
				var request = mapaController.get({ id: Route.routeOptions.id });
				request.done(function (response) {					
					vmMapa.title(response.Descricao);
					vmMapa.IdReferencia = response.IdReferencia;
				});

			});

			window.treeMapa = treeMapa;

			modalCamadas.init({
				external: true,
				successCallback: function (result) {						
					// callback onsubmit				
					modalCamadas.close();
					modalCamadas.resetModel();
					treeMapa.update().done(function () {
						// Torna camada visível na árvore após inserir
						window.setTimeout(function () {
							treeMapa.checkElement('li-' + Route.routeOptions.id + '-' + result.IdMapeamentoCamada);
						}, 100);
					});
				},
				IdMapeamentoMapa: idMapa
			});

			configuracaoMapa.init(idMapa);

			vmMapa.exibeModalCamadas = function () {
				modalCamadas.resetModel();
				modalCamadas.open();
			};

			window.modalCamadas = modalCamadas;				

			ko.applyBindings(vmMapa, document.getElementById('aside-menu'));

			if (localStorage.getItem('MapaPrimeiroAcesso-' + Route.routeOptions.id) === null) {
				if (mapService.getFeaturesCount() === 0) {
					bootbox.alert({
						message: 'Inicie desenhando o perímetro caso este mapa tenha uma delimitação de área. <br /><strong>Exemplo:</strong> Perímetro de uma Propriedade Rural',
						size: 'small',
						title: 'Bem vindo (a)',
						className: 'modal-sm modal-bemvindo-mapa'
					});
				}
				localStorage.setItem('MapaPrimeiroAcesso-' + Route.routeOptions.id, true);
			};
		});
		
	};

	
	return {
		init: init,
		routeOptions: {
			title: 'Mapa',
			showTitle: true,
			showHeader: false,
			pos: 1
		}
	}
});