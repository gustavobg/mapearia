﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'summernote'], function (categoria, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            categoria.config({
                saveUrl: '/Questionario/Categoria/NewEdit',
                getUrl: '/Questionario/Categoria/NewEditJson'
            });

            var request = categoria.get({ id: id }),
                form = $('#form-QuestionarioCategoria-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Categoria/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Categoria de Questinário salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});