﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (categoriaControle, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            categoriaControle.config({
                saveUrl: '/Questionario/CategoriaControle/NewEdit',
                getUrl: '/Questionario/CategoriaControle/NewEditJson'
            });

            var request = categoriaControle.get({ id: id }),
                form = $('#form-QuestionarioCategoriaControle-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/CategoriaControle/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Controle salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});