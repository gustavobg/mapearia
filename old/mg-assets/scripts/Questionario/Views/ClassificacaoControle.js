﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (classificacaoControle, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            classificacaoControle.config({
                saveUrl: '/Questionario/ClassificacaoControle/NewEdit',
                getUrl: '/Questionario/ClassificacaoControle/NewEditJson'
            });

            var request = classificacaoControle.get({ id: id }),
                form = $('#form-QuestionarioClassificacaoControle-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/ClassificacaoControle/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Classificacao de Controle salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});