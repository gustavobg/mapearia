﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (contexto, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            contexto.config({
                saveUrl: '/Questionario/Contexto/NewEdit',
                getUrl: '/Questionario/Contexto/NewEditJson'
            });

            var request = contexto.get({ parentId: Route.routeOptions.parentId, id: id }),
                form = $('#form-QuestionarioContexto-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //EmpresaUsuariaObrigatoria
                ko.computed(function () {
                    if (vm.EmpresaUsuariaObrigatoria() == false) {
                        vm.IdsPessoaEmpresa(null);
                    }
                });

                //PermiteConsultaPessoa
                ko.computed(function () {
                    if (vm.PermiteConsultaPessoa() == false) {
                        vm.IdsPessoaPerfil(null);
                    }
                });

                //TipoAplicacaoQuestionarioPessoa
                ko.computed(function () {
                    if (vm.TipoAplicacaoQuestionarioPessoa() == 1) {
                        vm.IdsPessoaPerfilAplicador(null);
                        vm.IdsPessoa(null);
                    }
                    else if (vm.TipoAplicacaoQuestionarioPessoa() == 2) {
                        vm.IdsPessoa(null);
                    }
                    else if (vm.TipoAplicacaoQuestionarioPessoa() == 3) {
                        vm.IdsPessoaPerfilAplicador(null);
                    }
                });

                //TipoAplicacaoQuestionario

                ko.computed(function () {

                    if (vm.TipoAplicacaoQuestionario() == 1) {
                        vm.IdsCategoriaServico(null);
                        vm.IdsDocumentoTipo(null);
                        vm.IdsProducaoCultura(null);

                        vm.IdsPessoaPerfil(null);
                        vm.PermiteConsultaPessoa(false);
                        vm.PermiteAdicionarPessoa(false);
                    }
                    else if (vm.TipoAplicacaoQuestionario() == 2) {
                        vm.IdsAplicacaoTipoParametro(null);
                        vm.PermiteSubAplicacao(false);
                        vm.IdsDocumentoTipo(null);
                        vm.IdsProducaoCultura(null);

                        vm.IdsPessoaPerfil(null);
                        vm.PermiteConsultaPessoa(false);
                        vm.PermiteAdicionarPessoa(false);
                    }
                    else if (vm.TipoAplicacaoQuestionario() == 3) {
                        vm.IdsAplicacaoTipoParametro(null);
                        vm.PermiteSubAplicacao(false);
                        vm.IdsCategoriaServico(null);
                        vm.IdsProducaoCultura(null);

                        vm.IdsPessoaPerfil(null);
                        vm.PermiteConsultaPessoa(false);
                        vm.PermiteAdicionarPessoa(false);
                    }
                    else if (vm.TipoAplicacaoQuestionario() == 4) {
                        vm.IdsAplicacaoTipoParametro(null);
                        vm.PermiteSubAplicacao(false);
                        vm.IdsCategoriaServico(null);
                        vm.IdsDocumentoTipo(null);

                        vm.IdsPessoaPerfil(null);
                        vm.PermiteConsultaPessoa(false);
                        vm.PermiteAdicionarPessoa(false);
                    }
                    else if (vm.TipoAplicacaoQuestionario() == 5) {
                        vm.IdsCategoriaServico(null);
                        vm.IdsDocumentoTipo(null);
                        vm.IdsProducaoCultura(null);
                        vm.IdsAplicacaoTipoParametro(null);
                        vm.PermiteSubAplicacao(false);
                    }
                    else if (vm.TipoAplicacaoQuestionario() == "" || vm.TipoAplicacaoQuestionario() == null) {

                        vm.IdsCategoriaServico(null);
                        vm.IdsDocumentoTipo(null);
                        vm.IdsProducaoCultura(null);
                        vm.IdsAplicacaoTipoParametro(null);
                        vm.PermiteSubAplicacao(false);

                        vm.IdsPessoaPerfil(null);
                        vm.PermiteConsultaPessoa(false);
                        vm.PermiteAdicionarPessoa(false);

                    }

                    //PermiteSubAplicacao
                    if(vm.PermiteSubAplicacao() == false)
                    {
                        vm.IdsSubAplicacaoTipoParametro(null);
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Contexto/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Contexto do Questinário salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                console.log(form[0]);
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});