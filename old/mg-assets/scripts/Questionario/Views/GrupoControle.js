﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (grupoControle, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            grupoControle.config({
                saveUrl: '/Questionario/GrupoControle/NewEdit',
                getUrl: '/Questionario/GrupoControle/NewEditJson'
            });

            var request = grupoControle.get({ id: id }),
                form = $('#form-QuestionarioGrupoControle-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/GrupoControle/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Categoria de Controle salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});