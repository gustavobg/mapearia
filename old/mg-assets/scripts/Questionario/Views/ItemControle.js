﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'knockout-upload-simple'], function (item, ko, mgFeedbackBase, vmMappingToUpload, toastr) {

    var vm = {},
        bind = function (id) {
            item.config({
                saveUrl: '/Questionario/ItemControle/NewEdit',
                getUrl: '/Questionario/ItemControle/NewEditJson'
            });

            var request = item.get({ id: id }),
                form = $('#form-QuestionarioItemControle-NewEdit');

            request.done(function (response) {
            	vm = ko.mapping.fromJS(response);
            	vm.extendGaleria = function (vmItem) {

            		vmItem.ArquivoDigital.uploadParams = ko.observable();

            		ko.computed(function () {
            			var descricao = ko.unwrap(vmItem.Descricao);
            			vmItem.ArquivoDigital.uploadParams({ friendlyName: descricao });
            		});

            		vmItem.ArquivoDigital.uploadSuccess = function (response) {            			
            			if (response && response.hasOwnProperty('Sucesso') && response.Sucesso === true) {
            				vmItem.ArquivoDigital.CaminhoArquivo(response.FilePath);
            				vmItem.ArquivoDigital.Arquivo(response.Arquivo);
            				vmItem.ArquivoDigital.NomeExibicao(ko.unwrap(vmItem.Descricao));
							vmItem.ArquivoDigital.Enviado(false);
							vmItem.ArquivoDigital.IdArquivoDigital(null);            		
            			} else {
							toastr.error('Erro ao realizar o upload. Contate o administrador do sistema.')
            			}
            			Loader.hide();
            		};
            		vmItem.ArquivoDigital.uploadProgress = function (loaded, total) {
            			Loader.show();
            		};
            		vmItem.ArquivoDigital.uploadError = function (response) {
            			Loader.hide();
            		};            		
            	};

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/ItemControle/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Item de Controle salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});