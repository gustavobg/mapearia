﻿define([
	'jquery',
	'knockout',
	'map.service',
	'map.toolbar',
	'../Views/TreeMapaResposta',
	'leaflet.omnivore',
	'util',
	'toastr',
	'intro',
	'vanillaUniform',
	'panels',
	'component.areaPositionInfo',
	'component.colorSelector',
	'component.iconSelector',
	'knockout-upload-simple',
	'select2',
	'bootstrap-select'
], function ($, ko, mapService, toolbar, treeMapaResposta, omnivore, utils, toastr, introJs, vanillaUniform) {

    var mapInstance = null;
    var panelEdit = null;
    var initialized = false;
    var mapaResposta = {};
    var _elementoCount = 0;
    var _mapeamentoIdsRelacionados = [];
    var _isBound = false;
    var _vmFilter = {
        dadosFiltro: ko.observable()
    };

    var initHelp = function () {
        var polygonTool = document.getElementById('map').getElementsByClassName('leaflet-draw-toolbar-top')[0];
        polygonTool.setAttribute('data-intro', 'Antes de começar a desenhar os elementos do mapa, desenhe o perímetro da sua propriedade utilizando a ferramenta de área');
        polygonTool.setAttribute('data-step', '1');

        var tour = introJs()
        tour.setOption('tooltipPosition', 'auto');
        tour.setOption('positionPrecedence', ['left', 'right', 'bottom', 'top']);
        tour.setOption('overlayOpacity', 0.4);
        tour.setOption('hintButtonLabel', 'Entendi');
        tour.setOption('nextLabel', 'Próximo &rarr;');
        tour.setOption('prevLabel', '&larr; Anterior');
        tour.setOption('skipLabel', 'Pular');
        tour.setOption('showStepNumbers', false);
        tour.setOption('showBullets', false);
        tour.setOption('doneLabel', 'Entendi');

        tour.start();
    };

    var getFilterData = function (returnJson) {
        var gridContext = Route.getGridContextById('QuestionarioRespostaIndex');
        if (gridContext !== null) {
            var result = gridContext.api().filterData();
            if (returnJson) {
                result = ko.toJS(result);
                delete result.__ko_mapping__;
                return result;
            } else {
                return result;
            }
        } else {
            return {};
        }
    }

    var formatDistance = function (a) {
        // m					
        var m = a;
        if (m > 1000) {
            return (m / 1000).toFixed(2) + ' km'
        } else {
            return m.toFixed(2) + ' m'
        }
    };

    var formatArea = function (a) {
        var m2 = a;
        if (m2 > 10000) {
            return (m2 / 10000).toFixed(2) + ' ha'
        } else {
            return (m2).toFixed(2) + ' km';
        }
    };

    var layerMouseOver = function (e) {
        var layer = e.target;
        var type = layer.feature.geometry.type;

        console.log('IdElementoReferencia: ' + layer.feature.properties.IdElementoReferencia + ', idElemento: ' + layer.feature.id);
    };

    var seed = 1;
    var eachLayerFeature = function (layer, feature) {
        layer = bindLayerPopup(layer);
        var type = layer.getType();
        //if (feature.properties.IdMapeamentoElemento === null) {
        //	feature.id = new Date().getTime() + seed;
        //	seed++;
        //}

        // mapeia elementos relacionados		
        if (feature.properties.IdElementoReferencia !== null) {
            // elemento é relacionado a uma camada
            if (Array.isArray(_mapeamentoIdsRelacionados['IdMapeamentoElemento-' + feature.properties.IdElementoReferencia])) {
                // se já existe o índice do elemento criado, dá um push
                _mapeamentoIdsRelacionados['IdMapeamentoElemento-' + feature.properties.IdElementoReferencia].push(feature.id);
            } else {
                _mapeamentoIdsRelacionados['IdMapeamentoElemento-' + feature.properties.IdElementoReferencia] = feature.id;
            }
        } else {
            // elemento é uma camada
            if (!Array.isArray(_mapeamentoIdsRelacionados['IdMapeamentoElemento-' + feature.id])) {
                _mapeamentoIdsRelacionados['IdMapeamentoElemento-' + feature.id] = [];
            }
        }

        layer.addEventListener('mouseover', layerMouseOver);

        return layer;
    };

    var getLayerGroupIdsRelacionados = function (id) {
        var ids = getMapeamentoIdsRelacionados(id);
        var layers = [];
        var layer = null;
        if (ids && ids.length > 0) {
            ids.forEach(function (id) {
                layer = mapService.getLayerById(id);
                if (layer) {
                    layers.push(layer);
                }
            })
        }
        return new L.LayerGroup(layers);
    }

    var showLayersRelacionadas = function (id) {
        var layerGroup = getLayerGroupIdsRelacionados(id);
        layerGroup.show();
        // current layer
        mapService.layerShow(id);
    };

    var hideLayersRelacionadas = function (id) {
        var layerGroup = getLayerGroupIdsRelacionados(id);
        layerGroup.hide();
        // current layer
        mapService.layerHide(id);
    };


    var getMapeamentoIdsRelacionados = function (id) {
        return _mapeamentoIdsRelacionados['IdMapeamentoElemento-' + id];
    }

    var getMapeamentoIdsRelacionadosArray = function () {
        return _mapeamentoIdsRelacionados;
    }

    var bindLayerPopup = function (layer) {
        return layer.bindPopup(document.getElementById('template-popup').innerHTML, mapService.getOptions().popupOptions)
            .addEventListener('popupopen', editPopupOpen)
            .addEventListener('popupclose', editPopupClose);
    };

    var editPopupClose = function (e) {
        document.getElementById('popup-info').id = 'popup-info-closing';
        var layer = e.target;
        layer.options.selected = false;
        layer.fire('mouseout');
    };

    var editPopupOpen = function (e) {
        var layer = e.target;
        ko.applyBindings({
            layer: layer
        }, document.getElementById('popup-info'));
    };

    var unbindLayerPopup = function (layer) {
        layer.unbindPopup();
    };


    var showDetalhesFiltro = function () {
        var filterData = getFilterData();
        if (Object.keys(filterData).length > 0) {
            if (_isBound === false) {
                // altera ids para não conflitar com página de listagem
                _vmFilter.dadosFiltro(getFilterData());
                ko.applyBindings(_vmFilter, document.getElementById('modal-filtro-template'));
                _isBound = true;
            }
            document.getElementById('alert-sem-filtro').style.display = 'none';
            document.getElementById('modal-filtro-content').style.display = 'block';
        } else {
            document.getElementById('alert-sem-filtro').style.display = 'block';
            document.getElementById('modal-filtro-content').style.display = 'none';
        }
        $('#modal-filtro-detalhes').modal();
    };

    var init = function (idMapa) {
        _isBound = false;
        _mapeamentoIdsRelacionados = [];

        mapUtils.defaultStyles.POINT = {
            color: '#9C27B0',
            shadowUrl: 'marker-shadow.png',
            name: '',
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            tooltipAnchor: [16, -28],
            shadowSize: [41, 41],
            className: 'leaflet-custom-icon'
        };

        mapService.init({
            attributionControl: false,
            minZoom: 4,
            maxZoom: 18,
            useSingleLayerGroup: true,
            checkPolygonIntersections: false, // desativa crop do cliente
            draggablePolygons: false,
            useCustomPopup: true,
            eachLayerFeature: eachLayerFeature,
            editable: false
            //,setAutoFeatureIds: false
        });

        mapInstance = mapService.getInstance();

        var measureFeatureGroup = new L.featureGroup();
        measureFeatureGroup.addTo(mapInstance);

        L.drawLocal = $.extend({}, L.drawLocal, {
            draw: {
                toolbar: {
                    // ex: actions.undo  or actions.cancel
                    actions: {
                        title: 'Cancelar medição',
                        text: 'Cancelar'
                    },
                    finish: {
                        title: 'Concluir medição',
                        text: 'Concluir'
                    },
                    undo: {
                        title: 'Remover último ponto desenhado',
                        text: 'Remover último ponto'
                    },
                    buttons: {
                        polyline: 'Medir perímetro',
                        polygon: 'Medir uma área'
                    }
                },
                handlers: {
                    circle: {
                        tooltip: {
                            start: ''
                        },
                        radius: 'Raio'
                    },
                    marker: {
                        tooltip: {
                            start: 'Clique em algum lugar do mapa para inserir o marcador.'
                        }
                    },
                    polygon: {
                        tooltip: {
                            start: '',
                            cont: '',
                            end: ''
                        }
                    },
                    polyline: {
                        error: '<strong>Erro:</strong> as linhas não podem se sobrepor!',
                        tooltip: {
                            start: '',
                            cont: '',
                            end: ''
                        }
                    },
                    rectangle: {
                        tooltip: {
                            start: 'Clique e arraste para desenhar um retângulo.'
                        }
                    },
                    simpleshape: {
                        tooltip: {
                            end: 'Solte o clique para finalizar o desenho.'
                        }
                    }
                }
            }
        });

        toolbar.init(mapService, mapService.getFeatureGroup());
        toolbar.setMode(mapUtils.toolbarMode.DRAW);

        toolbar.getDrawControl().setDrawingOptions({
        	polygon: {
        		shapeOptions: $.extend({}, mapUtils.defaultStyles.POLYGON, { fillColor: '#0288D1', color: '#0288D1', className: 'path-measure' })
        	},
        	polyline: {
        		shapeOptions: $.extend({}, mapUtils.defaultStyles.POLYLINE, { fillColor: '#0288D1', color: '#0288D1', className: 'path-measure' }),
        		feet: false
        	}
        });

        // esconde ponto
        $('.leaflet-draw-draw-marker').hide();

        mapInstance.off('draw:created', function (e) { });
        mapInstance.on('draw:created', function (e) {
            measureFeatureGroup.clearLayers();
            var newLayer = e.layer;
            //newLayer.bindPopup(document.getElementById('template-measure-popup').innerHTML, {})
            newLayer.on('click', function (e) {
                var layer = e.target;
                layer.remove();
            })
            newLayer.addTo(measureFeatureGroup);
            if (newLayer.getType() !== mapUtils.type.POINT) {
            	newLayer.showMeasurements({
            		showOnHover: false,
            		formatDistance: formatDistance,
            		formatArea: formatArea,
            		showDistances: true
            	});
            }
        });
      
        if (!initialized) {
            utils.insertCSS('/mg-assets/styles/mapeamento/mapeamento.css');
            utils.insertCSS('/mg-assets/styles/mapeamento/bootstrap-colorpicker.css');
            initialized = true;
        }

        mapInstance
			.on('mapearia.featureAdd', function () { })
			.on('mapearia.featureUpdate', function () { });

        mapaResposta.MapaUpdate = MapaUpdate;
        mapaResposta.getLayerGroupIdsRelacionados = getLayerGroupIdsRelacionados;
        mapaResposta.showLayersRelacionadas = showLayersRelacionadas;
        mapaResposta.hideLayersRelacionadas = hideLayersRelacionadas;
        mapaResposta.getFilterData = getFilterData;
        mapaResposta.getMapeamentoIdsRelacionados = getMapeamentoIdsRelacionados;
        mapaResposta.getMapeamentoIdsRelacionadosArray = getMapeamentoIdsRelacionadosArray;
        mapaResposta.getElementosCount = function () {
            return _elementoCount;
        };
        mapaResposta.togglePanel = function () {
        	var container = $('#route-map');
        	if (container.hasClass('aside-closed')) {
        		container.removeClass('aside-closed');
        	} else {
        		container.addClass('aside-closed');
        	}
        	window.setTimeout(function () {
        		mapService.getInstance().invalidateSize();
        	}, 200);
        };
        mapaResposta.showDetalhesFiltro = showDetalhesFiltro;
        mapaResposta.IdMapeamentoCamadaCategoriaFiltro = ko.observable(valorPadraoFiltro());
    	mapaResposta.elementosCount = 0;

    	window.mapaResposta = mapaResposta;

    	bindMapa(idMapa);

    	ko.computed(function () {
    		var valorFiltro = ko.unwrap(mapaResposta.IdMapeamentoCamadaCategoriaFiltro);
    		var isInitial = ko.computedContext.isInitial();
    		if (!isInitial) {
    			UpdateMapAndTree();
    			localStorage.setItem('IdMapeamentoCamadaCategoriaFiltro-' + Route.routeOptions.area, valorFiltro);
    		}
    	});
    };

    var UpdateMapAndTree = function () {
    	MapaUpdate().done(function () {
    		treeMapaResposta.update().done(function () {
    			window.setTimeout(function () {
    				treeMapaResposta.deselectAll();
    				mapService.layersSelectZoom();
    			}, 100);
    		});
    	});
    }

    var valorPadraoFiltro = function () {
    	var valorFiltro = null;
    	if (window.localStorage) {
    		valorFiltro = localStorage.getItem('IdMapeamentoCamadaCategoriaFiltro-' + Route.routeOptions.area);
    	}
    	if (valorFiltro === null) {
    		return '';
    	} else {
    		return valorFiltro
    	}
    };

    var bindMapa = function (idMapa) {
        treeMapaResposta.init(mapService, idMapa, {}).done(function () {
            if (treeMapaResposta.getSelectedElementLayer() === null) {
                mapService.layersSelectZoom(); // caso usuário não tenha selecionado nenhuma camada ou elemento, centraliza mapa de acordo com todos os elementos
            }
            // no mapa de medidor, os elementos são carregados após renderização da árvore
            var tree = treeMapaResposta.getTreeElement();
            var arrIdsElementosMapa = [];
            tree.on('ready.jstree', function (e, node) {
                var arrData = tree.jstree(true).get_json(null, { flat: true, no_data: false });
                arrData.forEach(function (elemento) {
                    if (elemento.data.id) {
                        arrIdsElementosMapa.push(elemento.data.id);
                    }
                });
                MapaUpdate(arrIdsElementosMapa).done(function () {
                    mapService.layersSelectZoom();
                });
            });
        });

        ko.applyBindings(mapaResposta, document.getElementById('aside-menu'));
    };

    var MapaUpdate = function () {
        var dfd = new $.Deferred();
        // recupera informações do servidor
        //url: '/Mapeamento/ElementoGeo/Teste?idMapeamentoElementoIn=' + arrIdsElementosMapa.join(','),

        var filtroIndex = getFilterData(true);
        var data = $.extend({}, { IdMapeamentoCamadaCategoriaIn: ko.unwrap(mapaResposta.IdMapeamentoCamadaCategoriaFiltro), IdProducaoUnidadeIn: '', Page: { OrderByColumn: "", OrderByDirection: "" } }, filtroIndex);

        Loader.show('Carregando informações do mapa');

        $.ajax({
            url: '/Mapeamento/ElementoGeo/RetornaElementoGeoParaMapaQuestionarioResposta',
            data: JSON.stringify(data),
            type: 'POST'
        }).done(function (response) {
        	if (response.hasOwnProperty('Data')) {

        		mapService.getFeatureGroup().clearLayers();

                var data = response.Data;
                var dataLength = data.length;
                var estilo = null;
                var i = 0;
                for (i; i < dataLength;) {
                    estilo = data[i].Estilo && data[i].Estilo.length > 0 ? JSON.parse(data[i].Estilo) : {};
                    addWkt(data[i].wkt, {
                        MapeamentoElementoObservacao: data[i].MapeamentoElementoObservacao,
                        MapeamentoElementoDescricao: data[i].MapeamentoElementoDescricao,
                        IdMapeamentoElementoGeo: data[i].IdMapeamentoElementoGeo,
                        IdMapeamentoElemento: data[i].IdMapeamentoElemento,
                        IdMapeamentoCamada: data[i].IdMapeamentoCamada,
                        MapeamentoCamadaCategoriaBase: data[i].MapeamentoCamadaCategoriaBase,
                        styles: estilo,
                        text: data[i].MapeamentoElementoDescricao,
                        IdElementoReferencia: data[i].idElementoReferencia
                    }, data[i].IdMapeamentoCamada);

                    i = i + 1;
                }
                dfd.resolve();
                Loader.hide();
            }
        }).error(function () {
            dfd.fail();
            Loader.hide();
        });

        return dfd.promise();
    };



    var normalizeStylesProperties = function (obj) {
        // todo: 
    };

    var addWkt = function (wktString, properties, layerGroupId) {
        // Todas as informações são inseridas através de features e suas respectivas propriedades
        var wktInstance = new Wkt.Wkt();

        try { // Catch any malformed WKT strings
            wktInstance.read(wktString);
        } catch (e1) {
            try {
                wktInstance.read(wktString.value.replace('\n', '').replace('\r', '').replace('\t', ''));
            } catch (e2) {
                if (e2.name === 'WKTError') {
                    console.error('WKT inválido', layerGroupId, properties);
                    return;
                } else {
                    console.error('WKT inválido', layerGroupId, properties);
                    return;
                }
            }
        }

        if (wktInstance.type !== 'geometrycollection') {
            var geoJson = {
                id: properties.IdMapeamentoElemento,
                layerGroupId: layerGroupId,
                geometry: wktInstance.toJson(),
                properties: properties,
                type: 'Feature'
            };
            mapService.featureAdd(geoJson);
        }
    };

    return {
        init: init,
        routeOptions: {
            title: 'Mapa',
            showTitle: true,
            showHeader: false,
            pos: 1
        }
    }
});