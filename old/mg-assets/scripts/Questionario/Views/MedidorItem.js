﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'mask-coordenada', 'ko-validate-rules', 'ko-validate'], function (medidor, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            medidor.config({
                saveUrl: '/Questionario/MedidorItem/NewEdit',
                getUrl: '/Questionario/MedidorItem/NewEditJson'
            });

            var request = medidor.get({ id: id }),
                form = $('#form-QuestionarioMedidorItem-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //Reset do campo Unidade de Produção ao alterar o valor do Local de Produção
                vm.IdProducaoLocal.subscribe(function (newValue) {
                    vm.IdProducaoUnidade(null);
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/MedidorItem/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Medidor de Item salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});