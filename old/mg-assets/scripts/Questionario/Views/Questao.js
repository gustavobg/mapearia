﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'knockout-upload-mapping', 'knockout-upload','button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'knockout-postbox'], function (questao, ko, mgFeedbackBase, toastr, vmMappingToUpload) {

    var vm = {},
        bind = function (id) {
            questao.config({
                saveUrl: '/Questionario/Questao/NewEdit',
                getUrl: '/Questionario/Questao/NewEditJson'
            });

            var request = questao.get({ parentId: Route.routeOptions.parentId, id: id }),
                form = $('#form-QuestionarioQuestao-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.dadosComplTopico = ko.observable({ 'Ativo': true, 'IdQuestionarioVersao': vm.IdQuestionarioVersao() });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    delete vm['ArquivosDigitaisToUploader']; // Importante remover propriedades adicionais da VM, para Razor validar a VM
                    delete vm['ArquivosDigitaisFromUploaderJS'];

                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Questao/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Questão salva com sucesso.');
                        }
                    });
                };

                vm = new vmMappingToUpload(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});