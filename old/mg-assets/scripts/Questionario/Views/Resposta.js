﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'mask-coordenada', 'ko-validate-rules', 'ko-validate'], function (resposta, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            resposta.config({
                saveUrl: '/Questionario/Resposta/NewEdit',
                getUrl: '/Questionario/Resposta/NewEditJson'
            });

            var request = resposta.get({ id: id }),
                form = $('#form-QuestionarioResposta-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                //TipoAplicacaoQuestionario  == Identifica a referência do Questioná sendo, 
                //1 - Aplicação
                //2 - Categoria de Serviço
                //3 - Tipo de Documento
                //4 - Cultura
                //5 - Pessoa
                vm.TipoAplicacaoQuestionario = ko.observable(0);
                vm.dadosCompleAplicacao = ko.observable(null);
                vm.dadosCompleSubAplicacao = ko.observable(null);
                vm.dadosCompleCategoriaServico = ko.observable(null);
                vm.dadosCompleTipoDocumento = ko.observable(null);
                vm.dadosCompleCultura = ko.observable(null);
                vm.dadosComplePessoa = ko.observable(null);
                vm.dadosCompleEmpresaUsuaria = ko.observable(null);
                vm.dadosComplePessoaAplicador = ko.observable(null);
                vm.dadosCompleMedidor = ko.observable(null);
                vm.aplicacaoVM = ko.observable(null);
                vm.lblAplicacaoTipo = ko.observable('');
                vm.lblSubAplicacaoTipo = ko.observable('');
                vm.lblQuestionarioUnidadeAmostral = ko.observable('Código');
                vm.QuestionarioCategoriaMedidorDescricao = ko.observable('');
                vm.MunicipioEstadoAplicacao = ko.observable('');

                vm.QuestionarioTipoMedidorDescricao = ko.observable('');
                vm.IdsCategoriaMedidor = ko.observable('');
                vm.medidorItemVM = ko.observable();

                //vm.QuestionarioConfiguracao = ko.observable();
                //vm.TopicoQuestao = ko.observableArray([])

                //Versão do Questionário.
                ko.computed(function () {
                    if (vm.IdQuestionarioVersao() != null ) {

                        //::TODO::
                        $.ajax({
                            url: '/Questionario/Versao/ListIdQuestionarioVersaoCompleto',
                            type: 'post',
                            dataType: 'json',
                            data: 'id=' + vm.IdQuestionarioVersao(),
                            async: false,
                            success: function (data) {
                                if (data.response != null) {

                                    if (data.response != null) {
                                        var contexto = data.response.Contexto;

                                        vm.lblAplicacaoTipo(contexto.AplicacaoTipoDescricao);
                                        vm.lblSubAplicacaoTipo(contexto.SubAplicacaoTipoDescricao);
                                        //

                                        vm.lblQuestionarioUnidadeAmostral(contexto.QuestionarioUnidadeAmostralDescricao.length > 0 ? contexto.QuestionarioUnidadeAmostralDescricao : "Código");

                                        vm.TipoAplicacaoQuestionario(contexto.TipoAplicacaoQuestionario);
                                        vm.ExibeEmpresaUsuaria(contexto.EmpresaUsuariaObrigatoria);
                                        vm.ExibeAtividadeEconomicaPeriodo(contexto.InformaPeriodoAtividade);
                                        vm.InformaHora(contexto.InformaHora);
                                        vm.InformaPessoaAplicacao(contexto.InformaPessoa);
                                        vm.PermiteDataAplicacaoDiferenteDoLancamento(contexto.PermiteDataDiferenteLancamento);
                                        vm.ExibeSubAplicacao(contexto.PermiteSubAplicacao);

                                        //Preenchimento Dados Complementares.
                                        vm.dadosCompleAplicacao({ 'IdAplicacaoTipoIn': contexto.IdsAplicacaoTipoParametro, 'Ativo': true });
                                        vm.dadosCompleCategoriaServico({ 'IdCategoriaServicoIn': contexto.IdsCategoriaServico, 'Ativo': true });
                                        vm.dadosCompleTipoDocumento({ 'IdDocumentoTipoIn': contexto.IdsDocumentoTipo, 'Ativo': true });
                                        vm.dadosCompleCultura({ 'IdProducaoCulturaIn': contexto.IdsProducaoCultura, 'Ativo': true });
                                        vm.dadosComplePessoa({ 'IdPessoaPerfilIn': contexto.IdsPessoaPerfil, 'Ativo': true, 'IdPessoaNotIn': '3' });
                                        vm.dadosCompleEmpresaUsuaria({ 'IdPessoaNotIn': contexto.IdsPessoaEmpresa, 'Ativo': true });
                                        vm.dadosComplePessoaAplicador({ 'IdPessoaIn': contexto.IdsPessoa, 'IdPessoaPerfilNotIn': contexto.IdsPessoaPerfilAplicador, 'Ativo': true, 'IdPessoaNotIn': '4' });
                                        vm.dadosCompleSubAplicacao({ 'IdAplicacaoTipoIn': contexto.IdsAplicacaoTipoParametro });
                                        vm.dadosCompleMedidor({ 'IdQuestionarioCategoriaMedidorIn': contexto.IdsCategoriaMedidor });
                                        vm.IdsCategoriaMedidor(contexto.IdsCategoriaMedidor == null ? '' : contexto.IdsCategoriaMedidor);

                                    }
                                }
                            }
                        });
                    }

                    //Apenas quando for New/ Em modo de edição Topico/Questão 
                    if (vm.IdQuestionarioVersao() != null && vm.IdQuestionarioResposta() == null) {

                        //Topico/Questão/Opção
                        $.ajax({
                            url: '/Questionario/Questao/ListarPorIdQuestionarioVersaoCompleto',
                            type: 'get',
                            dataType: 'json',
                            data: 'idQuestionarioVersao=' + vm.IdQuestionarioVersao(),
                            async: false,
                            success: function (data) {
                                if (data.response != null) {

                                    var xpto = ko.mapping.fromJS(data.response);
                                    vm.TopicoQuestao(data.response);
                                }
                            }
                        });
                    }
                });

                //Medidor Item
                ko.computed(function () {
                    if (vm.IdQuestionarioMedidorItem() != null && vm.medidorItemVM() != null &&
                       (vm.TipoAplicacaoQuestionario() != null && vm.TipoAplicacaoQuestionario() == 1) &&
                        (vm.IdAplicacao() == null) ) {

                        vm.IdAplicacao(vm.medidorItemVM().IdAplicacao)
                        vm.IdSubAplicacao(vm.medidorItemVM().IdSubAplicacao);
                        vm.MunicipioEstadoAplicacao(vm.medidorItemVM().MunicipioEstado);
                        
                    }
                    else
                    {
                        //vm.TipoAplicacaoQuestionario(null);
                    }

                    if(vm.IdQuestionarioMedidorItem() != null && vm.medidorItemVM() != null)
                    {
                        vm.QuestionarioTipoMedidorDescricao("Medidor (" + vm.medidorItemVM().QuestionarioTipoMedidorDescricao + ")");
                        vm.QuestionarioCategoriaMedidorDescricao(vm.medidorItemVM().QuestionarioCategoriaMedidorDescricao);
                    }
                    else
                    {
                        vm.QuestionarioTipoMedidorDescricao('Medidor');
                    }
                });

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Resposta/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Questionario preenchido com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});