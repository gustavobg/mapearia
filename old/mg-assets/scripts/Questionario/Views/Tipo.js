﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (tipo, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            tipo.config({
                saveUrl: '/Questionario/Tipo/NewEdit',
                getUrl: '/Questionario/Tipo/NewEditJson'
            });

            var request = tipo.get({ id: id }),
                form = $('#form-QuestionarioTipo-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Tipo/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Questinário salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});