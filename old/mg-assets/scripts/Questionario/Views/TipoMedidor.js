﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (tipoMedidor, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            tipoMedidor.config({
                saveUrl: '/Questionario/TipoMedidor/NewEdit',
                getUrl: '/Questionario/TipoMedidor/NewEditJson'
            });

            var request = tipoMedidor.get({ id: id }),
                form = $('#form-QuestionarioTipoMedidor-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/TipoMedidor/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Tipo de Medidor salvo com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});