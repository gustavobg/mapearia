﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'knockout-upload-mapping', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'datetimepicker', 'daterangepicker', 'knockout-upload', 'knockout-postbox'], function (topico, ko, mgFeedbackBase, toastr, vmMappingToUpload) {

    var vm = {},
        bind = function (id) {
            topico.config({
                saveUrl: '/Questionario/Topico/NewEdit',
                getUrl: '/Questionario/Topico/NewEditJson'
            });

            var request = topico.get({ parentId: Route.routeOptions.parentId, id: id }),
                form = $('#form-QuestionarioVersaoTopico-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    delete vm['ArquivosDigitaisToUploader']; // Importante remover propriedades adicionais da VM, para Razor validar a VM
                    delete vm['ArquivosDigitaisFromUploaderJS'];

                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Topico/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Tópico salvo com sucesso.');
                        }
                    });
                };

                vm = new vmMappingToUpload(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});