﻿define(['jquery', 'toastr', 'tree-util', 'jquery-ui/sortable', 'jstree', 'jstree.types', 'jstree.search', 'jstree.checkbox', 'jstree.state', 'jstree.dnd', 'jquery-qtip'], function ($, toastr, treeUtil) {

	var treeElement = null;
	var _mapService = null;

	var requestTree = function () {		
		var filtroIndex = mapaItemMedidor.getFilterData(true);

		var data = $.extend({}, { IdMapeamentoCamadaCategoriaIn: ko.unwrap(mapaItemMedidor.IdMapeamentoCamadaCategoriaFiltro), IdProducaoUnidadeIn: '', Page: { OrderByColumn: "", OrderByDirection: "asc" } }, filtroIndex);
		return $.ajax({
		    url: '/Mapeamento/Mapa/QuestionarioMedidor_RetornaArvorePorProducaoUnidade',
			type: 'POST',
			dataType: 'JSON',
			data: JSON.stringify(data)
		});
	}

	var createTree = function (data, id) {
		treeElement = $("#treeMapaMedidor").jstree({
			'core': {
				'multiple': true,
				'check_callback': function (operation, node, node_parent, node_position, more) {
					// operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
					// in case of 'rename_node' node_position is filled with the new node name
					if (operation === "move_node") {
						return true;
					}
					if (operation === "copy_node") {
						return true;
					}					
					return true;
				},
				'dblclick_toggle': false,
				'data': data,
				'strings': {
					'Loading ...': 'Carregando árvore...'
				}
			},
			'checkbox': {
				three_state: true,			
				whole_node: false,
				tie_selection: false
			},			
			'types': {
				"#": {
					"valid_children": ['folder']
				},
				'folder': {
					"valid_children": [],
					"icon": "fa fa-folder"
				},
				'default': {
					"icon": "fa fa-file-o",
					"valid_children": []
				}
			},
			'plugins': ['checkbox', 'search', 'dnd', 'types']
		}).on('check_node.jstree', function (e, n) {
			var arrIdsCheck = [];
			if (n.node.type === 'folder') {
				_mapService.layerShow(n.node.data.id);
				treeElement.jstree('open_node', n.node);
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false });
				children.forEach(function (d) {					
					mapaItemMedidor.showLayersRelacionadas(d.data.id);
				});
			} else {
				mapaItemMedidor.showLayersRelacionadas(n.node.data.id);				
			}
		}).on('uncheck_node.jstree', function (e, n) {
			if (n.node.type === 'folder') {
				treeElement.jstree('close_node', n.node);
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false });
				children.forEach(function (d) {					
					mapaItemMedidor.hideLayersRelacionadas(d.data.id);
				});
			} else {				
				mapaItemMedidor.hideLayersRelacionadas(n.node.data.id);
			}
		}).on('select_node.jstree', function (e, n) {
			if (n.node.type != 'folder') {
				var id = n.node.data.id;
				console.log(id);
				_mapService.layerSelectZoom(id);
			} else {
				var children = treeElement.jstree(true).get_json(n.node, { flat: true, no_data: false }),
					arrIds = [];
				children.forEach(function (d) {
					arrIds.push(d.data.id);
				});
				console.log(arrIds);
				_mapService.layersSelectZoom(arrIds);
			}
		}).on('load_node.jstree', function (node, status) {
			_setupEvents();			
			if (node.type == 'default') {
				console.log(node);
			}
		}).on('delete_node.jstree', function (e, node) {
		}).on('ready.jstree', function (e, node) {
			_setupEvents();
		}).on('refresh.jstree', function () {			
			_setupEvents();
		});
	};

	var getRelatedFeatures = function () {

	};
	
	var id = null;

	var options = {};

	var init = function (mapService, id, o) {
		_mapService = mapService;
		id = id;
		$.extend(true, options, o);
		// carrega árvore pela primeira vez
		return requestTree(id).done(function (response) {
			if (response.length > 0) {			
				createTree(response, id);
			} else {
				createTree([], id);
			}
		});
	};



	var _setupEvents = function () {	
	};

	var updateTree = function () {
		var dfd = $.Deferred();
		var idTree = Route.routeOptions.id;
		requestTree(idTree).done(function (response) {
			if (response.length > 0) {
				treeElement.on('redraw.jstree', function () {
					dfd.resolve(true);
				});				
				//treeElement.jstree(true).settings.core.data = JSON.parse(response);
				treeElement.jstree(true).settings.core.data = response;
				treeElement.jstree(true).refresh(true, true);
			} else {
				treeElement.on('redraw.jstree', function () {
					dfd.resolve(false);
				});
				treeElement.jstree(true).settings.core.data = [];
				treeElement.jstree(true).refresh(true, false);
			}
		});
		return dfd.promise();
	};

	return {
		init: init,
		update: updateTree,
		selectElement: function (idElement) {
			treeElement.jstree().select_node(idElement);
		},
		getChildrenElementCount: function (nodeId) {
			return treeElement.jstree().get_json(nodeId, { flat: true, no_data: false });
		},
		deselectAll: function () {
			debugger;
			treeElement.jstree().deselect_all();
		},
		getSelectedElementLayer: function () {
			var selected = treeElement.jstree().get_selected();
			if (selected.length > 0) {
				return treeElement.jstree().get_node(selected[0]);
			} else {
				return null;
			}
		},
		checkElement: function (idElement) {
			treeElement.jstree().check_node(idElement);
		},		
		getTreeElement: function () {
			return treeElement;
		}
	}
});