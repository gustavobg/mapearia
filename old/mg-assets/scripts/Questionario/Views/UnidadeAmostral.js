﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate'], function (unidadeAmostral, ko, mgFeedbackBase, toastr) {

    var vm = {},
        bind = function (id) {
            unidadeAmostral.config({
                saveUrl: '/Questionario/UnidadeAmostral/NewEdit',
                getUrl: '/Questionario/UnidadeAmostral/NewEditJson'
            });

            var request = unidadeAmostral.get({ id: id }),
                form = $('#form-QuestionarioUnidadeAmostral-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };
                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/UnidadeAmostral/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'UnidadeAmostral de Questinário salva com sucesso.');
                        }
                    });
                };

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});