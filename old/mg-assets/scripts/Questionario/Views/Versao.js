﻿define(['crud-controller', 'knockout', 'feedback', 'toastr', 'knockout-upload-mapping', 'button', 'jquery-flipper', 'jquery-inputmask', 'select2', 'antiforgery', 'ko-validate-rules', 'ko-validate', 'datetimepicker', 'daterangepicker', 'knockout-upload', 'knockout-postbox', 'vanillaUniform'], function (versao, ko, mgFeedbackBase, toastr, vmMappingToUpload) {

    var vm = {},
        bind = function (id) {
            versao.config({
                saveUrl: '/Questionario/Versao/NewEdit',
                getUrl: '/Questionario/Versao/NewEditJson'
            });

            var request = versao.get({ parentId: Route.routeOptions.parentId, id: id }),
                form = $('#form-QuestionarioVersao-NewEdit');

            function SelecionaItemControle() {
                var result = "";
                var i = 0;
                var total = vm.lstTopico().length;
                ko.utils.arrayForEach(vm.lstTopico(), function (item) {
                    i += 1;
                    if (ko.unwrap(item.RepresentacaoTipo) == 3) {
                        if (total > i) {
                            result += ko.unwrap(item.IdQuestionarioItemControle) + ", ";
                        }
                        else {
                            result += ko.unwrap(item.IdQuestionarioItemControle) + "";
                        }
                    }
                });

                return result;
            };

            function SelecionaCategoriaMedidor() {
                var result = "";
                var i = 0;
                var total = vm.lstTopico().length;
                ko.utils.arrayForEach(vm.lstTopico(), function (item) {
                    i += 1;
                    if (ko.unwrap(item.RepresentacaoTipo) == 2) {
                        if (total > i) {
                            result += ko.unwrap(item.IdQuestionarioCategoriaMedidor) + ", ";
                        }
                        else {
                            result += ko.unwrap(item.IdQuestionarioCategoriaMedidor) + "";
                        }
                    }

                });

                return result;
            };
            function SelecionaUnidadeAmostral() {
                var result = "";
                var i = 0;
                var total = vm.lstUnidadeAmostral().length;
                ko.utils.arrayForEach(vm.lstUnidadeAmostral(), function (item) {
                    i += 1;
                    if (total > i) {
                        result += ko.unwrap(item.IdQuestionarioUnidadeAmostral) + ", ";
                    }
                    else {
                        result += ko.unwrap(item.IdQuestionarioUnidadeAmostral) + "";
                    }
                });
                return result;
            };

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                var vmUnidadeAmostralExtend = function (ext) {

                    ext.dadosComplUnidadeAmostral = ko.observable();
                    ext.dadosComplUnidadeAmostral({ 'Ativo': true, 'IdQuestionarioUnidadeAmostralNotIn': SelecionaUnidadeAmostral() });
                }
                vm.vmUnidadeAmostralExtend = vmUnidadeAmostralExtend;

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    delete vm['ArquivosDigitaisToUploader']; // Importante remover propriedades adicionais da VM, para Razor validar a VM
                    delete vm['ArquivosDigitaisFromUploaderJS'];

                    $.ajaxJsonAntiforgery(form, {
                        data: ko.toJSON(vm),
                        url: '/Questionario/Versao/NewEdit/',
                        success: function (result) {
                            mgFeedbackBase.feedbackCrudRoute(result, 'Versão do Questinário salva com sucesso.');
                        }
                    });
                };

                vm = new vmMappingToUpload(vm);

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }
});