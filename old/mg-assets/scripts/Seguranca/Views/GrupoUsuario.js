﻿define(['crud-controller', 'knockout', 'feedback',  'jstree.util', 'toastr', 'bootbox',  'select2', 'jquery-flipper', 'ko-validate', 'jstree',  'jstree.dnd', 'jstree.types', 'jstree.search', 'jstree.checkbox'], function (grupoUsuario, ko, mgFeedbackBase, treeUtil, toastr, bootbox) {

    var vm = {},
        bind = function (id) {
            grupoUsuario.config({
                saveUrl: '/Seguranca/GrupoUsuario/NewEdit',
                getUrl: '/Seguranca/GrupoUsuario/NewEditJson',
            });
            var request = grupoUsuario.get({ id: id }),
                form = $('#form-SegurancaGrupoUsuario-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response, { 'copy': ['TreeDisponiveis', 'TreeSelecionados'] });
              
                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    vm.TreeDisponiveis = ko.observableArray([]);
                    vm.TreeSelecionados = ko.observableArray($("#tree-selecionados").jstree('deselect_all').jstree('get_json'));

                    var data = ko.toJSON(vm);

                    grupoUsuario.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Grupo de usuário salvo com sucesso.');

                        Menu.reload();

                    });
                };

                function toggleButtons(tree, elements) {
                    novo.attr({ 'disabled': true, 'title': 'Escolha uma única pasta para inserir uma filha' });

                    if (treeUtil.getSelectedNodes(tree) > 1) {
                        renomear.attr({ 'disabled': true, 'title': 'Selecione somente 1 (um) item para renomear' });
                        icone.attr({ 'disabled': true, 'title': 'Selecione somente 1 (um) item para alterar o ícone' });
                    }
                    else {
                        renomear.attr({ 'disabled': false, 'title': '' });
                        icone.attr({ 'disabled': false, 'title': '' });
                        if (treeUtil.getSelectedNodes(tree) > 0 && treeUtil.getSelectedNodes(tree, true)[0].type === 'folder') {
                            novo.attr({ 'disabled': false, 'title': '' });
                        }
                    }
                    if (copiedItens && copiedItens.length > 0)
                        colar.attr('disabled', false);
                    else
                        colar.attr('disabled', true);
                };

                var panelSelection = $('#panelSelection'),
                    selecionados = panelSelection.find('.selecionados'),
                    novo = panelSelection.find('.novo'),
                    remover = panelSelection.find('.remover'),
                    copiar = panelSelection.find('.copiar'),
                    colar = panelSelection.find('.colar'),
                    renomear = panelSelection.find('.renomear'),
                    icone = panelSelection.find('.icone'),
                    cancelar = panelSelection.find('.cancelar'),
                    copiedItens = null;

                var treeMenu = $("#tree-selecionados").jstree({
                    'core': {
                        'check_callback': function (operation, node, node_parent, node_position, more) {
                            // operation can be 'create_node', 'rename_node', 'delete_node', 'move_node' or 'copy_node'
                            // in case of 'rename_node' node_position is filled with the new node name
                            if (operation === "move_node") {
                                return true;
                            }
                            if (operation === "copy_node") {
                                return true;
                            }
                            // Evita criação de nó filho na tela
                            if (operation === "create_node" && node_parent.type === "default") {
                                alert('Não pode ser criado uma tela aninhada');
                                return false;
                            }
                            return true;
                        },
                        'data': vm.TreeSelecionados
                    },
                    'checkbox': {
                        'three_state': false
                    },
                    'search': {
                        'show_only_matches': true
                    },
                    'types': {
                        "#": {
                            "valid_children": ["default", "folder"]
                        },
                        "root": {
                            "valid_children": ["default", "folder"]
                        },
                        'folder': {
                            "valid_children": ["default", "folder"],
                            "icon": "fa fa-folder"
                        },
                        'default': {
                            "icon": "fa fa-file-o",
                            "valid_children": []
                        }
                    },
                    'plugins': ['checkbox', 'dnd', 'types', 'search'/*, 'contextmenu'*/]
                }).on('copy_node.jstree', function (e, node) {
                    node.node.data = node.original.data;
                    // Linha 3922 jstree.js
                    // node = old_ins ? old_ins.get_json(obj, { no_id : true, no_data : false, no_state : true }) : obj;
                    toggleButtons(treeMenu);
                }).on('select_node.jstree', function (e, node) {
                    var selectedLength = treeUtil.getSelectedNodes(treeMenu);
                    panelSelection.velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 200);
                    selecionados.html(treeUtil.getTextSelected(treeMenu, selectedLength));
                    toggleButtons(treeMenu);
                }).on('deselect_node.jstree', function (e, node) {
                    var selectedLength = treeUtil.getSelectedNodes(treeMenu);
                    selecionados.html(treeUtil.getTextSelected(treeMenu, selectedLength));
                    if (selectedLength == 0)
                        panelSelection.velocity({ bottom: '-74px', opacity: 0 }, 'easeInOut', 200);
                    toggleButtons(treeMenu);
                }).on('dnd_stop.vakata', function (data) {
                }).on('delete_node.jstree', function (e, node) {
                    panelSelection.velocity({ bottom: '-74px', opacity: 0 }, 'easeInOut', 200);
                    toggleButtons(treeMenu);
                }).on('paste.jstree', function (e, parent, node, mode) {
                    if (data.reference == "tela")
                        treeMenu.jstree('copy_node', treeUtil.getSelectedNodes(treeTelas, true), '#');

                    toggleButtons(treeMenu);
                });

                var treeTelas = $("#tree-disponiveis").jstree({
                    'core': {
                        'check_callback': false,
                        'data': vm.TreeDisponiveis
                    },
                    'checkbox': {
                        'three_state': false
                    },
                    'types': {
                        "folder": {
                            "icon": "fa fa-folder"
                        },
                        "default": {
                            "icon": "fa fa-file-o",
                            "valid_children": []
                        }
                    },
                    'search': {
                        'show_only_matches': true
                    },
                    'dnd': {
                        'always_copy': true
                    },
                    'plugins': ['types', 'checkbox', 'dnd', 'search']
                });

                var buscaTreeMenu = $('#BuscaTreeTelasSelecionadas'),
                    buscaTreeTelas = $('#BuscaTreeTelasDisponiveis');

                treeUtil.searchEvents(treeMenu, buscaTreeMenu);
                treeUtil.searchEvents(treeTelas, buscaTreeTelas);

                $('#btnNovaCategoria').on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    treeMenu.jstree('create_node', '#', defaults.jstree.nodeCategoria, 'first');
                });

                $('.btnExpandirCategoria').on('click', function (e) {
                    e.preventDefault();
                    var tree = $('#' + $(this).data('tree'));
                    tree.jstree('open_all', '#', 200);
                });

                $('.btnRecolherCategoria').on('click', function (e) {
                    e.preventDefault();
                    var tree = $('#' + $(this).data('tree'));
                    tree.jstree('close_all', '#', 200);
                });

                // Ações do panel
                remover.on('click', function (e) {
                    e.preventDefault();
                    treeMenu.jstree('delete_node', treeMenu.jstree('get_selected', true));
                });
                copiar.on('click', function (e) {
                    copiedItens = treeUtil.getSelectedNodes(treeMenu, true);
                    var l = copiedItens.length;
                    e.preventDefault();
                    toastr.info((l + ' ite' + (l > 1 ? 'ns' : 'm') + ' copiado' + (l > 1 ? 's' : '')), '', 100);
                    toggleButtons(treeMenu);
                });
                renomear.on('click', function (e) {
                    e.preventDefault();
                    var node = treeUtil.getSelectedNodes(treeMenu, true)[0];

                    bootbox.prompt({
                        title: "Renomear",
                        value: node.text,
                        callback: function (newName) {
                            if (newName === null) {
                            } else {
                                treeMenu.jstree('rename_node', node, (newName = newName && newName.length > 0 ? newName : node.text));
                            }
                        }
                    });
                });
                icone.on('click', function (e) {
                    e.preventDefault();
                    var node = treeUtil.getSelectedNodes(treeMenu, true)[0];

                    bootbox.prompt({
                        title: "Insira a classe do ícone",
                        value: node.icon,
                        callback: function (newIcon) {
                            console.log(newIcon);
                            if (newIcon === null || newIcon == '' || newIcon == 'True' || newIcon == 'true') {
                                newIcon = node.children.length > 0 ? 'fa fa-folder' : 'fa fa-file-o';
                                treeMenu.jstree('set_icon', node, newIcon);
                            } else {
                                treeMenu.jstree('set_icon', node, newIcon);
                            }
                        }
                    });
                });
                novo.on('click', function (e) {
                    e.preventDefault();
                    // get last selected node
                    var lastSelected = treeMenu.jstree('get_selected').pop();
                    treeMenu.jstree('create_node', lastSelected, defaults.jstree.nodeCategoria, 'inside');
                    //treeMenu.jstree('copy_node', copiedItens, lastSelected, 'inside');

                });
                colar.on('click', function (e) {
                    e.preventDefault();
                    if (copiedItens != null) {
                        if (copiedItens.length > 0) {
                            // get last selected node
                            var lastSelected = treeMenu.jstree('get_selected').pop();
                            treeMenu.jstree('copy_node', copiedItens, lastSelected, 'after');
                        }
                    }
                });
                cancelar.on('click', function (e) {
                    e.preventDefault();
                    treeMenu.jstree('deselect_all');
                    panelSelection.velocity({ bottom: '-74px', opacity: 0 }, 'easeInOut', 200);
                });

                window.vm = vm;
                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});