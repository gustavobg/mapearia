﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate'], function (restricaoAcesso, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            restricaoAcesso.config({
                saveUrl: '/Seguranca/RestricaoAcesso/SalvarConfiguracao',
                getUrl: '/Seguranca/RestricaoAcesso/NewEditJson',
            });
            var request = restricaoAcesso.get({ id: id }),
                form = $('#form-RestricaoAcesso-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);
              
                vm.Save = function () {
                    //if (!vm.isValidShowErrors()) { return; };

                    var data = { 'json': ko.toJSON(vm) };

                    restricaoAcesso.save(JSON.stringify(data), { dataType: "json" }).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Restrições salvas com sucesso.', false, function () {
                            window.location = '/#/Seguranca/GrupoUsuario/Index';
                        });
                    });
                };

                window.vm = vm;

                ko.applyBindingsToNode(document.getElementById('pnlObjetos'), { collapsible: { vmArray: [vm.IdsObjetos()] } });
                ko.applyBindingsToNode(document.getElementById('pnlTelas'), { collapsible: { vmArray: [vm.Restricoes()] } });

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});