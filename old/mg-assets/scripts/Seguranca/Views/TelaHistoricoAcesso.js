﻿define(['crud-controller', 'knockout', 'feedback', 'select2', 'jquery-flipper', 'ko-validate', 'jquery-inputmask'], function (empresa, ko, mgFeedbackBase) {

    var vm = {},
        bind = function (id) {
            empresa.config({
                saveUrl: '/Corporativo/Empresa/NewEdit',
                getUrl: '/Corporativo/Empresa/NewEditJson',
            });
            var request = empresa.get({ id: id }),
                form = $('#form-CorporativoEmpresa-NewEdit');

            request.done(function (response) {
                vm = ko.mapping.fromJS(response);

                vm.Save = function () {
                    if (!vm.isValidShowErrors()) { return; };

                    var data = ko.toJSON(vm);

                    empresa.save(data).done(function (result, status, xhr) {
                        mgFeedbackBase.feedbackCrudRoute(result, 'Empresa salva com sucesso.');
                    });
                };
                
                window.vm = vm;

                ko.applyBindings(vm, form[0]);
            });

            return request;
        };

    return {
        bind: bind
    }

});