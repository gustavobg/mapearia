﻿define(['util', 'jquery', 'knockout'], function (mg, $, ko) {

    var getConfig = function (obj) {   
            settings = obj;
        },
        settings = {},
        get = function (data, o) {
        	settings = $.extend(true, settings, o);
            if (!settings.hasOwnProperty('getUrl')) {
                throw ('Necessário informar URL do método para recuperar as informações (getUrl)');
            }
            return mg.getViewModel($.extend(true, { id: mg.getId(data), url: settings.getUrl }, data));
        },
        save = function (data, o) {
        	settings = $.extend(true, settings, o);
            if (!settings.hasOwnProperty('saveUrl'))
                throw ('Necessário informar URL do método para salvar o registro (saveUrl)');
            // deffered
            return $.ajaxJsonAntiforgery(Route.getToken(), {
                data: data, //ko.toJSON(vm),
                url: settings.saveUrl // saveNew ? saveNewEditUrl : newEditUrl,            
            });
        },
		// TODO
    	remove = function (data, o) {
    		settings = $.extend(true, settings, o);
    		if (!settings.hasOwnProperty('removeUrl'))
    			throw ('Necessário informar URL do método para remover o registro (removeUrl)');
    		return $.ajax(Route.getToken(), {
    			data: data, //ko.toJSON(vm),
    			url: settings.saveUrl // saveNew ? saveNewEditUrl : newEditUrl,            
    		});
    	};

    var controller = function (config) {    	
    	return {
    		get: function (data, o) { return get(data, config) },
    		save: function (data, o) { return save(data, config) },
    		remove: function (data, o) { return save(data, config) }
    	}
    }

    var getGridApiFromIndex = function (routeContainerNumber) {
        // get component instance from gridView
        var routeContainer = !routeContainer ? 'route-container-1' : 'route-container-' + routeContainerNumber;
        return ko.contextFor(document.getElementById(routeContainer).getElementsByTagName('grid-view-index')[0])['$data'].indexVM.api();
    };

    var getGridViewModel = function (routeContainerNumber) {
        // get component instance from gridView
        var routeContainer = !routeContainer ? 'route-container-1' : 'route-container-' + routeContainerNumber;
        return ko.contextFor(document.getElementById(routeContainer).getElementsByTagName('grid-view-index')[0])['$data'].indexVM;
    };

    return {
		instance: controller,
        config: getConfig,
        get: get,
        save: save,
        getGridApiFromIndex: getGridApiFromIndex,
        getGridViewModel: getGridViewModel
    }

});