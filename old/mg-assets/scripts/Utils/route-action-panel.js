﻿define([], function() {
    var setActionPanelHTML = function (routeOptions) {

        var htmlBackButton = '',
            htmlHistoryButton = '',
            html = '',
            parentIdHtml = (routeOptions.hasOwnProperty('parentId') && routeOptions.parentId != null ? '/' + routeOptions.parentId : ''),
            action = routeOptions.action;

        if (routeOptions.pos > 1 && routeOptions.showBackButton) {
            if (routeOptions.hasOwnProperty('routeBack')) {
                var previousRouteBackIndex = routeOptions.routeBack.length > 2 ? routeOptions.routeBack.length - 2 : 0;
                htmlBackButton = '<a href="' + routeOptions.routeBack[previousRouteBackIndex]['url'] + '" type="button" class="btn btn-primary btn-inverted btn-lg"><i class="material-icons material-md">&#xE5C4;</i> Voltar</a>';
            } else {
                htmlBackButton = '<a href="' + routeOptions.baseRouteUrl + '/List' + parentIdHtml + '" type="button" class="btn btn-primary btn-inverted btn-lg"><i class="material-icons material-md">&#xE5C4;</i> Voltar</a>';
            }
        }
        if (routeOptions.id != null && routeOptions.showHistoryButton && routeOptions.action !== 'New') {
            if (action !== 'Edit')
                htmlHistoryButton = '<a href="/#/' + routeOptions.modulo + '/' + action + '/History' + parentIdHtml + '/' + routeOptions.id + '" type="button" class="btn btn-primary btn-inverted btn-lg pull-right"><i class="material-icons material-md">&#xE889;</i> Histórico</a>';
            else
                htmlHistoryButton = '<a href="' + routeOptions.baseRouteUrl + '/History' + parentIdHtml + '/' + routeOptions.id + '" type="button" class="btn btn-primary btn-inverted btn-lg pull-right"><i class="material-icons material-md">&#xE889;</i> Histórico</a>';
        }
        if (routeOptions.showActionPanel === true) {
            html = '<div class="panel panel-default panel-action">' +
                        '<div class="panel-body">' +
                            (routeOptions.showSaveNew ? '<div class="checkbox checkbox-inline first"><label for="saveNewCheckbox"><input id="saveNewCheckbox" type="checkbox" data-bind="vanillaUniform: {}, checked: Route.saveNew" /> Salvar e Criar Novo</label></div>' : '') +
                            (routeOptions.showSaveButton ? '<button type="submit" id="btn-salvar" class="btn btn-primary btn-primary-action btn-lg"><i class="material-icons material-md">&#xE5CA;</i> Salvar</button>' : '') +
                            htmlBackButton +
                            htmlHistoryButton +
                        '</div>' +
                    '</div>';
        }
        return html;
    };

    return setActionPanelHTML;
});