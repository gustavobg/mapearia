﻿define(['jquery'], function ($) {
    var setBackButton = function (routeOptions) {
        var parentIdHtml = (routeOptions.hasOwnProperty('parentId') && routeOptions.parentId != null ? '/' + routeOptions.parentId : ''),
            btnBack = $('#btnBack');
       
        if (routeOptions.showBackButton) {
            // definir rota de volta           
            if (routeOptions.hasOwnProperty('routeBack')) {
                var previousRouteBackIndex = routeOptions.routeBack.length > 2 ? routeOptions.routeBack.length - 2 : 0;
                btnBack.addClass('in').find('a').attr('href', routeOptions.routeBack[previousRouteBackIndex]['url']);
            } else {
                if (routeOptions.action === 'History') {
                    btnBack.addClass('in').find('a').attr('href', routeOptions.baseRouteUrl + '/Edit/' + parentIdHtml + id);
                } else {
                    btnBack.addClass('in').find('a').attr('href', routeOptions.baseRouteUrl + '/List' + parentIdHtml);
                }
            }
        } else {
            // hide back button
            btnBack.removeClass('in');
        }
    };

    return setBackButton;
});
