﻿define(['jquery', 'knockout'], function ($, ko) {
    var setBreadCrumb = function (routeOptions) {
        var html = '',
            actionTitle = '',
            parentIdHtml = (routeOptions.hasOwnProperty('parentId') && routeOptions.parentId != null ? '/' + routeOptions.parentId : '');

        switch (routeOptions.action) {
            case 'New':
                actionTitle = 'Novo'; break;
            case 'Edit':
                actionTitle = (routeOptions.hasOwnProperty('actionTitle') ? routeOptions.actionTitle : ''); break;
            case 'History':
                actionTitle = 'Histórico'; break;
            case 'View':
                actionTitle = 'Visualização'; break;
            case 'List':
                actionTitle = 'Listagem'; break;
            default:
                actionTitle = (routeOptions.hasOwnProperty('actionTitle') ? routeOptions.actionTitle : '');
        }       

        if (routeOptions.showBreadcrumb) {
            if (routeOptions.hasOwnProperty('routeBack') && routeOptions.routeBack.length > 0) {
                // rotas customizadas
                var i = 0, routeBack = routeOptions.routeBack, routeBackLength = routeBack.length;
                for (i; i < routeBackLength - 1;) {
                    var routeBackItem = routeBack[i];
                    html += '<li><a href="' + routeBackItem['url'] + '">' + routeBackItem['displayName'] + '</a></li>';
                    i = i + 1;
                }
                html += '<li>' + routeBack[i]['displayName'] + '</li>';
            } else {
                // crud padrão
                if (routeOptions.action === 'History') {
                    html = '<li><a id="tituloTela" href="/' + routeOptions.baseRouteUrl + '/List' + parentIdHtml + '"></a></li>';
                    html += '<li><a href="/' + routeOptions.baseRouteUrl + '/Edit' + parentIdHtml + '/' + routeOptions.id + '">Edição</a></li>';
                    html += '<li class="actiHve">' + 'Histórico' + '</li>';
                } else if (routeOptions.action === 'List') {
                    html += '<li id="tituloTela" class="actiHve">' + '' + '</li>';
                } else {
                    html = '<li><a id="tituloTela" href="/' + routeOptions.baseRouteUrl + '/List' + parentIdHtml + '"></a></li>';
                    html += '<li class="actiHve">' + (actionTitle == '' ? 'Edição' : actionTitle) + '</li>';
                }
            }            
            if (routeOptions.hasOwnProperty('showTitle') && routeOptions.showTitle == true) {
                // show only title
                html = '<li id="tituloTela"></li>'
            }
        }

        $("#breadcrumb ol").empty().prepend(html);
      
        var tituloTela = document.getElementById('tituloTela');

        if (tituloTela != null) {
            if (window.hasOwnProperty('Tela'))
                tituloTela.innerHTML = window.Tela.TituloTela();
        }

    };

    return setBreadCrumb;
});
