﻿define(["jquery", "route-header-info", "route-bread-crumb", "route-back-button", "route-action-panel", "error-handler"], function ($, setHeaderInfo, setBreadCrumb, setBackButton, setActionPanelHTML, log) {
    var edit = function (routeOptions) {

        var path = null,
            id = routeOptions.id,
            viewName = routeOptions.modulo + '/Views/' + (routeOptions.hasOwnProperty('viewName') ? routeOptions.viewName : routeOptions.area),
            position = routeOptions.pos,
            pathScripts = viewName;

        window.scrollTo(0, 0);

        require([pathScripts], function (view) {

            if (view == undefined)
                throw ('A view deve retornar um objeto com a propriedade bind (que deve ser do tipo deferred), ou objeto página padrão');
            
            if (!view.hasOwnProperty('bind')) {
                // view padrão sem uso do CRUD
                Route.afterBindings();

                if (view.hasOwnProperty('routeOptions'));
                    routeOptions = $.extend(true, routeOptions, view.routeOptions);

                setBreadCrumb(routeOptions);
                setHeaderInfo(routeOptions);

                if (view.hasOwnProperty('init')) {
                	view.init(id);
                }
                return;
            } else {
                if (view.hasOwnProperty('url')) {
                    path = view.url;
                }
            }
            
            if (view.hasOwnProperty('routeOptions'));
                routeOptions = $.extend(true, routeOptions, view.routeOptions);

            setBackButton(routeOptions);

            require(['knockout', 'velocity'], function (ko) {
                // view padrão CRUD
                var routeContainer = routeOptions.routeContainer,
                    editForm = routeContainer.find('form'),
                    token = null,
                    footerHtml = '';

                if (routeOptions.showActionPanel) {
                    editForm.append(setActionPanelHTML(routeOptions));
                }
                if (routeOptions.view === true) {
                    editForm.addClass('read-mode');
                } else {
                    editForm.removeClass('read-mode');
                }

                editForm.find('.panel-action').not('.panel-selected').velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 500);

                if (!view.hasOwnProperty('bind')) {
                    throw ('No método bind da View, defina o método "bind" retornando a requisição (deferred)');
                };
                if (view.bind.hasOwnProperty('done')) {
                    throw ('No método bind da View, deve-se retornar a requisição (deferred)');
                }

                // catch any javascript errors from the route change
                view.bind(id).done(function (e, data) {
                    Route.afterBindings();
                    setBreadCrumb(routeOptions);
                }).fail(function (response) {
                    // Erro ao carregar view de Edição                   
                    var errorMessage = 'Erro ao carregar a view: ' + Route.routeOptions.url;
                    if (typeof(response) === 'object') {
                        if (response.hasOwnProperty('callback')) {
                            response.callback();
                        } else {
                            log(true, errorMessage);
                        }
                    } else {
                        log(true, errorMessage);
                    }
                });
            });
        });
        setHeaderInfo(routeOptions);
    };
    return edit;
});

