﻿define([], function () {
    var setHeaderInfo = function (routeOptions) {
        var mainTitle = document.getElementById('tituloTela'),
            additionalInfo = document.getElementById('title-aditional-info'),
            header = additionalInfo.parentNode,
            titleCodigo = routeOptions.titleCodigo.toString(),
            titleDescricao = routeOptions.titleDescricao.toString();

        if (routeOptions.hasOwnProperty('title') && routeOptions.title.length > 0)
            if (window.hasOwnProperty('Tela'))
                window.Tela.TituloTela(routeOptions.title);
        else
            mainTitle.innerHTML = routeOptions.title;

        if (routeOptions.hasOwnProperty('showHeader') && routeOptions.showHeader == false) {
            document.getElementById('content-header-toolbar').style.display = 'none';
        }

        if (titleCodigo.length > 0 || titleDescricao.length > 0) {
            header.className = 'has-info';
            additionalInfo.style.display = 'block';
            document.getElementById('title-aditional-info-code').innerHTML = '(' + titleCodigo + ')';
            document.getElementById('title-aditional-info-text').innerHTML = titleDescricao;
        } else {
            header.className = '';
            additionalInfo.style.display = 'none';
            document.getElementById('title-aditional-info-code').innerHTML = '';
            document.getElementById('title-aditional-info-text').innerHTML = '';
        }
    };
    return setHeaderInfo;
});
