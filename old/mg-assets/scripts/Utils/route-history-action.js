﻿define(['jquery', 'route-bread-crumb', 'route-back-button', 'dateFormat', 'gridview', 'toastr', 'velocity'], function ($, setBreadCrumb, setBackButton) {

    var history = function (routeOptions) {       
        var routeContainer = routeOptions.routeContainer;
        filtroGrade = routeContainer.find('#grade-historico').hide(),
        baseRouteUrl = '/' + routeOptions.baseRouteUrl;
        id = routeOptions.id,
        isBound = function (id) {
            return ko.contextFor(id) && ko.contextFor(id).hasOwnProperty('$data') ? true : false;
        };

        if (isBound(filtroGrade)) {
            // refresh grid   
            window.historyVM.api().refresh();
        } else {
            var historyVM = { api: ko.observable({}) };
            historyVM.formatDate = function (dateJson, format) {
                if (!dateJson || dateJson == '' || dateJson == 'undefined')
                    return '';
                return dateFormat(new Date(parseInt(dateJson.replace('/Date(', ''))), format)
            };
            historyVM.expandGridRow = function (event, url, data) {                
                var btn = $(event.target);
                var tr = $(event.toElement.parentNode.parentNode);

                if (tr.data('expand-grid-row')) {
                    tr.data('expand-grid-row', false);
                    tr.next().remove();

                    btn.toggleClass('expanded');
                    return;
                }

                btn.toggleClass('expanded');

                var tdCount = tr.find('td').length;

                tr.data('expand-grid-row', true);

                $.get(url, routeOptions.data, function (html) {
                    $('<tr class="sub"><td colspan="' + tdCount + '"></td></tr>').insertAfter(tr).find('td').html(html);                      
                });
            };           
            ko.applyBindings(historyVM, filtroGrade[0]);
        }

        filtroGrade.show();
        setBackButton(routeOptions);
        setBreadCrumb(routeOptions);
        Route.afterBindings();
       
    };
    return history;
});

