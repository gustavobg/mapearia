﻿define(['jquery', 'knockout', 'route-header-info', 'route-bread-crumb', 'route-back-button', '!domReady', 'vanillaUniform', 'button', 'jquery-collapsible', 'daterangepicker', 'gridview'], function ($, ko, setHeaderInfo, setBreadCrumb, setBackButton, domReady) {

    "use strict";

    var list = function (routeOptions) {
        var routeContainer = routeOptions.routeContainer,
            isBound = function (id) {
                return ko.contextFor(id) && ko.contextFor(id).hasOwnProperty('$data') ? true : false;
            },
            gridViewContext = ko.contextFor(routeContainer[0].getElementsByTagName('grid-view-index')[0]);

        var filtroGrade = routeContainer.find('#filtro-grade')[0];

        if (isBound(filtroGrade)) {
            // refresh grid
            window.setTimeout(function () {                   
                gridViewContext['$data'].indexVM.api().refresh();
            }, 50);

            Route.afterBindings();
            window.scrollTo(0, scrollY);
        } else {
                
            ko.applyBindings({ indexVM: { api: ko.observable({}), useSelection: ko.observable(true) }}, filtroGrade);

            Route.afterBindings();

            window.setTimeout(function () {
                $('#inputBuscaRapida').focus();
            }, 100);

            if (routeOptions.hasOwnProperty('useCustomView') && routeOptions.useCustomView == true) {
                var viewName = routeOptions.modulo + '/Views/Index' + (routeOptions.hasOwnProperty('viewName') ? routeOptions.viewName : routeOptions.area);
                require([viewName], function (view) {
                    if (!view.hasOwnProperty('bind')) {
                        throw ('No método bind da View, defina o método "bind"');
                    } else {
                        view.bind();
                    }
                });
            }
        }

        filtroGrade.style.display = 'block';

        domReady(function () {
            setBackButton(routeOptions);
            setBreadCrumb(routeOptions);
            setHeaderInfo(routeOptions);
        });
    };

    return list;
});