﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'toastr'], factory);
    } else {
        factory(root.jQuery, root.toastr);
    }
}(this, function ($, toastr) {

    $.getAntiForgeryToken = function (form) {
        if (typeof (form) === 'string')
            return form;
        return form.find('input[name="__RequestVerificationToken"]').val();        
    };

    $.ajaxGetJsonAntiforgery = function (url, data) {
        var headers = {},
            settings = {},
            errorMessage = 'Erro ao listar as informações, contate o administrador do sistema. ';

        headers['__RequestVerificationToken'] = Route.getToken();

        settings.url = url;
        settings.data = JSON.stringify(data);
        settings.dataType = 'json';
        settings.contentType = 'application/json; charset=utf-8';
        settings.type = 'POST';
        settings.cache = false;
        settings.headers = headers;

        // error feedback - form list
        settings.error = function (response) {
            // exceção não tratada
            toastr.error(errorMessage + '(' + response.status + ')', '', { timeOut: 5000 });
        };
        settings.success = function (response) {           
            if (response.hasOwnProperty('Response')) {
                var response = response.Response;
                // mensagem de erro tratada pelo servidor, responsta 200            
                var identificador = response.Identificador || 3,
                    mensagem = response.hasOwnProperty('MensagemFinal') && response.MensagemFinal.length > 0 ? response.MensagemFinal : errorMessage,
                    sucesso = response.hasOwnProperty('Sucesso') ? response.Sucesso : true;

                if (sucesso == false) {
                    if (identificador == 3) {
                        // 3 - observação
                        toastr.info(mensagem, '', { timeOut: 5000 });
                    } else {
                        // 4 - erro
                    	//toastr.error(mensagem, '', { timeOut: 5000 });
                    	// erro obstrui tela                    
                    	Route.showErrorBlock(mensagem);
                    	// document.body.classList.add('loading-fail');
                    }
                }
            } 
        };
        return $.ajax(settings);
    }

    $.ajaxJsonAntiforgery = function (formId, settings, token) {
        // Usados para POST's, salvar registros

        var headers = {};
                
        headers['__RequestVerificationToken'] = Route.getToken();

        settings.dataType = 'json';
        settings.contentType = 'application/json; charset=utf-8';
        settings.type = 'POST';
        settings.cache = false;
        settings.headers = headers;

        settings.beforeSend = function () {
            Loader.show();            
        };
        settings.complete = function () {
            Loader.hide();
        };

        // error feedback - form save
        settings.error = function (response) {
            var errorMessage = 'Erro ao salvar as informações, contate o administrador do sistema. ';
            toastr.error(errorMessage + '(' + response.status + ')', '', { timeOut: 5000 });
        };

        return $.ajax(settings);
    };

}));