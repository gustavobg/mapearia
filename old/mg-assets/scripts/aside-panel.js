﻿define(['knockout','jquery', 'text!../templates/aside-panel.html', 'velocity', 'velocity-ui'], function (ko, $, template) {

	ko.components.register('aside-panel', {
		viewModel: function (params) {
			var self = this,
                getValueObservable = function (value) {
                	if (ko.isObservable(value))
                		return value;
                	else {
                		return Array.isArray(value) ? ko.observableArray(value) : ko.observable(value);
                	}
                },
                context = params.getContext;


			this.data = params.data;
			
		},
		template: template
	});

});