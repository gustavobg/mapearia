﻿var mgChat = {
    Id: 0,
    Debug: false,
    RefreshTime: 6000,
    UserImage: '',
    NewMessageInfo: null,
    Minimized: $.cookie('mgChat-minimized') ? $.cookie('mgChat-minimized') : false,
    ContainerUsersOnline: "body",
    Init: function () {
        $(function () {

            $('#mgchat-dropdown-messages').hide();

            $('body').on("click", function (e) {
                mgChat.CloseMessageInfo();
            });

            $('.menu-message').on('click', function (e) {                
                e.stopPropagation();
                mgChat.CreateNewMesssageInfo();
            });

            mgChat.GetOnlineUsers(function () {               

                var chatWindowUserId = mg.GetSession('mgchat-window-userid');

                if (chatWindowUserId != null) {
                    mgChat.CreateChatWindow(chatWindowUserId);
                }

                mgChat.Ready();
            });
        });
    },
    Ready: function () {
        if (mgChat.Id != 0) {
            window.clearInterval(mgChat.Id);
        }

        mgChat.Work();
        mgChat.Id = window.setInterval(function () { mgChat.Work(); }, mgChat.RefreshTime);
    },
    OpenOnlineUsersWindow: function () {

        if (!$('#mgchat-usersonline').length) {
            if (mgChat.Users != null) {
                mgChat.CreateOnlineUsersWindow();
                mgChat.GetTotalMessages();
            }
            else {
                mgChat.GetOnlineUsers(function () {
                    mgChat.CreateOnlineUsersWindow();
                    mgChat.GetTotalMessages();
                });
            }
        }
    },
    GetOnlineUsers: function (callback) {
        $.ajax({
            global: false,
            type: "POST",
            url: "/Corporativo/Chat/OnlineUsers",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (!data || !data.Sucesso) {
                    mgChat.Users = null;
                    return;
                }

                mgChat.Users = data.Users;

                if (callback)
                    callback();
            }
        });
    },
    CloseOnlineUsersWindow: function () {
        var onlineUsersWindow = $('#mgchat-usersonline');
        if (!onlineUsersWindow.length)
            return;

        mg.RemoveSession('mgchat-usersonline-window');
        onlineUsersWindow.remove();
    },
    CreateOnlineUsersWindow: function () {

        if (mgChat.Users == null)
            return;


        var container = $('#mgchat-usersonline-container');

        if (!container.length || container.length == 0) {
            mg.SetSession('mgchat-usersonline-window', true);

            $("<div id='mgchat-usersonline'>" +
                    "<div id='mgchat-usersonline-head'>" +
                        "<div id='mgchat-usersonline-head-title'></div>" +
                        "<a href='#' id='mgchat-usersonline-head-close'>Fechar</a>" +
                     "</div>" +
                     "<div id='mgchat-usersonline-filter'>" +
                        "<div id='mgchat-usersonline-toolbar'>" +

                        "<div class='btn-group'>" + 
                          "<button type='button' id='mgchat-usersonline-filter-all' name='mgchat-usersonline-filter' value='all' class='btn btn-primary col-lg-4'>Todos</button>" +
                          "<button type='button' id='mgchat-usersonline-filter-online' name='mgchat-usersonline-filter' value='online' class='btn btn-primary col-lg-4'>Online</button>" +
                          "<button type='button' id='mgchat-usersonline-filter-newmessage' name='mgchat-usersonline-filter' value='newmessage' class='btn btn-primary col-lg-4'>c/ Msg</button>" +
                        "</div>" +

                        "</div>" + 
                        "<div id='mgchat-usersonline-filter-search'>" +
                            "<input type='text' id='mgchat-usersonline-filter-search-input' class='form-control' placeholder='filtrar por nome' />" +
                        "</div>" +
                     "</div>" +
                     "<div id='mgchat-usersonline-container'></div>" +
                "</div>")
            .appendTo(mgChat.ContainerUsersOnline);
            container = $('#mgchat-usersonline-container');


            var list = $('<ul></ul>');
            for (var i = 0; i < mgChat.Users.length; i++) {
                var user = mgChat.Users[i];

                str = '<li id="mgchat-userlist-user' + user.Id + '" ' +
                        'class="mgchat-userlist-user-' + (user.Online ? "online" : "offline") + ' mgchat-userlist-user" data-newmessage="false" data-online="' + user.Online + '"><a href="#" rel="' + user.Id + '">' +
                        '<img class="mgchat-userlist-user-image" src="' + user.Imagem + '" /><span class="mgchat-userlist-user-name" title="' + user.Nome + '">' + user.Nome + '</span><img class="mgchat-userlist-user-onoff ' + mgChat.OnlineOfflineClass(user) + '" id="mgchat-userlist-user-onoff' + user.Id + '"/>' +
                        '</a></li>';

                list.append(str);
            }
            container.append(list);

            container.find('a').click(function (e) {
                e.preventDefault();
                mgChat.CreateChatWindow($(this).attr('rel'));
                if (mg.GetSession('mgchat-usersonline-filter', 'all') == 'newmessage') {
                    $('#mgchat-usersonline-filter-all').click();
                }
            });

            //$('#mgchat-usersonline-filter');

            $('#mgchat-usersonline-filter-search-input').keyup(function () {
                mgChat.FilterUsersList();
            }).keydown(function (e) {                
                if (e.keyCode == 13) {
                    e.preventDefault();
                    var itemsLeft = container.find('ul li:visible');
                    if (itemsLeft.length == 1) {
                        var idItemLeft = $(itemsLeft[0]).find('a').attr('rel');
                        mgChat.CreateChatWindow(idItemLeft);
                        $('#mgchat-usersonline-filter-search-input').val('');
                    }
                }
            });

            var tipo = mg.GetSession('mgchat-usersonline-filter', 'all');
            
            $('#mgchat-usersonline-filter-' + tipo).addClass('active').click();

            $('#mgchat-usersonline-filter-online').click(function (e) {
                mg.SetSession('mgchat-usersonline-filter', 'online');
                mgChat.FilterUsersList();
                mgChat.SetToolbarActive(this);
            });
            $('#mgchat-usersonline-filter-all').click(function (e) {
                mg.SetSession('mgchat-usersonline-filter', 'all');
                mgChat.FilterUsersList();
                mgChat.SetToolbarActive(this);
            });
            $('#mgchat-usersonline-filter-newmessage').click(function (e) {
                mg.SetSession('mgchat-usersonline-filter', 'newmessage');
                mgChat.FilterUsersList();
                mgChat.SetToolbarActive(this);
            });

            $('#mgchat-usersonline-head-close').click(function (e) {
                e.preventDefault();
                mgChat.CloseOnlineUsersWindow();
            }).button({
                icons: {
                    primary: "ui-icon-close"
                },
                text: false
            });
        }
        else {
            for (var i = 0; i < mgChat.Users.length; i++) {
                var user = mgChat.Users[i];
                var userListItem = $('#mgchat-userlist-user' + user.Id);
                var img = $('#mgchat-userlist-user-onoff' + user.Id);

                userListItem.data('online', user.Online);

                if (userListItem.is('.mgchat-userlist-user-online')) {
                    if (!user.Online) {
                        userListItem.removeClass('mgchat-userlist-user-online').addClass('mgchat-userlist-user-offline');
                    }
                }
                else {
                    if (user.Online) {
                        userListItem.removeClass('mgchat-userlist-user-offline').addClass('mgchat-userlist-user-online');
                    }
                }

                img.removeClass("on").removeClass("off");
                img.addClass(mgChat.OnlineOfflineClass(user));
            }
        }

        mgChat.FilterUsersList();
    },
    SetToolbarActive: function (element) {
        $("#mgchat-usersonline-toolbar").find('button').removeClass('active');
        $(element).addClass('active');
    },
    FilterUsersList: function () {
        var tipo = mg.GetSession('mgchat-usersonline-filter', 'all');
        var container = $('#mgchat-usersonline-container');

        if (!container.length)
            return;

        if (tipo == 'all') {
            container.find('ul li').show();
        }
        else if (tipo == 'online') {
            container.find('.mgchat-userlist-user-online').show();
            container.find('.mgchat-userlist-user-offline').hide();
        }
        else if (tipo == 'newmessage') {
            $.each(container.find('[data-newmessage]'), function (index, obj) {
                if ($(this).data('newmessage') == 'false') {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            });
        }

        var str = $('#mgchat-usersonline-filter-search-input').val();
        if (str != '') {
            var usersToShow = mgChat.FindUsersByName(str);

            $.each(mgChat.Users, function (i, user) {
                var listItem = $('#mgchat-userlist-user' + user.Id);
                var show = false;

                if (listItem.is(':visible')) {
                    $.each(usersToShow, function (j, userToShow) {
                        if (userToShow.Id == user.Id) {
                            show = true;
                        }
                    });
                }
                if (show) {
                    listItem.show();
                }
                else {
                    listItem.hide();
                }
            });
        }
    },
    OnlineOfflineClass: function (user) {
        return user.Online ? 'on' : 'off';
    },
    CreateChatWindow: function (id) {
        mgChat.CloseChatWindow();

        var user = mgChat.FindUser(id);

        if (user == null)
            return;

        var chatWindow = mgChat.GetChatWindow(user);

        mg.SetSession('mgchat-window-userid', id);
        mgChat.GetMessagesFrom(true);
    },
    CloseChatWindow: function () {
        $('#mgchat-window').remove();
        mg.RemoveSession('mgchat-window-userid');
    },
    GetChatWindow: function (user) {

        $('<div id="mgchat-window" class="panel panel-default ' + (this.Minimized ? 'hide-chat' : '') + '">' +
                '<input type="hidden" id="mgchat-window-userid" value="' + user.Id + '" />' +
                '<div id="mgchat-window-head" class="panel-heading"><h3 class="panel-title">' + user.Nome + '</h3><a href="#" id="mgchat-window-head-close">×</a><a href="#" id="mgchat-window-head-minimize">Minimizar</a><img id="mgchat-window-head-image" src="' + user.Imagem + '" /><span id="mgchat-window-head-title"></span></div>' +
                '<div id="mgchat-window-container" class="panel-body">' +
                    '<div id="mgchat-window-history"></div>' +
                    '<div id="mgchat-window-messages"></div>' +
                    '<div id="mgchat-window-input">' +
                        '<textarea class="mgchat-window-input-text" id="mgchat-window-input-text" placeholder="Escreva uma mensagem" />' +
                        '<a href="#" class="mgchat-window-input-button btn btn-primary" id="mgchat-window-input-button">Enviar</a> <a class="mgchat-window-history" href="/WebChat/ChatHistorico.aspx?from=' + user.Id + '">Ver histórico</a>' +
                    '</div>' +
                '</div>' +
                '</div>').appendTo('body');

        var chatWindow = $('#mgchat-window');

        $('#mgchat-window-input-text').keydown(function (e) {
            if (e.ctrlKey && e.keyCode == 13) {
                $('#mgchat-window-input-button').click();
            } else if (e.keyCode == 27) {
                // Esc             
                mgChat.MinimizeChatWindow();
            }

        }).keyup(function (e) {
            var str = $(this).val();
            if (str.length > 1000) {
                $(this).val(str.substr(0, 1000));
                return false;
            }
        });
        window.setTimeout(function () {
            $('#mgchat-window-input-text').focus();
        }, 10);

        $('#mgchat-window-head-close').click(function (e) {
            e.preventDefault();
            mgChat.CloseChatWindow();
        });

        $(document).click(function (e) {
            var container = $("#mgchat-window");
            var usersContainer = $("#mgchat-usersonline-container");
            //console.log(!usersContainer.is(e.target) && usersContainer.has(e.target).length === 0);
            if (!container.is(e.target)
                && container.has(e.target).length === 0
                && e.target != 'btnChat'
                && !usersContainer.is(e.target)
                && usersContainer.has(e.target).length === 0
                )
            {
                mgChat.MinimizeChatWindow();
            } else {
                mgChat.ExpandChatWindow();
            }
            
        });

        $('#mgchat-window-head-minimize').click(function (e) {
            e.preventDefault();
            mgChat.MinimizeChatWindow();            
        });

        $('#mgchat-window-history').click(function () {           
            mgChat.CloseChatWindow();
        });

        $('#mgchat-window-input-button').click(function (e) {
            e.preventDefault();
            $(this).val('Aguarde...').button('disable');
            mgChat.ButtonSendClick();
        });

        chatWindow.draggable({ axis: "x", handle: "#mgchat-window-head", containment: "body" });

        $('#mgchat-window-input-text').focus();

        return chatWindow;
    },
    MinimizeChatWindow: function () {
        var container = $("#mgchat-window");
        container.addClass("hide-chat");
        this.Minimized = true;
        $.cookie('mgChat-minimized', true);
    },
    ExpandChatWindow: function () {
        var container = $("#mgchat-window");
        container.removeClass("hide-chat");
        this.Minimized = false;
        $.cookie('mgChat-minimized', false);
        window.setTimeout(function () {
            $("#mgchat-window-input-text").focus();
        }, 100);        
    },
    FindUsersByName: function (str) {
        var users = [];

        $.each(mgChat.Users, function (i, obj) {
            var user = mgChat.Users[i];

            if (user.Nome.toLowerCase().indexOf(str.toLowerCase()) >= 0) {
                users.push(user);
            }
        });

        return users;
    },
    FindUser: function (id) {
        var user;
        $.each(mgChat.Users, function (i, obj) {
            if (parseInt(obj.Id) == parseInt(id)) {
                user = obj;
                return;
            }
        });
        return user;
    },
    GetMessagesFrom: function (all) {
        var url = '/Corporativo/Chat/GetMessagesFrom';
        if (all) {
            url = '/Corporativo/Chat/GetAllMessagesFrom';
        }

        var userId = parseInt(mg.GetSession('mgchat-window-userid', 0));

        if (userId == 0)
            return;

        $.ajax({
            global:false,
            type: "POST",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ idPessoa: userId }),
            success: function (data) {
                if (!data || !data.Sucesso)
                    return;

                var messages = data.Messages;
                var ids = [];

                for (var i = 0; i < messages.length; i++) {
                    var msg = messages[i];
                    mgChat.AddMessageToWindow(msg, all);

                    if (msg.Recebida)
                        ids.push(msg.Id);
                }
                var windowMessages = $('#mgchat-window-messages');

                if (windowMessages.length) {                    
                    windowMessages.scrollTop(document.getElementById("mgchat-window-messages").scrollHeight);
                }

                if (ids.length > 0)
                    mgChat.ConfirmReading(ids);
            }
        });
    },
    GetMessage: function (id) {
        $.ajax({
            global: false,
            type: "POST",
            url: "/Corporativo/Chat/GetMessage",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ id: id }),
            success: function (data) {
                if (data && data.Messages.length > 0) {
                    return data.Messages[0];
                }
            }
        });
    },
    AddMessageToWindow: function (msg, all) {
        var messageContainer = $('#mgchat-window-messages');
        
        
        if (!$('#mgchat-window-message' + msg.Id).length) {

            var img = msg.Recebida ? mgChat.FindUser(msg.IdDe).Imagem : mgChat.UserImage;

            var str =
                "<div id='mgchat-window-message" + msg.Id + "' class='clearfix mgchat-window-message-" + (msg.Recebida ? "received" : "sent") + " '>" +
                    "<img src='" + img + "' />" +
                    "<div class='mgchat-window-message-baloon'>" +
                        "<div class='mgchat-window-message-body'>" + msg.Mensagem.replace(/(\r\n|\n|\r)/gm, '<br />') + "</div>" +
                        "<div class='mgchat-window-message-date'>" + msg.DataEnvio + "</div>" +
                    "</div>" +
                "</div>";
            messageContainer.append(str);

            

            if (!all) {
                messageContainer.animate({ scrollTop: document.getElementById("mgchat-window-messages").scrollHeight }, 1000);               
            }
        }
    },
    ConfirmReading: function (ids) {
        $.ajax({
            global: false,
            type: "POST",
            url: "/Corporativo/Chat/ConfirmReading",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ ids: ids }),
            success: function (data) {

            }
        });
    },
    Users: null,
    CloseMessageInfo: function () {
        $('#mgchat-dropdown-messages').slideUp(250).html('');
    },
    CreateNewMesssageInfo: function () {
        var notificationIcon = $('#mgchat-dropdown-messages').html('');
        
        var total = 0;

        if (mgChat.NewMessageInfo != null)
            total = mgChat.NewMessageInfo.Info.length;

        if (total > 0) {
            var notif = '<h3>Você possui ' + total.toString() +
					' nova' + (total > 1 ? 'as' : '') +
					' mensage' + (total > 1 ? 'ns' : 'm') +
				 '</h3>';
            var list = $('<ul></ul>');

            

            var messageCount = $.each(mgChat.NewMessageInfo.Info, function (index, obj) {
                var user = mgChat.FindUser(obj.IdPessoaDe);

                list.append(
			    "<li id='mgchat-dropdown-message-list" + user.Id + "' data-userid='" + user.Id + "'>" +
				    "<img src='" + user.Imagem + "' class='mgchat-dropdown-messages-avatar' />" +
				    "<span class='mgchat-dropdown-messages-name'>" + user.Nome + "</span>" +
				    "<span class='mgchat-dropdown-messages-message'>" + obj.Mensagem + "</span>" +
				    "<span class='mgchat-dropdown-messages-data'>" + obj.TempoEnviado + "</span>" +
			    "</li>");

                return index;
            });

            

            notificationIcon.append(notif);
            notificationIcon.append(list);
           
            notificationIcon.find('ul li').on('click', function (e) {
                e.stopPropagation();
                mgChat.CreateChatWindow($(this).data('userid'));
                mgChat.CloseMessageInfo();
            });
        }
        else {
            notificationIcon.append('<h3>Nenhuma nova mensagem</h3>');
        }

        notificationIcon.slideDown(250);
    },
    GetTotalMessages: function (callback) {
        $.ajax({
            global: false,
            type: "POST",
            url: "/Corporativo/Chat/TotalNewMessages",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                str = "";
                var total = 0;

                if (data && data.Sucesso) {
                    total = data.Info.length;
                    mgChat.NewMessageInfo = data;
                }
                else {
                    mgChat.NewMessageInfo = null;
                    return;
                }

                var userList = $('#mgchat-usersonline-container ul li');
                userList.removeClass('mgchat-usersonline-container-newmessage');
                userList.data('newmessage', 'false');

                var menuitemchat = $('#btnChat');
                var badge = menuitemchat.find('.badge');

                if (total > 0) {
                   
                    str += total.toString();


                    var chatWindowUserId = mg.GetSession('mgchat-window-userid');
                    

                    $.each(data.Info, function (index, obj) {
                        var user = mgChat.FindUser(obj.IdPessoaDe);

                        if (obj.IdPessoaDe == chatWindowUserId) {
                            var headImage = $('#mgchat-window-head-image');
                            headImage.bounce();
                        }

                        if (userList.length) {
                            var elem = $('#mgchat-userlist-user' + obj.IdPessoaDe);
                            elem.addClass('mgchat-usersonline-container-newmessage');
                            elem.data('newmessage', 'true');
                        }
                    });          
                    badge.text(str);
                    badge.show().bounceIn();
                }
                else {
                    badge.text('');
                    badge.hide();                    
                }

                mgChat.FilterUsersList();
            }
        });

        if (callback)
            callback();
    },
    Work: function () {

        mgChat.GetTotalMessages();

        if ($('#mgchat-usersonline').length) {
            mgChat.GetOnlineUsers(function () {
                mgChat.CreateOnlineUsersWindow();
            });
        }

        if ($('#mgchat-window').length && $('#mgchat-window-container').css("visibility") != "hidden") {
            mgChat.GetMessagesFrom();
        }
    },
    SendComplete: function () {
        var button = $('#mgchat-window-input-button');
        button.val('Enviar').button('enable');
    },
    ButtonSendClick: function () {
        var inputText = $('#mgchat-window-input-text');
        var button = $('#mgchat-window-input-button');

        var messageText = inputText.val();
        var cleanMsg = messageText.replace(/\r\n/g, '').replace(/\n/g, '').replace(/\s/g, '');
        if (cleanMsg == '') {
            inputText.focus();
            mgChat.SendComplete();
            return;
        }

        inputText.val('');

        $.ajax({
            global: false,
            url: "/Corporativo/Chat/SendMessage",
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            dataType: "json",
            data: JSON.stringify({ 'idPessoa': parseInt($('#mgchat-window-userid').val()), message: messageText }),
            success: function (data) {
                if (data && data.Messages.length > 0) {
                    mgChat.AddMessageToWindow(data.Messages[0]);
                }

                mgChat.SendComplete();
                inputText.focus();
            }
        });
    },
    Log: function (msg) {
        if (mgChat.Debug) {
            console.log('-- mgChat');
            console.log(msg);
        }
    }
}