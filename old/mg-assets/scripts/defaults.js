﻿var defaults = {
    'datepicker': {
        'defaultValues': {
            'icons': {
                time: 'fa fa-clock-o',
                up: 'fa fa-arrow-up',
                down: 'fa fa-arrow-down',
                date: 'fa fa-calendar-o',
                previous: 'fa fa-angle-left',
                next: 'fa fa-angle-right',
                today: 'glyphicon glyphicon-screenshot',
                clear: 'glyphicon glyphicon-trash',
                close: 'glyphicon glyphicon-remove'
            },
            'tooltips': {
                today: 'Hoje',
                clear: 'Limpar seleção',
                close: 'Fechar',
                selectMonth: 'Escolher o mês',
                prevMonth: 'Mês anterior',
                nextMonth: 'Próximo mês',
                selectYear: 'Escolher o ano',
                prevYear: 'Ano anterior',
                nextYear: 'Próximo ano',
                selectDecade: 'Escolher a década',
                prevDecade: 'Década anterior',
                nextDecade: 'Próxima década',
                prevCentury: 'Século Anterior',
                nextCentury: 'Próximo século'
            },
            locale: 'pt-br'
        },
        'date': { format: 'DD/MM/YYYY', enabledHours: false },
        'daymonth': { format: 'DD/MM', enabledHours: false },
        'monthyear': { format: 'MM/YY', enabledHours: false },
        'datetime': { format: 'DD/MM/YYYY HH:mm:ss' },
        'time': { format: 'HH:mm', enabledDates: false },
        'year': { format: 'YYYY', enabledDates: true, enabledHours: false, minDate: new Date(1981, 10, 11, 4, 30) }        
    },
    'gridNewEdit': {
        column: {
            acao: { 'displayName': 'Ações', 'width': '55px', 'header': true, 'sortable': false, 'css': 'acao' },
            acaoEditRemove: { 'displayName': 'Ações', 'width': '100px', 'header': true, 'sortable': false, 'css': 'acao' },
            ativo: { 'id': 'Ativo', 'width': '120px', 'header': true, 'filter': true, 'defaultValue': 'true', 'sortable': true, 'groupable': true, 'groupTextValues': ['Ativo', 'Inativo'] }
        }
    },
    'upload': {
        imageExtensions: ['jpe', 'jpeg', 'jpg', 'gif', 'png'],
        allowedExtensions: ['jpe', 'jpeg', 'jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'xls', 'xlsx', 'mp3'],
        serverURL: '/ArquivoDigital/FileUpload',         
        iconColor: '#cccccc',        
        'fileIconExtensions':  {
            'jpe': { className: 'fa fa-file-image-o', color: '#cccccc' },
            'jpeg': { className: 'fa fa-file-image-o', color: '#cccccc' },
            'jpg': { className: 'fa fa-file-image-o', color: '#cccccc' },
            'gif': { className: 'fa fa-file-image-o', color: '#cccccc' },
            'png': { className: 'fa fa-file-image-o', color: '#cccccc' },
            'txt': { className: 'fa fa-file-o', color: '#cccccc' },
            'csv': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'pdf': { className: 'fa fa-file-pdf-o', color: '#d9534f' },
            'doc': { className: 'fa fa-file-word', color: '#0277bd' },
            'docx': { className: 'fa fa-file-word', color: '#0277bd' },
            'dotx': { className: 'fa fa-file-word', color: '#0277bd' },
            'docm': { className: 'fa fa-file-word', color: '#0277bd' },
            'dotm': { className: 'fa fa-file-word', color: '#0277bd' },
            'xls': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xlsx': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xltx': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xlsm': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xltm': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xlam': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'xlsb': { className: 'fa fa-file-excel-o', color: '#5cb85c' },
            'ppt': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'pptx': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'potx': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'ppsx': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'ppam': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'pptm': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'potm': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'ppsm': { className: 'fa fa-file-powerpoint-o', color: '#d9534f' },
            'zip': { className: 'fa fa-file-archive-o', color: '#B02520' },
            'rar': { className: 'fa fa-file-archive-o', color: '#B02520' },
            'gzip': { className: 'fa fa-file-archive-o', color: '#B02520' },
            'dmg': { className: 'fa fa-file-archive-o', color: '#B02520' },
            'default': { className: 'fa fa-file-o', color: '#cccccc' }
        }
    },    
    'validate': {
        required: { required: true }
    },  
    'mask': {
        'cep': '99999-999',
        'cpf': '999.999.999-99',
        'cnpj': '99.999.999/9999-99',
        //'coordenada': '[-]99[9].999999, [-][9]99.999999',
        'date': { alias: 'date', placeholder: "dd/mm/aaaa" },
        'datetime': 'datetime',
        'integer': 'integer',
        'phone': 'phone',
        'money': 'money',
        'moneyoptions': { groupSeparator: '.', groupSize: 3, radixPoint: ',', autoGroup: true, digits: 2 },
        'coordenada': { groupSeparator: '.', groupSize: 3, radixPoint: ',', autoGroup: true, digits: 8 },
        'percentage': '99,99',
        'cartaocredito': '9999 9999 9999 9999'
    },
    'mascaraUnidadeTipo': {
        'padrao': { CasasDecimais: 2, Sigla: '', Descricao: 'Valor' },
        'moedaPadrao': [{ CasasDecimais: 2, Sigla: 'R$', Descricao: 'Reais', IdUnidade: 0, DescricaoUnidadeTipo: 'Valor' }]
    },
    'wysiwyg': {
        'text': {
            // codeview has a bug: https://github.com/summernote/summernote/pull/1267
            lang: 'pt-BR',
            height: '150px',
            toolbar: [
                ['style', ['style']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['link', 'picture']], // no insert buttons    
                ['view', ['fullscreen'/*, 'codeview'*/]]
            ]
        },
        'all': {
            lang: 'pt-BR',
            toolbar: [
                ['style', ['style']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'link']], // no insert buttons
                ['table', ['table']], // no table button
                ['view', ['fullscreen'/*, 'codeview'*/]],
                ['help', ['help']] //no help button
            ]
        }
    },
    'flipper': {
        ativoInativo: {
            'onText': 'Sim',
            'offText': 'Não',
            'css': 'secondary'
        },
        equipamentoAtivoInativo: {
            'onText': 'Ativo',
            'offText': 'Inativo',
            'css': 'secondary'
        },
        caracterizacaoAnimalAtivoInativo: {
            'onText': 'Ativo',
            'offText': 'Inativo',
            'css': 'secondary'
        }
    },
    'toastr': {
        //closeButton: true,
        positionClass: 'toast-top-full-width',
        showMethod: "slideDown",
        hideMethod: "slideUp",
        timeOut: "3000",
        preventDuplicates: false,
        showDuration: "50",
        hideDuration: "50"
    },
    'qtip': {
        'validate': {
            style: {
                classes: 'qtip-red'
            },
            content: {
                text: 'Verifique esse campo'
            },
            position: {
                my: 'top left',  // Position my top left...
                at: 'bottom left', // at the bottom right of.
                text: 'aaa',
                viewport: $(window),
                adjust: {
                    mouse: 'flip'
                }
            },
            show: 'focus',
            hide: {
                leave: true,
                event: 'blur'
            }
        },
        'dropdown': {
        	style: {
        		classes: 'qtip-popover qtip-dropdown qtip-shadow'
        	},
        	content: {
        		button: false
        	},
        	position: {
        		my: 'left top',  // Position my top left...
        		at: 'middle right', // at the bottom right of.
        		viewport: $(window),
        		adjust: {
        			mouse: 'flip'
        		}
        	},
        	show: {
        		event: 'click',
        		solo: true
        	},
        	hide: {
        		event: 'unfocus',
        		leave: true,
        		fixed: true,
        		delay: 3000
        	}
        },
        'popover': {
            style: {
                classes: 'qtip-popover qtip-shadow'
            },
            content: {
                button: 'Fechar'
            },
            position: {
                my: 'left top',  // Position my top left...
                at: 'middle right', // at the bottom right of.
                viewport: $(window),
                adjust: {
                    mouse: 'flip'
                }
            },
            show: {
                event: 'click',
                solo: true
            },
            hide: {
                event: 'unfocus',
                leave: true,
                fixed: true,
                delay: 3000
            }
        },
        'popoverFixed': {
            style: {
                classes: 'qtip-popover qtip-fixed qtip-shadow qtip-zindex'
            },
            position: {
                my: 'left top',  // Position my top left...
                at: 'middle right', // at the bottom right of.
                viewport: $(window),
                adjust: {
                    mouse: 'flip'
                }
            },
            content: {                
                button: 'Fechar'
            },
            show: {
                ready: 'true',
                solo: true
            },
            hide: {               
                fixed: true,
                leave: true,
                delay: 3000
            }
        },
        'title': {
            style: {
                classes: 'qtip-tipsy'
            },
            show: {                
                solo: false,
                event: 'focus mouseover'
            },
            position: {
                viewport: $(window),
                my: 'bottom center',  // Position my top left...
                at: 'top center', // at the bottom right of.
                adjust: {
                    mouse: 'shift'
                }
            },
            hide: {
                event: 'mouseleave blur',
                distance: 15
            }

        },
        'sliderVertical': {
            position: {
                my: 'right middle',  // Position my top left...
                at: 'middle left', // at the bottom right of.
                effect: false
            },
            style: {
                classes: 'qtip-tipsy qtip-slider-vertical'
            },
            show: 'focus keyup',
            hide: 'click unfocus'
        },
        'sliderHorizontal': {
            position: {
                my: 'top center',
                at: 'bottom center',
                effect: false
            },
            show: 'click focus keyup',
            style: {
                classes: 'qtip-tipsy qtip-slider-horizontal'
            },
            hide: 'click unfocus'
        }
    },
    'summernote': {
        local: {
            ptbr: {
                font: {
                    bold: 'Negrito',
                    italic: 'Itálico',
                    underline: 'Sublinhado',
                    strikethrough: 'Riscado',
                    clear: 'Remover estilo da fonte',
                    height: 'Altura da linha',
                    size: 'Tamanho da fonte'
                },
                image: {
                    image: 'Imagem',
                    insert: 'Inserir imagem',
                    resizeFull: 'Resize Full',
                    resizeHalf: 'Resize Half',
                    resizeQuarter: 'Resize Quarter',
                    floatLeft: 'Float Left',
                    floatRight: 'Float Right',
                    floatNone: 'Float None',
                    dragImageHere: 'Arraste uma imagem para cá',
                    selectFromFiles: 'Selecione a partir dos arquivos',
                    url: 'URL da image'
                },
                link: {
                    link: 'Link',
                    insert: 'Inserir link',
                    unlink: 'Remover link',
                    edit: 'Editar',
                    textToDisplay: 'Texto para exibir',
                    url: 'Para qual URL esse link leva?',
                    openInNewWindow: 'Abrir em uma nova janela'
                },
                video: {
                    video: 'Vídeo',
                    videoLink: 'Link para vídeo',
                    insert: 'Inserir vídeo',
                    url: 'URL do vídeo?',
                    providers: '(YouTube, Vimeo, Vine, Instagram, DailyMotion, ou Youku)'
                },
                table: {
                    table: 'Tabela'
                },
                hr: {
                    insert: 'Inserir linha horizontal'
                },
                style: {
                    style: 'Estilo',
                    normal: 'Normal',
                    blockquote: 'Citação',
                    pre: 'Código',
                    h1: 'Título 1',
                    h2: 'Título 2',
                    h3: 'Título 3',
                    h4: 'Título 4',
                    h5: 'Título 5',
                    h6: 'Título 6'
                },
                lists: {
                    unordered: 'Lista com marcadores',
                    ordered: 'Lista numerada'
                },
                options: {
                    help: 'Ajuda',
                    fullscreen: 'Tela cheia',
                    codeview: 'Ver código-fonte'
                },
                paragraph: {
                    paragraph: 'Parágrafo',
                    outdent: 'Menor tabulação',
                    indent: 'Maior tabulação',
                    left: 'Alinhar à esquerda',
                    center: 'Alinhar ao centro',
                    right: 'Alinha à direita',
                    justify: 'Justificado'
                },
                color: {
                    recent: 'Cor recente',
                    more: 'Mais cores',
                    background: 'Fundo',
                    foreground: 'Fonte',
                    transparent: 'Transparente',
                    setTransparent: 'Fundo transparente',
                    reset: 'Restaurar',
                    resetToDefault: 'Restaurar padrão'
                },
                shortcut: {
                    shortcuts: 'Atalhos do teclado',
                    close: 'Fechar',
                    textFormatting: 'Formatação de texto',
                    action: 'Ação',
                    paragraphFormatting: 'Formatação de parágrafo',
                    documentStyle: 'Estilo de documento'
                },
                history: {
                    undo: 'Desfazer',
                    redo: 'Refazer'
                }
            }
        }
    },
    'flot': {
        colors: ['#1C7BB3', '#30A94D', '#f8b324', '#ff5252', '#f945d5'],
        legend: {
            show: true,
            noColumns: 1, // number of colums in legend table
            labelFormatter: null, // fn: string -> string
            labelBoxBorderColor: '#ccc', // border color for the little label boxes
            container: null, // container (as jQuery object) to put legend in, null means default on top of graph
            position: 'ne', // position of default legend container within plot
            margin: 5, // distance from grid edge to default legend container within plot
            backgroundColor: null, // null means auto-detect
            backgroundOpacity: 0 // set to 0 to avoid background           
        },
        xaxis: {
            font: null, // null (derived from CSS in placeholder) or object like { size: 11, lineHeight: 13, style: 'italic', weight: 'bold', family: 'sans-serif', variant: 'small-caps' }
            color: null, // base color, labels, ticks
            tickColor: null, // possibly different color of ticks, e.g. 'rgba(0,0,0,0.15)'            
            tickFormatter: null, // fn: number -> string
            labelWidth: null, // size of tick labels in pixels
            labelHeight: null,
            ticks: null
        },
        yaxis: {
            autoscaleMargin: 0.02,
            position: 'left' // or 'right'
        },
        series: {
            points: {
                radius: 3,
                lineWidth: 2, // in pixels
                fill: true,
                fillColor: '#ffffff',
                symbol: 'circle' // or callback
            },
            lines: {
                lineWidth: 0, // in pixels
                fill: false,
                fillColor: null,
                steps: false
            },
            bars: {
                lineWidth: 2, // in pixels
                barWidth: 1, // in units of the x axis
                fill: true,
                fillColor: null,
                align: 'left', // 'left', 'right', or 'center'
                horizontal: false,
                zero: true
            },
            shadowSize: 0,
            highlightColor: null
        },
        grid: {
            show: true,
            aboveData: false,
            color: '#545454', // primary color used for outline and labels
            backgroundColor: null, // null for transparent, else color
            borderColor: null, // set if different from the grid color
            tickColor: null, // color for the ticks, e.g. 'rgba(0,0,0,0.15)'
            margin: 0, // distance from the canvas edge to the grid
            labelMargin: 5, // in pixels
            axisMargin: 8, // in pixels
            borderWidth: 0, // in pixels
            minBorderMargin: null, // in pixels, null means taken from points radius            
            markingsColor: '#f4f4f4',
            markingsLineWidth: 2,
            // interactive stuff
            clickable: true,
            hoverable: true,
            autoHighlight: true, // highlight in case mouse is near
            mouseActiveRadius: 10 // how far the mouse can be away to activate an item
        }
    },
    'plot': {
        'transparent': {
            animate: true,
            // Will animate plot on calls to plot1.replot({resetAxes:true})
            //animateReplot: true,
            //stackSeries: true,
            series: [
                {
                    color: '#5FAB78',
                    shadow: false,
                    markerOptions: {
                        shadow: false
                    }
                }
            ],
            grid: {
                drawBorder: false,
                shadow: false,
                background: 'transparent',
                textColor: '#FFFFFF',
                gridLineColor: '#FFFFFF'
            },
            axes: {
                textColor: '#FFFFFF',
                shadow: false,
                xaxis: {
                    tickOptions: {
                        textColor: '#FFFFFF'
                    }
                }
            }
        }
    },
    'jstree': {
        'nodeCategoria': {
            type: 'folder',
            text: 'Nova categoria'
        },
        'contextmenu': {
            'create': {
                'label': 'Criar categoria',
                'action': function (data) {
                    var ref = $.jstree.reference(data.reference), node = ref.get_node(data.reference);                  
                    ref.create_node(node, defaults.jstree.nodeCategoria, 'first', function (new_node) {
                        ref.edit(new_node);
                    });
                }
            },
            'rename': {
                'label': 'Renomear',
                '_disabled': function (data) {
                    var inst = $.jstree.reference(data.reference),
                        obj = inst.get_node(data.reference);                   
                    return obj.type !== 'folder';
                }
            },
            'remove': {
                'label': 'Remover'
            },
            'ccp': {
                'label': 'Editar',
                'action': false,
                'submenu': {
                    'cut': {
                        'label': 'Cortar'
                    },
                    'copy': {
                        'label': 'Copiar'
                    },
                    'paste': {
                        'label': 'Colar'
                    }
                }
            }
        }
    }
};

// Set global defaults

// datepicker
$.fn.datetimepicker.defaults = $.extend(true, $.fn.datetimepicker.defaults, defaults.datepicker.defaultValues);

// modal
$.fn.modal.Constructor.DEFAULTS.backdrop = 'static';
$.fn.modal.Constructor.DEFAULTS.keyboard = true;

// toastr
if (typeof toastr !== "undefined") {    
    toastr.options = defaults.toastr;
} else {
    require(['toastr'], function (toastr) {
        toastr.options = defaults.toastr;
        return toastr;
    });
}

// bootbox 
if (typeof bootbox !== "undefined") {
    bootbox.setDefaults({
        locale: "br",
        onEscape: true
    });
} else {
    require(['bootbox'], function (bootbox) {
        bootbox.setDefaults({
            locale: "br",
            onEscape: true
        });
        return bootbox;
    });    
}


// wysisyg
$.extend($.summernote.lang, defaults.summernote.local.ptbr);

if (!String.format) {
    String.format = function (format) {
        var args = Array.prototype.slice.call(arguments, 1);
        return format.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };
}


