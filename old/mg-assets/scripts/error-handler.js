﻿define(['knockout', 'jquery'], function (ko, $) {
    "use strict";

    var ErrorHandlingBindingProvider;
    var logError = function (err, additionalInfo, showErrorMessage) {
        showErrorMessage = (showErrorMessage === undefined ? true : showErrorMessage);
        //debugger;
        // hide all containers
        if (showErrorMessage) {            
            document.body.classList.add('loading-fail');
            document.getElementById('route-containers').getElementsByTagName('div')[0].style.display = 'none';
            document.getElementById('message-error-client').style.display = 'block';
        }
        // send log
        console.log(err, additionalInfo);
    };

    ErrorHandlingBindingProvider = function () {
        var orig;
        orig = new ko.bindingProvider();
        this.nodeHasBindings = orig.nodeHasBindings;
        this.getBindings = function (node, bindingContext) {
            var bindings_map, err, error, errMessage;
            try {
                bindings_map = orig.getBindings(node, bindingContext);
            } catch (error) {
                err = error;
                errMessage = ("Binding error: " + err.message + "\nNode:" + node + "\nContext:" + ko.contextFor(node));

                logError(errMessage, bindingContext, false);
                if (console && console.error)
                    console.error(errMessage);

                if (isDebugging) {
                    $(node).addClass("binding-error");
                } else {

                }
            }
            //console.log(bindings_map);
            if (bindings_map === null) {
                return null;
            }
            //console.log(ko.bindingHandlers);
            //if (!_(bindings_map).keys().any(function (m) {
            //  return _.has(ko.bindingHandlers, m);
            //}
            //    )) {
            //    console.error("No bindings found:", bindings_map, "Node:", node);
            //    $(node).addClass("binding-error");
            //    return null;
            //}
            return bindings_map;
        };
    };

    ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

    return logError;

});