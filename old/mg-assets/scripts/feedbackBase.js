﻿define(['jquery', 'toastr', 'util'], function ($, toastr, mg) {
    "use strict";

    var mgFeedbackBase = {

        feedbackCrudRoute: function (result, successMessage, redirectToList, callback) {           
            // TODO: Ao utilizar rota customizada, ao salvar com sucesso, retornar para a rota definida nas configurações
            var route = Route.currentRoute,
                listUrl = '/#/' + route.params.modulo + '/' + route.params.area + '/List' + (route.params.hasOwnProperty('parentId') ? '/' + route.params.parentId : ''),
                isPanelContext = $('html').hasClass('has-panel');

            if (Route.routeOptions.hasOwnProperty('routeBack')) {
            	var routeBack = Route.routeOptions.routeBack;
            	var currentIndex = Route.routeOptions.routeBack.length - 1; // será sempre o último indice
            	if (routeBack.length > 0 && routeBack[currentIndex].hasOwnProperty('feedBackRouteIndex')) {
            		// customiza rota de volta em ações de salvar/concluir processo
            		// o índice definido em 'feedBackRouteIndex' deve ser dos valores definidos em routeBack, veja em console.log(routeBack)
                	var routeIndex = routeBack[currentIndex].feedBackRouteIndex;
                	listUrl = routeBack[routeIndex].url;
            	}
            };

            if (typeof (redirectToList) === 'undefined')
                redirectToList = true;

            if (!successMessage || successMessage == '') {
                successMessage = 'Operação realizada com sucesso.';
            }

            if ((result.Response && result.Response.Sucesso) || (result.Sucesso && result.Sucesso == true)) {

                toastr.success(successMessage);

                if (redirectToList) {
                    if (isPanelContext) {
                        if (window.hasOwnProperty('PanelSet')) {
                            PanelSet.clearActivePanel();
                            PanelSet.closeActivePanel();
                        }
                    } else {
                        window.location = listUrl;
                    }
                }

                if (typeof callback === 'function') {
                    callback();
                }

            }
            else {
                var options = {
                    messageTitle: 'Erro na operação',
                    messageBody: 'Ocorreu um erro na operação',
                    keyboard: true,
                    buttons: [
                        { text: 'Fechar', css: 'btn-default', icon: 'fa-times', behavior: function () { } }
                    ]
                }

                if (result.Response && result.Response.MensagemFinal && result.Response.Identificador) {

                    options.messageBody = result.Response.MensagemFinal;

                    if (result.Response.Identificador == mg.DTOResponseBase.Identificador.RegistroObsoleto) {
                        options.buttons = [
                                { text: 'Voltar', css: 'btn-primary btn-inverted', icon: 'fa-arrow-left', behavior: function () { window.location = listUrl; } },
                                { text: 'Recarregar', css: 'btn-default', icon: 'fa-rotate-right', behavior: function () { window.location.reload(); } }
                        ];
                    }
                    else if (result.Response.Identificador == mg.DTOResponseBase.Identificador.RegistroDuplicado) {
                        options.buttons = [
                            { text: 'Fechar', css: 'btn-default', icon: 'fa-times', behavior: function () { } }
                        ];
                    }
                }

                mgFeedbackBase.Modal(options);
            }
        },

        feedbackCrud: function (result, successMessage, redirectToList, callback) {

            if (typeof (redirectToList) === 'undefined')
                redirectToList = true;
            else
                redirectToList = false;

            if (!successMessage || successMessage == '') {
                successMessage = 'Operação realizada com sucesso.';
            }
            //if ((result.Response && result.Response.Sucesso) || (result.sucesso || "success")) {
            if (result.Response && result.Response.Sucesso) {

                toastr.success(successMessage);

                if (redirectToList)
                    window.location.hash = '#list';

                if (typeof callback === 'function') {
                    callback();
                }

            }
            else {
                var options = {
                    messageTitle: 'Erro na operação',
                    messageBody: result.Response.MensagemFinal,
                    keyboard: true
                }

                if (result.Response.Identificador == mg.DTOResponseBase.Identificador.RegistroObsoleto) {
                    options.buttons = [
                            { text: 'Voltar', css: 'btn-primary btn-inverted', icon: 'fa-arrow-left', behavior: function () { window.location.hash = '#List'; } },
                            { text: 'Recarregar', css: 'btn-default', icon: 'fa-rotate-right', behavior: function () { window.location.reload(); } }
                    ];
                }
                else if (result.Response.Identificador == mg.DTOResponseBase.Identificador.RegistroDuplicado) {
                    options.buttons = [
                        { text: 'Fechar', css: 'btn-default', icon: 'fa-times', behavior: function () { } }
                    ];
                } else if (result.Response.Identificador == mg.DTOResponseBase.Identificador.FalhaValidacao) {
                	options.buttons = [
                        { text: 'Fechar', css: 'btn-default', icon: 'fa-times', behavior: function () { } }
                	];
                	options.messageTitle = 'Falha na validação das informações';
                }
            }

            mgFeedbackBase.Modal(options);
        },
        Modal: function (options) {
            var $modal = $('<div class="modal fade modal-warning" tabindex="-1">' +
                '    <div class="modal-dialog">' +
                '        <div class="modal-content">' +
                '            <div class="modal-header">' +
                '                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>' +
                '                <h4 class="modal-title">' + options.messageTitle + '</h4>' +
                '            </div>' +
                '            <div class="modal-body">' + options.messageBody +
                '            </div>' +
                '            <div class="modal-footer">' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>');

            var buttonTemplate = '<button type="button" class="btn btn-lg pull-left #css#"><i class="fa #icon#"></i> #text#</button>';

            $(options.buttons).each(function (i, btn) {
                var btnToAppend = $(buttonTemplate.replace('#css#', btn.css).replace('#icon#', btn.icon).replace('#text#', btn.text));

                btnToAppend.on('click', function (e) {
                    e.preventDefault();
                    btn.behavior();
                    $modal.modal('hide');
                });

                btnToAppend.appendTo($modal.find('.modal-footer'));
            });

            $modal.modal();

            $modal.on('hidden.bs.modal', function () {
                $modal.remove();
            });
        }
    };
    return mgFeedbackBase;
});