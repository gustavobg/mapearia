﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'defaults', 'jquery-qtip'], factory);
    } else {
        // Browser globals
        root.mgHelp = factory(root.jQuery, root.defaults);
    }
}(this, function ($, defaults) {

    "use strict";

    var mgHelp = {
        IdTela: 0,
        Fields: [],

        Init: function () {
            var self = this,
                i = 0,
                campos = mgHelp.Fields,
                length = campos.length;

            if (length == 0)
                return;

            for (i; i < length;) {

                var campo = campos[i],
                    elem = $('#' + campo.Campo);

                if (!elem.length)
                    return;

                var trigger = elem.parents('.form-group').stop().addClass('has-help popover-trigger').find('label').stop().click(function (e) { e.preventDefault(); }),
                    fieldId = elem.attr('id');
                
                if (!trigger.data('hasqtip')) {
                    //console.log('binding qtip', campo);
                    trigger.qtip($.extend(true, defaults.qtip.popover, {
                        content: {
                            text: campo.CorpoAjuda,
                            title: campo.TituloAjuda
                        }
                    }));
                }                
                i = i + 1;
            }
        },
        // [obsoleto]
        GenerateHelp: function () {
            var self = this;

            if (mgHelp.Fields.length == 0)
                return;

            $(mgHelp.Fields).each(function (idx, item) {
                var elem = $('#' + item);

                if (!elem.length)
                    return;

                var trigger = elem.parents('.form-group').stop().addClass('has-help popover-trigger').find('label').stop().click(function (e) { e.preventDefault(); }),
                    fieldId = elem.attr('id');

                if (!trigger.data('mghelp-help-text-loaded')) {
                    trigger.qtip($.extend(true, defaults.qtip.popover, {
                        content: {
                            text: function (event, api) {
                                $.post('/Configuracao/TelaCampo/List', JSON.stringify({ IdTela: mgHelp.IdTela, Campo: fieldId }))
                                    .done(function (result) {
                                        trigger.data('mghelp-help-text-loaded', 'true');
                                        var r = result.Data[0];
                                        api.set('content.text', r.CorpoAjuda);
                                        api.set('content.title', r.TituloAjuda);
                                    })
                                    .error(function () {
                                        api.toggle(false);
                                    });
                                return 'Carregando ajuda...';
                            }
                        }
                    }));
                }
            });

        }
    };
    window.mgHelp = mgHelp;

    return mgHelp;

}));

//TODO: API's, events, people, chat, peopleGroup, help

//var db = new ydn.db.Storage('help');

// initial data input
/*
$.get('data/help.json', function(data){
    db.put({name: 'help', keyPath: 'fieldId', indexes: [ { name: 'pageId' } ]}, data);
});

function generateHelp(pageId) {
    var key_range = ydn.db.KeyRange.only(pageId);
    var req = db.values(new ydn.db.IndexValueIterator('help', 'pageId', key_range), 10);
    req.then(function(records) {
        var i = 0, length = records.length, r = null, elem = null, trigger = null;
        for (i; i <= length;) {
            setTip(records[i]);
            i = i + 1;
        }
    }, function(e) {
            throw e;
        }
    );
}
*/

/*function setTip(r) {
    var elem = $(document.getElementById(r.fieldId));

    if (!elem.length)
        return;
    var trigger = elem.parents('.form-group').stop().addClass('has-help popover-trigger').find('label').stop().click(function (e) { e.preventDefault(); });

    if (!trigger.data('mghelp-help-text-loaded')) {
        trigger.qtip($.extend(defaults.qtip.popover, {
            content: {
                text: function (event, api) {
                    trigger.data('mghelp-help-text-loaded', 'true');
                    api.set('content.text', r.text);
                    api.set('content.title', r.title);

                    return 'Carregando ajuda...';
                }
            }
        }));
    }
}*/

//db.put('pageId', 20);

//var mgHelp = {
//	IdTela: 0,
//	Fields: [],
//	GenerateHelp: function () {
//	    var self = this;
//
//		if (mgHelp.Fields.length == 0)
//			return;
//
//		$(mgHelp.Fields).each(function (idx, item) {
//			var elem = $('#' + item);
//
//			if (!elem.length)
//			    return;
//
//		    var trigger = elem.parents('.form-group').stop().addClass('has-help popover-trigger').find('label').stop().click(function (e) { e.preventDefault(); }),
//			    fieldId = elem.attr('id');
//
//		    if (!trigger.data('mghelp-help-text-loaded')) {
//		        trigger.qtip($.extend(defaults.qtip.popover, {
//		            content: {
//		                text: function (event, api) {
//		                    $.post('data/help.json', JSON.stringify({ IdTela: mgHelp.IdTela, Campo: fieldId }))
//                                .done(function (result) {
//		                            trigger.data('mghelp-help-text-loaded', 'true');
//		                            var r = result.Data[0];
//		                            console.log(api);
//		                            api.set('content.text', r.CorpoAjuda);
//		                            api.set('content.title', r.TituloAjuda);
//                                })
//		                        .error(function () {
//		                            api.toggle(false);
//		                        });
//		                    return 'Carregando ajuda...';
//		                }
//		            }
//		        }));
//		    }
//		});
//
//	}
//};