﻿define(['jquery', 'jquery-qtip'], function ($) {
	$('body').on('mouseover focus', '[title]', function () {
		// qTip Perfomance optimization
		var $this = $(this);
		if (!$this.data('qtip-attached') && $this.not('.select2-offscreen') && $this.not('.no-tooltip')) {
			$this.qtip(defaults.qtip.title);
			$this.data('qtip-attached', true);
			// the qtip handler for the event may not be called since we just added it, so you   
			// may have to trigger it manually the first time.
			$this.trigger('mouseover');
		}
	});
});

