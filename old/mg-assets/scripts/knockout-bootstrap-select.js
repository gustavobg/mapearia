﻿define(['jquery', 'knockout', 'text!../templates/select-checkbox.html', 'bootstrap-select/plugin'], function ($, ko, templateSelectCheckbox) {
    
    'use strict';

    var jsonify = (function (div) {
        return function (json) {
            div.setAttribute('onclick', 'this.__json__ = ' + json);
            div.click();
            return div.__json__;
        }
    })(document.createElement('div'));

    var bootstrapSelect = function (params, componentInfo) {       
        var element = componentInfo.element,
            self = this,
            i = 0;

        params = ko.unwrap(params);

        var itens = ko.observableArray(null);
        var itensSelecionados = params.campoId;       
        var itensSelecionadosArr = ko.observableArray([]);      
        var listarTodosItems = ko.observable(null); // trigger de listagem de itens
        var selectOptions = params.select || {};
        var vmDataSelected = params.vmDataSelected || null;
        var vmTextSelected = params.vmTextSelected || ko.observable(''); // valor de texto separado por vírgulas: 122, 111, 13

        ko.computed(function () {           
            var itensValor = ko.unwrap(itensSelecionados);
            if (itensValor === null || itensValor === "" || itensValor === 0 || (itensValor.hasOwnProperty('length') && itensValor.length === 0)) {
                itensSelecionadosArr([]);
            } else {
                itensValor = itensValor.replace(/\s/gi, ''); // remove espaços
                itensSelecionadosArr(itensValor.split(','));
            }
        });

        itensSelecionadosArr.subscribe(function (value) {
            itensSelecionados(value.length === 0 ? '' : value.join(','));
        });

        var selectDefaultOptions = {
            liveSearch: false,
            maxOptions: false,
            placeholder: '',
            selectedTextFormat: 'count > 2',
            style: '',
            //width: 'fit',
            actionsBox: false,
            noneSelectedText: '',
            iconBase: 'material-icons',
            selectAllText: 'Todos',
            deselectAllText: 'Nenhum',
            tickIcon: 'done',
            countSelectedText: function (numSelected, numTotal) {
                return (numSelected == 1) ? "<span class='badge'>{0}</span> item selecionado" : "<span class='badge'>{0}</span> itens selecionados";
            },
            showSubtext: false
        };
        
        selectOptions = $.extend(true, selectDefaultOptions, selectOptions);
        
        // items, pode ser estático, observável ou utilizado url para pegar informação
        ko.computed(function () {

            // item estático ou observável
            if (params.hasOwnProperty('itens')) {
                itens(ko.unwrap(params.itens));

            } else if (params.hasOwnProperty('url')) {

                // items recuperados por URL
                var o = params;
                var isInitial = ko.computedContext.isInitial();

                // caso já existam valores nos itens selecionados, o ajax irá buscar somente aqueles campos para exibição imediata das informações, utilizando o 'campoIdPesquisa'
                // por padrão, ao clicar no select para exibir o dropdown, irá trazer todos os registros.

                $.ajax({
                    url: o.url,
                    quietMillis: 300,
                    dataType: 'json',
                    type: 'POST',
                    data: function () {                       
                        // usado somente para disparar a listagem de todos os itens pelo componente
                        listarTodosItems();

                        var defaults = {
                            pageSize: 400, // máximo de itens na lista de seleção
                            currentPage: 1,
                            dadosCompl: '',
                            dadosComplObservable: null,
                            idControleDependencia: ''
                        };

                        o = $.extend(true, defaults, o);
                
                        var json = {
                            Page: {
                                PageSize: 400, CurrentPage: 1,
                                OrderBy: o.campoOrdenacao
                            }
                        };

                        if (o.idControleDependencia.length > 0) {
                            var elementoIdControleDependencia = $('#' + o.idControleDependencia);
                            if (o.idDependencia.length === 0)
                                console.error('Bootstrap Select: Necessário informar o parâmetro idDependencia');
                            else if (elementoIdControleDependencia.length === 0) {
                                console.error('Bootstrap Select: Controle de dependência não encontrado');
                            } else {
                                $.extend(json, jsonify('{' + o.idDependencia + ':' + "'" + $('#' + o.idControleDependencia).val() + "'" + '}'));
                            }
                        }
                        if (itensSelecionadosArr.length > 0 && isInitial && o.hasOwnProperty('campoIdPesquisa')) {
                            // verifica se é a primeira requisição, e se existem valores selecionados
                            // irá retornar somente os resultados selecionados para melhor perfomance.
                            // carregamento parcial na primeira requisição (geralmente edição ou listagens de informações)
                            var campoIdPesquisa = ko.unwrap(o.campoIdPesquisa); // In
                            // itensSelecionadosValor ou campoId pode ter os formatos: '10, 1, 5' ou ['10','1', '5']
                            json[o.campoIdPesquisa] = itensSelecionadosArr.join(',');

                        } else {
                            // dados complementares do select
                            if (o.dadosCompl.length > 0) {
                                $.extend(json, jsonify('{' + o.dadosCompl + '}'));
                            }
                            // dados complementares observável do select
                            if (ko.isObservable(o.dadosComplObservable)) {
                                $.extend(json, ko.utils.unwrapObservable(o.dadosComplObservable));
                            }
                        }

                        return JSON.stringify(json);
                    }()
                }).done(function (result) {
                    // resultado do select
                    //var more = result.Page.CurrentPage < result.Page.TotalPages;
                    var convert,
                        retornoSelect = [],
                        i = 0;

                    if (result.hasOwnProperty('Data')) {
                        for (i; i < result.Data.length; i++) {
                            convert = result.Data[i];
                            convert.id = o.campoIdChaveRelacao ? convert[o.campoIdChaveRelacao] : convert[o.campoId];
                            convert.text = function (e) { return e[o.campoValor]; }(convert);
                            if (selectOptions.showSubtext) {
                                convert.subtext = o.hasOwnProperty('campoValorSub') && o.campoValorSub.length > 0 ? convert[o.campoValorSub] : '';
                            }
                            // dados concisos somente para exibição no componente
                            retornoSelect.push(convert);
                        }                        
                    } else {
                        console.error('Bootstrap Select: O parâmetro de retorno "Data" é esperado para recuperar os itens para seleção')
                    }
                    itens(retornoSelect);
                    //return { results: result.Data, more: more };
                });
            } else {
                console.error('Bootstrap Select: Forneça parâmetro de itens ou URL para os dados');
            }
        });     

        var itensRender = function () {           
        };

        return {
            itens: itens,
            itensRender: itensRender,
            itensSelecionados: itensSelecionados,
            itensSelecionadosArr: itensSelecionadosArr,
            listarTodosItems: listarTodosItems,
            selectOptions: selectOptions,
            vmDataSelected: vmDataSelected,
            vmTextSelected: vmTextSelected
        };
    };

    ko.components.register('select-checkbox', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                return new bootstrapSelect(params, componentInfo);
            }
        },
        template: templateSelectCheckbox
    });


    ko.bindingHandlers.bootstrapSelect = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var jsonify = (function (div) {
                return function (json) {
                    div.setAttribute('onclick', 'this.__json__ = ' + json);
                    div.click();
                    return div.__json__;
                }
            })(document.createElement('div'));

            var bootstrapSelect = function () {             
               
                var self = this;                
                var options = viewModel.selectOptions;
                var $element = $(element);

                var itensSelecionados = viewModel.itensSelecionados;
                var itensSelecionadosArr = viewModel.itensSelecionadosArr;
                var todosItensCarregados = false;
                
                $element.selectpicker(options).on('changed.bs.select', function (e, index, newValue, oldValue) {                   
                    var value = e.target.getElementsByTagName('option')[index].value;
                    if (newValue === true) {                       
                        itensSelecionadosArr.push(value)
                    } else {
                        itensSelecionadosArr.remove(value)
                    }
                }).on('show.bs.select', function () {
                    viewModel.listarTodosItems.valueHasMutated();
                    todosItensCarregados = true;
                });
            };

            return new bootstrapSelect();

        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var $element = $(element);           
            var itens = ko.unwrap(viewModel.itens);
            var itensSelecionados = ko.unwrap(viewModel.itensSelecionadosArr);
            var options = viewModel.selectOptions;

            $element.selectpicker('val', itensSelecionados);
            $element.selectpicker('refresh');           

            // percorre todos os itens e recupera textos e dados selecionados
            if (ko.isObservable(viewModel.vmDataSelected) || ko.isObservable(viewModel.vmTextSelected)) {
                var dataSelectedArr = [],
                    textSelectedArr = [],
                    itensSelecionadosLength = itensSelecionados.length,
                    i = 0;

                for (i; i < itens.length;) {
                    var item = itens[i];
                    if ($.inArray(item.id + "", itensSelecionados) > -1) {
                        dataSelectedArr.push(item);
                        textSelectedArr.push(item.text);
                    }
                    i = i + 1;
                }
                if (ko.isObservable(viewModel.vmDataSelected))
                    viewModel.vmDataSelected(dataSelectedArr);
                if (ko.isObservable(viewModel.vmTextSelected))
                    viewModel.vmTextSelected(textSelectedArr.join(', '));
            }
        }
    };

});

