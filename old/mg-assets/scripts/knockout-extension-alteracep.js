﻿define(['jquery', 'knockout', 'toastr'], function ($, ko, toastr) {
    ko.bindingHandlers.alteraCEP = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var element = $(element),
                vm = viewModel;

            element.on('change', function () {
                var cep = element.val();

                if (cep.length > 8) {
                    $.ajax({
                        type: "POST",
                        data: { CEP: cep },
                        url: "/Localizacao/HTMCEP/BuscaCEP",
                        dataType: "json",
                        success: function (result) {
                            if (result.Data != '') {
                                var json = JSON.parse(JSON.stringify(result.Data[0]));

                                vm.IdLocalidade(json.IdLocalidade);
                                vm.IdTipoLogradouro(json.IdTipoLogradouro);
                                vm.IdLogradouro(json.IdLogradouro);
                                vm.IdBairro(json.IdBairro);
                                vm.IdEstado(json.IdEstado);
                                vm.Localidade(json.NomeLocalidade);


                                if (!json.LocalidadeComLogradouroUnico) {
                                    vm.Logradouro(json.NomeLogradouro);
                                    vm.Bairro(json.NomeBairro);
                                }
                                else {
                                    vm.Logradouro(null);
                                    vm.Bairro(null);
                                }

                                $("#txtLocalidade").val(json.NomeLocalidade);
                                $("#ddlEstado").trigger("change");
                                $("#ddlLocalidade").trigger("change");
                                $("#ddlTipoLogradouro").trigger("change");
                            }
                            else
                                toastr.error("CEP não encontrado!");
                        },
                        beforeSend: function () {

                        },
                        complete: function (msg) {

                        }
                    });
                }
            });
        },
        update: function (element, valueAccessor) { }
    };
});
