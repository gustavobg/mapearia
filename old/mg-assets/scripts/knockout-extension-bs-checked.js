﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'bootstrap/button'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    //Foi implementado este handler devido ao Toggle do Bootstrap buttons (bsChecked)
    ko.bindingHandlers.bsChecked = {
        init: function (element, valueAccessor, allBindingsAccessor,
        viewModel, bindingContext) {
            var value = valueAccessor();
            var newValueAccessor = function () {
                return {
                    change: function () {
                        value(element.value);
                    }
                }
            };
            ko.bindingHandlers.event.init(element, newValueAccessor,
            allBindingsAccessor, viewModel, bindingContext);
        },
        update: function (element, valueAccessor, allBindingsAccessor,
        viewModel, bindingContext) {
            if ($(element).val() == ko.unwrap(valueAccessor())) {
                setTimeout(function () {
                    $(element).closest('.btn').button('toggle');
                }, 1);
            }
        }
    }
}));