﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.clearOnEsc = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {                       
          
            function clearValue() {
                var value = allBindings().hasOwnProperty('value') ? allBindings().value : (allBindings().hasOwnProperty('textInput') ? allBindings().textInput : $element.val());

                if (ko.isObservable(value)) {                    
                    var valueUnwrapped = ko.utils.unwrapObservable(value);                   
                    if (valueUnwrapped !== '' && valueUnwrapped != null)
                        value(null);
                } else {
                    return $element.val('');
                }
            };

            var $element = $(element);

            $element.on('keyup', function (e) {
                // 27 - Esc, 13 - Enter
                if (e.keyCode == 27) {                    
                    clearValue();
                }
            });            

            $element.wrap('<div class="input-clear-wrapper"></div>');

            var elementWrapper = $element.parent(),
                clearButton = $('<a class="input-clear-button" href="#" tabindex="-1"></a>').prependTo(elementWrapper);
            
            clearButton.on('click', function (e) {
                e.preventDefault();
                clearValue();
            });
        },
        update: function (element, valueAccessor) {}
    };

})); 