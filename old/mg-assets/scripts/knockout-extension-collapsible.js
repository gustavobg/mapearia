﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'util', 'velocity', 'jquery-collapsible/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko, root.mg, root.velocity);
    }
}(this, function ($, ko, mg, velocity) {

    ko.bindingHandlers.collapsible = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var options = valueAccessor().options || valueAccessor(),
                element = $(element);

            function getOptionsExtend(element) {
                if (element.hasClass('panel-filter')) {
                    var search = element.find('.search');

                    if (options.hasOwnProperty('open') && options.open == true)
                        search.hide();

                    element.collapsible($.extend(true, {
                        open: false,
                        useOptionalLabel: true,
                        scrollToElement: false,
                        beforeOpen: function (e, ui) {
                            // dentro do método do ui _trigger, evita duplicação de instâncias
                            search.velocity('slideUp', 100);
                        },
                        beforeClose: function (e, ui) {
                            search.velocity('slideDown', 100);
                        },
                        afterCreate: function (e, ui) {
                            var heading = element.find('.panel-heading');
                            heading.on('click', function (e) {
                                if (mg.hasClass(e.target, 'panel-heading')) {
                                    element.collapsible('toggle', e);
                                }
                            });

                        }
                    }, options));
                } else {
                    if (element.hasClass('optional')) {
                        options = $.extend(true, options, { headingClass: '> a', containerClass: '> div', useOptionalLabel: true });                       
                    }
                }
                return options;
            };

            if (options.hasOwnProperty('vmArray')) {
                var vmArray = ko.utils.unwrapObservable(options.vmArray),
                    i = 0, length = vmArray.length,
                    o = false, vm = null;
                for (i; i < length;) {
                    vm = ko.utils.unwrapObservable(vmArray[i]);
                    // campos personalizados
                    if (vm != null && vm.hasOwnProperty('CaracteristicaValor')) {
                        var valor = ko.utils.unwrapObservable(vm.CaracteristicaValor.Valor),
                            idCaracteristicaOpcao = ko.utils.unwrapObservable(vm.CaracteristicaValor.IdCaracteristicaOpcao);
                        if (valor != "" && valor != null)
                            o = true;
                        else if (idCaracteristicaOpcao != "" && idCaracteristicaOpcao != null)
                            o = true;
                    } else {
                        if (typeof (vm) === 'number') {
                            o = true;
                        } else if (vm != null && vm.length > 0)
                            o = true;
                    }
                    i = i + 1;
                };
                element.data('open', o);
            };
            
            options = $.extend(true, { useOptionalLabel: true, offsetTopElement: $('#aside-navbar') }, options);
            options = $.extend(true, options, getOptionsExtend(element));

            // todo: collapsible grid add
            //var panelBody = element.find('.panel-body');
            //var gridview = panelBody.find('grid-view');
            //if (gridview.length > 0) {
            //    var panelHeading = element.find('.panel-heading'),
            //        panelTitle = panelHeading.find('.panel-title'),
            //        panelTitleText = panelTitle.text(),
            //        panelTitleAdd = $('<h3 class="panel-title collapsible-heading"><button class="btn btn-secondary"><i class="material-icons material-md"></i> Adicionar ' + panelTitleText + '</button><span class="collapsible-optional-label">(Opcional)</span></h3>');

            //    // get gridview context                
            //    var gvApi = ko.unwrap(ko.contextFor(gridview[0].childNodes[0]).$data.api);
            //    var totalRecords = gvApi.totalRecords;
            //    var hide = { 'height': '0px', 'display': 'block', 'overflow': 'hidden', 'padding': 0 };
            //    var show = { 'height': 'auto', 'overflow': 'hidden', 'padding': '20px' };

            //    panelTitleAdd.appendTo(panelHeading);
            //    panelTitleAdd.find('button').on('click', function (e) {
            //        e.preventDefault();
            //        gvApi.newItem();
            //        panelBody.css(hide);
            //    });
                
            //    ko.computed(function () {
            //        var total = ko.unwrap(totalRecords);
            //        if (total > 0) {
            //            panelTitle.show();
            //            panelTitleAdd.hide();
            //            panelBody.css(show);
            //        } else {
            //            panelTitle.hide();
            //            panelTitleAdd.show();
            //            panelBody.css(hide);
            //        }
            //    })

            //}

            element.collapsible(options);

            ko.computed(function () {
                if (options.hasOwnProperty('useOptionalLabel') && ko.isObservable(options.useOptionalLabel)) {
                    var useOptionalLabel = !(ko.utils.unwrapObservable(options.useOptionalLabel));
                    if (useOptionalLabel) {
                        if (!ko.computedContext.isInitial())
                            element.data('open', true);
                        console.log('computed', options, element);
                        element.collapsible($.extend(true, options, getOptionsExtend(element)));
                    }
                    else {
                        element.collapsible('destroy');
                    }

                }
            });

        },
        update: function (element, valueAccessor) {
        }
    };
}));