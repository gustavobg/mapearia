﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'moment', 'daterangepicker/plugin', 'jquery-inputmask'], factory);
    } else {
        factory(root.jQuery, root.ko, root.moment);
    }
}(this, function ($, ko, moment) {

    ko.bindingHandlers.daterangepicker = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {            
            
            var daterangepicker = function () {
                
                var self = this,                    
                    isNullOrEmptyDate = function (v) {
                        return v == null || v == '' || v == 'Invalid date';
                    },                    
                    defaultOptions = {
                        autoUpdateInput: false,
                        alwaysShowCalendars: false,
                        buttonClasses: 'btn btn-sm',
                        applyClass: 'btn-primary btn-secondary',
                        locale: {
                            "format": "DD/MM/YYYY",
                            "separator": " até ",
                            "applyLabel": "Aplicar",
                            "cancelLabel": "Limpar",
                            "fromLabel": "De",
                            "toLabel": "Para",
                            "customRangeLabel": "Escolher período",
                            "daysOfWeek": [
                                "Dom",
                                "Seg",
                                "Ter",
                                "Qui",
                                "Sex",
                                "Sáb",
                                "Dom"
                            ],
                            "monthNames": [
                                "Janeiro",
                                "Fevereiro",
                                "Março",
                                "Abril",
                                "Maio",
                                "Junho",
                                "Julho",
                                "Agosto",
                                "Setembro",
                                "Outubro",
                                "Novembro",
                                "Dezembro"
                            ]
                        },
                        ranges: {
                            'Hoje': [moment(), moment()],
                            //'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Próximos 7 dias': [moment(), moment().add(6, 'days')],
                            'Próximos 30 Dias': [moment(), moment().add(29, 'days')],
                            'Próximos 60 Dias': [moment(), moment().add(59, 'days')],
                            'Próximos 180 Dias': [moment(), moment().add(179, 'days')],
                            'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                            'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                            'Últimos 60 Dias': [moment().subtract(59, 'days'), moment()],
                            'Últimos 180 Dias': [moment().subtract(179, 'days'), moment()],
                            //'Mês atual': [moment().startOf('month'), moment().endOf('month')],
                            //'Mês anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        template:
                            '<div id="calendar-' + element.id + '" class="daterangepicker dropdown-menu">' +
                                '<div class="calendar left">' +
                                    '<div class="daterangepicker_input">' +
                                      '<input class="form-control" data-bind="mask: { options: defaults.mask.date }, clearOnEsc: {}" type="text" name="daterangepicker_start" value="" />' +
                                      '<i class="material-icons">&#xE916;</i>' +
                                      '<div class="calendar-time">' +
                                        '<div></div>' +
                                        '<i class="material-icons">&#xE192;</i>' +
                                      '</div>' +
                                    '</div>' +
                                    '<div class="calendar-table"></div>' +
                                '</div>' +
                                '<div class="calendar right">' +
                                    '<div class="daterangepicker_input">' +
                                      '<input class="form-control" data-bind="mask: { options: defaults.mask.date }, clearOnEsc: {}" type="text" name="daterangepicker_end" value="" />' +
                                      '<i class="material-icons">&#xE916;</i>' +
                                      '<div class="calendar-time">' +
                                        '<div></div>' +
                                        '<i class="material-icons">&#xE192;</i>' +
                                      '</div>' +
                                    '</div>' +
                                    '<div class="calendar-table"></div>' +
                                '</div>' +
                                '<div class="ranges">' +                                   
                                '</div>' +
                                '<div class="footer ranges range_inputs calendar clearfix">' +
                                    '<button class="applyBtn" disabled="disabled" type="button"><i class="material-icons">&#xE876;</i></button> ' +
                                    '<button class="cancelBtn" type="button"></button>' +
                                '</div>' +
                            '</div>',
                        opens: 'center'
                    },
                    options = $.extend(true, defaultOptions, valueAccessor()),
                    $element = $(element),
                    inputGroup = $element.parent().find('.input-group-btn'),
                    setTextValue = function (startDate, endDate) {
                        if (isNullOrEmptyDate(startDate) || isNullOrEmptyDate(endDate)) {
                            $element.val('');
                        } else {
                            $element.val('' + startDate + options.locale.separator + endDate);
                        }                        
                    };

                self.getPosition = function () {
                    //return (el.position().left >= ($('body').width() / 2) ? 'left' : 'right');
                    return 'center';
                };                
                self.getValue = function (prop, defaultValue) {                 
                    var value = ko.observable(defaultValue);
                    if (valueAccessor().hasOwnProperty(prop)) {
                        if (ko.isObservable(valueAccessor()[prop])) {
                            value = valueAccessor()[prop];
                        } else {
                            value = ko.observable(valueAccessor()[prop]);
                        }
                    }
                    return value;
                };           
                self.startDate = self.getValue('startDate', null);
                self.endDate = self.getValue('endDate', null);
               
                if (inputGroup.length > 0) {
                    inputGroup.on('click', function () {
                        $element.trigger('focus');
                    });
                };
                  
                var api = null;

                ko.computed(function () {
                    var startDateValue = ko.utils.unwrapObservable(self.startDate),
                        endDateValue = ko.utils.unwrapObservable(self.endDate);
                   
                    if (ko.computedContext.isInitial()) {
                        // create element
                        options.startDate = startDateValue;
                        options.endDate = endDateValue;

                        $element.daterangepicker(options).addClass('datepicker').on('apply.daterangepicker', function (e, picker) {                           
                            setTextValue(picker.startDate.format('DD/MM/YYYY'), picker.endDate.format('DD/MM/YYYY'));                           
                            self.startDate(picker.startDate.format('DD/MM/YYYY'));
                            self.endDate(picker.endDate.format('DD/MM/YYYY'));                            
                        });

                        // null values
                        if (isNullOrEmptyDate(startDateValue) || isNullOrEmptyDate(endDateValue)) {
                            $element.val('');
                        }

                        $element.on('cancel.daterangepicker', function (ev, picker) {
                            $(this).val('');
                            self.startDate(null);
                            self.endDate(null);
                        });

                        $element.on('show.daterangepicker', function (ev, picker) {
                           
                            var startDateValue = picker.startDate.format('DD/MM/YYYY'),
                                endDateValue = picker.endDate.format('DD/MM/YYYY');
                          
                            if (isNullOrEmptyDate(startDateValue) || isNullOrEmptyDate(endDateValue)) {                                
                                picker.setStartDate(moment());
                                picker.setEndDate(moment());
                                api.updateView();
                            };
                        });

                        window.setTimeout(function () {
                            // calendar ready
                            api = $element.data('daterangepicker');   
                            //ko.applyBindings({}, document.getElementById('calendar-' + element.id));
                        }, 150);

                    } else {
                        // update observables                        
                        api.setStartDate(startDateValue);
                        api.setEndDate(endDateValue);
                        api.updateView();

                        setTextValue(startDateValue, endDateValue);
                    }
                }).extend({ rateLimit: { timeout: 100, method: "notifyWhenChangesStop" } });

            };

            return new daterangepicker();
            //$(element).off('keydown'); // remove evento enter do componente   

           
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {            
        }
    };
}));