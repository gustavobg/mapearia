﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'datetimepicker/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var el = $(element).parent();            
            el.datetimepicker(valueAccessor().options).addClass('datepicker').on('dp.change', function (e) {
                $(element).trigger('change');
            });
            $(element).off('keydown'); // remove evento enter do componente           
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var el = $(element).parent();
            el.datetimepicker(valueAccessor().options).addClass('datepicker').on('dp.change', function (e) {
                $(element).trigger('change');
            });
            
        }
    };
}));