﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.defaultAction = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {                       
            var trigger = valueAccessor();
          
            element = $(element);

            element.on('keydown', function (e) {
                // 27 - Esc, 13 - Enter
                if (e.keyCode == 13) {                    
                    if (typeof (trigger) === 'function')
                        trigger();
                    else if (typeof (trigger) === 'string') {
                        $(trigger).click();
                    }; 
                }
            });      
        },
        update: function (element, valueAccessor) {}
    };

})); 