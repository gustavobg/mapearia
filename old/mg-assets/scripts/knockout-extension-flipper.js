﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'jquery-flipper/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.flipper = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var vmValue = valueAccessor().observable || valueAccessor().value || allBindings().checked,
                options = valueAccessor().options || valueAccessor(),
                f = null;
            
            // DOM event
            if (ko.isObservable(vmValue)) {
                options = $.extend(options, {
                    afterTurnOn: function () {
                        vmValue(true);
                    },
                    afterTurnOff: function () {
                        vmValue(false);
                    },
                }, true);
            };

            f = $(element).flipper(options);

            // KO value change
            ko.computed(function () {

                var valor = ko.utils.unwrapObservable(vmValue);

                if (valor === 'true' || valor === true)
                    f.flipper('turnOn');
                else
                    f.flipper('turnOff');
            });            
        },
        update: function (element, valueAccessor, allBindings) {
        	var f = $(element);

        	var enable = allBindings().hasOwnProperty('enable') ? allBindings().enable : false;
        	var disable = allBindings().hasOwnProperty('disable') ? allBindings().disable : false;        	

        	if (ko.isObservable(enable)) {
        		enable = ko.unwrap(enable);
        		if (enable) {
        			f.flipper('enable');
        			f.prop('disabled', false);
        		} else {
        			f.flipper('disable')
        			f.prop('disabled', true);
        		}        		
        	} else if (ko.isObservable(disable)) {
        		disable = ko.unwrap(disable);
        		if (disable) {
        			f.flipper('disable');
        			f.prop('disabled', true);
        		} else {
        			f.flipper('enable');
        			f.prop('disabled', false);
        		}
        	}

            $(element).flipper("refresh");
        }
    };
    
}));