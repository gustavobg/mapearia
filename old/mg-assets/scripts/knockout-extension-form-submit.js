﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.validateForm = {
        // Set submit action and binds the ENTER key

        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // validate form adapter
           
            if (element.nodeName === 'FORM') {
                // find validate elements
                var options = valueAccessor(),
                    $el = $(element);
               
                element.setAttribute('novalidate', 'novalidate');

                if (options.hasOwnProperty('submitHandler')) {
                    var submitHandler = function () {
                        options.submitHandler();                    
                    };
                    $el.on('keydown', function (e) {
                        // blur elements to trigger viewmodel changes
                        if (e.keyCode === 13 && e.target.tagName != 'TEXTAREA' && !e.target.classList.contains('note-editable') && !e.target.classList.contains('modal')) {
                            // prevent top handlers from submitting
                            e.stopPropagation(); // prevent submit
                            e.preventDefault();                            
                            document.activeElement.blur();
                            submitHandler();                                
                            
                        } 
                    });
                    $el.on('submit', function (e) {                       
                        document.activeElement.blur();
                        e.stopPropagation(); // prevent submit
                        e.preventDefault();                        
                        submitHandler();
                    });
                }

            } else {
                throw ('ValidateError: This validateForm handler should be used on a form element');
            }

        }
    };

    ko.bindingHandlers.formSubmit = {
        // Set submit action and binds the ENTER key

        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // validate form adapter
            if (element.nodeName === 'FORM') {

                var options = valueAccessor(),
                    $el = $(element);

                element.setAttribute('novalidate', 'novalidate');

                if (options.hasOwnProperty('submitHandler')) {
                    var submitHandler = function () {
                        options.submitHandler();
                    };
                    $el.on('keydown', function (e) {
                        // blur elements to trigger viewmodel changes
                        if (e.keyCode === 13 && e.target.tagName != 'TEXTAREA' && !e.target.classList.contains('note-editable')) {
                            if (e.target.classList.contains('modal'))
                                return;
                            e.target.blur();
                            submitHandler();

                            // prevent top handlers from submitting
                            e.stopPropagation(); // prevent submit
                            e.preventDefault();
                        }
                    });
                    $el.on('submit', function (e) {
                        document.activeElement.blur();
                        e.stopPropagation(); // prevent submit
                        e.preventDefault();
                        submitHandler();
                    });
                }

            } else {
                throw ('formSubmit Error: This Form handler should be used on a form element');
            }

        }
    };
}));