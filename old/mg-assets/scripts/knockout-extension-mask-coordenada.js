﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'jquery-inputmask/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.maskCoordenada = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var valueAccessor = ko.utils.unwrapObservable(valueAccessor),
                valor = valueAccessor().value || allBindings().value || allBindings().textInput,
                extendedOptions = valueAccessor().hasOwnProperty('extendedOptions') ? valueAccessor().extendedOptions : {};
                obsCasasDecimais = valueAccessor().hasOwnProperty('casasDecimais') ? ko.observable(ko.utils.unwrapObservable(valueAccessor().casasDecimais)) : ko.observable(8);
                element = $(element);

            var arredondaDecimais = function () {
                var formattedValue = element.val();

                if (formattedValue !== '') {            
                    formattedValue = Number(formattedValue).toFixed(obsCasasDecimais()); // ex: 1200.000              

                    element.val(formattedValue).change();
                }
            };

            ko.computed(function () {               
                element.inputmask('decimal', $.extend(true, {
                    groupSeparator: '', groupSize: 3, radixPoint: '.', autoGroup: true, min: -90, max: 90, digits: obsCasasDecimais() }, extendedOptions))
                   .on('blur', function () { arredondaDecimais(); });

                arredondaDecimais();
            });

        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

        }
    }
}));