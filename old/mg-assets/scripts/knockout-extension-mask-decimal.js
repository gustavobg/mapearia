﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'jquery-inputmask/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.maskDecimal = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var maskDecimal = function () {
                var self = this;

                this.valueAccessor = ko.utils.unwrapObservable(valueAccessor);
                this.getValue = function (prop, defaultValue) {
                    var value = ko.observable(defaultValue);
                    if (valueAccessor().hasOwnProperty(prop)) {
                        if (ko.isObservable(valueAccessor()[prop])) {
                            value = valueAccessor()[prop];
                        } else {
                            value = ko.observable(valueAccessor()[prop]);
                        }
                    }
                    return value;
                };
                this.valor = allBindings().value || allBindings().textInput || valueAccessor().value;
                this.extendedOptions = valueAccessor().hasOwnProperty('extendedOptions') ? valueAccessor().extendedOptions : {};
                this.obsCasasDecimais = self.getValue('casasDecimais', 2);
                this.obsPrefix = self.getValue('prefix', '');
                this.obsSuffix = self.getValue('suffix', '');
                this.obsMax = self.getValue('max', +999999999999);
                this.obsMin = self.getValue('min', -999999999999);
                this.obsShowSuffix = self.getValue('showSuffix', false);
                this.obsValorApresentacao = self.getValue('valorApresentacao', '');
                this.element = $(element);
                this.maskBaseOptions = ko.computed(function () {
                    return {
                        autoUnmask: true, radixFocus: true, removeMaskOnSubmit: true,
                        showMaskOnFocus: true, clearMaskOnLostFocus: true,
                        groupSeparator: '.', groupSize: 3, radixPoint: ',',
                        autoGroup: true,
                        max: ko.utils.unwrapObservable(self.obsMax),
                        min: ko.utils.unwrapObservable(self.obsMin),
                        digits: ko.utils.unwrapObservable(self.obsCasasDecimais),
                        prefix: ko.utils.unwrapObservable(self.obsPrefix),
                        suffix: ko.utils.unwrapObservable(self.obsShowSuffix) ? ko.utils.unwrapObservable(self.obsSuffix) : '',
                        onUnMask: function (maskedValue, unmaskedValue) {
                            // unmasks value to view and controllers
                            var suffix = ko.utils.unwrapObservable(self.obsSuffix),
                                showSuffix = ko.utils.unwrapObservable(self.obsShowSuffix);

                            self.obsValorApresentacao(showSuffix ? maskedValue : maskedValue + suffix);

                            return Number(unmaskedValue.replace(/\./g, '').replace(/\,/g, '.'));
                        }
                    }
                });;

                ko.computed(function () {
                    var valor = ko.utils.unwrapObservable(self.valor);
                    window.setTimeout(function () {
                        if (valor == null || valor === '') {
                            self.obsValorApresentacao('');
                        } else {
                            self.element.inputmask('unmaskedvalue'); // forces onUnMask event
                        }
                    }, 100);
                });

                ko.computed(function () {
                    var casasDecimais = ko.utils.unwrapObservable(self.obsCasasDecimais),
                        prefix = ko.utils.unwrapObservable(self.obsPrefix),
                        suffix = ko.utils.unwrapObservable(self.obsSuffix),
                        element = self.element;
                    
                    if (element[0].inputmask)
                        element[0].inputmask.remove();

                    element
                        .inputmask('currency', $.extend(true, self.maskBaseOptions(), self.extendedOptions))
                        .on('blur', function () { self.valor(element.val()); })
                        .on('focus', function () { element.select();  });
                });
            };
            return new maskDecimal();
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {            
        }
    }
}));