﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'jquery-inputmask/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.mask = {

        init: function (element, valueAccessor) {            
        	var mask = valueAccessor(),
				observable = mask.value,
				el = $(element),
				observableMask = mask.maskValue,
                maskPhone = function () {
                    var phoneValue = ko.utils.unwrapObservable(observable);
                    phoneValue = phoneValue !== null ? phoneValue.replace(/\D/g, '') : '';
                    el.inputmask({ mask: (phoneValue.length > 10 ? "(99) 9\.9999-9999" : "(99) 9999-9999[9]"), greedy: false });
                };

            if (mask.options === 'money') {
                el.inputmask('decimal', defaults.mask.moneyoptions);
            } if (mask.options === 'coordenada') {
                el.inputmask('decimal', defaults.mask.coordenada);
            } else if (mask.options == "phone") {
                maskPhone();
            } else if (ko.isObservable(mask.maskValue)) {
                var options = $.extend(true, mask.options, { mask: mask.maskValue() });
                el.inputmask(options);
            } else {                
                el.inputmask(mask.options);
            }

            if (ko.isObservable(observable)) {
                if (typeof (observable) === 'function') {
                    el.on('focusout blur', function () {
                        if (mask.options == "phone") {
                            maskPhone();
                        }
                        if (mask.options !== 'date' || mask.options !== 'datetime')
                            observable(el.val())
                        else
                            observable(el.inputmask('unmaskedvalue'));
                    });
                }
            }

        },
        update: function (element, valueAccessor) {
            var mask = valueAccessor(), observable = mask.value, el = $(element), observableMask = mask.maskValue;
            if (mask.options == "phone") {
                el.on('focusout blur', function () {
                    var phone, element;
                    el.inputmask("remove");
                    phone = observable() ? observable().replace(/\D/g, '') : '';
                    el.inputmask({ mask: (phone.length > 10 ? "(99) 9\.9999-9999" : "(99) 9999-9999[9]"), greedy: false });
                });
            }
            else if (ko.isObservable(mask.maskValue)) {
                var options = $.extend(true, mask.options, { mask: mask.maskValue() });
                el.inputmask(options);
            }
        }
    }
}));