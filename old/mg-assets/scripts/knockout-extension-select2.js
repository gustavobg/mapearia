﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'select2/plugin', 'error-handler'], factory);
    } else {
        factory(this.jQuery, this.ko, this.logError);
    }
}(this, function ($, ko, logError) {

    ko.bindingHandlers.select2 = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var jsonify = (function (div) {
                return function (json) {
                    div.setAttribute('onclick', 'this.__json__ = ' + json);
                    div.click();
                    return div.__json__;
                }
            })(document.createElement('div')),
			defaults = {
				preSelecaoDadosCompl: null,
				preSelecaoUrl: '',
				preSelecaoData: null
			},
            o = valueAccessor(),
            extendedOptions = {},
            domElement = false;

        	// defaults
            $.extend(true, defaults, o);
        	
            // set default placeholder when allowClear is true
            if (o.allowClear) {
                if (o.placeholder.length == 0)
                    o.placeholder = "";
            };
           
            if (!o.hasOwnProperty('element')) {
                // DOM element                
                o.element = $(element).select2(o).on("change", getSelected).on("select2-clearing", onClear);

                domElement = true;

                if (element.tagName === 'SELECT')
                    o.multiple = true;

            } else {     
			
				o.element = $('#' + o.element);
				o.element.select2({
					placeholder: o.placeholder,
					triggerChange: false,
					allowClear: o.allowClear,
					multiple: o.multiple,
					width: o.width,
					query: function (query) {

						var self = this;
						var value = query.term;
						var currentPage = query.page;

						var dfd = new $.Deferred();

						// preSelecao
						var preSelecaoDadosCompl = ko.unwrap(o.preSelecaoDadosCompl);
						var preSelecaoUrl = ko.unwrap(o.preSelecaoUrl);
						var preSelecaoData = ko.unwrap(o.preSelecaoData);

						if (o.preSelecaoDadosCompl != null && preSelecaoUrl.length > 0 && preSelecaoData != null) {
							var json = {
								Page: {
									PageSize: null, CurrentPage: null,
									OrderBy: null
								}
							};
							if (o.hasOwnProperty('preSelecaoDadosCompl') && o.preSelecaoDadosCompl.length > 0) {
								$.extend(json, jsonify('{' + o.preSelecaoDadosCompl + '}'));
							}
							// novos params: preSelecaoUrl, preSelecaoObservable, preSelecaoDadosCompl
							// no evento beforeSend: resposta da url é notificada no preSelecaoObservable, que irá retornar os parametros complementares para a requisição
							$.ajax({
								url: ko.unwrap(o.preSelecaoUrl), //'/Corporativo/Pessoa/List' // o.preSelecaoUrl,
								method: 'POST',
								dataType: 'json',
								data: JSON.stringify(json)
							}).done(function (data) {
								o.preSelecaoData(data, dfd);
							});
						} else {
							dfd.resolve({});
						}

						dfd.done(function (extendedOptions) {							
							// faz listagem dos options
							$.ajax({
								dataType: 'json',
								cache: false,
								type: 'POST',
								url: o.url,
								data: function (extendedOptions) {
									var json = {
										Page: {
											PageSize: 10, CurrentPage: currentPage,
											OrderBy: o.ordenacao
										}
									};
									$.extend(true, json, extendedOptions);

									if (o.hasOwnProperty('dadosCompl') && o.dadosCompl.length > 0) {
										$.extend(json, jsonify('{' + o.dadosCompl + '}'));
									}
									if (o.hasOwnProperty('dadosComplObservable')) {
										if (ko.isObservable(o.dadosComplObservable)) {
											$.extend(json, ko.utils.unwrapObservable(o.dadosComplObservable));
										}
									}
									if (o.idControleDependencia.length > 0)
										$.extend(json, jsonify('{' + o.idDependencia + ':' + "'" + $('#' + o.idControleDependencia).val() + "'" + '}'));

									json[o.campoPesquisa] = value;
									return JSON.stringify(json);

								}(extendedOptions)								
							}).done(function (result) {
								// exibição dos resultados
								var more = result.Page.CurrentPage < result.Page.TotalPages;
								var convert;
								for (var i = 0; i < result.Data.length; i++) {
									convert = result.Data[i];
									convert.id = o.campoIdChaveRelacao ? convert[o.campoIdChaveRelacao] : convert[o.campoId];
									convert.text = function (e) { return e[o.campoValor]; }(convert);
								}
								query.callback({ results: result.Data, more: more });
							});
						});
            		},
            		initSelection: function (element, callback) {
            			// já possui id
            			//var id = element.val();
            			var id = ko.utils.unwrapObservable(o.valor);
            			if (id !== "" && id != "0" && id != null) {
            				var json = {
            					Page: {
            						OrderByColumn: o.ordenacao,
            						OrderByDirection: "asc"
            					}
            				},
							type = 'POST',
							url = o.url;

            				json[o.campoIdPesquisa] = id;

            				var data = JSON.stringify(json);

            				if (o.hasOwnProperty('urlSelecao') && o.urlSelecao.length > 0) {
            					type = 'GET';
            					url = o.urlSelecao;
            					data = 'id=' + id;
            				}

            				$.ajax(url, {
            					data: data,
            					dataType: "json",
            					type: type
            				}).done(function (result) {
            					// todo: Customizar a requisição, o.urlInitSelection (type: Get)

            					if (!Array.isArray(result.Data)) {
            						result.Data = [result.Data];
            					}

            					if (result.Data.length == 0) {
            						//logError('Erro na operação select2: result.Data esperado.', o, false);
            					} else {
            						var convertArray = result.Data,
										convert,
										textArr = [];

            						if (o.multiple) {
            							for (var i = 0; i < convertArray.length; i++) {
            								var text = '';
            								convert = convertArray[i];
            								text = function (e) { return e[o.campoValor]; }(convert);
            								convert.id = o.campoIdChaveRelacao ? convert[o.campoIdChaveRelacao] : convert[o.campoId];
            								convert.text = text;
            								textArr.push(text);
            							}
            						} else {
            							convert = convertArray[0];
            							convert.id = o.campoIdChaveRelacao ? convert[o.campoIdChaveRelacao] : convert[o.campoId];
            							convert.text = function (e) { return e[o.campoValor]; }(convert);
            							convertArray = convert;
            						}

            						if (ko.isObservable(o.vmTextSelected)) {
            							if (o.multiple) {
            								o.vmTextSelected(textArr.join(', '));
            							} else {
            								o.vmTextSelected(convert.text);
            							}
            						}

            						if (ko.isObservable(o.vmDataSelected)) {
            							o.vmDataSelected(convert);
            						}

            						callback(convertArray);
            					}
            				});
            			}
            			        			
            		}
				}).on("change", getSelected)
					.on("select2-clearing", onClear);

                ko.computed(function () {
                    if (ko.unwrap(o.allowClear) === false)
                        o.element.parent().find('.select2-search-choice-close').hide();
                    else {
                        if (!o.element.is(':disabled'))
                            o.element.parent().find('.select2-search-choice-close').show();
                    }
                });

                // verifica disabled
                if (ko.isObservable(o.disabledObservable)) {                	
                    var disabled = ko.unwrap(o.disabledObservable);
                    if (disabled !== null || disabled !== '') {
                        o.element.select2('disable', disabled);
                    }
                }

                // elemento de ação adicional              
                if (o.hasOwnProperty('footerElementId') && o.footerElementId.length > 0) {
                    o.element.select2("container").find('.select2-drop').append(document.getElementById(o.footerElementId).innerHTML);
                }

                if (ko.isObservable(o.valor)) {                    
                    o.valor.subscribe(function (v) {
                        if (o.hasOwnProperty('multiple') && o.multiple == true) {
                            o.element.change();
                        } else {
                            o.element.select2('val', v);
                        }
                    });
                }
            }    

            function getSelected(e) {
                var data = $(this).select2('data');

                if (domElement) {
                    o.valor(o.element.select2('val'));
                }

                if (ko.isObservable(o.vmTextSelected)) {
                   
                    // retorna valor de texto para observable vmTextSelected
                    if (data !== null) {
                        if (typeof (data) === 'object') {
                            var i = 0,
                                dataLength = data.length,
                                textArr = [];
                            for (i; i < dataLength;) {
                                textArr.push(data[i].text);
                                i = i + 1;
                            }
                            o.vmTextSelected(textArr.join(', '));
                        } else {
                            o.vmTextSelected($(this).select2('data').text);
                        }
                    }
                    else
                        o.vmTextSelected('');
                }
                if (ko.isObservable(o.vmDataSelected)) {
                    if (data !== null)
                        o.vmDataSelected($(this).select2('data'));
                    else
                        o.vmDataSelected({});
                }
            }

            function onClear(e) {
                // sobrescreve evento clear para tornar observable nula ao invés de ''
                o.valor(null);
                o.element.parent().find('.select2-chosen').empty();
                e.preventDefault();
            };

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                // This will be called when the element is removed by Knockout or
                // if some other part of your code calls ko.removeNode(element)                
                o.element.select2('destroy');                
            });

        },
        update: function (element, valueAccessor) {
        }
    };
}));