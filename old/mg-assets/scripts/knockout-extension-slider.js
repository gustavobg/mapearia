﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'jquery-slider/plugin', 'jquery-qtip'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko) {

    ko.bindingHandlers.slider = {
        init: function (element, valueAccessor) {
            // create ammount label or use a predefined container
            element = $(element);

            var options = valueAccessor();
            var labelElement = options.amountElement != undefined ? $(options.amountElement).first() : $('<span class="label label-slider label-primary"></span>').appendTo(element.parents('.form-group').stop().find('label').stop());

            element.slider($.extend(options, {
                start: function (e, ui) {
                    slider.trigger('slidestart', ui);
                },
                slide: function (event, ui) {
                    if (options.value) {
                        labelElement.text(ui.value);
                    }
                    else if (options.values.length > 0) {
                        var text = "", x = 0, l = options.values.length;
                        for (x; x < l; x++) {
                            text += ui.values[x] + " - ";
                        }
                        labelElement.text(text.slice(0, -3));
                    }
                }
            }));

            // set labels on init
            if (options.value) {
                labelElement.text(element.slider("value"));
            } else if (options.values.length) {
                var text = "", x = 0, l = options.values.length;
                for (x; x < l; x++) {
                    text += element.slider("values", x) + " - ";
                }
                labelElement.text(text.slice(0, -3));
            }

            var handlers = $('.ui-slider-handle', element);

            handlers.qtip($.extend(defaults.qtip.sliderHorizontal, {
                id: 'uislider',
                content: '' + element.slider('option', 'value')
            }));

            // update tooltip val
            element.on('slide slidestart', function (e, ui) {
                handlers.qtip('option', 'content.text', '' + ui.value);
            });
        }
    };
}));