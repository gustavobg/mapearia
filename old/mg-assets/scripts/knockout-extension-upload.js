﻿(function (root, factory, toastr) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery', 'knockout', 'toastr'], factory);
	} else {
		factory(root.jQuery, root.ko, root.toastr);
	}
}(this, function ($, ko, toastr) {

	ko.bindingHandlers.upload = {
		init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
			// upload
			// { $element: link, url: url, data: {}, $input: element > input, success, error, xhr  }			
			var defaults = {
				id: null,
				url: null,
				uploadOnChange: true,
				text: null
			};

			var selectedFile = ko.observable(null);
			var options = $.extend(true, defaults, ko.mapping.toJS(valueAccessor(), { 'ignore': ['uploadSubmit', 'text', 'data'] })),
				err = false;

			options.uploadSubmit = valueAccessor().hasOwnProperty('uploadSubmit') ? valueAccessor().uploadSubmit : function () { };
			options.text = valueAccessor().hasOwnProperty('text') ? valueAccessor().text : null;
			options.data = valueAccessor().hasOwnProperty('data') ? valueAccessor().data : null;

			var data = $.extend(true, {}, ko.unwrap(options.data));

			if (options.id === null || (options.id !== null && options.id.length === 0)) {
				console.error('Upload: Necessário informar o id do elemento (input[file])');
				err = true;
			}
			if (options.url === null || (options.url !== null && options.url.length === 0)) {
				console.error('Upload: Necessário informar a url do upload');
				err = true;
			}
			if (options.uploadOnChange === false && !ko.isObservable(options.uploadSubmit)) {
				console.error('Upload: Necessário informar o método (observable) para submeter o upload');
				err = true;
			} else {
				options.uploadSubmit(function () {
					upload(ko.unwrap(selectedFile), ko.toJS(data));
				});
			}
			if (err) {
				return;
			}
			
			var inputFileId = ko.unwrap(options.id);
			var $inputFile = $('#' + inputFileId);
			var url = ko.unwrap(options.url);

			$(element).on('click', function (e) {
				e.preventDefault();
				$inputFile.trigger('click');
			});

			// callbacks
			var success = valueAccessor().success;
			var error = valueAccessor().error;			

			if (options.uploadOnChange) {
				$inputFile.on('change', function (e) {
					var inputFiles = e.target.files || e.dataTransfer.files;

					if (inputFiles.length > 0) {
						selectedFile(inputFiles[0]);
						getFileName(inputFiles[0]);
						upload(inputFiles[0], ko.toJS(data));
					}
					// reset input
					$inputFile.val('');
				});
			} else {
				$inputFile.on('change', function (e) {
					var inputFiles = e.target.files || e.dataTransfer.files;
					if (inputFiles.length > 0) {
						selectedFile(inputFiles[0]);
						getFileName(inputFiles[0]);
					}
					// reset input
					$inputFile.val('');
				});
			}

			var getFileName = function (file) {
				if (ko.isObservable(options.text)) {
					options.text('');
					options.text(file.name)
					return file.name;
				}				
			};

			var getImageUrl = function (blob) {
				var reader = new FileReader();
				reader.readAsDataURL(blob);
				reader.onload = function (file) {
				};
			}

			var upload = function (blob, additionalData) {
				var xhr = new XMLHttpRequest(),
					formData = new FormData();

				formData.append('file', blob);

				additionalData = ko.unwrap(additionalData);

				for (var prop in additionalData) {
					if (additionalData.hasOwnProperty(prop)) {
						formData.append(prop, additionalData[prop]);
					}
				}

				if (xhr.upload) {
					xhr.upload.onprogress = function (e) {
						if (options.hasOwnProperty('progress') && typeof (options.progress) === 'function') {
							if (e.lengthComputable) {
								options.progress(e.loaded, e.total)
							} else {
								options.progress();
							}
						}						
					};
					//xhr.upload.onloadstart
					//xhr.upload.onload
					xhr.upload.onerror = function (e) {
						//toastr.error('Ocorreu um erro ao enviar o KML, tenta novamente mais tarde');
					};
				}
				xhr.onreadystatechange = function (e) {
					// complete
					var response = e.currentTarget.response;

					if (response !== null && response !== '') {
						response = JSON.parse(response);

						if (response.Sucesso === true) {
							success(response, e);
						} else {
							error(response, e);
						}
					}

				};
				xhr.open("POST", url, true);
				xhr.send(formData);
			}
		}
	};

}));