﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'vanillaUniform/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko, root.vanillaUniform);
    }
}(this, function ($, ko, vanillaUniform) {
    //var vanilla = null;
    ko.bindingHandlers.vanillaUniform = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

            var checked = allBindings().checked,
                options = ko.unwrap(valueAccessor()),
                text = (options.hasOwnProperty('text') ? options.text : ''),
                vmArrTextSelected = (options.hasOwnProperty('vmArrTextSelected') ? options.vmArrTextSelected : null);

            element.addEventListener('vanillaUniform.click', function (e) {
                console.log($(e.currentTarget).val(), e.detail);
            });

            // dom event
            element.addEventListener('vanillaUniform.change', function (e) {
                //console.log('vanillaUniform.change');
                var add = e.detail,
                    v = e.currentTarget.value,
                    textValue = $(e.currentTarget).data('text');
                    //,addTextSelected = function () {
                    //    if (ko.isObservable(vmArrTextSelected)) {
                    //        var value = ko.unwrap(vmArrTextSelected),
                    //            arrTexts = value == '' ? [] : value.split(', ');
                    //        arrTexts.push(textValue);
                    //        vmArrTextSelected(arrTexts.join(', '));
                    //    }
                    //},
                    //removeTextSelected = function (textValue) {
                    //    if (ko.isObservable(vmArrTextSelected)) {
                    //        var value = ko.unwrap(vmArrTextSelected),
                    //            arrTexts = value.split(', ');

                    //        if (arrTexts.indexOf(textValue) > -1) {
                    //            arrTexts.slice(arrTexts.indexOf(textValue), 1);
                    //            vmArrTextSelected(arrTexts.push(textValue).join(', '));
                    //        }
                    //    }
                    //};
             
                if (Array.isArray(ko.utils.unwrapObservable(checked))) {
                    if (add) {
                        checked.push(v);
                        if (ko.isObservable(vmArrTextSelected)) {
                            vmArrTextSelected.push(textValue);
                        }
                    } else {
                        checked.remove(v);
                        if (ko.isObservable(vmArrTextSelected)) {
                            vmArrTextSelected.remove(textValue);
                        }
                        //removeTextSelected();
                    }
                } else {
                    if (element.type === 'checkbox') {
                        checked(add ? true : false);                       
                    } else {
                        // radio
                        checked(v);                        
                    }
                    if (ko.isObservable(vmArrTextSelected)) {
                        vmArrTextSelected([textValue]);
                    }
                }
            });
            if (element.type === 'checkbox') {
                //$(element).on('change', function (e) {
                //    checked(e.currentTarget.checked);
                //});
            }
            if (text.length > 0) {
                $(element).data('text', text);
            }
            $(element).data('vanilla-uniform', new vanillaUniform(element, valueAccessor()));

        },
        update: function (element, valueAccessor, allBindings, viewModel) {
            var checked = allBindings().checked,
                checkedValue = ko.unwrap(allBindings().checked),				
                options = ko.unwrap(valueAccessor()),
                textValue = $(element).data('text'),
				disable = ko.unwrap(allBindings().disabled),
                vmArrTextSelected = options.hasOwnProperty('vmArrTextSelected') ? options.vmArrTextSelected : null,
                vanilla = $(element).data('vanilla-uniform'); // get vanilla instance

            // KO value change
            // TODO: dom reacts to knockout changes
            
            if (element.name) {
                if (element.type === 'checkbox') {
                    if (Array.isArray(checkedValue)) {
                        vanilla.toggle(element, checkedValue.indexOf(element.value) >= 0);
                    }
                    if (ko.isObservable(vmArrTextSelected)) {
                        //if (checkedValue.indexOf(element.value) >= 0) {
                        //    debugger;
                        //    // checkbox index is selected
                        //    vmArrTextSe lected.push(textValue);
                        //} else {
                        //    vmArrTextSelected.remove(textValue);
                        //}
                    }
                } else {
                    if (element.value == checkedValue) {
                    	vanilla.toggle(element, true);
                        //if (ko.isObservable(vmArrTextSelected)) {
                        //    vmArrTextSelected([textValue]);
                        //}
                    }
                }
            } else {
            	vanilla.toggle(element, checkedValue);            	
				
                if (ko.isObservable(vmArrTextSelected)) {
                    if (checkedValue) {
                        // checkbox index is selected
                        vmArrTextSelected.push([textValue]);
                    } else {
                        vmArrTextSelected([]);
                    }
                }
            }

            if (disable) {
            	vanilla.disable(element);
            } else {
            	vanilla.enable(element);
            }

            // get text selected


            //if (viewModel.hasOwnProperty('_change')) {
            //    viewModel._change(allBindings());
            //}
        }
    };

}));