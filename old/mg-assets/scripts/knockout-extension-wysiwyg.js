﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'bootstrap/tooltip', 'summernote/plugin'], factory);
    } else {
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko, vanillaUniform) {
    ko.bindingHandlers.wysiwyg = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            var extend = function () {
                if (valueAccessor().observable) {
                    return $.extend(valueAccessor().options, {
                        onSubmit: function (e) {
                            //valueAccessor().observable($(this).code());
                        },
                        onBlur: function (e) {
                            valueAccessor().observable($(this).code());
                        },
                        onKeyup: function () {
                        },
                        onChange: function (contents, $editable) {                          
                            //valueAccessor().observable(contents);
                        }
                    });
                } else {
                    return valueAccessor();
                }
            };           
            $(element).summernote(extend());
        },
        update: function (element, valueAccessor) {
            if (valueAccessor().observable)
                $(element).code(valueAccessor().observable());
        }
    };
}));