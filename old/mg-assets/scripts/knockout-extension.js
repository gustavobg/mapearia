﻿(function (root, factory) {
    // Module systems magic dance.
    if (typeof require === "function" && typeof exports === "object" && typeof module === "object") {
        // CommonJS or Node: hard-coded dependency on "knockout"
        factory(require('jquery'), require("knockout"), exports);
    } else if (typeof define === "function" && define["amd"]) {
        // AMD anonymous module with hard-coded dependency on "knockout"
        define(["jquery", "knockout", "exports"], factory);
    } else {
        // <script> tag: use the global `ko` object, attaching a `mapping` property
        factory(root.jQuery, root.ko);
    }
}(this, function ($, ko, exports) {


function getBindingHandlerValue(valueAccessor, allBindings) {
    if (valueAccessor().hasOwnProperty('value'))
        return valueAccessor().value;
    else if (valueAccessor().hasOwnProperty('observable'))
        return valueAccessor().observable;
    else if (allBindings().hasOwnProperty('value'))
        return allBindings().value;
    else if (allBindings().hasOwnProperty('textInput'))
        return allBindings().textInput;
    else if (allBindings().hasOwnProperty('checked'))
        return allBindings().checked;
};

var handler = {};





}));