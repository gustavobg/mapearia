﻿define(["jquery", "knockout", "ko-mapping", "toastr", "help", "text!../templates/grid-view-default.html", "text!../templates/grid-view-index.html", "bootstrap/modal", "default-action", "clear-on-esc", "vanillaUniform", 'jquery-collapsible', 'knockout-postbox', 'mask-decimal'], function ($, ko, mapping, toastr, mgHelp, gridViewDefaultTemplate, gridViewIndexTemplate) {
    // TODO
    // Modularizar grid, separar por extensões:
    // Base: DataSource, Order, Pagination
    // Extensões: HiddenColunms, AjaxSource, Filter, RowSelection, RowClick, Groupable, DataExport, DataPrint, ModalEdit, AsidePanelEdit
    // Eventos/Triggers getData

    //TODO: Gustavo, introjetai o módulo mask-decimal :| mals ai
    ko.observableArray.fn.sortByProperty = function (prop, order, vm) {
        vm.orderByCol(prop);
        vm.orderByDir(order);

        this.sort(function (obj1, obj2) {
            var obj1 = ko.utils.unwrapObservable(obj1[prop]),
                obj2 = ko.utils.unwrapObservable(obj2[prop]);
            if (obj1 == obj2)
                return 0;
            else if (obj1 < obj2)
                return order === 'desc' ? 1 : -1;
            else
                return order === 'desc' ? -1 : 1;
        });
    };
    ko.mapping = mapping;
    ko.gridEdit = {};
    ko.gridEdit.utils = {};
    ko.gridEdit.utils['getParamObservableArray'] = function (param) {
        return ko.isObservable(param) ? param : ko.observableArray(param);
    }
    ko.gridEdit.utils['getParamDefault'] = function (param, defaultValue) {
        if (param !== undefined)
            return param;
        else
            return defaultValue;
    };

    var gridEditVM = function (params) {
        var self = this,
            utils = ko.gridEdit.utils;

        this.setStatus = function (element, prop) {

            var context = ko.contextFor(element),
                $data = ko.dataFor(element),
                status = false,
                textTrue = 'Sim',
                textFalse = 'Não';

            if ($data.hasOwnProperty('textTrue'))
                textTrue = ko.utils.unwrapObservable($data['textTrue']);

            if ($data.hasOwnProperty('textFalse'))
                textFalse = ko.utils.unwrapObservable($data['textFalse']);

            if ($data.hasOwnProperty('status')) {
                status = ko.utils.unwrapObservable($data['status']);
            } else if ($data.hasOwnProperty('Ativo')) {
                status = ko.utils.unwrapObservable($data['Ativo']);
            } else {
                status = false;
                if (console) {
                    console.warn('GridView Warning: Label doesn\'t not have property "status" or "Ativo"', $data);
                }
            };

            return '<span class="' + (status ? 'label label-success' : 'label label-danger') + '">' + (status ? textTrue : textFalse) + '</span>';

        };
        this.columns = params.columns;
        this.columnsLength = 0;
        this.editColumn = '';
        this.getColumns = function () {

            var result = [];

            result = ko.utils.arrayFilter(ko.utils.unwrapObservable(self.columns), function (c) {
                if (c.hasOwnProperty('id'))
                    c.displayName = c.hasOwnProperty('displayName') ? c.displayName : c.id;

                // get edit column name
                if (c.hasOwnProperty('editColumn') && c.editColumn)
                    self.editColumn = c.id;

                return c.hasOwnProperty('header') && c.header;
            });
            self.columnsLength = result.length;

            return result;
        }();
        this.groupByOptions = ko.observableArray([]);
        this.getGroupByOptions = function () {
            var result = [];
            result = ko.utils.arrayFilter(ko.utils.unwrapObservable(self.columns), function (c) {
                if (c.hasOwnProperty('groupable') && c.groupable) {
                    if (!c.hasOwnProperty('id'))
                        throw ('GridView Error: Groupable columns requires an ID');

                    c.displayName = c.hasOwnProperty('displayName') ? c.displayName : c.id;
                    return c;
                }
            });
            result.unshift({ id: "", displayName: "Nenhum", groupable: true, defaultValue: "" });
            self.groupByOptions(result);
        }();

        this.groupBySelectedOptions = ko.pureComputed(function () {
            return ko.utils.arrayFirst(this.groupByOptions(), function (item) {
                return self.groupByCol() == item.id;
            });
        }, this);

        this.templateId = params.templateId;
        this.displayMode = ko.gridEdit.utils.getParamDefault(params.displayMode, 'modal');
        this.tabs = ko.observableArray([]);
        this.title = ko.gridEdit.utils.getParamDefault(params.title, '');
        this.useFilter = params.useFilter;
        this.useRemovedColumn = params.useRemovedColumn;
        this.removedColumnName = params.removedColumnName;
        this.useCustomHeaderAction = ko.gridEdit.utils.getParamDefault(params.useCustomHeaderAction, false);
        this.addTitle = ko.gridEdit.utils.getParamDefault(params.addTitle, 'Adicionar');
        this.parentId = ko.gridEdit.utils.getParamDefault(params.parentId, null);
        this.isLoading = ko.observable(false);
        this.loading = ko.observable(false);
        this.loadingTimeout = 0;

        ko.computed(function () {
            if (self.loading()) {
                self.isLoading(true);
            } else {
                //window.setTimeout(function () {
                self.isLoading(false);
                //}, 200);
            }
        }).extend({ rateLimit: { deferred: true, timeout: self.loadingTimeout } });
        // selection
        this.useSelectionObservable = params.useSelectionObservable;
        this.useSelection = ko.isObservable(this.useSelectionObservable) ? params.useSelectionObservable : ko.gridEdit.utils.getParamDefault(params.useSelection, false);

        this.setSelectedValues = function () {
            // setting selected values after pagination
            var data = self.data(),
                add = self.selectedValuesAdd() ? true : false,
                selectedArr = [];
            //console.log('setting selected', data, self.selectedValues());
            ko.utils.arrayForEach(data, function (item) {
                if (self.selectedValues().indexOf(item[self.editColumn]) > -1) {
                    item._selectedView(add);
                    selectedArr.push(item[self.editColumn]);
                }
            });
            //console.log(selectedArr);
        };
        this.resetSelected = function () {
            self.selectedValues([]);
            //self.selectAllState('none');
        };
        this.showOnlySelected = ko.observable(false);

        this.selectAll = ko.observable(false);
        this.selectAllState = ko.observable('none'); // all/none/partial       
        this.selectedValues = ko.observableArray();
        this.selectedValuesAdd = ko.observable(true);
        this.selectedCount = ko.observable(0);

        //ko.computed(function () {
        //    // ?
        //    if (self.selectedValuesAdd()) {
        //        self.selectedCount(self.selectedValues().length);
        //    } else {
        //        self.selectedCount(Number(self.totalRecords()) - Number(self.selectedValues().length));
        //    }
        //}).extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 150 } });

        ko.computed(function () {
            if (self.selectedValues().length == 0) {
                self.showOnlySelected(false);
            }
        });

        //ko.computed(function () {
        //    if (self.selectedValues() <= 0) {
        //        self.selectAll(false);
        //    }
        //}).extend({ deffered: true, timeout: 150 });

        ko.computed(function () {
            if (self.selectAll() === true) {
                self.selectAllState('all');

            } else {
                self.selectAllState('none');
            }
        });

        this.selectAllState.subscribe(function (state) {
            // limpa selecionados se desmarcar ou marcar opção para selecionar todos
            if (state == 'none' || state == 'all') {
                self.resetSelected();
            }
            self.selectedValuesAdd(state == 'all' || state == 'partial' ? false : true);
        });

        // viewModel constructor
        this.modelBase = function () {
            var model = new Object();
            ko.utils.arrayForEach(ko.utils.unwrapObservable(self.columns), function (c) {
                if (c.hasOwnProperty('id')) {
                    model[c.id] = c.hasOwnProperty('defaultValue') ? c.defaultValue : null;
                }
            });
            return model;
        };

        // viewModel extender
        this.vmExtend = utils.getParamDefault(params.vmExtend, {});
        this.fromJS = function (obj) {
            var observable = ko.mapping.fromJS(ko.unwrap(obj));
            if (typeof (self.vmExtend) === 'function') {
                return ko.utils.extend(observable, new self.vmExtend(observable));
            }
            return observable;
        };

        this.onSave = params.onSave;
        this.useRowClick = ko.observable(utils.getParamDefault(params.useRowClick, true));
        // pagination
        this.hiddenRowsCount = ko.observable(0);
        this.lastIndex = 0;

        this.pageSize = ko.observable(utils.getParamDefault(params.pageSize, 10));
        this.totalRecords = ko.observable();
        this.totalPages = ko.computed(function () {
            if (self.totalRecords() > 0)
                return Math.ceil(parseInt(self.totalRecords()) / parseInt(self.pageSize()));
            return 0;
        });
        this.currentPage = ko.observable(1);
        this.orderByCol = ko.observable(params.orderByCol);
        this.orderByDir = ko.observable(utils.getParamDefault(params.orderByDir, 'asc'));
        this.groupByCol = ko.observable(utils.getParamDefault(params.groupByCol, ''));
        this.groupByDir = ko.observable(utils.getParamDefault(params.groupByDir, 'asc'));


        this.showTotalRecords = utils.getParamDefault(params.showTotalRecords, true);
        this.onTemplateRender = function () {
            if (typeof (params.onTemplateRender) === 'function')
                params.onTemplateRender();
            // help
            mgHelp.Init();
        };
        this.indexGrid = utils.getParamDefault(params.indexGrid, false);
        // todo:
        this.exportPDF = function () { };
        this.exportExcel = function () { };
        this.goToPage = function () { };

        this.useHiddenColumns = params.useHiddenColumns || false;
        this.showNewButton = ko.gridEdit.utils.getParamDefault(params.showNewButton, true);
        this.showNewButtonObservable = ko.computed(function () {
            if (params.hasOwnProperty('showNewButtonObservable') && ko.isObservable(params.showNewButtonObservable)) {
                return ko.unwrap(params.showNewButtonObservable);
            } else {
                return this.showNewButton;
            }
        }, this);
        this.showExportButtons = ko.gridEdit.utils.getParamDefault(params.showExportButtons, true);
        this.showTotalRecordsSelect = ko.gridEdit.utils.getParamDefault(params.showTotalRecordsSelect, true);
        this.formId = params.formId; // to edit
        this.toggleNewEditContainer = ko.observable(false);
        //this.toggleNewEditContainer = ko.observable(false).stopPublishingOn(this.templateId + '.toggleNewEditContainer').syncWith(this.templateId + '.toggleNewEditContainer');
        this.debug = ko.gridEdit.utils.getParamDefault(params.debug, false);

        this.scriptInit = function () {
        };
        this.campoFiltroRapido = params.campoFiltroRapido;
        this.showHidden = ko.observable(false);
        this.showHiddenToggle = function () {
            self.showHidden(self.showHidden() ? false : true);
        };
        this.previousVal = '';

        // ---------
        // viewModel
        // ---------

        // Trabalhar com objetos estáticos não seria a solução para a modularização?
        // criar objeto create, igual jquery ui

        if (!this.indexGrid) {
            this.useGuid = true;
            this.data = params.data;
            this.hasData = ko.computed(function () {
                return self.data().length > 0;
            });
            this.itemSelected = ko.observable([]);
            //this.itemSelected = ko.observable([]).syncWith(this.templateId + '.itemSelected');
            this.action = ko.observable();
            this.getGuid = function () {
                function _p8(s) {
                    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
                }
                return _p8() + _p8(true) + _p8(true) + _p8();
            };
            // data item manipulation
            this.newItem = function () {
                // item selected properties must be observables
                var item = new self.modelBase();
                item._id = self.useGuid ? self.getGuid() : self.lastIndex;
                self.itemSelected(self.fromJS(item));
                self.toggleNewEditContainer(true);
                self.action("Adicionar");
            };
            this.editItem = function (obj) {
                var id = null,
                    objSelected = null;

                if (!obj.hasOwnProperty('_id')) {
                    // get context on link edit
                    var c = ko.contextFor(event.target),
                        id = c.$parent._id;
                } else {
                    id = obj._id;
                };

                objSelected = ko.utils.arrayFirst(self.data(), function (item) {
                    return item._id === id;
                });

                // convert toJS to convert observables to static objects, to simulate the "save" behavior
                self.itemSelected(self.fromJS(ko.mapping.toJS(objSelected)));

                //if (self.debug) console.log('opening item to edit', self.itemSelected());

                self.toggleNewEditContainer(true);
                self.action("Editar");
            };

            // save item
            this.saveItem = function (data) {

                var itemSelected = ko.mapping.toJS(self.itemSelected());

                if (self.hasOwnProperty('isValid')) {
                    if (!self.isValid()) {
                        self.showErrors();
                        return false;
                    }
                } else if (self.itemSelected().hasOwnProperty('isValid')) {
                    if (!self.itemSelected().isValid()) {
                        self.itemSelected().showErrors();
                        return false;
                    }
                }
                if (itemSelected.hasOwnProperty('_id')) {
                    self.data.remove(function (item) {
                        return ko.unwrap(item._id) === itemSelected._id;
                    });
                }
                // adiciona no início do índice para visualização
                self.data.unshift(itemSelected);

                //ko.postbox.publish(this.templateId + '.onItemSaved', itemSelected);

                if (self.debug && console)
                    console.log('Saving item: ', itemSelected);

                self.toggleNewEditContainer(false);
                self.currentPage(1);
            };

            // remove item
            this.removeItem = function (obj) {
                var id = null, itemRemoved = [], i = 0, length = 0;

                if (!obj.hasOwnProperty('_id')) {
                    // get context on link edit
                    var c = ko.contextFor(event.target),
                        id = c.$parent._id;
                } else {
                    id = obj._id;
                }

                itemRemoved = self.data.remove(function (item) {
                    return item._id === id;
                });

                if (self.useHiddenColumns) {
                    if (itemRemoved.length > 0) {
                        itemRemoved[0].Ativo = false;
                        self.data.push(itemRemoved[0]);
                    }
                } else if (self.useRemovedColumn) {
                    if (itemRemoved.length > 0) {
                        if (itemRemoved[0].hasOwnProperty(self.removedColumnName)) {
                            itemRemoved[0][self.removedColumnName] = true;
                            self.data.push(itemRemoved[0]);
                        } else {
                            throw ('GridView Error: Property "' + self.removedColumnName + '" not found in viewModel, using setRemovedColumn requires this column.');
                        }
                    }
                }
            }

            // default data pagination        
            self.data.sortByProperty(self.orderByCol(), self.orderByDir(), self);

            this.dataPaged = ko.observableArray();

            ko.computed(function () {
                var groupByCol = self.groupByCol(),
                    groupByDir = self.groupByDir();

                self.data.sortByProperty(self.groupByCol(), self.groupByDir(), self);

            });

            this.onRowAdd = function (el) {
                //debugger;
                //console.log(el);
                //if (el.nodeType === 1) {
                //    $(el).fadeIn();
                //}
            };

            // ---------------------------
            // Paginação ViewModel
            // responsável pela paginação da grid e exibição de dados
            // observa data() e paginações, e retorna dados mapeados
            // ---------------------------
            ko.computed(function () {
                if (self.debug)
                    console.log('getting Rows');

                var resultDataView = ko.unwrap(self.data),
                    currentPage = self.currentPage(),
                    pageSize = self.pageSize(),
                    startIndex = currentPage > 1 ? (((currentPage - 1) * pageSize)) : 0,
                    endIndex = (currentPage * pageSize),
                    index = startIndex,
                    data = null,
                    hiddenRowsCount = 0,
                    useSelection = ko.unwrap(self.useSelection);

                self.previousVal = '';

                // se usar colunas ocultas, espera-se que a viewModel tenha a coluna "Ativo"            
                if (self.useHiddenColumns) {
                    data = ko.utils.arrayFilter(resultDataView, function (item) {
                        item = ko.utils.unwrapObservable(item);
                        if (!item.hasOwnProperty('Ativo'))
                            throw ('A grid está configurada para utilizar a coluna "Ativo", certifique-se que a ViewModel possui essa propriedade');
                        if (!ko.utils.unwrapObservable(item.Ativo))
                            hiddenRowsCount++; // contabiliza colunas ocultas
                        else
                            return ko.utils.unwrapObservable(item.Ativo);
                    });
                    if (!self.showHidden())
                        resultDataView = data;

                } else if (self.useRemovedColumn) {

                    data = ko.utils.arrayFilter(resultDataView, function (item) {
                        item = ko.utils.unwrapObservable(item);
                        if (!item.hasOwnProperty(self.removedColumnName))
                            throw ('A grid está configurada para utilizar a coluna "' + self.removedColumnName + '", certifique-se que a ViewModel possui essa propriedade');
                        else
                            return !ko.utils.unwrapObservable(item[self.removedColumnName]);
                    });
                    resultDataView = data;
                };
                if (useSelection) {
                    data = ko.utils.arrayFilter(resultDataView, function (item) {
                        item = ko.utils.unwrapObservable(item);
                        return ko.utils.unwrapObservable(item._selectedView);
                    });
                    if (self.showOnlySelected() === true)
                        resultDataView = data;
                };
                self.totalRecords(resultDataView.length);
                self.hiddenRowsCount(hiddenRowsCount);


                // efetua paginação
                resultDataView = resultDataView.slice(startIndex, endIndex);
                self.dataPaged([]); // limpa para forçar render e fazer agrupamentos
                self.dataPaged(resultDataView);
            }, this);

            this.api = ko.observable({            	
                getSelectedItem: this.itemSelected,
                newItem: this.newItem,
                removeItem: this.removeItem,
                saveItem: this.saveItem,
                toggle: this.toggleNewEditContainer,
                groupByCol: this.groupByCol,
                groupByDir: this.groupByDir,
                totalRecords: this.totalRecords
            });
        } else {
            // ---------------
            // data from URL
            // ---------------
            this.defaultFilterId = 0;
            this.open = utils.getParamDefault(params.open, false);
            this.openFilter = utils.getParamDefault(params.openFilter, true);
            this.isInitial = ko.observable(true);
            this.showFilter = ko.observable(utils.getParamDefault(params.showFilter, true));
            this.showPanelFilter = ko.observable(utils.getParamDefault(params.showPanelFilter, false));
            this.usePagination = ko.observable(utils.getParamDefault(params.usePagination, true));

            self.defaultFilter = '';

            // filter viewModel constructor
            this.filterModelBase = function () {
                var model = new Object();
                ko.utils.arrayForEach(ko.utils.unwrapObservable(self.columns), function (c) {
                    if (c.hasOwnProperty('id') && c.hasOwnProperty('filter') && c.filter === true) {
                        model[c.id] = c.hasOwnProperty('defaultValue') ? c.defaultValue : null;
                        if (c.hasOwnProperty('defaultFilter') && c.defaultFilter)
                            self.defaultFilterId = c.id;
                    }
                });
                return model;
            };

            this.showGrid = ko.observable(true);
            this.filterData = ko.observable(self.fromJS(new self.filterModelBase()));
            this.defaultFilter = this.filterData()[this.defaultFilterId];

            this.clearFilters = function () {
                // TODO: ver erro
                self.defaultFilter();
                self.filterData(self.fromJS(new self.filterModelBase()));
                //self.defaultFilter = self.filterData()[this.defaultFilterId];                
            };

            this.newUrl = (self.parentId !== null ? params.newUrl + '/' + self.parentId : params.newUrl);
            this.listUrl = params.listUrl;
            this.editUrl = params.editUrl;
            this.data = ko.observableArray([]);

            // export to excel or pdf
            this.exportToPDF = function () {
                debugger;
                self.exportTo('pdf');
            };
            this.exportToExcel = function () {
                self.exportTo('xls');
            };
            this.exportTo = function (type) {

                debugger;
                var post = ko.mapping.toJS(self.filterData());

                post.Page = {
                    CurrentPage: 1,
                    PageSize: 999999, OrderBy: self.orderByCol() + " " + self.orderByDir(),
                    GroupBy: ''
                };

                post.ExportFile = type;
                post.columns = ko.unwrap(self.columns);

                $.ajax({
                    data: JSON.stringify(post),
                    url: params.urlAlternativeExport.length > 0 ? params.urlAlternativeExport : self.listUrl,
                    type: 'POST',
                    beforeSend: function () {
                        Loader.show('Gerando arquivo para download..');
                    },
                    success: function (result) {
                        if (type && type !== '') {
                            var url = '/Export/' + type + '/' + result.Key,
                                iframe = $('<iframe src="' + url + '" style="display:none;"></iframe>');
                            iframe.appendTo('body');
                            setTimeout(function () { iframe.remove(); }, 1000);
                        }
                    },
                    error: function (result) {
                        toastr.error('Erro ao gerar arquivo para download.');
                    }
                }).always(function () {
                    Loader.hide();
                });
            };

            this.ajaxDelay = 500;
            this.refreshValue = ko.observable(0);

            this.refreshData = function () {
                // trigger url computed
                self.loading(true);
                var refreshValue = ko.unwrap(self.refreshCount);
                self.ajaxDelay = 0;
                //self.loading(true);
                self.refreshValue(refreshValue++);
                //self.ajaxDelay = 500;
                //window.setTimeout(function () {
                //    self.loading(false);
                //}, 100)
            };

            var oldFilterData = '';
            var oldPageSize = 0;

            // ---------------------------
            // Paginação URL
            // ---------------------------
            ko.computed(function () {
                var post = ko.mapping.toJS(self.filterData()),
                    postData = JSON.stringify(post),
                    pageSize = self.pageSize(),
                    isInitial = ko.computedContext.isInitial();

                self.previousVal = '';

                // is searching or changing pageSize              
                if (oldFilterData != postData || oldPageSize != pageSize) {
                    self.currentPage(1);
                };
                // is changing filters
                if (oldFilterData != postData) {
                    if (!isInitial)
                        self.loading(true);
                    self.resetSelected();
                }
                oldFilterData = postData;
                oldPageSize = pageSize;

                // trigger computed manually
                self.refreshValue();

                // ignore first bind             
                if (isInitial && !self.open) {
                    return;
                } else {
                    self.isInitial(false);
                }

                if (self.usePagination()) {
                    post.Page = {
                        CurrentPage: self.currentPage(),
                        PageSize: pageSize,
                        OrderBy: (self.orderByCol() + " " + self.orderByDir()),
                        GroupBy: self.groupByCol() == '' ? '' : self.groupByCol() + ' ' + self.groupByDir(),
                        GroupByColumn: self.groupByCol(),
                        GroupByDirection: self.groupByDir()
                    };
                }

                if (self.debug && console) {
                    console.log('search post', JSON.stringify(post));
                }

                $.ajax({
                    beforeSend: function () {
                        self.loading(true);
                    },
                    data: JSON.stringify(post),
                    url: self.listUrl,
                    type: 'POST',
                    success: function (response) {
                        if (response.hasOwnProperty('Data')) {
                            var data = response.Data,
                                dataLength = data.length;

                            if (self.debug && console) {
                                console.log('Post response: ', response);
                            };

                            if (dataLength >= 0) {
                                self.data(response.Data);
                                if (response.hasOwnProperty('Page')) {
                                    self.totalRecords(response.Page.TotalRecords);
                                }
                            }
                            self.loading(false);
                        } else if (Array.isArray(response)) {
                            self.usePagination(false);
                            self.loading(false);
                            self.data(response);
                            self.totalRecords(response.length);
                        };
                    },
                    error: function (response) {
                        self.loading(false);
                        if (console)
                            console.error('GridViewError: ' + response.message, response);
                    }
                });
            }).extend({ rateLimit: { method: "notifyWhenChangesStop", deferred: true, timeout: self.ajaxDelay } });


            this.newItem = function (obj) {
                window.location = self.editUrl + objSelected[primaryKeyColumnId];
            }

            this.api = params.api;

            //this.selectedData = ko.observable(null);
            //ko.computed(function () {

            //});
            this.getSelectedData = function () {
                var dfd = $.Deferred();
                if (self.indexGrid) {
                    if (!self.selectedValuesAdd()) {
                        // selecionar todos faz post para pegar os ids
                        var post = ko.mapping.toJS(self.filterData()),
                            postData = JSON.stringify(post),
                            pageSize = 9999999999;

                        post.Page = {
                            CurrentPage: 1,
                            PageSize: pageSize
                        };
                        if (self.debug && console) {
                            console.log('getting ids', JSON.stringify(post));
                        };
                        $.ajax({
                            data: JSON.stringify(post),
                            url: self.listUrl,
                            type: 'POST',
                            success: function (response) {
                                if (response.hasOwnProperty('Data')) {
                                    var data = response.Data,
                                        dataLength = data.length,
                                        i = 0,
                                        id = null,
                                        arrSelected = [],
                                        selectedValues = self.selectedValues();
                                    if (dataLength >= 0) {
                                        // pega somente Ids e ignora o id já selecionado                                        
                                        for (i; i < dataLength;) {
                                            id = data[i][self.editColumn];
                                            if (selectedValues.indexOf(id))
                                                arrSelected.push(id);
                                            i = i + 1;
                                        }
                                        dfd.resolve([arrSelected, 'In'], self.data, self.selectedValues);
                                    }
                                };
                            },
                            error: function (response) {
                                dfd.reject('Erro ao recuperar Ids selecionados: ' + response.error)
                            }
                        });
                    } else {
                        dfd.resolve([self.selectedValues(), 'In'], self.data, self.selectedValues);
                    }
                } else {
                    dfd.resolve([self.selectedValues(), self.selectedValuesAdd() ? 'In' : 'NotIn'], self.data, self.selectedValues);
                }
                return dfd.promise();
            };

            if (ko.isObservable(this.api)) {
                var exports = $.extend(true, { refresh: self.refreshData, filterData: self.filterData }, (self.useSelection ? {
                    selectedData: self.getSelectedData,
                    resetSelected: self.resetSelected,
                    getData: self.data
                } : {}));

                this.api(exports);
            }
        };

        this.toInt = function (value) {
            if (typeof (value === 'string') && value != null) {
                return Number(value.replace(/\./g, '').replace(/,/g, '.'));
            } else {
                return Number(value);
            }
        };

        ko.computed(function () {
            var index = self.lastIndex,
                data = self.data(), // paged data                
                useSelection = ko.unwrap(self.useSelection);

            // set client temp id and selected columns
            ko.utils.arrayForEach(data, function (item) {

                if (!item.hasOwnProperty('_id')) {
                    item._id = index;
                    index++;
                }
                if (useSelection) {
                    if (!item.hasOwnProperty('_selected')) {
                        //item._selected = ko.observable(false);
                        item._selectedView = ko.observable(false); // just for ui, the real value is _selected                   
                        item._change = function ($data, event) {
                            var editColumn = $data[self.editColumn] != undefined ? $data[self.editColumn] : item._id,
                                add = self.selectedValues().indexOf(editColumn) == -1;
                            if (add) {
                                // add
                                self.selectedValues.push(editColumn);
                            } else {
                                // remove
                                self.selectedValues.remove(editColumn);
                            }
                            state = self.selectAllState();
                        };
                        ko.computed(function () {
                            var selectedAllState = self.selectAllState();
                            item._selectedView(selectedAllState === 'all' ? true : false);
                            // temp
                            if (self.totalPages() === 1) {
                                if (selectedAllState === 'all') {
                                    self.selectedValues.push(item[self.editColumn]);
                                }
                            }
                        });
                    }
                }
            });
            self.lastIndex = index + 1;

            // sets selected values after pagination
            if (useSelection) {
                window.setTimeout(function () {
                    self.setSelectedValues();
                }, 10);
            }
        });

        this.getGroupTextValues = function (value) {
            var g = self.groupBySelectedOptions();

            // check parameter
            if (g.hasOwnProperty('groupTextValues') && typeof (g.groupTextValues) === 'object' && g.groupTextValues.length === 2) {
                // check value
                if (value == 'true' || value === true || (typeof (value) === 'string' && value.length > 0)) {
                    return g.groupTextValues[0] !== 'defaultValue' ? g.groupTextValues[0] : value;
                } else {
                    return g.groupTextValues[1] !== 'defaultValue' ? g.groupTextValues[1] : value;
                }
            } else {
                return value;
            }
        };
        this.onRowRendered = function (rowElement, data) {
            var groupByCol = ko.unwrap(self.groupByCol),
                previousVal = ko.unwrap(self.previousVal);

            if (rowElement[1]) {
                // tr
                if (self.showHidden() && ko.unwrap(data.Ativo) === false)
                    rowElement[1].classList.add('hidden-row');
                if (data.hasOwnProperty('EditarRegistro') && ko.unwrap(data.EditarRegistro) === false)
                    rowElement[1].classList.add('row-prevent-click');
                if (data.hasOwnProperty('_selectedView')) {
                    ko.computed(function () {
                        if (data._selectedView() === true) {
                            rowElement[1].classList.add('table-row-active');
                        } else {
                            rowElement[1].classList.remove('table-row-active');
                        }
                    });
                }
            }
            if (groupByCol && groupByCol.length > 0) {
                // group by col                
                var valorGroupByCol = ko.unwrap(data[groupByCol]);
                if (previousVal === '' || previousVal !== valorGroupByCol) {
                    $('<tr class="tr-column-title ' + ko.unwrap(self.groupByDir) + '"><td colspan="' + self.columnsLength + '">' + self.getGroupTextValues(valorGroupByCol) + '</td></tr>')
                    .insertBefore(rowElement[1]);
                    self.previousVal = valorGroupByCol;
                }
            }
            // Renderiza linha da grid, iterando os dados do elemento da linha atual
        };

        self.getColumnsCSS = function (data) {
            var result = '';
            //if (data && data.sortable && data.id) {
            if (data && data.id) { // Todas as colunas serão ordenáveis de acordo com o Humberto (01/12/15)
                if (data.hasOwnProperty('sortable') && data.sortable == false)
                    return '';
                result = 'sortable ';
                if (data.id === self.orderByCol())
                    result += self.orderByDir() === 'asc' ? 'asc' : 'desc';
                return result;
            } else {
                return '';
            }
        };

        self.update = function (page, pageSize, exportType) {

        };

        self.firstPage = function () {
            self.loading(true);
            self.currentPage(1);
        }
        self.lastPage = function () {
            self.loading(true);
            self.currentPage(Number(self.totalPages()));
        }
        self.nextPage = function () {
            if ((Number(self.currentPage()) + 1) <= Number(self.totalPages())) {
                self.loading(true);
                self.currentPage(Number(self.currentPage()) + 1);
            }
        }
        self.previousPage = function () {
            if (Number(self.currentPage()) > 1 && Number(self.totalPages()) > 1) {
                self.loading(true);
                self.currentPage(Number(self.currentPage()) - 1);
            }
        }
    }

    ko.components.register('grid-view', {
        viewModel: gridEditVM,
        template: gridViewDefaultTemplate,
        synchronous: true
    });

    ko.components.register('grid-view-index', {
        viewModel: gridEditVM,
        template: gridViewIndexTemplate,
        synchronous: true
    });

    ko.bindingHandlers.searchBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {
            // TODO: keyup enter search
        },
        update: {

        }
    };

    ko.bindingHandlers.modalBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {

            var modalElement = $(element),
                form = $('#form-' + vm.templateId);

            modalElement.on('shown.bs.modal', function () {
                //modalElement.attr({ 'tabindex': 0 });
            }).on('hidden.bs.modal', function (e, c) {
                // ignore summernote hidden event
                if (e.target.classList.contains('note-image-dialog'))
                    return;
                vm.toggleNewEditContainer(false);
                vm.itemSelected(null);
            }).on('hide.bs.modal', function () {
            }).on('click', '.button-save', function (e, data) {
                vm.saveItem();
            })

            vm.toggleNewEditContainer.subscribe(function (v) {
                if (v)
                    modalElement.modal('show');
                else
                    modalElement.modal('hide');
            });

        },
        update: function () { }
    };
    ko.bindingHandlers.panelSetBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {
            var panelSetElement = $(element),
                panelElement = panelSetElement.find('.panel-container').first(), // faz o bind no primeiro panel do panelSet, pois será o único que usa a viewmodel da lista.        
                newEditForm = panelElement.find('form');

            // get tabs templates
            $('template').each(function () {
                var item = this;
                if (item.dataset.hasOwnProperty('tabId') && item.dataset.tabId === vm.templateId) {
                    vm.tabs.push({
                        'tabId': item.getAttribute('id'),
                        'tabTitle': item.dataset.hasOwnProperty('tabTitle') ? item.dataset.tabTitle : item.getAttribute('id'),
                        'tabStyle': item.dataset.hasOwnProperty('tabStyle') ? item.dataset.tabStyle : '',
                        'tabView': item.dataset.hasOwnProperty('tabView') ? item.dataset.tabView : '',
                        'tabController': item.dataset.hasOwnProperty('tabController') ? item.dataset.tabController : '',
                        'tabTemplate': ko.observable(item.getAttribute('id'))
                    });
                }
            });

            vm.tabTemplate = ko.observable();

            panelSetElement.panelSet({
                afterOpen: function (e, ui) {
                    var item = ko.contextFor(ui.closest('.panel-container')[0])['$data'];
                    // abrir panel com url e controller                  
                    if (item.hasOwnProperty('tabView') && item.tabView.length > 0 && item.tabController.length > 0) {
                        item.tabTemplate(item.tabId);
                        //$.get(item.tabView, function (htmlResponse) {
                        //$('#tab-' + item.tabId).html(htmlResponse);
                        // retorna controller do documento
                        require([item.tabController], function (tabView) {
                            tabView.bind(0).done(function () {
                                //request.resolve();
                            });
                        });
                        //});
                    }
                },
                afterOpenFirst: function (e, ui) {
                    vm.toggleNewEditContainer(true);
                },
                afterCloseAll: function (e, ui) {
                    vm.toggleNewEditContainer(false);
                    vm.itemSelected([]);
                }
            });

            vm.toggleNewEditContainer.subscribe(function (v) {
                if (v)
                    panelSetElement.panelSet('openFirst');
                else
                    panelSetElement.panelSet('closeAll');
            });


        },
        update: function (element, valueAccessor, allBindings, vm, bindingContext) {
        }
    };
    ko.bindingHandlers.rowEditEnable = {
        init: function (element, valueAccessor, allBindings, vm) {
            if (vm.hasOwnProperty('EditarRegistro')) {
                var editarRegistro = ko.unwrap(vm.EditarRegistro);
                if (editarRegistro === true) {
                    element.classList.remove('disabled');
                } else {
                    element.classList.add('disabled');
                }
            }
        },
        update: function (element, valueAccessor, allBindings, vm) {

        }
    }
    ko.bindingHandlers.rowClick = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {
            var editColumn = vm.editColumn,
                element = $(valueAccessor().element);

            element.on('click', 'tr', function (e) {
                var $target = $(e.target),
                    $currentTarget = $(e.currentTarget);

                if ($target.is('a') || $target.is('i') || $target.is('input') || $target.is('.vanilla-checkbox') || $target.is('.vanilla-checkbox *'))
                    return;
                e.preventDefault();
                e.stopPropagation();

                if ($currentTarget.hasClass('tr-column-title')) {
                    if (vm.groupByDir() === 'asc') {
                        vm.orderByDir('desc');
                        vm.groupByDir('desc');
                    }
                    else {
                        vm.groupByDir('asc');
                        vm.orderByDir('asc');
                    }
                    e.preventDefault();
                    e.stopPropagation();
                    return;
                };

                var data = ko.dataFor(e.currentTarget),
                    isEditable = function () {
                        if (data.hasOwnProperty('EditarRegistro')) {
                            return ko.unwrap(data.EditarRegistro) == true ? true : false;
                        } else {
                            return true;
                        }
                    };

                if (vm.indexGrid) {
                    // use checkbox select
                    if (data.hasOwnProperty('_selectedView')) {
                        $currentTarget.find('.vanilla-checkbox').trigger('click');
                        return;
                    }
                    // custom editUrl data attribute 
                    if ($currentTarget.data().hasOwnProperty('editUrl') && isEditable()) {
                        window.location = $currentTarget.data('editUrl');
                        return;
                    } else if (vm.hasOwnProperty('editUrl') && isEditable()) {
                        // url
                        if (typeof editColumn === 'string' && editColumn.length > 0) {
                            window.location = (vm['parentId'] !== null ? (vm.editUrl + '/' + vm['parentId'] + '/' + data[editColumn]) : (vm.editUrl + '/' + data[editColumn]));
                        } else {
                            if (console && console.warn)
                                console.warn('GridView warning: "editColumn" parameter required to rowClick, provide in the columns parameter');
                        }
                    }
                } else {
                    // vm
                    if (isEditable()) {
                        vm.editItem(data);
                    }
                }
            });
        },
        update: function () {

        }
    };

    ko.bindingHandlers.gridSelectPanelAction = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {

            var $element = $(element),
                vm = bindingContext['$component'];

            vm.selectionPanelIsOpen = ko.observable(false);

            vm.openSelectionPanel = function () {
                $element.velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 200);
                vm.selectionPanelIsOpen(true);
            };
            vm.closeSelectionPanel = function () {
                $element.velocity({ bottom: '-74px', opacity: 0 }, 'easeInOut', 200);
                vm.selectionPanelIsOpen(false);
            };

            $element.find('.cancelar').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                vm.resetSelected();
                vm.selectAllState('none');
                vm.selectAllState.valueHasMutated();
            });
        },
        update: function (element, valueAccessor, allBindings, vm, bindingContext) {
            var $element = $(element),
                vm = bindingContext['$component'],
                useSelection = ko.unwrap(vm.useSelection);

            if (ko.utils.unwrapObservable(vm.selectedValues) <= 0) {
                vm.closeSelectionPanel();
            } else {
                vm.openSelectionPanel();
            }

        }
    }

    ko.bindingHandlers.gridBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {

            var element = $(element);

            element.on('click', 'th', function (e, data) {
                var data = ko.dataFor(e.target),
                    elementDataSortable = $(e.currentTarget).data('sortable');

                //if (data && data.sortable) {
                if (data) { // Todas colunas serão ordenáveis de acordo com o Humberto (01/12/15)
                    if ((data.hasOwnProperty('sortable') && data.sortable == false) || elementDataSortable != undefined && elementDataSortable == false)
                        return;

                    var th = $(this),
                        allThs = th.parent().children('th').not(th),
                        order = 'desc';

                    allThs.removeClass('asc desc');

                    if (th.hasClass('desc')) {
                        th.removeClass('desc').addClass('asc');
                        order = 'asc';
                    }
                    else
                        th.removeClass('asc').addClass('desc');

                    if (vm.hasOwnProperty('listUrl') && vm.listUrl.length > 0) {
                        // ajax sort                        
                        vm.orderByCol(data.id);
                        vm.orderByDir(order);

                        if (data.id === vm.groupByCol()) {
                            vm.groupByDir(order);
                        }
                    } else {
                        vm.data.sortByProperty(data.id, order, vm);
                    };
                }
            });
        },
        update: function () { }
    };

});
