﻿define(['knockout', 'jquery', 'text!../templates/panels-set.html', 'jquery-panel-set'], function (ko, $, panelsTemplate) {
    "use strict";
    
    var counter = 0,
        subs = null;

    var panelsSet = function (params) {
        
        var getDeferredGridContext = function (id) {
            var def = new $.Deferred();
            var ret = null;
            window.setTimeout(function () {
                ret = ko.contextFor(document.getElementById('gv-' + id).childNodes[0]).$component;
                def.resolve(ret);
            }, 900);
            return def.promise();
        };

        var getGridContext = function (id) {
            return ko.contextFor(document.getElementById('gv-' + id).childNodes[0]).$component;
        }

        var getPanels = function (params) {
            var panels = ko.unwrap(params.panels);

            panels = ko.utils.arrayMap(panels, function (panel) {
                var template = ko.unwrap(panel.template),                    
                    data = panel.template.data;

                panel.template.isVmContext = false;
                panel.template.contextType = '';
                panel.template.css = template.hasOwnProperty('css') ? template.css : '';                
                panel.template.id = template.id;
                panel.template.beforeSave = template.hasOwnProperty('beforeSave') ? template.beforeSave : function () { };
                
                if (template.hasOwnProperty('gridContext')) {
                    panel.template.contextType = 'grid';                    
                    panel.template.gvContext = ko.observable({
                        toggleNewEditContainer: getGridContext(template.gridContext).toggleNewEditContainer,
                        itemSelected: getGridContext(template.gridContext).itemSelected,
                        saveItem: function () {
                            panel.template.beforeSave(panel.template.gvContext);
                            getGridContext(template.gridContext).saveItem();
                        }
                    });
                } else if (template.hasOwnProperty('vmContext')) {
                    panel.template.contextType = 'viewModel';
                    panel.template.gvContext = ko.observable({
                        itemSelected: template.vmContext.itemSelected,
                        saveItem: function () {
                            panel.template.beforeSave(template.vmContext);
                            template.vmContext.saveItem();
                        }
                    });
                } else {
                    // uses url and controller
                    panel.template.contextType = 'controller';
                    panel.template.isBounded = false;
                    panel.template.isGridContext = false;
                    panel.template.viewHtml = ko.observable('');
                    panel.template.viewHtmlResponse = ko.observable('');
                    //panel.template.css = 'full';
                }
                return panel;
            });
            
            return panels;
        };
        var panelRender = function (element, data) {          
            if (data.template.hasOwnProperty('controller')) {              
                $.ajax({
                    url: data.template.url                   
                }).done(function (htmlResponse) {
                    data.template.viewHtmlResponse = htmlResponse;
                    data.template.viewHtml(htmlResponse);
                });

            } else {
                // isGridContext
                if (data.template.hasOwnProperty('isGridContext')) {
                }
            }
        };
        return {
            getPanels: new getPanels(params),
            panelsInstance: params.panelsInstance,
            panelRender: panelRender
        }
    };

    var getComponentInstance = $.Deferred();
    var instance = null;   

    // binding handlers
    ko.bindingHandlers.panelsBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {
            var panels = vm.getPanels,
                panelsElement,
                panelLenght = panels.length,
                i = 0;

            window.setTimeout(function () {
                panelsElement = $(element).panelSet({
                	afterOpen: function (e, data) {                		
                        var panelContext = panels[data.index];
                        // is bounded
                        if (panelContext !== undefined && panelContext.template.hasOwnProperty('controller')) {
                            require([panelContext.template.controller], function (view) {
                                if (panelContext.template.isBounded === false || ko.unwrap(panelContext.template.viewHtml) == '') {
                                    // refresh view if its cleared
                                    if (ko.unwrap(panelContext.template.viewHtml) == '') {
                                        panelContext.template.viewHtml(panelContext.template.viewHtmlResponse);
                                    }
                                    view.bind(0).done(function () {
                                        panelContext.template.isBounded = true;
                                    });
                                }
                            });
                        }
                    },
                    afterOpenFirst: function (e, data) {
                    },
                    afterCloseAll: function (e, data) {
                        var panelContext = panels[0];
                        panelContext.template.gvContext().toggleNewEditContainer(false);
                    }
                });
                
                for (i; i < panelLenght;) {
                	var currentPanel = panels[i];
                	if (currentPanel.template.hasOwnProperty('gvContext')) {
                		if (currentPanel.template.gvContext().hasOwnProperty('toggleNewEditContainer')) {
                			counter++;
                			subs = currentPanel.template.gvContext().toggleNewEditContainer.subscribe(function (v) {
                				if (v === true) {
                					PanelSet.openFirst();
                				} else {
                					// Supondo que o primeiro panel seja de contexto de grid
                					PanelSet.closeAll();
                				}
                			});
                		}
                	}
                    i = i + 1;
                };

                $(document).on('keyup', function (e) {
                    if (e.keyCode === 27) {
                        if (panelsElement.panelSet('getActivePanelIndex') === 0)
                            panelsElement.panelSet('closeActivePanel');
                    }
                });

                window.PanelSet = {
                    open: function (e, index) {                        
                        return panelsElement.panelSet('open', e, index);
                    },
                    close: function (e, index) {                        
                        return panelsElement.panelSet('close', e, index);
                    },
                    openFirst: function () {                        
                        return panelsElement.panelSet('openFirst');
                    },
                    openNextPanel: function (e) {
                        return panelsElement.panelSet('openNextPanel', e);
                    },
                    closeAll: function () {                      
                        return panelsElement.panelSet('closeAll');
                    },
                    closeActivePanel: function () { return panelsElement.panelSet('closeActivePanel'); },
                    clearActivePanel: function () { panels[panelsElement.panelSet('getActivePanelIndex')].template.viewHtml(''); }
                }

            }, 100);          

        },
        update: function (element, valueAccessor, allBindings, vm, bindingContext) {
            
        }
    };
    ko.components.register('panels', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                instance = new panelsSet(params);
                return instance;
            }
        },
        template: panelsTemplate
    });

    return getComponentInstance;
});