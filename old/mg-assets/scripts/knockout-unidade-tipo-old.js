﻿(function (factory) {
    // Module systems magic dance.
    if (typeof require === "function" && typeof exports === "object" && typeof module === "object") {
        // CommonJS or Node: hard-coded dependency on "knockout"
        factory(require('jquery'), require("knockout"), require("jquery-inputmask"), require("text!../templates/unidade-tipo.html"), exports);
    } else if (typeof define === "function" && define["amd"]) {
        // AMD anonymous module with hard-coded dependency on "knockout"
        define(["jquery", "knockout", "jquery-inputmask", "text!../templates/unidade-tipo.html", "exports"], factory);
    } else {
        // <script> tag: use the global `ko` object, attaching a `mapping` property
        factory(jQuery, ko, null);
    }
}(function ($, ko, unidadeTipoTemplate, exports) {

    var templateFromUrlLoader = {
        loadTemplate: function (name, templateConfig, callback) {            
            if (templateConfig.fromUrl) {
                // Uses jQuery's ajax facility to load the markup from a file
                var fullUrl = '/mg-assets/templates/' + templateConfig.fromUrl + '?cacheAge=' + templateConfig.maxCacheAge;
                $.ajax({
                    url: fullUrl,
                    type: 'GET',
                    async: true,
                    success: function (markupString) {
                        // We need an array of DOM nodes, not a string.
                        // We can use the default loader to convert to the
                        // required format.
                        ko.components.defaultLoader.loadTemplate(name, markupString, callback);
                    }
                });
            } else {
                // Unrecognized config format. Let another loader handle it.
                callback(null);
            }
        }
    };

    // Register loader
    ko.components.loaders.unshift(templateFromUrlLoader);

    ko.components.register('unidade-tipo', {
        viewModel: function (params) {
            var self = this,
                getParamDefault = function (param, defaultValue) {
                    if (param != undefined)
                        return param;
                    else
                        return defaultValue;
                };
            getParamObservable = function (param) {
                return ko.isObservable(param) ? param : ko.observable(param);
            },
            getParamObservableArray = function (param) {
                return ko.isObservable(param) ? param : ko.observableArray(param);
            },
            jsonify = (function (div) {
                return function (json) {
                    div.setAttribute('onclick', 'this.__json__ = ' + json);
                    div.click();
                    return div.__json__;
                }
            })(document.createElement('div'));

            // ko.utils.unwrapObservable é utilizado quando o valor recebido poderá ser ou não uma observable            
            self.idCampo = getParamDefault(params.idCampo, '');
            self.valorMonetario = getParamDefault(params.valorMonetario, false);
            self.required = getParamDefault(params.required, false);
            self.alteraTipoUnidade = getParamDefault(params.alteraTipoUnidade, false);
            self.alteraUnidade = getParamDefault(params.alteraUnidade, false);
            self.exibeTipoUnidade = getParamDefault(params.exibeTipoUnidade, true);
            self.idUnidadeTipoIn = getParamDefault(ko.utils.unwrapObservable(params.idUnidadeTipoIn), []);
            self.unidades = getParamDefault(params.unidades, []);
            self.label = getParamDefault(params.label, '');
            self.validateBind = ko.utils.extend({ required: self.required, options: { appendErrorsToRoot: true } }, params.validateBind);

            self.unidadeParametroData = getParamDefault(params.unidadeParametroData, null);
            self.unidadeTipoParametroData = getParamDefault(params.unidadeTipoParametroData, null);

            self.permiteUnidadeNula = getParamDefault(params.permiteUnidadeNula, false);

            self.debug = false;

            // se parâmetros são estáticos serão convertidos para observables        
            self.valor = getParamObservable(params.valor);
            self.idUnidade = getParamObservable(params.idUnidade);
            self.idUnidadeIn = getParamObservableArray(params.idUnidadeIn);
            self.idUnidadeTipo = getParamObservable(params.idUnidadeTipo);

            // informações do campo da unidade para a tela
            self.campoUnidade = ko.observableArray();
            self.campoUnidadeTipo = ko.observableArray();

            if (self.unidades.length > 0) {
                // será passado uma lista customizada de unidades
                self.campoUnidade(self.unidades);
            } else {
                ko.computed(function () {
                    var parametrosData = {
                        IdUnidadeTipoIn: self.idUnidadeTipoIn.length > 0 ? ko.toJS(self.idUnidadeTipoIn.join(',')) : null,
                        Ativo: true,
                        Page: {
                            PageSize: 99999, CurrentPage: 1,
                            OrderBy: "Descricao"
                        }
                    };
                    if (self.unidadeTipoParametroData)
                        parametrosData = $.extend(true, parametrosData, jsonify('{' + JSON.stringify(self.unidadeTipoParametroData) + '}'));

                    $.ajax({
                        url: '/Estoque/UnidadeTipo/List',
                        type: "POST",
                        dataType: 'json',
                        async: false, // usa síncrono para definição de observables após bind da lista. TODO: Verificar uma solução p/ assíncrono p/ melhor de desempenho
                        data: JSON.stringify(parametrosData),
                        success: function (result) {
                            // o knockout consegue atualizar o dropdown com elementos deffered.                
                            self.campoUnidadeTipo(result.Data);
                        }
                    });
                }).extend();


                // utilizar após a chamada da lista de tipos
                self.unidadeTipoSelecionado = ko.observable();
                ko.computed(function () {
                    var unidadeTipoSelecionado = ko.utils.arrayFilter(self.campoUnidadeTipo(), function (item) {
                        return item.IdUnidadeTipo === self.idUnidadeTipo();
                    });
                    self.unidadeTipoSelecionado(unidadeTipoSelecionado[0]);
                });

                // É necessário ser computed, pois será disparado quando observable dependente self.idUnidadeTipo() for alterada
                ko.computed(function () {
                    var idUnidade = self.idUnidade(),
                        idUnidadeIn = self.idUnidadeIn();

                    var parametrosData = {
                        IdUnidadeTipo: ko.toJS(self.idUnidadeTipo()), // Ao selecionar uma unidade tipo este computed dispara
                        IdUnidadeIn: idUnidade != null && idUnidade != undefined && idUnidadeIn != null && idUnidadeIn != undefined && idUnidadeIn.length > 0 ? ko.toJS(idUnidadeIn.join(',')): null,
                        Ativo: true,
                        Page: {
                            PageSize: 99999, CurrentPage: 1,
                            OrderBy: "Sigla"
                        }
                    };
                    if (self.unidadeParametroData)
                        parametrosData = $.extend(true, parametrosData, jsonify('{' + self.unidadeParametroData + '}'));

                    $.ajax({
                        url: '/Estoque/Unidade/List',
                        type: "POST",
                        dataType: 'json',
                        async: false, // usa síncrono para definição de observables após bind da lista. TODO: Verificar uma solução assíncrona p/ melhor de desempenho
                        data: JSON.stringify(parametrosData),
                        success: function (result) {
                            if (self.permiteUnidadeNula)
                                result.Data.unshift([]);
                            self.campoUnidade(result.Data);
                        }
                    });
                });
            }

            // utilizar após a chamada da lista de tipos
            // TODO: Impedir computed de disparar quando selecionar um idUnidade nulo
            self.unidadeSelecionadoSigla = ko.observable();
            self.unidadeSelecionadoApresentacao = getParamObservable(params.unidadeSelecionadoApresentacao);
            self.unidadeSelecionadoCasasDecimais = ko.observable(0);
            self.unidadeSelecionadoCasasDecimais.extend({ rateLimit: 10 }); // tempo adicional para atualizar template
            self.unidadeTipoSelecionadoDescricao = ko.observable();
            ko.computed(function () {
                var idUnidade = self.idUnidade(),
                    valor = self.valor();

                if (idUnidade || self.unidades.length > 0) {
                    var unidadeSelecionadoApresentacao = ko.utils.arrayFilter(self.campoUnidade(), function (item) {
                        return item.IdUnidade == idUnidade;
                    });
                    if (unidadeSelecionadoApresentacao.length > 0) {
                        if (valor != null && valor.length > 0)
                            self.unidadeSelecionadoApresentacao(valor + ' (' + unidadeSelecionadoApresentacao[0].Sigla + ')');
                        else
                            self.unidadeSelecionadoApresentacao('');

                        self.unidadeSelecionadoSigla(unidadeSelecionadoApresentacao[0].Sigla);
                        self.unidadeSelecionadoCasasDecimais(unidadeSelecionadoApresentacao[0].CasasDecimais);
                        self.unidadeTipoSelecionadoDescricao(unidadeSelecionadoApresentacao[0].DescricaoUnidadeTipo);
                    }
                } else {
                    self.unidadeSelecionadoCasasDecimais(defaults.mascaraUnidadeTipo.padrao.CasasDecimais);
                    self.unidadeTipoSelecionadoDescricao(defaults.mascaraUnidadeTipo.padrao.Descricao);
                    self.unidadeSelecionadoSigla(defaults.mascaraUnidadeTipo.padrao.Sigla);
                    self.unidadeSelecionadoApresentacao(valor);

                }
            });
            self.valorPlaceholder = ko.computed(function () {
                //return self.valor().toFixed(self.unidadeSelecionadoCasasDecimais()).toString().replace('/\./gi',',');
            });
        },
        template: unidadeTipoTemplate == null ? { fromUrl: 'unidade-tipo.html', maxCacheAge: 9999 } : unidadeTipoTemplate
    });


    ko.bindingHandlers.maskDecimalUnidadeTipo = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {
            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here

            var $el = $(element);

            var obsCasasDecimais = valueAccessor().casasDecimais;
            var obsValor = valueAccessor().valor;

            var arredondaDecimais = function (element) {
                var el = $(element),
                    formattedValue = el.val();

                if (formattedValue !== '') {                    
                    formattedValue = Number(formattedValue.replace(/\./g, '').replace(/,/, '.')).toFixed(obsCasasDecimais()); // ex: 1200.000              

                    el.val(formattedValue.replace(/\./g, ',')).change();
                }
            };

            if (ko.isObservable(obsCasasDecimais)) {
                ko.computed(function () {
                    
                    $el.inputmask('remove')
                       .inputmask('decimal', { groupSeparator: '.', groupSize: 3, radixPoint: ',', autoGroup: true, digits: obsCasasDecimais() })
                       .on('blur', function () { arredondaDecimais(this); });

                    arredondaDecimais($el);
                });
            }

        },
        update: function (element, valueAccessor, allBindings, vm, bindingContext) {
        }
    };

}));