﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery', 'knockout', 'text!../templates/unidade-tipo.html', 'unidades', 'clear-on-esc', 'mask-decimal'], factory);
    } else {
        factory(root.jQuery, root.ko, templateUnidadeTipo, unidadesDeferred);
    }
}(this, function ($, ko, templateUnidadeTipo, unidadesDeferred) {
   
    ko.components.register('unidade-tipo', {
        viewModel: function (params) {  
            var self = this,
                getValueObservable = function (value) {
                    if (ko.isObservable(value))
                        return value;
                    else {
                        return Array.isArray(value) ? ko.observableArray(value) : ko.observable(value);
                    }
                },
                context = params.getContext;
            
            
            this.unidades = ko.observableArray(null);
            this.unidadeSelecionada = ko.observable();
            this.unidadeSelecionadaId = ko.observable();
            this.unidadeSelecionadaCasasDecimais = ko.observable(2);
            this.unidadeSelecionadaSigla = ko.observable('');
            this.unidadeSelecionadaApresentacao = getValueObservable(params.unidadeSelecionadaApresentacao || ko.observable(''));
            this.unidadeSelecionadaTipoDescricao = ko.observable();
            this.label = getValueObservable(params.label || ko.observable(''));
            this.id = params.id;
            this.idUnidade = getValueObservable(params.idUnidade || ko.observable([]));
            this.idUnidadeIn = getValueObservable(params.idUnidadeIn || ko.observableArray([]));
            this.idUnidadeTipo = getValueObservable(params.idUnidadeTipo || ko.observableArray([]));
            this.idUnidadeTipoIn = getValueObservable(params.idUnidadeTipoIn || ko.observableArray([]));
            this.editaUnidade = params.editaUnidade != undefined ? params.editaUnidade : ko.observable(true);
            this.valor = getValueObservable(params.valor);
            this.validateBind = getValueObservable(params.validateBind || { required: false }); // { required: true, min: 10, max: 30 }
            this.validateUnidade = ko.observable(true);

            // TODO: Escolha de unidade obrigatória se valor for diferente de 0
            this.validateUnidade = ko.computed(function () { 
                var valor = ko.utils.unwrapObservable(self.valor);               
                if (valor != null && ko.isObservable(params.idUnidade) ||
                    valor == null && ko.isObservable(params.idUnidade) && Number(ko.utils.unwrapObservable(self.idUnidade)) != 0) {
                    return true;
                } else {
                    return false;
                }
            });

            // 1 - Recuperar todas as unidades e armazenar em memória
            // Usar propriedade isDirty, para determinar quando atualizar a requisição            
            if (window['Unidades'] == undefined) {
                unidadesDeferred.done(function (unidadesArr) {
                    window.Unidades = unidadesArr;
                    self.unidades(unidadesArr);
                });
            } else {
                self.unidades(window.Unidades);
            } 
           
            ko.computed(function () {
                var data = ko.utils.arrayFirst(self.unidades(), function (item) {
                    return self.idUnidade() == item.IdUnidade;
                });
                if (data) {                    
                    self.unidadeSelecionadaCasasDecimais(data.CasasDecimais);
                    self.unidadeSelecionadaSigla(' (' + data.Sigla + ')');
                    self.unidadeSelecionadaId(data.IdUnidade);
                    self.unidadeSelecionadaTipoDescricao(data.DescricaoUnidadeTipo);
                } 
            });

            this.unidadesFiltro = ko.computed(function () {
                // filter by IdUnidadeIn, InUnidadeTipoIn
                var idUnidadeTipoIn = ko.utils.unwrapObservable(self.idUnidadeTipoIn),
                    idUnidadeTipo = Number(ko.utils.unwrapObservable(self.idUnidadeTipo)),
                    idUnidadeIn = ko.utils.unwrapObservable(self.idUnidadeIn),
                    idUnidade = Number(ko.utils.unwrapObservable(self.idUnidade)),
                    unidades = self.unidades();
                // convert single idUnidadeTipo to idUnidadeTipoIn
                if (!isNaN(idUnidadeTipo)) {
                    idUnidadeTipoIn = [idUnidadeTipo];
                }
                // convert single idUnidade to idUnidadeIn
                if (!isNaN(idUnidade)) {
                    idUnidadeIn = [idUnidade];
                }
                if (idUnidadeIn.length > 0 || idUnidadeTipoIn.length > 0) {
                    return ko.utils.arrayFilter(unidades, function (item) {
                        // item expects object                        
                        // filter IdUnidadeIn, IdUnidadeTipoIn
                        if (idUnidadeIn.length > 0 && idUnidadeIn.indexOf(item.IdUnidade) >= 0) {
                            return true;
                        } else if (idUnidadeTipoIn.length > 0 && idUnidadeTipoIn.indexOf(item.IdUnidadeTipo) >= 0) {                           
                            return true;
                        } else {
                            return false;
                        }
                    });
                } else {
                    return unidades;
                }
            });
        },
        template: templateUnidadeTipo
    });   
}));