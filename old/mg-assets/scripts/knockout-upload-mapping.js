﻿var vmMappingToUpload = function (vm) {

    var self = this,
        fileJs = function () {
        this.name = '';
        this.extension = '';
        this.serverPath = '';
        this.serverId = 0;
        this.removed = false;
        this.fileSize = 0;
        this.fileReadProgress = 0;
        this.uploadProgress = 0;
        this.uploadStatus = 0; // 0 default, 1 success, 2 error,
        this.uploadMessage = '';
    };
   
    ko.utils.extend(self, ko.unwrap(vm));

    self.ArquivosDigitaisToUploader = ko.observableArray();

    // map to uploader                
    var mapToUploader = ko.utils.arrayMap(ko.unwrap(self.ArquivosDigitais), function (item) {
        var file = new fileJs();
        file.name = item.Arquivo;
        file.serverPath = item.CaminhoArquivo;
        file.serverId = item.IdArquivoDigital;
        file.friendlyName = item.NomeExibicao;
        file.uploadStatus = 3;
        return file;
    });
    self.ArquivosDigitaisToUploader(mapToUploader);
    console.log('to', self.ArquivosDigitaisToUploader());

    self.ArquivosDigitaisFromUploaderJS = ko.observableArray();
    self.ArquivosDigitaisFromUploaderJS.subscribe(function () {

        var i = 0, mapFromUploader = [];
        var mapFromUploader = ko.utils.arrayMap(ko.utils.unwrapObservable(self.ArquivosDigitaisFromUploaderJS), function (item) {
            var arquivoDigital = new fileJs();
            arquivoDigital.Arquivo = item.name;
            arquivoDigital.CaminhoArquivo = item.serverPath;
            arquivoDigital.Enviado = item.serverId > 0 ? true : false;
            arquivoDigital.IdArquivoDigital = item.serverId > 0 ? item.serverId : null;
            arquivoDigital.NomeExibicao = item.friendlyName;
            return arquivoDigital;
        });

        self.ArquivosDigitais(mapFromUploader);
    });
};