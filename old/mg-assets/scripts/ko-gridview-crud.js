﻿define(function (require, exports) {    
        
    var $ = require('jquery'),
        ko = require('knockout'),
        Gridview = require('ko-gridview').Gridview,
        Utils = require('ko-gridview-utils');

    var Crud = function (params, componentInfo) {

        var exports = {},
            $super = Gridview.prototype,
            $this = this;

        // call superconstructor
        Gridview.call($super, params, componentInfo, true);

        //this.editColumn = '';
        //this.title = ko.gridEdit.utils.getParamDefault(params.title, '');
        //this.addTitle = ko.gridEdit.utils.getParamDefault(params.addTitle, 'Adicionar');
        //this.formId = params.formId; // to edit
        //this.toggleNewEditContainer = ko.observable(false);
        //this.getColumns = function () {

        //    var result = [];

        //    result = ko.utils.arrayFilter(ko.utils.unwrapObservable($super.columns), function (c) {
        //        if (c.hasOwnProperty('id'))
        //            c.displayName = c.hasOwnProperty('displayName') ? c.displayName : c.id;

        //        // get edit column name
        //        if (c.hasOwnProperty('editColumn') && c.editColumn)
        //            self.editColumn = c.id;

        //        return c.hasOwnProperty('header') && c.header;
        //    });
        //    self.columnsLength = result.length;

        //    return result;
        //}();
        // create viewModel from columns configuration
        this.modelBase = function () {
            var model = new Object();
            ko.utils.arrayForEach(ko.utils.unwrapObservable($super.columns), function (c) {
                if (c.hasOwnProperty('id')) {
                    model[c.id] = c.hasOwnProperty('defaultValue') ? c.defaultValue : null;
                }
            });
            return model;
        };
       
        this.dataSelected = ko.observable([]);
        this.action = ko.observable();

        // data item manipulation
        this.newItem = function () {
            // item selected properties must be observables                            
            var item = new self.modelBase();
            item._id = self.lastIndex;
            self.dataSelected(self.fromJS(item));
            //self.toggleNewEditContainer(true);
            self.action("Adicionar");
        };
        this.editItem = function (obj) {
            var id = null,
                objSelected = null;

            if (!obj.hasOwnProperty('_id')) {
                // get context on link edit
                var c = ko.contextFor(event.target),
                    id = c.$parent._id;
            } else {
                id = obj._id;
            };

            objSelected = ko.utils.arrayFirst($super.data(), function (item) {
                return item._id === id;
            });

            // convert toJS to convert observables to static objects, to simulate the "save" behavior
            self.dataSelected(self.fromJS(ko.mapping.toJS(objSelected)));

            if (self.debug) console.log('opening item to edit', self.dataSelected());

            //self.toggleNewEditContainer(true);
            self.action("Editar");
        };

        // save item
        this.saveItem = function (data) {

            var dataSelected = ko.mapping.toJS(self.dataSelected());

            if (self.hasOwnProperty('isValid')) {
                if (!self.isValid()) {
                    self.showErrors();
                    return false;
                }
            }
            if (dataSelected.hasOwnProperty('_id')) {
                self.data.remove(function (item) {
                    return item._id === dataSelected._id;
                });
            }
            // adiciona no início do índice para visualização
            self.data.unshift(dataSelected);

            if (self.debug)
                console.log('Saving item: ', dataSelected);

            //self.toggleNewEditContainer(false);
            self.currentPage(1);
        };

        // remove item from data
        this.removeItem = function (obj) {
            var id = null,
                itemRemoved = [],
                i = 0,
                length = 0;

            if (!obj.hasOwnProperty('_id')) {
                // get context on link edit
                var c = ko.contextFor(event.target),
                    id = c.$parent._id;
            } else {
                id = obj._id;
            }

            itemRemoved = self.data.remove(function (item) {
                return item._id === id;
            });

            if (self.useHiddenColumns) {
                if (itemRemoved.length > 0) {
                    itemRemoved[0].Ativo = false;
                    self.data.push(itemRemoved[0]);
                }
            } else if (self.useRemovedColumn) {
                if (itemRemoved.length > 0) {
                    if (itemRemoved[0].hasOwnProperty(self.removedColumnName)) {
                        itemRemoved[0][self.removedColumnName] = true;
                        self.data.push(itemRemoved[0]);
                    } else {
                        throw ('GridView Error: Property "' + self.removedColumnName + '" not found in viewModel, using setRemovedColumn requires this column.');
                    }
                }
            }
        }

        $(componentInfo.element)
            .on('beforePagination', function (e) {

            })
            .on('afterRowRender', function (e, rowElement, data) {

            })
            .on('overwriteRowData', function (e, item) {
                //console.log(item);
            })
            
        //exports.groupByOptions = self.groupByOptions;
        //exports.getGroupByOptions = self.getGroupByOptions();

        return (this);

    };

    exports.Crud = Crud;
});

