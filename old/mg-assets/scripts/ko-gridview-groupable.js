﻿define(function (require, exports) {    
        
    var $ = require('jquery'),
        ko = require('knockout'),
        Gridview = require('ko-gridview').Gridview,
        Utils = require('ko-gridview-utils');

    var Groupable = function (params, componentInfo) {

        var exports = {},
            $super = {},
            $this = this;

        // call superconstructor
        Gridview.call($super, params, componentInfo, true);       

        // get groupby options
        this.groupByOptions = ko.observableArray([]);
        this.groupByCol = ko.observable(Utils.getParamDefault(params.groupByCol, ''));
        this.groupByDir = ko.observable(Utils.getParamDefault(params.groupByDir, 'asc'));
        
        var getGroupByOptions = function () {
            debugger;
            var result = [];
            result = ko.utils.arrayFilter(ko.utils.unwrapObservable($super.columns), function (c) {
                if (c.hasOwnProperty('groupable') && c.groupable) {
                    if (!c.hasOwnProperty('id'))
                        throw ('GridView Error: Groupable columns requires an ID');

                    c.displayName = c.hasOwnProperty('displayName') ? c.displayName : c.id;
                    return c;
                }
            });
            result.unshift({ id: "", displayName: "Nenhum", groupable: true, defaultValue: "" });

            $this.groupByOptions(result);

            return result.length;
        }();

        var groupBySelectedOptions = ko.pureComputed(function () {
            return ko.utils.arrayFirst(this.groupByOptions(), function (item) {
                return $this.groupByCol() == item.id;
            });           
        }, this);        

        var previousVal = '';

        var getGroupTextValues = function (value) {
            var g = groupBySelectedOptions();

            // check parameter
            if (g.hasOwnProperty('groupTextValues') && typeof (g.groupTextValues) === 'object' && g.groupTextValues.length === 2) {
                // check value
                if (value == 'true' || value === true || (typeof (value) === 'string' && value.length > 0)) {
                    return g.groupTextValues[0] !== 'defaultValue' ? g.groupTextValues[0] : value;
                } else {
                    return g.groupTextValues[1] !== 'defaultValue' ? g.groupTextValues[1] : value;
                }
            } else {
                return value;
            }
        };

        var removeGroupable = function () {
            $(componentInfo.element).find('.tr-column-title').remove();
            $this.groupByCol('');
        };       

        $(componentInfo.element)
            .on('beforePagination', function (e) {
                previousVal = '';
            })
            .on('afterRowRender', function (e, rowElement, data) {
                var groupByCol = ko.unwrap($this.groupByCol);
                
                if (groupByCol && groupByCol.length > 0) {
                    // group by col        
                    if (previousVal === '' || previousVal !== data[groupByCol]) {
                        $('<tr class="tr-column-title ' + $this.groupByDir() + '"><td colspan="' + $super.columnsLength + '">' + getGroupTextValues(data[groupByCol]) + '</td></tr>')
                        .insertBefore(rowElement[1]);
                        previousVal = data[groupByCol];
                    }
                } 
            })

        $this.groupByCol.subscribe(function (val) {
            if (val === '') {                
                removeGroupable();               
                // todo: get default sort values when remove groupable?
                //$this.data.sortByProperty('id', 'asc', $this);
            } else {               
                var groupByCol = ko.unwrap($this.groupByCol),
                    groupByDir = ko.unwrap($this.groupByDir);

                $super.data.sortByProperty(groupByCol, groupByDir, $super);
                //$super.orderByCol(groupByCol);
                //$super.orderByDir(groupByDir);
                $super.refreshData();
            }
        });
        //$super.orderByCol.subscribe(function (val) {
        //    var groupByCol = ko.unwrap($this.groupByCol);
        //    // remove groups when orderByCol is different than groupByCol
        //    if (groupByCol != val) {
        //        removeGroupable();
        //    }
        //});

        // computed for groupable trigger
        ko.computed(function () {
            var groupByDir = ko.unwrap($this.groupByDir),
                groupByCol = ko.unwrap($this.groupByCol);
            
            if (groupByCol !== '') {
                //$super.orderByDir(groupByDir);
                //$super.orderByCol(groupByCol);

                $super.data.sortByProperty(groupByCol, groupByDir, $super);
                //$super.orderByCol(groupByCol);
                //$super.orderByDir(groupByDir);

                //$super.refreshData();
                //$this.goToFirstPage();
            }
        });

        return (this);
    };

    exports.Groupable = Groupable;   
});

