﻿define(function (require, exports) {
        
    var ko = require('knockout'),
        Gridview = require('ko-gridview').Gridview,
        Groupable = require('ko-gridview-groupable').Groupable,
        Crud = require('ko-gridview-crud').Crud,
        template = require('text!../templates/gridview.html');   

    // Gridview.prototype = Groupable;
    
    ko.components.register('gv', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                //Gridview.prototype = new Groupable(params, componentInfo);
                //Groupable.prototype = Object.create(Gridview.prototype, [params, componentInfo]);
                //Groupable.prototype = new Gridview
                //Groupable.prototype.constructor = Groupable;
                debugger;
                Groupable.call(Gridview.prototype, params, componentInfo);
                Crud.call(Gridview.prototype, params, componentInfo);               
                //return new Groupable(params, componentInfo);
                return new Gridview(params, componentInfo);
            }
        },
        //viewModel: { instance: new Gridview },
        template: template
    });
});

