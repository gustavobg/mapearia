﻿define(['jquery', 'knockout', 'ko-gridview'], function ($, ko, sortable) {
    var orderBind = function (element, valueAccessor, allBindings, vm, bindingContext) {

        var element = $(element);

        ko.computed(function () {
            var orderByDir = ko.unwrap(vm.orderByDir),
                orderByCol = ko.unwrap(vm.orderByCol);

            // set order arrows
            var th = $('#column-' + orderByCol),
                   allThs = th.parent().children('th').not(th),
                   order = 'desc';

            allThs.removeClass('asc desc');
            
            th.addClass(orderByDir);

            //if (th.hasClass('desc')) {
            //    th.removeClass('desc').addClass('asc');
            //    order = 'asc';
            //}
            //else
            //    th.removeClass('asc').addClass('desc');

        });

        //element.on('click', '.tr-column-title', function (e, data) {
        //    var groupByDir = ko.unwrap(vm.groupByDir);
        //    vm.groupByDir(groupByDir == 'asc' ? 'desc' : 'asc');
        //    vm.orderByDir(groupByDir == 'asc' ? 'desc' : 'asc')
        //});

        element.on('click', 'th', function (e, data) {          

            var data = ko.dataFor(e.target),
                elementDataSortable = $(e.currentTarget).data('sortable');

            //if (data && data.sortable) {
            if (data) { // Todas colunas serão ordenáveis de acordo com o Humberto (01/12/15)
                if ((data.hasOwnProperty('sortable') && data.sortable == false) || elementDataSortable != undefined && elementDataSortable == false)
                    return;               
                
                var th = $(this),
                    order = 'desc';

                if (th.hasClass('desc'))
                    order = 'asc';
                else
                    order = 'desc';

                vm.data.sortByProperty(data.id, order, vm);

                //if (vm.hasOwnProperty('listUrl') && vm.listUrl.length > 0) {
                //    // ajax sort                        
                //    vm.orderByCol(data.id);
                //    vm.orderByDir(order);

                //    if (data.id === vm.groupByCol()) {
                //        vm.groupByDir(order);
                //    }

                //} else {
                //    vm.data.sortByProperty(data.id, order, vm);
                //};
            }
        });
    };

    ko.bindingHandlers.gvOrderBind = {
        init: orderBind,
        update: function () { }
    };
});


