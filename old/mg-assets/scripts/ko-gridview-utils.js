﻿define(['knockout'], function (ko) {
    //var Utils = (function (ko) {
    return {
            getParamObservable: function (param) {
                return ko.isObservable(param) ? param : ko.observable(param);
            },
            getParamObservableArray: function (param) {
                return ko.isObservable(param) ? param : ko.observableArray(param);
            },
            getParamDefault: function (param, defaultValue) {
                if (param !== undefined)
                    return param;
                else
                    return defaultValue;
            },
            extend: function (base, constructor) {
                var prototype = new Function();
                prototype.prototype = base.prototype;
                constructor.prototype = new prototype();
                constructor.prototype.constructor = constructor;            
            }
        }
    //})(ko);
});
