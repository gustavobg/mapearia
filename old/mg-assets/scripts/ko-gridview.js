﻿define(function (require, exports) {

    var $ = require('jquery'),
        ko = require('knockout'),
        utils = require('ko-gridview-utils');

    require('ko-gridview-sortable-handler');

    ko.observableArray.fn.sortByProperty = function (prop, order, context) {
        // extend observable array     
        //var vm = {};

        context.orderByCol(prop);
        context.orderByDir(order);

        this.sort(function (obj1, obj2) {
            var obj1 = ko.utils.unwrapObservable(obj1[prop]),
                obj2 = ko.utils.unwrapObservable(obj2[prop]);
            if (obj1 == obj2)
                return 0;
            else if (obj1 < obj2)
                return order === 'desc' ? 1 : -1;
            else
                return order === 'desc' ? -1 : 1;
        });
    };
    
    var Gridview = function (params, componentInfo, ignoreComputeds) {
        //var params = args[0],
        //    componentInfo = args[1],
        //    $this = this;       

        var $this = this;

        //if (instance) {
        //    return instance;
        //}
        
        this.id = params.id;
        this.columns = params.columns;
        this.data = utils.getParamObservableArray(params.data);
        this.originalData = ko.unwrap(params.data);
        this.refreshData = function () {            
            //var tempData = ko.unwrap($this.data);
            //$this.data([]);
            //$this.data(tempData);
            //$this.getCurrentPage(1);
            $this.getDataPaged.valueHasMutated();
            //$this.data.valueHasMutated();

        };
        this.hasData = ko.computed(function () {
            return $this.data().length > 0;
        });        
        // pagination
        //this.hiddenRowsCount = ko.observable(0);
        //this.isLoading = ko.observable(false);
        this.lastIndex = 0;
        this.getPageSize = ko.observable(utils.getParamDefault(params.pageSize, 10));
        this.getTotalRecords = ko.observable();
        this.getTotalPages = ko.computed(function () {
            if ($this.getTotalRecords() > 0)
                return Math.ceil(parseInt($this.getTotalRecords()) / parseInt($this.getPageSize()));
            return 0;
        });
        this.getCurrentPage = ko.observable(1);
        this.goToFirstPage = function () {
            //$this.isLoading(true);
            $this.getCurrentPage(1);
        }
        this.goToLastPage = function () {
            //$this.isLoading(true);
            $this.getCurrentPage(Number($this.getTotalPages()));
        }
        this.goToNextPage = function () {
            if ((Number($this.getCurrentPage()) + 1) <= Number($this.getTotalPages())) {
                //$this.isLoading(true);
                $this.getCurrentPage(Number($this.getCurrentPage()) + 1);
            }
        }
        this.goToPreviousPage = function () {
            if (Number($this.getCurrentPage()) > 1 && Number($this.getTotalPages()) > 1) {
                //$this.isLoading(true);
                $this.getCurrentPage(Number($this.getCurrentPage()) - 1);
            }
        }
        //this.goToPage = function () { };
        // order
        this.orderByCol = ko.observable(utils.getParamDefault(params.orderByCol, ''));
        this.orderByDir = ko.observable(utils.getParamDefault(params.orderByDir, 'asc'));
        this.showTotalRecords = utils.getParamDefault(params.showTotalRecords, true);        
        //this.getDataPaged = ko.observableArray();

        this.getColumnsCSS = function (data) {
            var result = '';
            //if (data && data.sortable && data.id) {
            if (data && data.id) { // Todas as colunas serão ordenáveis de acordo com o Humberto (01/12/15)
                if (data.hasOwnProperty('sortable') && data.sortable == false)
                    return '';
                result = 'sortable ';
                if (data.id === $this.orderByCol())
                    result += $this.orderByDir() === 'asc' ? 'asc' : 'desc';
                return result;
            } else {
                return '';
            }
        };
        this.columnsLength = 0;
        this.getColumns = function () {
            var result = [];
            //sumColumns = [];

            result = ko.utils.arrayFilter(ko.utils.unwrapObservable($this.columns), function (c) {
                if (c.hasOwnProperty('id'))
                    c.displayName = c.hasOwnProperty('displayName') ? c.displayName : c.id;

                // get edit column name
                //if (c.hasOwnProperty('editColumn') && c.editColumn)
                //    $this.editColumn = c.id;

                // get edit column name
                //if (c.hasOwnProperty('sumColumn') && c.sumColumn)
                //    sumColumns.push(c.id);

                return c.hasOwnProperty('header') && c.header;
            });
            $this.columnsLength = result.length;
            //$this.sumColumns = sumColumns;

            return result;
        }();

        this.afterRowRender = function (rowElement, data) {
    
            $(componentInfo.element).trigger('afterRowRender', [rowElement, data]);

            //if (rowElement[1]) {
            //    // tr
            //    if ($this.showHidden() && data.Ativo === false)
            //        rowElement[1].classList.add('hidden-row');
            //    if (data.hasOwnProperty('EditarRegistro') && data.EditarRegistro === false)
            //        rowElement[1].classList.add('row-prevent-click');
            //    if (data.hasOwnProperty('_selectedView')) {
            //        ko.computed(function () {
            //            if (data._selectedView() === true) {
            //                rowElement[1].classList.add('table-row-active');
            //            } else {
            //                rowElement[1].classList.remove('table-row-active');
            //            }
            //        });
            //    }
            //}           
            // Renderiza linha da grid, iterando os dados do elemento da linha atual  
        };

        this.i = 0;

       
        // default data pagination           
        $this.data.sortByProperty($this.orderByCol(), $this.orderByDir(), $this);

        // responsável pela paginação da grid e exibição de dados
        // observa data() e eventos de paginações, retorna dados mapeados
        this.getDataPaged = ko.computed(function () {
            //console.log($this.i++);
            var resultDataView = ko.unwrap($this.data),
                currentPage = ko.unwrap($this.getCurrentPage),
                pageSize = ko.unwrap($this.getPageSize),
                startIndex = currentPage > 1 ? (((currentPage - 1) * pageSize)) : 0,
                endIndex = (currentPage * pageSize),
                index = startIndex,
                data = null,
                hiddenRowsCount = 0,
                orderByCol = ko.unwrap($this.orderByCol),
                orderByDir = ko.unwrap($this.orderByDir);

            $(componentInfo.element).trigger('beforePagination', []);

            // se usar colunas ocultas, espera-se que a viewModel tenha a coluna "Ativo"            
            //if ($this.useHiddenColumns) {
            //    data = ko.utils.arrayFilter(resultDataView, function (item) {
            //        item = ko.utils.unwrapObservable(item);
            //        if (!item.hasOwnProperty('Ativo'))
            //            throw ('A grid está configurada para utilizar a coluna "Ativo", certifique-se que a ViewModel possui essa propriedade');
            //        if (!ko.utils.unwrapObservable(item.Ativo))
            //            hiddenRowsCount++; // contabiliza colunas ocultas
            //        else
            //            return ko.utils.unwrapObservable(item.Ativo);
            //    });
            //    if (!$this.showHidden())
            //        resultDataView = data;

            //}
            //else if ($this.useRemovedColumn) {

            //    data = ko.utils.arrayFilter(resultDataView, function (item) {
            //        item = ko.utils.unwrapObservable(item);
            //        if (!item.hasOwnProperty($this.removedColumnName))
            //            throw ('A grid está configurada para utilizar a coluna "' + $this.removedColumnName + '", certifique-se que a ViewModel possui essa propriedade');
            //        else
            //            return !ko.utils.unwrapObservable(item[$this.removedColumnName]);
            //    });
            //    resultDataView = data;
            //};
            //if ($this.useSelection) {
            //    data = ko.utils.arrayFilter(resultDataView, function (item) {
            //        item = ko.utils.unwrapObservable(item);
            //        return ko.utils.unwrapObservable(item._selectedView);
            //    });
            //    if ($this.showOnlySelected() === true)
            //        resultDataView = data;
            //};
            $this.getTotalRecords(resultDataView.length);
            //$this.hiddenRowsCount(hiddenRowsCount);

            // efetua paginação
            resultDataView = resultDataView.slice(startIndex, endIndex);
            //$this.getDataPaged(resultDataView);
            return resultDataView;
        });

        //// ignore multiple computed instances
        //if (ignoreComputeds === true)
        //    return this;


        // overwrite row data from grid
        var lastIndex = 0;

        ko.computed(function () {
            var index = lastIndex,
                data = ko.unwrap($this.data); // paged data                

            // set client temp id
            ko.utils.arrayForEach(data, function (item) {

                $(componentInfo.element).trigger('overwriteRowData', [item]);

                if (!item.hasOwnProperty('_id')) {
                    item._id = index;
                    index++;
                }
            });
            lastIndex = index + 1;
        });
        
        return (this);
        //return Gridview.instance;

        //return {
        //    id: $this.id,
        //    getData: $this.data,
        //    afterRowRender: $this.afterRowRender,
        //    showTotalRecords: $this.showTotalRecords,
        //    getColumns: $this.getColumns,
        //    getColumnsCSS: $this.getColumnsCSS,
        //    getDataPaged: $this.dataPaged,
        //    getTotalRecords: $this.totalRecords,
        //    getTotalPages: $this.totalPages,
        //    getPageSize: $this.pageSize,
        //    getCurrentPage: $this.currentPage,
        //    goToNextPage: $this.nextPage,
        //    goToFirstPage: $this.firstPage,
        //    goToLastPage: $this.lastPage,
        //    goToPreviousPage: $this.previousPage,
        //}
    };

    exports.Gridview = Gridview;


    ko.bindingHandlers.sortBindings = {
        init: function (element, valueAccessor, allBindings, vm, bindingContext) {

            var element = $(element);

            element.on('click', 'th', function (e, data) {
                var data = ko.dataFor(e.target),
                    elementDataSortable = $(e.currentTarget).data('sortable');

                //if (data && data.sortable) {
                if (data) { // Todas colunas serão ordenáveis de acordo com o Humberto (01/12/15)
                    if ((data.hasOwnProperty('sortable') && data.sortable == false) || elementDataSortable != undefined && elementDataSortable == false)
                        return;

                    var th = $(this),
                        allThs = th.parent().children('th').not(th),
                        order = 'desc';

                    allThs.removeClass('asc desc');

                    if (th.hasClass('desc')) {
                        th.removeClass('desc').addClass('asc');
                        order = 'asc';
                    }
                    else
                        th.removeClass('asc').addClass('desc');

                    if (vm.hasOwnProperty('listUrl') && vm.listUrl.length > 0) {
                        // ajax sort                        
                        vm.orderByCol(data.id);
                        vm.orderByDir(order);

                        if (data.id === vm.groupByCol()) {
                            vm.groupByDir(order);
                        }

                    } else {
                        vm.data.sortByProperty(data.id, order, vm);
                    };
                }
            });
        },
        update: function () { }
    };

    //var $this = this;
    //,
    //    element = componentInfo.element,
    //    $element = $(element),
    //    $grid = $element.find('table').first(),
    //    $tbody = $grid.find('tbody').first();

    //this.params = params;

    // basic grid functions
    // list, sort and data pagination          

    //ko.gv = Gridview;
});