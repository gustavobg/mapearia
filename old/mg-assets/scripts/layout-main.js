﻿require(['jquery', 'knockout', 'perfect-scrollbar', 'print-header', 'usuario-perfil-bind', 'jquery-pushmenu', 'antiforgery', 'notifications-bind', 'bootstrap/dropdown'], function ($, ko, Ps, printHeader) {   

    'use strict';

    var loadedSenha = false;

    $('#modal-redefinirsenha').on('click', '#btnRedefinirSenhaSubmit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#frmRedefinirSenha').submit();
    });

    $('#btnRedefinirSenha').on('click', function (e) {
        e.preventDefault();

        var btn = $(this),
            href = btn.attr('href'),
            target = $('#modal-redefinirsenha'),
            form = null;

        if (!loadedSenha) {
            // /Login/RedefinirSenha
            target.find('.modal-body').load(href + ' #container', function () {
                require(['login-redefinir-senha-in'], function (setIdUsuario) {
                    var idUsuario = SessaoUsuario.IdUsuario,
                        form = $('#frmRedefinirSenha');

                    setIdUsuario(idUsuario);

                    loadedSenha = true;
                    target.modal();

                    form.on('reenvia.success', function () {
                    }).on('reenvia.error', function () {
                    });
                });
            });
        } else {
            $('#modal-redefinirsenha').modal('show');
        }
    });

    var vmMenuEmpresas = {
        Empresas: ko.observableArray([]),
        DropdownEmpresasClick: function () {

            $.post('/Corporativo/Pessoa/ListPessoaPersonalizado', JSON.stringify({
                ComPessoa: false,
                ComEmpresa: false,
                ComEmpresaUsuaria: true,
                IdPessoaNotIn: SessaoUsuario.IdUnidadeSelecionado,
                Ativo: true,
                Page: {
                    PageSize: 9999, CurrentPage: 1, OrderBy: 'Nome'
                }
            })).success(function (data) {
                vmMenuEmpresas.Empresas(data.Data);
            });
        }
    };
    //window.vmMenuEmpresas = vmMenuEmpresas;
    var menuFirstBind = true;
    // Menu
    function bindMenu() {
        var getMenu = $.Deferred();

        if (!menuFirstBind) {
            getMenu = $.get('/GrupoUsuarioMenu/RetornaMenuUsuarioLogado');
        }
        else
            renderMenu();

        getMenu.done(function (html) {
            renderMenu(html);
        });

        menuFirstBind = false;
    };

    function renderMenu(html) {
        var menu = $('#mmenuhtml');

        if (menu.hasClass('mmenu-rendered'))
            menu.mmenu('destroy');

        if (html != undefined)
            menu.children('ul:first-child').html(html);

        menu = menu.mmenu({
            afterCreate: function () {
                // insere scrollbar após criar o menu
                var menuList = $('.menu-list');
                menuList.each(function () {
                    Ps.initialize($(this)[0]);
                });
                menuList.addClass('always-visible');

                // agiliza exibição de informações de título de tela 
                menu.find('[data-titulo-tela-set]').on('click', function () {
                    console.log($(this).data('tituloTelaSet'));
                    var tituloTela = $(this).data('tituloTelaSet');
                    if (tituloTela != 'undefined') {
                        if (window.hasOwnProperty('Tela'))
                            window.Tela.TituloTela(tituloTela);
                        if (document.getElementById('tituloTela') != null)
                            document.getElementById('tituloTela').innerHTML = tituloTela;
                    }
                });
            },
            sectionAfterOpen: function (e, d) {
                Ps.update(d.nextSection.children('.menu-list')[0]);
            },
            searchResultsChanged: function (e, el) {
                // atualiza scrollbar quando alterar resultados de busca
                Ps.update($(el)[0]);
            }
        });
        window.Menu = {
            element: menu,
            reload: bindMenu
        }
    }
    bindMenu();

    // hora servidor
    function addMinutes(date, minutes) {
        return new Date(date.getTime() + minutes * 60000);
    };
    function addZero(i) {
        if (i < 10)
            i = "0" + i;
        return i;
    }

    var timeElement = document.getElementById('hora-servidor');
    var tempo = new Date(timeElement.dataset.time);

    window.setInterval(function () {
        tempo = addMinutes(tempo, 1);
        var tempoFormatado = addZero(tempo.getDate()) + '/' + addZero(tempo.getMonth() + 1) + '/' + addZero(tempo.getFullYear()) + ' - ' + addZero(tempo.getHours()) + ':' + addZero(tempo.getMinutes());
        timeElement.innerHTML = tempoFormatado;
        printHeader.dataHora(tempoFormatado);
    }, 60000);

    ko.applyBindings(vmMenuEmpresas, document.getElementById('bind-empresas'));
});