﻿if (typeof (toastr) === 'object') {
    toastr.options = {
        positionClass: 'toast-top-full-width',
        showMethod: "slideDown",
        hideMethod: "slideUp",
        timeOut: "1500",
        showDuration: "50",
        hideDuration: "50"
    };
}