﻿require(['jquery', 'knockout', 'jquery-loader', 'vanillaUniform', 'form-submit', 'bxslider', 'ko-validate', 'jquery-flipper', 'bootstrap', 'bootstrap/alert', 'toastr'], function ($, ko, Loader) {
    var Loader = new Loader({ globalAjaxLoading: false });
    Loader.init();

    function fadeIn(el, display) {
        el.style.opacity = 0;
        el.style.display = display || "block";

        (function fade() {
            var val = parseFloat(el.style.opacity);
            if (!((val += .1) > 1)) {
                el.style.opacity = val;
                requestAnimationFrame(fade);
            }
        })();
    };

    // armazena usuário em sessão
    var vmLogin = function () {
        var lembrar = localStorage.getItem("lembrar");
        this.usuario = ko.observable(lembrar !== null && lembrar === "true" ? localStorage.getItem("lembrar.usuario") : '');  
        this.lembrar = ko.observable(lembrar !== null && lembrar === "true" ? true : false);
        ko.computed(function () {            
            localStorage.setItem("lembrar", this.lembrar());
            if (!this.lembrar()) 
                localStorage.setItem("lembrar.usuario", '');
        }, this);        
    };
    vmLogin = new vmLogin();
    ko.applyBindings(vmLogin, document.getElementById('login'));

    fadeIn(document.getElementById('login'));

    function setGaleria(d) {
        var arquivosDigitais = d.ArquivosDigitais,
            sliderContainer = document.getElementById('slider');
        bxSliderHTML = '<ul style="display: none" id="bxslider" class="bxslider">',
        i = 0;

        for (i; i < arquivosDigitais.length;) {
            bxSliderHTML += '<li><img data-loaded="false" data-index="' + i + '" data-src="' + arquivosDigitais[i].CaminhoArquivo + '" class="bxslider-image" src="/mg-assets/images/transp.gif" /></li>';
            i = i + 1;
        }
        bxSliderHTML += '</ul>';

        sliderContainer.innerHTML = bxSliderHTML;
        fadeIn(sliderContainer.childNodes[0], 'block');

        // Ajusta proporção das imagens do slider

        var width = window.screen.width,
            height = window.screen.height,
            imagens = document.getElementById('bxslider').getElementsByTagName('img'),
            i = 0, total = imagens.length;

        // prop certa 1.77
        for (i; i < total; i++) {
            if ((width / height) > 1.77) {
                imagens[i].style.height = height + 'px';
            } else {
                imagens[i].style.width = width + 'px';
            }
        }
        var slider = $('#bxslider').bxSlider({
            responsive: false,
            auto: true,
            randomStart: true,
            onSliderLoad: function (index) {
                // first image
                var img = $('#bxslider').find('[data-index="' + index + '"]').get(0);
                img.src = img.dataset.src;
                img.addEventListener('load', function () {
                    img.dataset.loaded = true;
                });
            },
            onSlideBefore: function ($slideElement, oldIndex, newIndex) {
                var img = $slideElement.find('img').get(0),
                    nextImg = $('#bxslider').find('[data-index="' + newIndex + '"]').get(0);

                if (img.dataset.loaded.toLowerCase() == 'false') {
                    img.src = img.dataset.src;
                    img.dataset.loaded = true;
                }
                if (nextImg.dataset.loaded.toLowerCase() == 'false') {
                    nextImg.src = nextImg.dataset.src;
                    nextImg.dataset.loaded = true;
                };
            }
        });

    };

    var video = $('#logon').children('video')[0],
        loadedUsuario = false,
        loadedSenha = false,
        toggleVideo = function (target) {
            if (video !== undefined) {
                video.pause();
                target.modal()
                    .on('hidden.bs.modal', function () {
                        if (video !== undefined)
                            video.play();
                    });
            };
        };

        
    $('#modal-reenviarsenha').on('click', '#btnReenviarSenha', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#frmRecuperarSenha').submit();
    });

    $('#btnEsqueciSenha').on('click', function (e) {
        e.preventDefault();

        var btn = $(this),
            href = btn.attr('href'),
            target = $(btn.data('target')),
            form = null,
            message = null;

        $('#message-reenviarsenha').hide();

        if (!loadedSenha) {
            target.find('.modal-body').load(href + ' #container', function () {
                require(['recupera-senha'], function () {
                    loadedSenha = true;
                    target.modal();
                    toggleVideo(target);

                    var form = $('#frmRecuperarSenha');
                    form.on('reenvia.success', function (e, msg) { })
                        .on('reenvia.error', function (e, msg) {});
                });                   
            });
        } else {            
            $('#modal-reenviarsenha').modal('show');
        }
    });

   

    $('#btnEsqueciUsuario').on('click', function (e) {
        e.preventDefault();

        var btn = $(this),
            href = btn.attr('href'),
            target = $(btn.data('target'));

        $('#message-reenviarusuario').hide();

        if (!loadedUsuario) {
            target.find('.modal-body').load(href + ' #container', function () {
                require(['recupera-usuario'], function () {
                    loadedUsuario = true;
                    target.modal();
                    toggleVideo(target);

                    var form = $('#frmRecuperarUsuario');

                    form.on('reenvia.success', function (e, msg) { })
                        .on('reenvia.error', function (e, msg) { });
                });
            });
        } else {            
            $('#modal-reenviarusuario').modal('show');
        }
    });

    $('#txtUsuario').focus();
    setTimeout(function () {
        $('#loginContainer').addClass('active');
    }, 100);   

    // subdominio
    var url = document.URL;

    $.post('/Login/BuscarSubDominio',
        {
            url: url
        },
        function (result) {
            if (result.Tipo == 1) {
                $('#emp').val(result.Data);
                $("#empresas").css("display", "none");
            }
            else if (result.Tipo == 2) {
                var data = eval(result.Data);
                var ddl = $("#ddlEmpresas");
                for (k = 0; k < data.length; k++)
                    ddl.append("<option value='" + data[k].IdEmpresa + "'>" + data[k].Nome + "</option>");

                $("#empresas").css("display", "block");
            }
            setGaleria(JSON.parse(result.DataTema));
        }
    );

    var error = $('#error'),
        errorMessage = error.find('.text');


    $('#btn-login').click(function (e) {
        e.preventDefault();
        var idEmpresa = "";

        if ($("#empresas").css("display") == 'block') {
            if ($('#ddlEmpresas').val() == "") {
                errorMessage.html('Escolha uma empresa');
                error.slideDown();
                $('#ddlEmpresas').focus();
            }
            else {
                idEmpresa = $('#ddlEmpresas').val();
            }
        }
        else {
            idEmpresa = $('#emp').val();
        }
        Loader.show();
        $.post('/Login/Autenticar',
            {
                usuario: $('#txtUsuario').val(),
                senha: $('#txtSenha').val(),
                emp: idEmpresa
            },
            function (result) {
                if (result.Sucesso) {
                    if (vmLogin.lembrar()) {
                        // armazena usuário em local
                        localStorage.setItem("lembrar.usuario", vmLogin.usuario());
                    }                    
                    window.location.href = redirectURL;
                }
                else {
                    Loader.hide();
                    errorMessage.html(result.Mensagem);
                    error.slideDown();
                }
            }
        );
    });

    // IsDebugging
    if (isDebugging !== undefined && isDebugging) {
        var btnLogin = $("#btn-login"),
            ddlEmpresas = $('#ddlEmpresas'),
            txtUsuario = $('#txtUsuario'),
            txtSenha = $('#txtSenha'),
            autoLoginMessage = $('#autologin-message'),
            chkAutoLogin = $('#chk-autologin'),
            autoLoginEnabled = localStorage.getItem("autologin") === null ? "true" : localStorage.getItem("autologin");
            autologin = setTimeout(function () {
            if (autoLoginEnabled === "true") {
                var usuario = localStorage.getItem("usuario") || 'admin',
                    senha = localStorage.getItem("senha") || '123',
                    empresa = localStorage.getItem("empresa") || 1;

                ddlEmpresas.val(empresa);
                txtUsuario.val(usuario);
                txtSenha.val(senha);

                btnLogin.click();
            }
        }, 1000);

        if (autoLoginEnabled === "true") {
            autoLoginMessage.show();
        }

        $('body').on('keydown', function (e) {
            if (e.keyCode === 121) {
                $('#autologin-message').hide();
                chkAutoLogin.flipper('turnOff');
                localStorage.setItem("autologin", false);
                clearTimeout(autologin);
            }
        });

        chkAutoLogin
            .attr({ checked: autoLoginEnabled === "true" ? true : false })
            .insertBefore(btnLogin)
            .flipper({
                'onText': 'Autologin Ativado',
                'offText': 'Autologin Desativado',
                'css': 'warning',
                'width': '200px',
                'afterTurnOn': function (e, c) {
                    localStorage.setItem("autologin", true);
                },
                'afterTurnOff': function (e, c) {
                    localStorage.setItem("autologin", false);
                }
            });


        btnLogin.on('click', function () {
            // set localStorage                                
            localStorage.setItem("empresa", ddlEmpresas.val());
            localStorage.setItem("usuario", txtUsuario.val());
            localStorage.setItem("senha", txtSenha.val());
        });
    }
});