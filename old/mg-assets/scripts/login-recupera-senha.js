﻿require(['jquery', 'knockout', 'toastr', 'form-submit', 'login-defaults', 'ko-validate'], function ($, ko, toastr) {

    var form = $('#frmRecuperarSenha'),
        message = $('<div id="message-reenviarsenha" class="alert alert-icon" style="display: none"></div>').prependTo(form),
        vmSenha = {
        usuario: ko.observable(),
        email: ko.observable(),
        Save: function () {            
            if (vmSenha.isValid()) {
                $.post('/Login/RecuperarAcessoPorEmail',
                   {
                       login: vmSenha.usuario(),
                       email: vmSenha.email()
                   },
                   function (result) {

                       if (result.Sucesso) {                           
                           //toastr.success(result.Mensagem);
                           $('#txtEmail').prop('disabled', true);
                           form.trigger('reenvia.success', [result.Mensagem]);
                           message.addClass('alert-success').removeClass('alert-danger').html('<span class="text">' + result.Mensagem + '</span>').prepend('<i class="material-icons">&#xE876;</i>').slideDown();
                           vmSenha.usuario('');
                           vmSenha.email('');
                       }
                       else {
                           //toastr.error(result.Mensagem);
                           form.trigger('reenvia.error', [result.Mensagem]);
                           message.addClass('alert-danger').removeClass('alert-success').html('<span class="text">' + result.Mensagem + '</span>').prepend('<i class="material-icons">&#xE002;</i>').slideDown();

                       }
                   }
               );
            } else {
                vmSenha.showErrors();
            }
        }
    }

    $('#btn-RecuperarSenha').on('click', function () {
        $('#frmRecuperarSenha').submit();
    })

    ko.applyBindings(vmSenha, document.getElementById('frmRecuperarSenha'));
});

