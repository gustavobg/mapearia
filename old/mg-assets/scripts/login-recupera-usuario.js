﻿define(['jquery', 'knockout', 'toastr', 'form-submit', 'login-defaults', 'ko-validate'], function ($, ko, toastr) {
    var form = $('#frmRecuperarUsuario'),
        message = $('<div id="message-reenviarusuario" class="alert alert-icon" style="display: none;"></div>').prependTo(form),
        vmUsuario = {
        email: ko.observable(),
        Save: function () {
            debugger;
            if (vmUsuario.isValid()) {
                $.post('/Login/RecuperarUsuarioPorEmail',
                    {
                        email: vmUsuario.email()
                    },
                    function (result) {
                        if (result.Sucesso) {
                            //toastr.success(result.Mensagem);
                            // trigger form
                            message.addClass('alert-success').removeClass('alert-danger').html('<span class="text">' + result.Mensagem + '</span>').prepend('<i class="material-icons">&#xE876;</i>').slideDown();
                            form.trigger('reenvia.success', [result.Mensagem]);
                            $('#txtEmailUsuario').prop('disabled', true);
                            vmUsuario.email('');                           
                        }
                        else {
                            //toastr.error(result.Mensagem);
                            message.addClass('alert-danger').removeClass('alert-success').html('<span class="text">' + result.Mensagem + '</span>').prepend('<i class="material-icons">&#xE002;</i>').slideDown();
                            form.trigger('reenvia.error', [result.Mensagem]);
                        }
                    }
                );
            } else {
                vmUsuario.showErrors();
            }
        }
    };

    $('#btnRecuperarUsuario').on('click', function () {
        $('#frmRecuperarUsuario').submit();
    });
   
    $('#btnReenviarUsuario').on('click', function (e) {        
        $('#frmRecuperarUsuario').submit();
    });

    ko.applyBindings(vmUsuario, document.getElementById('frmRecuperarUsuario'));
});