﻿(function (root, factory) {   
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'knockout', 'toastr', 'form-submit', 'ko-validate'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('jquery'), require('knockout'), require('toastr'), require('form-submit'), require('ko-validate'));
    } else {
        // Browser globals
        root.setIdUsuario = factory(root.jQuery, root.ko, root.toastr);
    }
}(this, function ($, ko, toastr) {


    var setIdUsuario = function (idUsuario) {        
        document.getElementById('idUsuario').value = idUsuario;
    };

    var vmSenha = {
        senha: ko.observable(),
        confirma_senha: ko.observable(),
        Save: function (e) {            
            if (vmSenha.isValid()) {
                $.post('/Seguranca/Usuario/AlterarSenhaUsuario',
                  {
                      idUsuario: $('#idUsuario').val(),
                      novaSenha: $('#txtSenha').val()
                  },
                  function (result) {
                      if (result.Sucesso) {
                          toastr.success(!result.hasOwnProperty('Mensagem') ? 'Senha alterada com sucesso!' : result.Mensagem);
                          $('#frmRedefinirSenha').trigger('redefinir.success');
                          $('#modal-redefinirsenha').modal('hide');
                      }
                      else {
                          toastr.error(result.Mensagem);
                          $('#frmRedefinirSenha').trigger('redefinir.error');
                      }
                      vmSenha.senha('');
                      vmSenha.confirma_senha('');
                  }
              );
            } else {
                vmSenha.showErrors();
            }
        }
    }
   
    $('#btn-RedefinirSenha').on('click', function (e) {
        $('#frmRedefinirSenha').submit();
    });

    ko.applyBindings(vmSenha, document.getElementById('frmRedefinirSenha'));

    return setIdUsuario;

}));