﻿require(['jquery', 'knockout', 'toastr', 'form-submit', 'ko-validate'], function ($, ko, toastr) {
    var vmSenha = {
        senha: ko.observable(),
        confirma_senha: ko.observable(),
        Save: function (e) {
            if (vmSenha.isValid()) {
                $.post('/Login/RedefinirSenha',
                  {
                      idUsuario: $('#routeDataId').val(),
                      senha: $('#txtSenha').val()
                  },
                  function (result) {
                      if (result.Sucesso) {
                          toastr.info(result.Mensagem);
                          $('#frmRedefinirSenha').trigger('redefinir.success');
                      }
                      else {
                          toastr.error(result.Mensagem);
                          $('#frmRedefinirSenha').trigger('redefinir.error');
                      }
                      // Ocorrendo algum erro ou não, desabilito os campos.
                      $('#txtSenha').prop('disabled', true);
                      $('#txtNovaSenha').prop('disabled', true);
                      $('#btn-RedefinirSenha').prop('disabled', true);

                      window.setTimeout(function () {
                          window.location = '/Login';
                      }, 5000);
                  }
              );
            } else {
                vmSenha.showErrors();
            }
        }
    }
    

    // E-mail
    $(document).ready(function () {
        $.post('/Login/ValidarToken', {
            id: $('#routeDataId').val()
        },
        function (result) {
            if (result.Sucesso) {
                toastr.info(result.Mensagem);
            }
            else {                
                toastr.error(result.Mensagem);

                $('#txtSenha').prop('disabled', true);
                $('#txtNovaSenha').prop('disabled', true);
                $('#btn-RedefinirSenha').prop('disabled', true);
            }
        });
    });

    $('#btn-RedefinirSenha').on('click', function (e) {
        $('#frmRedefinirSenha').submit();
    });

    ko.applyBindings(vmSenha, document.getElementById('frmRedefinirSenha'));

});