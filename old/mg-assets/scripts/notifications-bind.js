﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'knockout', 'workers'], factory);
    } else {
        // Browser globals
        factory(root.jQuery, root.ko, root.workers);
    }
}(this, function ($, ko, workers) {

    var vmNotificacoes = function () {
      
        var self = this,
            listGroupEl = document.getElementById('notifications').getElementsByClassName('list-group')[0],
            first = true;

        self.listaNovasNotificacoes = ko.observableArray([]);
        self.qtdNovasNotificacoes = ko.observable(0);
        self.possuiNovasNotificacoes = ko.pureComputed(function () {
            return self.qtdNovasNotificacoes() > 0;
        });

        self.getNotificacaoTipoIcone = function (idNotificacaoTipo) {
            var type = 'info';
            switch (idNotificacaoTipo) {
                case 3:
                    type = 'warning';
                    break;
                case 4:
                    type = 'close';
                    break;
                case 9:
                    type = 'check';
                    break;
                default:
                    type = 'info_outline';
            }
            return type;
        };
        self.getNotificacaoTipoCSS = function (idNotificacaoTipo) {
            var type = 'info';
            switch (idNotificacaoTipo) {
                case 3:
                    type = 'btn btn-rounded-icon btn-warning';
                    break;
                case 4:
                    type = 'btn btn-rounded-icon btn-error';
                    break;
                case 9:
                    type = 'btn btn-rounded-icon btn-success';
                    break;
                default:
                    type = 'btn btn-rounded-icon btn-info';
            }
            return type;
        };
        self.getNovasNotificacoes = function () {
            var arrIdNotificacoes = [], i = 0;


            $.post('/Configuracao/NotificacaoWorker/RetornaTotalNotificacoesNovas', { ApenasTotalizador: false })
                .done(function (r) {
                    if (r.Data.length > 0) {
                        self.listaNovasNotificacoes(r.Data);
                        for (i; i < r.Data.length;) {
                            arrIdNotificacoes.push(r.Data[i].IdNotificacaoEnvio);
                            i = i + 1;
                        }
                        self.atualizaDataVisualizacao(arrIdNotificacoes);

                        // set scrollbar
                        if (first) {
                            Ps.initialize(listGroupEl); // need to be set after binding
                            first = false;
                        } else {
                            Ps.update(listGroupEl);
                        }
                    };
                });
        };
        self.atualizaDataVisualizacao = function (arrIdNotificacoes) {
            $.post('/Configuracao/NotificacaoWorker/AtualizaDataVisualizacao', { idsNotificacoesEnvio: arrIdNotificacoes.join(',') });
        };
        ko.computed(function () {

            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                      .toString(16)
                      .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                  s4() + '-' + s4() + s4() + s4();
            };

            workers.assignTask('RetornaTotalNotificacoesNovas', function () {
                //return $.post('/Configuracao/NotificacaoWorker/RetornaTotalNotificacoesNovas', {
                //    ApenasTotalizador: true,
                //    guid: function guid() {
                //        function s4() {
                //            return Math.floor((1 + Math.random()) * 0x10000)
                //              .toString(16)
                //              .substring(1);
                //        }
                //        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                //          s4() + '-' + s4() + s4() + s4();
                //    }()
                //})
                //.done(function (r) {
                //    self.qtdNovasNotificacoes(r.Data);
                //})
            }
            );
        });
    };

    var vmNotificacoes = new vmNotificacoes();

    window.vmNotificacoes = vmNotificacoes;
    

    ko.applyBindings(vmNotificacoes, document.getElementById('notifications'));

}));