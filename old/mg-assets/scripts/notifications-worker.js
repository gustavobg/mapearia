﻿//function getNotificationsInterval() {
//    var self = this,
//        xhr = new XMLHttpRequest(),
//        data = new FormData();

//    data.append('IdTela', this.data);

//    xhr.open('POST', '/Configuracao/NotificacaoWorker/GetNotificacaoTelaExibicao', true);
//    xhr.onreadystatechange = function (e) {
//        if (this.readyState == 4 && this.status == 200) {
//            var response = JSON.parse(xhr.responseText);
//            if (response.length > 0) {
//                self.postMessage(response);
//            }
//        }
//    };
//    xhr.send(data);    
//};
function getNotifications() {
    return
    $.ajax({
        type: "POST",
        async: true,
        url: '/Configuracao/NotificacaoWorker/GetNotificacaoTelaExibicao',
        data: 'idTela=' + Notifications.IdTela,
        success: function (data) {
            self.Trigger(data);
        },
        fail: function(data)
        {
            alert('erro');
        }
    });
}

workers.assignTask('GetNotificacaoTelaExibicao', getNotifications);

//onmessage = function (e) {
//    this.data = e.data;
//    this.setInterval(function () {
//        getNotificationsInterval();
//    }, 600000);
//    // Dez minutos (está complicado em ambiente de desenvolvimento)
//    //console.log('Message received from main script');
//    //var workerResult = 'Result: ' + (e.data);
//    //console.log('Posting message back to main script');
//    //postMessage(workerResult);
//}