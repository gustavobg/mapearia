﻿(function (root, factory) {   
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'workers'], factory);
    } else if (typeof exports === "object") {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require("jquery"), require("workers"));
    } else {
        // Browser globals (root is window)
        root.Notifications = factory(root.jQuery, root.workers);        
    }
}(this, function ($, workers) {

    var exports = {},
        Notifications = {
        workerURL: '/mg-assets/scripts/notifications-worker.js',
        worker: null,
        IdTela: null,
        notificationsList: {},

        GetNotificationsInterval: function () {
            var self = this;
            return $.ajax({
                type: "POST",
                async: true,
                url: '/Configuracao/NotificacaoWorker/GetNotificacaoTelaExibicao',
                data: 'idTela=' + Notifications.IdTela,
                success: function (data) {
                    Notifications.Trigger(data);
                }
            });

        },
        Init: function () {

            workers.assignTask('GetNotificacaoTelaExibicao', Notifications.GetNotificationsInterval);

            //Page.on('page.change', function () {
            //    //Notifications.GetNotificationsInterval();
            //});
        },
        OnHiddenFixedToastr: function () {
            document.body.classList.remove('fixed-top-toastr');
        },
        OnShownFixedToastr: function () {
            document.body.classList.add('fixed-top-toastr');
        },
        Trigger: function (list) {
            // For debug purposes: 
            // Notifications.Trigger([{ 'IdNotificacaoForma': 10, 'IdNotificacaoTipo': 9, 'Corpo': 'Olá mundo', 'Assunto': 'Teste' }])

            var i = 0; length = list.length, n = null;
            for (i; i < length;) {
                n = list[i]; // notification json
                if (!n.hasOwnProperty('IdNotificacaoForma'))
                    return;
                switch (n.IdNotificacaoForma) {
                    case 4:
                        Notifications.TriggerModal(n);
                        break;
                    case 5:
                        Notifications.TriggerModal(n);
                        break;
                    case 6:
                        Notifications.TriggerToastr(n, {});
                        break;
                    case 8:
                        Notifications.TriggerTooltip(n, {});
                        break;
                    case 10:
                        console.log(n);
                        Notifications.TriggerToastr(n, { /*preventSetNotificationViewed: true,*/ preventDuplicates: true, timeOut: 0, closeButton: true, extendedTimeOut: 0, positionClass: "toast-top-full-width fixed", onShown: Notifications.OnShownFixedToastr, onHidden: Notifications.OnHiddenFixedToastr });
                        break;
                    default:

                }
                i = i + 1;
            }
        },
        // Tipos: alerta 3, erro 4, informacao 2, padrao 1
        TriggerModal: function (notification) {
            // using bootbox
            if ($('body').hasClass('modal-open'))
                return;

            var type, options = {};

            switch (notification.IdNotificacaoTipo) {
                case 9:
                    type = 'success';
                    break;
                case 3:
                    type = 'warning';
                    break;
                case 4:
                    type = 'danger';
                    break;
                default:
                    type = '';
            }

            options = {
                message: notification.Corpo,
                title: notification.Assunto,
                className: type == '' ? '' : 'modal-' + type,
                closeButton: false,
                buttons: {
                    success: {
                        label: 'OK',
                        className: "btn-success",
                        callback: function () {
                            Notifications.SetNotificationViewed(notification.IdNotificacaoEnvio);
                        }
                    }
                }
            };

            bootbox.dialog(options, function () {
                //Notifications.SetNotificationViewed(notification.IdNotificacaoEnvio);
            });

        },
        TriggerToastr: function (notification, options) {
            var type = '';
            switch (notification.IdNotificacaoTipo) {
                case 3:
                    type = 'warning';
                    break;
                case 4:
                    type = 'error';
                    break;
                case 9:
                    type = 'success';
                    break;
                default:
                    type = 'info';
            }
            toastr[type](notification.Corpo, notification.Assunto, options);
            //if (!options.hasOwnProperty('preventSetNotificationViewed') || options.preventSetNotificationViewed === false)
            Notifications.SetNotificationViewed(notification.IdNotificacaoEnvio);
        },
        TriggerTooltip: function (notification) {
            if (!notification.TelaCampo.length > 0)
                return;

            var el = $('#' + notification.TelaCampo);

            if (!el.length)
                return;

            var trigger = el.parents('.form-group').stop()
                            .addClass('has-help popover-trigger').find('label').stop()
                            .click(function (e) { e.preventDefault(); }),
                            fieldId = el.attr('id');

            if (!trigger.data('notification-loaded')) {
                trigger.qtip($.extend(defaults.qtip.popover, {
                    content: {
                        text: function (event, api) {
                            window.setTimeout(function () {
                                trigger.data('notification-loaded', 'true');
                                api.set('content.title', notification.Assunto);
                                api.set('content.text', notification.Corpo);
                            }, 10);
                        }
                    }
                }));
            };
        },
        SetNotificationViewed: function (idNotificacaoEnvio) {
            var self = this,
            xhr = new XMLHttpRequest(),
            data = new FormData();

            data.append('idNotificacaoEnvio', idNotificacaoEnvio);

            xhr.open('POST', '/Configuracao/NotificacaoWorker/PostNotificacaoExibida', true);
            xhr.onreadystatechange = function (e) {
                if (this.readyState == 4 && this.status == 200) {
                    // Notificação enviada com sucesso
                }
            };
            xhr.send(data);
        }
    };

    exports = Notifications;

    return exports;
}));


























