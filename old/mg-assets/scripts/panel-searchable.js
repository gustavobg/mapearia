﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery', 'select2'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('jquery'), require('select2'));
    } else {
        // Browser globals
        root.Searchable = factory(root.jQuery);
    }
}(this, function ($) {

    var exports = {},
        Searchable = function (o) {

        var jsonify = (function (div) {
            return function (json) {
                div.setAttribute('onclick', 'this.__json__ = ' + json);
                div.click();
                return div.__json__;
            }
        })(document.createElement('div'));

        o.element.select2({
            placeholder: o.element.attr('placeholder'),
            triggerChange: true,
            allowClear: typeof (o.element.data('allowclear')) == "boolean" ? o.element.data('allowclear') : true,
            multiple: o.element.attr('multiple') == 'multiple',
            ajax: {
                url: o.url,
                quietMillis: 300,
                dataType: 'json',
                type: 'POST',
                data: function (value, currentPage) {
                    var json = {
                        Page: {
                            PageSize: 10, CurrentPage: currentPage,
                            OrderBy: o.ordenacao
                        }
                    };
                    if (o.dadosCompl.length > 0)
                        $.extend(json, jsonify('{' + o.dadosCompl + '}'));

                    if (o.idControleDependencia.length > 0)
                        $.extend(json, jsonify('{' + o.idDependencia + ':' + "'" + $('#' + o.idControleDependencia).val() + "'" + '}'));

                    json[o.campoPesquisa] = value;

                    return JSON.stringify(json);
                },
                results: function (result) {
                    var more = result.Page.CurrentPage < result.Page.TotalPages;
                    var convert;

                    for (var i = 0; i < result.Data.length; i++) {
                        convert = result.Data[i];
                        convert.id = convert[o.campoId || o.campoIdPesquisa];
                        convert.text = function (e) { return e[o.campoValor]; }(convert);
                    }
                    return { results: result.Data, more: more };
                }
            },

            initSelection: function (element, callback) {
                var id = element.val();
                if (id !== "" && id != "0") {
                    var json = {
                        Page: {
                            OrderByColumn: o.ordenacao,
                            OrderByDirection: "asc"
                        }
                    };
                    json[o.campoIdPesquisa] = id;

                    //if (o.dadosCompl.length > 0)
                    //    $.extend(json, jsonify('{' + o.dadosCompl + '}'));

                    //if (o.idControleDependencia.length > 0)
                    //    $.extend(json, jsonify('{' + o.idDependencia + ':' + "'" + $('#' + o.idControleDependencia).val() + "'" + '}'));

                    $.ajax(o.url, {
                        data: JSON.stringify(json),
                        dataType: "json",
                        type: 'POST'
                    }).done(function (result) {
                        if (result.Data.length == 0) {
                            toastr.error('Erro na operação: result.Data esperado!');
                        }

                        var convertArray = result.Data;
                        var convert;

                        if (element.attr('multiple') == 'multiple') {
                            for (var i = 0; i < convertArray.length; i++) {
                                convert = convertArray[i];
                                convert.id = convert[o.campoId];
                                convert.text = function (e) { return e[o.campoValor]; }(convert);
                            }
                        }
                        else {
                            convert = convertArray[0];
                            convert.id = convert[o.campoId];
                            convert.text = function (e) { return e[o.campoValor]; }(convert);
                            convertArray = convert;
                        }
                        callback(convertArray);
                    });
                }
            }
        }).on("change", function (e) {
            //$(this).valid();
        });
    };

    exports = Searchable;

    return exports;

}));