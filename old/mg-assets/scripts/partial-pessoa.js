﻿(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['feedback', 'jquery', 'knockout', 'jquery-panel-set'], factory);
    } else {
        // Browser globals
        factory(root.mgFeedbackBase, root.jQuery, root.ko);
    }
}(this, function (mgFeedbackBase, $, ko) {

    ko.bindingHandlers.pessoaBinding = {
        init: function (element, valueAccessor) {
            var vmPessoaNew = {
                PessoaNatureza: {
                    PessoaFisica: 1,
                    PessoaJuridica: 2
                },
                IdPessoa: ko.observable(null),
                IdsPerfil: ko.observable(null),
                Ativo: ko.observable(true),
                Codigo: ko.observable(null),
                Documento1: ko.observable(null),
                Documento2: ko.observable(null),
                Documento3: ko.observable(null),
                IdPessoaNatureza: ko.observable(null),
                Nome: ko.observable(null),
                NomeSecundario: ko.observable(null),
                Telefone1: ko.observable(null),
                Telefone2: ko.observable(null),
                PalavraChave: ko.observable(null),
                Observacao: ko.observable(null),
                SaveNew: function () {
                    if (vmPessoaNew.isValid()) {
                        $.ajax({
                            data: { 'json': ko.toJSON(vmPessoaNew) },
                            url: '/Corporativo/Pessoa/New/',
                            type: "POST",
                            success: function (result) {
                                if (Route.hasOwnProperty('currentRoute')) {
                                    mgFeedbackBase.feedbackCrudRoute(result, 'Pessoa adicionada com sucesso.', false, function () {
                                        $('#btnPanelClose').click();
                                    });
                                } else {
                                    // fallback to old crud route
                                    mgFeedbackBase.feedbackCrud(result, 'Pessoa adicionada com sucesso.', false, function () {
                                        $('#btnPanelClose').click();
                                    });
                                }
                                // retorna IdPessoa salvo para selecionar dropdown de pessoa
                                // por padrão, id do elemento da pessoa deve ser ddlPessoa                                
                                var context = ko.dataFor(document.getElementById('ddlPessoa'));
                                context.IdPessoa(result.IdPessoa);

                                // reset                            
                                vmPessoaNew.IdPessoa(null),
                                vmPessoaNew.IdsPerfil(null),
                                vmPessoaNew.Ativo(true),
                                vmPessoaNew.Codigo(null),
                                vmPessoaNew.Documento1(null),
                                vmPessoaNew.Documento2(null),
                                vmPessoaNew.Documento3(null),
                                vmPessoaNew.IdPessoaNatureza(null),
                                vmPessoaNew.Nome(null),
                                vmPessoaNew.NomeSecundario(null),
                                vmPessoaNew.Telefone1(null),
                                vmPessoaNew.Telefone2(null),
                                vmPessoaNew.PalavraChave(null),
                                vmPessoaNew.Observacao(null);
                            },
                            complete: function () {

                            }
                        });
                    } else {
                        vmPessoaNew.showErrors();
                    };
                }
            };

            vmPessoaNew.LabelNome = ko.computed(function () {
                if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaFisica)
                    return "Nome";
                else if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaJuridica)
                    return "Razão Social";
            });
            vmPessoaNew.LabelNomeSecundario = ko.computed(function () {
                if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaFisica)
                    return "Apelido";
                else if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaJuridica)
                    return "Nome Fantasia";
            });
            vmPessoaNew.LabelDocumento1 = ko.computed(function () {
                if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaFisica)
                    return "CPF";
                else if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaJuridica)
                    return "CNPJ";
            });
            vmPessoaNew.LabelDocumento2 = ko.computed(function () {
                if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaFisica)
                    return "RG";
                else if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaJuridica)
                    return "Insc. Estadual";
            });
            vmPessoaNew.LabelDocumento3 = ko.computed(function () {
                if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaFisica)
                    return "CNH";
                else if (vmPessoaNew.IdPessoaNatureza() == vmPessoaNew.PessoaNatureza.PessoaJuridica)
                    return "Insc. Municipal";
            });

            var formNew = element.children[0];
            ko.applyBindings(vmPessoaNew, formNew);

        },
        update: function () {

        }
    }

}));

    




