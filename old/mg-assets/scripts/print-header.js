﻿define(['knockout', 'jquery'], function (ko, $) {
    'use strict';

    // Define cabeçalho de impressão
    var vmHeader = {
        tituloTela: ko.observable(),
        nomeFantasia: ko.observable($('#empresaNomeFantasia').text()),
        razaoSocial: ko.observable($('#empresaNome').text()),
        usuario: ko.observable(),
        dataHora: ko.observable($('#hora-servidor').text())
    };
    
    ko.applyBindings(vmHeader, document.getElementById('pageHeaderPrint'));

    return vmHeader;

});