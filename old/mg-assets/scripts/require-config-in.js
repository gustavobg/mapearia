﻿(function (requirejs) {
    'use strict';
    var basePathScripts = '../../App_Assets/publish/scripts/' + (isDebugging ? 'debug' : 'min') + '/';
    var basePathMapScripts = '../../Map_Assets/';
    var mapBower = basePathMapScripts + 'bower_components/';
    var mapLibs =  basePathMapScripts + 'app/libs/';
    var mapScripts = basePathMapScripts + 'app/scripts/';

    var config = {
        baseUrl: '/mg-assets/scripts/',
        waitSeconds: 30,
        shim: {
            'bootstrap': { deps: ['jquery'] },
            'bootstrap/transition': { deps: ['jquery'] },
            'bootstrap/dropdown': { deps: ['jquery'] },
            'bootstrap/button': { deps: ['jquery'] },
            'bootstrap/modal': { deps: ['jquery', 'bootstrap/transition'] },
            'bootstrap/tooltip': { deps: ['jquery', 'bootstrap/transition'] },
            'jquery-loader': { deps: ['jquery'], exports: 'Loader' },
            'knockout-upload-mapping': { deps: ['jquery', 'knockout', 'ko-mapping'], exports: 'vmMappingToUpload' },
            'select2': { deps: ['jquery'] },
            'sammy': ['jquery'],
            'modernizr': { exports: 'Modernizr' },
            'shortcut': { exports: 'shortcut' },
            'datetimepicker/locale': { deps: ['moment'] },
            'notifications-bind': { deps: ['jquery', 'knockout', 'workers'] },
            'defaults': { deps: ['jquery', 'bootbox', 'toastr', 'datetimepicker', 'bootstrap/modal', 'summernote', 'select2/locale', 'datetimepicker/locale'], exports: 'defaults' },
            'jquery-inputmask': { deps: ['jquery'] },
            'velocity-animations': { deps: ['jquery', 'velocity', 'velocity-ui'] },
            'velocity-ui': { deps: ['jquery', 'velocity'] },
            'util': { exports: 'mg', deps: ['antiforgery'] },
            'feedback': { deps: ['jquery', 'toastr'], exports: 'mgFeedbackBase' }            
        },
        paths: {
        	'domReady': 'domReady',
        	'async': 'async',
            // essentials plugins
            'jquery': basePathScripts + 'jquery',
            'knockout': basePathScripts + 'knockout.debug',
            'jquery-ui/core': basePathScripts + 'core',
            'jquery-ui/widget': basePathScripts + 'widget',
            'jquery-ui/sortable': basePathScripts + 'sortable',
            'jquery-ui/mouse': basePathScripts + 'mouse',
            'knockout-postbox': basePathScripts + 'knockout-postbox.min',
            'ko-mapping': basePathScripts + 'knockout.mapping',
            'toastr': basePathScripts + 'toastr',
            'bootstrap/modal': basePathScripts + 'modal',
            'bootstrap/button': basePathScripts + 'button',
            'bootstrap/dropdown': basePathScripts + 'dropdown',
            'bootstrap/transition': basePathScripts + 'transition',
            'bootstrap/tooltip': basePathScripts + 'tooltip',
            'moment': basePathScripts + 'moment',
            'datetimepicker/locale': basePathScripts + 'moment-pt-br',
            'datetimepicker/plugin': basePathScripts + 'bootstrap-datetimepicker',
            'datetimepicker': 'knockout-extension-datetimepicker',
            'daterangepicker/plugin': basePathScripts + 'daterangepicker',
            'daterangepicker': 'knockout-extension-daterangepicker',            
            'ko-validate': basePathScripts + 'knockout-validate',
            'ko-validate-rules': 'knockout-validate-custom-rules',
            'jquery-loader': basePathScripts + 'jquery-loader',
            'bootbox': basePathScripts + 'bootbox',
            'jquery-panel-set': basePathScripts + 'jquery-panel-set',
            'shortcut': basePathScripts + 'shortcut',
            'searchable': 'panel-searchable',
            'sammy': basePathScripts + 'sammy',
            'dateFormat': basePathScripts + 'dateFormat',
            'modernizr': basePathScripts + 'modernizr',
            'jstree': basePathScripts + 'jstree',
            'jstree.search': basePathScripts + 'jstree.search',
            'jstree.checkbox': basePathScripts + 'jstree.checkbox',
            'jstree.dnd': basePathScripts + 'jstree.dnd',
            'jstree.state': basePathScripts + 'jstree.state',
            'jstree.types': basePathScripts + 'jstree.types',
            'jstree.wholerow': basePathScripts + 'jstree.wholerow',
            'jstree.util': 'tree-util',
            'jquery-pushmenu': basePathScripts + 'jquery-pushmenu',
            'jquery-inputmask/plugin': basePathScripts + 'jquery.inputmask.bundle',
            'jquery-inputmask': 'knockout-extension-mask',
            'perfect-scrollbar': basePathScripts + 'perfect-scrollbar',
            'velocity-animations': basePathScripts + 'jquery-velocity-animations',
            'velocity-ui': basePathScripts + 'velocity.ui',
            'velocity': basePathScripts + 'jquery.velocity',
            'jquery-qtip': basePathScripts + 'jquery.qtip',
            'knockout-upload': 'knockout-upload',
            'knockout-upload-simple': 'knockout-extension-upload',
            'default-action': 'knockout-extension-default-action',
            'clear-on-esc': 'knockout-extension-clear-on-esc',
            'button': 'knockout-extension-bs-checked',
            'mask-decimal': 'knockout-extension-mask-decimal',
            'mask-coordenada': 'knockout-extension-mask-coordenada',
            'jquery-flipper/plugin': basePathScripts + 'jquery-flipper',
            'jquery-flipper': 'knockout-extension-flipper',
            'jquery-slider/plugin': basePathScripts + 'slider',
            'jquery-slider': 'knockout-extension-slider',
            'summernote/plugin': basePathScripts + 'summernote',
            'summernote': 'knockout-extension-wysiwyg',
            'select2/locale': 'select2-locale',
            'select2/plugin': basePathScripts + 'select2',
            'select2': 'knockout-extension-select2',
            'jquery-collapsible': 'knockout-extension-collapsible',
            'jquery-collapsible/plugin': basePathScripts + 'jquery-collapsible',
            'qunit': basePathScripts + 'qunit',
            'vanillaUniform/plugin': 'vanilla-uniform',
            'vanillaUniform': 'knockout-extension-vanilla-uniform',
            'bootstrap-select/plugin': basePathScripts + 'bootstrap-select',
            'bootstrap-select': 'knockout-bootstrap-select',
            'alteraCEP': 'knockout-extension-alteracep',
            'feedback': 'feedbackBase',
            'antiforgery': 'ajax-json-antiforgery',
            'util': 'util',
            'stopBindings': 'knockout-extension-stopbindings',
            'unidades': 'unidades',
            //'unidade-tipo': 'knockout-unidade-tipo',
            'unidade-tipo': 'knockout-unidade-tipo',
            //'unidade-tipo-2': 'knockout-unidade-tipo-2',
            'panels': 'knockout-panel-set',
            'gridview': 'knockout-grid-view',
            'defaults': 'defaults',
            'workers': 'workers',
            'notifications': 'notifications',
            'notifications-bind': 'notifications-bind',
            'help': 'help',
            'routes': 'routes',
            'route-header-info': 'Utils/route-header-info',
            'route-back-button': 'Utils/route-back-button',
            'route-bread-crumb': 'Utils/route-bread-crumb',
            'route-action-panel': 'Utils/route-action-panel',
            'route-edit-action': 'Utils/route-edit-action',
            'route-list-action': 'Utils/route-list-action',
            'route-history-action': 'Utils/route-history-action',
            'crud-controller': 'Utils/crud-controller',
            'form-submit': 'knockout-extension-form-submit',
            'tela-bind': 'tela-bind',
            'tooltip': 'init-tooltip',			
        	'aside-panel': 'aside-panel',
            // pages
            'partial-pessoa': 'partial-pessoa',
        	// login pages			
            'login-defaults': 'login-defaults',
            'login-redefinir-senha-in': 'login-redefinir-senha-in',
            'redefinir-senha': 'login-redefinir-senha',
            'chance': 'https://cdnjs.cloudflare.com/ajax/libs/chance/1.0.0/chance.min',
            'colorselector-map': 'knockout-extension-colorselector-map',
        	// map scripts {
				'templates': basePathMapScripts + 'app/templates',
				'colorpicker': mapBower + 'mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker',				
				'text': mapBower + 'text/text',
				'inputmask': mapBower + 'jquery.inputmask/dist/min/jquery.inputmask.bundle.min',
				'intro': mapBower + 'intro.js/intro',
				'tiles.google': mapLibs + 'leaflet.google',        	
				'component.areaPositionInfo': mapScripts + 'component.areaPositionInfo',
				'component.colorSelector': mapScripts + 'component.colorSelector',
				'component.iconSelector': mapScripts + 'component.iconSelector',
				'component.fillOpacitySelector': mapScripts + 'component.fillOpacitySelector',
				'component.strokeWeightSelector': mapScripts + 'component.strokeWeightSelector',
				'leaflet.extensions': mapScripts + 'leaflet.extensions',
				'leaflet.omnivore': mapLibs + 'leaflet.omnivore/leaflet.omnivore.min',
				'turf.overlaps': mapScripts + 'turf.overlaps',
				'map.utils': mapScripts + 'map.utils',
				'map.service': mapScripts + 'map.service',				
				'map.gridlayer': mapScripts + 'map.gridlayer',
				'map.toolbar': mapScripts + 'map.toolbar',
				'map.layerEdit': mapScripts + 'map.layerEdit'
        	// } map scripts 
        }
    };

    requirejs.config(config);

    // requirejs error handler
    requirejs.onError = function (err, x) {        
        // Erros de requireJS.
        // 101 - Módulo não encontrado
        // 102 - Erro de script
        // 103 - Timeout        
        if (err.requireType === 'notloaded')
            return;

        document.body.classList.add('loading-fail');
        document.getElementById('route-containers').getElementsByTagName('div')[0].style.display = 'none';
        document.getElementById('message-error-client').style.display = 'block';
        // todo:  fazer log de erros ser global, para trabalhar com erros de require.js        
        console.log(err.requireType, x, err);

        //if (err.requireType === 'timeout') {
        //}
    };

    return config;

}(requirejs));


//requirejs.config({
//    baseUrl: 'mg-assets/scripts',
//    shim: {        
//        'jquery-loader': { deps: ['jquery'], exports: 'Loader' }
//    },
//    paths: {        
//        'jquery': '../../App_Assets/publish/scripts/debug/jquery',
//        'toastr': '../../App_Assets/publish/scripts/debug/toastr',
//        'antiforgery': 'ajax-json-antiforgery',
//    },
//});



