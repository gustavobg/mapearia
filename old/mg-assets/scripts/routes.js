﻿define(['jquery', 'sammy', 'knockout', 'defaults', 'jquery-loader', 'toastr', 'ko-mapping', 'text!routes-custom.json', 'workers', 'route-edit-action', 'route-list-action', 'route-history-action', 'route-header-info', 'error-handler', 'velocity-animations', 'form-submit', 'bootstrap-select', 'stopBindings'], function (
    $, Sammy, ko, defaults, Loader, toastr, koMapping, routesConfig, workers, edit, list, history, setHeader, log) {

    Loader = new Loader({ globalAjaxLoading: false });
    Loader.init();

    // exposes Knockout to global
    ko.mapping = koMapping;
    window.ko = ko;

    var scrollY = 0,
        getRoutesConfig = function () {
            return JSON.parse(routesConfig);
        };

    // exposes Loader to global
    window.Loader = Loader;

    window.Route = {
    	getGridContextById: function (id) {
    		var gridview = document.getElementById(id);
    		if (gridview) {
    			return ko.contextFor(gridview)['$data'].indexVM;
    		} else {
    			return null;
    		}
    	},
        pushData: {},
        routeOptions: {},
        previousRoute: null,
        currentRoute: null,
        currentRouteController: null,
        saveNew: ko.observable(false),
        quiet: false, // if its true, ignore route actions
        token: null,
        afterBindings: function () {
            document.getElementsByTagName('body')[0].classList.remove('loading-panels');
        },
        setHeaderInfo: function (descricao, codigo) {
            Route.routeOptions.titleCodigo = codigo && codigo.length > 0 ? codigo.toString() : '',
            Route.routeOptions.titleDescricao = descricao;

            setHeader(Route.routeOptions);
        },
        isRedirecting: false,
        redirectToLogin: function (response) {
            require(['toastr'], function (toastr) {
                toastr.options = $.extend(toastr.options, { progressBar: true, timeOut: 3000, extendedTimeOut: 800 });
                if ((response.status == 401 || response.status == 403) && Route.isRedirecting != true) {
                    Route.isRedirecting = true;
                    toastr.error('Ops, parece que sua sessão expirou, você será redirecionado para a página de login em 3 segundos.');
                    window.setTimeout(function () { window.location = '/Login?redir=' + Route.currentRoute.path.replace(/#/gi, '!!') }, 3000);
                }
            });
        },
        redirectRouteAction: function (action) {
            var route = Route.currentRoute;
            if (route.params.hasOwnProperty('modulo') && route.params.hasOwnProperty('area')) {
                window.location.hash = '/' + route.params.modulo + '/' + route.params.area + '/' + action;
                // TODO: verificar bug velocity e remover essa linha
                Route.routeOptions.routeContainer.find('form').find('.panel-action').not('.panel-selected').velocity({ bottom: 0, opacity: 1 }, 'easeInOut', 500);
            }
        },
        listRoutePath: function () {
            var route = Route.currentRoute;
            if (route.params.hasOwnProperty('modulo') && route.params.hasOwnProperty('area'))
                return '/#/' + route.params.modulo + '/' + route.params.area + '/List';
        },
        reload: function (position) {
            position = position == undefined ? Route.currentRoutePosition : position;
            getView(Route.currentRoute, position);
        },
        setToken: function (token) {
            Route.token = token;
        },
        setTokenFromView: function (view) {
            var token = $(view).find('input[name="__RequestVerificationToken"]');
            if (token.length > 0) {
                Route.token = token[0].value;
            }
        },
        getToken: function () {
            return Route.token; //document.getElementsByTagName('form').length > 0 ? document.getElementsByTagName('form')[0].children[0].value : null;
        },
        setCurrentRoute: function (route) {
            Route.currentRoute = route;
            if (route.params.hasOwnProperty('area'))
                Route.currentRouteController = route.params.area;
        },
        showErrorBlock: function (mensagem) {			
        	var message = document.getElementById('message-client');
        	message.innerHTML = '';

        	document.getElementById('message-error-client').style.display = 'block';

        	var divs = document.getElementById('route-containers').getElementsByTagName('div'),
        		i = 0,
        		length = divs.length;
        	for (i; i < length; i++) {
        		divs[i].style.display = 'none';
        	}
        	if (mensagem && mensagem.length > 0) {        		
        		message.innerHTML = mensagem;
        	} else {        		
        		message.innerHTML = 'O suporte foi notificado e nossos desenvolvedores estão trabalhando rapidamente para resolver a questão.';
        	}
        }
    }

    var setRouteBackIds = function (routeObject, routeOptions) {
        // objeto da rota deve ser qualificado, ex: "{ 'id': 'teste', 'url': '/rota/teste/{id}' }"
        var route = routeObject;
        if (typeof (route) === 'string')
            throw ('Route Error: Rota deve ser um objeto qualificado, ex: "{ \'id\': \'teste\', \'url\': \'/rota/teste/{id}\' }"');
        for (var index in route) {
            if (route.hasOwnProperty(index)) {
                // troca {xxx} por respectivos objetos no routeOptions
                var i = 0,
                    url = route[index]['url'],
                    matches = url.match(/{\w*}/gi),
                    match = '',
                    matchObject = '',
                    totalMatches = matches != null ? matches.length : 0;

                for (i; i < totalMatches; i++) {
                    match = matches[i]; // propriedade do objeto routeOptions
                    matchObject = match.replace(/\W/gi, '');
                    url = url.replace(match, routeOptions[matchObject]);
                }
                route[index]['url'] = url;
            }
        }
        return route;
    };
    var getUrlRouteBack = function (routeOptions) {
        // rotas definidas via arquivo de configuração 'routes-custom.json'
        var routes = new getRoutesConfig(),
            routesUrl = routes['route-url'],
            routeBack = {},
            routeBackResult = [],
            i = 0,
            z = 0;

        // método responsável pelo retorno de rotas baseados em URL
        // são utilizados por rotas que não são definidas na view (geralmente telas de histórico)
        routesUrl = setRouteBackIds(routesUrl, routeOptions);

        for (i; i < routesUrl.length; i++) {
            // get current url routeBack
            var currentRouteBack = routesUrl[i],
                currentRouteBackIndex = null;
            if (Route.currentRoute.path == currentRouteBack.url) {
                // percorre propriedade que tem os índices da rota de volta
                for (z; z < currentRouteBack.routeBackIndex.length; z++) {
                    routes[currentRouteBack.routeId] = setRouteBackIds(routes[currentRouteBack.routeId], routeOptions);
                    currentRouteBackIndex = currentRouteBack.routeBackIndex[z];
                    routeBackResult.push(routes[currentRouteBack.routeId][currentRouteBackIndex]);
                }
                routeBackResult.push(currentRouteBack);
                break;
            }
        }
        return routeBackResult.length > 0 ? { routeBack: routeBackResult } : {};
    };
    var getRouteBack = function (data, routeOptions) {
        var dataRoute = data,
            routeBack = {},
            pos = dataRoute.pos,
            routes = new getRoutesConfig(),
            i = 0,
            z = 0;

        // rotas definidas via DOM
        if (dataRoute.hasOwnProperty('routeCustom')) {
            routeBack = dataRoute.routeCustom,
            routeBackResult = [];
            if (routes.hasOwnProperty(routeBack)) {
                // verifica posicionamento das rotas, e exibe rota de volta baseada na url atual 
                routeBack = setRouteBackIds(routes[routeBack], routeOptions);
                for (i; i < routeBack.length; i++) {
                    // get current url routeBack
                    var currentRouteBack = routeBack[i],
                        currentRouteBackIndex = null,
                        currentRoutePath = Route.currentRoute.path.replace(/\/$/gi, '');
                    if (currentRoutePath == currentRouteBack.url) {
                        // percorre propriedade que tem os índices da rota de volta
                        for (z; z < currentRouteBack.routeBackIndex.length; z++) {
                            currentRouteBackIndex = currentRouteBack.routeBackIndex[z];
                            routeBackResult.push(routeBack[currentRouteBackIndex]);
                        }
                        routeBackResult.push(currentRouteBack);
                        break;
                    }
                }
                routeBack = routeBackResult;
            } else {
                //console.error('Route Error: Rota customizada não cadastrada, verifique se a rota "' + routes.id + '" foi cadastrada nas configurações de rotas.');
            }
        } else if (dataRoute.hasOwnProperty('routeBack')) {
            routeBack = setRouteBackIds(dataRoute.routeBack, routeOptions);
        } else {
            throw ('Route Error: A propriedade "route-back-custom" requer parâmetros: id e pos');
        }
        dataRoute.routeBack = routeBack;
        return dataRoute;
    }

    var links = document.getElementById('aside-messages').getElementsByClassName('redirect');
    for (var i = 0; i < links.length; i++) {
    	links[i].addEventListener('click', function (e) {
    		e.preventDefault();
    		window.location.reload();
    	});
    }

    var getView = function (route) {

        if (Route.hasOwnProperty('destroy')) {
            if (typeof (Route.destroy) === 'function') {
                Route.destroy();
                Route['destroy'] = undefined;
            }
        }

        var previousRouteCustom = null;

        if (!route.params.hasOwnProperty('action'))
            return;
        if (route.params.action == 'History') {
            // como a tela de histórico não tem uma propriedade view customizada, (view única), recupera rotas customizadas da anterior, caso houver.
            // funciona somente na navegação linear, e não por acesso direto
            if (Route.hasOwnProperty('routeOptions'))
                previousRouteCustom = Route.routeOptions.hasOwnProperty('routeCustom') ? Route.routeOptions.routeCustom : null;
        }

        var area = route.params.area,
            modulo = route.params.modulo,
            id = route.params.id,
            parentId = route.params.hasOwnProperty('parentId') ? route.params.parentId : null,
            baseUrl = modulo + '/' + area,
            baseRouteUrl = '#/' + baseUrl,
            url = baseUrl,
            callback = function () { },
            data = parentId != null ? { 'parentId': parentId } : {}, // additional options
            action = route.params.action === 'Index' ? 'List' : route.params.action,
            routeOptions = {
                title: '',
                view: false,
                action: action,
                showBreadcrumb: true,
                showActionPanel: true,
                showHeader: true,
                showHeaderTitle: true,
                displayMessage: false,
                id: id,
                parentId: parentId,
                showBackButton: true,
                showSaveNew: false,
                showSaveButton: true,
                showHistoryButton: true,
                titleDescricao: '',
                titleCodigo: ''
            };

        //var actionFormat = action.toLowerCase().split('-'),
        //    format = '';
        //if (actionFormat.length > 1) {
        //    action = actionFormat[1];
        //    format = actionFormat[0];
        //} else {
        //    action = actionFormat[0];
        //}

        // default routeOptions by actions       
        switch (action.toLowerCase()) {
            case 'edit':
                url += '/NewEdit';
                routeOptions.pos = 2;
                callback = edit;
                // save last scroll position                
                scrollY = window.scrollY;
                break;
            case 'view':
                url += '/NewEdit';
                routeOptions.pos = 2;
                routeOptions.view = true;
                routeOptions.showSaveButton = false;
                routeOptions.showHistoryButton = false;
                callback = edit;
                break;
            case 'new':
                url += '/NewEdit';
                routeOptions.pos = 2;
                routeOptions.id = 0;
                callback = edit;
                break;
            case 'history':
                url = '/Historico/Index?obj=' + area + '&' + 'id=' + id;
                routeOptions = $.extend(true, routeOptions, { pos: 3, data: { obj: area, id: id } });
                callback = history;
                break;
            case 'list':
                routeOptions.pos = 1;
                routeOptions.showActionPanel = false;
                routeOptions.showBackButton = false;
                routeOptions.showBreadcrumb = true;
                url += '/Index';
                callback = list;
                break;
            default:
                // other action                
                routeOptions.pos = 2;
                routeOptions.viewName = action;
                url += '/' + action;
                callback = edit;
                break;
        };

        // close menu
        Menu.element.mmenu('menuHide');
        // add loading message
        document.body.classList.add('loading-panels');
        document.body.classList.remove('loading-fail');

        Route.currentRoute = route;

        routeOptions.modulo = modulo;
        routeOptions.area = area;
        routeOptions.url = url;
        routeOptions.baseRouteUrl = baseRouteUrl;

        workers.pauseTasks();

        // get view html            
        $.get(url, data, function (htmlResponse) {

            workers.resumeTasks();

            var viewHtml = document.createElement('div');
            viewHtml.innerHTML = htmlResponse;

            var routeExtension = viewHtml.getElementsByTagName('routeOptions'),
                $routeExtension = $(routeExtension);

            $('.message-container').hide();

            if (routeExtension.length > 0) {
                if ($routeExtension.data().hasOwnProperty('routeCustom')) {
                    // route back 
                    routeOptions = $.extend(true, routeOptions, getRouteBack($routeExtension.data(), routeOptions));
                }
                // extend html routeOptions
                routeOptions = $.extend(true, routeOptions, $routeExtension.data());
            } else {
                // get url routeBacks
                routeOptions = $.extend(true, routeOptions, getUrlRouteBack(routeOptions));
            }

            // displayMessage são mensagens de sistema, ex: 404
            if (routeOptions.showHeader && routeOptions.displayMessage == false) {
                document.getElementById('content-header-toolbar').style.display = 'block';
                require(['tela-bind'], function (tela) {
                    tela.bindTelaInfo(baseUrl + '/Index');
                    tela.bindFavorito('/' + baseRouteUrl + '/Index');
                });
            } else {
                routeOptions.showHeaderTitle = false;
            }
            document.getElementById('content-header-toolbar').style.display = routeOptions.showHeaderTitle ? 'block' : 'none';
            document.getElementById('aside-navbar').style.display = routeOptions.showHeader ? 'block' : 'none';

            if (routeOptions.displayMessage) {
                document.getElementsByTagName('body')[0].classList.add('display-message');
                Route.afterBindings();
            } else {
                document.getElementsByTagName('body')[0].classList.remove('display-message');
            }

            // hide all containers            
            $('#route-containers > div').hide();
            $('.modal-route').modal('hide');

            var $view = $('#route-container-' + routeOptions.pos);

            routeOptions.routeContainer = $view;

            Route.routeOptions = routeOptions;
            Route.currentRoutePosition = routeOptions.pos;

            // extend pushData params
            if (Route.hasOwnProperty('pushData') && Route.pushData !== null) {
                var pushData = Route.pushData;
                if (pushData.hasOwnProperty('routeOptions')) {
                    routeOptions = $.extend(true, routeOptions, pushData.routeOptions);
                }
            }

            // check if list is bounded, or if parentId is different           
            if ($view.data('view') == baseRouteUrl + '/List' && ($view.data().hasOwnProperty('parentId') ? ($view.data('parentId') == routeOptions.parentId ? true : false) : false)) {
                // List is bounded
                // hide all
                $('#route-containers > div').hide();
                // show view
                //if (format === 'm') {
                //    $view.show().modal();
                //} else {
                $view.show();
                //}

                if (routeOptions.hasOwnProperty('message') && routeOptions.message == true)
                    $view.html(htmlResponse);

                // go to action
                if (!(routeOptions.hasOwnProperty('quiet') && routeOptions.quiet == true))
                    callback(routeOptions);
            } else {
                // show view            
                //view.show().html(viewHtml.html());
                //if (format === 'm') {
                //    $('<div class="modal modal-route"><div class="modal-dialog"><div class="modal-content"><a id="btnBack" href="#">VOlta</a>' + htmlResponse + "<div></div></div>").modal();
                //} else {
                $view.show().html(htmlResponse);
                //}
                // go to action
                if (!(routeOptions.hasOwnProperty('quiet') && routeOptions.quiet == true))
                    callback(routeOptions);
            };

            $view.data('view', baseRouteUrl + '/' + action);
            $view.data('parentId', routeOptions.parentId);

            // get token from last view
            Route.setTokenFromView($view);

        }).fail(function (err) {
            if (err.status == 401 || err.status == 403) {
                //Route.redirectToLogin(err);
                SessaoUsuario.exibeSessaoExpirada();
            } else {
                if (err.status == 404) {
                    document.getElementById('message-error-404').style.display = 'block';
                } else {                    
                    // erro fatal
                    document.getElementById('message-error-client').style.display = 'block';
                }
                document.body.classList.add('loading-fail');
                document.getElementById('route-containers').getElementsByTagName('div')[0].style.display = 'none';
            }            
        });
    };

    Sammy('#content', function () {

        var jsonify = (function (div) {
            return function (json) {
                div.setAttribute('onclick', 'this.__json__ = ' + json);
                div.click();
                return div.__json__;
            }
        })(document.createElement('div'));

        // home refresh
        if (document.getElementById('home-dashboard')) {
            document.getElementById('home-dashboard').style.display = 'block';
        }
        //document.getElementById('home-dashboard').style.display = 'block';
        document.body.classList.remove('loading-panels');
        document.body.classList.remove('loading-fail');

        var self = this;

        this.after(function () {
            Route.quiet = false;
        });

        this.before(function () {
            // clear pushData        	
            if (Route.hasOwnProperty('pushData') && Route.pushData !== null) {
                Route.pushData = null;
            }
        });

        // plugin para utilizar 

        // rota inteligente com parâmetros
        // #/r/:modulo/:area/:action/p/{objeto}?
        this.get(/#\/r\/(.*)\/p\/(.*)/, function () {
            var context = this;
            debugger;
            window.history.replaceState(null, null, '#/' + this.params['splat'][0]);
            this.redirect('#/' + this.params['splat'][0]);

            Route.pushData = jsonify(this.params['splat'][1]);

            return false;
        });

        this.get('#/?:modulo/:area/?', function () {
            if (Route.quiet)
                return;
            this.redirect('#/' + this.params.modulo + '/' + this.params.area + '/List');
        });
        this.get('#/?:modulo/:area/:action/?', function () {
            if (Route.quiet)
                return;
            getView(this);
        });
        this.get('#/?:modulo/:area/:action/:id/?', function () {
            if (Route.quiet)
                return;

            // parentId 'New'
            if (this.params.action === 'New') {
                this.params.parentId = this.params.id;
                getView(this);
            } else if (this.params.action === 'List') {
                this.params.parentId = this.params.id;
                getView(this);
            } else if (this.params.action === 'Ativar') {

                var self = this;
                $.post(self.params.modulo + '/' + self.params.area + '/Ativar/' + self.params.id).done(function (data) {
                    if (data.response.Response.Sucesso == true) {
                        self.redirect('#/' + self.params.modulo + '/' + self.params.area + '/List');
                    }
                    else {
                        toastr.err(data.response.Response.MensagemFinal);
                        self.redirect('#/' + self.params.modulo + '/' + self.params.area + '/List');
                    }
                });

            } else if (this.params.action === 'Inativar') {

                var self = this;
                $.post(self.params.modulo + '/' + self.params.area + '/Inativar/' + self.params.id).done(function (data) {
                    if (data.response.Response.Sucesso == true) {
                        self.redirect('#/' + self.params.modulo + '/' + self.params.area + '/List');
                    }
                    else {
                        toastr.err(data.response.Response.MensagemFinal);
                        self.redirect('#/' + self.params.modulo + '/' + self.params.area + '/List');
                    }
                });
            } else {
                getView(this);
            }

        });
        // parentId
        this.get('#/?:modulo/:area/:action/:parentId/:id/?', function () {
            if (Route.quiet)
                return;
            getView(this);
        });

        this.notFound = function () {
        }
    }).run();

    // tratamento de erros de transição de rota causados por cliente
    window.onerror = function (err) {

    };
});