﻿define(['jquery', 'toastr', 'knockout', 'print-header'], function ($, toastr, ko, printHeader) {

    "use strict";

    var favorito = function (routeUrl) {

        if (document.getElementById('ctnAlternarFavorito') != null) {

            document.getElementById('ctnAlternarFavorito').innerHTML = '<button type="button" id="btnAlternarFavorito" class="btn-rounded-icon btn-primary btn-inverted btn-fav"><i class="fa fa-star"></i><i class="fa fa-star-o"></i></button>';

            var btnAlternarFavorito = $("#btnAlternarFavorito");

            // Obs: se add = true ação e ícone serão de remoção
            var toggleFavoritos = function (favoritado) {
                btnAlternarFavorito.hasClass('favoritado')
                    ? btnAlternarFavorito.removeClass('favoritado').attr('title', 'Adicionar tela aos favoritos')
                    : btnAlternarFavorito.addClass('favoritado').attr('title', 'Remover tela dos favoritos');
            };

            $.ajax("/Configuracao/Favorito/List", {
                data: JSON.stringify({
                    Url: routeUrl,
                    Page: {
                        OrderByColumn: "Descricao", OrderByDirection: "asc"
                    }
                }),
                dataType: "json",
                type: 'POST'
            }).done(function (result) {
                var url = '',
                    favoritado = result.Data[0] != null ? true : false;

                favoritado ? btnAlternarFavorito.addClass('favoritado').attr('title', 'Remover tela dos favoritos') : btnAlternarFavorito.removeClass('favoritado').attr('title', 'Adicionar tela aos favoritos');

                btnAlternarFavorito.off('click');
                btnAlternarFavorito.on('click', function () {
                    var favoritado = btnAlternarFavorito.hasClass('favoritado') ? true : false;
                    // se está favoritado, irá remover
                    $.ajax('/Configuracao/Favorito/' + (favoritado ? 'Remover' : 'Adicionar'), {
                        data: { url: routeUrl },
                        type: 'POST'
                    }).done(function (result) {
                        if (result.Sucesso == true) {                            
                            toastr.success('Tela ' + (favoritado ? 'removida do' : 'adicionada ao') + ' menu "Favoritos"');
                            toggleFavoritos();
                            Menu.reload();
                        }
                        else {
                            toastr.error('Erro ao ' + (favoritado ? 'remover' : 'adicionar') + ' o favorito');
                        }
                    });
                });
            });
        };
    },
    bind = function (telaUrl) {
        var tituloTela = document.getElementById('tituloTela');
        //tituloTela.classList.add('in');        
        // get tela info
        //Velocity(tituloTela, { "width": "0px" });
        $.post('/Configuracao/Tela/ListTelaCampo', { url: telaUrl }, function (tela) {
            
            if (tela.Descricao != null && tela.Descricao != '') {
                window.setTimeout(function () {
                    //tituloTela.innerHTML = tela.Descricao;
                    document.title = tela.Descricao;
                }, 100);
            }
            // notificações de tela
            require(['help', 'knockout'], function (mgHelp, ko) {
                mgHelp.IdTela = tela.IdTela;
                mgHelp.Fields = tela.Campos;
                mgHelp.Init();
            });

            window.Tela = {
                TituloMenu: tela.TituloMenu,
                TituloTela: ko.observable(tela.TituloTela),
                Descricao: tela.Descricao
            };

            ko.computed(function () {
                var tituloTela = ko.unwrap(Tela.TituloTela);
                $('#tituloTela').html(tituloTela);
                printHeader.tituloTela(tituloTela);
                //$('#tituloTelaPrint').html(Tela.TituloTela());
            });

            // btnHelp
            // bind helpButton
            var btnHelp = $('#btn-help').off('click');

            if (tela.TituloAjuda != null && tela.CorpoAjuda != null &&
                tela.TituloAjuda.length > 0 && tela.CorpoAjuda.length > 0) {
                btnHelp.off('click').show();
                btnHelp.on('click', function (e) {
                    require(['jquery', 'help', 'bootstrap/modal'], function ($, mgHelp) {
                        var modal = $('#modal-help'),
                        title = modal.find('.modal-title'),
                        body = modal.find('.modal-body');

                        e.preventDefault();

                        title.html(tela.TituloAjuda);
                        body.html(tela.CorpoAjuda);

                        modal.modal({ keyboard: true }).on('hidden.bs.modal', function () { body.html(''); });
                    });
                });
            } else {
                btnHelp.hide();
            }
        });
    };

    return {
        bindTelaInfo: bind,
        bindFavorito: favorito
    };

});