﻿define(['jquery', 'jstree', 'jstree.search', 'jstree.checkbox'], function ($) {

    var exports = {};

    function getSelectedNodes(obj, getObjects) {
        var nodes = obj.jstree('get_selected', true), count = 0, i = 0, nodesSelected = [];
        for (i; i <= nodes.length;) {
            if (typeof (nodes[i]) == "object") {
                count++;
                if (getObjects) {
                    nodesSelected.push(nodes[i]);
                }
            }
            i = i + 1;
        }
        return getObjects ? nodesSelected : count;
    };

    function getTextSelected(tree, length) {
        return (length + ' ite' + (length > 1 ? 'ns' : 'm') + ' selecionado' + (getSelectedNodes(tree) > 1 ? 's' : ''));
    };

    function searchEvents(tree, input) {
        var to = false,
            clearContainer = input.next(),
            alertEmpty = tree.prev();

        clearContainer.click(function () {
            input.val('');
            tree.jstree('clear_search');
            alertEmpty.hide();
            tree.show();
        });

        alertEmpty.find('.link-edit').on('click', function (e) {
            e.preventDefault();
            clearContainer.trigger('click');
        });

        input.keyup(function (e) {
            if (to) { clearTimeout(to); }

            if (e.keyCode == 27) {
                clearContainer.trigger('click');
            }
            to = setTimeout(function () {
                var v = input.val();
                tree.jstree(true).search(v);

                if (v.length > 0 && tree.find('.jstree-search').length == 0) {
                    alertEmpty.show();
                    tree.hide();
                }
                else {
                    alertEmpty.hide();
                    tree.show();
                }

                // toggle clear
                //if (v.length === 0)
                //    clearContainer.hide();
                //else
                //    clearContainer.show();

            }, 250);
        });
    }  
    exports.getSelectedNodes = getSelectedNodes;
    exports.getTextSelected = getTextSelected;
    exports.searchEvents = searchEvents;    

    return exports;
});

