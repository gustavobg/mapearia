﻿define(['jquery'], function ($) {
    var paramsData = {
        Page: {
            PageSize: 99999, CurrentPage: 1,
            OrderBy: "Sigla"
        }
    }, 
    unidade = function (idUnidade, idUnidadeTipo, sigla, casasDecimais, descricaoUnidadeTipo) {
        this.IdUnidade = idUnidade;
        this.IdUnidadeTipo = idUnidadeTipo;
        this.Sigla = sigla;
        this.CasasDecimais = casasDecimais;
        this.DescricaoUnidadeTipo = descricaoUnidadeTipo;
        
    },
    deferred = new $.Deferred();
    
    $.ajax({
        url: '/Estoque/Unidade/List',
        type: "POST",
        dataType: 'json',
        data: JSON.stringify(paramsData),
        async: false,
        success: function (result) {
            var data = result.Data,
                i = 0,
                length = data.length,
                item = null,
                arrUnidades = [];

            for (i; i < length;) {
                item = data[i];
                arrUnidades.push(new unidade(item['IdUnidade'], item['IdUnidadeTipo'], item['Sigla'], item['CasasDecimais'], item['DescricaoUnidadeTipo']));
                i = i + 1;
            }
            arrUnidades.unshift(new unidade(null, null, '', 0, ''));
            //console.log(arrUnidades);
            deferred.resolve(arrUnidades);
        }
    });

    return deferred;
});