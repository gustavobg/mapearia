﻿define(['jquery', 'knockout', 'print-header'], function ($, ko, printHeader) {

    "use strict";

    var vmUsuarioPerfil = function () {
        var self = this;
        this.ArquivoDigital = ko.observable(SessaoUsuario.ArquivoDigital);
        this.ArquivoDigitalCSS = ko.pureComputed(function () {
            return 'url(' + this.ArquivoDigital() + ')';
        }, this);
        this.Nome = ko.observable(SessaoUsuario.Nome);
        this.NomeSecundario = ko.observable(SessaoUsuario.NomeSecundario);
        this.NomeCompleto = ko.pureComputed(function () {
            return this.Nome() + '<br />' + this.NomeSecundario();
        }, this);
        this.Email = ko.observable(SessaoUsuario.Email);
        this.Telefone1 = ko.observable(SessaoUsuario.Telefone1);
        this.AtualizaUsuarioPerfil = function () {
            $.post('/Corporativo/UsuarioPerfil/GetUsuarioPerfil').done(function (d) {
                var arquivoDigital = d.ArquivoDigital != null && d.ArquivoDigital.hasOwnProperty('CaminhoArquivo') ? d.ArquivoDigital.CaminhoArquivo : SessaoUsuario.CaminhoImagem;
                self.ArquivoDigital(arquivoDigital);
                self.Nome(d.Nome);
                self.NomeSecundario(d.NomeSecundario);
                self.Email(d.Email);
                self.Telefone1(d.Telefone1);

                // Atualiza global SessaoUsuario
                SessaoUsuario.Nome = d.Nome;
                SessaoUsuario.NomeSecundario = d.NomeSecundario;
                SessaoUsuario.Email = d.Email;
                SessaoUsuario.Telefone1 = d.Telefone1;
                SessaoUsuario.ArquivoDigital = arquivoDigital;
            });
        };
        ko.pureComputed(function () {
            printHeader.usuario(ko.unwrap(self.Nome));
        });
    };
    var vmUsuarioPerfil = new vmUsuarioPerfil();

    // Atualiza cabeçalho de impressão   
    printHeader.usuario(SessaoUsuario.Nome);

    document.getElementById('alterarPerfil').addEventListener('click', function () {
        document.getElementById('usuarioPerfil').classList.remove('open');
    });

    ko.applyBindings(vmUsuarioPerfil, document.getElementById('usuarioPerfil'));

    return vmUsuarioPerfil;

});