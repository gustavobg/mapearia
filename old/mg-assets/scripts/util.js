﻿define(['knockout', 'jquery'], function (ko, $) {
    "use strict";

    var mg = {
        DTOResponseBase: {
            Identificador: {
                RegistroObsoleto: 1,
                RegistroDuplicado: 2,
                FalhaValidacao: 3
            }
        },
        isBound: function(id) {
        	return !!ko.dataFor(document.getElementById(id));
        },
        insertCSS: function (url) {
        	var link = document.createElement("link");
        	link.type = "text/css";
        	link.rel = "stylesheet";
        	link.href = url;
        	document.getElementsByTagName("head")[0].appendChild(link);
        },
        getUnformattedNumber: function (formattedValue) {
            // expects 'R$ 1.800,00' => 1800.00
            return Number(formattedValue.replace(/[^\d,]/g, '').replace(/,/g, '.'));
        },
        getApiFromList: function (routeIndex) {
            // método para acessar a viewmodel da grid na página de listagem (crud) e retornar a api (observable)
            debugger;
            routeIndex = routeIndex == undefined || routeIndex == null ? 1 : routeIndex;            
            var routeContainer = document.getElementById('route-container-' + routeIndex),
                gvIndexElement = routeContainer != null ? routeContainer.getElementsByTagName('grid-view-index') : null,
                gvIndexElementVmContext = gvIndexElement != null && gvIndexElement.length > 0 ? ko.contextFor(gvIndexElement[0]) : null,
                gvApi = null,
                api = {};

            if (gvIndexElementVmContext != null && gvIndexElementVmContext.hasOwnProperty('$data')) {
                gvApi = gvIndexElementVmContext['$data'];
                if (gvApi.hasOwnProperty('indexVM') && gvApi.indexVM.hasOwnProperty('api')) {
                    api = gvApi.indexVM.api();
                } else {
                    if (console) {
                        console.warn('Não foi possível retornar a api da gridview na página de listagem, não foi encontrado a api da gridview.');
                    }
                }
            } else {
                if (console) {
                    console.warn('Não foi possível retornar a api da gridview na página de listagem, o contexto da lista não foi encontrado');
                }
            }
            //ko.contextFor(document.getElementById('route-container-' + routeIndex).getElementsByTagName('grid-view-index')[0])['$data'].indexVM.api().selectedData().done(function (r) { });

            return api;
        },
        guid: function () {
        	function s4() {
        		return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
        	}
        	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
				s4() + '-' + s4() + s4() + s4();
        },
        getId: function (data) {
            var id = null;
            if (typeof (data) === 'string' || !isNaN(data)) {
                return data;
            }
        },
        getViewModel: function (params) {
            if (!params.hasOwnProperty('url'))
                throw ('Parâmetro "url" obrigatório');
            if (params.hasOwnProperty('id')) {
                return $.ajaxGetJsonAntiforgery(params.url, params, null, Route.getToken());
            } else {
                throw ('Parâmetro "id" obrigatório');
            }
        },
        hasClass: function (ele, cls) {
            return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
        },
        addClass: function (ele, cls) {
            if (!this.hasClass(ele, cls)) ele.className += " " + cls;
        },
        removeClass: function (ele, cls) {
            if (hasClass(ele, cls)) {
                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                ele.className = ele.className.replace(reg, ' ');
            }
        },
        CollapsibleVM: function (collapsibleElement, vmArray) {
            var i = 0,
                length = vmArray.length,
                o = false;
            for (i; i <= length;) {
                if (typeof (vmArray[i]) === 'number') {
                    o = true;
                } else if (vmArray[i] != null && vmArray[i].length > 0)
                    o = true;

                i = i + 1;
            }
            $(collapsibleElement).data('open', o);
        },
        CollapsibleCaracteristicaValor: function (vm) {
            var i = 0;
            length = vm.length, o = false;
            for (i; i < length;) {
                if (vm[i].hasOwnProperty('CaracteristicaValor')) {
                    if (vm[i].CaracteristicaValor.Valor != "" && vm[i].CaracteristicaValor.Valor != null)
                        o = true;
                    else if (vm[i].CaracteristicaValor.IdCaracteristicaOpcao != "" && vm[i].CaracteristicaValor.IdCaracteristicaOpcao != null)
                        o = true;
                }
                i = i + 1;
            }
            $('#pnlCamposPersonalizados').data('open', o);
        },
        StringToInt: function (str) {
            return parseInt('0' + str.replace(/\,/g, '').replace(',', '.'));
        },
        StringToFloat: function (str) {
            return parseFloat('0' + str.replace(/\./g, '').replace(',', '.'));
        },
        SetSession: function (key, value) {
            sessionStorage.setItem(key, JSON.stringify(value));
        },
        GetSession: function (key, nullValue) {
            var obj = JSON.parse(sessionStorage.getItem(key));

            if (obj === null && nullValue)
                return nullValue;
            else
                return obj;
        },
        RemoveSession: function (key) {
            sessionStorage.removeItem(key);
        },
        UserImage: null,
        AtualizarDataUltimaInteracao: function () {
            $.ajax({
                global: false,
                type: 'POST',
                url: '/Login/AtualizarDataUltimaInteracao'
            });
        },
        ValidarCPF: function (cpf) {
            var numeros, digitos, soma, i, resultado, digitos_iguais;
            cpf = cpf.replace(/(\.)|(\-)|(\/)/g, '');

            digitos_iguais = 1;
            if (cpf.length < 11)
                return false;
            for (i = 0; i < cpf.length - 1; i++)
                if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais) {
                numeros = cpf.substring(0, 9);
                digitos = cpf.substring(9);
                soma = 0;
                for (i = 10; i > 1; i--)
                    soma += numeros.charAt(10 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                numeros = cpf.substring(0, 10);
                soma = 0;
                for (i = 11; i > 1; i--)
                    soma += numeros.charAt(11 - i) * i;
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
            } else
                return false;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        ValidarCNPJ: function (cnpj) {
            var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
            cnpj = cnpj.replace(/(\.)|(\-)|(\/)/g, '');

            digitos_iguais = 1;
            if (cnpj.length < 14 && cnpj.length < 15)
                return false;
            for (i = 0; i < cnpj.length - 1; i++)
                if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            if (!digitos_iguais) {
                tamanho = cnpj.length - 2
                numeros = cnpj.substring(0, tamanho);
                digitos = cnpj.substring(tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(0))
                    return false;
                tamanho = tamanho + 1;
                numeros = cnpj.substring(0, tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--) {
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1))
                    return false;
                return true;
            } else
                return false;
        }
    };

    return mg;

});