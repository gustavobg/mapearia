﻿// required <script src="//cdnjs.cloudflare.com/ajax/libs/dom4/1.4.5/dom4.js"></script>
(function (root, factory) {   
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals
        root.vanillaUniform = factory();
    }
}(this, function (jquery) {

    'use strict';

    function vanillaUniform(selector, options) {
        // Variables
        var exports = {}, // Object for public APIs
            settings = {},
            onToggle = null, // toggle event
            defaults = {
                classUniform: 'checker',
                checkParentCSS: false
            },
            self = this,
        // Methods
        /**
            * A simple forEach() implementation for Arrays, Objects and NodeLists
            * @@private
            * @@param {Array|Object|NodeList} collection Collection of items to iterate
            * @@param {Function} callback Callback function for each iteration
            * @@param {Array|Object|NodeList} scope Object/NodeList/Array that forEach is iterating over (aka `this`)
            */
        forEach = function (collection, callback, scope) {
            if (Object.prototype.toString.call(collection) === '[object Object]') {
                for (var prop in collection) {
                    if (Object.prototype.hasOwnProperty.call(collection, prop)) {
                        callback.call(scope, collection[prop], prop, collection);
                    }
                }
            } else {
                for (var i = 0, len = collection.length; i < len; i++) {
                    callback.call(scope, collection[i], i, collection);
                }
            }
        },
            /**
            * Merge defaults with user options
            * @@private
            * @@param {Object} defaults Default settings
            * @@param {Object} options User options
            * @@returns {Object} Merged values of defaults and options
            */
        extend = function (defaults, options) {
            var extended = {};
            forEach(defaults, function (value, prop) {
                extended[prop] = defaults[prop];
            });
            forEach(options, function (value, prop) {
                extended[prop] = options[prop];
            });
            return extended;
        },
        triggerEvent = function (elem, type, event) {
            if (typeof (elem.fireEvent) === 'function') {
                elem.fireEvent("on" + type);
            } else {
                event = document.createEvent("MouseEvents");
                event.initMouseEvent(type, true, true, elem.ownerDocument.defaultView,
                    0, 0, 0, 0, 0, false, false, false, false, 0, null);
                elem.dispatchEvent(event);
            }
        },
        addClass = function (el, className) {
            if (el.classList)
                el.classList.add(className);
            else
                el.className += ' ' + className;
            return el;
        },
        wrapInner = function (el, before, after) {
            var wrapped = before + el.innerHTML + after;
            el.innerHTML = wrapped;
        },
        wrap = function (el, before, after) {
            el.outerHTML = before + el.outerHTML + after;
        },
        isChecked = function (el) {

        },
        // walk throught selector
        initialize = function (selector) {
            if (typeof (selector) === 'string') {
                forEach(elements, function (el) {
                    create(el);
                });
            } else if (typeof (selector) === 'object') {
                create(selector);
            };
        },
        log = function (message) {
            if (console && console.hasOwnProperty('log'))
                console.log(message);
        },
		_element = null,
		_container = null,
        create = function (el) {
            if (el.tagName === 'INPUT' && (el.type === 'radio' || el.type === 'checkbox')) {
                var container = document.createElement('div');
                if (el.type === 'radio') {
                    container.classList.add('vanilla-radio');
                    el.before(container); // wraps
                    container.append(el);
                } else {
                    container.classList.add('vanilla-checkbox');
                    el.before(container);
                    container.append(el);
                }
                if (el.checked) {
                    addClass(container, 'checked');
                }
                if (el.getAttribute('disabled') != null) {
                    addClass(container, 'disabled');
                }
                container.append(document.createElement('span').cloneNode(true)); // insert span to use content :before icon font                

                setupEvents(el);

                _container = container;
                _element = el;
            }
        },
        setupEvents = function (el) {
            var input = el,
                container = el.parentNode,
                i = 0,
                idAttr = input.getAttribute('id'),
                labels = idAttr != null && idAttr.length > 0 ? document.querySelectorAll("[for=" + idAttr + "]") : [],
                len = labels.length,
                isDisabled = input.getAttribute('disabled') != null;

            for (i; i < len;) {
                // handle label click
                labels[i].addEventListener('click', function (e) {
                    log('label click');
                    e.stopPropagation();
                    e.preventDefault();
                    if (!isDisabled) {
                        toggleElement(input);
                    }
                });
                i = i + 1;
            }
            container.addEventListener('click', function (e) {
                log('container click');
                e.stopPropagation();
                e.preventDefault();
                if (!isDisabled) {
                    click(e, input);
                    triggerEvent(input, 'change');
                }
            });
        },
        click = function (e, input) {
            toggleElement(input);
        },
        setParentActiveClass = function (container, to) {
            if (self.options.checkParentCSS == true) {
                var parentContainer = container.parentNode;
                if (to == true)
                    parentContainer.classList.add('active');
                else
                    parentContainer.classList.remove('active');
            }

        },
        enable = function (input) {
        	var outerInput = input,
				container = input.parentNode;
        	container.classList.remove('disabled');
        },
		disable = function (input) {
			var outerInput = input,
				container = input.parentNode;
			container.classList.add('disabled');
		},
        toggleElement = function (input, to) {
            var outerInput = input,
                container = input.parentNode,
                value = false;
            if (input.type === 'radio') {
                // remove all checked markup  
                if (input.getAttribute('name')) {
                    forEach(document.getElementsByName(input.name), function (input) {
                        if (outerInput != input) {
                            input.removeAttribute('checked');
                            input.parentNode.classList.remove('checked');
                            setParentActiveClass(input.parentNode, false);
                        }
                    });
                }
            }

            if (to != undefined) {
                if (to == true) {
                    input.setAttribute('checked', 'checked');
                    container.classList.add('checked');
                    setParentActiveClass(container, true)
                } else {
                    input.removeAttribute('checked');
                    container.classList.remove('checked');
                    setParentActiveClass(container, false)
                }
            } else {
                if (input.type === 'radio' && input.checked) {
                } else {
                    if (input.checked) {
                        input.removeAttribute('checked');
                        container.classList.remove('checked');
                        setParentActiveClass(container, false)
                    } else {
                        input.setAttribute('checked', 'checked');
                        container.classList.add('checked');
                        setParentActiveClass(container, false)
                        value = true;
                    }
                }
                
                // Trigger Events Vanilla [http://blog.garstasio.com/you-dont-need-jquery/events/]
                input.dispatchEvent(new CustomEvent('vanillaUniform.change', { bubbles: true, cancelable: true, detail: value }));
            }
        };

        self.options = extend(defaults, options || {});
        initialize(selector); // constructor

        return {
        	toggle: toggleElement,
        	enable: enable,
			disable: disable
        }
    };
   
    return vanillaUniform;
     
    // Public APIs
   

}));