﻿(function (root, factory) {   
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        root.workers = factory(root.jQuery);
    }
}(this, function ($) {

    'use strict';    

    // Variables
    var exports = {}, // Object for public APIs
        settings = {},
        defaults = {
            pool: 30000
        },
        paused = false,
        active = false,
        taskList = [],
        setVisibility = function (state) {
            active = state === "visible" ? true : false;            
        },
        checkVisibility = function () {
            var hidden = "hidden";

            // Standards:
            if (hidden in document)
                document.addEventListener("visibilitychange", onchange);
            else if ((hidden = "mozHidden") in document)
                document.addEventListener("mozvisibilitychange", onchange);
            else if ((hidden = "webkitHidden") in document)
                document.addEventListener("webkitvisibilitychange", onchange);
            else if ((hidden = "msHidden") in document)
                document.addEventListener("msvisibilitychange", onchange);
                // IE 9 and lower:
            else if ("onfocusin" in document)
                document.onfocusin = document.onfocusout = onchange;
                // All others:
            else
                window.onpageshow = window.onpagehide = window.onfocus = window.onblur = onchange;

            function onchange(evt) {
                var v = "visible", h = "hidden",
                    evtMap = {
                        focus: v, focusin: v, pageshow: v, blur: h, focusout: h, pagehide: h
                    };

                evt = evt || window.event;
                if (evt.type in evtMap)
                    setVisibility(evtMap[evt.type]);
                else
                    setVisibility(this[hidden] ? "hidden" : "visible");
            }

            // set the initial state (but only if browser supports the Page Visibility API)
            if (document[hidden] !== undefined)
                onchange({ type: document[hidden] ? "blur" : "focus" });

        }(),        
        isActive = function () {
            return active;
        }, 
        getActiveTasks = function () {       
            return taskList;
        },
        getActiveTasksLength = function () {           
            return taskList.length;
        },
        assignTask = function (id, def, pool) {
            // deffered function            
            //taskList[id] = def;
            taskList.push(def);
        },
        isPaused = function () {
            return paused;
        },
        pauseTasks = function () {
            paused = true;
        },
        resumeTasks = function () {
            paused = false;
        },
        taskRunner = function () {
            var def = $.Deferred(),
                time = window.setInterval(function () {                    
                    if (isPaused() || !isActive()) {
                        return;
                    }
                    if (getActiveTasks().length > 0) {
                        var deferreds = $.map(getActiveTasks(), function (current) {                           
                            //var ajaxOptions = $.extend({ url: base + current }, defaults);
                            //return $.ajax(ajaxOptions);
                            return current();
                        });                    
                        $.when.apply($, deferreds)                            
                            .then(function () {                                
                                // all requests completed                                
                                //console.log(arguments);
                            }, function () {
                                // error
                                //console.log(xhr);
                            }, function () {
                                // progress                                
                            });
                    } else {
                        // Nenhum worker atribuído
                    }
                }, 3000);
            
        },        

    exports = function (options) {
        settings = $.extend(defaults, options || {}); // Merge user options with defaults            
    };

    // Setup ajax
    $.ajaxSetup({
        beforeSend: function () {            
            exports.pauseTasks();
        },
        complete: function (response, xhr) {            
            if (response.status == 401 || response.status == 403) {
                SessaoUsuario.exibeSessaoExpirada(response);
                //Route.redirectToLogin(response);
            } else {
                exports.resumeTasks();
            }
        },
        error: function (response, xhr) {
            if (response.status == 401 || response.status == 403) {
                //Route.redirectToLogin(response);
                SessaoUsuario.exibeSessaoExpirada(response);
            };
        }
    });

    taskRunner();

    exports.assignTask = assignTask;
    exports.pauseTasks = pauseTasks;
    exports.resumeTasks = resumeTasks;
    exports.getActiveTasks = getActiveTasks;
    exports.isActive = isActive;

    // Public APIs
    window.workers = exports;
    return exports;

}));