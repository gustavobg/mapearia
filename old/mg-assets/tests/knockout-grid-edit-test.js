﻿//Ordenar
//- Valor padrão
//- Evento DOM
//- Futura API..
//Paginação
//- Parametros de paginacao
//- Contagem de páginas
//- Paginação exibindo registros inativos
//- Paginação exibindo registros excluídos
//VM Extender
//- Testar comportamento do extender
//dataMapped
//- Testar se foram criados os _ids corretamente
//newItem
//- testar eventos que antecedem o método
//editItem
//- testar se consegue selecionar item para edição (vm e DOM)
//- testar se consegue alterar e salvar um item editado (vm e DOM)
//- Modal: Testar se modal abre corretamente (DOM)
//removeItem
//- Testar se item é corretamente removido (vm e DOM)
//- Testar se item é corretamente removido com a propriedade useHiddenColumns (vm e DOM)
//- Testar se item é corretamente removido com a propriedade useRemovedColumns (vm e DOM)

// TODO Testes da Grid em Ajax

module("knockout-grid-edit");
/*
 - Faz o bind?
 - Valida obrigatório?
 - Exibe mensagem de erro?
 https://github.com/AndersMalmgren/Knockout.Bindings/blob/master/test/test.js
 */


function setupGridEdit() {    
    var c = $('#qunit-fixture'),
        vm = {
            data: ko.observableArray([
                { name: "Moroni", age: 50 },
                { name: "Tiancum", age: 43 },
                { name: "Jacob", age: 27 },
                { name: "Nephi", age: 29 },
                { name: "Enos", age: 34 },
                { name: "Moroni Older", age: 99 },
                { name: "Tiancum", age: 43 },
                { name: "Jacob", age: 27 },
                { name: "Nephi", age: 29 },
                { name: "Zulu", age: 34 },
                { name: "Moroni", age: 50 },
                { name: "Tiancum", age: 43 },
                { name: "Jacob Younger", age: 12 },
                { name: "Nephi", age: 29 },
                { name: "Enos", age: 34 },
                { name: "Tiancum", age: 43 },
                { name: "Jacob", age: 27 },
                { name: "Nephi", age: 29 },
                { name: "Moroni", age: 50 },
                { name: "Tiancum", age: 43 },
                { name: "Tiancum", age: 43 },
                { name: "Jacob", age: 27 },
                { name: "Nephi", age: 29 },
                { name: "Moroni", age: 50 },
                { name: "Tiancum", age: 43 }
            ])
        };

    params = {
        'data': vm.data,
        'title': 'Customer',
        'templateId': 'Customer',
        'columns': [
            { 'id': 'name', 'displayName': 'Customer name', 'header': true, 'sortable': true },
            { 'id': 'age', 'displayName': 'Customer age', 'header': true, 'sortable': true },
            { 'displayName': 'Ações', 'header': true }
        ],
        'useHiddenColumns': true,
        'orderByCol': 'name',
        'orderByDir': 'desc'
    };  

    $('<div id="grid"><div data-bind="component: { name: \'grid-view\', params: params }"></div></div>')
        .appendTo(c)
        .append('<template id="list-' + params.templateId + '"></template>')
        .append('<template id="newEdit-' + params.templateId + '">' +
            '<input id="name" type="text" data-bind="textInput: name" class="form-control" />' +
            '<input id="age" type="text" data-bind="textInput: age" class="form-control" required="required" />' +
            '</template>');   

    ko.cleanNode(c[0]);
    ko.applyBindings(vm, document.getElementById('grid'));
    
    return vm;
}

setupGridEdit();

test('gridview binding', function (assert) {    
    
    assert.ok(true, 'test');
});
//test('parameter set', function () {
//    var vm = setupViewModel(),
//            nameElement = document.getElementById('name');
//    ko.cleanNode(nameElement);
//    ko.applyBindingsToNode(nameElement, { value: name, validate: { required: { param: false, message: '123' } } }, vm);
//    ok($(nameElement).next().text() === '123', 'message appended correctly');
//    ok(vm.invalidFields().indexOf('name') === 0, 'param set to false correctly');
//    vm = setupViewModel();
//    ko.applyBindingsToNode(nameElement, { value: name, validate: { required: { param: true, message: '123' } } }, vm);
//    ok(vm.invalidFields().indexOf('name') >= 0, 'param set to true correctly');
//});
